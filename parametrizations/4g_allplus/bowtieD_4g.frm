#-
#include- ../2to2tools.prc

CF PropGlu,PropGht,VertGlu,VertGlu4,VertGht;
CF INTdboxD;

#procedure to4D

id,once dot(x1?,l1) = dot(x1,k1);
id,once AB(x1?,l1,x2?) = AB(x1,k1,x2);
id,once dot(x1?,l2) = dot(x1,k1)-dot(x1,p1);
id,once AB(x1?,l2,x2?) = AB(x1,k1,x2)-AB(x1,p1,x2);
id,once dot(x1?,l3) = dot(x1,k1)-dot(x1,p1)-dot(x1,p2);
id,once AB(x1?,l3,x2?) = AB(x1,k1,x2)-AB(x1,p1,x2)-AB(x1,p2,x2);
id,once dot(x1?,l6) = -dot(x1,k2);
id,once AB(x1?,l6,x2?) = -AB(x1,k2,x2);
id,once dot(x1?,l5) = -dot(x1,k2)+dot(x1,p4);
id,once AB(x1?,l5,x2?) = -AB(x1,k2,x2)+AB(x1,p4,x2);
id,once dot(x1?,l4) = -dot(x1,k2)+dot(x1,p3)+dot(x1,p4);
id,once AB(x1?,l4,x2?) = -AB(x1,k2,x2)+AB(x1,p3,x2)+AB(x1,p4,x2);
id,once dot(x1?,l7) = -dot(x1,k1)-dot(x1,k2);
id,once AB(x1?,l7,x2?) = -AB(x1,k1,x2)-AB(x1,k2,x2);

#call cancel

*** on-shell conditions ***
id dot(k1,k1) = 0;
id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;

id dot(k2,k2) = 0;
id dot(k2,p4) = 0;
id dot(k2,p3) = s12/2;

#endprocedure


.global

G AMP =

  +EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p1,mu4)*(

    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*VertGlu(-l2,p2,l3,nu2,mu2,rho3)*VertGlu(-l3,-l7,l4,nu3,nu7,rho4)*
    VertGlu(-l4,p3,l5,nu4,mu3,rho5)*VertGlu(-l5,p4,l6,nu5,mu4,rho6)*VertGlu(-l6,l7,l1,nu6,rho7,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*PropGlu(l2,nu2,rho2,mu11)*PropGlu(l3,nu3,rho3,mu11)*PropGlu(l4,nu4,rho4,mu22)*
    PropGlu(l5,nu5,rho5,mu22)*PropGlu(l6,nu6,rho6,mu22)*PropGlu(l7,nu7,rho7,mu33)*INV(dot(l7,l7))

  +xxghost1*
    VertGht(l2,mu1)*VertGht(l3,mu2)*VertGht(l4,nu7)*
    VertGht(l5,mu3)*VertGht(l6,mu4)*VertGht(l1,rho7)*
    PropGht(l1,mu11)*PropGht(l2,mu11)*PropGht(l3,mu11)*PropGht(l4,mu22)*
    PropGht(l5,mu22)*PropGht(l6,mu22)*PropGlu(l7,nu7,rho7,mu33)*INV(dot(l7,l7))

  +xxghost2*
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*VertGlu(-l2,p2,l3,nu2,mu2,rho3)*VertGht(l4,nu3)*
    VertGht(l5,mu3)*VertGht(l6,mu4)*VertGht(l7,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*PropGlu(l2,nu2,rho2,mu11)*PropGlu(l3,nu3,rho3,mu11)*PropGht(l4,mu22)*
    PropGht(l5,mu22)*PropGht(l6,mu22)*PropGht(l7,mu33)*INV(dot(l7,l7))

  +xxghost3*
    VertGht(l2,mu1)*VertGht(l3,mu2)*VertGht(-l7,rho4)*
    VertGlu(-l4,p3,l5,nu4,mu3,rho5)*VertGlu(-l5,p4,l6,nu5,mu4,rho6)*VertGht(l1,nu6)*
    PropGht(l1,mu11)*PropGht(l2,mu11)*PropGht(l3,mu11)*PropGlu(l4,nu4,rho4,mu22)*
    PropGlu(l5,nu5,rho5,mu22)*PropGlu(l6,nu6,rho6,mu22)*PropGht(l7,mu33)*INV(dot(l7,l7))

  +xxdiag2*
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*VertGlu(-l2,p2,l3,nu2,mu2,rho3)*VertGlu(-l3,-p1-p2,l1,nu3,nu7,rho1)*
    VertGlu(-l4,p3,l5,nu4,mu3,rho5)*VertGlu(-l5,p4,l6,nu5,mu4,rho6)*VertGlu(-l6,p1+p2,l4,nu6,rho7,rho4)*
    PropGlu(l1,nu1,rho1,mu11)*PropGlu(l2,nu2,rho2,mu11)*PropGlu(l3,nu3,rho3,mu11)*PropGlu(l4,nu4,rho4,mu22)*
    PropGlu(l5,nu5,rho5,mu22)*PropGlu(l6,nu6,rho6,mu22)*PropGlu(p1+p2,nu7,rho7,0)/s12

  +xxghost4*
    VertGht(l2,mu1)*VertGht(l3,mu2)*VertGht(l1,nu7)*
    VertGlu(-l4,p3,l5,nu4,mu3,rho5)*VertGlu(-l5,p4,l6,nu5,mu4,rho6)*VertGlu(-l6,p1+p2,l4,nu6,rho7,rho4)*
    PropGht(l1,mu11)*PropGht(l2,mu11)*PropGht(l3,mu11)*PropGlu(l4,nu4,rho4,mu22)*
    PropGlu(l5,nu5,rho5,mu22)*PropGlu(l6,nu6,rho6,mu22)*PropGlu(p1+p2,nu7,rho7,mu33)/s12

  +xxghost5*
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*VertGlu(-l2,p2,l3,nu2,mu2,rho3)*VertGlu(-l3,-p1-p2,l1,nu3,nu7,rho1)*
    VertGht(l5,mu3)*VertGht(l6,mu4)*VertGht(l4,rho7)*
    PropGlu(l1,nu1,rho1,mu11)*PropGlu(l2,nu2,rho2,mu11)*PropGlu(l3,nu3,rho3,mu11)*PropGht(l4,mu22)*
    PropGht(l5,mu22)*PropGht(l6,mu22)*PropGlu(p1+p2,nu7,rho7,mu33)/s12

  +xxghost6*
    VertGht(l2,mu1)*VertGht(l3,mu2)*VertGht(l1,nu7)*
    VertGht(l5,mu3)*VertGht(l6,mu4)*VertGht(l4,rho7)*
    PropGht(l1,mu11)*PropGht(l2,mu11)*PropGht(l3,mu11)*PropGht(l4,mu22)*
    PropGht(l5,mu22)*PropGht(l6,mu22)*PropGlu(p1+p2,nu7,rho7,mu33)/s12

  +xxdiag3*
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*VertGlu(-l2,p2,l3,nu2,mu2,rho3)*VertGlu4(-l3,l4,-l6,l1,nu3,rho4,nu6,rho1)*
    VertGlu(-l4,p3,l5,nu4,mu3,rho5)*VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    PropGlu(l1,nu1,rho1,mu11)*PropGlu(l2,nu2,rho2,mu11)*PropGlu(l3,nu3,rho3,mu11)*PropGlu(l4,nu4,rho4,mu22)*
    PropGlu(l5,nu5,rho5,mu22)*PropGlu(l6,nu6,rho6,mu22)

  +xxghost7*
    VertGht(l2,mu1)*VertGht(l3,mu2)*
    VertGht(l5,mu3)*VertGht(l6,mu4)*
    PropGht(l1,mu11)*PropGht(l2,mu11)*PropGht(l3,mu11)*PropGht(l4,mu22)*
    PropGht(l5,mu22)*PropGht(l6,mu22)

)


   - INTdboxD(k1,k2,mu11,mu12,mu22)*INV(dot(l7,l7))

;

id xxghost1 = -2;
id xxghost2 = -2;
id xxghost3 = -2;

repeat;
id,once VertGlu(p1?, p2?, p3?, mu1?, mu2?, mu3?) = I/rt2*(
    GGG(mu2,mu3)*(MOM(p2,mu1)-MOM(p3,mu1))
  + GGG(mu3,mu1)*(MOM(p3,mu2)-MOM(p1,mu2))
  + GGG(mu1,mu2)*(MOM(p1,mu3)-MOM(p2,mu3))
);

id,once VertGlu4(p1?,p2?,p3?,p4?,mu1?,mu2?,mu3?,mu4?) = I/2*(
  - GGG(mu1,mu2)*GGG(mu3,mu4)
  - GGG(mu2,mu3)*GGG(mu1,mu4)
  + 2*GGG(mu1,mu3)*GGG(mu2,mu4)
);

id,once VertGht(p1?, mu1?) = I/rt2*(
    MOM(p1,mu1)
);

id MOM(p1+p2,mu1?) = MOM(p1,mu1)+MOM(p2,mu1);
id MOM(-p1-p2,mu1?) = -MOM(p1,mu1)-MOM(p2,mu1);

#call Lcontract
id,once PropGlu(p?, mu1?, mu2?, m?) = I*( -GGG(mu1,mu2) );
id,once PropGht(p?, m?) = 1;
#call Lcontract

endrepeat;
.sort

id Ds = [Ds-2]+2;
*id [Ds-2] = Ds-2;
*if(count(Ds,2)<2);
*discard;
*endif;

repeat;
#call to4D
id s13 = -s12-s14;
argument INV;
#call to4D
endargument;
endrepeat;
id dot(k1,k2) = dot(k1,k2)-mu12/2;
argument INV;
id dot(k1,k2) = dot(k1,k2)-mu12/2;
endargument;
.sort

#call cancel
#call simplify4

B VertGlu;
print[];
.sort

#include- dboxD.prc

#call subsLmom(../final,bowtieD)
id mu12 = tau5;
#call cancel
argument INV;
#call subsLmom(../final,bowtieD)
id mu12 = tau5;
#call cancel
endargument;
#call simplify4

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);
id NUM(x1?) = x1;
.sort

AB tau1,tau2,tau3,tau4,tau5,s12,s13,s14;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id s13 = -s12-s14;
argument;
id s13 = -s12-s14;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(s12+s14) = -s13;

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);

#call cancel
id NUM(x1?) = x1;

.sort
#write <tmp.prc> "G AMP = %e" AMP

.sort
multiply ds(0,0,0,0,0);
repeat;
id,once tau1*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1+1,x2,x3,x4,x5);
id,once tau2*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2+1,x3,x4,x5);
id,once tau3*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3+1,x4,x5);
id,once tau4*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3,x4+1,x5);
id,once tau5*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3,x4,x5+1);
endrepeat;

#include- ../final/bowtieD.coeffvecs.prc

B INV;
print[];
.store

#do k=1,146

G dvec'k' = AMP;
id ds('k') = 1;
id ds(x?) = 0;
id NUM(x?) = x;

print+s;
.store

#enddo

#include- ../final/bowtieD.integrand.prc
#include- ../final/bowtieD.coeffvecs.prc

#do k=1,146
id ds('k') = dvec'k';
#enddo

denominators INV;

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);
id INV(s12+s14) = -1/s13;

#call simplify4

id cs(x1?,x2?,x3?,x4?,x5?,x6?,x7?) =
  (2*dot(k1,p4))^x3*
  (2*dot(k2,p1))^x4*
  (2*dot(k1,omega4))^x1*
  (2*dot(k2,omega4))^x2*
  mu11^x5*
  mu12^x6*
  mu22^x7
;
.sort

id dot(k1,omega4)*dot(k2,omega4) = -s14*s13/s12/2*(
    + 1/2*s14*s12*s13^-1
          - mu12
          - dot(p1,k2)*s12*s13^-1
          - 2*dot(p1,k2)*dot(p4,k1)*s14^-1*s12*s13^-1
          - 4*dot(p1,k2)*dot(p4,k1)*s13^-1
          - dot(p4,k1)*s12*s13^-1
          - dot(k1,k2)
);

id NUM(x?) = x;
#call simplify4

AB mu11,mu12,mu22;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

#write <bowtieD.prc> "id INTbowtieD(k1,k2,mu11,mu12,mu22) = %e" INTbowtieD

B dot,NUM,mu11,mu22,mu12;
format 150;
print+s;
.end
