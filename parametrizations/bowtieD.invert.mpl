interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

s13:=-s12-s14;

# solving bow-tie in 4-2e dimensions

read("bowtieD.reduceM.log"):
unassign('Dvec'):

dvec:=Vector(146):
for k from 1 to 146 do:
dvec[k]:=Dvec[k]:
od:

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(eqsU):
Ieqs:=MatrixInverse(eqsSq):

dvecT:=dtransform.dvec:

dvecR:=dvecT:
Cvec:=Ieqs.dvecR:

fd:=fopen("final/bowtieD.integrand.mpl",WRITE):

fprintf(fd,"Cvec:=Vector(146):\n"):
for k from 1 to 146 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,simplify(Cvec[k])):
od:
fclose(fd):


read("final/bowtieD.coeffvecs.mpl"):

integrand:=0:
for i from 1 to 146 do:
integrand:=integrand+Clbl[i]*Cvec[i];
od;

fd:=fopen("final/bowtieD.integrand.prc",WRITE):
fprintf(fd,"L INTbowtieD = \n%A;\n",integrand):
fclose(fd):

