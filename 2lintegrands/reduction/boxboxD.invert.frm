* vim: set foldmethod=marker:
#-
S Omega;
S s,t,u,tau,tau1,...,tau4;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,p,x,P,h,k,e,q,c;
S mS;
S k1f,k2f,k3f,k4f,k1,k2;
auto S mu;
S l1,...,l20;

auto S Basis;
CF Power,cc,DP,nc;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}
*{{{ simplify3
#procedure simplify3

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS

id AB(p2,P1,p3) = 0;
id AB(p3,P1,p2) = 0;
id S(p2,p3) = S(P1);
id AB(p2,P1,p2) = -S(P1);

#endprocedure
*}}}
*{{{ simplify4
#procedure simplify4

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p4)*IA(p2?,p4) = -B(p2,p3)*IB(p1,p3);
id B(p1?,p4)*IB(p2?,p4) = -A(p2,p3)*IA(p1,p3);
id A(px?,p4)*B(p4,py?!{px}) = -AB(px,MOM(p1,p2,p3),py);
#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS
id AB(p1,MOM(p2,p3),p1) = -S(p1,p4);
id AB(p2,MOM(p1,p3),p2) = -S(p1,p3);
id AB(p3,MOM(p1,p2),p3) = -S(p1,p2);
id S(p3,p4) = S(p1,p2);
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);

#endprocedure
*}}}

off stats;

#define topo "boxboxD"
.global

#do i=1,7
G L'i' = l'i';
#enddo

id l1=k1;
id l2=k1-p1;
id l3=k1-p1-p2;
id l4=-k2+p3+p4;
id l5=-k2+p4;
id l6=-k2;
id l7=-k2-k1;
.sort

.store
#do sol=1,1
G K1'sol' = k1'sol';
G K2'sol' = k2'sol';
G MU11'sol' = mu11'sol';
G MU12'sol' = mu12'sol';
G MU22'sol' = mu22'sol';
#enddo

id k11 = p1+1/4*(S(p1,p4)-tau1+tau3)*IS(p1,p4)*A(p2,p3)*IA(p1,p3)*eta(p1,p2)+1/4*(S(p1,p4)-tau1-tau3)*IS(p1,p3)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k21 = p4-1/4*(S(p1,p4)-tau2-tau4)*IS(p1,p4)*A(p4,p1)*IA(p3,p1)*eta(p3,p4)-1/4*(S(p1,p4)-tau2+tau4)*IS(p1,p3)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id mu111 = -1/4*S(p1,p2)*IS(p1,p4)*IS(p1,p3)*((S(p1,p4)-tau1)^2-tau3^2);
id mu121 = -1/2*S(p1,p2)*IS(p1,p4)*IS(p1,p3)*(-S(p1,p4)^2+S(p1,p4)*(tau1+tau2)+tau1*tau2-tau3*tau4+2*S(p1,p4)*IS(p1,p2)*tau1*tau2);
id mu221 = -1/4*S(p1,p2)*IS(p1,p4)*IS(p1,p3)*((S(p1,p4)-tau2)^2-tau4^2);

.store

L ISP1 = DP(L1,p4);
L ISP2 = DP(-L6,p1);
L ISP3 = DP(L1,Omega);
L ISP4 = DP(-L6,Omega);
L ISP5 = mu11;
L ISP6 = mu12;
L ISP7 = mu22;

multiply replace_(k1,K11);
multiply replace_(k2,K21);
multiply replace_(mu11,MU111);
multiply replace_(mu12,MU121);
multiply replace_(mu22,MU221);
.sort

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,4
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
id Omega = ( AB(p2,p3,p1)*AB(p1,mu,p2) - AB(p1,p3,p2)*AB(p2,mu,p1) )/2*IS(p1,p2);
#call lprod

endrepeat;

id NUM(x?) = x;
#call sortINV
#call cancel
#call simplify4
#call cancel
id S(p1,p4)=-S(p1,p2)-S(p1,p3);
#call cancel

id cc(?x) = 1;

B tau1,...,tau4;
print;
.end
*.store

G Delta = Basis'topo';
#include- 'topo'.basis.h
id Power(x1?,x2?) = x1^x2;
id nc(x?)*cc(?nn) = nc(x,?nn);
.store

#do sol=1,1

G Delta'sol' = Delta;

multiply replace_(k1,K1'sol');
multiply replace_(k2,K2'sol');
multiply replace_(mu11,MU11'sol');
multiply replace_(mu12,MU12'sol');
multiply replace_(mu22,MU22'sol');
.sort

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,4
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
id Omega = ( AB(p2,p3,p1)*AB(p1,mu,p2) - AB(p1,p3,p2)*AB(p2,mu,p1) )/2*IS(p1,p2);
#call lprod

endrepeat;

id NUM(x?) = x;
#call sortINV
#call cancel
#call simplify4
#call cancel
id S(p1,p4)=-S(p1,p2)-S(p1,p3);
#call cancel

B tau1,...,tau4;
print[];
.store

#enddo

#write <final/'topo'.coeffvecs.mpl> "Cvec:=Vector(160):" 
#do cc=1,160
L cveccount = Delta;
id nc('cc',?x$clbl) = 1;
id nc(?x)=0;
.sort
#write <final/'topo'.coeffvecs.mpl> "Clbl['cc']:=cs['$clbl']:"
.store
#enddo

#write <'topo'.reduceM.log> "ReduceMatrix:=Matrix(160,160):"
#write <final/'topo'.coeffvecs.mpl> "Dvec:=Vector(160):" 
#define dd "0"

#do sol=1,1
#do pow1=0,4
#do pow2=0,4
#do pow3=0,4
#do pow4=0,4

#$nonzero=0;
L tmpRow = Delta'sol';

if(count(tau1,1)!='pow1');
discard;
endif;
id tau1=1;
id 1/tau1=1;

if(count(tau2,1)!='pow2');
discard;
endif;
id tau2=1;
id 1/tau2=1;

if(count(tau3,1)!='pow3');
discard;
endif;
id tau3=1;
id 1/tau3=1;

if(count(tau4,1)!='pow4');
discard;
endif;
id tau4=1;
id 1/tau4=1;
.sort

$nonzero=$nonzero+1;

.sort
hide;

#write "'pow1','pow2','pow3','pow4','$nonzero'" 

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "Dvec['dd']:=ds['pow1','pow2','pow3','pow4']:" 

#do cc=1,160

L tmpEl'cc' = tmpRow;
id nc('cc',?x$clbl) = 1;
id nc(?x)=0;
.sort

#write <'topo'.reduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
.sort
hide;

#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo
#enddo
#enddo

.end
