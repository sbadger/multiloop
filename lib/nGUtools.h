* */ vim: set foldmethod=marker: */

#define libpath "lib/"
S l0,...,l15;
auto S b,r;
auto F fTMP;

CF Pent,Box,Tri,Bub;
CF PentBar,BoxBar,TriBar,BubBar;
CF Kmom,SIGN,dot,Idot,pow;

auto S a;
S Ca,Cb;

auto CF poly;
CF cf,loop,xsmpstr,spmom,M;
CF sortinv,ff;
CF Delta,IDelta;
CF RtDelta,IRtDelta;
CF Iinv,Inum,invtmp,numtmp;

set ls:l1,...,l15,l0;
set ls2:l1,...,l15,l0;

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}

*{{{convertS
#procedure convertS
#do i={2,3,4,5,6,7,8}
id S(p1?,...,p'i'?) = S(p1+...+p'i');
id IS(p1?,...,p'i'?) = IS(p1+...+p'i');
id PP(m?,p1?,...,p'i'?) = PP(m,p1+...+p'i');
id IPP(m?,p1?,...,p'i'?) = IPP(m,p1+...+p'i');
#enddo
#endprocedure
*}}}

*{{{numargs
#procedure numargs

id AB(p1?,p2?,p3?,p4?,p5?) = AB3(p1,p2,p3,p4,p5);
id AA(p1?,p2?,p3?,p4?,p5?,p6?) = AA4(p1,p2,p3,p4,p5,p6);
id BB(p1?,p2?,p3?,p4?,p5?,p6?) = BB4(p1,p2,p3,p4,p5,p6);
id AB(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = AB5(p1,p2,p3,p4,p5,p6,p7);
id AA(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = AA6(p1,p2,p3,p4,p5,p6,p7,p8);
id BB(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = BB6(p1,p2,p3,p4,p5,p6,p7,p8);

id IAB(p1?,p2?,p3?,p4?,p5?) = IAB3(p1,p2,p3,p4,p5);
id IAA(p1?,p2?,p3?,p4?,p5?,p6?) = IAA4(p1,p2,p3,p4,p5,p6);
id IBB(p1?,p2?,p3?,p4?,p5?,p6?) = IBB4(p1,p2,p3,p4,p5,p6);
id IAB(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = IAB5(p1,p2,p3,p4,p5,p6,p7);
id IAA(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = IAA6(p1,p2,p3,p4,p5,p6,p7,p8);
id IBB(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = IBB6(p1,p2,p3,p4,p5,p6,p7,p8);

#endprocedure
*}}}

*{{{undonumargs
#procedure undonumargs

id AB3(p1?,p2?,p3?,p4?,p5?) = AB(p1,p2,p3,p4,p5);
id AA4(p1?,p2?,p3?,p4?,p5?,p6?) = AA(p1,p2,p3,p4,p5,p6);
id BB4(p1?,p2?,p3?,p4?,p5?,p6?) = BB(p1,p2,p3,p4,p5,p6);
id AB5(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = AB(p1,p2,p3,p4,p5,p6,p7);
id AA6(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = AA(p1,p2,p3,p4,p5,p6,p7,p8);
id BB6(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = BB(p1,p2,p3,p4,p5,p6,p7,p8);

id IAB3(p1?,p2?,p3?,p4?,p5?) = IAB(p1,p2,p3,p4,p5);
id IAA4(p1?,p2?,p3?,p4?,p5?,p6?) = IAA(p1,p2,p3,p4,p5,p6);
id IBB4(p1?,p2?,p3?,p4?,p5?,p6?) = IBB(p1,p2,p3,p4,p5,p6);
id IAB5(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = IAB(p1,p2,p3,p4,p5,p6,p7);
id IAA6(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = IAA(p1,p2,p3,p4,p5,p6,p7,p8);
id IBB6(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = IBB(p1,p2,p3,p4,p5,p6,p7,p8);

#endprocedure
*}}}

*{{{sortnuminv
#procedure sortnuminv

*** due to a bug in some versions of form    ***
*** we first map to a non-anti-symmetric     ***
*** functions before applying the identities ***

id num(0)=0;
id inv(0)=1/zero;

argument inv,num;
id A(px?,py?) = naA(px,py);
id IA(px?,py?) = InaA(px,py);
id B(px?,py?) = naB(px,py);
id IB(px?,py?) = InaB(px,py);
endargument;

splitarg num,inv;

id num(px?) = px;
al inv(px?) = sortinv(px);

argument sortinv;
#do fun={AA,BB,naA,naB,AB,S,PP,dot,num,inv,Delta,RtDelta}
id 'fun'(?x) = ff('fun'(?x));
id I'fun'(?x) = ff(I'fun'(?x));
#enddo
repeat;
id ff(px1?)*ff(px2?) = ff(px1*px2);
endrepeat;
endargument;

id sortinv(px?) = 1/px;
id 1/ff(px?) = sortinv(px);

argument sortinv;
#do fun={AA,BB,naA,naB,AB,S,PP,dot,Delta,RtDelta}
id 'fun'(?x) = I'fun'(?x);
al I'fun'(?x) = 'fun'(?x);
#enddo
id num(px?) = inv(px);
al inv(px?) = num(px);
endargument;

id sortinv(px?) = px;

repeat;
id num(px1?,px2?,?x) = num(px1+px2,?x);
id inv(px1?,px2?,?x) = inv(px1+px2,?x);
endrepeat;

argument inv,num;
id naA(px?,py?) = A(px,py);
id InaA(px?,py?) = IA(px,py);
id naB(px?,py?) = B(px,py);
id InaB(px?,py?) = IB(px,py);
endargument;

id naA(px?,py?) = A(px,py);
id InaA(px?,py?) = IA(px,py);
id naB(px?,py?) = B(px,py);
id InaB(px?,py?) = IB(px,py);

id num(px?)*inv(px?) = 1;

#endprocedure
*}}}

*{{{simplifystrings
#procedure simplifystrings

#call cancel

id BB(px?,py?) = 0;
id IBB(px?,py?) = 0;
id AA(px?,py?) = 0;
id IAA(px?,py?) = 0;

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(px?ps,py?,?y) = 'fun'(px,xsmpstr(px)*px*py,?y);
argument 'fun';
	id (px?ps)^2 = 0;
	id xsmpstr(p1?ps) = 1/p1;
endargument;
id 'fun'(?y,py?,px?ps) = 'fun'(?y,xsmpstr(px)*px*py,px);
argument 'fun';
	id (px?ps)^2 = 0;
	id xsmpstr(p1?ps) = 1/p1;
endargument;
#enddo

repeat;
id AA(p1?ps,p2?ps,?x,px?) = A(p1,p2)*BA(p2,?x,px);
id AB(p1?ps,p2?ps,?x,px?) = A(p1,p2)*BB(p2,?x,px);
id BB(p1?ps,p2?ps,?x,px?) = B(p1,p2)*AB(p2,?x,px);

id AA(px?,?x,p1?ps,p2?ps) = AB(px,?x,p1)*A(p1,p2);
id AB(px?,?x,p1?ps,p2?ps) = AA(px,?x,p1)*B(p1,p2);
id BB(px?,?x,p1?ps,p2?ps) = BA(px,?x,p1)*B(p1,p2);

id IAA(p1?ps,p2?ps,?x,px?) = IA(p1,p2)*IBA(p2,?x,px);
id IAB(p1?ps,p2?ps,?x,px?) = IA(p1,p2)*IBB(p2,?x,px);
id IBB(p1?ps,p2?ps,?x,px?) = IB(p1,p2)*IAB(p2,?x,px);

id IAA(px?,?x,p1?ps,p2?ps) = IAB(px,?x,p1)*IA(p1,p2);
id IAB(px?,?x,p1?ps,p2?ps) = IAA(px,?x,p1)*IB(p1,p2);
id IBB(px?,?x,p1?ps,p2?ps) = IBA(px,?x,p1)*IB(p1,p2);

id AA(p1?ps,py?,p2?ps,?x,px?) = AB(p1,py,p2)*AA(p2,?x,px);
id IAA(p1?ps,py?,p2?ps,?x,px?) = IAB(p1,py,p2)*AA(p2,?x,px);

id BB(p1?ps,py?,p2?ps,?x,px?) = BA(p1,py,p2)*BB(p2,?x,px);
id IBB(p1?ps,py?,p2?ps,?x,px?) = IBA(p1,py,p2)*IBB(p2,?x,px);

id AB(px?,?x,p1?ps,py?,pz?ps) = AB(px,?x,p1)*AB(p1,py,pz);
id IAB(px?,?x,p1?ps,py?,pz?ps) = IAB(px,?x,p1)*IAB(p1,py,pz);

id AA(px?,?x,p1?ps,py?,pz?ps) = AA(px,?x,p1)*BA(p1,py,pz);
id IAA(px?,?x,p1?ps,py?,pz?ps) = IAA(px,?x,p1)*IBA(p1,py,pz);

id BB(px?,?x,p1?ps,py?,pz?ps) = BB(px,?x,p1)*AB(p1,py,pz);
id IBB(px?,?x,p1?ps,py?,pz?ps) = IBB(px,?x,p1)*IAB(p1,py,pz);

#call cancel;
endrepeat;

#endprocedure
*}}}

*{{{sortminusl
#procedure sortminusl

id A(-l1?,px?) = xsgn*I*A(l1,px);
id IA(-l1?,px?) = -xsgn*I*IA(l1,px);
id B(-l1?,px?) = xsgn*I*B(l1,px);
id IB(-l1?,px?) = -xsgn*I*IB(l1,px);

id AB(?x,-l1?) = xsgn*I*AB(?x,l1);
id AB(-l1?,?x) = xsgn*I*AB(l1,?x);

id IAB(?x,-l1?) = -xsgn*I*IAB(?x,l1);
id IAB(-l1?,?x) = -xsgn*I*IAB(l1,?x);

id AA(-l1?,?x) = xsgn*I*AA(l1,?x);
id BB(-l1?,?x) = xsgn*I*BB(l1,?x);
id IAA(-l1?,?x) = -xsgn*I*IAA(l1,?x);
id IBB(-l1?,?x) = -xsgn*I*IBB(l1,?x);
id AA(?x,-l1?) = xsgn*I*AA(?x,l1);
id BB(?x,-l1?) = xsgn*I*BB(?x,l1);
id IAA(?x,-l1?) = -xsgn*I*IAA(?x,l1);
id IBB(?x,-l1?) = -xsgn*I*IBB(?x,l1);

id AB(px?,?x,-l1?,?y,py?) = -AB(px,?x,l1,?y,py);
id IAB(px?,?x,-l1?,?y,py?) = -IAB(px,?x,l1,?y,py);
id AA(px?,?x,-l1?,?y,py?) = -AA(px,?x,l1,?y,py);
id IAA(px?,?x,-l1?,?y,py?) = -IAA(px,?x,l1,?y,py);
id BB(px?,?x,-l1?,?y,py?) = -BB(px,?x,l1,?y,py);
id IBB(px?,?x,-l1?,?y,py?) = -IBB(px,?x,l1,?y,py);

#do i=1,4
id IA(b'i',r'i')*IB(r'i',b'i') = xsgn*IAB(b'i',l'i',b'i');
id A(px?,r'i')*B(r'i',py?) = AB(px,r'i',py);
id B(px?,r'i')*AB(r'i',?y) = BB(px,r'i',?y);
id A(px?,r'i')*AB(?y,r'i') = -AA(?y,r'i',px);
id B(px?,r'i')*AA(r'i',?y) = BA(px,r'i',?y);
id A(px?,r'i')*BB(?y,r'i') = -BA(?y,r'i',px);
#enddo

id xsgn=1;
id I^2=-1;

#endprocedure
*}}}

*{{{collectkf
#procedure collectkf(Kf,K,k)
*** for eliminating K1f/K2f in 2-mass triangles ***

#call cancel
id IA('k','Kf')*IB('Kf',py?) = IAB('k','K',py);
id IA(px?,'Kf')*IB('Kf','k') = IAB(px,'K','k');
id IA(px?,'Kf')*IB('Kf',py?) = IAB(px,'Kf',py);

id IA(px?,'Kf') = IAB(px,'K','k')*B('Kf','k');
id IAA(?x,'Kf') = IAB(?x,'K','k')*B('Kf','k');
id IAA('Kf',?x) = IBA('k','K',?x)*B('k','Kf');
id IAB('Kf',?x) = IBB('k','K',?x)*B('k','Kf');
id IB(px?,'Kf') = IBA(px,'K','k')*A('Kf','k');
id IBB(?x,'Kf') = IBA(?x,'K','k')*A('Kf','k');
id IBB('Kf',?x) = IAB('k','K',?x)*A('k','Kf');

#call cancel

id A('k','Kf')*IB('Kf',py?) = AB('k','K',py);
id A(px?,'Kf')*IB('Kf','k') = AB(px,'K','k');
id A(px1?,'Kf')*B('Kf',px2?) = AB(px1,'Kf',px2);
id A(px1?,'Kf')*AB(px2?,px3?,'Kf') = AA(px1,'Kf',px3,px2);

print+s;
.end

#endprocedure
*}}}

*{{{collectl
#procedure collectl(a,b)

id IA(l1?ls2,l2?ls2) = IBB('b',l1,l2,'b')*B('b',l1)*B(l2,'b');
id IB(l1?ls2,l2?ls2) = IAA('b',l1,l2,'b')*A('a',l1)*A(l2,'a');

id A(px?!ls2,l1?ls2)*B(l1?ls2,py?!ls2) = AB(px,l1,py);
id B(px?!ls2,l1?ls2)*AB(l1?ls2,?x) = BB(px,l1,?x);
id AB(?x,l1?ls2)*A(l1?ls2,py?!ls) = AA(?x,l1,py);

id IA(px?!ls2,l1?ls2)*IB(l1?ls2,py?!ls2) = IAB(px,l1,py);
id IB(px?!ls2,l1?ls2)*IAB(l1?ls2,?x) = IBB(px,l1,?x);
id IAB(?x,l1?ls2)*IA(l1?ls2,py?!ls2) = IAA(?x,l1,py);
.sort
id A(px?,l1?ls2)*B(l1?ls2,py?) = AB(px,l1,py);
id B(px?,l1?ls2)*AB(l1?ls2,?x) = BB(px,l1,?x);
id AB(?x,l1?ls2)*A(l1?ls2,py?) = AA(?x,l1,py);

id IA(px?,l1?ls2)*IB(l1?ls2,py?!ls2) = IAB(px,l1,py);
id IB(px?,l1?ls2)*IAB(l1?ls2,?x) = IBB(px,l1,?x);
id IAB(?x,l1?ls2)*IA(l1?ls2,py?!ls2) = IAA(?x,l1,py);
.sort

id A(l1?ls2,px?) = BA('b',l1,px)*IB('b',l1);
id AB(l1?ls2,?x) = BB('b',l1,?x)*IB('b',l1);
id AA(l1?ls2,?x) = BA('b',l1,?x)*IB('b',l1);
id AA(?x,l1?ls2) = AB(?x,l1,'b')*IB(l1,'b');

id B(l1?ls2,px?) = AB('a',l1,px)*IA('a',l1);
id AB(?x,l1?ls2) = AA(?x,l1,'a')*IA(l1,'a');
id BB(l1?ls2,?x) = AB('a',l1,?x)*IA('a',l1);
id BB(?x,l1?ls2) = BA(?x,l1,'a')*IA(l1,'a');

id A(px?!ls2,l1?ls2)*B(l1?ls2,py?!ls2) = AB(px,l1,py);
id B(px?!ls2,l1?ls2)*AB(l1?ls2,?x) = BB(px,l1,?x);
id AB(?x,l1?ls2)*A(l1?ls2,py?!ls2) = AA(?x,l1,py);

id IA(px?!ls2,l1?ls2)*IB(l1?ls2,py?!ls2) = IAB(px,l1,py);
id IB(px?!ls2,l1?ls2)*IAB(l1?ls2,?x) = IBB(px,l1,?x);
id IAB(?x,l1?ls2)*IA(l1?ls2,py?!ls2) = IAA(?x,l1,py);

id IA(l1?ls2,px?) = IBA('b',l1,px)*B('b',l1);
id IAB(l1?ls2,?x) = IBB('b',l1,?x)*B('b',l1);
id IAA(l1?ls2,?x) = IBA('b',l1,?x)*B('b',l1);
id IAA(?x,l1?ls2) = IAB(?x,l1,'b')*B(l1,'b');

id IB(l1?ls2,px?) = IAB('a',l1,px)*A('a',l1);
id IAB(?x,l1?ls2) = IAA(?x,l1,'a')*A(l1,'a');
id IBB(l1?ls2,?x) = IAB('a',l1,?x)*A('a',l1);
id IBB(?x,l1?ls2) = IBA(?x,l1,'a')*A(l1,'a');
#call cancel

id A(px?!ls2,l1?ls2)*B(l1?ls2,py?!ls2) = AB(px,l1,py);
id B(px?!ls2,l1?ls2)*AB(l1?ls2,?x) = BB(px,l1,?x);
id AB(?x,l1?ls2)*A(l1?ls2,py?!ls2) = AA(?x,l1,py);

id IA(px?!ls2,l1?ls2)*IB(l1?ls2,py?!ls2) = IAB(px,l1,py);
id IB(px?!ls2,l1?ls2)*IAB(l1?ls2,?x) = IBB(px,l1,?x);
id IAB(?x,l1?ls2)*IA(l1?ls2,py?!ls2) = IAA(?x,l1,py);

#endprocedure
*}}}

*{{{collectl3
#procedure collectl3(a,b)

id A(px?,l3)*B(l3,py?) = AB(px,l3,py);
id B(px?,l3)*AB(l3,?x) = BB(px,l3,?x);
id AB(?x,l3)*A(l3,py?) = AA(?x,l3,py);

id IA(px?,l3)*IB(l3,py?) = IAB(px,l3,py);
id IB(px?,l3)*IAB(l3,?x) = IBB(px,l3,?x);
id IAB(?x,l3)*IA(l3,py?) = IAA(?x,l3,py);
.sort
id A(px?,l3)*B(l3,py?) = AB(px,l3,py);
id B(px?,l3)*AB(l3,?x) = BB(px,l3,?x);
id AB(?x,l3)*A(l3,py?) = AA(?x,l3,py);

id IA(px?,l3)*IB(l3,py?) = IAB(px,l3,py);
id IB(px?,l3)*IAB(l3,?x) = IBB(px,l3,?x);
id IAB(?x,l3)*IA(l3,py?) = IAA(?x,l3,py);
.sort

id A(l3,px?) = BA('b',l3,px)*IB('b',l3);
id AB(l3,?x) = BB('b',l3,?x)*IB('b',l3);
id AA(l3,?x) = BA('b',l3,?x)*IB('b',l3);
id AA(?x,l3) = AB(?x,l3,'b')*IB(l3,'b');

id B(l3,px?) = AB('a',l3,px)*IA('a',l3);
id AB(?x,l3) = AA(?x,l3,'a')*IA(l3,'a');
id BB(l3,?x) = AB('a',l3,?x)*IA('a',l3);
id BB(?x,l3) = BA(?x,l3,'a')*IA(l3,'a');

id A(px?,l3)*B(l3,py?) = AB(px,l3,py);
id B(px?,l3)*AB(l3,?x) = BB(px,l3,?x);
id AB(?x,l3)*A(l3,py?) = AA(?x,l3,py);

id IA(px?,l3)*IB(l3,py?) = IAB(px,l3,py);
id IB(px?,l3)*IAB(l3,?x) = IBB(px,l3,?x);
id IAB(?x,l3)*IA(l3,py?) = IAA(?x,l3,py);

id IA(l3,px?) = IBA('b',l3,px)*B('b',l3);
id IAB(l3,?x) = IBB('b',l3,?x)*B('b',l3);
id IAA(l3,?x) = IBA('b',l3,?x)*B('b',l3);
id IAA(?x,l3) = IAB(?x,l3,'b')*B(l3,'b');

id IB(l3,px?) = IAB('a',l3,px)*A('a',l3);
id IAB(?x,l3) = IAA(?x,l3,'a')*A(l3,'a');
id IBB(l3,?x) = IAB('a',l3,?x)*A('a',l3);
id IBB(?x,l3) = IBA(?x,l3,'a')*A(l3,'a');
#call cancel

id A(px?,l3)*B(l3,py?) = AB(px,l3,py);
id B(px?,l3)*AB(l3,?x) = BB(px,l3,?x);
id AB(?x,l3)*A(l3,py?) = AA(?x,l3,py);

id IA(px?,l3)*IB(l3,py?) = IAB(px,l3,py);
id IB(px?,l3)*IAB(l3,?x) = IBB(px,l3,?x);
id IAB(?x,l3)*IA(l3,py?) = IAA(?x,l3,py);

#call reverseBA

#endprocedure
*}}}

*{{{cleanint
#procedure cleanint
repeat;
#do fun={AA,BB,AB,IAA,IBB,IAB}
id 'fun'(px?,?x,-l1?ls,?y,py?) = -'fun'(px,?x,l1,?y,py);
#enddo
endrepeat;

#do fun={AA,BB,AB}
id 'fun'(px?,?x,py?,py?,?z,pz?) = S(py)*'fun'(px,?x,?z,pz);
#enddo

#do fun={IAA,IBB,IAB}
id 'fun'(px?,?x,py?,py?,?z,pz?) = IS(py)*'fun'(px,?x,?z,pz);
#enddo

splitarg, S;

#do fun={AA,BB,AB,IAA,IBB,IAB}
id 'fun'(p1?,p2?,p3?) = 'fun'(p1,P(p2),p3);
id 'fun'(p1?,p2?,p3?,p4?) = 'fun'(p1,P(p2),P(p3),p4);
id 'fun'(p1?,p2?,p3?,p4?,p5?) = 'fun'(p1,P(p2),P(p3),P(p4),p5);
id 'fun'(p1?,p2?,p3?,p4?,p5?,p6?) = 'fun'(p1,P(p2),P(p3),P(p4),P(p5),p6);
id 'fun'(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = 'fun'(p1,P(p2),P(p3),P(p4),P(p5),P(p6),p7);
id 'fun'(p1?,p2?,p3?,p4?,p5?,p6?,p7?,p8?) = 'fun'(p1,P(p2),P(p3),P(p4),P(p5),P(p6),P(p7),p8);
#enddo

argument AB,AA,BB,IAA,IBB;
splitarg, P;
#do i=1,4
id P(?x1,l'i',?x2) = P(l'i',?x1,?x2);
id P(?x1,-l'i',?x2) = P(-l'i',?x1,?x2);
#enddo
endargument;

#do i=1,4
repeat;
#do fun={AA,BB,AB}
id 'fun'(px?,P(l'i',?x),?y,py?) = 'fun'(px,l'i',?y,py)+'fun'(px,P(?x),?y,py);
id 'fun'(px?,P(-l'i',?x),?y,py?) = -'fun'(px,l'i',?y,py)+'fun'(px,P(?x),?y,py);
id 'fun'(px?,p1?,P(l'i',?x),?y,py?) = 'fun'(px,p1,l'i',?y,py)+'fun'(px,p1,P(?x),?y,py);
id 'fun'(px?,p1?,P(-l'i',?x),?y,py?) = -'fun'(px,p1,l'i',?y,py)+'fun'(px,p1,P(?x),?y,py);
id 'fun'(px?,p1?,p2?,P(l'i',?x),?y,py?) = 'fun'(px,p1,p2,l'i',?y,py)+'fun'(px,p1,p2,P(?x),?y,py);
id 'fun'(px?,p1?,p2?,P(-l'i',?x),?y,py?) = -'fun'(px,p1,p2,l'i',?y,py)+'fun'(px,p1,p2,P(?x),?y,py);
id 'fun'(px?,p1?,p2?,p3?,P(l'i',?x),?y,py?) = 'fun'(px,p1,p2,p3,l'i',?y,py)+'fun'(px,p1,p2,p3,P(?x),?y,py);
id 'fun'(px?,p1?,p2?,p3?,P(-l'i',?x),?y,py?) = -'fun'(px,p1,p2,p3,l'i',?y,py)+'fun'(px,p1,p2,p3,P(?x),?y,py);
#enddo
endrepeat;
#enddo

argument AB,AA,BB,IAA,IBB,IAB;
id P=0;
id P(p1?) = p1;
id P(p1?,p2?) = p1+p2;
id P(p1?,p2?,p3?) = p1+p2+p3;
id P(p1?,p2?,p3?,p4?) = p1+p2+p3+p4;
endargument;

#call cancel;
.sort
#endprocedure
*}}}

*{{{topologise
#procedure topologise

*** factors of i for fermion propagators ***
#do topo={Pent,Box,Tri,Bub}
repeat id,once 'topo'(?x1,ptl(?y,1/2),?x2) = I*'topo'(?x1,TMP(?y,1/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,I/2),?x2) = I*'topo'(?x1,TMP(?y,I/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,3/2),?x2) = I*'topo'(?x1,TMP(?y,3/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,5/2),?x2) = I*'topo'(?x1,TMP(?y,5/2),?x2);
#enddo
#do topo={PentBar,BoxBar,TriBar,BubBar}
repeat id,once 'topo'(?x1,ptl(?y,1/2),?x2) = -I*'topo'(?x1,TMP(?y,1/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,I/2),?x2) = -I*'topo'(?x1,TMP(?y,I/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,3/2),?x2) = -I*'topo'(?x1,TMP(?y,3/2),?x2);
repeat id,once 'topo'(?x1,ptl(?y,5/2),?x2) = -I*'topo'(?x1,TMP(?y,5/2),?x2);
#enddo

argument Pent,Box,Tri,Bub,PentBar,BoxBar,TriBar,BubBar;
id TMP(?x) = ptl(?x);
endargument;
.sort

*{{{ general pentagon
#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
#do m1={1,-1}
#do n1={1,-1}
id Pent(Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?),ptl(l4,'m1',xptl4?),ptl(l5,'n1',xptl5?))
	= AMP(ptl(-l5,-'n1',-xptl5),?x1,ptl(l1,'i1',xptl1))
	* AMP(ptl(-l1,-'i1',-xptl1),?x2,ptl(l2,'j1',xptl2))
	* AMP(ptl(-l2,-'j1',-xptl2),?x3,ptl(l3,'k1',xptl3))
	* AMP(ptl(-l3,-'k1',-xptl3),?x4,ptl(l4,'m1',xptl4))
	* AMP(ptl(-l4,-'m1',-xptl4),?x5,ptl(l5,'n1',xptl5))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)*SIGN(xptl5);
#enddo
#enddo
#enddo
#enddo
#enddo

#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
#do m1={1,-1}
#do n1={1,-1}
id PentBar(Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?),ptl(l4,'m1',xptl4?),ptl(l5,'n1',xptl5?))
	= AMP(ptl(-l5,-'n1',xptl5),?x1,ptl(l1,'i1',-xptl1))
	* AMP(ptl(-l1,-'i1',xptl1),?x2,ptl(l2,'j1',-xptl2))
	* AMP(ptl(-l2,-'j1',xptl2),?x3,ptl(l3,'k1',-xptl3))
	* AMP(ptl(-l3,-'k1',xptl3),?x4,ptl(l4,'m1',-xptl4))
	* AMP(ptl(-l4,-'m1',xptl4),?x5,ptl(l5,'n1',-xptl5))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)*SIGN(xptl5);
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}

*{{{ general box
#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
#do m1={1,-1}
id Box(Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?),ptl(l4,'m1',xptl4?))
	= AMP(ptl(-l4,-'m1',-xptl4),?x1,ptl(l1,'i1',xptl1))
	* AMP(ptl(-l1,-'i1',-xptl1),?x2,ptl(l2,'j1',xptl2))
	* AMP(ptl(-l2,-'j1',-xptl2),?x3,ptl(l3,'k1',xptl3))
	* AMP(ptl(-l3,-'k1',-xptl3),?x4,ptl(l4,'m1',xptl4))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4);
#enddo
#enddo
#enddo
#enddo

#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
#do m1={1,-1}
id BoxBar(Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?),ptl(l4,'m1',xptl4?))
	= AMP(ptl(-l4,-'m1',xptl4),?x1,ptl(l1,'i1',-xptl1))
	* AMP(ptl(-l1,-'i1',xptl1),?x2,ptl(l2,'j1',-xptl2))
	* AMP(ptl(-l2,-'j1',xptl2),?x3,ptl(l3,'k1',-xptl3))
	* AMP(ptl(-l3,-'k1',xptl3),?x4,ptl(l4,'m1',-xptl4))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4);
#enddo
#enddo
#enddo
#enddo

*}}}

*{{{ general triangle
#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
id Tri(Kmom(?x1),Kmom(?x2),Kmom(?x3),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?))
	= AMP(ptl(-l3,-'k1',-xptl3),?x1,ptl(l1,'i1',xptl1))
	* AMP(ptl(-l1,-'i1',-xptl1),?x2,ptl(l2,'j1',xptl2))
	* AMP(ptl(-l2,-'j1',-xptl2),?x3,ptl(l3,'k1',xptl3))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3);
#enddo
#enddo
#enddo

#do i1={1,-1}
#do j1={1,-1}
#do k1={1,-1}
id TriBar(Kmom(?x1),Kmom(?x2),Kmom(?x3),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?),ptl(l3,'k1',xptl3?))
	= AMP(ptl(-l3,-'k1',xptl3),?x1,ptl(l1,'i1',-xptl1))
	* AMP(ptl(-l1,-'i1',xptl1),?x2,ptl(l2,'j1',-xptl2))
	* AMP(ptl(-l2,-'j1',xptl2),?x3,ptl(l3,'k1',-xptl3))*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3);
#enddo
#enddo
#enddo
*}}}

*{{{ general bubble
#do i1={1,-1}
#do j1={1,-1}
id Bub(Kmom(?x1),Kmom(?x2),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?))
	= AMP(ptl(-l2,-'j1',-xptl2),?x1,ptl(l1,'i1',xptl1))
	* AMP(ptl(-l1,-'i1',-xptl1),?x2,ptl(l2,'j1',xptl2))*SIGN(xptl1)*SIGN(xptl2);
#enddo
#enddo

#do i1={1,-1}
#do j1={1,-1}
id BubBar(Kmom(?x1),Kmom(?x2),ptl(l1,'i1',xptl1?),ptl(l2,'j1',xptl2?))
	= AMP(ptl(-l2,-'j1',xptl2),?x1,ptl(l1,'i1',-xptl1))
	* AMP(ptl(-l1,-'i1',xptl1),?x2,ptl(l2,'j1',-xptl2))*SIGN(xptl1)*SIGN(xptl2);
#enddo
#enddo

*}}}
.sort

*** assume this is still a bug in diagrams.mpl
id SIGN(0) =   1;
id SIGN(1/2) = -1;
id SIGN(I/2) = -1;
id SIGN(1) =   -1;
id SIGN(-1) =   -1;
id SIGN(3/2) = -1;
id SIGN(2) =   1;
id SIGN(5/2) = -1;

argument AMP;
id ptl(px?,p,?x) = ptl(px,1,?x);
id ptl(px?,m,?x) = ptl(px,-1,?x);
endargument;

#endprocedure
*}}}

*{{{ adjacentMHVtrick
#procedure adjacentMHVtrick

repeat;
id,once AMP(?x) = TMPout*TMPin(?x)*TMPcount(0,0,0);
  repeat;
    id TMPin(ptl(p1?,h1?,ptl1?),?x1)*TMPout(?x2)*TMPcount(xargs?,hidx?,pidx?) =
    TMPin(?x1)*TMPout(?x2,ptl(p1,h1,ptl1))*TMPcount(xargs+1,hidx+h1,pidx+abs_(ptl1));
    id TMPin=1;
  endrepeat;
  id TMPcount(?x) = fTMPMHVcheck(?x);
  id TMPout(?x) = TMP(?x);
endrepeat;
id TMP(?x) = AMP(?x);

** remove adjacent 3pt MHV amps for g,q,s ***
#do amp={0,1,2}
id fTMPMHVcheck(3,-1,'amp')*fTMPMHVcheck(3,-1,'amp') = 0;
id fTMPMHVcheck(3,1,'amp')*fTMPMHVcheck(3,1,'amp') = 0;
id fTMPMHVcheck(3,-1,'amp')*fTMPMHVcheck(?x1)*fTMPMHVcheck(?x2)*fTMPMHVcheck(3,-1,'amp') = 0;
id fTMPMHVcheck(3,1,'amp')*fTMPMHVcheck(?x1)*fTMPMHVcheck(?x2)*fTMPMHVcheck(3,1,'amp') = 0;
id fTMPMHVcheck(3,1,'amp')*fTMPMHVcheck(?x1)*fTMPMHVcheck(3,-1,'amp')*fTMPMHVcheck(?x2) = 0;
id fTMPMHVcheck(3,-1,'amp')*fTMPMHVcheck(?x1)*fTMPMHVcheck(3,1,'amp')*fTMPMHVcheck(?x2) = 0;
id fTMPMHVcheck(?x1)*fTMPMHVcheck(3,1,'amp')*fTMPMHVcheck(?x2)*fTMPMHVcheck(3,-1,'amp') = 0;
id fTMPMHVcheck(?x1)*fTMPMHVcheck(3,-1,'amp')*fTMPMHVcheck(?x2)*fTMPMHVcheck(3,1,'amp') = 0;
#enddo

id fTMPMHVcheck(?x) = 1;
.sort

#endprocedure
*}}}

*{{{ subtreesGU
#procedure subtreesGU

*** default choices for masses ***
*** 11/2 = t : mt
*** 11 = massive scalar : mS

argument AMP;
id ptl(p1?,H,0,?x) = ptl(p1,0,H,?x);
id ptl(p1?,h1?,0,?x) = ptl(p1,h1,0,?x);
id ptl(p1?,h1?,11/2) = ptl(p1,h1,11/2,flat(p1),ref(p1),mt);
id ptl(p1?,h1?,-11/2) = ptl(p1,h1,-11/2,flat(p1),ref(p1),mt);
endargument;

*** make reference vectors if in standard notation ***
argument AMP;
argument ptl;
#do i=1,15
id ref(P'i') = e'i';
id ref(p'i') = x'i';
id ref(p'i'h) = x'i';
id flat(P'i') = q'i';
#enddo
id ref(MOM(?x)) = ee;
id flat(MOM(?x)) = bb;
#do i=1,15
id ref(-l'i') = b'i';
id ref(l'i') = b'i';
id flat(l'i') = r'i';
id flat(-l'i') = -r'i';
#enddo
endargument;
endargument;

#do pid={5,3,1,5/2,3/2,1/2}
id AMP(?x1,ptl(p1?,h1?,-'pid',?y1),?x2) = AMP(ptl(p1,h1,-'pid',?y1),?x2,?x1);
id AMP(ptl(p1?,h1?,-'pid',?y1),?x1,ptl(p2?,h2?,-'pid',?y2)) = AMP(ptl(p2,h2,-'pid',?y2),ptl(p1,h1,-'pid',?y1),?x1);
id AMP(ptl(p1?,h1?,-'pid',?y1),?x,ptl(p2?,h2?,'pid',?y2)) = (-1)^(nargs_(?x))*AMP(ptl(p1,h1,-'pid',?y1),ptl(p2,h2,'pid',?y2),reverse_(?x));
#enddo

#define path "'treepath'"
#include- 'treepath'/includetrees.h
id NUM(px?) = px;
id I^2=-1;

#endprocedure
*}}}

*{{{ OLDsubtreesGU
#procedure OLDsubtreesGU

.sort
*** try fix multiple fermion amplitudes                   ***
*** amplitudes DO NOT sum to zero...fix in maple gen??    ***
repeat;
id,once AMP(?x) = TMPout*TMPin(?x)*TMPcount(0,0);
  repeat;
    id TMPin(ptl(p1?,h1?,ptl1?),?x1)*TMPout(?x2)*TMPcount(xargs?,pidx?) =
    TMPin(?x1)*TMPout(?x2,ptl(p1,h1,ptl1))*TMPcount(xargs+1,pidx+ptl1);
    id TMPin=1;
  endrepeat;
  if(match(TMPcount(xn?,-1/2+I/2)));
    argument TMPout;
      id ptl(l1?ls,?x,I/2) = ptl(l1,?x,1/2);
      id ptl(l1?ls,?x,-I/2) = ptl(l1,?x,1/2);
      id ptl(-l1?ls,?x,I/2) = ptl(-l1,?x,-I/2);
    endargument;
    id TMPcount(?x) = 1;
  endif;
  if(match(TMPcount(xn?,-1/2-I/2)));
    argument TMPout;
      id ptl(l1?ls,?x,-I/2) = ptl(l1,?x,1/2);
      id ptl(-l1?ls,?x,-I/2) = ptl(-l1,?x,1/2);
    endargument;
    id TMPcount(?x) = 1;
  endif;
  if(match(TMPcount(xn?,I)));
    argument TMPout;
      id ptl(l1?ls,?x,I/2) = ptl(l1,?x,-I/2);
      id ptl(-l1?ls,?x,I/2) = ptl(-l1,?x,-I/2);
      argument ptl;
       id I=1;
      endargument;
    endargument;
    id TMPcount(?x) = 1;
  endif;
  id TMPcount(?x) = 1;
  id TMPout(?x) = TMP(?x);
endrepeat;
id TMP(?x) = AMP(?x);

** if the above worked then we can chuck any amp that doesn't sum to zero... ***
** special cases: QDSqb(=3),QDbSq(=1)
repeat;
id,once AMP(?x) = TMPout*TMPin(?x)*TMPcount(0,0);
  repeat;
    id TMPin(ptl(p1?,h1?,ptl1?),?x1)*TMPout(?x2)*TMPcount(xargs?,pidx?) =
    TMPin(?x1)*TMPout(?x2,ptl(p1,h1,ptl1))*TMPcount(xargs+1,pidx+ptl1);
    id TMPin=1;
  endrepeat;
  id TMPcount(xn?,0) = 1;
  id TMPcount(xn?,3) = 1;
  id TMPcount(xn?,1) = 1;
  id TMPcount(xn?,-1) = 1;
  id TMPcount(xn?,-2) = 1;
  id TMPcount(xn?,xx?) = 0;
  id TMPout(?x) = TMP(?x);
endrepeat;
id TMP(?x) = AMP(?x);

*** now try to find single currents labelled as qq... ***
repeat;
id,once AMP(?x) = TMPout*TMPin(?x)*TMPcount(0,0);
  repeat;
    id TMPin(ptl(p1?,h1?,ptl1?),?x1)*TMPout(?x2)*TMPcount(xargs?,pidx?) =
    TMPin(?x1)*TMPout(?x2,ptl(p1,h1,ptl1))*TMPcount(xargs+1,pidx+abs_(ptl1));
    id TMPin=1;
  endrepeat;
  if(match(TMPcount(xn?,abs_(I/2)+abs_(-I/2))));
    argument TMPout;
      argument ptl;
        id I=1;
      endargument;
    endargument;
  endif;
  id TMPcount(xn?,xx?) = 1;
  id TMPout(?x) = TMP(?x);
endrepeat;
id TMP(?x) = AMP(?x);

*** this is where to add ref vectors etc.    ***
*** may want to do it before making particle ***
*** indices into labels                      ***
repeat;
id,once AMP(?x) = TMPout*TMPin(?x)*TMPcount(0,0);
  repeat;
    id TMPin(ptl(p1?,h1?,ptl1?),?x1)*TMPout(?x2)*TMPcount(xargs?,pidx?) =
    TMPin(?x1)*TMPout(?x2,ptl(p1,h1,ptl1))*TMPcount(xargs+1,pidx+abs_(h1)-1+abs_(ptl1));
    id TMPin=1;
  endrepeat;
  id TMPcount(3,3)*TMPout(?x1,ptl(p1?,h1?,0),?x2) = TMPout(?x1,ptl(p1,h1,0,ref(p1)),?x2);
  id TMPcount(3,4)*TMPout(?x1,ptl(p1?,h1?,0),?x2) = TMPout(?x1,ptl(p1,h1,0,ref(p1)),?x2);
  id TMPcount(4,2+abs_(H))*TMPout(?x1,ptl(p1?,h1?!{H,V},0),?x2) = TMPout(?x1,ptl(p1,h1,0,ref(p1)),?x2);
  id TMPcount(?x) = 1;
  id TMPout(?x) = TMP(?x);
endrepeat;
id TMP(?x) = AMP(?x);
.sort

argument AMP;
id ptl(p1?,H,0,?x) = ptl(p1,0,H,?x);
id ptl(p1?,h1?,0,?x) = ptl(p1,h1,g,?x);
id ptl(p1?,h1?,1/2) = ptl(p1,h1,q);
id ptl(p1?,h1?,I/2) = ptl(p1,h1,qq);
id ptl(p1?,h1?,-1/2) = ptl(p1,h1,qb);
id ptl(p1?,h1?,-I/2) = ptl(p1,h1,qqb);
id ptl(p1?,h1?,1) = ptl(p1,h1,sc);
id ptl(p1?,h1?,-1) = ptl(p1,h1,scb);
id ptl(p1?,h1?,3/2) = ptl(p1,h1,Q,flat(p1),ref(p1),mQ);
id ptl(p1?,h1?,-3/2) = ptl(p1,h1,Qb,flat(p1),ref(p1),mQ);
id ptl(p1?,h1?,2) = ptl(p1,h1,Sc,mS);
id ptl(p1?,h1?,-2) = ptl(p1,h1,Scb,mS);
id ptl(p1?,h1?,5/2) = ptl(p1,h1,QD,flat(p1),ref(p1),mD);
id ptl(p1?,h1?,-5/2) = ptl(p1,h1,QDb,flat(p1),ref(p1),mD);
endargument;

*** make reference vectors if in standard notation ***
argument AMP;
argument ptl;
#do i=1,10
id ref(P'i') = e'i';
id ref(p'i') = x'i';
id ref(p'i'h) = x'i';
id flat(P'i') = q'i';
#enddo
id ref(MOM(?x)) = ee;
id flat(MOM(?x)) = bb;
#do i=1,4
id ref(-l'i') = b'i';
id ref(l'i') = b'i';
id flat(l'i') = r'i';
id flat(-l'i') = -r'i';
#enddo
endargument;
endargument;

*** sort scalar "currents" ***
#do scalar={sc,Sc}
id AMP(?x1,ptl(p1?,h1?,'scalar',?y),?x2) = AMP(ptl(p1,h1,'scalar',?y),?x2,?x1);

repeat;
id,once AMP(ptl(pq?,xhq?,'scalar',?xq),ptl(pqb?,xhqb?,'scalar'b,?xqb),?xx) =
    TMPAMP(ptl(pqb,xhqb,'scalar'b,?xqb),?xx)*TMP(ptl(pq,xhq,'scalar',?xq));

repeat id TMPAMP(?xx,xptl?)*TMP(?yy) = xFlipSign*TMPAMP(?xx)*TMP(?yy,xptl);
id TMPAMP=1;
id TMP(?xxx) = AMP(?xxx);
endrepeat;
#enddo

id xFlipSign=-1;

#do fermion={q,Q,QD}
id AMP(?x1,ptl(p1?,h1?,'fermion',?y),?x2) = AMP(ptl(p1,h1,'fermion',?y),?x2,?x1);
#enddo

*** first exclude any multiple fermion amplitudes ***
id AMP(ptl(?x1,q),ptl(?x,qq),?x2) = AMP(ptl(?x,qq),?x2,ptl(?x1,q));
id AMP(ptl(?x1,q),ptl(?x,qqb),?x2) = AMP(ptl(?x,qqb),?x2,ptl(?x1,q));
id AMP(?x1,ptl(?x,qq),?x2) = TMPqqAMP(?x1,ptl(?x,qq),?x2);

*** sort fermion currents, probably better to do this in the libraries... ***
#do fermion={q,Q}
#do stupid=1,5
id,once AMP(ptl(pq?,xhq?,'fermion',?xq),ptl(pqb?,xhqb?,'fermion'b,?xqb),?xx) =
    -TMPAMP(ptl(pqb,xhqb,'fermion'b,?xqb),?xx)*TMP(ptl(pq,xhq,'fermion',?xq));

repeat id TMPAMP(?xx,xptl?)*TMP(?yy) = xFlipSign*TMPAMP(?xx)*TMP(?yy,xptl);
id TMPAMP=1;
id TMP(?xxx) = AMP(?xxx);
#enddo
#enddo

id xFlipSign=-1;
id TMPqqAMP(?x) = AMP(?x);

#call OrderedTrees

id AMP(?x) = AMP(nargs_(?x),?x);
#do k=1,10
id AMP('k',?x) = A'k'(?x);
#enddo

#define path "'treepath'"
#include- 'treepath'/includetrees.h
id NUM(px?) = px;
id I^2=-1;

#endprocedure
*}}}

*{{{ OrderedTrees
#procedure OrderedTrees

*** this is a code for unwrapping sub-leading colour ordered trees
*** of scalars and fermions, in general we must sum of the mergings of
*** the two sets of gluons. the full implementation will be added in the future

#do fl={q,sc}
id AMP(ptl(?x1,'fl'),ptl(?x2,g),ptl(?x3,'fl'b),ptl(?x4,g))
   =
   +AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),ptl(?x2,g),ptl(?x4,g))
   +AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),ptl(?x4,g),ptl(?x2,g));

id AMP(ptl(?x1,'fl'),ptl(?x2,g),ptl(?x3,'fl'b),ptl(?x4,g),ptl(?x5,g))
   =
   +AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),ptl(?x2,g),ptl(?x4,g),ptl(?x5,g))
   +AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),ptl(?x4,g),ptl(?x2,g),ptl(?x5,g))
   +AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),ptl(?x4,g),ptl(?x5,g),ptl(?x2,g));

id AMP(ptl(?x1,'fl'),ptl(?x3,'fl'b),?yy) =
  (-1)^(nargs_(?yy))*AMP(ptl(?x1,'fl'),reverse_(?yy),ptl(?x3,'fl'b));
#enddo

#endprocedure
*}}}

*{{{sortstrings
#procedure sortstrings

id spnstr(vb(p,Q4),?x,u(p,Q1)) = AA(e4,Psm(Q4,m4),?x,Psp(Q1,m1),e1)*IA(e4,q4)*IA(q1,e1);
id spnstr(vb(m,Q4),?x,u(p,Q1)) = BA(e4,Psm(Q4,m4),?x,Psp(Q1,m1),e1)*IB(e4,q4)*IA(q1,e1);
id spnstr(vb(p,Q4),?x,u(m,Q1)) = AB(e4,Psm(Q4,m4),?x,Psp(Q1,m1),e1)*IA(e4,q4)*IB(q1,e1);
id spnstr(vb(m,Q4),?x,u(m,Q1)) = BB(e4,Psm(Q4,m4),?x,Psp(Q1,m1),e1)*IB(e4,q4)*IB(q1,e1);

id spnstr(ub(p,Q1),?x,v(p,Q4)) = AA(e1,Psp(Q1,m1),?x,Psm(Q4,m1),e4)*IA(e1,q1)*IA(q4,e4);
id spnstr(ub(m,Q1),?x,v(p,Q4)) = BA(e1,Psp(Q1,m1),?x,Psm(Q4,m1),e4)*IB(e1,q1)*IA(q4,e4);
id spnstr(ub(p,Q1),?x,v(m,Q4)) = AB(e1,Psp(Q1,m1),?x,Psm(Q4,m1),e4)*IA(e1,q1)*IB(q4,e4);
id spnstr(ub(m,Q1),?x,v(m,Q4)) = BB(e1,Psp(Q1,m1),?x,Psm(Q4,m1),e4)*IB(e1,q1)*IB(q4,e4);

#call cancel
.sort

repeat;
#do fun={AA,BB,AB,BA}
#do k=1,6
id 'fun'(p1?,...,p'k'?,Psm(py?,m?),?x) = 'fun'(p1,...,p'k',py,?x)-m*'fun'(p1,...,p'k',?x);
id 'fun'(p1?,...,p'k'?,Psp(py?,m?),?x) = 'fun'(p1,...,p'k',py,?x)+m*'fun'(p1,...,p'k',?x);
#enddo
#enddo

#do fun={A,B}
#do k={1,3,5,7}
id A'fun'(q1?,...,q'k'?,ep(p2,m),?x) = AA(q1,...,q'k',p2)*B'fun'(p3,?x)*IB(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p2,p),?x) = -AA(q1,...,q'k',p3)*B'fun'(p2,?x)*IA(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p3,m),?x) = -AA(q1,...,q'k',p3)*B'fun'(p2,?x)*IB(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p3,p),?x) = AA(q1,...,q'k',p2)*B'fun'(p3,?x)*IA(p2,p3)*rt2;
#enddo
#do k={2,4,6,8}
id A'fun'(q1?,...,q'k'?,ep(p2,m),?x) = AB(q1,...,q'k',p3)*A'fun'(p2,?x)*IB(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p2,p),?x) = -AB(q1,...,q'k',p2)*A'fun'(p3,?x)*IA(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p3,m),?x) = -AB(q1,...,q'k',p2)*A'fun'(p3,?x)*IB(p2,p3)*rt2;
id A'fun'(q1?,...,q'k'?,ep(p3,p),?x) = AB(q1,...,q'k',p3)*A'fun'(p2,?x)*IA(p2,p3)*rt2;
#enddo
#enddo

#do fun={A,B}
#do k={2,4,6,8}
id B'fun'(q1?,...,q'k'?,ep(p2,m),?x) = BA(q1,...,q'k',p2)*B'fun'(p3,?x)*IB(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p2,p),?x) = -BA(q1,...,q'k',p3)*B'fun'(p2,?x)*IA(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p3,m),?x) = -BA(q1,...,q'k',p3)*B'fun'(p2,?x)*IB(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p3,p),?x) = BA(q1,...,q'k',p2)*B'fun'(p3,?x)*IA(p2,p3)*rt2;
#enddo
#do k={1,3,5,7}
id B'fun'(q1?,...,q'k'?,ep(p2,m),?x) = BB(q1,...,q'k',p3)*A'fun'(p2,?x)*IB(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p2,p),?x) = -BB(q1,...,q'k',p2)*A'fun'(p3,?x)*IA(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p3,m),?x) = -BB(q1,...,q'k',p2)*A'fun'(p3,?x)*IB(p2,p3)*rt2;
id B'fun'(q1?,...,q'k'?,ep(p3,p),?x) = BB(q1,...,q'k',p3)*A'fun'(p2,?x)*IA(p2,p3)*rt2;
#enddo
#enddo
endrepeat;

#do fun={AA,BB}
#do k={1,3,5,7}
id 'fun'(p1?,...,p'k'?) = 0;
#enddo
#enddo

#do fun={AB,BA}
#do k={2,4,6,8}
id 'fun'(p1?,...,p'k'?) = 0;
#enddo
#enddo

#call cancel

id dot(ep2(p),ep3(p)) = -B(p2,p3)*IA(p2,p3);
id dot(ep2(m),ep3(m)) = -A(p2,p3)*IB(p2,p3);
id dot(ep2(m),ep3(p)) = 0;
id dot(ep2(p),ep3(m)) = 0;

id dot(ep3(m),Q1) = AB(p3,Q1,p2)*IB(p3,p2)/rt2;
id dot(ep3(p),Q1) = AB(p2,Q1,p3)*IA(p2,p3)/rt2;
id dot(ep2(m),Q1) = AB(p2,Q1,p3)*IB(p2,p3)/rt2;
id dot(ep2(p),Q1) = AB(p3,Q1,p2)*IA(p3,p2)/rt2;

id rt2^2=2;
id 1/rt2^2=1/2;

#endprocedure
*}}}

*{{{dotsort
#procedure dotsort

id dot(px?,px?) = S(px);
id dot(px?ps,py?ps) = S(px,py)/2;
id dot(px?,py?ps) = AB(py,px,py)/2;
id dot(py?ps,px?) = AB(py,px,py)/2;
id Idot(px?,px?) = IS(px);
id Idot(px?ps,py?ps) = IS(px,py)*2;
id Idot(px?,py?ps) = IAB(py,px,py)*2;
id Idot(py?ps,px?) = IAB(py,px,py)*2;

splitarg S,IS,PP,IPP;
id S(px?ps) = 0;
id S(-px1?)=S(px1);
id S(-px1?,-px2?)=S(px1,px2);
id S(-px1?,-px2?,-px3?)=S(px1,px2,px3);
id S(-px1?,-px2?,-px3?,-px4?)=S(px1,px2,px3,px4);
id S(-px1?,-px2?,-px3?,-px4?,-px5?)=S(px1,px2,px3,px4,px5);

id IS(-px1?)=IS(px1);
id IS(-px1?,-px2?)=IS(px1,px2);
id IS(-px1?,-px2?,-px3?)=IS(px1,px2,px3);
id IS(-px1?,-px2?,-px3?,-px4?)=IS(px1,px2,px3,px4);
id IS(-px1?,-px2?,-px3?,-px4?,-px5?)=IS(px1,px2,px3,px4,px5);

id IPP(m,-px1?)=IPP(m,px1);
id IPP(m,-px1?,-px2?)=IPP(m,px1,px2);
id IPP(m,-px1?,-px2?,-px3?)=IPP(m,px1,px2,px3);
id IPP(m,-px1?,-px2?,-px3?,-px4?)=IPP(m,px1,px2,px3,px4);
id IPP(m,-px1?,-px2?,-px3?,-px4?,-px5?)=IPP(m,px1,px2,px3,px4,px5);

#endprocedure
*}}}

*{{{factornum
#procedure factornum(k)

#call map2nonas
argument num;
#call map2nonas
endargument;


id num(px?) = num(poly(1,1,1)*px);

splitarg num;
argument num;
repeat id poly(px1?,px2?,px3?)*AA?(?arg2) = poly(px1*AA(?arg2),px2*AA(?arg2),px3*AA(?arg2));
repeat id poly(px1?,px2?,px3?)*py? = poly(px1*py,px2*py,px3*py);
argument poly;
#call map2as
endargument;
endargument;

.sort

*#do xx={2,3,4,5,6,7,8,1/2,1/3,1/4,1/5,1/6,1/8,-1,-2,-3,-4,-5,-6,-7,-8,-1/2,-1/3,-1/4,2/3,-2/3,-1/8,4/3,5/2,-4/3,-5/2,2/9,-2/9,3/2,-3/2,5/4,-5/4,3/4,-3/4,16,-16,4/5,2/5,7/5,9/5,-4/5,-2/5,-7/5,-9/5}

#do i=1,2
#do xx=1,25
  id num(?x,'xx'*poly(px1?,px2?,px3?),?y) = num(?x,poly('xx'*px1,'xx'*px2,'xx'*px3),?y);
  id num(?x,-'xx'*poly(px1?,px2?,px3?),?y) = num(?x,poly(-'xx'*px1,-'xx'*px2,-'xx'*px3),?y);
  #do yy=2,20
   id num(?x,'xx'/'yy'*poly(px1?,px2?,px3?),?y) = num(?x,poly('xx'/'yy'*px1,'xx'/'yy'*px2,'xx'/'yy'*px3),?y);
   id num(?x,-'xx'/'yy'*poly(px1?,px2?,px3?),?y) = num(?x,poly(-'xx'/'yy'*px1,-'xx'/'yy'*px2,-'xx'/'yy'*px3),?y);
   id num(?x,'xx'/'yy'*poly(S(p2,p3),S(p2,p3),S(p2,p3)),?y) = num(?x,poly('xx'/'yy'*S(p2,p3),'xx'/'yy'*S(p2,p3),'xx'/'yy'*S(p2,p3)),?y);
  #enddo
#enddo
#enddo

*#write "inside factorise"
*print+s test;
*.end


argument num;
argument poly,1;
	id IA(px1?,px2?) = 1;
	id IB(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id I'fun'(?x) = 1;
	#enddo
	id 1/m?=1;
endargument;

argument poly,2;
	id A(px1?,px2?) = 1;
	id B(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id 'fun'(?x) = 1;
	#enddo
	id IA(px1?,px2?) = A(px1,px2);
	id IB(px1?,px2?) = B(px1,px2);
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id I'fun'(?x) = 'fun'(?x);
	#enddo
	id m?=1;
endargument;

argument poly,3;
	id A(px1?,px2?) = 1;
	id B(px1?,px2?) = 1;
	id IA(px1?,px2?) = 1;
	id IB(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id 'fun'(?x) = 1;
		id I'fun'(?x) = 1;
	#enddo
	id m?=1;
endargument;

id poly(px1?,px2?,xx?) = poly(px1,px2/xx);

argument poly;
#call map2nonas
endargument;

#do fun1={AB,AA,BB,S,PP,naA,naB}
#do fun2={AB,AA,BB,S,PP,naA,naB}
id poly(px1?,'fun1'(?y1)*'fun2'(?y2))=poly(px1,'fun1'(?y1),'fun2'(?y2));
#enddo
#enddo

argument poly;
#call map2as
endargument;


endargument;

#do i=1,'k'
id num(?x1,poly(px1?,?y1,px2?,?y2),?x2,poly(px3?,?y3,px2?,?y4),?x3) = num(?x1,poly(px1,?y1,px2,?y2),?x2,poly(px3*inv(px2),?y3,1,?y4),?x3);
argument num;
argument poly,1;
#call sortnuminv;
endargument;
endargument;

id,once num(?x1,poly(px1?,px2?$tmp,?y),?x2) = inv(px2)*numtmp(?x1,polytmp(px1,?y),?x2);
argument numtmp;
argument poly,1;
multiply $tmp;
endargument;

id polytmp(?x)=poly(?x);
endargument;
id numtmp(?x) = num(?x);
$tmp=1;

#enddo

argument num;
id poly(px?) = px;
#call cancel
endargument;

repeat;
id num(px1?,px2?,?x) = num(px1+px2,?x);
endrepeat;

#call sortnuminv

#endprocedure
*}}}

*{{{factorinv
#procedure factorinv(k)

#call map2nonas
argument inv;
#call map2nonas
endargument;

id inv(px?) = inv(poly(1,1,1)*px);

splitarg inv;
argument inv;
repeat id poly(px1?,px2?,px3?)*AA?(?arg2) = poly(px1*AA(?arg2),px2*AA(?arg2),px3*AA(?arg2));
repeat id poly(px1?,px2?,px3?)*py? = poly(px1*py,px2*py,px3*py);
argument poly;
#call map2as
endargument;
endargument;

#do xx={2,3,4,5,6,1/2,1/3,1/4,1/5,1/6,-1,-2,-3,-4,-1/2,-1/3,-1/4}
repeat id inv(?x,'xx'*poly(px1?,px2?,px3?),?y) = inv(?x,poly('xx'*px1,'xx'*px2,'xx'*px3),?y);
#enddo

argument inv;
argument poly,1;
	id IA(px1?,px2?) = 1;
	id IB(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id I'fun'(?x) = 1;
	#enddo
	id 1/m?=1;
endargument;

argument poly,2;
	id A(px1?,px2?) = 1;
	id B(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id 'fun'(?x) = 1;
	#enddo
	id IA(px1?,px2?) = A(px1,px2);
	id IB(px1?,px2?) = B(px1,px2);
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id I'fun'(?x) = 'fun'(?x);
	#enddo
	id m?=1;
endargument;

argument poly,3;
	id A(px1?,px2?) = 1;
	id B(px1?,px2?) = 1;
	id IA(px1?,px2?) = 1;
	id IB(px1?,px2?) = 1;
	#do fun={AA,BB,AB,BA,dot,S,PP}
		id 'fun'(?x) = 1;
		id I'fun'(?x) = 1;
	#enddo
	id m?=1;
endargument;

id poly(px1?,px2?,xx?) = poly(px1,px2/xx);

endargument;

#do i=1,'k'
id,once inv(?x1,poly(px1?,px2?$tmp),?x2) = num(px2)*invtmp(?x1,polytmp(px1),?x2);
argument invtmp;
argument poly,1;
multiply $tmp;
endargument;
id polytmp(px?)=poly(px);
endargument;
id invtmp(?x)=inv(?x);
$tmp=1;
#enddo

argument inv;
id poly(px?) = px;
endargument;

repeat;
id inv(px1?,px2?,?x) = inv(px1+px2,?x);
endrepeat;

#call sortnuminv

#endprocedure
*}}}

