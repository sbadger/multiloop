id INTdb320(k1, k2, mu11, mu12, mu22, p1, p2, p3, p4, p5) =
  -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
      1/TR5*s12*s23*s45*(s15*s34*s45)
    - ( s12*s23*s45 + 1/TR5*s12*s23*s45*(s12*(s23-s15)-s23*s34+(s15+s34)*s45) )*dot(k1,p5)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));

id INTdb230(k1, k2, mu11, mu12, mu22, p1, p2, p3, p4, p5) =
  -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
      1/TR5*s45*s34*s12*(s15*s23*s12)
    - ( s45*s34*s12 + 1/TR5*s45*s34*s12*(s45*(s34-s15)-s34*s23+(s15+s23)*s12) )*dot(k2,p1)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));


id mu33 = mu11+mu12+mu22;

