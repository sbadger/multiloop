# vim: set foldmethod=marker:
interface(quiet=true):
interface(displayprecision=10):
interface(rtablesize=infinity):

mypow:=proc(a,b); return a^b; end:

Digits:=50:
with(spinors):

genmoms([0,0,0,0,0]):

printf("p1(%.16f,%.16f,%.16f,%.16f)\n",p1[1],p1[2],p1[3],p1[4]);
printf("p2(%.16f,%.16f,%.16f,%.16f)\n",p2[1],p2[2],p2[3],p2[4]);
printf("p3(%.16f,%.16f,%.16f,%.16f)\n",p3[1],p3[2],p3[3],p3[4]);
printf("p4(%.16f,%.16f,%.16f,%.16f)\n",p4[1],p4[2],p4[3],p4[4]);
printf("p5(%.16f,%.16f,%.16f,%.16f)\n",p5[1],p5[2],p5[3],p5[4]);

#p1:=<1.14003888976994408,-0.24753366981115985,0.62401889540947939,-0.92142073488154130>:
#p2:=<0.79072516177906147,-0.170227666833905588,-0.74654013133688475,0.197349069464020131>:
#p3:=<1.09342151427863267,0.61978524371253966,-0.75676920469256491,0.48860743996847684>:
#p4:=<-1.25895978588413421,-0.68753861593695004,-0.73637823485741372,0.75499502600825643>:
#p5:=<-1.76522577994350401,0.48551470886947581,1.61566867547738400,-0.51953080055921210>:

s12:=S(p1,p2):
s23:=S(p2,p3):
s123:=S(p1,p2,p3):
s125:=S(p1,p2,p5):
s15:=S(p1,p5):

delta:=proc(p1,p2,p3,p4,p5,a,b,c) local s12,s23,s34,s45,s51;
s12:=S(p1,p2);
s23:=S(p2,p3);
s34:=S(p3,p4);
s45:=S(p4,p5);
s51:=S(p5,p1);
return s12*s51+a*s12*s23+b*s23*s34-s51*s45+c*s34*s45;
end:

G4:=proc(p1,p2,p3,p4) local s12,s13,s14,s23,s24,s34;
s12:=S(p1,p2):
s13:=S(p1,p3):
s14:=S(p1,p4):
s23:=S(p2,p3):
s24:=S(p2,p4):
s34:=S(p3,p4):
return s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2;
end:

R:=proc(p1,p2,p3,p4,p5) local e1234;
global mhv;
e1234:=(-AB(p1,p2,p3,p4,p1)+AB(p1,p4,p3,p2,p1));
return mhv*e1234*S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1)/G4(p1,p2,p3,p4);
end:

Nf:=0:Ns:=0:
#Hel:=6: HelStr[6]:="-++--": mhv:=-1:
#Hel:=10: HelStr[10]:="-+-+-": mhv:=-1:
#Hel:=26: HelStr[26]:="-+-++": mhv:=1:
#Hel:=28: HelStr[28]:="--+++": mhv:=1:
#Hel:=22: HelStr[22]:="-++-+": mhv:=1:
Hel:=31: HelStr[31]:="+++++": mhv:=1:

printf("Two-loop 5-pt Nf=%d, Ns=%d integrand coefficients. Hel = %s (%d)\n",Nf,Ns,HelStr[Hel],Hel);

tree[6]:= B(p2,p3)^4*IB(p1,p2)*IB(p2,p3)*IB(p3,p4)*IB(p4,p5)*IB(p5,p1):
tree[10]:= B(p2,p4)^4*IB(p1,p2)*IB(p2,p3)*IB(p3,p4)*IB(p4,p5)*IB(p5,p1):
tree[26]:= A(p1,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):
tree[28]:= A(p1,p2)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):
tree[22]:= A(p1,p4)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):
tree[31]:= 0:

printf("tree = %.16Ze\n",tree[Hel]);

BDKa:=proc(p1,p2,p3,p4,p5);
return ( S(p1,p2)^2*S(p2,p3) - R(p1,p2,p3,p4,p5)*delta(p1,p2,p3,p4,p5,-1,1,-1)*S(p1,p2)*IS(p3,p4)*IS(p4,p5)*IS(p5,p1) )/2;
end:
BDKc:=proc(p1,p2,p3,p4,p5);
return ( S(p1,p2)*S(p3,p4)*S(p4,p5) + R(p1,p2,p3,p4,p5)*delta(p1,p2,p3,p4,p5,1,-1,1)*IS(p2,p3)*IS(p5,p1) )/2;
end:
BDKd:=proc(p1,p2,p3,p4,p5);
return 2*R(p1,p2,p3,p4,p5)/2;
end:
BDKe:=proc(p1,p2,p3,p4,p5);
return -2*S(p1,p2)*R(p1,p2,p3,p4,p5)/2;
end:

read("C8.boxpent1.INT.log"):
read("C8.boxpent2.INT.log"):
read("C7.dbox1.INT.log"):
read("C7.dbox4.INT.log"):
read("pentagonbox.integrand.mpl"):

OnShell_pentagonbox:=proc(p1,p2,p3,p4,p5,sol)
local k1,k2,bb,basis1,basis2,cfs1,cfs2;
global l1,l2,l3,l4,l5,l6,l7,l8;

basis1:=[p1,p2,eta(p1,p2)/2,eta(p2,p1)/2]:
cfs1:=[
 [1,0,A(p2,p3)*IA(p1,p3),0],
 [1,0,0,B(p2,p3)*IB(p1,p3)],
 [1,0,0,B(p2,p3)*IB(p1,p3)],
 [1,0,A(p2,p3)*IA(p1,p3),0]
];
basis2:=[p4,p5,eta(p4,p5)/2,eta(p5,p4)/2]:
cfs2:=[
 [0,1,-A(p5,p1)*IA(p4,p1),0],
 [0,1,0,-B(p5,p1)*IB(p4,p1)],
 [0,1,-B(p4,p3)*IB(p3,p5),0],
 [0,1,0,-A(p4,p3)*IA(p3,p5)]
];

k1:=Vector(4);
k2:=Vector(4);
for bb from 1 to 4 do:
k1:=k1+basis1[bb]*cfs1[sol,bb];
k2:=k2+basis2[bb]*cfs2[sol,bb];
od:

l1:=k1;
l2:=k1-p1;
l3:=k1-p1-p2;
l4:=k1-p1-p2-p3;
l5:=-k2+p4+p5;
l6:=-k2+p5;
l7:=-k2;
l8:=-k1-k2;

return;

end:

Dvec:=Vector(4):
for sol from 1 to 4 do:
  OnShell_pentagonbox(p1,p2,p3,p4,p5,sol);
  Integrand:=evalf(
    +C8_boxpent2_INT[Hel,0,0]
    +C8_boxpent2_INT[Hel,1/2,1/2]
    +C8_boxpent2_INT[Hel,1/2,0]
    +C8_boxpent2_INT[Hel,0,1/2]
    +C8_boxpent2_INT[Hel,1,1]
    +C8_boxpent2_INT[Hel,0,1]
    +C8_boxpent2_INT[Hel,1,0]
    +C8_boxpent2_INT[Hel,1/2,-1/2]
    +C8_boxpent2_INT[Hel,1/2,1]
    +C8_boxpent2_INT[Hel,1,1/2]
  );
  Dvec[sol]:=Integrand;
  print(Dvec[sol]);
#  Dvec[sol] := 1 + 2*2*dot(-l7,p1) + 3*2*dot(-l7,p2) + 4*2*dot(l1,p5);
od:

printf("pentagon|box 123x45x\n");
printf("polynomial coefficients from trees:\n");
for i from 1 to 4 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%.10Ze\n",Dvec[i]);
#print(expand(Dvec[i]));
od:

Cvec_123x45x:=invert_pentagonbox(p1,p2,p3,p4,p5,Dvec):

printf("integrand coefficients:\n");
for i from 1 to 4 do:
if(abs(Cvec_123x45x[i])<1e-20)then:Cvec_123x45x[i]:=0:fi:
printf("%.10Ze\n",Cvec_123x45x[i]);
#print(expand(Cvec_123x45x[i]));
od:

DEN:=s23-s15-s123:
NUM1:=s123*(s15-s123)+s23*s125+s12*(s15-2*s23+2*s123):
NUM2:=s12-s23+s15-s125:

#printf("MI coefficients:\n");
#print("I8[1]     = ",expand(Cvec_123x45x[1]+xx1*NUM1/DEN*Cvec_123x45x[3]));
#print("I8[k2.p1] = ",expand(Cvec_123x45x[2]+xx2*NUM2/DEN*Cvec_123x45x[3]));
#print("I8[k1.p5] = ",expand(Cvec_123x45x[4]));

printf("check with hep-th/0604074\n");
printf("%.16Ze\n",I*BDKe(p5,p4,p3,p2,p1)*tree[Hel]);
printf("%.16Ze\n",I*BDKc(p5,p4,p3,p2,p1)*tree[Hel]);

Dvec:=Vector(4):
for sol from 1 to 4 do:
  OnShell_pentagonbox(p5,p4,p3,p2,p1,sol);
  nl1:=-l7:
  nl2:=-l6:
  nl3:=-l5:
  nl4:=-l4:
  nl5:=-l3:
  nl6:=-l2:
  nl7:=-l1:
  l1:=nl1:
  l2:=nl2:
  l3:=nl3:
  l4:=nl4:
  l5:=nl5:
  l6:=nl6:
  l7:=nl7:
  Integrand:=evalf(
    +C8_boxpent1_INT[Hel,0,0]
    +C8_boxpent1_INT[Hel,1/2,1/2]
    +C8_boxpent1_INT[Hel,1/2,0]
    +C8_boxpent1_INT[Hel,0,1/2]
    +C8_boxpent1_INT[Hel,1,1]
    +C8_boxpent1_INT[Hel,0,1]
    +C8_boxpent1_INT[Hel,1,0]
    +C8_boxpent1_INT[Hel,1/2,-1/2]
    +C8_boxpent1_INT[Hel,1/2,1]
    +C8_boxpent1_INT[Hel,1,1/2]
  );
  Dvec[sol]:=Integrand;
od:

printf("pentagon|box 12x345x\n");
printf("polynomial coefficients from trees:\n");
for i from 1 to 4 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%.10Ze\n",Dvec[i]);
#print(expand(Dvec[i]));
od:

Cvec_12x345x:=invert_pentagonbox(p5,p4,p3,p2,p1,Dvec):

printf("integrand coefficients:\n");
for i from 1 to 4 do:
if(abs(Cvec_12x345x[i])<1e-20)then:Cvec_12x345x[i]:=0:fi:
printf("%.10Ze\n",Cvec_12x345x[i]);
#print(expand(Cvec_12x345x[i]));
od:

printf("check with hep-th/0604074\n");
printf("%.16Ze\n",I*BDKe(p1,p2,p3,p4,p5)*tree[Hel]);
printf("%.16Ze\n",I*BDKc(p1,p2,p3,p4,p5)*tree[Hel]);

# formulae from Cachazo's paper

#caB5  := 1/(1+A(p2,p3)*B(p2,p5)*IA(p1,p3)*IB(p1,p5)):
#caB5t := 1/(1+B(p2,p3)*A(p2,p5)*IB(p1,p3)*IA(p1,p5)):
#caGa  := 1/(1+A(p3,p4)*B(p4,p1)*IA(p3,p5)*IB(p5,p1)):
#caGat := 1/(1+B(p3,p4)*A(p4,p1)*IB(p3,p5)*IA(p5,p1)):

#caE := S(p1,p2)*S(p1,p2)*S(p2,p3)*S(p5,p1)/(caB5-caB5t)*tree[Hel];
#caC := -S(p1,p2)*S(p3,p4)*S(p4,p5)*caGat/(caGa-caGat)*tree[Hel];

Integrand_pentagonbox:=proc(k1,k2,p1,p2,p5,Cvec);
return Cvec[1]+2*dot(k2,p1)*Cvec[2]+2*dot(k2,p2)*Cvec[3]+2*dot(k1,p5)*Cvec[4]
end:

OnShell_boxboxMK4:=proc(p1,p2,p3,P4,tau,sol)
local k1,k2,p4f,bb,basis1,basis2,cfs1,cfs2,XX,YY;
global l1,l2,l3,l4,l5,l6,l7;

XX:=S(P4)/2/dot(P4,p3):
p4f:=P4-XX*p3:
YY:=XX+1:

basis1:=[p1,p2,eta(p1,p2)/2,eta(p2,p1)/2]:
cfs1:=[
 [1,0,A(p2,p3)*IA(p1,p3),0],
 [1,0,0,B(p2,p3)*IB(p1,p3)],
 [1,0,tau,0],
 [1,0,0,tau],
 [1,0,(YY*A(p2,p3)+A(p2,p4f)*tau)/(YY*A(p1,p3)+A(p1,p4f)*tau),0],
 [1,0,0,(YY*B(p2,p3)+B(p2,p4f)*tau)/(YY*B(p1,p3)+B(p1,p4f)*tau)]
];

basis2:=[p3,p4f,eta(p3,p4f)/2,eta(p4f,p3)/2]:
cfs2:=[
 [0,1,-tau,0],
 [0,1,0,-tau],
 [0,1,-YY*B(p3,p2)*IB(p2,p4f),0],
 [0,1,0,-YY*A(p3,p2)*IA(p2,p4f)],
 [0,1,0,-tau],
 [0,1,-tau,0]
];

k1:=Vector(4);
k2:=Vector(4);
for bb from 1 to 4 do:
k1:=k1+basis1[bb]*cfs1[sol,bb];
k2:=k2+basis2[bb]*cfs2[sol,bb];
od:

l1:=k1;
l2:=k1-p1;
l3:=k1-p1-p2;
l4:=-k2+p3+P4;
l5:=-k2+P4;
l6:=-k2;
l7:=-k1-k2;

return;

end:

Dvec:=Vector(54):
for sol from 1 to 6 do:
  for tk from -4 to 4 do:
  tau:=evalf(exp(2*Pi*I*tk/9)):
  OnShell_boxboxMK4(p1,p2,p3,p4+p5,tau,sol);
  Integrand:=evalf(
    +C7_dbox1_INT[Hel,0,0]
    +C7_dbox1_INT[Hel,1/2,1/2]
    +C7_dbox1_INT[Hel,1/2,0]
    +C7_dbox1_INT[Hel,0,1/2]
    +C7_dbox1_INT[Hel,1,1]
    +C7_dbox1_INT[Hel,0,1]
    +C7_dbox1_INT[Hel,1,0]
    +C7_dbox1_INT[Hel,1/2,-1/2]
    +C7_dbox1_INT[Hel,1/2,1]
    +C7_dbox1_INT[Hel,1,1/2]
    -I*Integrand_pentagonbox(-l6,l1,p4,p5,p1,Cvec_12x345x)*IS(l6+p5)
  );
  for kk from 1 to 9 do:
    Dvec[kk-9+sol*9]:=Dvec[kk-9+sol*9]+Integrand/9/(tau^(kk-5));
  od:
od:
od:

printf("box|box 12x34x\n");
printf("polynomial coefficients from trees:\n");
for i from 1 to 54 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%.10Ze\n",Dvec[i]);
#print(i,expand(Dvec[i]));
od:

printf("check with hep-th/0604074\n");
printf("%.16Ze\n",-BDKa(p1,p2,p3,p4,p5)*tree[Hel]);

OnShell_boxbox5leg:=proc(p1,p2,p3,p4,p5,tau,sol)
local k1,k2,bb,basis1,basis2,cfs1,cfs2;
global l1,l2,l3,l4,l5,l6,l7;

basis1:=[p1,p2,eta(p1,p2)/2,eta(p2,p1)/2]:
cfs1:=[
 [1,0,A(p2,p3)*IA(p1,p3),0],
 [1,0,0,B(p2,p3)*IB(p1,p3)],
 [1,0,tau,0],
 [1,0,0,tau],
 [1,0,(A(p2,p3)+A(p2,p4)*tau)/(A(p1,p3)+A(p1,p4)*tau),0],
 [1,0,0,(B(p2,p3)+B(p2,p4)*tau)/(B(p1,p3)+B(p1,p4)*tau)]
];

basis2:=[p3,p4,eta(p3,p4)/2,eta(p4,p3)/2]:
cfs2:=[
 [0,1,-tau,0],
 [0,1,0,-tau],
 [0,1,-B(p3,p2)*IB(p2,p4),0],
 [0,1,0,-A(p3,p2)*IA(p2,p4)],
 [0,1,0,-tau],
 [0,1,-tau,0]
];

k1:=Vector(4);
k2:=Vector(4);
for bb from 1 to 4 do:
k1:=k1+basis1[bb]*cfs1[sol,bb];
k2:=k2+basis2[bb]*cfs2[sol,bb];
od:

l1:=k1;
l2:=k1-p1;
l3:=k1-p1-p2;
l4:=-k2+p3+p4;
l5:=-k2+p4;
l6:=-k2;
l7:=-k1-k2-p5;

return;

end:

Dvec:=Vector(54):
for sol from 1 to 6 do:
  for tk from 0 to 8 do:
  tau:=evalf(exp(2*Pi*I*tk/9+0*1e-2*I)):
  OnShell_boxbox5leg(p4,p5,p1,p2,p3,tau,sol);
  nl1:=l4:
  nl2:=l5:
  nl3:=l6:
  nl4:=l1:
  nl5:=l2:
  nl6:=l3:
  l1:=nl1:
  l2:=nl2:
  l3:=nl3:
  l4:=nl4:
  l5:=nl5:
  l6:=nl6:
  Integrand:=evalf(
    +C7_dbox4_INT[Hel,0,0]
    -C7_dbox4_INT[Hel,1/2,1/2]
    -C7_dbox4_INT[Hel,1/2,0]
    -C7_dbox4_INT[Hel,0,1/2]
    +C7_dbox4_INT[Hel,1,1]
    +C7_dbox4_INT[Hel,0,1]
    +C7_dbox4_INT[Hel,1,0]
    +C7_dbox4_INT[Hel,1/2,-1/2]
    +C7_dbox4_INT[Hel,1/2,1]
    +C7_dbox4_INT[Hel,1,1/2]
    -I*Integrand_pentagonbox(-l6,l1,p4,p5,p1,Cvec_12x345x)*IS(l4+p3)
    -I*Integrand_pentagonbox(l1,-l6,p1,p2,p5,Cvec_123x45x)*IS(l3-p3)
  );
  for kk from 1 to 9 do:
    Dvec[kk-9+sol*9]:=Dvec[kk-9+sol*9]+Integrand/9/(tau^(kk-5));
  od:
od:
od:

printf("box|box 12[3,x]45x\n");
printf("polynomial coefficients from trees:\n");
for i from 1 to 54 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%.10Ze\n",Dvec[i]);
od:

printf("check with hep-th/0604074\n");
printf("%.16Ze\n",-BDKd(p1,p2,p3,p4,p5)*tree[Hel]);

