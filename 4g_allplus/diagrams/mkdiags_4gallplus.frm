* vim: set foldmethod=marker:
#-
#include ../2to2tools.prc
#include diagrams.prc

auto CF INTdb, INTbt;
S Dsm2;

off stats;
.global

#procedure signs

*** diagrams for box-box: db220
id xxdiag1  =  1;
id xxdiag2  = -2;
id xxdiag3  = -2;
id xxdiag4  = -2;
*** diagrams for triangle-triangle butterfly : bt22
id xxdiag5  =  1;
id xxdiag6  =  2;
id xxdiag7  =  2;
id xxdiag8  =  4;
id xxdiag9  = -1;
id xxdiag10 = -1/2;
*** diagrams for box-triangle: db210
id xxdiag11 =  1;
id xxdiag12 = -2;
id xxdiag13 =  2;
id xxdiag14 =  2;
id xxdiag15 = -1;
id xxdiag16 =  2;
*** diagrams for triangle-bubble butterfly : bt21

id xxsub = 1;

#endprocedure

#define rerun "0"
#define dd "0"

#if 'rerun'=1

*** diagrams for box-box: db220
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

*}}}
*** diagrams for triangle-triangle butterfly : bt22
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4,'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4,'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;
*}}}
*** diagrams for box-triangle: db210
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(p4, mu4, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(p4, mu4, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;

*}}}
*** diagrams for triangle-bubble butterfly : bt21
*{{{

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(p4, mu4, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, rho7, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(p4, mu4, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(p12, nu7, 0), P(-qq34, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho7, nu7, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(qq, nu5, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(qq, nu5, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p34, nu6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p3, mu3, 0), P(p4, mu4, 0), P(-p34, rho6, 0))*Prop(rho6, nu6, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(qq, nu5, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(p4, mu4, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')
;

*}}}

#call expandtopo
multiply EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p1,mu4);
#call subvertices
#call cancel
.sort

argument AB,dot,S,IS;
id kk = k1;
id kk1 = MOM(k1,-p1);
id kk12 = MOM(k1,-p1,-p2);
id p12 = MOM(p1,p2);
id p34 = MOM(-p1,-p2);
id qq = k2;
id qq4 = MOM(k2,-p4);
id qq34 = MOM(k2,-p3,-p4);
id kq = MOM(k1,k2);
endargument;

*{{{ expand and simplify ids

id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
repeat id AB(p1?,MOM(k1?{k1,k2},?xx),p3?) = AB(p1,k1,p3)+AB(p1,MOM(?xx),p3);

argument AB;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
id AB(p1?,-MOM(?x1),p2?) = -AB(p1,MOM(?x1),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
id AB(p1?,MOM,p3?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

repeat id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
repeat id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
#call cancel

repeat id dot(MOM(p1?),xx?) = dot(p1,xx);

id dot(MOM(k1?{k1,k2},?x1),MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2)) + dot(k2,MOM(?x1)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),MOM(?x2)) = dot(k1,MOM(?x2)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),p1?) = dot(k1,p1) + dot(MOM(?x1),p1);
id dot(k1?{k1,k2},MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2));

argument dot;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
id dot(p1?ps,MOM(p2?,?x)) = dot(p1,p2) + dot(p1,MOM(?x));
id dot(p1?ps,MOM(?x1,p1?ps,?x2)) = dot(p1,MOM(?x1,?x2));
id dot(p1?ps,MOM(p1?ps,?x2)) = dot(p1,MOM(?x2));
id dot(p1?ps,MOM(p1?ps,p2?)) = dot(p1,p2);
id dot(MOM,x?) = 0;
argument dot;
repeat id MOM(p1?,?x) = p1 + MOM(?x);
id MOM = 0;
endargument;
id dot(p1?ps,p1?ps) = 0;
id dot(p?,p?) = S(p);
id IS(MOM(?x)) = IS(?x);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;

id dot(p?,p?) = S(p);
splitarg S,IS;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
.sort

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);
*
id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);
*
id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);
*
id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

id AB(px1?,MOM(px2?,px3?,px4?),px5?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

#call cancel
id S(k1?{k1,k2}) = dot(k1,k1);
#do i=1,4
#do j=1,4
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id dot(p1?ps,p2?) = dot(p1,MOM(p2));
argument dot;
splitarg MOM;
endargument;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
#do i=1,4
#do j=1,4
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

#call simplify4

*}}}

.sort

#do i=1,'dd'
  #write <tmp/diagrams.prc> "G diag'i' = %e" diag'i'
#enddo

#else
#include- tmp/diagrams.prc
#endif

B MOM,V;
print[];
.store

L cutAMPdb220 =
#do i=1,4
+xxdiag'i'*diag'i'
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3+p4) = -s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p3+p4) = s12/2;
id dot(k2,p3) = s12/2;
id dot(k2,p4) = 0;

topolynomial;
.sort
format maple;
#write <db220.input.mpl> "INT_db220 := proc(k1,k2,mu11,mu12,mu22)"
#write <db220.input.mpl> "local Z;"
#write <db220.input.mpl> "global p1,p2,p3,p4;"
#write <db220.input.mpl> "%X"
#write <db220.input.mpl> "return %e" cutAMPdb220
#write <db220.input.mpl> "end:"
.store

L cutAMPbt22 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=5,10
+xxdiag'i'*diag'i'
#enddo
-xxsub*INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)*IS(kq)
;
*#call signs
id INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) =
    ((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 4*(mu12^2-4*mu11*mu22))*
    (-I*s12^2*s14*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1));

id mu33 = mu11+mu22+mu12;

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3+p4) = -s12/2;
id dot(k2,p3+p4) = s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p3) = s12/2;
id dot(k2,p4) = 0;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);

topolynomial;
.sort
format maple;
#write <bt22.input.mpl> "INT_bt22 := proc(k1,k2,mu11,mu12,mu22)"
#write <bt22.input.mpl> "local Z;"
#write <bt22.input.mpl> "global p1,p2,p3,p4;"
#write <bt22.input.mpl> "%X"
#write <bt22.input.mpl> "return %e" cutAMPbt22
#write <bt22.input.mpl> "end:"
.store

L cutAMPdb210 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(qq4)
#enddo
#do i=11,16
+xxdiag'i'*diag'i'
#enddo
-xxsub*INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)*IS(qq4)
;
#call signs
id INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) =
    ((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 4*(mu12^2-4*mu11*mu22))*
    (-I*s12^2*s14*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1));
id mu33 = mu11+mu22+mu12;

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3+p4) = -s12/2;
id dot(k2,p3+p4) = s12/2;
id dot(k2,p1+p2) = -s12/2;

id IS(qq4) = INV(-2*dot(k2,p4));

topolynomial;
.sort
format maple;
#write <db210.input.mpl> "INT_db210 := proc(k1,k2,mu11,mu12,mu22)"
#write <db210.input.mpl> "local Z;"
#write <db210.input.mpl> "global p1,p2,p3,p4;"
#write <db210.input.mpl> "%X"
#write <db210.input.mpl> "return %e" cutAMPdb210
#write <db210.input.mpl> "end:"
.store

L cutAMPbt21 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kq)*IS(qq4)
#enddo
#do i=5,10
+xxdiag'i'*diag'i'*IS(qq4)
#enddo
#do i=11,16
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=17,25
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)*IS(kq)*IS(qq4)
-xxsub2*INTdb210(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)*IS(kq)
-xxsub3*INTbt22(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)*IS(qq4)
;
#call signs

id INTdb220(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) =
    ((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 4*(mu12^2-4*mu11*mu22))*
    (-I*s12^2*s14*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1));
id INTdb210(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) = 0;

*** option 1 ***
*id INTbt22(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) = I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1)*(
*      - 2*(Ds-2)*(mu11+mu22)*mu12*s12*s14
*      - (Ds-2)^2*(2*dot(k1,k2)-mu12+s12)*s14*mu11*mu22
*);

*** option 2 : from matrix fit ***
id INTbt22(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4) = I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1)*(

      - 2*Dsm2*(mu11+mu22)*mu12*s12*s14

      + Dsm2^2*mu11*mu22 * (
          + mu12*s14
          + dot(p1,k2)*s14*s12*s13^(-1)
          + 2*dot(p1,k2)*dot(p4,k1)*NUM(s12 + 2*s14)*s13^(-1)
          + dot(p4,k1)*s14*s12*s13^(-1)
          - 2*dot(k1,omega4)*dot(k2,omega4)*s12*s13^(-1)
          + 1/2*NUM(2*s12 + s14)*s14*s12*s13^(-1)
      )
);

id dot(k1?,omega4) = (AB(p2,p3,p1)*AB(p1,k1,p2)-AB(p1,p3,p2)*AB(p2,k1,p1))/2/s12;

id mu33 = mu11+mu22+mu12;

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p3+p4) = s12/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id IS(qq4) = INV(-2*dot(k2,p4));

topolynomial;
.sort
format maple;
#write <bt21.input.mpl> "INT_bt21 := proc(k1,k2,mu11,mu12,mu22)"
#write <bt21.input.mpl> "local Z;"
#write <bt21.input.mpl> "global p1,p2,p3,p4;"
#write <bt21.input.mpl> "%X"
#write <bt21.input.mpl> "return %e" cutAMPbt21
#write <bt21.input.mpl> "end:"
.end

