(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];

(* Set Ds = 4 for FDH scheme *)
Dsm2 = 2;

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];


invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> s12/2, p[1].p[3] -> (s45-s12-s23)/2, p[1].p[4] -> (s23-s15-s45)/2, p[1].p[5] -> s15/2,
      p[2].p[3] -> s23/2, p[2].p[4] -> (s15-s23-s34)/2, p[2].p[5] -> (s34-s12-s15)/2,
      p[3].p[4] -> s34/2, p[3].p[5] -> (s12-s34-s45)/2,
      p[4].p[5] -> s45/2};

kids = {w132.p[1]->0, w132.p[2]->0, w132.p[3]->0, w132.p[4]->tr5/s13/2, w132.p[5]->-tr5/s13/2, w132.w132->s12*s23/s13};

kinpt = {{s12 -> 20.2290755631707604, s23 -> 42.0000000000000000, s34 -> -24.7243908366266393, s45 -> 66.4581511263415208, s15 -> -7.4487816732532786, tr5 -> -444.0227329401300486*I},
{s12 -> 5.8114718025114551, s23 -> 111.1780435632823470, s34 -> -119.0194598298895731, s45 -> 254.9541143630703645, s15 -> -53.8854746804948466, tr5 -> 2979.0073842318299273*I},
{s12 -> 5.9571362953672327, s23 -> 40.1514282934543239, s34 -> -36.2730540885621669, s45 -> 74.8503905319947464, s15 -> -24.9449619371828435, tr5 -> 538.3217054317520188*I},
{s12 -> 112.2435728409058258, s23 -> 66.3747561767499787, s34 -> -30.3020511466356522, s45 -> 210.9066573298103079, s15 -> -44.7777082212000479, tr5 -> -5928.2692960291381970*I},
{s12 -> 10.9986072396381874, s23 -> 28.2607463973121173, s34 -> -42.2313469357018403, s45 -> 74.7516402803334802, s15 -> -42.4980946075818944, tr5 -> -142.3635183570893599*I},
{s12 -> 118.7855926706272785, s23 -> 53.9818166789401602, s34 -> -25.6400502441703823, s45 -> 185.0172468007340911, s15 -> -44.4043719169864743, tr5 -> 3210.7885656301791267*I},
{s12 -> 4.4943324127920787, s23 -> 7.6612238501591257, s34 -> -2.0184222660204847, s45 -> 12.4185321673556850, s15 -> -3.1054517865068683, tr5 -> -9.0460969387101647*I},
{s12 -> 32.8920448142684442, s23 -> 14.9528259898354056, s34 -> -25.2253794731356940, s45 -> 64.7495223774064302, s15 -> -46.3471973791896082, tr5 -> -312.2382515279947267*I},
{s12 -> 2.6938951472977894, s23 -> 87.5149321330084852, s34 -> -44.9308164948185656, s45 -> 138.3350022314754431, s15 -> -25.5900095848883444, tr5 -> 403.5825871904223699*I},
{s12 -> 41.4846922834953429, s23 -> 54.8912529307605732, s34 -> -29.7545074372895912, s45 -> 96.6609343321370708, s15 -> -14.8256506536339814, tr5 -> 42.7530199419167165*I}};


bt32[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T0[p1_,p2_,p3_,p4_,p5_] := MBprepare[l[1]*l[1]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*w132},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T2[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]}*
     PR[l[1]+l[2],0,a8],
	{l[1],l[2]}, Invariants->invariants];

bt32T3[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*w132},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]}*
     PR[l[1]+l[2],0,a8],
	{l[1],l[2]}, Invariants->invariants];

bt32T3a[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*w132,l[1]l[1]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T3b[p1_,p2_,p3_,p4_,p5_] := 2*MBprepare[{l[1]*w132,l[1]l[2]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T3c[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*w132,l[2]l[2]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt32T4[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*l[1]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]}*
     PR[l[1]+l[2],0,a8],
	{l[1],l[2]}, Invariants->invariants];

bt32T5[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*w132,l[1]*w132},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];


bt22M1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1]-p[p2],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt22M1T1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1]-p[p2],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]
	 PR[l[1]+l[2],0,a7]},
	{l[1],l[2]}, Invariants->invariants];


bt22M2[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt22M2T1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]
	 PR[l[1]+l[2],0,a7]},
	{l[1],l[2]}, Invariants->invariants];


bt225L[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*p[p3]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT2[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[2]*p[p3]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT3[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*p[p3],l[2]*p[p3]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT4[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]*
     PR[l[1]+l[2],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT4a[p1_,p2_,p3_,p4_,p5_] := 2*MBprepare[{l[1]l[2]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT4b[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]l[1]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT4c[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[2]l[2]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT5[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*p[p3],l[2]*p[p5]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

bt225LT6[p1_,p2_,p3_,p4_,p5_] := MBprepare[{l[1]*p[p1],l[2]*p[p3]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];


ord = {{1,2,3,4,5}, {2,3,4,5,1}, {3,4,5,1,2}, {4,5,1,2,3}, {5,1,2,3,4}};
bt32F = Table[0,{k,1,5}]
bt32T0F = Table[0,{k,1,5}]
bt32T1F = Table[0,{k,1,5}]
bt32T2F = Table[0,{k,1,5}]
bt32T3F = Table[0,{k,1,5}]
bt32T3aF = Table[0,{k,1,5}]
bt32T3bF = Table[0,{k,1,5}]
bt32T3cF = Table[0,{k,1,5}]
bt32T4F = Table[0,{k,1,5}]
bt32T5F = Table[0,{k,1,5}]


bt22M1F = Table[0,{k,1,5}]
bt22M1T1F = Table[0,{k,1,5}]


bt22M2F = Table[0,{k,1,5}]
bt22M2T1F = Table[0,{k,1,5}]


bt225LF = Table[0,{k,1,5}]
bt225LT1F = Table[0,{k,1,5}]
bt225LT2F = Table[0,{k,1,5}]
bt225LT3F = Table[0,{k,1,5}]
bt225LT4F = Table[0,{k,1,5}]
bt225LT4aF = Table[0,{k,1,5}]
bt225LT4bF = Table[0,{k,1,5}]
bt225LT4cF = Table[0,{k,1,5}]
bt225LT5F = Table[0,{k,1,5}]
bt225LT6F = Table[0,{k,1,5}]


Do[
bt22M1F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt22M1,ord[[k]]]]);
bt22M1T1F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,tt-1},Apply[bt22M1T1,ord[[k]]]]);,
{k,1,1}]


Do[
bt22M2F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt22M2,ord[[k]]]]);
bt22M2T1F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,tt-1},Apply[bt22M2T1,ord[[k]]]]);,
{k,1,1}]


Do[
bt225LF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225L,ord[[k]]]]);
bt225LT1F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT1,ord[[k]]]]);
bt225LT2F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT2,ord[[k]]]]);
bt225LT3F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT3,ord[[k]]]]);
bt225LT4aF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT4a,ord[[k]]]]);
bt225LT4bF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT4b,ord[[k]]]]);
bt225LT4cF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT4c,ord[[k]]]]);
bt225LT4F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,tt-1},Apply[bt225LT4,ord[[k]]]]);
bt225LT5F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT5,ord[[k]]]]);
bt225LT6F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1},Apply[bt225LT6,ord[[k]]]]);,
{k,1,1}]


Do[
bt32F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32,ord[[k]]]]);
bt32T0F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T0,ord[[k]]]]);
bt32T1F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T1,ord[[k]]]]);
bt32T2F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1,tt-1},Apply[bt32T2,ord[[k]]]]);
bt32T3F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1,tt-1},Apply[bt32T3,ord[[k]]]]);
bt32T3aF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T3a,ord[[k]]]]);
bt32T3bF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T3b,ord[[k]]]]);
bt32T3cF[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T3c,ord[[k]]]]);
bt32T4F[[k]] = MBresolve[#,{eps->0,tt->0}] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1,tt-1},Apply[bt32T4,ord[[k]]]]);
bt32T5F[[k]] = MBresolve[#,eps] & /@ (+ eps^2*INT[6 - 2*eps,{1,1,1,1,1,1,1},Apply[bt32T5,ord[[k]]]]);,
{k,1,1}]


bt32x1 = MBexpand[bt32F[[1]],1/cGamma2,{eps,0,0}]
bt32xk1sq = MBexpand[bt32T0F[[1]],1/cGamma2,{eps,0,0}]
bt32xk1w132 = MBexpand[bt32T1F[[1]],1/cGamma2,{eps,0,0}]
tmp=MBexpand[bt32T2F[[1]],1,{tt,0,0}];
bt32xk1k2sq = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]]
tmp=MBexpand[bt32T3F[[1]],1,{tt,0,0}];
bt32xk1k2sqk1w132 = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]] /. kids
tmp=MBexpand[bt32T4F[[1]],1,{tt,0,0}];
bt32xk1k2sqk1sq = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]]
bt32xk1w132sq = MBexpand[bt32T5F[[1]],1/cGamma2,{eps,0,0}] /. kids

MBmerge[MBexpand[bt32T3aF[[1]],1/cGamma2,{eps,0,0}]] /. kids
MBmerge[MBexpand[bt32T3bF[[1]],1/cGamma2,{eps,0,0}]] /. kids
MBmerge[MBexpand[bt32T3cF[[1]],1/cGamma2,{eps,0,0}]] /. kids


bt22M1x1 = MBexpand[bt22M1F[[1]],1/cGamma2,{eps,0,0}]
tmp = MBexpand[bt22M1T1F[[1]],1,{tt,0,0}];
bt22M1xk1k2sq = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]]


bt22M2x1 = MBexpand[bt22M2F[[1]],1/cGamma2,{eps,0,0}]
tmp = MBexpand[bt22M2T1F[[1]],1,{tt,0,0}];
bt22M2xk1k2sq = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]]


bt225Lx1 = MBexpand[bt225LF[[1]],1/cGamma2,{eps,0,0}]
bt225Lxk1p3 = MBmerge[MBexpand[bt225LT1F[[1]],1/cGamma2,{eps,0,0}]]
bt225Lxk2p3 = MBmerge[MBexpand[bt225LT2F[[1]],1/cGamma2,{eps,0,0}]]
bt225Lxk1p3k2p3 = MBmerge[MBexpand[bt225LT3F[[1]],1/cGamma2,{eps,0,0}]]
tmp = MBexpand[bt225LT4F[[1]],1,{tt,0,0}];
bt225Lxk1k2sq = MBmerge[MBexpand[tmp,1/cGamma2,{eps,0,0}]]


MBmerge[MBexpand[bt225LT4aF[[1]],1/cGamma2,{eps,0,0}]]
MBmerge[MBexpand[bt225LT4bF[[1]],1/cGamma2,{eps,0,0}]]
MBmerge[MBexpand[bt225LT4cF[[1]],1/cGamma2,{eps,0,0}]]


bt225Lxk1p3k2p5 = MBmerge[MBexpand[bt225LT5F[[1]],1/cGamma2,{eps,0,0}]]


bt225Lxk1p1k2p3 = MBmerge[MBexpand[bt225LT6F[[1]],1/cGamma2,{eps,0,0}]]


Print["bt32[1] = ",0 // InputForm]
Print["bt32[(k1.w132] = ",0 // InputForm]
Print["bt32[(k1+k2)^2] = ",bt32xk1k2sq[[1,1]] // InputForm]
Print["bt32[(k1+k2)^2k1.w132] = ",bt32xk1k2sqk1w132[[1,1]] // InputForm]


Print["bt22M1[1] = ",bt22M1x1[[1,1]] // InputForm]
Print["bt22M1[(k1+k2)^2] = ",bt22M1xk1k2sq[[1,1]] // InputForm]


Print["bt22M2[1] = ",bt22M2x1[[1,1]] // InputForm]
Print["bt22M2[(k1+k2)^2] = ",bt22M2xk1k2sq[[1,1]] // InputForm]


Print["bt225L[1] = ",bt225Lx1[[1,1]] // InputForm]
Print["bt225L[(k1+k2)^2] = ",bt225Lxk1k2sq[[1,1]] // InputForm]
Print["bt225L[k1.p3] = ",bt225Lxk1p3[[1,1]] // InputForm]
Print["bt225L[k2.p3] = ",bt225Lxk2p3[[1,1]] // InputForm]
Print["bt225L[(k1.p3*k2.p3] = ",bt225Lxk1p3k2p3[[1,1]] // InputForm]
Print["bt225L[(k1.p3*k2.p5] = ",bt225Lxk1p3k2p5[[1,1]] // InputForm]
Print["bt225L[(k1.p1*k2.p3] = ",bt225Lxk1p1k2p3[[1,1]] // InputForm]



