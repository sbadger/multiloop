interface(quiet=true);
with(spinors):

Digits:=30:
assign(declaremomn([seq(0,k=1..7)])):

#P1:=p1+p2:
#P2:=p3+p4:
#P3:=p5+p6:

P1:=p1+p2:
P2:=p3:
P3:=p4+p5:

#P1:=p1:
#P2:=p2:
#P3:=p3+p4:

#P1:=p1:
#P2:=p2:
#P3:=p3:

gg:=dot(P1,P2)-sqrt(dot(P1,P2)^2-S(P1)*S(P2));
gg:=dot(P1,P2)+sqrt(dot(P1,P2)^2-S(P1)*S(P2));

k1f := INV(gg^2-S(P1)*S(P2))*( -S(P1)*P2+gg*P1 )*gg:
k2f := INV(gg^2-S(P1)*S(P2))*( -S(P2)*P1+gg*P2 )*gg:

dot(k1f,k1f);
dot(k2f,k2f);
quit;

zero=dot(k1f,k1f);
zero=dot(k2f,k2f);
zero=2*dot(k1f,k2f)-gg;

IF:=(x,y)->1/(x+y):
F:=(x,y)->x+y:
read("test.mpl"):
w:= ( AB(k2f,P3,k1f)*eta(k1f,k2f)-AB(k2f,P3,k1f)*eta(k2f,k1f) )/2/gg:

massless=CC-sqrt(DD);

l21:=l11-P2:
l31:=l11-P2-P3:
l41:=l11+P1:

zero=expand(dot(l11,l11));
zero=expand(dot(l21,l21));
zero=expand(dot(l31,l31));
zero=expand(dot(l41,l41));

l22:=l12-P2:
l32:=l12-P2-P3:
l42:=l12+P1:

zero=expand(dot(l12,l12));
zero=expand(dot(l22,l22));
zero=expand(dot(l32,l32));
zero=expand(dot(l42,l42));

