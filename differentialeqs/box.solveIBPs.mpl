interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

read("IBPmatrix.mpl"):
read("IBPvector.mpl"):

IBPs:=MM.II;

integrals:=[]:
for i from 1 to nops(II) do:
integrals:=[op(integrals),II[i]]:
od:

eqs:={}:
for i from 1 to 4 do:
eqs:={op(eqs),IBPs[i]=0}:
od:

sols:=solve(eqs,[I4(2,1,1,1),I4(1,2,1,1),I4(1,1,2,1),I4(1,1,1,2)]);

printf("id %A;\n",collect(sols[1,1],I4));
printf("id %A;\n",collect(sols[1,2],I4));
printf("id %A;\n",collect(sols[1,3],I4));
printf("id %A;\n",collect(sols[1,4],I4));
