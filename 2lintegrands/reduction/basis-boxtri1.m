
Get["../../autobasisv1/basis-radium.m"];

(* box-tri1 *)
L=2;
Dim=4;
ExternalMomentaBasis={p2,p3};
Kinematics={p2^2->0,p3^2->0,p2 p3->s/2, \[Omega]1^2->1, \[Omega]2^2->1};
numeric={s->11};
Props={l1,l1+p2+p3,l2+p2,l2,l2-p3,l2-l1-p3};
RenormalizationLoopMomenta={
{1,0},
{0,1},
{1,1}
};
RenormalizationPower={3,4,5};

GenerateBasis[]
ISP
Basis

Integrand = 0;
For[j = 1, j <= Length[Basis], j++,
  ThisTerm = cc[]*nc[j];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm*c[ i, Basis[[j, i]]]*ISP[[i]]^Basis[[j, i]];
   ];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm //. cc[x1___]*c[i, x2_] -> cc[x1, x2]
   ];
  Integrand = Integrand + ThisTerm;
  ];

x11=DP[L1,p2];
x13=DP[L1,Omega1];
x23=DP[L4,Omega1];
x14=DP[L1,Omega2];
x24=DP[L4,Omega2];

fout = OpenWrite["boxtri1.basis.h"];
WriteString[fout,"id Basisboxtri1 = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];
