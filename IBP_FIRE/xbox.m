(* vim: set ft=mma: *)

UsingFermat = False;
QLinkPath = "/home/het/badger/local/FIRE/QLink64";
FLinkPath = "/home/het/badger/local/FLink/FLink";
FermatPath = "/home/het/badger/local/ferl64/feris64";
(*DataPath = "/scr.local/badger/";*)
DataPath = "/lscrx/badger/";
DatabaseUsage = 0;

Get["/home/het/badger/local/FIRE/SBases_3.1.0.m"];
Get["/home/het/badger/local/FIRE/FIRE_3.4.0.m"];
Get["/home/het/badger/local/FIRE/IBP.m"]

Internal = {l1, l2};
External = {k1, k2, k4};
Propagators = {l1^2, l2^2, (l1 + k1)^2, (l2 + k2)^2, (l1 - l2 + k1)^2, (l2 - l1 + k2)^2, (l1 - l2 + k1 + k4)^2, (l1 - k1 - k2 - k4)^2, (l2 + k4)^2};
PrepareIBP[];
reps = {k1^2 -> 0, k2^2 -> 0, k4^2 -> 0, k1 k2 -> s/2, k2 k4 -> -(s + t)/2, k1 k4 -> t/2};
startinglist = {IBP[l1, k1], IBP[l1, k2], IBP[l1, k4], IBP[l1, l1], IBP[l1, l2] , IBP[l2, k1], IBP[l2, k2], IBP[l2, k4], IBP[l2, l1], IBP[l2, l2]} /. reps;
SYMMETRIES = {{2,1,4,3,6,5,7,9,8}};
Prepare[AutoDetectRestrictions -> True];
SaveStart["CrossedBox"];

BuildAll[{0,0,0,0,0,0,0,0,0}];

Burn[];

Print["evaluating x-box ",m,n]

II[m,n]=F[{1,1,1,1,1,1,1,m,n}];
Print["Integral ",m,n]
II[m,n] // InputForm
expr=II[m,n] /. d->4-2*eps;
Print["eps->0 "]
Normal[Series[expr, {eps, 0, 0}]] // InputForm

