#-
auto S p,x,db,bt,s
S k1,k2;
S mu11,mu12,mu33,mu22;
S a,b,c,d,e,f;
S D,eps,[],Delta,[Ds-2];

S Tmu11,Tmu22,Tmu33,dim;
CF diff;

CF I,NUM,INV;

CF INT;
CF MBresolve;

#procedure expandmu

#do mu={mu11,mu22,mu33}
id 'mu'^x? = (-1)^x*NUM('mu',x);
repeat;
id NUM('mu',0) = 1;
id NUM('mu',x?)*I(D?,?pows) = NUM('mu',x-1)*I(D+1,?pows)*xT'mu'*dim + diff('mu',0)*I(D,?pows);
endrepeat;

id xTmu11 = xT2 + xT3;
id xTmu33 = xT1 + xT2;
id xTmu22 = xT1 + xT3;
id diff(mu11,x?) = diff(xT1,0);
id diff(mu22,x?) = diff(xT2,0);
id diff(mu33,x?) = diff(xT3,0);

repeat id diff(a?, x?)*a? = diff(a, x+1);
id diff(a?, 0) = 0;
id diff(a?, 1) = 1;

#enddo

id I(2,?args) = eps*(eps-1)*I(D,?args);
id I(1,?args) = eps*I(D,?args);
id I(0,?args) = I(D,?args);

id xT1*xT2 = 1/dim - (xT2*xT3+xT1*xT3);

repeat id dim*I(D?,?args) = I(D+2,?args);

#endprocedure

#procedure expandx(topo)
id xT1 = T1;
id xT2 = T2;
id xT3 = T3;

id x1^3*I(x?,p1?,?args) = 6*I(x,p1+3,?args);
id x1^2*I(x?,p1?,?args) = 2*I(x,p1+2,?args);
id x1*I(x?,p1?,?args) = I(x,p1+1,?args);
#do n=2,8
id x'n'^3*I(x?,p1?,...,p'n'?,?args) = 6*I(x,p1,...,p{'n'-1},p'n'+3,?args);
id x'n'^2*I(x?,p1?,...,p'n'?,?args) = 2*I(x,p1,...,p{'n'-1},p'n'+2,?args);
id x'n'*I(x?,p1?,...,p'n'?,?args) = I(x,p1,...,p{'n'-1},p'n'+1,?args);
#enddo

AB eps;
.sort
collect NUM;
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(xx?) = xx;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

id I(d?,?args) = INT(d,?args,'topo');

argument INT,1;
id D = 4-2*eps;
endargument;

#endprocedure
