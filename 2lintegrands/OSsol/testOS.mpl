# vim: set foldmethod=marker:
interface(quiet=true):
Digits:=30:
with(spinors):

assign(declaremomn([0,0,0,0])):

freevars:={tau1=0.6739,tau2=0.1123,tau=0.3927}:

read(cat("boxbox.OSsol.log")):
#{{{
for k from 1 to 6 do:
printf("boxbox : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k1[k]-p2:
l[4]:=k2[k]+p3:
l[5]:=k2[k]:
l[6]:=k2[k]-p4:
l[7]:=k2[k]-k1[k]-p1-p4:

for j from 1 to 7 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

read(cat("tripent.OSsol.log")):
#{{{
for k from 1 to 2 do:
assign(sol[k]):
printf("tripent : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k1[k]+p2+p3:
l[4]:=k2[k]+p3:
l[5]:=k2[k]:
l[6]:=k2[k]-p4:
l[7]:=k2[k]-k1[k]+p2+p3:

for j from 1 to 7 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

read(cat("boxboxX.OSsol.log")):
#{{{
for k from 1 to 8 do:
assign(sol[k]):
printf("boxboxX : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k2[k]+p3:
l[4]:=k2[k]:
l[5]:=k2[k]-p4:
l[6]:=k2[k]-k1[k]+p2+p3:
l[7]:=k2[k]-k1[k]+p3:

for j from 1 to 7 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

P1:=p1+p4:
read(cat("boxtri1.OSsol.log")):
#{{{
for k from 1 to 4 do:
printf("boxtri1 : solution %d\n",k):

l[1]:=k1[k]:
l[2]:=k1[k]-P1:
l[3]:=k2[k]+p2:
l[4]:=k2[k]:
l[5]:=k2[k]-p3:
l[6]:=k2[k]-k1[k]-p3:

for j from 1 to 6 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

read(cat("boxtri2.OSsol.log")):
#{{{
for k from 1 to 6 do:
assign(sol[k]):
printf("boxtri2 : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k2[k]+p3:
l[4]:=k2[k]:
l[5]:=k2[k]-p4:
l[6]:=k2[k]-k1[k]+p2+p3:

for j from 1 to 6 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

read(cat("tritriBT.OSsol.log")):
#{{{
for k from 1 to 4 do:
assign(sol[k]):
printf("tritriBT : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k1[k]-p2:
l[4]:=k2[k]+p3:
l[5]:=k2[k]:
l[6]:=k2[k]-p4:

for j from 1 to 6 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4');
od:
#}}}

