#-
#include- ../lib/GU.hdr

auto S mu,nu,rho;

S i,I;
S s12,s13,s14;
S tau1,...,tau4;
S k1,k2,ww;
S Ds,[Ds-2];

CF NUM,INV,TMP;
CF cs,ds,dot;

#include- ../lib/factorise.h

#procedure simplify

id AB(p3,p2+p4,p1) = 0;
id i = I;

id A(p1?ps,p3)*IA(p2?ps,p3) = -B(P124-p1-p2,p2)*IB(P124-p1-p2,p1);
id B(p1?ps,p3)*IB(p2?ps,p3) = -A(P124-p1-p2,p2)*IA(P124-p1-p2,p1);
multiply replace_(P124,p1+p2+p4);
#call cancel
id A(p1?ps,p4)*IA(p2?ps,p4) = -B(P123-p1-p2,p2)*IB(P123-p1-p2,p1);
id B(p1?ps,p4)*IB(p2?ps,p4) = -A(P123-p1-p2,p2)*IA(P123-p1-p2,p1);
multiply replace_(P124,p1+p2+p4);
multiply replace_(P123,p1+p2+p3);
#call cancel

AB s13,s12,s14;
.sort
collect NUM;
factarg NUM;
chainout NUM;
argument NUM;
id s13 = -s12-s14;
endargument;

splitarg NUM;
id NUM(xx?) = xx;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

#endprocedure

set mus:mu1,...,mu10,nu1,...,nu10,rho1,...,rho10;

off stats;

.global

#include- out.prc

argument NUM;
id AB(p1?ps,p2?ps,p1?ps) = S(p1,p2);
id S(p1,p2) = s12;
id S(p3,p4) = s12;
id S(p2,p1) = s12;
id S(p4,p3) = s12;
id S(p1,p3) = s13;
id S(p3,p1) = s13;
id S(p2,p4) = s13;
id S(p4,p2) = s13;
id S(p1,p4) = s14;
id S(p4,p1) = s14;
id S(p2,p3) = s14;
id S(p3,p2) = s14;
id s13 = -s12-s14;
id AB(p3,p2,p4) = -AB(p3,p1,p4);
id AB(p1,p4,p2) = -AB(p1,p3,p2);
id AB(p2,p3,p4) = -AB(p2,p1,p4);
id AB(p3,p4,p1) = -AB(p3,p2,p1);
id AB(p3,p4,p2) = -AB(p3,p1,p2);
endargument;

id NUM(xx?) = xx;
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
#call cancel
id IA(p1?,p2?) = B(p2,p1)*IS(p1,p2);
id IB(p1?,p2?) = A(p2,p1)*IS(p1,p2);
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id S(p1,p2) = s12;
id S(p3,p4) = s12;
id S(p2,p1) = s12;
id S(p4,p3) = s12;
id S(p1,p3) = s13;
id S(p3,p1) = s13;
id S(p2,p4) = s13;
id S(p4,p2) = s13;
id S(p1,p4) = s14;
id S(p4,p1) = s14;
id S(p2,p3) = s14;
id S(p3,p2) = s14;
id IS(p1,p2) = 1/s12;
id IS(p3,p4) = 1/s12;
id IS(p2,p1) = 1/s12;
id IS(p4,p3) = 1/s12;
id IS(p1,p3) = 1/s13;
id IS(p3,p1) = 1/s13;
id IS(p2,p4) = 1/s13;
id IS(p4,p2) = 1/s13;
id IS(p1,p4) = 1/s14;
id IS(p4,p1) = 1/s14;
id IS(p2,p3) = 1/s14;
id IS(p3,p2) = 1/s14;

id A(p2,p4) = -s13/s14*A(p1,p4)*A(p2,p3)*IA(p1,p3);
id IA(p2,p4) = -s14/s13*IA(p1,p4)*IA(p2,p3)*A(p1,p3);
id A(p3,p4) = s12/s14*A(p1,p4)*A(p2,p3)*IA(p1,p2);
id IA(p3,p4) = s14/s12*IA(p1,p4)*IA(p2,p3)*A(p1,p2);
id B(p1,p2) = -s12*IA(p1,p2);
id B(p3,p4) = -s12*IA(p3,p4);
id B(p1,p4) = -s14*IA(p1,p4);
id B(p2,p3) = -s14*IA(p2,p3);
id B(p2,p4) = -s13*IA(p2,p4);
id B(p1,p3) = -s13*IA(p1,p3);
id IB(p1,p2) = -1/s12*A(p1,p2);
id IB(p3,p4) = -1/s12*A(p3,p4);
id IB(p1,p4) = -1/s14*A(p1,p4);
id IB(p2,p3) = -1/s14*A(p2,p3);
id IB(p2,p4) = -1/s13*A(p2,p4);
id IB(p1,p3) = -1/s13*A(p1,p3);
#call cancel 

#call simplify
id NUM(x?) = x;

*B NUM,tau1,...,tau4,Ds,[Ds-2];
*format 150;
*print+s;
.sort

*id tau3^2 = -4*s14*s13/s12*(  
*    - mu11
*    + tau1 * ( 1/2*s12/s13 )
*    + tau1^2 * (  - 1/4*s12/s13/s14 )
*    + 1/4*s12 + 1/4*s12^2/s13
*    );

*id tau3*tau4 = -2*s13*s14/s12*(
*    - mu12
*    + tau2 * (  - 1/2*s12/s13 )
*    + tau1 * (  - 1/2*s12/s13 )
*    + tau1*tau2 * (  - 1/2*s12/s13/s14 - 1/s13 )
*    - 1/2*s12 - 1/2*s12^2/s13
*    );

*id tau4^2 = -4*s14*s13/s12*(    
*    - mu22
*    + tau2 * ( 1/2*s12/s13 )
*    + tau2^2 * (  - 1/4*s12/s13/s14 )
*    + 1/4*s12 + 1/4*s12^2/s13
*    );

*id tau1 = 2*dot(k1,p4);
*id tau2 = 2*dot(k2,p1);
*id tau3 = 2*dot(k1,ww);
*id tau4 = 2*dot(k2,ww);
.sort

#call simplify
id NUM(s12+s14) = -s13;
id NUM(-s12-s14) = s13;
id NUM(x?) = x;
id s13=-s12-s14;

multiply ds(0,0,0,0);
repeat;
id,once tau1*ds(x1?,x2?,x3?,x4?) = ds(x1+1,x2,x3,x4);
id,once tau2*ds(x1?,x2?,x3?,x4?) = ds(x1,x2+1,x3,x4);
id,once tau3*ds(x1?,x2?,x3?,x4?) = ds(x1,x2,x3+1,x4);
id,once tau4*ds(x1?,x2?,x3?,x4?) = ds(x1,x2,x3,x4+1);
endrepeat;

*format 150;
*B Ds,dot,mu11,mu12,mu22,tau1,...,tau4;
*print+s;
.store

#do i=1,160
G Dvec'i' = AMP;
#include- /lscr221/badger/gitrepos/multiloop/2lintegrands/reduction/final/boxboxD.coeffvecs.prc

id TMPdvec('i') = 1;
id TMPdvec(x?) = 0;

.store
#enddo

L Integrand =
#do i=1,160
+xCvec'i'*xClbl'i'
#enddo
;
#include- /lscr221/badger/gitrepos/multiloop/2lintegrands/reduction/final/boxboxD.integrand.prc

denominators INV;
argument INV;
id S(p1,p2) = s12;
id S(p1,p3) = s13;
id S(p1,p4) = s14;
endargument;
id S(p1,p2) = s12;
id S(p1,p3) = s13;
id S(p1,p4) = s14;

factarg INV;
chainout INV;
splitarg INV;
id INV(xx?) = 1/xx;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);
#call simplify
id NUM(?x)*INV(?x) = 1;
id NUM(s12+s14) = -s13;
id NUM(-s12-s14) = s13;
id NUM(x?) = x;
#call simplify
id NUM(?x)*INV(?x) = 1;
id NUM(s12+s14) = -s13;
id NUM(-s12-s14) = s13;
id NUM(x?) = x;

id IA(p3,p4) = s14/s12*IA(p1,p4)*IA(p2,p3)*A(p1,p2);
#call cancel 
id IA(p2,p4) = -s14/s13*IA(p1,p4)*IA(p2,p3)*A(p1,p3);
#call cancel
id s13 = -s12-s14;
#call simplify
id NUM(?x)*INV(?x) = 1;
id NUM(x?) = x;
#call cancel
#call simplify
id NUM(?x)*INV(?x) = 1;
id NUM(s12+s14) = -s13;
id NUM(-s12-s14) = s13;
id NUM(x?) = x;
#call simplify
id NUM(?x)*INV(?x) = 1;
id NUM(s12+s14) = -s13;
id NUM(-s12-s14) = s13;
id NUM(x?) = x;


id xxghost1=2;
id xxghost2=-2;
id xxghost3=-2;
id Ds=[Ds-2]+2;

format 150;
B Ds,[Ds-2],dot,mu11,mu12,mu22,tau1,...,tau4;
print+s;
.end
