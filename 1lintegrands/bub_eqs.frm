* vim: set foldmethod=marker:
#-
S x,y,z,w1,w2,w3;
S tau;
S [l1.p3],[l4.p2],[l1.w],[l4.w];
auto S c,X,Y,p,a1,...,a4,b1,...,b4,x,y,P;
S k1f,XX,S1,S2,S12,gg,sqrtgg;
S t,t1,...,t8,l1,...,l7;
CF INV,NUM,S(s),IS(s);

CF A(a),B(a),AB;
CF IA(a),IB(a),IAB,dot;
CF M,sqrt,F,IF,eta;
S mu;
S I,DD,CC;

auto S xx;

set mus:mu;
set massless:p0,k1f,XX;

*{{{ lprod
#procedure lprod

id AB(p1?,mu?mus,p2?)*AB(p3?,mu?mus,p4?) = -2*A(p1,p3)*B(p2,p4);
id M(px?,mu?mus)*M(py?,mu?mus) = dot(px,py);
id M(px?,mu?mus)*AB(p1?,mu?mus,p2?) = AB(p1,px,p2);

#endprocedure
*}}}
*{{{ cancel
#procedure cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;
id S(p1?)*IS(p1?) = 1;
id AB(?x)*IAB(?x) = 1;
id F(?x)*IF(?x) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

id S(k1f,XX) = gg;
id IS(k1f,XX) = 1/gg;
id I^2=-1;

id NUM(px?)*INV(px?) = 1;
splitarg NUM;
id NUM(px?) = px;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
id INV(S(px?)) = IS(px);
id INV(gg) = 1/gg;
id INV(-1) = -1;
id INV(4) = 1/4;
id INV(AB(?x)) = IAB(?x);
id INV(INV(?x)) = NUM(?x);

#endprocedure
*}}}
*{{{ factorise(func)
#procedure factorise(func)
argument 'func';
topolynomial;
endargument;
.sort
factarg 'func';
.sort
argument 'func';
frompolynomial;
endargument;
chainout 'func';
#endprocedure
*}}}
*{{{ expandkf
#procedure expandkf
#call cancel
id AB(px?,p1,py?) = AB(px,k1f,py)+S(p1)/gg*AB(px,XX,py);
id AB(px?,p0?massless,py?) = A(px,p0)*B(p0,py);
id A(k1f,XX)*B(XX,k1f) = gg;
id IA(k1f,XX)*IB(XX,k1f) = 1/gg;
id dot(p1,p1) = S(p1);
#endprocedure
*}}}

off stats;

.global

#do s=1,2
G k's' = ( a1's'*k1f+a2's'*XX+a3's'*AB(k1f,mu,XX) + a4's'*AB(XX,mu,k1f) )/2;
.sort
G l1's' = k's';
G l2's' = k's'-M(p1,mu);
#enddo
G W3 = k1f-S(p1)/gg*XX;
G W1 = ( AB(k1f,mu,XX) - AB(XX,mu,k1f) )/2;
G W2 = I*( AB(k1f,mu,XX) + AB(XX,mu,k1f) )/2;

id k1f = AB(k1f,mu,k1f);
id XX = AB(XX,mu,XX);

id a41=a11*a21/t1;
id a11=t2; 
id a21=(1-t2)*S(p1)/gg;
id a31=t1;

id a32=a12*a22/t1;
id a12=t2; 
id a22=(1-t2)*S(p1)/gg;
id a42=t1;

format 150;
B INV,AB;
print+s l11,l12;
.store

*{{{ check on-shell conditions
#do s=1,2
#do k=1,2
L zero'k''s' = l'k''s'*l'k''s';
#enddo
#enddo

#call lprod
#call cancel
#call expandkf


print+s;
.store
*}}}

G Delta = c000 
  + c100*dot(l1,w1) + c010*dot(l1,w2) + c001*dot(l1,w3)
  + c110*dot(l1,w1)*dot(l1,w2) + c011*dot(l1,w2)*dot(l1,w3) + c101*dot(l1,w1)*dot(l1,w3)
  + c2x2*(dot(l1,w1)^2-dot(l1,w3)^2)
  + cx22*(dot(l1,w2)^2-dot(l1,w3)^2)
;

B x;
print+s;
.store

#do s=1,2
G Delta's' = Delta;

#do x=1,3
repeat;
id,once dot(l1,w'x') = l1's'*W'x';
#call lprod
endrepeat;
.sort
#enddo

#call cancel
#call expandkf
#call cancel

B t1,t2;
print[];
.store
#enddo

#do t1=-2,2
#do t2=0,4

#do s=1,2
L d's'{'t1'+2}'t2' = Delta's';
#enddo

if(count(t1,1)!='t1');
discard;
endif;
if(count(t2,1)!='t2');
discard;
endif;

id t1=1;
id 1/t1=1;
id t2=1;
id 1/t2=1;

print+s;
.sort
hide;
#enddo
#enddo

#define csset "{c000,c100,c010,c001,c110,c011,c101,c2x2,cx22}"
.sort

#write <matrix_bub.mpl> "cs:=Vector(9):"
#define i "1"
#do c='csset'
#write <matrix_bub.mpl> "cs['i']:='c':"
#define i "{'i'+1}"
#enddo

#write <matrix_bub.mpl> "dvec:=Vector(24):"
#write <matrix_bub.mpl> "eqs:=Matrix(24,9):"
#define i "1"
#do s=1,2
#do t1=-2,2

#define t2min "0"
#if 't1'<0
#redefine t2min "{-'t1'}"
#endif

#write "'t1','t2min',{2-'t1'}"

#do t2='t2min',{2-'t1'}

#write <matrix_bub.mpl> "dvec['i']:=ds['s','t1','t2']:"

#define j "1"
#do c='csset'

L eqs'i'x'j' = d's'{'t1'+2}'t2';

if(count('c',1)<1);
discard;
endif;
id 'c'=1;

.sort
#write <matrix_bub.mpl> "eqs['i','j']:=%E:" eqs'i'x'j'
#define j "{'j'+1}"
.sort
hide;
#enddo

#define i "{'i'+1}"
#enddo
#enddo
#enddo

.sort
unhide;

L cfss = NUM(Delta);

argument NUM;
id dot(l1,w3) = 1;
endargument;

format 150;
print cfss;
.sort

repeat id NUM(x?,?xx) = 1+NUM(?xx);
print cfss;

.end
