(* ::Package:: *)

SetDirectory["/Users/simon/gitrepos/multiloop/5g_allplus/nplanar_diagrams"]


ToMomTwistors = {
s12->x1,s13->-x5*x2-x1,s14->x5*x3+x5*x2-x3,s15->-x3*(x5-1),s23->x4*x2,s24->-(x2+x1)*(-x3+x4*x3+x4*x2)/x2,s25->(x2*x3+x1*x3+x1*x2)/x2*(-1+x4),s34->(-x5*x2*x3-x1*x3+x4*x2*x3+x4*x1*x3+x4*x1*x2)/x2,s35->-(x3+x2)*(-x1+x4*x1-x5*x2+x4*x2)/x2,s45->x2*(-x5+x4),a12->-1,a13->-1,a14->-1,a15->-1,a23->1/x1,a24->(x2+x1)/x1/x2,a25->(x2*x3+x1*x3+x1*x2)/x1/x2/x3,a34->1/x2,a35->(x3+x2)/x2/x3,a45->1/x3,b12->x1,b13->-x5*x2-x1,b14->x5*x3+x5*x2-x3,b15->-x3*(x5-1),b23->-x4*x1*x2,b24->x1*(-x3+x4*x3+x4*x2),b25->-x1*x3*(-1+x4),b34->x1*x3-x4*x1*x3-x4*x1*x2+x5*x2*x3-x4*x2*x3,b35->x3*(-x1+x4*x1-x5*x2+x4*x2),b45->-x2*x3*(-x5+x4)
};
FromMomTwistors = {
x1->s12,
x2 -> (s12*(s23-s15)+s23*s34+s15*s45-s34*s45-TR5)/2/s34,
x3 ->((s23\[Minus]s45)(s23 s34+s15 s45\[Minus]s34 s45\[Minus]TR5)+s12 (s15\[Minus]s23)s23+s12 (s15+s23)s45)/(2(s12+s23\[Minus]s45)s45),
x4 -> -(s12*(s23-s15)+s23*s34+s15*s45-s34*s45+TR5)/(2*s12*(s15-s23+s45)),
x5 -> ((s23-s45)*(s12*(s23-s15)+s23*s34+s15*s45-s34*s45+TR5))/(2*s12*s23*(-s15+s23-s45))};
numerics ={s12->20.229075563170760414851788657611,s13->4.229075563170760414851788657611,s14->-17.009369453088242234365957963397,
s15->-7.448781673253278595337619351826,s23->42.000000000000000000000000000000,s24->-24.724390836626639297668809675913,
s25->-37.504684726544121117182978981698,s34->-24.724390836626639297668809675913,s35->-21.504684726544121117182978981698,
s45->66.458151126341520829703577315223,TR5->-444.022732940130048643034445881230I};
TR5sq = s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2 /. {
s13->s45-s12-s23, s14-> s23-s45-s15,s24->s15-s23-s34,s25->s34-s15-s12, s35->s12-s34-s45} // Simplify;
rule1 = Table[TR5^(2k)->TR5sq^k,{k,1,8}];
rule2 = Table[TR5^(2k+1)->TR5sq^k*TR5,{k,1,8}];
tr5rule={rule1,rule2} // Flatten;
prefactor=-0.000246728505083995950083470738-0.000187855584874189032328317829I;
FromMomTwistorsNumerical=FromMomTwistors /. numerics;
FullNumerics = ToMomTwistors /. FromMomTwistorsNumerical // Chop;


<< "db311_tmp.m";


d500=Coefficient[zero,tau1^5] /. {tau2-> 0, tau3->0}  /. ToMomTwistors;
d410=Coefficient[zero,tau1^4*tau2] /. tau3->0  /. ToMomTwistors;
d401=Coefficient[zero,tau1^4*tau3] /. tau2->0  /. ToMomTwistors;
d400=Coefficient[zero,tau1^4] /. {tau2-> 0, tau3->0}  /. ToMomTwistors;


d500 = Simplify[d500];
d410 = Simplify[d410];
d401 = Simplify[d401];
d400 = Simplify[d400];


Ans = Simplify[Solve[{d500 == 0, d401 == 0, d410 == 0, d400==0},{xx1,xx0}]];


a0 = xx0 /. Ans[[1]]
a1 = xx1 /. Ans[[1]]
a0= Factor[a0 /. FromMomTwistors ] //. tr5rule // Simplify;
a1= Factor[a1 /. FromMomTwistors ] //. tr5rule // Simplify;


na0 = Collect[Numerator[a0],TR5];
da0 = Denominator[a0];
na0 = Collect[Simplify[na0 //.tr5rule],TR5];
a0 = na0/da0 // FullSimplify

na1 = Collect[Numerator[a1],TR5];
da1 = Denominator[a1];
na1 = Collect[Simplify[na1 //.tr5rule],TR5];
a1 = na1/da1 // FullSimplify


N[prefactor*a0 /. numerics // ScientificForm ,10]
N[prefactor*a1/2/. numerics // ScientificForm ,10]


file= OpenWrite["db311_coeffs.prc"]
WriteString[file,"id a0 = ", a0 /. {TR5->tr5} //InputForm,";\n"]
WriteString[file,"id a1 = ", a1 /. {TR5->tr5} //InputForm,";\n"]
Close[file]



