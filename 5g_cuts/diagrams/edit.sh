#! /bin/sh

for f in *.input.mpl checks_5g+++++.mpl
do
  sed -i -e "s/Z\([0-9]*\)_=/Z[\1]:=/g" $f
  sed -i -e "s/Z\([0-9]*\)_/Z[\1]/g" $f
  sed -i -e "s/\([^:]\)=/\1:=/g" $f
  mv $f tmp
done
