# vim: set foldmethod=marker:
interface(quiet=true):
interface(displayprecision=10):

mypow:=proc(a,b); return a^b; end:

Digits:=40:
with(spinors):
genmoms([0,0,0,0]):

s:=S(p1,p2):
t:=S(p1,p4):

read("Dcvecs.log"):

print("non-spurious 4-d terms:");

cs[0,0,0,0,0,0,0];
cs[0,0,1,0,0,0,0];
cs[0,0,0,1,0,0,0];
cs[0,0,2,0,0,0,0];
cs[0,0,1,1,0,0,0];
cs[0,0,0,2,0,0,0];
cs[0,0,3,0,0,0,0];
cs[0,0,2,1,0,0,0];
cs[0,0,1,2,0,0,0];
cs[0,0,0,3,0,0,0];
cs[0,0,4,0,0,0,0];
cs[0,0,3,1,0,0,0];
cs[0,0,2,2,0,0,0];
cs[0,0,1,3,0,0,0];
cs[0,0,0,4,0,0,0];

print("MI coeffs:");

C1:=
+cs[0,0,0,0,0,0,0]
+s*t*cs[0,0,1,1,0,0,0]/2
-s^2*t*cs[0,0,2,1,0,0,0]/2
-s^2*t*cs[0,0,1,2,0,0,0]/2
+s^3*t*cs[0,0,3,1,0,0,0]/2
+s^3*t*cs[0,0,1,3,0,0,0]/2
;
