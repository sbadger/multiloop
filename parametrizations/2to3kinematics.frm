#-

auto S x,s,p,k,l,a,b,Dmu,mu,nu,rho,omega;

S x,y,z,w,t;

CF A(a),B(a),IA(a),IB(a);
CF eta,AB,IAB,MOM,dot(s);

CF cc,Power,NUM,INV;
CF EPS,GGG(s);

CF cs,ds;

S rt2,I,Ds,[Ds-2],m;

CF schouten;

auto S tau,Basis;

set ps:p1,...,p4;
set Dmu2:Dmu11,Dmu12,Dmu22,Dmu33;
set mus:mu1,...,mu20,nu1,...,nu20,rho1,...,rho20;

#procedure cancel
id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id AB(?x)*IAB(?x) = 1;
id NUM(?x)*INV(?x) = 1;
#endprocedure

off stats;

.global

L zero1 = schouten(A,p1,p2,p3,p4);
L zero2 = schouten(A,p1,p2,p3,p5);
L zero3 = schouten(A,p1,p2,p4,p5);
L zero4 = schouten(A,p2,p3,p4,p5);

L zero5  = AB(p1,MOM(p2,p3,p4),p5);
L zero6  = AB(p1,MOM(p2,p3,p5),p4);
L zero7  = AB(p1,MOM(p2,p4,p5),p3);
L zero8  = AB(p1,MOM(p3,p4,p5),p2);

L zero9  = AB(p5,MOM(p2,p3,p4),p1);
L zero10  = AB(p1,MOM(p2,p3,p4),p5)*AB(p5,MOM(p2,p3,p4),p1);

L test1  = AB(p5,MOM(p2,p3),p1);

L mhv12 = A(p1,p2)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1);
L mhvb12 = B(p1,p2)^4*IB(p1,p2)*IB(p2,p3)*IB(p3,p4)*IB(p4,p5)*IB(p5,p1);
L mhv23 = A(p2,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1);
L mhv24 = A(p2,p4)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1);
L mhv25 = A(p2,p5)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1);

L test2 = s12*s23*s34*s45*s15;

repeat;
id AB(p1?,MOM(p2?,?x),p3?) = A(p1,p2)*B(p2,p3)+AB(p1,MOM(?x),p3);
id AB(p1?,MOM,p2?) = 0;
endrepeat;

id schouten(A?,p1?,p2?,p3?,p4?) = A(p1,p2)*A(p3,p4)+A(p1,p3)*A(p4,p2)+A(p1,p4)*A(p2,p3);

#do i=1,5
#do j=1,5
#if 'i'<'j'
id A(p'i',p'j') = a'i''j';
id B(p'i',p'j') = b'i''j';
id IA(p'i',p'j') = -1/s'i''j'*b'i''j';
id IB(p'i',p'j') = -1/s'i''j'*a'i''j';
#endif
#enddo
#enddo

id a12 = 1;
id a13 = -x;
id a14 = (1+x)/(x*y-1);
id a15 = -1;
id a23 = 1;
id a24 = -(1+y)/(x*y-1);
id a25 = y;
id a34 = 1;
id a35 = 1-x*y;
id a45 = 1;
id s12 = -w*z*(x*y-1);
id 1/s12 = -1/w/z/(x*y-1);
id s13 = -x*(t*w-z*y*t-z*y*w);
id 1/s13 = -1/x/(t*w-z*y*t-z*y*w);
id s14 = (w-z*y)*(1+x)*t;
id 1/s14 = 1/(w-z*y)/(1+x)/t;
id s15 = z*y*t-z*w-t*w;
id 1/s15 = -1/(-z*y*t+z*w+t*w);
id s23 = t*w*x-t*z-z*w;
id 1/s23 = 1/(t*w*x-t*z-z*w);
id s24 = -(-z+w*x)*t*(1+y);
id 1/s24 = -1/(-z+w*x)/t/(1+y);
id s25 = y*(x*z*w+t*w*x-t*z);
id 1/s25 = 1/y/(x*z*w+t*w*x-t*z);
id s34 = w*(x*y-1)*t;
id 1/s34 = 1/w/(x*y-1)/t;
id s35 = -(x*y-1)*(t*z+z*w+t*w);
id 1/s35 = -1/(x*y-1)/(t*z+z*w+t*w);
id s45 = z*(x*y-1)*t;
id 1/s45 = 1/z/(x*y-1)/t;
id b12 = x*z*y*w-z*w;
id b13 = z*y*(t+w)-t*w;
id b14 = z*y*(x*y-1)*t-w*(x*y-1)*t;
id b15 = z*y*t-w*(z+t);
id b23 = z*(t+w)-t*w*x;
id b24 = z*(x*y-1)*t-w*x*(x*y-1)*t;
id b25 = t*z-w*x*(z+t);
id b34 = t^2*(x*y-1)-(t+w)*(x*y-1)*t;
id b35 = t^2-(t+w)*(z+t);
id b45 = t^2*(x*y-1)-(x*y-1)*t*(z+t);

denominators INV;
factarg INV;
chainout INV;
splitarg INV;
id INV(x1?) = x1;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);
#call cancel

AB x,y,z,w,t;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
#call cancel

format 150;
B INV;
print+s;
.end
