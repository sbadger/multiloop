interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

# solving db320 in 4-2e dimensions

NUM:=x->x;
INV:=x->1/x;

read("db320D.reduceM.log"):
unassign('Dvec'):

dvec:=Vector(79):
for k from 1 to 79 do:
dvec[k]:=Dvec[k]:
od:

eqs:=ReduceMatrix:
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(eqsP.eqsL):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=eqsU:
Ieqs:=MatrixInverse(eqsSq):

dvecT:=dtransform.dvec:

dvecR:=dvecT:
Cvec:=Ieqs.dvecR:

fd:=fopen("final/db320D.integrand.mpl",WRITE):

fprintf(fd,"Cvec:=Vector(79):\n"):
for k from 1 to 79 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,Cvec[k]):
od:
fclose(fd):

read("final/db320D.coeffvecs.mpl"):

integrand:=0:
for i from 1 to 79 do:
integrand:=integrand+Clbl[i]*Cvec[i];
od;

fd:=fopen("final/db320D.integrand.prc",WRITE):
fprintf(fd,"L INTdb320D = \n%A;\n",integrand):
fclose(fd):

