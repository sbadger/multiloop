* vim: set foldmethod=marker:

CF BoxBoxBox;

*{{{topologise3
#procedure topologise3

*{{{ general triple box
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
#do i8={1,-1}
#do i9={1,-1}
#do i10={1,-1}

id BoxBoxBox(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),
    Kmom(?x5),Kmom(?x6),Kmom(?x7),Kmom(?x8),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),
    ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),ptl(l5,'i5',xptl5?),
    ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?),
    ptl(l8,'i8',xptl8?),ptl(l9,'i9',xptl9?),ptl(l10,'i10',xptl10?)
   )

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4),ptl(-l9,-'i9',-xptl9))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5),ptl(l10,'i10',xptl10))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l7,'i7',xptl7))
	  * AMP(ptl(-l7,-'i7',-xptl7),?x7,ptl(l8,'i8',xptl8),ptl(-l10,-'i10',-xptl10))
	  * AMP(ptl(-l8,-'i8',-xptl8),?x8,ptl(l1,'i1',xptl1),ptl(l9,'i9',xptl9))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)*SIGN(xptl8)
	*SIGN(xptl9)*SIGN(xptl10)
	;

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort

*** factors of I for fermion propagators ***
*** still need to fix signs in trees and correctly assign propagators ***
id SIGN(0)    = 1;
id SIGN(1/2)  = -I;
id SIGN(-1/2) = I;
id SIGN(1)    = -1;
id SIGN(-1)   = -1;

id AMP(?x1,,?x2) = AMP(?x1,?x2);
id AMP(?x1,0,?x2) = AMP(?x1,?x2);

#endprocedure
*}}}
