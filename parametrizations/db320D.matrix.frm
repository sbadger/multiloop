#-
#include- 2to3tools.prc
.global

#define sol "1"

#do i=1,7
G L'i'(nu1) = l'i';
#enddo
G MU11 = mu11;
G MU12 = mu12;
G MU22 = mu22;

id l1=k1;
id l2=k1-p1;
id l3=k1-p1-p2;
id l4=k2-p1-p2-p3;
id l5=k2-p4;
id l6=k2-p3-p4;
id l7=k2;
id l8=k1+k2;
id k1 = av1*a1+av2*a2+av3*a3+av4*a4;
id k2 = bv1*b1+bv2*b2+bv3*b3+bv4*b4;
id av1 = MOM(p1,nu1);
id av2 = MOM(p2,nu1);
id av3 = B(p1,p3)*IB(p2,p3)*AB(p1,nu1,p2)/2;
id av4 = B(p2,p3)*IB(p1,p3)*AB(p2,nu1,p1)/2;
id bv1 = MOM(p4,nu1);
id bv2 = MOM(p5,nu1);
id bv3 = B(p3,p4)*IB(p3,p5)*AB(p4,nu1,p5)/2;
id bv4 = B(p3,p5)*IB(p3,p4)*AB(p5,nu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = -tau1*s23/s13;
id a4 = 1+tau1;

id b1 = 0;
id b2 = 1;
id b3 = tau2/s34;
id b4 = tau3/s35;

id mu11 = s12*s23/s13*tau1*(1+tau1);
id mu22 = -tau2*tau3*s45/s34/s35;
id mu12 =
         + tr5 * (
          - 1/2*s13^-1
          - 1/2*s13^-1*tau3*s35^-1
          - tau1*s13^-1
          - 1/2*(s34 + s45 - s12)*s13^-1*tau2*s34^-1*s35^-1
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^-1*s34^-1*tau3*s35^-1
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^-1*tau2*s34^-1*s35^-1
          )

          - 1/2*(s34 + s45 - s12)*(s23*s34 - s45*s15 - s45*s34 + s12*s15
          - s12*s23)*s13^-1*tau2*s34^-1*s35^-1
          + 1/2*( - s23*s34 + s45*s15 + s45*s34 - 2*s45*s23 + 2*s45^2 - s12*s15 + s12*s23 - 2*s12*s45)*s13^-1*tau3*s35^-1
          - 1/2*(s23*s34 - s45*s15 - s45*s34 + s12*s15 - s12*s23)*s13^-1
          - 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^-1*s34^-1*tau3*s35^-1
          + 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^-1*tau2*s34^-1*s35^-1
         ;

B AB,MOM;
print+s;
.store

G basis41(nu1) = MOM(p1,nu1);
G basis42(nu1) = MOM(p2,nu1);
G basis43(nu1) = MOM(p3,nu1);
G basis44(nu1) = MOM(p5,nu1);

.store

G x14 = 2*MOM(k1,nu1)*basis44(nu1);
G x22 = 2*MOM(k2,nu1)*basis42(nu1);
G x21 = 2*MOM(k2,nu1)*basis41(nu1);
G xmu11 = MU11;
G xmu12 = MU12;
G xmu22 = MU22;

L zero1 = MOM(k1,nu1)*MOM(k1,nu1)-mu11;
L zero2 = MOM(k1,nu1)*MOM(p1,nu1);
L zero3 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)-s12/2;
L zero4 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)+MOM(k1,nu1)*MOM(p3,nu1)-s45/2;
L zero5 = MOM(k2,nu1)*MOM(p4,nu1)+MOM(k2,nu1)*MOM(p5,nu1)-s45/2;
L zero6 = MOM(k2,nu1)*MOM(p5,nu1);
L zero7 = MOM(k2,nu1)*MOM(k2,nu1)-mu22;
L zero8 = 2*MOM(k1,nu1)*MOM(k2,nu1)-mu12;

id mu11 = MU11;
id mu12 = MU12;
id mu22 = MU22;

id MOM(k1,nu1) = L1(nu1);
id MOM(k2,nu1) = L7(nu1);

id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id MOM(p1?,nu1?)*AB(p2?,nu1?,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?,p2?)*AB(p3?,nu1?,p4?) = -2*A(p1,p3)*B(p2,p4);
id NUM(x?) = x;

#call cancel
#call simplify5

*#call changevars
*#call simplify5new

format 150;
print+s;
.store

G Delta = Basisdb320D;

#include- final/db320D.basis-new.h

id Power(x1?,x2?) = x1^x2;

id mu11 = xmu11;
id mu12 = xmu12;
id mu22 = xmu22;
.sort

B tau1,tau2,tau3;
print[];
.sort

#$nc = 1;
B cc;
.sort
keep brackets;
id cc(?x) = cc($nc,?x);
$nc=$nc+1;
.sort
#write "{'$nc'-1} integrand coefficients"
#do cc=1,{'$nc'-1}
id cc('cc',?x$clbl) = cc('cc',?x);
.sort
#write <final/db320D.coeffvecs.mpl> "Clbl['cc']:=cs('$clbl'):"
#enddo

.store

#write <db320D.NumericalReduceM.log> "ReduceMatrix:=Matrix({'$nc'-1},{'$nc'-1}):"
#write <final/db320D.coeffvecs.mpl> "Dvec:=Vector(79):"
#define dd "0"

#do pow1=0,6
#do pow2=0,6
#do pow3=0,6

#$nonzero=0;
L tmpRow = Delta;

#do pp=1,3
if(count(tau'pp',1)!=`pow`pp'');
discard;
endif;
id tau`pp'=1;
id 1/tau`pp'=1;
#enddo

.sort
$nonzero=$nonzero+1;
.sort
hide;

*#write "'pow1','pow2','pow3','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/db320D.coeffvecs.mpl> "Dvec['dd']:=ds('pow1','pow2','pow3'):"
#write <final/db320D.coeffvecs.prc> "id ds('pow1','pow2','pow3') = ds('dd');"

#do cc=1,{'$nc'-1}
  L tmpEl'cc' = tmpRow;
  id cc('cc',?x$clbl) = 1;
  id cc(?x)=0;
  .sort
  format maple;
  #write <db320D.NumericalReduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
  .sort
  hide;
#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo

.end
