d100 := ds[1,-2,0]:
d101 := ds[1,-2,1]:
d102 := ds[1,-2,2]:
d103 := ds[1,-2,3]:
d104 := ds[1,-2,4]:
d110 := ds[1,-1,0]:
d111 := ds[1,-1,1]:
d112 := ds[1,-1,2]:
d113 := ds[1,-1,3]:
d114 := ds[1,-1,4]:
d120 := ds[1,0,0]:
d121 := ds[1,0,1]:
d122 := ds[1,0,2]:
d123 := ds[1,0,3]:
d124 := ds[1,0,4]:
d130 := ds[1,1,0]:
d131 := ds[1,1,1]:
d132 := ds[1,1,2]:
d133 := ds[1,1,3]:
d134 := ds[1,1,4]:
d140 := ds[1,2,0]:
d141 := ds[1,2,1]:
d142 := ds[1,2,2]:
d143 := ds[1,2,3]:
d144 := ds[1,2,4]:
d200 := ds[2,-2,0]:
d201 := ds[2,-2,1]:
d202 := ds[2,-2,2]:
d203 := ds[2,-2,3]:
d204 := ds[2,-2,4]:
d210 := ds[2,-1,0]:
d211 := ds[2,-1,1]:
d212 := ds[2,-1,2]:
d213 := ds[2,-1,3]:
d214 := ds[2,-1,4]:
d220 := ds[2,0,0]:
d221 := ds[2,0,1]:
d222 := ds[2,0,2]:
d223 := ds[2,0,3]:
d224 := ds[2,0,4]:
d230 := ds[2,1,0]:
d231 := ds[2,1,1]:
d232 := ds[2,1,2]:
d233 := ds[2,1,3]:
d234 := ds[2,1,4]:
d240 := ds[2,2,0]:
d241 := ds[2,2,1]:
d242 := ds[2,2,2]:
d243 := ds[2,2,3]:
d244 := ds[2,2,4]:
csolA[1] := 1/2*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d121 + 
      NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d120 + 1/2*NUM( - 
      4*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d122:
csolA[2] := 1/2*S(p1)*IS(p1)*IWRAP(gg)*d131 + S(p1)*IS(p1)*IWRAP(gg)*d130 + 1/
      2*IS(p1)*d113 - IS(p1)*d111:
csolA[3] := 1/2*S(p1)*IS(p1)*IWRAP(gg)*I*d131 + S(p1)*IS(p1)*IWRAP(gg)*I*d130
       - 1/2*IS(p1)*I*d113 + IS(p1)*I*d111:
csolA[4] :=  - 1/2*IS(p1)*d122 - 1/2*IS(p1)*d121:
csolA[5] := 2*S(p1)^2*IS(p1)^2*IWRAP(gg)^2*I*d140 - 2*IS(p1)^2*I*d102:
csolA[6] :=  - 1/2*S(p1)*IS(p1)^2*IWRAP(gg)*I*d131 + 1/2*IS(p1)^2*I*d113:
csolA[7] :=  - 1/2*S(p1)*IS(p1)^2*IWRAP(gg)*d131 - 1/2*IS(p1)^2*d113:
csolA[8] := S(p1)^4*IS(p1)^4*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(
      gg))*WRAP(gg)*IWRAP(gg)^3*d140 + S(p1)^2*IS(p1)^4*NUM( - 8*S(p1) + WRAP(
      gg))*INV( - 8*S(p1) + WRAP(gg))*d102 + S(p1)*IS(p1)^2*INV( - 8*S(p1) + 
      WRAP(gg))*d122:
csolA[9] :=  - S(p1)^4*IS(p1)^4*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + 
      WRAP(gg))*WRAP(gg)*IWRAP(gg)^3*d140 - S(p1)^2*IS(p1)^4*NUM( - 8*S(p1) + 
      WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d102 + S(p1)*IS(p1)^2*INV( - 8*S(p1
      ) + WRAP(gg))*d122:
zeroA[1] := d104 - d102:
zeroA[2] := d112 + d113 + d111:
zeroA[3] := d103 + 2*d102:
zeroA[4] := d202 - S(p1)^2*IWRAP(gg)^2*d140:
zeroA[5] := d203 + 2*S(p1)^2*IWRAP(gg)^2*d140:
zeroA[6] := d204 - S(p1)^2*IWRAP(gg)^2*d140:
zeroA[7] := d211 - S(p1)*IWRAP(gg)*d130:
zeroA[8] := d212 - S(p1)*IWRAP(gg)*d131 + S(p1)*IWRAP(gg)*d130:
zeroA[9] := d213 + S(p1)*IWRAP(gg)*d131:
zeroA[10] := d220 - d120:
zeroA[11] := d221 - d121:
zeroA[12] := d222 - d122:
zeroA[13] := S(p1)*IS(p1)*d230 - IS(p1)*WRAP(gg)*d111:
zeroA[14] := S(p1)*IS(p1)*d231 + IS(p1)*WRAP(gg)*d113:
printf("1,%A = %.16Ze\n",cs[1],csol[1]);
printf("2,%A = %.16Ze\n",cs[2],csol[2]);
printf("3,%A = %.16Ze\n",cs[3],csol[3]);
printf("4,%A = %.16Ze\n",cs[4],csol[4]);
printf("5,%A = %.16Ze\n",cs[5],csol[5]);
printf("6,%A = %.16Ze\n",cs[6],csol[6]);
printf("7,%A = %.16Ze\n",cs[7],csol[7]);
printf("8,%A = %.16Ze\n",cs[8],csol[8]);
printf("9,%A = %.16Ze\n",cs[9],csol[9]);
printf("zero10 = %.16Ze\n",zeroA[10]);
printf("zero11 = %.16Ze\n",zeroA[11]);
printf("zero12 = %.16Ze\n",zeroA[12]);
printf("zero13 = %.16Ze\n",zeroA[13]);
printf("zero14 = %.16Ze\n",zeroA[14]);
printf("zero15 = %.16Ze\n",zeroA[15]);
printf("zero16 = %.16Ze\n",zeroA[16]);
printf("zero17 = %.16Ze\n",zeroA[17]);
printf("zero18 = %.16Ze\n",zeroA[18]);
printf("zero19 = %.16Ze\n",zeroA[19]);
printf("zero20 = %.16Ze\n",zeroA[20]);
printf("zero21 = %.16Ze\n",zeroA[21]);
printf("zero22 = %.16Ze\n",zeroA[22]);
printf("zero23 = %.16Ze\n",zeroA[23]);
printf("zero24 = %.16Ze\n",zeroA[24]);
