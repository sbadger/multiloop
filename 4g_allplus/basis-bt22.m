Get["/lscr221/badger/gitrepos/mathematicam2/BasisDet-1-02.m"];

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* bow-tie *)
L=2;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={
  p1^2 -> 0, p2^2 -> 0, p4^2 -> 0,
  p1 p2 -> s/2, p1 p4 -> t/2,
  p2 p4 -> -(s+t)/2,
  \[Omega]1^2 -> -t(s+t)/s};
numeric={
  s -> 11,t -> 3
};
Props={l1, l1-p1, l1-p1-p2, l2-p3-p4, l2-p4, l2};
RenormalizationCondition={{{1,0},3},{{0,1},3},{{1,1},6}};

GenerateBasis[0]

fout = OpenWrite["basis/bt22.basis.h"];
WriteString[fout,"id Basisbt22 = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

