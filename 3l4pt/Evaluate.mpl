# vim: set foldmethod=marker:
interface(quiet=true):
interface(displayprecision=10):
Digits:=50:
with(spinors):

d:=Array(0..33,1..14,-4..4,-4..4):

## set 4pt kinematics
genmoms([0,0,0,0]):

#printf("reading in integrands...\n");
#start:=time():
#read("C10.trbox.INT.log"):
#printf("finished in %.2f s\n",time()-start);

#{{{ on-shell loop momenta

OSmoms:=proc(p1,p2,p3,p4,sol,t1,t2)
local k1,k2,k3,s12,s13,s14,cf1,cf2,cf3,bv1,bv2,bv3,Z11,Z12,Z13,Z14,Z21,Z22,Z23,Z24,Z31,Z34,Z41,Z43,b,is12,is14,is13;
global l1,l2,l3,l4,l5,l6,l7,l8,l9,l10;

s12:=S(p1,p2):
s14:=S(p1,p4):
s13:=S(p1,p3):

is12:=1/s12:
is14:=1/s14:
is13:=1/s13:

cf1 := [
[ 0,  0, 1-s12*is14/t2-s12*is14*t1/t2,  0],
[ 0,  0,  0, s14*is13-s12*is13/t2-s12*is13*t1/t2],
[ 0,  0, -s13*is14-s13*is14*t1,  0],
[ 0,  0,  0, -1-t1],
[ 0,  0, t2,  0],
[ 0,  0,  0, s14*is13*t2],
[ 0,  0, -s13*is14-s13*is14*t1,  0],
[ 0,  0,  0, -1-t1],
[ 0,  0, t2,  0],
[ 0,  0,  0, s14*is13*t2],
[ 0,  0, 1,  0],
[ 0,  0,  0, s14*is13],
[ 0,  0, t1,  0],
[ 0,  0,  0, s14*is13*t1]
];

cf3 := [
[ 0,  0, 1+t2,  0],
[ 0,  0,  0, s14*is13+s14*is13*t2],
[ 0,  0,  0, -1+s12*is13/t1],
[ 0,  0, -s13*is14+s12*is14/t1,  0],
[ 0,  0,  0, -1-t1],
[ 0,  0, -s13*is14-s13*is14*t1,  0],
[ 0,  0,  0, t2],
[ 0,  0, s13*is14*t2,  0],
[ 0,  0, 1,  0],
[ 0,  0,  0, s14*is13],
[ 0,  0, t2,  0],
[ 0,  0,  0, s14*is13*t2],
[ 0,  0, t2,  0],
[ 0,  0,  0, s14*is13*t2]
];

cf2 := [
[s12*is14/t1/t2+s12*is14/t1+s12*is14/t2+s12*is14, 1/t1*t2-s12*is14/t1-s12*is14, -s13*is14/t1-s13*is14, -s12*is13/t1-s12*is13/t1*t2+s12^2*is13*is14/t1/t2+s12^2*
      is13*is14/t1+s12^2*is13*is14/t2+s12^2*is13*is14],
[s12*is14/t1/t2+s12*is14/t1+s12*is14/t2+s12*is14, 1/t1*t2-s12*is14/t1-s12*is14, -1/t1-1/t1*t2+s12*is14/t1/t2+s12*is14/t1+s12*is14/t2
      +s12*is14, -s12*is14/t1-s12*is14],
[-s13*is14*t1-s13*is14*t1*t2+s12*is14+s12*is14*t2, s12*is14/t1*t2+s12*is14*t2, s13*is14*t2-s12*is14/t1*t2, -s12*is14-s12*is14*t2-s12*is14*t1-s12*is14*t1*t2],
[-s13*is14*t1-s13*is14*t1*t2+s12*is14+s12*is14*t2, s12*is14/t1*t2+s12*is14*t2, -s13*is14-s13*is14*t2-s13*is14*t1-s13*is14*t1*t2, s12*s13*is13*is14*t2-s12^2*is13*is14/t1*t2],
[s12*is14/t1+s12*is14,  0,  0, -s12*s13*is13*is14+s12^2*is13*is14/t1],
[s12*is14/t1+s12*is14,  0, -s13*is14+s12*is14/t1,  0],
[ 0, -s12*is14/t1-s12*is14, -s13*is14+s12*is14/t1,  0],
[ 0, -s12*is14/t1-s12*is14,  0, -s12*s13*is13*is14+s12^2*is13*is14/t1],
[t1,  0,  0, s12*is13+s12*is13*t1],
[t1,  0, 1+t1,  0],
[ 0, t1,  0, s12*is13-s12*is13*t1],
[ 0, t1, 1-t1,  0],
[ 0,  0,  0, s12*is13],
[ 0,  0, 1,  0]
];

#cf1:=[
# [0 , 0 , 1-s12/s14*(1+t2)/t1 , 0],
# [0 , 0 , 0 , s14/s13*( 1+(1+t2)/t1 )],
# [0 , 0 , -s13/s14*(1+t2) , 0],
# [0 , 0 , 0 , -(1+t2)],
# [0 , 0 , t1 , 0],
# [0 , 0 , 0 , t1],
# [0 , 0 , -s13/s14*(1+t2) , 0],
# [0 , 0 , 0 , -(1+t2)],
# [0 , 0 , t2 , 0],
# [0 , 0 , 0 , t2],
# [0 , 0 , 1 , 0],
# [0 , 0 , 0 , s14/s13],
# [0 , 0 , t2 , 0],
# [0 , 0 , 0 , t2]
#]:

#Z11:=s12/s14*(1+1/t1)*(1+1/t2);
#Z12:=t1/t2-s12/s14*(1+1/t2);
#Z13:=-s13/s14*(1+1/t2);
#Z14:=s12/s13*( s12/s14*(1+1/t1)*(1+1/t2)-(1+t1)/t2 );

#Z21:=s12/s14*(1+1/t2)*(1-s14/s12/t1);
#Z22:=-s12/s14*(1+(1+t1)/t2);
#Z23:=(1+(1+t1)/t2)*(s12/s14-1/t1);
#Z24:=-s12/s14*(1+1/t2);

#Z31:=(1+t1)*(s12/s14-s13/s14*t2);
#Z34:=-s12/s14*(1+t1)*(1+t2);

#Z41:=(1+t1)*(s12/s14-s13/s14*t2);
#Z43:=-s13/s14*(1+t1)*(1+t2);

#cf2:=[
#  [Z11 , Z12 , Z13 , Z14],
#  [Z21 , Z22 , Z23 , Z24],
#  [Z31 , s12/s14*(1+1/t2)*t1 , (s13/s14-s12/s14/t2)*t1 , Z34],
#  [Z41 , s12/s14*(1+1/t2)*t1 , Z43 , s12/s14*t1*(1-s12/s13/t2)],
#  [s12/s14*(1+1/t2) , 0 , 0 , s12/s13/s14*(s12/t2-s13)],
#  [s12/s14*(1+1/t2) , 0 , s12/s14/t2-s13/s14 , 0],
#  [0 , -s12/s14*(1+1/t2) , s12/s14/t2-s13/s14 , 0],
#  [0 , -s12/s14*(1+1/t2) , 0 , s12/s13/s14*(s12/t2-s13)],
#  [t1 , 0 , 0 , s12/s13*(1+t1)],
#  [t1 , 0 , 1+t1 , 0],
#  [0 , t2 , 0 , s12/s13*(1-t2)],
#  [0 , t2 , 1-t2 , 0],
#  [0 , 0 , 0 , s12/s13],
#  [0 , 0 , 1 , 0]
#]:

#cf3:=[
# [0 , 0 , 1+t1 , 0],
# [0 , 0 , 0 , -s12/s13*t1+s14/s13],
# [0 , 0 , 0 , s12/s13/t2-1],
# [0 , 0 , s12/s14/t2-s13/s14 , 0],
# [0 , 0 , 0 , -(1+t2)],
# [0 , 0 , -s13/s14*(1+t2) , 0],
# [0 , 0 , 0 , t1],
# [0 , 0 , t1 , 0],
# [0 , 0 , 1 , 0],
# [0 , 0 , 0 , s14/s13],
# [0 , 0 , t1 , 0],
# [0 , 0 , 0 , t1],
# [0 , 0 , t1 , 0],
# [0 , 0 , 0 , t1]
#]:

bv1:=[p1 , p2 , A(p2,p3)*IA(p1,p3)*eta(p1,p2)/2 , A(p1,p3)*IA(p2,p3)*eta(p2,p1)/2 ]:
bv2:=[p2 , p3 , A(p3,p4)*IA(p2,p4)*eta(p2,p3)/2 , A(p2,p4)*IA(p3,p4)*eta(p3,p2)/2 ]:
bv3:=[p3 , p4 , A(p4,p1)*IA(p3,p1)*eta(p3,p4)/2 , A(p3,p1)*IA(p4,p1)*eta(p4,p3)/2 ]:

k1:=Vector(4):
k2:=Vector(4):
k3:=Vector(4):
for b from 1 to 4 do:
k1:=k1+cf1[sol,b]*bv1[b];
k2:=k2+cf2[sol,b]*bv2[b];
k3:=k3+cf3[sol,b]*bv3[b];
od:

l1:=k1+p1:
l2:=k1:
l3:=k1-p2:
l4:=k2:
l5:=k3+p3:
l6:=k3:
l7:=k3-p4:
l8:=k2+p1+p2:
l9:=k2-k1+p2:
l10:=k2-k3-p3:

return;

end:
#}}}

#for sol from 1 to 14 do;
#print(sol);
#OSmoms(p1,p2,p3,p4,sol,0.8927,0.2637):
#for k from 1 to 10 do:
#print(k,"zero=",expand(dot(l||k,l||k)));
#od:
#od:

Nf:=4;
Ns:=3:
#ftop:=1:

flav:=[
[0,0,0],
# Nf terms
[0,0,1/2],
[0,1/2,0],
[1/2,0,0],
[0,1/2,1/2],
[1/2,1/2,0],
[1/2,1/2,1/2],
# Ns terms
[0,0,1],
[0,1,0],
[1,0,0],
[0,1,1],
[1,1,0],
[1,1,1],
# Ns*Nf terms
[0,1,1/2],
[1,1,1/2],
[0,1/2,1],
[1,1/2,0],
[1/2,1,1],
[1/2,1,0],
[0,1/2,-1/2],
[1,1/2,1/2],
[1/2,1/2,1],
[1/2,-1/2,0],
[1/2,1/2,-1/2],
[1/2,-1/2,-1/2],
[1,0,1/2],
[1/2,0,1],
[1/2,0,1/2],# Nf^2
[1,0,1],# Ns^2
[1,1/2,1],
[1,1/2,-1/2],
[1/2,-1/2,-1],
[1/2,-1/2,1/2],
[1/2,-1,1/2]# 2*Nf*Ns
]:

flavcf:=[
1,
"-n_f",
"-n_f",
"-n_f",
"-n_f",
"-n_f",
"-n_f",
"n_s",
"n_s",
"n_s",
"n_s",
"n_s",
"n_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"n_f^2",
"n_s^2",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"-n_fn_s",
"2n_fn_s"
]:

nops(flavcf);

fset:={0=g,1/2=q,-1/2="\\qb",1=s,-1="\\sb"}:
for i from 1 to 34 do:
f4:=subs(fset,flav[i,1]-flav[i,2]):
f5:=subs(fset,flav[i,2]-flav[i,3]):
printf("$%A$ &\t$%A$ &\t$%A$ &\t$%A$ &\t$%A$ &\t$%A$\\\\\n",subs(fset,flav[i,1]),subs(fset,-flav[i,2]),subs(fset,-flav[i,3]),f4,f5,flavcf[i]);
od:
quit;

hel:=10:
dout:=fopen(cat("out.",hel,".ds.log"),WRITE):

printf("reading in tree-level...\n");
start:=time():
read("trees.log"):
printf("finished in %.2f s\n",time()-start);
print("tree=",A0tree[hel]);

print("target N=4 coefficient = ",-A0tree[hel]*S(p1,p2)^3*S(p2,p3));

for ftop from 1 to 34 do:
printf("flavour config. %d...\n",ftop);

ds:=Array(1..14,-4..4,-4..4):
for sol from 1 to 14 do:

printf("starting solution %d...\n",sol);
start:=time():

for k1 from 0 to 8 do:
tau1:=evalf( exp(2*Pi*I*k1/9+1e-3*I) ):
for k2 from 0 to 8 do:
tau2:=evalf( exp(2*Pi*I*k2/9+1e-3*I) ):


OSmoms(p1,p2,p3,p4,sol,tau1,tau2):
intvalue := C10_trbox_INT[hel,op(flav[ftop])]:

for pow1 from -4 to 4 do:
for pow2 from -4 to 4 do:
  ds[sol,pow1,pow2] := ds[sol,pow1,pow2]+intvalue/9/9/tau1^pow1/tau2^pow2;
od:
od:

od:
od:

printf("finshed solution %d in %.2f s...\n",sol,time()-start);

for pow1 from -4 to 4 do:
for pow2 from -4 to 4 do:

  check:=d[ftop-1,sol,pow2,pow1]:

  if abs(ds[sol,pow1,pow2])<1e-10 then:
    ds[sol,pow1,pow2]:=0:
    printf("ds[%d,%d,%d,%d] = %.10Ze \t (abs. diff vs. HFJ %.10Ze)\n",ftop,sol,
      pow1,pow2,ds[sol,pow1,pow2],
      ds[sol,pow1,pow2]-check);
  else:
    printf("ds[%d,%d,%d,%d] = %.10Ze \t (rel. diff vs. HFJ %.10Ze)\n",ftop,sol,
      pow1,pow2,ds[sol,pow1,pow2],
      (ds[sol,pow1,pow2]-check)/ds[sol,pow1,pow2]);
  fi;

  fprintf(dout,"ds[%d,%d,%d,%d,%d] := %.16e + (%.16e)*I;\n",hel,ftop,sol,pow1,pow2,Re(ds[sol,pow1,pow2]),Im(ds[sol,pow1,pow2]));
od:
od:

od:

od:

fclose(dout);

