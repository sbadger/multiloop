#-
S q, i, isq2, sq2, k1, k2, m, m1, m2, m11, m22, m33, m12, P124, Ds;
auto S mu, nu, rho, l, p, x;
CF V, eps, epsp, epsm, eps0, P, eta(s),
  A(a), B(a), IA(a), IB(a),
  S(s), IS(s), prop, AB,
  dotp(s),dot;

CF Vg, propg;
CF bar;

S s12,s13,s14;
S tau1,...,tau4;

CF NUM,INV,TMP,DDim;

#procedure cancel

id dotp(p1?ps,p1?ps) = 0;
id dot(p1?ps,p1?ps) = 0;
id dotp(p1?ps,p2?ps) = 1/2*S(p1,p2);
id dot(p1?ps,p2?ps) = 1/2*S(p1,p2);
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

id S(p3,p4) = S(p1,p2);
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id IS(p3,p4) = IS(p1,p2);
id IS(p2,p3) = IS(p1,p4);
id IS(p2,p4) = IS(p1,p3);

id S(p1,p2) = s12;
id S(p1,p3) = s13;
id S(p1,p4) = s14;

id IS(p1,p2) = 1/s12;
id IS(p1,p3) = 1/s13;
id IS(p1,p4) = 1/s14;

#endprocedure

#procedure Lcontract

id P(-p1?,?x) = -P(p1,?x);

id epsp(p1?,x1?,mu1?) = isq2*AB(x1,mu1,p1)*IA(x1,p1);
id epsm(p1?,x1?,mu1?) = isq2*AB(p1,mu1,x1)*IB(p1,x1);

id AB(p1?,mu1?mus,p3?)*eta(mu1?mus,mu2?mus) = AB(p1,mu2,p3);
id AB(p1?,mu1?mus,p3?)*P(p2?,mu1?mus) = AB(p1,p2,p3);
id AB(p1?,mu1?mus,p2?)*AB(p3?,mu1?mus,p4?) = -2*A(p1,p3)*B(p2,p4);

#do fun={epsp,epsm,P}
id 'fun'(?x,mu1?)*eta(mu1?,mu2?) = 'fun'(?x,mu2);
#enddo
id eta(mu1?,mu2?)*eta(mu2?,mu3?) = eta(mu1,mu3);
id P(p1?,mu1?)*P(p2?,mu1?) = dotp(p1,p2);
id eta(mu1?,mu1?) = Ds;

id i^2 = -1;
id isq2^2 = 1/2;

#call cancel

#endprocedure

#procedure subsL
id,once dot(k1,px?) = cf11*dot(p1,px)+cf12*dot(p2,px)+cf13*AB(p1,px,p2)/2+cf14*AB(p2,px,p1)/2;
al,once dot(px?,k1) = cf11*dot(p1,px)+cf12*dot(p2,px)+cf13*AB(p1,px,p2)/2+cf14*AB(p2,px,p1)/2;
al,once AB(px?,k1,py?) = cf11*AB(px,p1,py)+cf12*AB(px,p2,py)+cf13*A(p1,px)*B(py,p2)+cf14*A(p2,px)*B(py,p1);

al,once dot(k2,px?) = cf21*dot(p3,px)+cf22*dot(p4,px)+cf23*AB(p3,px,p4)/2+cf24*AB(p4,px,p3)/2;
al,once dot(px?,k2) = cf21*dot(p3,px)+cf22*dot(p4,px)+cf23*AB(p3,px,p4)/2+cf24*AB(p4,px,p3)/2;
al,once AB(px?,k2,py?) = cf21*AB(px,p3,py)+cf22*AB(px,p4,py)+cf23*A(p3,px)*B(py,p4)+cf24*A(p4,px)*B(py,p3);
#endprocedure

#procedure simplify

#call cancel
id A(x1?,p3)*B(p3,x2?) = -A(x1,p1)*B(p1,x2)-A(x1,p2)*B(p2,x2)-A(x1,p4)*B(p4,x2);
#call cancel

id A(p1?ps,p3)*IA(p2?ps,p3) = -B(P124-p1-p2,p2)*IB(P124-p1-p2,p1);
id B(p1?ps,p3)*IB(p2?ps,p3) = -A(P124-p1-p2,p2)*IA(P124-p1-p2,p1);
multiply replace_(P124,p1+p2+p4);
#call cancel

AB s13,s12,s14;
.sort
collect NUM;
factarg NUM;
chainout NUM;
argument NUM;
id s13 = -s12-s14;
endargument;

splitarg NUM;
id NUM(xx?) = xx;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

factarg NUM;
chainout NUM;

splitarg NUM;
id NUM(xx?) = xx;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

#endprocedure

set ps:p1,p2,p3,p4,x1,...,x4;
set mus:mu1,...,mu10,nu1,...,nu10,rho1,...,rho10;

on stats;

.global

*** on-shell solution ***

G cf11 = 1;
G cf12 = 0;
G cf13 = A(p2,p3)*IA(p1,p3)*(s14-tau1+tau3)/2/s14;
G cf14 = A(p1,p3)*IA(p2,p3)*(s14-tau1-tau3)/2/s13;
G cf21 = 0;
G cf22 = 1;
G cf23 = -A(p4,p1)*IA(p3,p1)*(s14-tau2-tau4)/2/s14;
G cf24 = -A(p3,p1)*IA(p4,p1)*(s14-tau2+tau4)/2/s13;

G cm11 = -s12/4/s14/s13*( (s14-tau1)^2-tau3^2 );
G cm12 = -s12/2/s14/s13*( -s14^2 + s14*(tau1+tau2)+tau1*tau2-tau3*tau4 + 2*s14/s12*tau1*tau2 );
G cm22 = -s12/4/s14/s13*( (s14-tau2)^2-tau4^2 );
G cm33 = cm12+cm11+cm22;
.store

L test0 = dot(k1,k1)-cm11;
L test1 = dot(k1,p1);
L test2 = dot(k1,p2)-s12/2;

L test3 = dot(k2,k2)-cm22;
L test4 = dot(k2,p4);
L test5 = dot(k2,p3)-s12/2;

L test6= dot(k1,k2)-cm12/2;

repeat;
#call subsL
#call cancel
endrepeat;
#call simplify

print+s;
.store

G AMP = 
  
  +eps(p1,mu1)*eps(p2,mu2)*eps(p3,mu3)*eps(p4,mu4)*
    V(-l1,p1,l2,nu1,mu1,rho2)*V(-l2,p2,l3,nu2,mu2,rho3)*V(-l3,-l7,l4,nu3,nu7,rho4)*
    V(-l4,p3,l5,nu4,mu3,rho5)*V(-l5,p4,l6,nu5,mu4,rho6)*V(-l6,l7,l1,nu6,rho7,rho1)*
    prop(l1,nu1,rho1,m11)*prop(l2,nu2,rho2,m11)*prop(l3,nu3,rho3,m11)*prop(l4,nu4,rho4,m22)*
    prop(l5,nu5,rho5,m22)*prop(l6,nu6,rho6,m22)*prop(l7,nu7,rho7,m33)

  +xxghost1*eps(p1,mu1)*eps(p2,mu2)*eps(p3,mu3)*eps(p4,mu4)*
    Vg(l2,mu1)*Vg(l3,mu2)*Vg(l4,nu7)*
    Vg(l5,mu3)*Vg(l6,mu4)*Vg(l1,rho7)*
    propg(l1,m11)*propg(l2,m11)*propg(l3,m11)*propg(l4,m22)*
    propg(l5,m22)*propg(l6,m22)*prop(l7,nu7,rho7,m33)

  +xxghost2*eps(p1,mu1)*eps(p2,mu2)*eps(p3,mu3)*eps(p4,mu4)*
    V(-l1,p1,l2,nu1,mu1,rho2)*V(-l2,p2,l3,nu2,mu2,rho3)*Vg(l4,nu3)*
    Vg(l5,mu3)*Vg(l6,mu4)*Vg(l7,rho1)*
    prop(l1,nu1,rho1,m11)*prop(l2,nu2,rho2,m11)*prop(l3,nu3,rho3,m11)*propg(l4,m22)*
    propg(l5,m22)*propg(l6,m22)*propg(l7,m33)

  +xxghost3*eps(p1,mu1)*eps(p2,mu2)*eps(p3,mu3)*eps(p4,mu4)*
    Vg(l2,mu1)*Vg(l3,mu2)*Vg(-l7,rho4)*
    V(-l4,p3,l5,nu4,mu3,rho5)*V(-l5,p4,l6,nu5,mu4,rho6)*Vg(l1,nu6)*
    propg(l1,m11)*propg(l2,m11)*propg(l3,m11)*prop(l4,nu4,rho4,m22)*
    prop(l5,nu5,rho5,m22)*prop(l6,nu6,rho6,m22)*propg(l7,m33)

;

id eps(p1,mu1?) = epsp(p1,p2,mu1);
id eps(p2,mu1?) = epsp(p2,p3,mu1);
id eps(p3,mu1?) = epsp(p3,p4,mu1);
id eps(p4,mu1?) = epsp(p4,p1,mu1);

repeat;
id,once V(p1?, p2?, p3?, mu1?, mu2?, mu3?) = i*isq2*(
    eta(mu2,mu3)*(P(p2,mu1)-P(p3,mu1))
  + eta(mu3,mu1)*(P(p3,mu2)-P(p1,mu2))
  + eta(mu1,mu2)*(P(p1,mu3)-P(p2,mu3))
);
id,once Vg(p1?, mu1?) = i*isq2*(
    P(p1,mu1)
);


#call Lcontract
id,once prop(p?, mu1?, mu2?, m?) = i*( -eta(mu1,mu2) + (P(p,mu1)*P(bar(p),mu2)+P(p,mu2)*P(bar(p),mu1))/2/m^2 );
id,once propg(p?, m?) = 1;
#call Lcontract
endrepeat;
.sort

*B dotp,AB;
*print+s;
*.end

repeat;
id,once dotp(x1?,l1) = dotp(x1,k1);
id,once AB(x1?,l1,x2?) = AB(x1,k1,x2);
id,once dotp(x1?,l2) = dotp(x1,k1)-dotp(x1,p1);
id,once AB(x1?,l2,x2?) = AB(x1,k1,x2)-AB(x1,p1,x2);
id,once dotp(x1?,l3) = dotp(x1,k1)-dotp(x1,p1)-dotp(x1,p2);
id,once AB(x1?,l3,x2?) = AB(x1,k1,x2)-AB(x1,p1,x2)-AB(x1,p2,x2);
id,once dotp(x1?,l6) = -dotp(x1,k2);
id,once AB(x1?,l6,x2?) = -AB(x1,k2,x2);
id,once dotp(x1?,l5) = -dotp(x1,k2)+dotp(x1,p4);
id,once AB(x1?,l5,x2?) = -AB(x1,k2,x2)+AB(x1,p4,x2);
id,once dotp(x1?,l4) = -dotp(x1,k2)+dotp(x1,p3)+dotp(x1,p4);
id,once AB(x1?,l4,x2?) = -AB(x1,k2,x2)+AB(x1,p3,x2)+AB(x1,p4,x2);
id,once dotp(x1?,l7) = -dotp(x1,k1)-dotp(x1,k2);
id,once AB(x1?,l7,x2?) = -AB(x1,k1,x2)-AB(x1,k2,x2);

id,once dotp(x1?,bar(l1)) = dotp(x1,bar(k1));
id,once AB(x1?,bar(l1),x2?) = AB(x1,bar(k1),x2);
id,once dotp(x1?,bar(l2)) = dotp(x1,bar(k1))-dotp(x1,p1);
id,once AB(x1?,bar(l2),x2?) = AB(x1,bar(k1),x2)-AB(x1,p1,x2);
id,once dotp(x1?,bar(l3)) = dotp(x1,bar(k1))-dotp(x1,p1)-dotp(x1,p2);
id,once AB(x1?,bar(l3),x2?) = AB(x1,bar(k1),x2)-AB(x1,p1,x2)-AB(x1,p2,x2);
id,once dotp(x1?,bar(l6)) = -dotp(x1,bar(k2));
id,once AB(x1?,bar(l6),x2?) = -AB(x1,bar(k2),x2);
id,once dotp(x1?,bar(l5)) = -dotp(x1,bar(k2))+dotp(x1,p4);
id,once AB(x1?,bar(l5),x2?) = -AB(x1,bar(k2),x2)+AB(x1,p4,x2);
id,once dotp(x1?,bar(l4)) = -dotp(x1,bar(k2))+dotp(x1,p3)+dotp(x1,p4);
id,once AB(x1?,bar(l4),x2?) = -AB(x1,bar(k2),x2)+AB(x1,p3,x2)+AB(x1,p4,x2);
id,once dotp(x1?,bar(l7)) = -dotp(x1,bar(k1))-dotp(x1,bar(k2));
id,once AB(x1?,bar(l7),x2?) = -AB(x1,bar(k1),x2)-AB(x1,bar(k2),x2);

id AB(p1?ps,bar(k1?),p2?ps) = AB(p1,k1,p2);

#call cancel

*** on-shell conditions ***
id dotp(k1,k1) = 0;
id dotp(bar(k1),bar(k1)) = 0;
id dotp(k1,p1) = 0;
id dotp(bar(k1),p1) = 0;
id dotp(k1,p2) = s12/2;
id dotp(bar(k1),p2) = s12/2;

id dotp(k2,k2) = 0;
id dotp(bar(k2),bar(k2)) = 0;
id dotp(k2,p4) = 0;
id dotp(bar(k2),p4) = 0;
id dotp(k2,p3) = s12/2;
id dotp(bar(k2),p3) = s12/2;

id dotp(k1,k2) = 0;
id dotp(bar(k1),bar(k2)) = 0;
id dotp(k1,bar(k2)) = (m33-m11-m22)/2;
id dotp(bar(k1),k2) = (m33-m11-m22)/2;

id s13 = -s12-s14;
endrepeat;
.sort

#call cancel
.sort

id A(x1?,p3)*B(p3,x2?) = -A(x1,p1)*B(p1,x2)-A(x1,p2)*B(p2,x2)-A(x1,p4)*B(p4,x2);
#call cancel

id A(p1?ps,p3)*IA(p2?ps,p3) = -B(P124-p1-p2,p2)*IB(P124-p1-p2,p1);
id B(p1?ps,p3)*IB(p2?ps,p3) = -A(P124-p1-p2,p2)*IA(P124-p1-p2,p1);
multiply replace_(P124,p1+p2+p4);

id A(p1?ps,p2?ps)*B(p2?ps,p1?ps) = S(p1,p2);
id S(p2,p4) = -S(p1,p2)-S(p1,p4);
#call cancel
id dotp(x1?,x2?) = dot(x1,x2);
*if(count(Ds,1)<1);
*discard;
*endif;
*id Ds=1;

topolynomial;
.sort
format maple;
#write <C7_dboxD_INT.mpl> "A4g_dblbox:=proc(k1,k2,m11,m12,m22) local Z,m33,ans,s12,s13,s14,i;\n"
#write <C7_dboxD_INT.mpl> "m33:=m11+m12+m22:"
#write <C7_dboxD_INT.mpl> "s12:=S(p1,p2):"
#write <C7_dboxD_INT.mpl> "s13:=S(p1,p3):"
#write <C7_dboxD_INT.mpl> "s14:=S(p1,p4):"
#write <C7_dboxD_INT.mpl> "i:=I:"
#write <C7_dboxD_INT.mpl> "Z:=Vector(200):\n"
#write <C7_dboxD_INT.mpl> "%X"
#write <C7_dboxD_INT.mpl> "ans:=\n%E:\n" AMP
#write <C7_dboxD_INT.mpl> "end:"

frompolynomial;
*B epsp,epsm,AB;
*format 150;
*print+s;
.sort
B V;
print[];
.sort

repeat;
#call subsL
#call cancel
endrepeat;
.sort

#call simplify

id 1/m11 = INV(cm11);
id 1/m22 = INV(cm22);
id 1/m33 = INV(cm11+cm12+cm22);
id m11 = cm11;
id m22 = cm22;
id m33 = cm11+cm12+cm22;

#call simplify
.sort
#write <out.prc> "G AMP = %e" AMP;

B tau1,tau2,tau3,tau4,INV;
format 150;
print[];
.end
