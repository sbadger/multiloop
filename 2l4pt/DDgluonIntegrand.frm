* vim: set sw=2:
* vim: set ft=form:
* vim: set foldmethod=marker:
#-
#define treepath "../amplib"
#include- ../lib/GU.hdr
#include- ../lib/factorise.h

CF REF,EPS,V3,G(s),VEC,dot(s),PRP;
CF MASS;
S l1,...,l10,k1,k2;
auto S mu,nu;

S rt2;

set ls:l1,...,l7,k1,k2;

#procedure lexpand
repeat;
id,once G(mu1?,mu2?)*G(mu2?,mu3?) = G(mu1,mu3);
id,once G(mu1?,mu2?)*VEC(p1?,mu2?) = VEC(p1,mu1);
id,once G(mu1?,mu2?)*EPS(p1?,mu2?) = EPS(p1,mu1);
endrepeat;
id G(nu1?,nu1?) = 4;

id VEC(p1?,mu1?)*VEC(p2?,mu1?) = dot(p1,p2);
id EPS(p1?,mu1?)*VEC(p2?,mu1?) = dot(EPS(p1),p2);
id EPS(p1?,mu1?)*EPS(p2?,mu1?) = dot(EPS(p1),EPS(p2));

normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?,EPS(p1?)) = 0;

id dot(l1,px?) = dot(k1,px);
id dot(l2,px?) = dot(k1,px)-dot(p1,px);
id dot(l3,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px);
id dot(l4,px?) = -dot(k2,px)+dot(p3,px)+dot(p4,px);
id dot(l5,px?) = -dot(k2,px)+dot(p4,px);
id dot(l6,px?) = -dot(k2,px);
id dot(l7,px?) = -dot(k1,px)-dot(k2,px);

id dot(l1,px?) = dot(k1,px);
id dot(l2,px?) = dot(k1,px)-dot(p1,px);
id dot(l3,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px);
id dot(l4,px?) = -dot(k2,px)+dot(p3,px)+dot(p4,px);
id dot(l5,px?) = -dot(k2,px)+dot(p4,px);
id dot(l6,px?) = -dot(k2,px);
id dot(l7,px?) = -dot(k1,px)-dot(k2,px);

id dot(p1?ps,p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;

.sort

id dot(k1,p3) = -dot(k1,p1)-dot(k1,p2)-dot(k1,p4);
id dot(k2,p2) = -dot(k2,p1)-dot(k2,p3)-dot(k2,p4);
id dot(k1,p1) = 0; * + props ...
id dot(k1,p2) = S(p1,p2)/2;
id dot(k2,p3) = 0;
id dot(k2,p4) = S(p1,p2)/2;

.sort

id dot(k1,k1)=mu11;
id dot(k2,k2)=mu22;
id dot(k1,k2)=mu12/2;

.sort

#call orderMOM
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id S(p3,p4) = S(p1,p2);
#endprocedure

off statistics;
.global

L FDdbox = EPS(p1,mu1)*EPS(p2,mu2)*EPS(p3,mu3)*EPS(p4,mu4)*(

  V3(nu1,mu1,nu2,-l1,p1,l2)*PRP(nu2,nu3,l2)*
  V3(nu3,mu2,nu4,-l2,p2,l3)*PRP(nu4,nu5,l3)*
  V3(nu5,nu6,nu13,-l3,l4,-l7)*PRP(nu6,nu7,l4)*
  V3(nu7,mu3,nu8,-l4,p3,l5)*PRP(nu8,nu9,l5)*
  V3(nu9,mu4,nu10,-l5,p4,l6)*PRP(nu10,nu11,l6)*
  V3(nu11,nu12,nu14,-l6,l1,l7)*PRP(nu12,nu1,l1)*PRP(nu13,nu14,l7)

);

#do i=1,7

id,once V3(mu1?,mu2?,mu3?,p1?,p2?,p3?) =
  + G(mu1,mu2)*( VEC(p1,mu3)-VEC(p2,mu3) )/rt2
  + G(mu2,mu3)*( VEC(p2,mu1)-VEC(p3,mu1) )/rt2
  + G(mu3,mu1)*( VEC(p3,mu2)-VEC(p1,mu2) )/rt2
;

id,once PRP(nu1?,nu2?,p1?) = G(nu1,nu2)+VEC(p1,nu1)*VEC(p1,nu2)*MASS(p1);

#call lexpand

#enddo

id MASS(l1) = 1/mu11;
id MASS(l2) = 1/mu11;
id MASS(l3) = 1/mu11;
id MASS(l4) = 1/mu22;
id MASS(l5) = 1/mu22;
id MASS(l6) = 1/mu22;
id MASS(l7) = 1/mu33;

AB mu11,mu12,mu22,mu33;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
argument NUM;
id mu12=mu33-mu11-mu22;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

B NUM,mu11,mu12,mu22,mu33;
format 150;
print[];
.sort

*** ++++ helicity ***

argument S,dot;
id EPS(p1) = EPS(p1,1);
id EPS(p2) = EPS(p2,1);
id EPS(p3) = EPS(p3,1);
id EPS(p4) = EPS(p4,1);
endargument;

id dot(EPS(p1?,1),p2?ps) = AB(REF(p1),p2,p1)*IA(REF(p1),p1)/rt2;
id dot(p2?ps,EPS(p1?,1)) = AB(REF(p1),p2,p1)*IA(REF(p1),p1)/rt2;
id dot(EPS(p1?,1),l1?ls) = AB(REF(p1),l1,p1)*IA(REF(p1),p1)/rt2;
id dot(l1?ls,EPS(p1?,1)) = AB(REF(p1),l1,p1)*IA(REF(p1),p1)/rt2;
id dot(EPS(p1?,1),EPS(p2?,1)) = -B(p1,p2)*A(REF(p1),REF(p2))*IA(REF(p1),p1)*IA(REF(p2),p2);
id dot(l1?ls,p1?ps) = AB(p1,l1,p1)/2;

id 1/rt2^2=1/2;
id rt2^2=2;

format 150;
B mu11,mu12,mu22;
print+s;
.sort
B mu11,mu12,mu22;
print[];
.sort

argument AB,IA,IB,A,B;
id REF(p1) = p3;
id REF(p2) = p3;
id REF(p3) = p1;
id REF(p4) = p1;
endargument;

#call cancel
#call compress(0)
#call breakstrings

id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);

#call cancel
format 150;
B mu11,mu12,mu22,mu33;
print+s;
.sort
B mu11,mu12,mu22,mu33;
print[];
.sort

id NUM(x?) = x;

AB A,B,IA,IB;
.sort
collect NUM;
#call SortCollect
id NUM(x?) = x;
id AB(p1?,p2?ps,p1?) = S(p1,p2);
#call orderMOM
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id S(p3,p4) = S(p1,p2);

id S(p1,p3) =  -S(p1,p2)-S(p1,p4);
id AB(px?,p4,py?) = -AB(px,p1,py)-AB(px,p2,py)-AB(px,p3,py);
#call cancel
#call compress(0)
#call breakstrings
#call cancel
.sort
#call orderMOM
id AB(p1,MOM(p2,p4),p3) = 0;
id AB(p3,MOM(p2,p4),p1) = 0;
id AB(p1,MOM(p2,p3),p4) = 0;
id AB(p4,MOM(p2,p3),p1) = 0;

*topolynomial;
*B mu11,mu12,mu22,mu33;
*.sort
*collect NUM;
*factarg NUM;
*chainout NUM;
*splitarg NUM;
*id NUM(x?) = x;
*repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
*.sort
*format maple;
*#write <C7.planarD.INT.log> "C7_1_INT_D:=proc(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4)"
*#write <C7.planarD.INT.log> "%X"
*#write <C7.planarD.INT.log> "return %e" FDdbox
*#write <C7.planarD.INT.log> "end:"

*.sort
*id NUM(xx?) = xx;
*frompolynomial;

format 150;
B mu11,mu12,mu22,AB,A,B;
print+s;
.sort
B xxx;
print[];
.end
