#-
auto S x,s,p,k,l,a,b,Dmu,mu,nu,rho,omega;

CF A(a),B(a),IA(a),IB(a);
CF eta,AB,IAB,MOM,dot(s),S(s),IS(s);

CF cc,Power,NUM,INV;
CF EPS,GGG(s);

CF cs,ds;

S rt2,I,Ds,[Ds-2],m;

auto S tau,Basis;

set ps:p1,...,p4;
set Dmu2:Dmu11,Dmu12,Dmu22,Dmu33;
set mus:mu1,...,mu20,nu1,...,nu20,rho1,...,rho20;

#procedure cancel
id AB(p1?,MOM,p2?) = 0;

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id AB(?x)*IAB(?x) = 1;
id NUM(?x)*INV(?x) = 1;
#endprocedure

#procedure Lcontract

id MOM(-p1?,?x) = -MOM(p1,?x);

id EPS(1,p1?,x1?,nu1?) = AB(x1,nu1,p1)*IA(x1,p1)/rt2;
id EPS(-1,p1?,x1?,nu1?) = AB(p1,nu1,x1)*IB(p1,x1)/rt2;

id AB(p1?,nu1?mus,p3?)*GGG(nu1?mus,mu2?mus) = AB(p1,mu2,p3);
id AB(p1?,nu1?mus,p3?)*MOM(p2?,nu1?mus) = AB(p1,p2,p3);
id AB(p1?,nu1?mus,p2?)*AB(p3?,nu1?mus,p4?) = -2*A(p1,p3)*B(p2,p4);

#do fun={EPS,MOM}
id 'fun'(?x,nu1?)*GGG(nu1?,mu2?) = 'fun'(?x,mu2);
#enddo
id GGG(nu1?,mu2?)*GGG(mu2?,mu3?) = GGG(nu1,mu3);
id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id GGG(nu1?,nu1?) = Ds;

id I^2=1;
id rt2^2 = 2;
id 1/rt2^2 = 1/2;

#endprocedure

#procedure subsLmom(path,topo)

#do rk={1,5,9,13,17}
id,once dot(k1,px?) = MOM(k1,mu{'rk'})*MOM(px,mu{'rk'});
id,once AB(p1?,k1,p2?) = AB(p1,mu{'rk'+1},p2)*MOM(k1,mu{'rk'+1});
id,once dot(k2,px?) = MOM(k2,mu{'rk'+2})*MOM(px,mu{'rk'+2});
id,once AB(p1?,k2,p2?) = AB(p1,mu{'rk'+3},p2)*MOM(k2,mu{'rk'+3});
#enddo

#if 'topo'=db220

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*B(p1,p3)*IB(p2,p3)*AB(p1,mu1,p2)/2+a4*A(p1,p3)*IA(p2,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p3,mu1)+b2*MOM(p4,mu1)+b3*B(p3,p1)*IB(p4,p1)*AB(p3,mu1,p4)/2+b4*A(p3,p1)*IA(p4,p1)*AB(p4,mu1,p3)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s12*s13/s14*tau1*tau2;
id mu22 = -s12*s13/s14*tau3*tau4;
id mu12 = (-tau1-tau2+tau3+tau4+tau2*tau3+tau1*tau4)*s13
        - (tau2*tau4+tau1*tau3)*s13^2/s14
        + s14;

#elseif 'topo'=bt22

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*B(p1,p3)*IB(p2,p3)*AB(p1,mu1,p2)/2+a4*A(p1,p3)*IA(p2,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p3,mu1)+b2*MOM(p4,mu1)+b3*B(p3,p1)*IB(p4,p1)*AB(p3,mu1,p4)/2+b4*A(p3,p1)*IA(p4,p1)*AB(p4,mu1,p3)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s12*s13/s14*tau1*tau2;
id mu22 = -s12*s13/s14*tau3*tau4;
id mu12 = tau5;

#elseif 'topo'=db210

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*AB(p1,mu1,p2)/2+a4*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*AB(p1,mu1,p2)/2+b4*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = -(1+tau3);
id b2 = tau3;
id b3 = tau4;
id b4 = tau5;

id mu11 = -tau1*tau2*s12;
id mu22 = -(tau3*(1+tau3) + tau4*tau5)*s12;
id mu12 = (tau3 - tau1*tau5 - tau2*tau4)*s12;

#elseif 'topo'=bt21

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*AB(p1,mu1,p2)/2+a4*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*AB(p1,mu1,p2)/2+b4*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = -(1+tau3);
id b2 = tau3;
id b3 = tau4;
id b4 = tau5;

id mu11 = -tau1*tau2*s12;
id mu22 = -(tau3*(1+tau3) + tau4*tau5)*s12;
id mu12 = tau6;

#endif

#call Lcontract

#endprocedure

#procedure simplify4
id s12 = A(p1,p2)*B(p2,p1);
id s13 = A(p1,p3)*B(p3,p1);
id s14 = A(p1,p4)*B(p4,p1);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
#call cancel
repeat;
id A(p2,p4) = -s13/s14*A(p1,p4)*A(p2,p3)*IA(p1,p3);
id IA(p2,p4) = -s14/s13*IA(p1,p4)*IA(p2,p3)*A(p1,p3);
id A(p3,p4) = s12/s14*A(p1,p4)*A(p2,p3)*IA(p1,p2);
id IA(p3,p4) = s14/s12*IA(p1,p4)*IA(p2,p3)*A(p1,p2);
id B(p1,p2) = -s12*IA(p1,p2);
id B(p3,p4) = -s12*IA(p3,p4);
id B(p1,p4) = -s14*IA(p1,p4);
id B(p2,p3) = -s14*IA(p2,p3);
id B(p2,p4) = -s13*IA(p2,p4);
id B(p1,p3) = -s13*IA(p1,p3);
id IB(p1,p2) = -1/s12*A(p1,p2);
id IB(p3,p4) = -1/s12*A(p3,p4);
id IB(p1,p4) = -1/s14*A(p1,p4);
id IB(p2,p3) = -1/s14*A(p2,p3);
id IB(p2,p4) = -1/s13*A(p2,p4);
id IB(p1,p3) = -1/s13*A(p1,p3);
#call cancel
endrepeat;

argument INV;
id s12 = A(p1,p2)*B(p2,p1);
id s13 = A(p1,p3)*B(p3,p1);
id s14 = A(p1,p4)*B(p4,p1);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
#call cancel
repeat;
id A(p2,p4) = -s13/s14*A(p1,p4)*A(p2,p3)*IA(p1,p3);
id IA(p2,p4) = -s14/s13*IA(p1,p4)*IA(p2,p3)*A(p1,p3);
id A(p3,p4) = s12/s14*A(p1,p4)*A(p2,p3)*IA(p1,p2);
id IA(p3,p4) = s14/s12*IA(p1,p4)*IA(p2,p3)*A(p1,p2);
id B(p1,p2) = -s12*IA(p1,p2);
id B(p3,p4) = -s12*IA(p3,p4);
id B(p1,p4) = -s14*IA(p1,p4);
id B(p2,p3) = -s14*IA(p2,p3);
id B(p2,p4) = -s13*IA(p2,p4);
id B(p1,p3) = -s13*IA(p1,p3);
id IB(p1,p2) = -1/s12*A(p1,p2);
id IB(p3,p4) = -1/s12*A(p3,p4);
id IB(p1,p4) = -1/s14*A(p1,p4);
id IB(p2,p3) = -1/s14*A(p2,p3);
id IB(p2,p4) = -1/s13*A(p2,p4);
id IB(p1,p3) = -1/s13*A(p1,p3);
#call cancel
endrepeat;
endargument;

AB s12,s13,s14;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id s13 = -s12-s14;
argument;
id s13 = -s12-s14;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(s12+s14) = -s13;

#call cancel

#endprocedure

off statistics;

