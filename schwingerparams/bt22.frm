#-

auto S p,x;
S s12,s14;
S k1,k2;
S mu11,mu12,mu33,mu22;
S a,b,c,d,e,f;
S D,eps;

CF I,NUM,INV;

off stats;

.global

G P1 = k1;
G P2 = k1-p1;
G P3 = k1-p12;
G P4 = k2;
G P5 = k2-p4;
G P6 = k2+p12;

.store

G QQ =
#do i=1,6
+x'i'*P'i'^2
#enddo
;

id p1^2=0;
id p12^2=s12;
id p4^2=0;

B k1,k2;
.sort
collect NUM;
id k1^2*NUM(xx?$a) = k1^2*a;
id k2^2*NUM(xx?$b) = k2^2*b;
*id k1*k2*NUM(xx?$c) = k1*k2*c;
*** for bow-tie topologies c=0
#$c=0;
id k1*NUM(xx?$d) = k1*d;
id k2*NUM(xx?$e) = k2*e;
id NUM(xx?$f) = f;

id k1 = k1 - c*INV(a)*k2/2 + (c*e/4-b*d/2)*INV(a*b-c^2/4);
al k2 = k2 + (c*d/4-a*e/2)*INV(a*b-c^2/4);

B k1,k2,INV;
.sort
collect NUM;
factarg NUM;
factarg INV;
chainout NUM;
chainout INV;
id NUM(xx?)*INV(xx?) = 1;
splitarg INV,NUM;
id NUM(xx?) = xx;
id INV(xx?) = 1/xx;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

id k1^2 = 0;
id k2^2 = 0;
multiply -NUM(c^2-4*a*b)/4;
id NUM(xx?)*INV(xx?) = 1;
id NUM(xx?) = xx;
id a = $a;
id b = $b;
id c = $c;
id d = $d;
id e = $e;
id f = $f;

id p1*p12 = s12/2;
id p12^2 = s12;
id p4*p12 = -s12/2;
id p1^2 = 0;
id p1*p4 = s14/2;
id p4^2 = 0;

.sort

G T1 = $a-$c/2;
G T2 = $b-$c/2;
G T3 = $c/2;

print+s;
.store

L [mu11*mu22] = mu11*mu22;

multiply I(D,1,1,1,1,1,1);

#do mu={mu11,mu22}
id 'mu'^x? = NUM('mu',x);
repeat;
id NUM('mu',0) = 1;
id NUM('mu',x?)*I(D?,?pows) = 'mu'*NUM('mu',x-1)*(x-1-eps)*I(D+2,?pows);
endrepeat;
#enddo

id mu11 = T2+T3;
id mu33 = T1+T2;
id mu22 = T1+T3;

repeat id x1*I(x?,p1?,?args) = I(x,p1+1,?args);
#do n=2,8
repeat id x'n'*I(x?,p1?,...,p'n'?,?args) = I(x,p1,...,p{'n'-1},p'n'+1,?args);
#enddo

AB eps;
.sort
collect NUM;
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(xx?) = xx;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

format 150;
print+s;
.end
