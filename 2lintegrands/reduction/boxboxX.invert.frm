* vim: set foldmethod=marker:
#-
S Omega;
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,p,x,P,h,k,e,q,c;
S mS;
S k1f,k2f,k3f,k4f,k1,k2;
S mu;
S l1,...,l20;

auto S Basis;
CF Power,cc,DP;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}
*{{{ simplify3
#procedure simplify3

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS

id AB(p2,P1,p3) = 0;
id AB(p3,P1,p2) = 0;
id S(p2,p3) = S(P1);
id AB(p2,P1,p2) = -S(P1);

#endprocedure
*}}}
*{{{ simplify4
#procedure simplify4

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p4)*IA(p2?,p4) = -B(p2,p3)*IB(p1,p3);
id B(p1?,p4)*IB(p2?,p4) = -A(p2,p3)*IA(p1,p3);
id A(px?,p4)*B(p4,py?!{px}) = -AB(px,MOM(p1,p2,p3),py);
#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS
id AB(p1,MOM(p2,p3),p1) = -S(p1,p4);
id AB(p2,MOM(p1,p3),p2) = -S(p1,p3);
id AB(p3,MOM(p1,p2),p3) = -S(p1,p2);
id S(p3,p4) = S(p1,p2);
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);

#endprocedure
*}}}

off stats;

#define topo "boxbox"
.global

#do i=1,7
G L'i' = l'i';
#enddo
#include- ../OSsol/final/'topo'.OSprops.h
.store
#do sol=1,6
G K1'sol' = k1'sol';
G K2'sol' = k2'sol';
#enddo
#include- ../OSsol/final/'topo'.OSsol.final.h
.store

G Delta = Basis'topo';
#include- 'topo'.basis.h
id Power(x1?,x2?) = x1^x2;
.store

#do sol=1,6

L Delta'sol' = Delta;

multiply replace_(k1,K1'sol');
multiply replace_(k2,K2'sol');
.sort

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,4
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
id Omega = ( AB(p2,p3,p1)*AB(p1,mu,p2) - AB(p1,p3,p2)*AB(p2,mu,p1) )/2;
#call lprod

endrepeat;

id NUM(x?) = x;
#call sortINV
#call cancel
#call simplify4
#call cancel
id S(p1,p3)=-S(p1,p2)-S(p1,p4);

B tau;
print[];
.store

#enddo

.end
