
Get["../../autobasisv1/BasisDet-1-02.m"];

(* pentagon-box *)
L = 2; Dim = 4; n = 5;
ExternalMomentaBasis = {p1, p2, p3, p5};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1 p2 -> s12/2, p1 p3 -> (s123 - s12 - s23)/2, p1 p5 -> s15/2,
   p2 p3 -> s23/2, p2 p5 -> (s125 - s15 - s12)/2,
   p3 p5 -> (s12 - s123 - s125)/2,
   \[Omega]1^2 -> -s12*(s12 + s23)/s12};
numeric = {s12 -> 11, s123 -> 3, s15 -> 17, s23 -> 19, s125 -> 29};
Props = {l1, l1 - p1, l1 - p1 - p2, l1 - p1 - p2 - p3, l2, l2 - p5, l2 + p1 + p2 + p3, l1 + l2};
RenormalizationCondition = {{{1, 0}, 5}, {{0, 1}, 4}, {{1, 1}, 7}};
GenerateBasis[0]

ISP
Basis
Integrand = 0;
For[j = 1, j <= Length[Basis], j++,
  ThisTerm = cc[]*nc[j];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm*c[ i, Basis[[j, i]]]*ISP[[i]]^Basis[[j, i]];
   ];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm //. cc[x1___]*c[i, x2_] -> cc[x1, x2]
   ];
  Integrand = Integrand + ThisTerm;
  ];

x14=DP[L1,p5];
x21=DP[-L7,p1];
x22=DP[-L7,p2];

fout = OpenWrite["pentagonbox.basis.h"];
WriteString[fout,"id Basispentagonbox = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];
