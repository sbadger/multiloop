interface(quiet=true):
#interface(rtablesize=infinity):
with(LinearAlgebra):

read("matrix_bub.mpl"):

eqsP,eqsL,eqsU:=LUDecomposition(eqs);
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL));
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[10..24])):

Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dvec:

dvecR:=SubVector(dvecT,[1..9]):
csol:=simplify(Ieqs.dvecR);

fd:=fopen("bub.d2c.mpl",WRITE):
for k from 1 to 9 do:
tmp:=simplify(csol[k]);
fprintf(fd,"print(%A,%A):\n",cs[k],tmp):
od:

for k from 10 to 24 do:
fprintf(fd,"print(%A);\n",simplify(dvecT[k])):
od:

fclose(fd):

for s from 1 to 2 do:
  for tt1 from -2 to 2 do:
    for tt2 from 0 to 4 do:
      ds[s,tt1,tt2]:=cat('d',s,tt1+2,tt2):
    od:
  od:
od:

fd:=fopen("bub.d2c.h",WRITE):
for k from 1 to 9 do:
tmp:=simplify(csol[k]);
fprintf(fd,"L %A = %A;\n",cs[k],tmp):
od:

for k from 10 to 24 do:
fprintf(fd,"L zero%d = %A;\n",k-9,simplify(dvecT[k])):
od:

fclose(fd):

unassign('ds'):
