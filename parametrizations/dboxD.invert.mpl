interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

s13:=-s12-s14;

# solving planar double box in 4-2e dimensions

read("dboxD.reduceM.log"):
unassign('Dvec'):

dvec:=Vector(160):
for k from 1 to 160 do:
dvec[k]:=Dvec[k]:
od:

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(eqsU):
Ieqs:=MatrixInverse(eqsSq):

dvecT:=dtransform.dvec:

dvecR:=dvecT:
Cvec:=Ieqs.dvecR:

fd:=fopen("final/dboxD.integrand.mpl",WRITE):
fprintf(fd,"Cvec:=Vector(160):\n"):
for k from 1 to 160 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,simplify(Cvec[k])):
od:
fclose(fd):

read("final/dboxD.coeffvecs.mpl"):

integrand:=0:
for i from 1 to 160 do:
integrand:=integrand+Clbl[i]*Cvec[i];
od;

fd:=fopen("final/dboxD.integrand.prc",WRITE):
fprintf(fd,"L INTdboxD = \n%A;\n",integrand):
fclose(fd):

