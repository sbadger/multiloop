* vim: set foldmethod=marker:
#-
#include ../2to3tools.prc
#include diagrams.prc

auto CF INTdb, INTbt, Delta;
CF F1,F2,F3,F4,ww,MU,trp,trm;
S Dsm2;
S w,w123,w543,w132,w534,w12p,w12m,w45p,w45m,w13p,w13m,w53p,w53m;

off stats;
.global

#procedure signs

*** diagrams for pentagon-box: db431
id xxdiag1  =  1;
id xxdiag2  =  2;
id xxdiag3  = -2;
id xxdiag4  =  2;
*** diagrams for box-pentagon: db341
id xxdiag5  =  1;
id xxdiag6  = -2;
id xxdiag7  =  2;
id xxdiag8  =  2;
*** diagrams for triangle-box butterfly : bt340
id xxdiag22 =  1;
id xxdiag23 =  2;
id xxdiag24 = -2;
id xxdiag25 = -4;
id xxdiag26 = -1;
id xxdiag27 =  1/2;

*** diagrams for pentagon-box: db332
id xxnpdiag1  =  1;
id xxnpdiag2  =  2;
id xxnpdiag3  =  2;
id xxnpdiag4  =  -2;
*** diagrams for pentagon-box: db422
id xxnpdiag5  =  1;
id xxnpdiag6  =  -2;
id xxnpdiag7  =  -2;
id xxnpdiag8  =  -2;
*** diagrams for "non-planar" double-box: npdb331M1
id xxnpdiag9  =  -1;
*** diagrams for "non-planar" double-box: npdb331M2
id xxnpdiag10  =  -1;
*** diagrams for "non-planar" double-box: npdb331M3
id xxnpdiag17  =  -1;
*** diagrams for double-box: db232
id xxnpdiag11  =  1;
id xxnpdiag12  =  -2;
id xxnpdiag13  =  2;
id xxnpdiag14  =  2;
id xxnpdiag15  =  -1;
id xxnpdiag16  =  -2;
*** diagrams for "non-planar" bow-tie: npbt3305L
id xxnpdiag18  =  -1;
id xxnpdiag19  =  -2;

*** diagrams for reordered box-pentagon: db341(k1,k2+p3,p1,p2,p4,p5,p3)
id xxALTdiag1  =  1;
id xxALTdiag2  = -2;
id xxALTdiag3  =  2;
id xxALTdiag4  =  2;
*** diagrams for reordered triangle-box: db340(k1,k2+p3,p1,p2,p4,p5,p3)
id xxALTdiag5  =  1;
id xxALTdiag6  = 2;
id xxALTdiag7  = -2;
id xxALTdiag8  =-4;
id xxALTdiag9  = -1;
id xxALTdiag10 = 1/2;

#endprocedure

#define rerun "1"
#define dd "0"
#define ALTdd "0"

#if 'rerun'=1

*** diagrams for reordered box-pentagon: db341(k1,k2+p3,p1,p2,p4,p5,p3)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq3, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq3, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq3, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq3, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for reordered triangle-box butterfly : bt340(k1,k2+p3,p1,p2,p4,p5,p3)
*{{{
#define f1 "0"
#define f2 "0"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(p12, rho8, 0), P(-qq45, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(p12, rho8, 0), P(-qq45, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "1"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(p12, rho8, 0), P(-qq45, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qqp3, nu7, 'f2'), P(p12, rho8, 0), P(-qq45, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine ALTdd "{'ALTdd'+1}"
G ALTdiag'ALTdd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(qqp3, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p3, mu3, 0), P(-qqp3, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;
*}}}
*** diagrams for pentagon-box: db332
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for pentagon-box: db422
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
*}}}
*** diagram for double-box: db331 (M1)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq, nu7, 'f3'), P(p3, mu3, 0))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*Prop(rho7, nu7, 'f3')
;
*}}}
*** diagram for double-box: db331 (M2)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(p3, mu3, 0), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*Prop(rho7, nu7, 'f3')
;
*}}}
*** diagrams for triangle-box: db232
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p12, rho2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
      V(P(-p12, nu2, 0), P(p1, mu1, 0), P(p2, mu2, 0))*Prop(rho2, nu2, 0)/s12*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p12, rho2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
      V(P(-p12, nu2, 0), P(p1, mu1, 0), P(p2, mu2, 0))*Prop(rho2, nu2, 0)/s12*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p12, rho2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
      V(P(-p12, nu2, 0), P(p1, mu1, 0), P(p2, mu2, 0))*Prop(rho2, nu2, 0)/s12*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p12, rho2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
      V(P(-p12, nu2, 0), P(p1, mu1, 0), P(p2, mu2, 0))*Prop(rho2, nu2, 0)/s12*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq3, rho7, 'f3'), P(p3, mu3, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
*}}}
*** diagram for double-box: db331 (M3)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq45, rho4, 'f2'), P(kq3, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq5, nu5, 'f2'), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq3, rho7, 'f3'), P(p3, mu3, 0))*Prop(rho1, nu1, 'f1')*Prop(rho7, nu7, 'f3')
;
*}}}
*** diagrams for : bow-tie npbt330;5L
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    Prop(rho4, nu4, 0)/s12*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho4, 0), P(-qq45, rho5, 'f2'), P(p3, mu3, 0))*Prop(rho5, nu5, 'f2')
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    Prop(rho4, nu4, 0)/s12*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho4, 0), P(-qq45, rho5, 'f2'), P(p3, mu3, 0))*Prop(rho5, nu5, 'f2')
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G npdiag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    Prop(rho4, nu4, 0)/s12*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho4, 0), P(-qq45, rho5, 'f2'), P(p3, mu3, 0))*Prop(rho5, nu5, 'f2')
;
*}}}

#call expandtopo
multiply EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5);
*multiply EPS(1,p1,p5,mu1)*EPS(1,p2,p1,mu2)*EPS(1,p3,p1,mu3)*EPS(1,p4,p1,mu4)*EPS(1,p5,p1,mu5);

#call subvertices
#call cancel

.sort

argument AB,dot,S,IS;
id kk = k1;
id kk1 = MOM(k1,-p1);
id kk12 = MOM(k1,-p1,-p2);
id kk123 = MOM(k1,-p1,-p2,-p3);
id p123 = MOM(p1,p2,p3);
id p12 = MOM(p1,p2);
id p23 = MOM(p2,p3);
id p45 = MOM(p4,p5);
id qq = k2;
id qqp3 = MOM(k2,p3);
id qq4 = MOM(k2,-p4);
id qq5 = MOM(k2,-p5);
id qq34 = MOM(k2,-p3,-p4);
id qq45 = MOM(k2,-p4,-p5);
id qq345 = MOM(k2,-p3,-p4,-p5);
id kq = MOM(k1,k2);
id kq5 = MOM(k1,k2,p5);
id kq3 = MOM(k1,k2,p3);
endargument;

*{{{ expand and simplify ids

id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
repeat id AB(p1?,MOM(k1?{k1,k2},?xx),p3?) = AB(p1,k1,p3)+AB(p1,MOM(?xx),p3);

argument AB;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
id AB(p1?,-MOM(?x1),p2?) = -AB(p1,MOM(?x1),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
id AB(p1?,MOM,p3?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

repeat id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
repeat id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
#call cancel

repeat id dot(MOM(p1?),xx?) = dot(p1,xx);

id dot(MOM(k1?{k1,k2},?x1),MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2)) + dot(k2,MOM(?x1)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),MOM(?x2)) = dot(k1,MOM(?x2)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),p1?) = dot(k1,p1) + dot(MOM(?x1),p1);
id dot(k1?{k1,k2},MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2));

argument dot;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
id dot(p1?ps,MOM(p2?,?x)) = dot(p1,p2) + dot(p1,MOM(?x));
id dot(p1?ps,MOM(?x1,p1?ps,?x2)) = dot(p1,MOM(?x1,?x2));
id dot(p1?ps,MOM(p1?ps,?x2)) = dot(p1,MOM(?x2));
id dot(p1?ps,MOM(p1?ps,p2?)) = dot(p1,p2);
id dot(MOM,x?) = 0;
argument dot;
repeat id MOM(p1?,?x) = p1 + MOM(?x);
id MOM = 0;
endargument;
id dot(p1?ps,p1?ps) = 0;
id dot(p?,p?) = S(p);
id IS(MOM(?x)) = IS(?x);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;

id dot(px?,p1+p2+p3) = -dot(px,p4+p5);
id dot(px?,p3+p4+p5) = -dot(px,p1+p2);
id dot(p?,p?) = S(p);
splitarg S,IS;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
.sort

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

id AB(px1?,MOM(px2?,px3?,px4?),px5?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

#call cancel
id IS(p1,p2,p3) = 1/s45;
id IS(p3,p4,p5) = 1/s12;
id dot(p1+p2,p4+p5) = -(s12+s45)/2;
id dot(p1+p2,p3+p4) = -(s12+s34)/2;
id S(k1?{k1,k2}) = dot(k1,k1);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id dot(p1?ps,p2?) = dot(p1,MOM(p2));
argument dot;
splitarg MOM;
endargument;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

AB s12,s23,s34,s45,s15,s13,s14,s24,s25,s35;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument INV,NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

#do i=1,5
#do j=1,5
#if 'j'>'i'
  id s'i''j' = S(p'i',p'j');
  id 1/s'i''j' = IS(p'i',p'j');
#endif
#enddo
#enddo
argument NUM,INV;
#do i=1,5
#do j=1,5
#if 'j'>'i'
  id s'i''j' = S(p'i',p'j');
  id 1/s'i''j' = IS(p'i',p'j');
#endif
#enddo
#enddo
endargument;


*}}}

.sort

#do i=1,'dd'
  #write <tmp/np_diagrams.prc> "G npdiag'i'(k1,k2,p1,p2,p3,p4,p5) = %e;" npdiag'i'
#enddo

#do i=1,'ALTdd'
  #write <tmp/np_diagrams.prc> "G ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5) = %e;" ALTdiag'i'
#enddo


#else
#include- tmp/np_diagrams.prc
#endif
#include- tmp/diagrams.prc

*B MOM,V;
*print[];
.store

L cutAMPdb332 =
#do i=1,4
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p3) = -dot(k1,p3);

topolynomial;
.sort
format maple;
#write <db332.input.mpl> "INT_332 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <db332.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db332.input.mpl> "  p1 := p[1];"
#write <db332.input.mpl> "  p2 := p[2];"
#write <db332.input.mpl> "  p3 := p[3];"
#write <db332.input.mpl> "  p4 := p[4];"
#write <db332.input.mpl> "  p5 := p[5];"
#write <db332.input.mpl> "  s12 := S(p1,p2);"
#write <db332.input.mpl> "  s13 := S(p1,p3);"
#write <db332.input.mpl> "  s14 := S(p1,p4);"
#write <db332.input.mpl> "  s15 := S(p1,p5);"
#write <db332.input.mpl> "  s23 := S(p2,p3);"
#write <db332.input.mpl> "  s24 := S(p2,p4);"
#write <db332.input.mpl> "  s25 := S(p2,p5);"
#write <db332.input.mpl> "  s34 := S(p3,p4);"
#write <db332.input.mpl> "  s35 := S(p3,p5);"
#write <db332.input.mpl> "  s45 := S(p4,p5);"
#write <db332.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db332.input.mpl> "%X"
#write <db332.input.mpl> "return %e;" cutAMPdb332
#write <db332.input.mpl> "end:"
frompolynomial;
.sort
format 150;
#write <db332.input.frm> "L INTdb332 = %e;" cutAMPdb332
.store

L cutAMPdb422 =
#do i=5,8
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p1+p2+p3) = s45/2;
id dot(k1,p3) = (s45-s12)/2;
id dot(k2,p4) = 0;
id dot(k2,p5) = -dot(k1,p5);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db422.input.mpl> "INT_422 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <db422.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db422.input.mpl> "  p1 := p[1];"
#write <db422.input.mpl> "  p2 := p[2];"
#write <db422.input.mpl> "  p3 := p[3];"
#write <db422.input.mpl> "  p4 := p[4];"
#write <db422.input.mpl> "  p5 := p[5];"
#write <db422.input.mpl> "  s12 := S(p1,p2);"
#write <db422.input.mpl> "  s13 := S(p1,p3);"
#write <db422.input.mpl> "  s14 := S(p1,p4);"
#write <db422.input.mpl> "  s15 := S(p1,p5);"
#write <db422.input.mpl> "  s23 := S(p2,p3);"
#write <db422.input.mpl> "  s24 := S(p2,p4);"
#write <db422.input.mpl> "  s25 := S(p2,p5);"
#write <db422.input.mpl> "  s34 := S(p3,p4);"
#write <db422.input.mpl> "  s35 := S(p3,p5);"
#write <db422.input.mpl> "  s45 := S(p4,p5);"
#write <db422.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db422.input.mpl> "%X"
#write <db422.input.mpl> "return %e;" cutAMPdb422
#write <db422.input.mpl> "end:"
frompolynomial;
.sort
format 150;
#write <db422.input.frm> "L INTdb422 = %e;" cutAMPdb422
.store

L cutAMPdbNP331M1 =
#do i=9,9
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
#do i=1,4
-xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
#enddo
#do i=1,4
-xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
#enddo
+ DeltaNP332(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
+ Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(kk123) = INV(S(k1-p1-p2-p3)-mu11);
id IS(kq3) = INV(S(k1+k2+p3)-mu11-mu12-mu22);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <dbNP331M1.input.mpl> "INT_NP331M1 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <dbNP331M1.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <dbNP331M1.input.mpl> "  p1 := p[1];"
#write <dbNP331M1.input.mpl> "  p2 := p[2];"
#write <dbNP331M1.input.mpl> "  p3 := p[3];"
#write <dbNP331M1.input.mpl> "  p4 := p[4];"
#write <dbNP331M1.input.mpl> "  p5 := p[5];"
#write <dbNP331M1.input.mpl> "  s12 := S(p1,p2);"
#write <dbNP331M1.input.mpl> "  s13 := S(p1,p3);"
#write <dbNP331M1.input.mpl> "  s14 := S(p1,p4);"
#write <dbNP331M1.input.mpl> "  s15 := S(p1,p5);"
#write <dbNP331M1.input.mpl> "  s23 := S(p2,p3);"
#write <dbNP331M1.input.mpl> "  s24 := S(p2,p4);"
#write <dbNP331M1.input.mpl> "  s25 := S(p2,p5);"
#write <dbNP331M1.input.mpl> "  s34 := S(p3,p4);"
#write <dbNP331M1.input.mpl> "  s35 := S(p3,p5);"
#write <dbNP331M1.input.mpl> "  s45 := S(p4,p5);"
#write <dbNP331M1.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <dbNP331M1.input.mpl> "%X"
#write <dbNP331M1.input.mpl> "return %e;" cutAMPdbNP331M1
#write <dbNP331M1.input.mpl> "end:"
.sort
frompolynomial;
.store

.store
L cutAMPdbNP331M2 =
#do i=10,10
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
#do i=5,8
-xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=1,4
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
#enddo
- DeltaNP332(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
- Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq345)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq345) = INV(S(k2-p3-p4-p5)-mu22);
id IS(kq3) = INV(S(k1+k2+p3)-mu11-mu12-mu22);
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <dbNP331M2.input.mpl> "INT_NP331M2 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <dbNP331M2.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <dbNP331M2.input.mpl> "  p1 := p[1];"
#write <dbNP331M2.input.mpl> "  p2 := p[2];"
#write <dbNP331M2.input.mpl> "  p3 := p[3];"
#write <dbNP331M2.input.mpl> "  p4 := p[4];"
#write <dbNP331M2.input.mpl> "  p5 := p[5];"
#write <dbNP331M2.input.mpl> "  s12 := S(p1,p2);"
#write <dbNP331M2.input.mpl> "  s13 := S(p1,p3);"
#write <dbNP331M2.input.mpl> "  s14 := S(p1,p4);"
#write <dbNP331M2.input.mpl> "  s15 := S(p1,p5);"
#write <dbNP331M2.input.mpl> "  s23 := S(p2,p3);"
#write <dbNP331M2.input.mpl> "  s24 := S(p2,p4);"
#write <dbNP331M2.input.mpl> "  s25 := S(p2,p5);"
#write <dbNP331M2.input.mpl> "  s34 := S(p3,p4);"
#write <dbNP331M2.input.mpl> "  s35 := S(p3,p5);"
#write <dbNP331M2.input.mpl> "  s45 := S(p4,p5);"
#write <dbNP331M2.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <dbNP331M2.input.mpl> "%X"
#write <dbNP331M2.input.mpl> "return %e;" cutAMPdbNP331M2
#write <dbNP331M2.input.mpl> "end:"
.sort
frompolynomial;
.store

L cutAMPdb341ALT =
#do i=1,4
+ xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
+ Delta431(k2+p3,k1,p3,p5,p4,p2,p1)
;
#call signs

repeat id dot(x?,qqp3) = dot(x,k2) + dot(x,p3);
repeat id AB(x?,qqp3,y?) = AB(x,k2,y) + AB(x,p3,y);
id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = -dot(k1,p3)-dot(k2,p3);

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qqp3) = INV(S(k2+p3)-mu22);
id IS(kq) = INV(S(k1+k2)-mu11-mu12-mu22);
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db341ALT.input.mpl> "INT_341ALT := proc(p, k1, k2, mu11, mu12, mu22)"
#write <db341ALT.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db341ALT.input.mpl> "  p1 := p[1];"
#write <db341ALT.input.mpl> "  p2 := p[2];"
#write <db341ALT.input.mpl> "  p3 := p[3];"
#write <db341ALT.input.mpl> "  p4 := p[4];"
#write <db341ALT.input.mpl> "  p5 := p[5];"
#write <db341ALT.input.mpl> "  s12 := S(p1,p2);"
#write <db341ALT.input.mpl> "  s13 := S(p1,p3);"
#write <db341ALT.input.mpl> "  s14 := S(p1,p4);"
#write <db341ALT.input.mpl> "  s15 := S(p1,p5);"
#write <db341ALT.input.mpl> "  s23 := S(p2,p3);"
#write <db341ALT.input.mpl> "  s24 := S(p2,p4);"
#write <db341ALT.input.mpl> "  s25 := S(p2,p5);"
#write <db341ALT.input.mpl> "  s34 := S(p3,p4);"
#write <db341ALT.input.mpl> "  s35 := S(p3,p5);"
#write <db341ALT.input.mpl> "  s45 := S(p4,p5);"
#write <db341ALT.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db341ALT.input.mpl> "%X"
#write <db341ALT.input.mpl> "return %e;" cutAMPdb341ALT
#write <db341ALT.input.mpl> "end:"
.sort
frompolynomial;
.store

L cutAMPdbNP331M3 =
#do i=17,17
+ xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
#do i=1,4
- xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qqp3)
#enddo
#do i=1,4
+ xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
- DeltaNP332(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
- Delta431(k2+p3,k1,p3,p5,p4,p2,p1)*IS(qqp3)
;

#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = -dot(k1,p3)-dot(k2,p3);

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qqp3) = INV(S(k2+p3)-mu22);
id IS(kq) = INV(S(k1+k2)-mu11-mu12-mu22);
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <dbNP331M3.input.mpl> "INT_NP331M3 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <dbNP331M3.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <dbNP331M3.input.mpl> "  p1 := p[1];"
#write <dbNP331M3.input.mpl> "  p2 := p[2];"
#write <dbNP331M3.input.mpl> "  p3 := p[3];"
#write <dbNP331M3.input.mpl> "  p4 := p[4];"
#write <dbNP331M3.input.mpl> "  p5 := p[5];"
#write <dbNP331M3.input.mpl> "  s12 := S(p1,p2);"
#write <dbNP331M3.input.mpl> "  s13 := S(p1,p3);"
#write <dbNP331M3.input.mpl> "  s14 := S(p1,p4);"
#write <dbNP331M3.input.mpl> "  s15 := S(p1,p5);"
#write <dbNP331M3.input.mpl> "  s23 := S(p2,p3);"
#write <dbNP331M3.input.mpl> "  s24 := S(p2,p4);"
#write <dbNP331M3.input.mpl> "  s25 := S(p2,p5);"
#write <dbNP331M3.input.mpl> "  s34 := S(p3,p4);"
#write <dbNP331M3.input.mpl> "  s35 := S(p3,p5);"
#write <dbNP331M3.input.mpl> "  s45 := S(p4,p5);"
#write <dbNP331M3.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <dbNP331M3.input.mpl> "%X"
#write <dbNP331M3.input.mpl> "return %e;" cutAMPdbNP331M3
#write <dbNP331M3.input.mpl> "end:"
.sort
frompolynomial;
.store

L cutAMPdb232 =
#do i=1,4
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk1)
#enddo
#do i=11,16
+xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
- DeltaNP332(k1,k2,p1,p2,p3,p4,p5)*IS(kk1)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p3) = -dot(k1,p3);

id IS(kk1) = INV(S(k1-p1)-mu11);
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db232.input.mpl> "INT_232 := proc(p, k1, k2, mu11, mu12, mu22)"
#write <db232.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db232.input.mpl> "  p1 := p[1];"
#write <db232.input.mpl> "  p2 := p[2];"
#write <db232.input.mpl> "  p3 := p[3];"
#write <db232.input.mpl> "  p4 := p[4];"
#write <db232.input.mpl> "  p5 := p[5];"
#write <db232.input.mpl> "  s12 := S(p1,p2);"
#write <db232.input.mpl> "  s13 := S(p1,p3);"
#write <db232.input.mpl> "  s14 := S(p1,p4);"
#write <db232.input.mpl> "  s15 := S(p1,p5);"
#write <db232.input.mpl> "  s23 := S(p2,p3);"
#write <db232.input.mpl> "  s24 := S(p2,p4);"
#write <db232.input.mpl> "  s25 := S(p2,p5);"
#write <db232.input.mpl> "  s34 := S(p3,p4);"
#write <db232.input.mpl> "  s35 := S(p3,p5);"
#write <db232.input.mpl> "  s45 := S(p4,p5);"
#write <db232.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db232.input.mpl> "%X"
#write <db232.input.mpl> "return %e;" cutAMPdb232
#write <db232.input.mpl> "end:"
.sort
frompolynomial;
.store

L cutAMPbt340ALT =

*** 341
#do i=1,4
 - xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
#enddo

*** 340
#do i=5,10
 - xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo

*** 340
+ 0*Delta340(k1,k2+p3,p1,p2,p4,p5,p3)
*** 341
+ Delta341(k1,k2+p3,p1,p2,p4,p5,p3)*IS(kq3)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(qq345) = INV(S(k2-p3-p4-p5)-mu22);
id IS(qqp3) = INV(S(k2+p3)-mu22);
id IS(kq3) = INV(S(k1+k2+p3)-mu11-mu12-mu22);
id IS(kq) = INV(S(k1+k2)-mu11-mu12-mu22);

#include- analytic_5g+++++.prc

argument;
id qqp3 = k2+p3;
endargument;

.sort

topolynomial;
.sort
format maple;
#write <bt340ALT.input.mpl> "INT_340ALT := proc(p, k1, k2, mu11, mu12, mu22)"
#write <bt340ALT.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt340ALT.input.mpl> "  p1 := p[1];"
#write <bt340ALT.input.mpl> "  p2 := p[2];"
#write <bt340ALT.input.mpl> "  p3 := p[3];"
#write <bt340ALT.input.mpl> "  p4 := p[4];"
#write <bt340ALT.input.mpl> "  p5 := p[5];"
#write <bt340ALT.input.mpl> "  s12 := S(p1,p2);"
#write <bt340ALT.input.mpl> "  s13 := S(p1,p3);"
#write <bt340ALT.input.mpl> "  s14 := S(p1,p4);"
#write <bt340ALT.input.mpl> "  s15 := S(p1,p5);"
#write <bt340ALT.input.mpl> "  s23 := S(p2,p3);"
#write <bt340ALT.input.mpl> "  s24 := S(p2,p4);"
#write <bt340ALT.input.mpl> "  s25 := S(p2,p5);"
#write <bt340ALT.input.mpl> "  s34 := S(p3,p4);"
#write <bt340ALT.input.mpl> "  s35 := S(p3,p5);"
#write <bt340ALT.input.mpl> "  s45 := S(p4,p5);"
#write <bt340ALT.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt340ALT.input.mpl> "%X"
#write <bt340ALT.input.mpl> "return %e;" cutAMPbt340ALT
#write <bt340ALT.input.mpl> "end:"
.sort
frompolynomial;
.store

L cutAMPbtNP3305L =

*** NP330;5L
#do i=18,19
 + xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo

*** NP331;M3
#do i=17,17
 + xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo

*** NP331;M2
#do i=10,10
 + xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
#enddo

*** 332
#do i=1,4
 + xxnpdiag'i'*npdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)*IS(kq)
#enddo

*** 341
#do i=5,8
 - xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
#enddo
#do i=1,4
 - xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qqp3)*IS(kq3)
#enddo

*** 340
#do i=22,27
 - xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=5,10
 - xxALTdiag'i'*ALTdiag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qqp3)
#enddo

*** 332
- DeltaNP332(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)*IS(kq)
*** NP331;M2;
- DeltaNP331M2(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
*** NP331;M3
- DeltaNP331M3(k1,k2,p1,p2,p3,p4,p5)*IS(kq3)
*** 340
+ Delta340(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
+ Delta340(k1,k2+p3,p1,p2,p4,p5,p3)*IS(qqp3)
*** 341
+ Delta341(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
+ Delta341(k1,k2+p3,p1,p2,p4,p5,p3)*IS(qqp3)*IS(kq3)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(qq345) = INV(S(k2-p3-p4-p5)-mu22);
id IS(qqp3) = INV(S(k2+p3)-mu22);
id IS(kq3) = INV(S(k1+k2+p3)-mu11-mu12-mu22);
id IS(kq) = INV(S(k1+k2)-mu11-mu12-mu22);

#include- analytic_5g+++++.prc

.sort

topolynomial;
.sort
format maple;
#write <btNP3305L.input.mpl> "INT_NP3305L := proc(p, k1, k2, mu11, mu12, mu22)"
#write <btNP3305L.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <btNP3305L.input.mpl> "  p1 := p[1];"
#write <btNP3305L.input.mpl> "  p2 := p[2];"
#write <btNP3305L.input.mpl> "  p3 := p[3];"
#write <btNP3305L.input.mpl> "  p4 := p[4];"
#write <btNP3305L.input.mpl> "  p5 := p[5];"
#write <btNP3305L.input.mpl> "  s12 := S(p1,p2);"
#write <btNP3305L.input.mpl> "  s13 := S(p1,p3);"
#write <btNP3305L.input.mpl> "  s14 := S(p1,p4);"
#write <btNP3305L.input.mpl> "  s15 := S(p1,p5);"
#write <btNP3305L.input.mpl> "  s23 := S(p2,p3);"
#write <btNP3305L.input.mpl> "  s24 := S(p2,p4);"
#write <btNP3305L.input.mpl> "  s25 := S(p2,p5);"
#write <btNP3305L.input.mpl> "  s34 := S(p3,p4);"
#write <btNP3305L.input.mpl> "  s35 := S(p3,p5);"
#write <btNP3305L.input.mpl> "  s45 := S(p4,p5);"
#write <btNP3305L.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <btNP3305L.input.mpl> "%X"
#write <btNP3305L.input.mpl> "return %e;" cutAMPbtNP3305L
#write <btNP3305L.input.mpl> "end:"
.end
