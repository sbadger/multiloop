* vim: set foldmethod=marker:
#-
S x,y,z,W1,W2;
S tau;
S [l1.p3],[l4.p2],[l1.w],[l4.w];
auto S c,X,Y,p,a1,...,a4,b1,...,b4;
S k1f,k2f,S1,S2,S12,gg,sqrtgg;
S t,t1,...,t8,l1,...,l7;
CF INV,NUM,S(s),IS(s);

CF A(a),B(a),AB;
CF IA(a),IB(a),IAB,dot;
S mu;
S I;

*{{{
#procedure lprod

id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

id S(k1f,k2f) = gg;
id IS(k1f,k2f) = 1/gg;
id I^2=-1;

#endprocedure
*}}}

off stats;

.global

#do s=1,2
G k's' = ( a1's'*k1f+a2's'*k2f+a3's'*AB(k1f,mu,k2f) + a4's'*AB(k2f,mu,k1f) )/2;

id k1f = AB(k1f,mu,k1f);
id k2f = AB(k2f,mu,k2f);

.sort
G l1's' = k's';
G l2's' = k's'-AB(p2,mu,p2)/2;
G l3's' = k's'+AB(p1,mu,p1)/2;
#enddo
G w4 = ( AB(k2f,p3,k1f)*AB(k1f,mu,k2f) - AB(k1f,p3,k2f)*AB(k2f,mu,k1f) )/2/gg;
G w1 = ( AB(k1f,mu,k2f) - AB(k2f,mu,k1f) )/2/gg;
G w2 = I*( AB(k1f,mu,k2f) + AB(k2f,mu,k1f) )/2/gg;

id a31=tau;
id a41=a11*a21/tau;

id a32=a12*a22/tau;
id a42=tau;

id a12=a11;
id a22=a21;

** massless **
*id a11*a21 = 0;

.store

G Delta =
#do alpha=0,2
#do beta=0,2

+c'alpha''beta'*(dot(l1,W1))^'alpha'*(dot(l1,W2))^'beta'

#enddo
#enddo
;

id c20=c20m02;
id c02=-c20m02;
id c22=0;

B x;
print+s;
.store

#do s=1,2
G Delta's' = Delta;
L test1's' = 2*l1's'*w1;
L test2's' = 2*l1's'*w2;
L test3's' = 2*l1's'*w4;
L test4's' = 2*l1's'*AB(p3,mu,p3)+S(p2,p3);

repeat;
id,once dot(l1,W1) = l1's'*w1;
#call lprod
endrepeat;

repeat;
id,once dot(l1,W2) = l1's'*w2;
#call lprod
endrepeat;

B tau,c00,c10,c20m02,c12,c01,c11,c21;
.sort
collect NUM;
argument NUM;
topolynomial;
endargument;
.sort
factarg NUM;
.sort
argument NUM;
frompolynomial;
endargument;
chainout NUM;
splitarg NUM;
id NUM(p1?) = p1;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
id NUM(S1+S2+S12) = 0;
id NUM(p1?) = p1;
#call lprod

id A(px?,p3)*B(p3,py?) = AB(px,p3,py);

B tau;
print+s;
.store
#enddo

#do pow=-3,3

#do s=1,2
L d's'{'pow'+3} = Delta's';
#enddo

if(count(tau,1)!='pow');
discard;
endif;

id tau=1;
id 1/tau=1;

B tau;
print[];
.sort
hide;

#enddo

#define csset "{c00,c10,c01,c20m02,c11,c21,c12}"
.sort

#write <matrix_tri.mpl> "cs:=Vector(7):"
#define i "1"
#do c='csset'
#write <matrix_tri.mpl> "cs['i']:='c':"
#define i "{'i'+1}"
#enddo

#write <matrix_tri.mpl> "dvec:=Vector(14):"
#write <matrix_tri.mpl> "eqs:=Matrix(14,7):"
#define i "1"
#do s=1,2

#define tmax "3"
#define tmin "-3"
.sort

#write "'s','tmin','tmax'"

#do t='tmin','tmax'
#write <matrix_tri.mpl> "dvec['i']:=ds['s','t']:"

#define j "1"
#do c='csset'

L eqs'i'x'j' = d's'{'t'+3};

if(count('c',1)<1);
discard;
endif;
id 'c'=1;

.sort
#write <matrix_tri.mpl> "eqs['i','j']:=%E:" eqs'i'x'j'
#define j "{'j'+1}"
.sort
hide;
#enddo

#define i "{'i'+1}"
#enddo
#enddo

.sort
unhide;

L cfss = NUM(Delta);

argument NUM;
id dot(l1,W1) = 1;
id dot(l1,W2) = x;
endargument;
splitarg NUM;
argument NUM;
id x=1;
endargument;


format 150;
print cfss;
.sort

repeat id NUM(x?,?xx) = 1+NUM(?xx);
print cfss;

.end
