CC:= - NUM( - S(P1)*S(P2)*S(P3) - 2*S(P1)*S(P2)*dot(k1f,P3) + 2*S(P1)*
      S(P2)*dot(k2f,P3) - 2*S(P1)*S(P2)*dot(P2,P3) + 2*S(P1)*dot(k2f,P3
      )*gg - 2*S(P2)*dot(k1f,P3)*gg + S(P3)*gg^2 + 2*dot(P2,P3)*gg^2);

DD:=4*NUM(gg^3*MU1^2 + S(P1)^2*S(P2)*gg + S(P1)^2*S(P2)^2 + S(P1)^2*S(
      P2)^2*gg^(-1)*MU1^2 - 2*S(P1)*S(P2)*gg*MU1^2 + S(P1)*S(P2)*gg^2
       + S(P1)*S(P2)^2*gg)*AB(k1f,P3,k2f)*AB(k2f,P3,k1f) + NUM( - S(P1)
      *S(P2)*S(P3) - 2*S(P1)*S(P2)*dot(k1f,P3) + 2*S(P1)*S(P2)*dot(k2f,
      P3) - 2*S(P1)*S(P2)*dot(P2,P3) + 2*S(P1)*dot(k2f,P3)*gg - 2*S(P2)
      *dot(k1f,P3)*gg + S(P3)*gg^2 + 2*dot(P2,P3)*gg^2)^2;

xDET:=4*NUM(gg^3*MU1^2 + S(P1)^2*S(P2)*gg + S(P1)^2*S(P2)^2 + S(P1)^2*
      S(P2)^2*gg^(-1)*MU1^2 - 2*S(P1)*S(P2)*gg*MU1^2 + S(P1)*S(P2)*gg^2
       + S(P1)*S(P2)^2*gg)*AB(k1f,P3,k2f)*AB(k2f,P3,k1f);

l11:= - 1/4*INV(gg^2 - S(P1)*S(P2))*INV(NUM(gg^2 - S(P1)*S(P2)))^2*INV(
      gg^(-1)*MU1^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)*gg + INV(
      gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)^2 + INV(gg^2 - S(P1)*S(P2))^2
      *S(P1)*S(P2)*gg^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)*S(P2)^2*gg)*
      NUM(gg + S(P1))*NUM(gg + S(P2))*S(P1)*S(P2)*IAB(k2f,P3,k1f)*F(CC,
       - sqrt(DD))*eta(k2f,k1f) + 1/2*INV(gg^2 - S(P1)*S(P2))*NUM(gg + 
      S(P1))*S(P2)*eta(k1f,k1f) - 1/2*INV(gg^2 - S(P1)*S(P2))*NUM(gg + 
      S(P2))*S(P1)*eta(k2f,k2f) - 1/4*INV(gg^2 - S(P1)*S(P2))*IAB(k1f,
      P3,k2f)*F(CC,sqrt(DD))*eta(k1f,k2f) - 1/4*INV(NUM(gg^2 - S(P1)*S(
      P2)))^2*INV(gg^(-1)*MU1^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(
      P2)*gg + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)^2 + INV(gg^2 - 
      S(P1)*S(P2))^2*S(P1)*S(P2)*gg^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)
      *S(P2)^2*gg)*NUM(gg - S(P1)*S(P2)*gg^(-1))*IAB(k2f,P3,k1f)*F(CC,
       - sqrt(DD))*eta(k2f,k1f)*MU1^2;

l12:= - 1/4*INV(gg^2 - S(P1)*S(P2))*INV(NUM(gg^2 - S(P1)*S(P2)))^2*INV(
      gg^(-1)*MU1^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)*gg + INV(
      gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)^2 + INV(gg^2 - S(P1)*S(P2))^2
      *S(P1)*S(P2)*gg^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)*S(P2)^2*gg)*
      NUM(gg + S(P1))*NUM(gg + S(P2))*S(P1)*S(P2)*IAB(k1f,P3,k2f)*F(CC,
       - sqrt(DD))*eta(k1f,k2f) + 1/2*INV(gg^2 - S(P1)*S(P2))*NUM(gg + 
      S(P1))*S(P2)*eta(k1f,k1f) - 1/2*INV(gg^2 - S(P1)*S(P2))*NUM(gg + 
      S(P2))*S(P1)*eta(k2f,k2f) - 1/4*INV(gg^2 - S(P1)*S(P2))*IAB(k2f,
      P3,k1f)*F(CC,sqrt(DD))*eta(k2f,k1f) - 1/4*INV(NUM(gg^2 - S(P1)*S(
      P2)))^2*INV(gg^(-1)*MU1^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(
      P2)*gg + INV(gg^2 - S(P1)*S(P2))^2*S(P1)^2*S(P2)^2 + INV(gg^2 - 
      S(P1)*S(P2))^2*S(P1)*S(P2)*gg^2 + INV(gg^2 - S(P1)*S(P2))^2*S(P1)
      *S(P2)^2*gg)*NUM(gg - S(P1)*S(P2)*gg^(-1))*IAB(k1f,P3,k2f)*F(CC,
       - sqrt(DD))*eta(k1f,k2f)*MU1^2;

