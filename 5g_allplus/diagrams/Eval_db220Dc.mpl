interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.16e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_db220Dc:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,p23f,p23;

p23 := p[2]+p[3]:
p23f := p23 - S(p23)/2./dot(p23,p[1])*p[1]:

av[1] := p[1];
av[2] := p23f;
av[3] := A(p23f,p[4])/A(p[1],p[4])*eta(p[1],p23f)/2;
av[4] := B(p23f,p[4])/B(p[1],p[4])*eta(p23f,p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [s45/(s45-s23), 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s45*s15/s14*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 := s45*s15/(s45-s23)*(1-tau1-tau2)*(1 + tau3 + tau4) - s45*s15/s14*(tau2*tau3 + tau1*tau4);

return k1,k2,mu11,mu12,mu22;
end:


xxdiag1:=1:
xxdiag2:=2:
xxdiag3:=-2:
xxdiag4:=2:
xxdiag9:=1:
xxdiag10:=-2:
xxdiag11:=2:
xxdiag12:=-4:
xxdiag13:=-1:
xxdiag14:=1:
xxdiag15:=1:
xxdiag16:=-2:
xxdiag17:=-2:
xxdiag18:=-2:
xxdiag19:=-1:
xxdiag20:=2:
xxv4s:=I/2:

xxsub:=1:
Ds:=Dsm2+2;
#read("tmp/db220Dc.input.mpl"):
read("INT_5g+++++.mpl"):

for k1 from 0 to 4 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
for k4 from 0 to 4 do:
ds(k1,k2,k3,k4):=0:
od:
od:
od:
od:

for th1 from 0 to 4 do:
for th2 from 0 to 4 do:
for th3 from 0 to 4 do:
for th4 from 0 to 4 do:
printf("%d%d%d%d\n",th1,th2,th3,th4);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/5);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/5);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/5);
t4 := 1.3*exp(2.*evalf(Pi)*I*th4/5);

k1,k2,mu11,mu12,mu22 := on_shell_db220Dc([p1,p2,p3,p4,p5],t1,t2,t3,t4):
val := INT_db220Dc(k1,k2,mu11,mu12,mu22);

for k1 from 0 to 4 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
for k4 from 0 to 4 do:
  ds(k1,k2,k3,k4) := expand(ds(k1,k2,k3,k4) + evalf(val/t1^k1/t2^k2/t3^k3/t4^k4/(5.*5.*5.*5.))):
od:
od:
od:
od:

od:
od:
od:
od:

read("../final/db220Dc.coeffvecs.mpl");

for k1 from 0 to 4 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
for k4 from 0 to 4 do:
 tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3,k4))):
 tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3,k4))):
 tmp3 := Re(coeff(ds(k1,k2,k3,k4),Dsm2)):
 tmp4 := Im(coeff(ds(k1,k2,k3,k4),Dsm2)):
 tmp5 := Re(coeff(ds(k1,k2,k3,k4),Dsm2^2)):
 tmp6 := Im(coeff(ds(k1,k2,k3,k4),Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("d_%d%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k1, k2, k3, k4, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
#  lprint(k1,k2,k3,k4,expand(ds(k1,k2,k3,k4)));
od:
od:
od:
od:

for k from 1 to 160 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 tmp5 := Re(coeff(Dvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Dvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint(k,expand(Dvec[k]));
od:

read("../db220Dc.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(160):
Cvec := iMM.Dvec:

for k from 1 to 160 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 tmp5 := Re(coeff(Cvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Cvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint(Clbl[k],expand(Cvec[k]));
od:


