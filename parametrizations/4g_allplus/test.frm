#-
#include- ../2to2tools.prc

.global

#include- tmp.prc
#call cancel

id xxdiag3 = -xxdiag2;

id xxghost4 = 2*I*xxdiag2;
id xxghost5 = 2*I*xxdiag2;
id xxghost6 = 4*xxdiag2;
id xxghost7 = -I/2*xxdiag2;

id xxdiag2 = 1;
*id [Ds-2] = Ds-2;
*if(count(Ds,1)<2);
*discard;
*endif;

multiply ds(0,0,0,0,0);
repeat;
id,once tau1*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1+1,x2,x3,x4,x5);
id,once tau2*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2+1,x3,x4,x5);
id,once tau3*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3+1,x4,x5);
id,once tau4*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3,x4+1,x5);
id,once tau5*ds(x1?,x2?,x3?,x4?,x5?) = ds(x1,x2,x3,x4,x5+1);
endrepeat;

#include- ../final/bowtieD.coeffvecs.prc

B INV;
print[];
.store

#do k=1,146

G dvec'k' = AMP;
id ds('k') = 1;
id ds(x?) = 0;
id NUM(x?) = x;

print+s;
.store

#enddo

#include- ../final/bowtieD.integrand.prc
#include- ../final/bowtieD.coeffvecs.prc

#do k=1,146
id ds('k') = dvec'k';
#enddo

denominators INV;

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);
id INV(s12+s14) = -1/s13;

#call simplify4
id NUM(x?) = x;
#call simplify4

id cs(x1?,x2?,x3?,x4?,x5?,x6?,x7?) =
  (2*dot(k1,p4))^x3*
  (2*dot(k2,p1))^x4*
  (2*dot(k1,omega4))^x1*
  (2*dot(k2,omega4))^x2*
  mu11^x5*
  mu12^x6*
  mu22^x7
;
.sort

id dot(k1,omega4)*dot(k2,omega4) = -s14*s13/s12/2*(
    + 1/2*s14*s12*s13^-1
          - mu12
          - dot(p1,k2)*s12*s13^-1
          - 2*dot(p1,k2)*dot(p4,k1)*s14^-1*s12*s13^-1
          - 4*dot(p1,k2)*dot(p4,k1)*s13^-1
          - dot(p4,k1)*s12*s13^-1
          - dot(k1,k2)
);

id NUM(x?) = x;
#call simplify4

AB mu11,mu12,mu22;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(x1?) = x1;

id dot(k1,omega4)*dot(k2,omega4) = -s14*s13/s12/2*(
    + 1/2*s14*s12*s13^-1
          - mu12
          - dot(p1,k2)*s12*s13^-1
          - 2*dot(p1,k2)*dot(p4,k1)*s14^-1*s12*s13^-1
          - 4*dot(p1,k2)*dot(p4,k1)*s13^-1
          - dot(p4,k1)*s12*s13^-1
          - dot(k1,k2)
);

B [Ds-2],Ds;
format 150;
print+s;
.end
