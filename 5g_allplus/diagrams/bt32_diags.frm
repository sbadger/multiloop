#-
#include- ../2to3tools.prc

CF PropGlu,PropGht,VertGlu,VertGlu4,VertGht;
CF INTdb320;

#procedure to4D

id,once dot(l1,px?) = dot(k1,px);
id,once dot(l2,px?) = dot(k1,px)-dot(p1,px);
id,once dot(l3,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px);
id,once dot(l4,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px)-dot(p3,px);
id,once dot(l5,px?) = -dot(k2,px)+dot(p4,px)+dot(p5,px);
id,once dot(l6,px?) = -dot(k2,px)+dot(p5,px);
id,once dot(l7,px?) = -dot(k2,px);
id,once dot(l8,px?) = -dot(k1,px)-dot(k2,px);
id,once AB(px?,l1,py?) = AB(px,k1,py);
id,once AB(px?,l2,py?) = AB(px,k1,py)-AB(px,p1,py);
id,once AB(px?,l3,py?) = AB(px,k1,py)-AB(px,p1,py)-AB(px,p2,py);
id,once AB(px?,l4,py?) = AB(px,k1,py)-AB(px,p1,py)-AB(px,p2,py)-AB(px,p3,py);
id,once AB(px?,l5,py?) = -AB(px,k2,py)+AB(px,p4,py)+AB(px,p5,py);
id,once AB(px?,l6,py?) = -AB(px,k2,py)+AB(px,p5,py);
id,once AB(px?,l7,py?) = -AB(px,k2,py);
id,once AB(px?,l8,py?) = -AB(px,k1,py)-AB(px,k2,py);

#call cancel

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;

#endprocedure

.global

G AMP =

  +EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5)*(

  + xxdiag1*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGlu(-l4,-l8,l5,nu4,nu8,rho5)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    VertGlu(-l7,l8,l1,nu7,rho8,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)*
    PropGlu(l8,nu8,rho8,mu33)
  )*INV(dot(l8,l8))

  + xxdiag2*(
    VertGht(l2,mu1)*
    VertGht(l3,mu2)*
    VertGht(l4,mu3)*
    VertGht(-l8,rho5)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    VertGht(l1,nu7)*
    PropGht(l1,mu11)*
    PropGht(l2,mu11)*
    PropGht(l3,mu11)*
    PropGht(l4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)*
    PropGht(l8,mu33)
  )*INV(dot(l8,l8))

  + xxdiag3*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGht(l5,nu4)*
    VertGht(l6,mu4)*
    VertGht(l7,mu5)*
    VertGht(l8,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGht(l5,mu22)*
    PropGht(l6,mu22)*
    PropGht(l7,mu22)*
    PropGht(l8,mu33)
  )*INV(dot(l8,l8))

   + xxdiag4*(
    VertGht(l2,mu1)*
    VertGht(l3,mu2)*
    VertGht(l4,mu3)*
    VertGht(l5,rho8)*
    VertGht(l6,mu4)*
    VertGht(l7,mu5)*
    VertGht(l1,nu8)*
    PropGht(l1,mu11)*
    PropGht(l2,mu11)*
    PropGht(l3,mu11)*
    PropGht(l4,mu11)*
    PropGht(l5,mu22)*
    PropGht(l6,mu22)*
    PropGht(l7,mu22)*
    PropGlu(l8,nu8,rho8,mu33)
  )*INV(dot(l8,l8))

** gluon tri - box
  + xxdiag5*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGlu(-l4,-p1-p2-p3,l1,nu4,nu8,rho1)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    VertGlu(-l7,p1+p2+p3,l5,nu7,rho8,rho5)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)*
    PropGlu(p1+p2+p3,nu8,rho8,0)
  )/s45

** gluon tri - box (4-vertex)
  + xxdiag6*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGlu4(-l4,l5,-l7,l1,nu4,rho5,nu7,rho1)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)
  )

  )

  - INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*INV(dot(l8,l8))

;

id xxdiag1 = 0;
id xxdiag2 = 0;
id xxdiag3 = 0;
id xxdiag4 = 0;

#include- INT_db320.prc

repeat;
id,once VertGlu(p1?, p2?, p3?, mu1?, mu2?, mu3?) = I/rt2*(
    GGG(mu2,mu3)*(MOM(p2,mu1)-MOM(p3,mu1))
  + GGG(mu3,mu1)*(MOM(p3,mu2)-MOM(p1,mu2))
  + GGG(mu1,mu2)*(MOM(p1,mu3)-MOM(p2,mu3))
);

id,once VertGlu4(p1?,p2?,p3?,p4?,mu1?,mu2?,mu3?,mu4?) = I/2*(
  - GGG(mu1,mu2)*GGG(mu3,mu4)
  - GGG(mu2,mu3)*GGG(mu1,mu4)
  + 2*GGG(mu1,mu3)*GGG(mu2,mu4)
);

id,once VertGht(p1?, mu1?) = I/rt2*(
    MOM(p1,mu1)
);

id MOM(p1+p2,mu1?) = MOM(p1,mu1)+MOM(p2,mu1);
id MOM(-p1-p2,mu1?) = -MOM(p1,mu1)-MOM(p2,mu1);

#call Lcontract
id,once PropGlu(p?, mu1?, mu2?, m?) = I*( -GGG(mu1,mu2) );
id,once PropGht(p?, m?) = 1;
#call Lcontract

endrepeat;

B VertGlu,VertGht;
print[];
.sort

*id Ds = [Ds-2]+2;
id [Ds-2] = Ds-2;
if(count(Ds,1)<2);
discard;
endif;

repeat;
#call to4D
endrepeat;
id INV(dot(l8,l8)) = INV(2*dot(k1,k2)-mu12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
#call cancel

.sort
topolynomial;

.sort
format maple;
#write <bt32D.input.mpl> "INT_bt32D := proc(k1,k2,mu11,mu12,mu22) local Z:"
#write <bt32D.input.mpl> "%X"
#write <bt32D.input.mpl> "return %e" AMP
#write <bt32D.input.mpl> "end:"
.sort
frompolynomial;

*repeat;
*#call subsLmom(../final,bt32D)
*#call cancel
*endrepeat;
*.sort
*#write <bt32D_integrand.prc> "G AMP = %e" AMP

format 150;
B Ds,INV;
*B tau1,...,tau4;
print+s;
.end
