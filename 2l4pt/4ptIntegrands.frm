* vim: set sw=2:
* vim: set ft=form:
* vim: set foldmethod=marker:
#-
#define treepath "../amplib"
#include- ../lib/GU.hdr
#include- ../lib/nGUtools.h
#include- ../lib/nGU2tools.h
#include- ../lib/GUanalytic.h
#include- ../lib/factorise.h

CF SUSY,HEL,FFSvertex;
CF BFly4PT;
S Nf,Ns;

*{{{extraids
#procedure extraids
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
#endprocedure
*}}}

off statistics;
.global

#do hlbl={6,9,10,12}

L tmp = HEL('hlbl')*TMP;

#do i=3,1,-1
id HEL(x1?,?x) = HEL(x1-{2^'i'}*integer_(x1/2^{'i'}),integer_(x1/2^{'i'}),?x);
#enddo

repeat;
id HEL(x1?,?x)*TMP(?y) = HEL(?x)*TMP(?y,2*x1-1);
endrepeat;
id HEL = 1;
id TMP(?x) = HEL(?x);
id HEL(a1?$H1,a2?$H2,a3?$H3,a4?$H4) = HEL(a1,a2,a3,a4);

print+s;
.store

#write "*** N=0 4-point two-loop ampltudes ***"
#write "*** helicity = '$H1','$H2','$H3','$H4' ***"

#do fl1={0,1/2,1}
#do fl2={0,1/2,1,-1/2}

*{{{ planar double box integrand
L [C7.12*34*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),
ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),
ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar triangle|pentagon (penta-box) integrand
L [C7.1*234*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*TriPent(
Kmom(ptl(p1,'$H1',0)),
Kmom,
Kmom(ptl(p2,'$H2',0)),
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),
ptl(l3,'h3','fl2'),ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),
ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ non-planar planar crossed double box integrand
L [C7.1*34*2] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBoxX(
Kmom(ptl(p1,'$H1',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
Kmom(ptl(p2,'$H2',0)),
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),
ptl(l3,'h3','fl2'),ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1'),ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

*{{{ planar box|triangle integrand [12*3*]
L [C6.12*3*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*BoxTri(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0),ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),
ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar box|triangle integrand [12*3[4]]
L [C6.12*3[4]] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*BoxTri(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),
ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar box|triangle integrand [12[3]4*]
L [C6.12[3]4*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*BoxTri(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),
ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar triangle|box integrand [1*34*]
L [C6.1*34*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*TriBox(
Kmom(ptl(p1,'$H1',0),ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),
ptl(l3,'h3','fl2'),ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar triangle|box integrand [1[2]34*]
L [C6.1[2]34*] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*TriBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),
ptl(l3,'h3','fl2'),ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar triangle|box integrand [2*34[1]]
L [C6.2*34[1]] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+TMPFL('fl1','fl2')*TriBox(
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom(ptl(p1,'$H1',0)),
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),
ptl(l3,'h3','fl2'),ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),
ptl(l6,'h6','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}
*{{{ planar triangle|triangle integrand [12*34] (bow-tie)
L [C6.12*34] =
#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2

+BFly4PT('fl1','fl2')*TMPFL('fl1','fl2')*TriTriB(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom(0,0,0),
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),
ptl(l4,'h4','fl2'),ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

id BFly4PT(0,0) = TMPreplace(0,0,0,0);
id BFly4PT(1,1) = TMPreplace(1,1,3,3)-TMPFL(xf)*TMPreplace(1,3,3,1);
id BFly4PT(1/2,1/2) = TMPreplace(1/2,1/2,3/2,3/2)-TMPFL(xf)*TMPreplace(1/2,3/2,3/2,1/2);
id BFly4PT(1/2,-1/2) = TMPreplace(1/2,-1/2,-3/2,3/2);
id BFly4PT(xf1?,xf2?) = TMPreplace(xf1,xf2,xf2,xf1);

id TMPFL(1/2) = Nf;
id TMPFL(1) = Ns;

id TMPFL(0,0) = 1;
id TMPFL(1/2,0) = -Nf;
id TMPFL(0,1/2) = -Nf;
id TMPFL(1/2,1/2) = -Nf;
id TMPFL(1,0) = Ns;
id TMPFL(0,1) = Ns;
id TMPFL(1,1) = Ns;
id TMPFL(1/2,1) = Nf*Ns/2;
id TMPFL(1,1/2) = Nf*Ns/2;
id TMPFL(1/2,-1/2) = Nf*Ns/2;
id TMPFL(?x) = 0;
.sort
#call topologise2
id TMPreplace(x3?,x4?,x6?,x1?) = replace_(BFLAV3,x3)*replace_(BFLAV4,x4)*replace_(BFLAV6,x6)*replace_(BFLAV1,x1);

#call subtreesGU

#call sortminusl
#call sortminusl

.sort

format maple;
#write <C7.planar.INT.log> "C7_1_INT['hlbl','fl1','fl2']:=\n%e\n" [C7.12*34*]
#write <C7.planar.INT.log> "C7_2_INT['hlbl','fl1','fl2']:=\n%e\n" [C7.1*234*]

#write <C6.planar.INT.log> "C6_1_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.12*3*]
#write <C6.planar.INT.log> "C6_2_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.12*3[4]]
#write <C6.planar.INT.log> "C6_3_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.12[3]4*]
#write <C6.planar.INT.log> "C6_4_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.1*34*]
#write <C6.planar.INT.log> "C6_5_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.1[2]34*]
#write <C6.planar.INT.log> "C6_6_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.2*34[1]]
#write <C6.planar.INT.log> "C6_7_INT['hlbl','fl1','fl2']:=\n%e\n" [C6.12*34]

#write <C7.nplanar.INT.log> "C7_1_INT['hlbl','fl1','fl2']:=\n%e\n" [C7.1*34*2]

#write "*** helicity = '$H1','$H2','$H3','$H4' ***"
#write "*** flavour = 'fl1','fl2' ***"
format 180;
B IA,IB,IAB,IS;
print+s [C6.1*34*];
.store
#enddo
#enddo

#enddo
.end

