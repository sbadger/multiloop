Get["/Users/simon/gitrepos/BasisDet/BasisDet-1-02.m"];

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* db221 *)
L=2;
Dim=4-2\[Epsilon];
n=5;
ExternalMomentaBasis = {p5, p1, p4, p2};
Kinematics = {
   p1^2 -> 0, p3^2 -> 0, p4^2 -> 0, p5^2 -> 0,
   p1*p2 -> s12/2, p1*p3 -> (s45-s12-s23)/2, p1*p4 -> (s23-s15-s45)/2, p1*p5 -> s15/2,
   p2*p3 -> s23/2, p2*p4 -> (s15-s23-s34)/2, p2*p5 -> (s34-s12-s15)/2,
   p3*p4 -> s34/2, p3*p5 -> (s12-s34-s45)/2,
   p4*p5 -> s45/2
};

numeric = {s12 -> 11, s23 -> 17, s34 -> 7, s45 -> 3, s15 -> 29};

Props = {l1, l1 - p1, l1 - p1 - p2, l2, l2 - p4, l2 - p3 - p4, l1 + l2 + p5 , l1 + l2};

RenormalizationCondition = {{{1, 0}, 5}, {{0, 1}, 5}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["basis/db221D.basis.h"];
WriteString[fout,"id Basisdb221D = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

(* db221 *)
L=2;
Dim=4;
n=5;
ExternalMomentaBasis = {p5, p1, p4, p2};
Kinematics = {
   p1^2 -> 0, p3^2 -> 0, p4^2 -> 0, p5^2 -> 0,
   p1*p2 -> s12/2, p1*p3 -> (s45-s12-s23)/2, p1*p4 -> (s23-s15-s45)/2, p1*p5 -> s15/2,
   p2*p3 -> s23/2, p2*p4 -> (s15-s23-s34)/2, p2*p5 -> (s34-s12-s15)/2,
   p3*p4 -> s34/2, p3*p5 -> (s12-s34-s45)/2,
   p4*p5 -> s45/2
};

numeric = {s12 -> 11, s23 -> 17, s34 -> 7, s45 -> 3, s15 -> 29};

Props = {l1, l1 - p1, l1 - p1 - p2, l2, l2 - p4, l2 - p3 - p4, l1 + l2 + p5 , l1 + l2};

RenormalizationCondition = {{{1, 0}, 5}, {{0, 1}, 5}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["basis/db221.basis.h"];
WriteString[fout,"id Basisdb221 = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];


