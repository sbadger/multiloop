* vim: set foldmethod=marker:
#-
#include ../2to3tools.prc
#include diagrams.prc

auto CF INTdb, INTbt;
S Dsm2;
S w,w123,w543,w132,w534,w12p,w12m,w45p,w45m,w13p,w13m,w53p,w53m;

off stats;
.global

#procedure signs

*** diagrams for pentagon-box: db320
id xxdiag1  =  1;
id xxdiag2  =  2;
id xxdiag3  = -2;
id xxdiag4  =  2;
*** diagrams for box-pentagon: db230
id xxdiag5  =  1;
id xxdiag6  = -2;
id xxdiag7  =  2;
id xxdiag8  =  2;
*** diagrams for box-triangle butterfly : bt32
id xxdiag9  =  1;
id xxdiag10 = -2;
id xxdiag11 =  2;
id xxdiag12 = -4;
id xxdiag13 = -1;
id xxdiag14 =  1/2;
*** diagrams for double-box with massive 2nd leg : db220c
id xxdiag15 =  1;
id xxdiag16 = -2;
id xxdiag17 = -2;
id xxdiag18 = -2;
id xxdiag19 = -1;
id xxdiag20 =  2;
*** diagrams for double-box with 5-legs : db220b
id xxdiag21 = -1;
*** diagrams for triangle-box butterfly : bt23
id xxdiag22 =  1;
id xxdiag23 =  2;
id xxdiag24 = -2;
id xxdiag25 = -4;
id xxdiag26 = -1;
id xxdiag27 =  1/2;
*** diagrams for double-triangle butterfly with massive 2nd leg : bt22c
id xxdiag28 =  1;
id xxdiag29 =  2;
id xxdiag30 =  2;
id xxdiag31 =  4;
id xxdiag32 = -1;
id xxdiag33 = -2;
id xxdiag34 = -1;
id xxdiag35 = -1/2;
id xxdiag36 =  1;
*** diagrams for double-triangle butterfly with five legs : bt22b
id xxdiag37 =  1;
id xxdiag38 =  2;
id xxdiag39 =  2;
id xxdiag40 =  4;
id xxdiag41 = -1;
id xxdiag42 = -2;
id xxdiag43 = -1;
id xxdiag44 = -2;
*** diagrams for double-box : db310
id xxdiag45 =  1;
id xxdiag46 =  2;
id xxdiag47 =  2;
id xxdiag48 = -2;
id xxdiag49 = -1;
id xxdiag50 = -2;
*** diagrams for butterfly : bt31
id xxdiag51 =  1;
id xxdiag52 = -2;
id xxdiag53 = -2;
id xxdiag54 =  4;
id xxdiag55 = -1;
id xxdiag56 =  2;
id xxdiag57 = -1;
id xxdiag58 = -1/2;
id xxdiag59 =  1;
*** diagrams for double-box : db220e (db220M4)
id xxdiag60 =  1;
id xxdiag61 = -2;
id xxdiag62 = -2;
id xxdiag63 = -2;
id xxdiag64 = -1;
id xxdiag65 =  2;
*** diagrams for double-box : db210b
id xxdiag66 = -1;
id xxdiag67 =  1;
*** diagrams for butterfly : bt22e
id xxdiag68 =  1;
id xxdiag69 =  2;
id xxdiag70 =  2;
id xxdiag71 =  4;
id xxdiag72 = -1;
id xxdiag73 = -2;
id xxdiag74 = -1;
id xxdiag75 = -1/2;
id xxdiag76 =  1;
*** diagrams for butterfly : bt21b
id xxdiag77 =  1;
id xxdiag78 =  2;
id xxdiag79 = -2;
id xxdiag80 = -4;
id xxdiag81 = -1;
id xxdiag82 = -2;
id xxdiag83 = -1;
id xxdiag84 =  2;
id xxdiag85 = -1;
id xxdiag86 = -2;
id xxdiag87 =  1;
id xxdiag88 =  2;
id xxdiag89 =  1;

id xxsub = 1;
id xxsub1 = 1;
id xxsub2 = 1;
id xxsub3 = 1;
id xxsub4 = 1;
id xxsub5 = 1;
id xxsub6 = 1;
id xxsub7 = 1;
id xxsub8 = 1;
id xxsub9 = 1;
id xxsub10 = 1;
id xxsub11 = 1;

#endprocedure

#define rerun "0"
#define dd "0"

#if 'rerun'=1

*** diagrams for pentagon-box: db320
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-pentagon: db230
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-triangle butterfly : bt32
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;
*}}}
*** diagrams for double-box with massive 2nd leg : db220c
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for double-box with 5-legs : db220b
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for triangle-box butterfly : bt23
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;
*}}}
*** diagrams for double-triangle butterfly with massive 2nd leg : bt22c
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

*}}}
*** diagrams for double-triangle butterfly with five legs : bt22b
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

*}}}
*** diagrams for pentagon-triangle: db310
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-triangle butterfly: bt31
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

*}}}
*** diagrams for double-box: db220e (12*34*)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for box-triangle: db210b
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk123, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p45, rho6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk123, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;
*}}}
*** diagrams for double-triangle butterfly: bt22e
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

*}}}
*** diagrams for triangle-bubble butterfly: bt21
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

*}}}
#call expandtopo
multiply EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5);

#call subvertices
#call cancel

*if(count(Ds,1)<1);
*discard;
*endif;
.sort

argument AB,dot,S,IS;
id kk = k1;
id kk1 = MOM(k1,-p1);
id kk12 = MOM(k1,-p1,-p2);
id kk123 = MOM(k1,-p1,-p2,-p3);
id p123 = MOM(p1,p2,p3);
id p12 = MOM(p1,p2);
id p23 = MOM(p2,p3);
id p45 = MOM(p4,p5);
id qq = k2;
id qq5 = MOM(k2,-p5);
id qq45 = MOM(k2,-p4,-p5);
id qq345 = MOM(k2,-p3,-p4,-p5);
id kq = MOM(k1,k2);
endargument;

*{{{ expand and simplify ids

id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
repeat id AB(p1?,MOM(k1?{k1,k2},?xx),p3?) = AB(p1,k1,p3)+AB(p1,MOM(?xx),p3);

argument AB;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
id AB(p1?,-MOM(?x1),p2?) = -AB(p1,MOM(?x1),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
id AB(p1?,MOM,p3?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

repeat id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
repeat id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
#call cancel

repeat id dot(MOM(p1?),xx?) = dot(p1,xx);

id dot(MOM(k1?{k1,k2},?x1),MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2)) + dot(k2,MOM(?x1)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),MOM(?x2)) = dot(k1,MOM(?x2)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),p1?) = dot(k1,p1) + dot(MOM(?x1),p1);
id dot(k1?{k1,k2},MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2));

argument dot;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
id dot(p1?ps,MOM(p2?,?x)) = dot(p1,p2) + dot(p1,MOM(?x));
id dot(p1?ps,MOM(?x1,p1?ps,?x2)) = dot(p1,MOM(?x1,?x2));
id dot(p1?ps,MOM(p1?ps,?x2)) = dot(p1,MOM(?x2));
id dot(p1?ps,MOM(p1?ps,p2?)) = dot(p1,p2);
id dot(MOM,x?) = 0;
argument dot;
repeat id MOM(p1?,?x) = p1 + MOM(?x);
id MOM = 0;
endargument;
id dot(p1?ps,p1?ps) = 0;
id dot(p?,p?) = S(p);
id IS(MOM(?x)) = IS(?x);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;

id dot(px?,p1+p2+p3) = -dot(px,p4+p5);
id dot(px?,p3+p4+p5) = -dot(px,p1+p2);
id dot(p?,p?) = S(p);
splitarg S,IS;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
.sort

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);
*
id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);
*
id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);
*
id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

id AB(px1?,MOM(px2?,px3?,px4?),px5?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

#call cancel
id IS(p1,p2,p3) = 1/s45;
id IS(p3,p4,p5) = 1/s12;
id dot(p1+p2,p4+p5) = -(s12+s45)/2;
id S(k1?{k1,k2}) = dot(k1,k1);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id dot(p1?ps,p2?) = dot(p1,MOM(p2));
argument dot;
splitarg MOM;
endargument;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

AB s12,s23,s34,s45,s15,s13,s14,s24,s25,s35;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument INV,NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

*}}}

.sort

#do i=1,'dd'
  #write <tmp/diagrams.prc> "G diag'i' = %e" diag'i'
#enddo

#else
#include- tmp/diagrams.prc
#endif

B MOM,V;
print[];
.store

L cutAMPdb320 =
#do i=1,4
+xxdiag'i'*diag'i'
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

topolynomial;
.sort
format maple;
#write <db320D.input.mpl> "INT_db320D := proc(k1,k2,mu11,mu12,mu22)"
#write <db320D.input.mpl> "local Z;"
#write <db320D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db320D.input.mpl> "%X"
#write <db320D.input.mpl> "return %e" cutAMPdb320
#write <db320D.input.mpl> "end:"
.store

L cutAMPdb230 =
#do i=5,8
+xxdiag'i'*diag'i'
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p3) = -s45/2+s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

topolynomial;
.sort
format maple;
#write <db230D.input.mpl> "INT_db230D := proc(k1,k2,mu11,mu12,mu22)"
#write <db230D.input.mpl> "local Z;"
#write <db230D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db230D.input.mpl> "%X"
#write <db230D.input.mpl> "return %e" cutAMPdb230
#write <db230D.input.mpl> "end:"
.store

L cutAMPbt32 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'
#enddo
-xxsub*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
;
#include- INT_db320.prc
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);

topolynomial;
.sort
format maple;
#write <bt32D.input.mpl> "INT_bt32D := proc(k1,k2,mu11,mu12,mu22)"
#write <bt32D.input.mpl> "local Z;"
#write <bt32D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt32D.input.mpl> "%X"
#write <bt32D.input.mpl> "return %e" cutAMPbt32
#write <bt32D.input.mpl> "end:"
.store

L cutAMPbt23 =
#do i=5,8
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'
#enddo
-xxsub*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
;
#include- INT_5g+++++.prc
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p3) = -s45/2+s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);

topolynomial;
.sort
format maple;
#write <bt23D.input.mpl> "INT_bt23D := proc(k1,k2,mu11,mu12,mu22)"
#write <bt23D.input.mpl> "local Z;"
#write <bt23D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt23D.input.mpl> "%X"
#write <bt23D.input.mpl> "return %e" cutAMPbt23
#write <bt23D.input.mpl> "end:"
.store

L cutAMPdb220c =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kk12)
#enddo
#do i=15,20
+xxdiag'i'*diag'i'
#enddo
-xxsub*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk12)
;
#include- INT_db320.prc
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p1+p2+p3) = -s45/2;

id IS(kk12) = INV(-2*dot(k1,MOM(p1,p2))+s12);

topolynomial;
.sort
format maple;
#write <db220Dc.input.mpl> "INT_db220Dc := proc(k1,k2,mu11,mu12,mu22)"
#write <db220Dc.input.mpl> "local Z;"
#write <db220Dc.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db220Dc.input.mpl> "%X"
#write <db220Dc.input.mpl> "return %e" cutAMPdb220c
#write <db220Dc.input.mpl> "end:"
.store

L cutAMPdb220b =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kk123)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'*IS(qq345)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub2*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)
;
#include- INT_db320.prc
#call signs

*if(count(Ds,1)<1);
*discard;
*endif;
*.sort

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);

topolynomial;
.sort
format maple;
#write <db220Db.input.mpl> "INT_db220Db := proc(k1,k2,mu11,mu12,mu22)"
#write <db220Db.input.mpl> "local Z;"
#write <db220Db.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db220Db.input.mpl> "%X"
#write <db220Db.input.mpl> "return %e" cutAMPdb220b
#write <db220Db.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt22c =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kk12)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'*IS(kk12)
#enddo
#do i=15,20
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=28,36
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk12)*IS(kq)
-xxsub2*INTdb220c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
-xxsub3*INTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk12)
;
#include- INT_5g+++++.prc
#call signs

id Dsm2 = Ds-2;
*if(count(Ds,1)<1);
*discard;
*endif;
.sort

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p1+p2+p3) = -s45/2;


id IS(kk12) = INV(-2*dot(k1,MOM(p1,p2))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;

topolynomial;
.sort
format maple;
#write <bt22Dc.input.mpl> "INT_bt22Dc := proc(k1,k2,mu11,mu12,mu22)"
#write <bt22Dc.input.mpl> "local Z;"
#write <bt22Dc.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt22Dc.input.mpl> "%X"
#write <bt22Dc.input.mpl> "return %e" cutAMPbt22c
#write <bt22Dc.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt22b =
#do i=1,4
+xxdiag'i'*diag'i'*IS(kk123)*IS(kq)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'*IS(qq345)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'*IS(qq345)
#enddo
#do i=37,44
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
-xxsub2*INTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub3*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
-xxsub4*INTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)
-xxsub5*INTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
;
#include- INT_5g+++++.prc
#call signs

*if(count(Ds,1)<1);
*discard;
*endif;
*.sort

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

topolynomial;
.sort
format maple;
#write <bt22Db.input.mpl> "INT_bt22Db := proc(k1,k2,mu11,mu12,mu22)"
#write <bt22Db.input.mpl> "local Z;"
#write <bt22Db.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt22Db.input.mpl> "%X"
#write <bt22Db.input.mpl> "return %e" cutAMPbt22b
#write <bt22Db.input.mpl> "end:"
frompolynomial;
.store

L cutAMPdb310 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(qq5)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)
;
#include- INT_5g+++++.prc
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));

topolynomial;
.sort
format maple;
#write <db310D.input.mpl> "INT_db310D := proc(k1,k2,mu11,mu12,mu22)"
#write <db310D.input.mpl> "local Z;"
#write <db310D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db310D.input.mpl> "%X"
#write <db310D.input.mpl> "return %e" cutAMPdb310
#write <db310D.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt31 =
#do i=1,4
+xxdiag'i'*diag'i'*IS(qq5)*IS(kq)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'*IS(qq5)
#enddo
#do i=51,59
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
-xxsub2*INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
-xxsub3*INTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)
;
#include- INT_5g+++++.prc
id INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = 0;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

topolynomial;
.sort
format maple;
#write <bt31D.input.mpl> "INT_bt31D := proc(k1,k2,mu11,mu12,mu22)"
#write <bt31D.input.mpl> "local Z;"
#write <bt31D.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt31D.input.mpl> "%X"
#write <bt31D.input.mpl> "return %e" cutAMPbt31
#write <bt31D.input.mpl> "end:"
frompolynomial;
.store

L cutAMPdb210b =
#do i=1,4
+xxdiag'i'*diag'i'*IS(qq5)*IS(kk123)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'*IS(qq5)*IS(qq345)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'*IS(qq5)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'*IS(qq345)
#enddo
#do i=66,67
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)
-xxsub2*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(qq345)
-xxsub3*INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub4*INTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)
-xxsub5*INTdb220e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)
;
#include- INT_5g+++++.prc
id INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = 0;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);

topolynomial;
.sort
format maple;
#write <db210Db.input.mpl> "INT_db210Db := proc(k1,k2,mu11,mu12,mu22)"
#write <db210Db.input.mpl> "local Z;"
#write <db210Db.input.mpl> "global p1,p2,p3,p4,p5;"
#write <db210Db.input.mpl> "%X"
#write <db210Db.input.mpl> "return %e" cutAMPdb210b
#write <db210Db.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt21b =
#do i=1,4
+xxdiag'i'*diag'i'*IS(qq5)*IS(kk123)*IS(kq)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'*IS(qq5)*IS(qq345)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'*IS(qq5)*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'*IS(qq5)*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'*IS(qq5)*IS(qq345)
#enddo
#do i=37,44
+xxdiag'i'*diag'i'*IS(qq5)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'*IS(kk123)*IS(kq)
#enddo
#do i=51,59
+xxdiag'i'*diag'i'*IS(kk123)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'*IS(qq345)*IS(kq)
#enddo
#do i=66,67
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=68,76
+xxdiag'i'*diag'i'*IS(qq345)
#enddo
#do i=77,89
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)*IS(kq)
-xxsub2*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(qq345)*IS(kq)
-xxsub7*INTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)*IS(qq5)
-xxsub4*INTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
-xxsub8*INTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)*IS(qq5)
-xxsub11*INTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)
-xxsub3*INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
-xxsub9*INTbt31(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub5*INTdb220e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
-xxsub6*INTdb210b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
-xxsub10*INTbt22e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)
;
#include- INT_5g+++++.prc
id INTdb310(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = 0;
id INTdb210b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = 0;
id INTbt31(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = 0;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;
.sort

#write <bt21Db.input.frm> "G AMP = %e" cutAMPbt21b

topolynomial;
.sort
format maple;
#write <bt21Db.input.mpl> "INT_bt21Db := proc(k1,k2,mu11,mu12,mu22)"
#write <bt21Db.input.mpl> "local Z;"
#write <bt21Db.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt21Db.input.mpl> "%X"
#write <bt21Db.input.mpl> "return %e" cutAMPbt21b
#write <bt21Db.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt22e =
#do i=5,8
+xxdiag'i'*diag'i'*IS(qq5)*IS(kq)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'*IS(qq5)
#enddo
#do i=68,76
+xxdiag'i'*diag'i'
#enddo
-xxsub1*INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
-xxsub2*INTdb220e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
-xxsub3*INTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq5)
;
#include- INT_5g+++++.prc
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p3+p4+p5) = s12/2;
id dot(k2,p3) = (s12-s45)/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

B xxdiag1,...,xxdiag89;
print[];
.sort

topolynomial;
.sort
format maple;
#write <bt22De.input.mpl> "INT_bt22De := proc(k1,k2,mu11,mu12,mu22)"
#write <bt22De.input.mpl> "local Z;"
#write <bt22De.input.mpl> "global p1,p2,p3,p4,p5;"
#write <bt22De.input.mpl> "%X"
#write <bt22De.input.mpl> "return %e" cutAMPbt22e
#write <bt22De.input.mpl> "end:"
frompolynomial;
.end
