L c000 = 1/2*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d121 + NUM(
       - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d120 + 1/2*NUM( - 4*S(
      p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg))*d122;

L c100 = 1/2*S(p1)*IS(p1)*IWRAP(gg)*d131 + S(p1)*IS(p1)*IWRAP(gg)*d130 + 1/2*
      IS(p1)*d113 - IS(p1)*d111;

L c010 = 1/2*S(p1)*IS(p1)*IWRAP(gg)*I*d131 + S(p1)*IS(p1)*IWRAP(gg)*I*d130 - 1/
      2*IS(p1)*I*d113 + IS(p1)*I*d111;

L c001 =  - 1/2*IS(p1)*d122 - 1/2*IS(p1)*d121;

L c110 = 2*S(p1)^2*IS(p1)^2*IWRAP(gg)^2*I*d140 - 2*IS(p1)^2*I*d102;

L c011 =  - 1/2*S(p1)*IS(p1)^2*IWRAP(gg)*I*d131 + 1/2*IS(p1)^2*I*d113;

L c101 =  - 1/2*S(p1)*IS(p1)^2*IWRAP(gg)*d131 - 1/2*IS(p1)^2*d113;

L c2x2 = S(p1)^4*IS(p1)^4*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(gg)
      )*WRAP(gg)*IWRAP(gg)^3*d140 + S(p1)^2*IS(p1)^4*NUM( - 8*S(p1) + WRAP(gg)
      )*INV( - 8*S(p1) + WRAP(gg))*d102 + S(p1)*IS(p1)^2*INV( - 8*S(p1) + 
      WRAP(gg))*d122;

L cx22 =  - S(p1)^4*IS(p1)^4*NUM( - 8*S(p1) + WRAP(gg))*INV( - 8*S(p1) + WRAP(
      gg))*WRAP(gg)*IWRAP(gg)^3*d140 - S(p1)^2*IS(p1)^4*NUM( - 8*S(p1) + WRAP(
      gg))*INV( - 8*S(p1) + WRAP(gg))*d102 + S(p1)*IS(p1)^2*INV( - 8*S(p1) + 
      WRAP(gg))*d122;

L zero1 = d104 - d102;

L zero2 = d112 + d113 + d111;

L zero3 = d103 + 2*d102;

L zero4 = d202 - S(p1)^2*IWRAP(gg)^2*d140;

L zero5 = d203 + 2*S(p1)^2*IWRAP(gg)^2*d140;

L zero6 = d204 - S(p1)^2*IWRAP(gg)^2*d140;

L zero7 = d211 - S(p1)*IWRAP(gg)*d130;

L zero8 = d212 - S(p1)*IWRAP(gg)*d131 + S(p1)*IWRAP(gg)*d130;

L zero9 = d213 + S(p1)*IWRAP(gg)*d131;

L zero10 = d220 - d120;

L zero11 = d221 - d121;

L zero12 = d222 - d122;

L zero13 = S(p1)*IS(p1)*d230 - IS(p1)*WRAP(gg)*d111;

L zero14 = S(p1)*IS(p1)*d231 + IS(p1)*WRAP(gg)*d113;

