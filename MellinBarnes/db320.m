(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];

(* Set Ds = 4 for FDH scheme *)
Dsm2 = 2;

INTdb320mu[MBrepr_] := {
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,1,3},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,3,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,2,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,3,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,1,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,2,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,3,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,3,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,2,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,3,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,3,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,2,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{3,1,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (- 8*(1 + 2*eps)*INT[6 - 2*eps,{1,1,1,1,1,1,1,1},MBrepr]*eps),
       MBresolve[#,eps] & /@ (+ 3*INT[6 - 2*eps,{1,1,1,1,1,1,1,1},MBrepr]*eps^2*Dsm2)
       };

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];


invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> -ms12/2, p[1].p[3] -> -(ms45-ms12-ms23)/2, p[1].p[4] -> -(ms23-ms15-ms45)/2, p[1].p[5] -> -ms15/2,
      p[2].p[3] -> -ms23/2, p[2].p[4] -> -(ms15-ms23-ms34)/2, p[2].p[5] -> -(ms34-ms12-ms15)/2,
      p[3].p[4] -> -ms34/2, p[3].p[5] -> -(ms12-ms34-ms45)/2,
      p[4].p[5] -> -ms45/2};

kinpt = {{ms12->1., ms23->3., ms34->11., ms45->19., ms15->31., s12->-1., s23->-3., s34->-11., s45->-19., s15->-31., tr5->-Sqrt[154429.]}}


db320[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[1]+l[2],0,a8]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

ord = {{1,2,3,4,5}, {2,3,4,5,1}, {3,4,5,1,2}, {4,5,1,2,3}, {5,1,2,3,4}};
db320F = Table[0,{k,1,5}]


db320F[[1]] = INTdb320mu[Apply[db320,ord[[1]]]];


maxeps=-1
npt=1;
Rdb320 = Table[0,{k,1,19}]
SetVerbosity[1];


Do[
tmp = MBexpand[db320F[[1,part]],1/cGamma2,{eps,0,maxeps}];
tmp = Process[tmp,{4,3}];
Rdb320[[part]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16, MaxCuhreDim->3, MaxPoints->1000000, MaxRecursion->3000];,
{part,1,19}]


DumpSave["Output_db320.mx",Rdb320]


Rdb320 >> "test.out" 
