#define nsols "1"
#if 'sol'=1
id a1 = 0;
id a2 = 0;
id a3 = a3;
id a4 = a4;
id b1 = 0;
id b2 = 0;
id b3 = b3;
id b4 = b4;
id mu11 = -a3*a4*s12;
id mu12 = (a3*b3*s12^2+2*a3*b3*s12*s14+a3*b3*s14^2-a4*b3*s14^2-a3*b4*s14^2+a4*b4*s14^2+a3*s12*s14+b3*s12*s14+a3*s14^2-a4*s14^2+b3*s14^2-b4*s14^2+s14^2)/s14;
id mu22 = -b3*b4*s12;
#endif

