* vim: set foldmethod=marker:
#-
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,c,p,x,P,h,e,q,s;
S mS;
S k1f,k2f,k3f,k4f,k1,k2,k3;
S mu;
S l1,...,l20;
CF FRAC,N;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

off stats;

.global
#define sol "0"

#do topo={boxboxbox,tcourt}

#write "### on shell solutions for 'topo' ###\n"

#include- final/'topo'.OSprops.h
#include- 'topo'.OSsol.h

#write <'topo'.tblA.tex> "\% 'topo'-A"
#write <'topo'.tblA.tex> "\bbegin{array}{|c|c|c|c|c|}"
#write <'topo'.tblA.tex> "soln. & a_1 & a_2 & a_3 & a_4 \b\b"

#write <'topo'.tblB.tex> "\% 'topo'-B"
#write <'topo'.tblB.tex> "\bbegin{array}{|c|c|c|c|c|}"
#write <'topo'.tblB.tex> "soln. & b_1 & b_2 & b_3 & b_4 \b\b"

#write <'topo'.tblC.tex> "\% 'topo'-C"
#write <'topo'.tblC.tex> "\bbegin{array}{|c|c|c|c|c|}"
#write <'topo'.tblC.tex> "soln. & c_1 & c_2 & c_3 & c_4 \b\b"

#write <'topo'.OSsol.log> "k1:=Array(1..'nsols'):"
#write <'topo'.OSsol.log> "k2:=Array(1..'nsols'):"
#write <'topo'.OSsol.log> "k3:=Array(1..'nsols'):"

#do i=1,4
#write <'topo'.OSsol.log> "av'i':=%E:" av'i'
#write <'topo'.OSsol.log> "bv'i':=%E:" bv'i'
#write <'topo'.OSsol.log> "cv'i':=%E:" cv'i'
#enddo

#do sol=1,'nsols'

#write "### 'topo' solution 'sol' ###\n"

#do k=1,4
L a'k''sol' = a'k';
L b'k''sol' = b'k';
L c'k''sol' = c'k';
#enddo

#include- final/'topo'.OSprops.h
#include- 'topo'.OSsol.h
id N(2156) = N(11)*N(14)*N(14);
id N(1331) = N(11)*N(11)*N(11);
id N(588) = N(3)*N(14)*N(14);
id N(363) = N(3)*N(11)*N(11);
id N(196) = N(14)*N(14);
id N(154) = N(11)*N(14);
id N(121) = N(11)*N(11);
id N(33) = N(3)*N(11);
denominators INV;
argument INV;
id N(2156) = N(11)*N(14)*N(14);
id N(1331) = N(11)*N(11)*N(11);
id N(588) = N(3)*N(14)*N(14);
id N(363) = N(3)*N(11)*N(11);
id N(196) = N(14)*N(14);
id N(154) = N(11)*N(14);
id N(121) = N(11)*N(11);
id N(33) = N(3)*N(11);
endargument;

id N(3) = S(p1,p2);
id N(11) = S(p1,p4);
id N(14) = -S(p1,p3);
argument NUM,INV;
id N(3) = S(p1,p2);
id N(11) = S(p1,p4);
id N(14) = -S(p1,p3);
endargument;

*** explicitly perform tau shifts ***

#if 'topo'=boxboxbox

  #if 'sol'=1
    multiply replace_(b4,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,IS(p1,p4)*(tau2-S(p1,p3)));
  #elseif 'sol'=2
    multiply replace_(a3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,S(p1,p3)*IS(p1,p4)*(tau2-1));
  #elseif 'sol'=3
    multiply replace_(b4,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,IS(p1,p4)*(tau2-S(p1,p3)));
  #elseif 'sol'=4
    multiply replace_(b4,tau1);
    multiply replace_(c4,tau2);
  #elseif 'sol'=5
    multiply replace_(b4,tau1);
    multiply replace_(c4,tau2);
  #elseif 'sol'=6
    multiply replace_(a4,tau1);
    multiply replace_(c4,tau2);
  #elseif 'sol'=7
    multiply replace_(a4,tau1);
    multiply replace_(b4,tau2);
  #elseif 'sol'=8
    multiply replace_(a3,tau1);
    multiply replace_(b3,tau2);
  #elseif 'sol'=9
    multiply replace_(a3,tau1);
    multiply replace_(c3,tau2);
  #elseif 'sol'=10
    multiply replace_(b4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(tau2,IS(p1,p3)*(tau2+S(p1,p4)));
  #elseif 'sol'=11
    multiply replace_(b3,tau1);
    multiply replace_(c3,tau2);
  #elseif 'sol'=12
    multiply replace_(b4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(tau2,S(p1,p4)*IS(p1,p3)*(tau2+1));
  #elseif 'sol'=13
    multiply replace_(b4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(tau2,S(p1,p3)*IS(p1,p4)*(tau1+1));
  #elseif 'sol'=14
    multiply replace_(a4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(tau2,tau2-1);
  #endif

#elseif 'topo'=tcourt
  #if 'sol'=1
    multiply replace_(a3,tau1);
    multiply replace_(c3,tau2);
  #elseif 'sol'=2
    multiply replace_(a3,tau1);
    multiply replace_(c3,tau2);
  #elseif 'sol'=3
    multiply replace_(b3,tau1);
    multiply replace_(c1,tau2);
    multiply replace_(c3,tau3);
  #elseif 'sol'=4
    multiply replace_(b3,tau1);
    multiply replace_(c2,tau2);
    multiply replace_(c3,tau3);
  #elseif 'sol'=5
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,tau2-1);
  #elseif 'sol'=6
    multiply replace_(b4,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,S(p1,p3)*IS(p1,p4)*(tau1-1));
  #elseif 'sol'=7
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau2,tau2+1);
  #elseif 'sol'=8
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau1,tau1-tau2);
  #elseif 'sol'=9
    multiply replace_(b3,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(tau1,tau1-1);
  #elseif 'sol'=10
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
  #elseif 'sol'=11
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau1,S(p1,p4)*IS(p1,p3)*(tau1+1));
  #elseif 'sol'=12
    multiply replace_(b4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(c4,tau3);
  #elseif 'sol'=13
    multiply replace_(a4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(c4,tau3);
  #elseif 'sol'=14
    multiply replace_(b4,tau1);
    multiply replace_(c3,tau2);
    multiply replace_(c4,tau3);
  #elseif 'sol'=15
    multiply replace_(a4,tau1);
    multiply replace_(c4,tau2);
  #elseif 'sol'=16
    multiply replace_(c3,tau1);
    multiply replace_(c4,tau2);
    multiply replace_(tau1,S(p1,p4)*IS(p1,p3)*(tau1-1));
  #endif

#endif

denominators INV;

argument NUM,INV;
#call cancel
endargument;
#call cancel

id NUM(x?) = x;
#call cancel
B tau,tau1,tau2,tau3,INV;
.sort
collect NUM;
#call SortCollect

argument NUM,INV;
id S(p1,p3) = -S(p1,p2)-S(p1,p4);
endargument;
#call sortNUM
#call cancel

id NUM(x?) = x;
multiply xxx;
B INV,xxx;
.sort
collect NUM;
#call wrap(tau);
#call wrap(tau1);
#call wrap(tau2);
#call wrap(tau3);
#call ISortCollect
#call SortCollect
#call unwrap(tau);
#call unwrap(tau1);
#call unwrap(tau2);
#call unwrap(tau3);
id xxx=1;
.sort

id INV(S(p1,p2)+S(p1,p4)) = -IS(p1,p3);
#call cancel

*** more tau shifts ***
*** getting messy now ***
id 1/tau1 = INV(tau1);

#if 'topo'=boxboxbox
  #if 'sol'=1
    multiply replace_(tau1,tau2*(tau1-1)*INV(S(p1,p2)+tau2));
  #elseif 'sol'=3
    multiply replace_(tau1,tau2*(tau1-1)*INV(S(p1,p2)+tau2));
  #elseif 'sol'=10
  #endif
#endif

argument NUM,INV;
#call cancel
splitarg INV;
repeat id tau2*INV(tau2,S(p1,p2)) = ( 1-S(p1,p2)*INV(tau2,S(p1,p2)) );
endargument;
#call cancel
#call sortNUM
#call sortINV

id 1/tau2 = INV(tau2);

#if 'topo'=boxboxbox
  #if 'sol'=1
    multiply replace_(tau2,S(p1,p2)*tau2*INV(1-tau2));
  #elseif 'sol'=3
  #elseif 'sol'=10
  #endif
#endif

argument NUM,INV;
#call cancel
repeat id tau2*INV(1-tau2) = ( -1 + INV(1-tau2) );
endargument;
#call cancel
#call sortINV
id INV(INV(?x)) = NUM(?x);
#call sortNUM
#call cancel

id NUM(x?) = x;
#call cancel
B tau,tau1,tau2,tau3,INV;
.sort
collect NUM;
#call SortCollect

argument NUM,INV;
id S(p1,p3) = -S(p1,p2)-S(p1,p4);
endargument;
#call sortNUM
#call cancel

argument INV;
topolynomial;
endargument;
.sort
factarg INV;
.sort
argument INV;
frompolynomial;
endargument;
chainout INV;
#call sortINV;
id INV( - 1 + INV(1 - tau2)) =- NUM(-1+tau2)/tau2;
#call sortNUM
#call cancel

format 150;
print+s;
.sort

id IS(p1?,p2?) = INV(S(p1,p2));
id S(p1?,p2?) = NUM(S(p1,p2));
id 1/tau = INV(tau);
id 1/tau1 = INV(tau1);
id 1/tau2 = INV(tau2);
id 1/tau3 = INV(tau3);
id tau = NUM(tau);
id tau1 = NUM(tau1);
id tau2 = NUM(tau2);
id tau3 = NUM(tau3);

multiply FRAC(1,1);
repeat id FRAC(x1?,x2?)*NUM(x3?) = FRAC(x1*NUM(x3),x2);
repeat id FRAC(x1?,x2?)*INV(x3?) = FRAC(x1,x2*INV(x3));
id FRAC(1,1) = 1;
id FRAC(x?,1) = NUM(x);
.sort

format 200;
#write <'topo'.tblA.tex> "'sol'"
#do i=1,3
#write <'topo'.tblA.tex> " & %E" a'i''sol'
#enddo
#write <'topo'.tblA.tex> " & %E \b\b" a4'sol'

#write <'topo'.tblB.tex> "'sol'"
#do i=1,3
#write <'topo'.tblB.tex> " & %E" b'i''sol'
#enddo
#write <'topo'.tblB.tex> " & %E \b\b" b4'sol'

#write <'topo'.tblC.tex> "'sol'"
#do i=1,3
#write <'topo'.tblC.tex> " & %E" c'i''sol'
#enddo
#write <'topo'.tblC.tex> " & %E \b\b" c4'sol'


id FRAC(x1?,x2?) = x1*x2;
argument NUM;
id NUM(tau1?{tau,tau1,tau2}) = tau1;
endargument;
#call sortNUM
.sort
format maple;
#write <'topo'.OSsol.log> "k1['sol']:="
#do i=1,4
#write <'topo'.OSsol.log> "+(%E)*av'i' " a'i''sol'
#enddo
#write <'topo'.OSsol.log> ":"
#write <'topo'.OSsol.log> "k2['sol']:="
#do i=1,4
#write <'topo'.OSsol.log> "+(%E)*bv'i' " b'i''sol'
#enddo
#write <'topo'.OSsol.log> ":"
#write <'topo'.OSsol.log> "k3['sol']:="
#do i=1,4
#write <'topo'.OSsol.log> "+(%E)*cv'i' " c'i''sol'
#enddo
#write <'topo'.OSsol.log> ":"

format 150;
B INV;
print+s;
.sort
hide;

L kloop1 =
#do i=1,4
+a'i''sol'*av'i'
#enddo
;
L kloop2 =
#do i=1,4
+b'i''sol'*bv'i'
#enddo
;
L kloop3 =
#do i=1,4
+c'i''sol'*cv'i'
#enddo
;
.sort

#write <final/'topo'.OSsol.final.h> "id k1'sol' = %e" kloop1
#write <final/'topo'.OSsol.final.h> "id k2'sol' = %e" kloop2
#write <final/'topo'.OSsol.final.h> "id k3'sol' = %e" kloop3

.store
#enddo

#write <'topo'.tblA.tex> "\bend{array}"
#write <'topo'.tblB.tex> "\bend{array}"
#write <'topo'.tblC.tex> "\bend{array}"

.store

#enddo

.end

