(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

maxeps = 2
npt = 1

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];
NUM[x_] := x;

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];

(*Series[cGamma1,{eps,0,2}]*)
(*Series[Exp[-eps*EulerGamma],{eps,0,2}]*)
(*Series[cGamma2,{eps,0,2}]*)
(*Series[Exp[-2*eps*EulerGamma],{eps,0,2}]*)

invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> -s12/2, p[1].p[3] -> -(s45-s12-s23)/2, p[1].p[4] -> -(s23-s15-s45)/2, p[1].p[5] -> -s15/2,
      p[2].p[3] -> -s23/2, p[2].p[4] -> -(s15-s23-s34)/2, p[2].p[5] -> -(s34-s12-s15)/2,
      p[3].p[4] -> -s34/2, p[3].p[5] -> -(s12-s34-s45)/2,
      p[4].p[5] -> -s45/2};

kinpt = {
{s12 -> 20.2290755631707604, s23 -> 42.0000000000000000, s34 -> -24.7243908366266393, s45 -> 66.4581511263415208, s15 -> -7.4487816732532786, tr5 -> -444.0227329401300486*I}
};

box[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]+p[p4]+p[p5],0,a4]},
	{l[1]}, Invariants->invariants];

pent = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[1],0,a2] PR[l[1]-p[1]-p[2],0,a3] PR[l[1]-p[1]-p[2]-p[3],0,a4] PR[l[1]+p[5],0,a5]},
	{l[1]}, Invariants->invariants];

pentF = {MBresolve[#,eps] & /@ (eps*(-1+eps)*(-2+eps)*INT[10 - 2*eps,{1,1,1,1,1},pent])};
ord = {{1,2,3,4,5}, {2,3,4,5,1}, {3,4,5,1,2}, {4,5,1,2,3}, {5,1,2,3,4}};
boxF = Table[0,{k,1,5}]
Do[
  boxF[[k]] = {MBresolve[#,eps] & /@ (eps*(-1+eps)*INT[8 - 2*eps,{1,1,1,1}, Apply[box,ord[[k]]]])};,
{k,1,5}]

SetVerbosity[1];
MBexpand[pentF,1/cGamma1,{eps,0,maxeps}];
Process[%,{5,4,3}];
ResPent = MBintegrate[%, kinpt[[npt]], FixedContours->True];
ResBox = Table[0,{k,1,5}]
Do[
  tmp = MBexpand[boxF[[k]],1/cGamma1,{eps,0,maxeps}];
  ResBox[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True];,
{k,1,5}]


A51 = s12*s23*ResBox[[1,1]] + s23*s34*ResBox[[1,1]] + s34*s45*ResBox[[1,1]] + s45*s15*ResBox[[1,1]] + s15*s12*ResBox[[1,1]] +
      2*tr5*ResPent[[1]] /. kinpt[[npt]];
Series[%,{eps,0,maxeps}]

A514D = -1/6*(s12*s23 + s23*s34 + s34*s45 + s45*s15 + s15*s12 + tr5) /. kinpt[[npt]]
