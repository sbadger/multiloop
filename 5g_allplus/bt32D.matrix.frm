#-
#include- 2to3tools.prc

#define topo "bt32D"

.global

G basis41(nu1) = MOM(p1,nu1);
G basis42(nu1) = MOM(p2,nu1);
G basis43(nu1) = MOM(p3,nu1);
G basis44(nu1) = MOM(p5,nu1);

.store

G x14 = 2*MOM(k1,nu1)*basis44(nu1);
G x22 = 2*MOM(k2,nu1)*basis42(nu1);
G x21 = 2*MOM(k2,nu1)*basis41(nu1);
G xmu11 = mu11;
G xmu12 = mu12;
G xmu22 = mu22;

L zero1 = MOM(k1,nu1)*MOM(k1,nu1)-mu11;
L zero2 = MOM(k1,nu1)*MOM(p1,nu1);
L zero3 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)-s12/2;
L zero4 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)+MOM(k1,nu1)*MOM(p3,nu1)-s45/2;
L zero5 = MOM(k2,nu1)*MOM(p4,nu1)+MOM(k2,nu1)*MOM(p5,nu1)-s45/2;
L zero6 = MOM(k2,nu1)*MOM(p5,nu1);
L zero7 = MOM(k2,nu1)*MOM(k2,nu1)-mu22;

#call subsLmom(1,'topo')

id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id MOM(p1?,nu1?)*AB(p2?,nu1?,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?,p2?)*AB(p3?,nu1?,p4?) = -2*A(p1,p3)*B(p2,p4);
id NUM(x?) = x;

#call cancel
#call simplify5
id tr5 = TR5;
*** uncomment these lines to convert to mom. twistor variables
*#call changevars
*#call simplify5new

format 150;
print+s;
.store

G Delta = Basis'topo';

#include- basis/'topo'.basis.h

id Power(x1?,x2?) = x1^x2;

id mu11 = xmu11;
id mu12 = xmu12;
id mu22 = xmu22;
.sort

B tau1,tau2,tau3,tau4;
print[];
.sort

#$nc = 1;
B cc;
.sort
keep brackets;
id cc(?x) = cc($nc,?x);
$nc=$nc+1;
.sort
#write "{'$nc'-1} integrand coefficients"
#do cc=1,{'$nc'-1}
id cc('cc',?x$clbl) = cc('cc',?x);
.sort
#write <final/'topo'.coeffvecs.mpl> "Clbl['cc']:=cs('$clbl'):"
#enddo

.store

#write <'topo'.ReduceM.log> "ReduceMatrix:=Matrix({'$nc'-1},{'$nc'-1}):"
#write <final/'topo'.coeffvecs.mpl> "Dvec:=Vector(85):"
#define dd "0"

#do pow1=0,6
#do pow2=0,6
#do pow3=0,6
#do pow4=0,6

#$nonzero=0;
L tmpRow = Delta;

#do pp=1,4
if(count(tau'pp',1)!=`pow`pp'');
discard;
endif;
id tau`pp'=1;
id 1/tau`pp'=1;
#enddo

.sort
$nonzero=$nonzero+1;
.sort
hide;

*#write "'pow1','pow2','pow3','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "Dvec['dd']:=ds('pow1','pow2','pow3','pow4'):"
#write <final/'topo'.coeffvecs.prc> "id ds('pow1','pow2','pow3','pow4') = ds('dd');"

#do cc=1,{'$nc'-1}
  L tmpEl'cc' = tmpRow;
  id cc('cc',?x$clbl) = 1;
  id cc(?x)=0;
  .sort
  format maple;
  #write <'topo'.ReduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
  .sort
  hide;
#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo
#enddo

.end
