* vim: set foldmethod=marker:
#-
S s,t,u,tau,tau1,...,tau4;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,p,x,P,h,e,q,s;
S mS;
S k1f,k2f,k3f,k4f,k1,k2;
S mu,mu11,mu12,mu22;
S l1,...,l20;
CF FRAC,N;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

off stats;

.global
#define sol "0"

*#do topo={boxboxD}
#define topo "boxboxD"

#write "### on shell solutions for 'topo' ###\n"

#include- final/'topo'.OSprops.h
#include- 'topo'.OSsol.h

#write <'topo'.tblA.tex> "\% 'topo'-A"
#write <'topo'.tblA.tex> "\bbegin{array}{|c|c|c|c|c|}"
#write <'topo'.tblA.tex> "soln. & a_1 & a_2 & a_3 & a_4 \b\b"

#write <'topo'.tblB.tex> "\% 'topo'-B"
#write <'topo'.tblB.tex> "\bbegin{array}{|c|c|c|c|c|}"
#write <'topo'.tblB.tex> "soln. & b_1 & b_2 & b_3 & b_4 \b\b"

#write <'topo'.OSsol.log> "k1:=Array(1..'nsols'):"
#write <'topo'.OSsol.log> "k2:=Array(1..'nsols'):"

#do i=1,4
#write <'topo'.OSsol.log> "av'i':=%E:" av'i'
#write <'topo'.OSsol.log> "bv'i':=%E:" bv'i'
#enddo

.store

#do sol=1,'nsols'

#write "### 'topo' solution 'sol' ###\n"

#do k=1,4
L a'k''sol' = a'k';
L b'k''sol' = b'k';
#enddo
L mu11'sol' = mu11;
L mu12'sol' = mu12;
L mu22'sol' = mu22;

#include- final/'topo'.OSprops.h
#include- 'topo'.OSsol.h
id N(196) = N(14)*N(14);
id 1/N(196) = 1/N(14)/N(14);
id N(121) = N(11)*N(11);
id 1/N(121) = 1/N(11)/N(11);
id N(33) = N(3)*N(11);
id 1/N(33) = 1/N(3)/N(11);
denominators INV;

id N(3) = S(p1,p2);
id N(11) = S(p1,p4);
id N(14) = -S(p1,p3);
id 1/N(3) = IS(p1,p2);
id 1/N(11) = IS(p1,p4);
id 1/N(14) = -IS(p1,p3);
argument NUM,INV;
id N(3) = S(p1,p2);
id N(11) = S(p1,p4);
id N(14) = -S(p1,p3);
id 1/N(3) = IS(p1,p2);
id 1/N(11) = IS(p1,p4);
id 1/N(14) = -IS(p1,p3);
endargument;

*** explicitly perform tau shifts ***

#if 'topo'=boxboxD

  multiply replace_(a3,tau1);
  multiply replace_(a4,tau2);
  multiply replace_(b3,tau3);
  multiply replace_(b4,tau4);

#endif

argument NUM,INV;
#call cancel
endargument;
#call cancel

id NUM(x?) = x;
#call cancel
B tau,tau1,tau2,tau3,tau4,INV;
.sort
collect NUM;
#call SortCollect

id NUM(S(p1,p3) + S(p1,p4)) = -S(p1,p2);
id NUM(S(p1,p3) + S(p1,p2)) = -S(p1,p4);

id NUM(x?) = x;
multiply xxx;
B INV,xxx;
.sort
collect NUM;
#call wrap(tau);
#call wrap(tau1);
#call wrap(tau2);
#call wrap(tau3);
#call wrap(tau4);
#call ISortCollect
#call SortCollect
#call unwrap(tau);
#call unwrap(tau1);
#call unwrap(tau2);
#call unwrap(tau3);
#call unwrap(tau4);
id xxx=1;
.sort

id IS(p1?,p2?) = INV(S(p1,p2));
id IAB(?x) = INV(AB(?x));
id S(p1?,p2?) = NUM(S(p1,p2));
id AB(?x) = NUM(AB(?x));
id 1/tau = INV(tau);
id 1/tau1 = INV(tau1);
id 1/tau2 = INV(tau2);

multiply FRAC(1,1);
repeat id FRAC(x1?,x2?)*NUM(x3?) = FRAC(x1*NUM(x3),x2);
repeat id FRAC(x1?,x2?)*INV(x3?) = FRAC(x1,x2*INV(x3));
id FRAC(1,1) = 1;
id FRAC(x?,1) = NUM(x);
id NUM(NUM(x?)) = NUM(x);
.sort

format 200;
#write <'topo'.tblA.tex> "'sol'"
#do i=1,3
#write <'topo'.tblA.tex> " & %E" a'i''sol'
#enddo
#write <'topo'.tblA.tex> " & %E \b\b" a4'sol'

#write <'topo'.tblB.tex> "'sol'"
#do i=1,3
#write <'topo'.tblB.tex> " & %E" b'i''sol'
#enddo
#write <'topo'.tblB.tex> " & %E \b\b" b4'sol'

id FRAC(x1?,x2?) = x1*x2;
id NUM(tau1?{tau,tau1,tau2}) = tau1;
.sort
format maple;
#write <'topo'.OSsol.log> "k1['sol']:="
#do i=1,4
#write <'topo'.OSsol.log> "+(%E)*av'i' " a'i''sol'
#enddo
#write <'topo'.OSsol.log> ":"
#write <'topo'.OSsol.log> "k2['sol']:="
#do i=1,4
#write <'topo'.OSsol.log> "+(%E)*bv'i' " b'i''sol'
#enddo
#write <'topo'.OSsol.log> ":"

format 150;
B INV,tau1,tau2,tau;
print+s;
.sort
hide;

L kloop1 =
#do i=1,4
+a'i''sol'*av'i'
#enddo
;
L kloop2 =
#do i=1,4
+b'i''sol'*bv'i'
#enddo
;
.sort

#write <final/'topo'.OSsol.final.h> "id k1'sol' = %e" kloop1
#write <final/'topo'.OSsol.final.h> "id k2'sol' = %e" kloop2
#write <final/'topo'.OSsol.final.h> "id mu11'sol' = %e" mu11'sol'
#write <final/'topo'.OSsol.final.h> "id mu12'sol' = %e" mu12'sol'
#write <final/'topo'.OSsol.final.h> "id mu22'sol' = %e" mu22'sol'

.store
#enddo

#write <'topo'.tblA.tex> "\bend{array}"
#write <'topo'.tblB.tex> "\bend{array}"

.store

*#enddo

.end

