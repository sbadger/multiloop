id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,-1),ptl(p4?,1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,-1),ptl(p4?,1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,-1),ptl(p4?,1,1)) =
	  - A(p1,p3)*IA(p1,p2)^2*IA(p2,p3)*IA(p3,p4)^2*NUM(A(p1,p2)^2*A(p3,p4)^2 - A(p1,p3)*A(p1,p4)*A(p2,p3)*A(p2,p4) + A(p1,p4)^2*A(p2,p3)^2)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,-1),ptl(p4?,-1,1)) =
	  - A(p1,p3)*IA(p1,p2)^2*IA(p2,p3)*IA(p3,p4)^2*NUM(A(p1,p2)^2*A(p3,p4)^2 - A(p1,p3)*A(p1,p4)*A(p2,p3)*A(p2,p4) + A(p1,p4)^2*A(p2,p3)^2)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,-1),ptl(p4?,1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,-1),ptl(p4?,1,1)) =
	 A(p2,p4)*IA(p1,p2)^2*IA(p1,p4)*IA(p3,p4)^2*NUM(A(p1,p2)^2*A(p3,p4)^2 - A(p1,p3)*A(p1,p4)*A(p2,p3)*A(p2,p4) + A(p1,p4)^2*A(p2,p3)^2)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,-1),ptl(p4?,-1,1)) =
	 A(p2,p4)*IA(p1,p2)^2*IA(p1,p4)*IA(p3,p4)^2*NUM(A(p1,p2)^2*A(p3,p4)^2 - A(p1,p3)*A(p1,p4)*A(p2,p3)*A(p2,p4) + A(p1,p4)^2*A(p2,p3)^2)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,-1),ptl(p4?,1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,-1),ptl(p4?,1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,-1),ptl(p4?,-1,1)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,-1),ptl(p4?,1,1)) =
	  0;

