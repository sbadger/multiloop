#-
#include ../2to3tools.prc

CF F1,F2,F3;

off stats;

#procedure ExpandSpinorStrings
repeat;
id,once AB(p1?,p2?ps,?args) = A(p1,p2)*BB(p2,?args);
al,once AA(p1?,p2?ps,?args) = A(p1,p2)*BA(p2,?args);
al,once BB(p1?,p2?ps,?args) = B(p1,p2)*AB(p2,?args);
al,once BA(p1?,p2?ps,?args) = B(p1,p2)*AA(p2,?args);
al,once IAB(p1?,p2?ps,?args) = IA(p1,p2)*IBB(p2,?args);
al,once IAA(p1?,p2?ps,?args) = IA(p1,p2)*IBA(p2,?args);
al,once IBB(p1?,p2?ps,?args) = IB(p1,p2)*IAB(p2,?args);
al,once IBA(p1?,p2?ps,?args) = IB(p1,p2)*IAA(p2,?args);

id BB(p1?,p2?) = B(p1,p2);
id IBB(p1?,p2?) = IB(p1,p2);
id AA(p1?,p2?) = A(p1,p2);
id IAA(p1?,p2?) = IA(p1,p2);

endrepeat;
id BA(?args) = AB(reverse_(?args));
id IBA(?args) = IAB(reverse_(?args));
#call cancel
#endprocedure

.global

L Delta221 = IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    NUM((Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-mu11*mu22))*(
      a0 + a1*dot(k1,p4) + a2*dot(k2,p1) + a3*dot(k2,p5)
    ));
#include db221_coeffs.prc
.sort

L Delta311 = IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    NUM((Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-mu11*mu22))*(
      a0 + a1*dot(k2,p5)
    ));

id NUM((Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-mu11*mu22)) = F1(Ds,mu11,mu12,mu22);

#include db311_coeffs.prc
denominators INV;
.sort

B INV;
.sort
keep brackets;
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

multiply tr5;
multiply A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1);
.sort
#call cancel

#call ExpandSpinorStrings
AB  s12,s13,s14,s15,
    s23,s24,s25,
    s34,s35,
    s45,tr5,xx0,...,xx10;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);
#call cancel

id NUM( - s34*s45 + s23*s34 + s15*s45 + s12*s23 - s12*s15 - tr5) = -2*BA(p1,p2,p3,p4,p1);
id NUM( - s34*s45 + s23*s34 + s15*s45 - s12*s23 - s12*s15 - tr5) = 2*BA(p1,p2,p3,p5,p1);
id NUM( - s34*s45 + s23*s34 + s15*s45 - s12*s23 + s12*s15 - tr5) = -2*BA(p1,p2,p4,p5,p1);
id NUM(s34*s45 - s23*s34 + s15*s45 + s12*s23 - s12*s15 + tr5) = -2*BA(p1,p3,p4,p5,p1);
id NUM( - s34*s45 - s23*s34 + s15*s45 + s12*s23 - s12*s15 + tr5) = 2*BA(p2,p3,p4,p5,p2);
id NUM( - s34*s45 + s23*s34 + s15*s45 + s12*s23 - s12*s15 + tr5) = -2*AB(p1,p2,p3,p4,p1);
id NUM( - s34*s45 + s23*s34 + s15*s45 - s12*s23 - s12*s15 + tr5) = 2*AB(p1,p2,p3,p5,p1);
id NUM( - s34*s45 + s23*s34 + s15*s45 - s12*s23 + s12*s15 + tr5) = -2*AB(p1,p2,p4,p5,p1);
id NUM(s34*s45 - s23*s34 + s15*s45 + s12*s23 - s12*s15 - tr5) = -2*AB(p1,p3,p4,p5,p1);
id NUM( - s34*s45 - s23*s34 + s15*s45 + s12*s23 - s12*s15 - tr5) = 2*AB(p2,p3,p4,p5,p2);

B NUM,dot;
print+s;
.end
.store

L [2*BA(p1,p2,p3,p4,p1)] = 2*BA(p1,p2,p3,p4,p1);
L [2*BA(p1,p2,p3,p5,p1)] = 2*BA(p1,p2,p3,p5,p1);
L [2*BA(p1,p2,p4,p5,p1)] = 2*BA(p1,p2,p4,p5,p1);
L [2*BA(p1,p3,p4,p5,p1)] = 2*BA(p1,p3,p4,p5,p1);
L [2*BA(p2,p3,p4,p5,p2)] = 2*BA(p2,p3,p4,p5,p2);

L [2*AB(p1,p2,p3,p4,p1)] = 2*AB(p1,p2,p3,p4,p1);
L [2*AB(p1,p2,p3,p5,p1)] = 2*AB(p1,p2,p3,p5,p1);
L [2*AB(p1,p2,p4,p5,p1)] = 2*AB(p1,p2,p4,p5,p1);
L [2*AB(p1,p3,p4,p5,p1)] = 2*AB(p1,p3,p4,p5,p1);
L [2*AB(p2,p3,p4,p5,p2)] = 2*AB(p2,p3,p4,p5,p2);

#call ExpandSpinorStrings
#call simplify5
id NUM(x?) = x;
AB  s12,s13,s14,s15,
    s23,s24,s25,
    s34,s35,
    s45,tr5,xx0,...,xx10;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);

format 150;
B dot,mu11,mu12,mu22,Ds,NUM;
print;
.end
