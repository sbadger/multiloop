* vim: set sw=2:
* vim: set ft=form:
* vim: set foldmethod=marker:
#-
#define treepath "../amplib"
#include- ../lib/GU.hdr
#include- ../lib/nGUtools.h
#include- ../lib/nGU2tools.h
#include- ../lib/nGU3tools.h
#include- ../lib/GUanalytic.h
#include- ../lib/factorise.h
CF SUSY,HEL,FFSvertex,FLTEST;
S Nf,Ns;

*{{{extraids
#procedure extraids
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
#endprocedure
*}}}

off statistics;
.global

#do hlbl={6,10,12}

L tmp = HEL('hlbl')*TMP;

#do i=3,1,-1
id HEL(x1?,?x) = HEL(x1-{2^'i'}*integer_(x1/2^{'i'}),integer_(x1/2^{'i'}),?x);
#enddo

repeat;
id HEL(x1?,?x)*TMP(?y) = HEL(?x)*TMP(?y,2*x1-1);
endrepeat;
id HEL = 1;
id TMP(?x) = HEL(?x);
id HEL(a1?$H1,a2?$H2,a3?$H3,a4?$H4) = HEL(a1,a2,a3,a4);

print+s;
.store

L tree=AMP(ptl(p1,'$H1',0),ptl(p2,'$H2',0),ptl(p3,'$H3',0),ptl(p4,'$H4',0));
#call subtreesGU

.sort
format maple;
#write <trees.log> "A0tree['hlbl']:=\n%e\n" tree

format 150;
B IA,IB;
print+s;
.store

#write "*** N=0 4-point three-loop ampltudes ***"
#write "*** helicity = '$H1','$H2','$H3','$H4' ***"

#do fl1={0,1/2,1}
#do fl2={0,1/2,1,-1/2,-1}
#do fl3={0,1/2,1,-1/2,-1}

*{{{ planar triple box integrand
L C10trboxINT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2
#do h8=-1,1,2
#do h9=-1,1,2
#do h10=-1,1,2

+TMPFL('fl1','fl2','fl3')*BoxBoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom,
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl3'),ptl(l6,'h6','fl3'),ptl(l7,'h7','fl3'),ptl(l8,'h8','fl2'),
ptl(l9,'h9','fl2'-'fl1'),ptl(l10,'h10','fl2'-'fl3')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

;

*}}}

*id TMPFL(0,0,0) = 1;

*id TMPFL(1/2,0,0) = Nf;
*id TMPFL(0,1/2,0) = Nf;
*id TMPFL(0,0,1/2) = Nf;
*id TMPFL(1/2,1/2,0) = Nf;
*id TMPFL(0,1/2,1/2) = Nf;
*id TMPFL(1/2,0,1/2) = Nf^2;
*id TMPFL(1/2,1/2,1/2) = Nf;

*id TMPFL(1,0,0) = Ns;
*id TMPFL(0,1,0) = Ns;
*id TMPFL(0,0,1) = Ns;
*id TMPFL(1,1,0) = Ns;
*id TMPFL(0,1,1) = Ns;
*id TMPFL(1,0,1) = Ns^2;
*id TMPFL(1,1,1) = Ns;

*id TMPFL(0,1/2,1) = Nf*Ns;
*id TMPFL(0,1,1/2) = Nf*Ns;
*id TMPFL(1,0,1/2) = Nf*Ns;
*id TMPFL(1/2,0,1) = Nf*Ns;
*id TMPFL(1,1/2,0) = Nf*Ns;
*id TMPFL(1/2,1,0) = Nf*Ns;

*id TMPFL(1/2,1,1) = Nf*Ns;
*id TMPFL(1,1/2,1) = Nf*Ns;
*id TMPFL(1,1,1/2) = Nf*Ns;

*id TMPFL(1/2,1/2,1) = Nf*Ns;
*id TMPFL(1/2,1,1/2) = Nf*Ns;
*id TMPFL(1,1/2,1/2) = Nf*Ns;

*id TMPFL(1/2,1/2,-1/2) = Nf*Ns;
*id TMPFL(1/2,-1/2,1/2) = Nf*Ns;
*id TMPFL(1/2,-1/2,-1/2) = Nf*Ns;

*id TMPFL(0,1,-1/2) = Nf*Ns;
*id TMPFL(1/2,-1/2,0) = Nf*Ns;

*id TMPFL(1/2,1,-1/2) = 2*Nf*Ns;
*id TMPFL(1,-1/2,1) = Nf*Ns^2;
id TMPFL(?x) = 1;
.sort

#call topologise3
#call subtreesGU

#call sortminusl
#call sortminusl
#call sortminusl
.sort

format 180;
B TMPFL,AMP;
print[];
.sort

id AMP(?x) = 0;

.sort
format maple;
#write <C10.trbox.INT.log> "C10_trbox_INT['hlbl','fl1','fl2','fl3']:=\n%e\n" C10trboxINT


.store
#enddo
#enddo
#enddo

#enddo
.end

