PolyOrder = DegreeLexicographic;
Findx[xx0_] := Module[{xx = xx0, i, j, s, t},
  For[i = 1, i <= L, i++,
   For[j = 1, j <= Dim, j++,
     If[xlist[[i, j]] == xx, s = i; t = j;];
     ];
   ];
  Return[{s, t}]
  ]
PreTreatment[] := 
 Module[{AdditionalKinematics1, AdditionalKinematics2, 
   KinematicsCondition, LoopMomentaList, ExternalMomentaList, 
   RemindingMomentaList, MomentaProjection},
  n = Length[ExternalMomentaBasis] + 1;
  LoopMomentaList = Table[ToExpression["l" <> ToString[i]], {i, 1, L}];
  ExternalMomentaList = 
   Table[ToExpression["p" <> ToString[i]], {i, 1, n}];
  RemindingMomentaList = 
   Complement[ExternalMomentaList, ExternalMomentaBasis];
  
  NSpurious = Dim - n + 1;
  If[NSpurious < 0, NSpurious = 0];
  NSpurious;
  SpuriousMomentaList = 
   Table[ToExpression["\[Omega]" <> ToString[i]], {i, 1, NSpurious}];
  SpacetimeBasis = Join[ExternalMomentaBasis, SpuriousMomentaList];
  TotalMomentaList = 
   Join[LoopMomentaList, SpacetimeBasis, RemindingMomentaList];
  
  AdditionalKinematics1 = 
   Table[ExternalMomentaList[[i]]*SpuriousMomentaList[[j]] -> 0, {i, 
      1, Length[ExternalMomentaList]}, {j, 1, 
      Length[SpuriousMomentaList]}] // Flatten;
  AdditionalKinematics2 = 
   Table[SpuriousMomentaList[[i]]*SpuriousMomentaList[[j]] -> 0, {i, 
      1, Length[SpuriousMomentaList]}, {j, i + 1, 
      Length[SpuriousMomentaList]}] // Flatten;
  
  KinematicsCondition = 
   Join[Kinematics, AdditionalKinematics1, AdditionalKinematics2];
  Gram = Table[
    SpacetimeBasis[[i]]*SpacetimeBasis[[j]], {i, 1, 
     Length[SpacetimeBasis]}, {j, 1, Length[SpacetimeBasis]}];
  ExtendGram = 
   Table[RemindingMomentaList[[i]]*SpacetimeBasis[[j]], {i, 1, 
     Length[RemindingMomentaList]}, {j, 1, Length[SpacetimeBasis]}];
  Gram = Gram //. KinematicsCondition;
  ExtendGram = ExtendGram //. KinematicsCondition;
  If[Length[RemindingMomentaList] == 1, 
   ExtendGram = {-Sum[Gram[[j]], {j, 1, Dim - NSpurious}] // 
      Simplify}];
  xlist = 
   Table[ToExpression["x" <> ToString[i] <> ToString[j]], {i, 1, 
     L}, {j, 1, Dim}];
  MomentaProjection = Join[xlist, Gram, ExtendGram];
  ScalarProductRules = 
   Flatten[Table[
     TotalMomentaList[[i]]*TotalMomentaList[[j]] -> 
       MomentaProjection[[i]].Inverse[Gram].MomentaProjection[[j]] // 
      Simplify, {i, 1, Length[TotalMomentaList]}, {j, i, 
      Length[TotalMomentaList]}]];
  CutEqn = 
   Table[Expand[Props[[i]]^2] //. ScalarProductRules /. numeric // 
     Simplify, {i, 1, Length[Props]}];
  CutEqn = Simplify[CutEqn //. numeric];
  ]
Shuffling[] := Module[{xxlist},
  xxlist = xlist // Transpose // Reverse;
  SPList = Flatten[xxlist];
  ]
ExpRead[monomial_, VarList_] := Exponent[monomial, VarList];
ExpRecover[list_, VarList_] := Inner[Power, VarList, list, Times];
ISPSearch[] := Module[{PreGr, PreLTList, expr, ISPlist, RSP},
  PreGr = GroebnerBasis[CutEqn, SPList, MonomialOrder -> PolyOrder];
  PreLTList = 
   Table[MonomialList[PreGr[[i]], SPList, PolyOrder][[1]], {i, 1, 
     Length[PreGr]}];
  RSP = {};
  For[i = 1, i <= Length[PreLTList], i++,
   expr = PreLTList[[i]];
   j = Total[Exponent[expr, SPList]];
   If[j == 1, 
    RSP = Append[RSP, ExpRecover[Exponent[expr, SPList], SPList]]];
   ];
  RSPSolution = 
   Table[RSP[[i]] -> 
      PolynomialReduce[RSP[[i]], PreGr, SPList, 
        MonomialOrder -> PolyOrder][[2]] // Simplify, {i, 1, 
     Length[RSP]}];
  
  CutEqnISP = Union[CutEqn //. RSPSolution // Simplify];
  ISP = MonomialList[Total[Complement[SPList, RSP]], SPList, 
    PolyOrder];
  LoopMomentaISP = 
   Table[Characters[ToString[ISP[[i]]]][[2]] // ToExpression, {i, 1, 
     Length[ISP]}];
  ISPMatrix = Table[Null, {i, 1, Length[ISP]}];
  For[i = 1, i <= Length[ISP], i++,
   ISPlist = Table[0, {i, 1, L}];
   ISPlist[[LoopMomentaISP[[i]]]] = 1;
   ISPMatrix[[i]] = ISPlist;
   ];
  ISPMatrix = ISPMatrix // Transpose;
  SpuriousISP = Table[1, {i, 1, Length[ISP]}];
  For[i = 1, i <= Length[ISP], i++,
   If[Findx[ISP[[i]]][[2]] >= Dim - NSpurious + 1, 
     SpuriousISP[[i]] = 
      SpuriousMomentaList[[-Dim + NSpurious + 
         Findx[ISP[[i]]][[2]]]]];
   ];
  ]
TestFunction[x_] := Piecewise[{{1, x >= 0}, {I, x < 0}}];
Compare[list10_, list20_] := 
 Module[{list1 = list10, list2 = list20, list, len, flag},
  If[list1 == list2, Return[0]];
  list = list1 - list2;
  flag = Sum[TestFunction[list[[i]]], {i, 1, Length[list]}];
  If[Im[flag] > 0, Return[-1], Return[1]];
  ]
RenorTest[list0_] := 
 Module[{list = list0, list1, list2, flag, exp, i, n},
  exp = Inner[Power, ISP, list, Times]; 
  list1 = Exponent[exp, ISP];
  list2 = ISPMatrix.list1;
  For[i = 1, i <= Length[RenormalizationLoopMomenta], i++,
   n = list2.RenormalizationLoopMomenta[[i]];
   If[n > RenormalizationPower[[i]], Return[0]];
   ];
  Return[1];
  ]
SpuriousTest[list0_] := 
 Module[{list = list0, list2, SpuriousCounting, i, exp, sign = 0},
  exp = Inner[Power, SpuriousISP, list, Times];
  list2 = Exponent[exp, SpuriousMomentaList];
  For[i = 1, i <= NSpurious, i++,
   If[Mod[list2[[i]], 2] == 1, sign = 1; Break[];];
   ];
  Return[sign];
  ]
(*Blist=Table[ExpRead[LTList[[i]],ISP],{i,1,Length[LTList]}];
Length[Blist] *)

GenerateBasis[] := 
 Module[{ttt, Gr, LTList, PreTargetList, TargetList, PreList, 
   TempMonoList, TempExpList, SpuriousCount, NonRenorCount, r, 
   ThisTerm, j},
  ttt = TimeUsed[];
  PreTreatment[];
  Shuffling[];
  ISPSearch[];
  Gr = GroebnerBasis[CutEqnISP, ISP, MonomialOrder -> PolyOrder];
  LTList = 
   Table[MonomialList[Gr[[i]], ISP, PolyOrder][[1]], {i, 1, 
     Length[Gr]}];
  PreTargetList = Table[{0, 1, 2, 3, 4}, {i, 1, Length[ISP]}];
  TargetList = Tuples[PreTargetList];
  PreList = {};
  For[i = 1, i <= Length[TargetList], i++,
   If[RenorTest[TargetList[[i]]] == 1, 
     PreList = Append[PreList, TargetList[[i]]]];
   ];
  Print["Possible renormalizable terms: ", Length[PreList]];
  
  Basis = {};
  SpuriousBasis = {};
  NSpuriousBasis = {};
  TempMonoList = {};
  TempExpList = {};
  SpuriousCount = 0;
  NonRenorCount = 0;
  For[i = 1, i <= Length[PreList], i++,
   r = PolynomialReduce[ExpRecover[PreList[[i]], ISP], Gr, ISP, 
      MonomialOrder -> PolyOrder][[2]];
   TempMonoList = MonomialList[r, ISP, PolyOrder];
   TempExpList = 
    Table[ExpRead[TempMonoList[[j]], ISP], {j, 1, 
      Length[TempMonoList]}];
   Basis = Union[Basis, TempExpList];
   ];
  
  Integrand = 0;
  For[j = 1, j <= Length[Basis], j++,
   ThisTerm = cc[];
   For[i = 1, i <= Length[ISP], i++,
    ThisTerm = ThisTerm*c[ i, Basis[[j, i]]]*ISP[[i]]^Basis[[j, i]];
    ];
   For[i = 1, i <= Length[ISP], i++,
    ThisTerm = ThisTerm //. cc[x1___]*c[i, x2_] -> cc[x1, x2]
    ];
   Integrand = Integrand + ThisTerm;
   ];
   
  For[i = 1, i <= Length[Basis], i++, 
   If[SpuriousTest[Basis[[i]]] == 1, SpuriousCount++;
     SpuriousBasis = Append[SpuriousBasis, Basis[[i]]];
     ];];
  NSpuriousBasis = Complement[Basis, SpuriousBasis];
  For[i = 1, i <= Length[Basis], i++, 
   If[RenorTest[Basis[[i]]] == 0, NonRenorCount++];];
  Print["Irreducible Scalar Products:", ISP];
  Print["Cut equations for ISP are listed in the variable \
'CutEqnISP'"];
  
  Print[];
  Print["The basis contains ", Length[Basis], 
   " terms, which are listed in the variable 'Basis'"];
  Print["Number of spurious terms: ", SpuriousCount, 
   ", listed in the variable 'SpuriousBasis'"];
  Print["Number of non-spurious terms: ", 
   Length[Basis] - SpuriousCount, 
   ", listed in the variable 'NSpuriousBasis'"];
  Print["Number of non-renormalizable terms: ", NonRenorCount];
  Print["Time used: ", TimeUsed[] - ttt, " seconds"];
  ]