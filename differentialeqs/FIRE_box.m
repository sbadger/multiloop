(* vim: set ft=mma: *)

(*UsingFermat = False;*)
(*QLinkPath = "/Users/simon/local/FIRE/QLink64";*)
(*FLinkPath = "/Users/simon/local/FLink/FLink";*)
(*FermatPath = "/Users/simon/local/ferl64/feris64";*)
(*DatabaseUsage = 0;*)

Get["/Users/simon/local/FIRE/SBases_3.1.0.m"];
Get["/Users/simon/local/FIRE/FIRE_3.4.0.m"];
Get["/Users/simon/local/FIRE/IBP.m"]

rename = {G[{a1_,a2_,a3_,a4_}] -> I4[a1,a2,a3,a4]};

Internal = {k};
External = {p1,p2,p3};
Propagators = {k^2, (k-p2)^2,(k-p2-p3)^2,(k-p1-p2-p3)^2};
PrepareIBP[];
reps = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p1 p2 -> (s123-s13-s23)/2, p1 p3 -> s13/2, p2 p3 -> s23/2};
startinglist = {IBP[k, p1],IBP[k,p2],IBP[k,p3],IBP[k,k]} /. reps;
Prepare[AutoDetectRestrictions -> True];

Burn[];

II[1,1,0,2]=F[{1,1,0,2}];
Print["id I4(1,1,0,2) = ",II[1,1,0,2] /. rename // CForm,";"]

II[2,0,1,1]=F[{2,0,1,1}];
Print["id I4(2,0,1,1) = ",II[2,0,1,1] /. rename // CForm,";"]
