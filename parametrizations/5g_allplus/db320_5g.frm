#-
#include- ../2to3tools.prc

CF PropGlu,PropGht,VertGlu,VertGlu4,VertGht;
CF INTdboxD;

#procedure to4D

id,once dot(l1,px?) = dot(k1,px);
id,once dot(l2,px?) = dot(k1,px)-dot(p1,px);
id,once dot(l3,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px);
id,once dot(l4,px?) = dot(k1,px)-dot(p1,px)-dot(p2,px)-dot(p3,px);
id,once dot(l5,px?) = -dot(k2,px)+dot(p4,px)+dot(p5,px);
id,once dot(l6,px?) = -dot(k2,px)+dot(p5,px);
id,once dot(l7,px?) = -dot(k2,px);
id,once dot(l8,px?) = -dot(k1,px)-dot(k2,px);
id,once AB(px?,l1,py?) = AB(px,k1,py);
id,once AB(px?,l2,py?) = AB(px,k1,py)-AB(px,p1,py);
id,once AB(px?,l3,py?) = AB(px,k1,py)-AB(px,p1,py)-AB(px,p2,py);
id,once AB(px?,l4,py?) = AB(px,k1,py)-AB(px,p1,py)-AB(px,p2,py)-AB(px,p3,py);
id,once AB(px?,l5,py?) = -AB(px,k2,py)+AB(px,p4,py)+AB(px,p5,py);
id,once AB(px?,l6,py?) = -AB(px,k2,py)+AB(px,p5,py);
id,once AB(px?,l7,py?) = -AB(px,k2,py);
id,once AB(px?,l8,py?) = -AB(px,k1,py)-AB(px,k2,py);

#call cancel

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;

#endprocedure

.global

G AMP =

  +EPS(-1,p1,p2,mu1)*EPS(-1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5)*(

  + xxdiag1*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGlu(-l4,-l8,l5,nu4,nu8,rho5)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    VertGlu(-l7,l8,l1,nu7,rho8,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)*
    PropGlu(l8,nu8,rho8,mu33)
  )

  + xxdiag2*(
    VertGht(l2,mu1)*
    VertGht(l3,mu2)*
    VertGht(l4,mu3)*
    VertGht(-l8,rho5)*
    VertGlu(-l5,p4,l6,nu5,mu4,rho6)*
    VertGlu(-l6,p5,l7,nu6,mu5,rho7)*
    VertGht(l1,nu7)*
    PropGht(l1,mu11)*
    PropGht(l2,mu11)*
    PropGht(l3,mu11)*
    PropGht(l4,mu11)*
    PropGlu(l5,nu5,rho5,mu22)*
    PropGlu(l6,nu6,rho6,mu22)*
    PropGlu(l7,nu7,rho7,mu22)*
    PropGht(l8,mu33)
  )

  + xxdiag3*(
    VertGlu(-l1,p1,l2,nu1,mu1,rho2)*
    VertGlu(-l2,p2,l3,nu2,mu2,rho3)*
    VertGlu(-l3,p3,l4,nu3,mu3,rho4)*
    VertGht(l5,nu4)*
    VertGht(l6,mu4)*
    VertGht(l7,mu5)*
    VertGht(l8,rho1)*
    PropGlu(l1,nu1,rho1,mu11)*
    PropGlu(l2,nu2,rho2,mu11)*
    PropGlu(l3,nu3,rho3,mu11)*
    PropGlu(l4,nu4,rho4,mu11)*
    PropGht(l5,mu22)*
    PropGht(l6,mu22)*
    PropGht(l7,mu22)*
    PropGht(l8,mu33)
  )

   + xxdiag4*(
    VertGht(l2,mu1)*
    VertGht(l3,mu2)*
    VertGht(l4,mu3)*
    VertGht(l5,rho8)*
    VertGht(l6,mu4)*
    VertGht(l7,mu5)*
    VertGht(l1,nu8)*
    PropGht(l1,mu11)*
    PropGht(l2,mu11)*
    PropGht(l3,mu11)*
    PropGht(l4,mu11)*
    PropGht(l5,mu22)*
    PropGht(l6,mu22)*
    PropGht(l7,mu22)*
    PropGlu(l8,nu8,rho8,mu33)
  )

);

repeat;
id,once VertGlu(p1?, p2?, p3?, mu1?, mu2?, mu3?) = I/rt2*(
    GGG(mu2,mu3)*(MOM(p2,mu1)-MOM(p3,mu1))
  + GGG(mu3,mu1)*(MOM(p3,mu2)-MOM(p1,mu2))
  + GGG(mu1,mu2)*(MOM(p1,mu3)-MOM(p2,mu3))
);

id,once VertGlu4(p1?,p2?,p3?,p4?,mu1?,mu2?,mu3?,mu4?) = I/2*(
  - GGG(mu1,mu2)*GGG(mu3,mu4)
  - GGG(mu2,mu3)*GGG(mu1,mu4)
  + 2*GGG(mu1,mu3)*GGG(mu2,mu4)
);

id,once VertGht(p1?, mu1?) = I/rt2*(
    MOM(p1,mu1)
);

id MOM(p1+p2,mu1?) = MOM(p1,mu1)+MOM(p2,mu1);
id MOM(-p1-p2,mu1?) = -MOM(p1,mu1)-MOM(p2,mu1);

#call Lcontract
id,once PropGlu(p?, mu1?, mu2?, m?) = I*( -GGG(mu1,mu2) );
id,once PropGht(p?, m?) = 1;
#call Lcontract

endrepeat;

B VertGlu,VertGht;
print[];
.sort

*id Ds = [Ds-2]+2;
*id [Ds-2] = Ds-2;
*if(count(Ds,2)<2);
*discard;
*endif;

repeat;
#call to4D
endrepeat;
#call cancel

.sort
topolynomial;

.sort
format maple;
#write <db320D_mmppp.input.mpl> "INT_db320D_mmppp := proc(k1,k2,mu11,mu12,mu22) local Z:"
#write <db320D_mmppp.input.mpl> "%X"
#write <db320D_mmppp.input.mpl> "return %e" AMP
#write <db320D_mmppp.input.mpl> "end:"
.sort
frompolynomial;

repeat;
#call subsLmom(../final,db320D)
#call cancel
endrepeat;
.sort
#write <tmp_mmppp.prc> "G AMP = %e" AMP

B tau1,...,tau3;
print+s;
.end

multiply ds(0,0,0);
repeat;
id,once tau1*ds(x1?,x2?,x3?) = ds(x1+1,x2,x3);
id,once tau2*ds(x1?,x2?,x3?) = ds(x1,x2+1,x3);
id,once tau3*ds(x1?,x2?,x3?) = ds(x1,x2,x3+1);
endrepeat;

#include- ../final/db320D.coeffvecs.prc

B INV;
print[];
.store

#do k=1,79

G dvec'k' = AMP;
id ds('k') = 1;
id ds(x?) = 0;
id NUM(x?) = x;

print+s;
.store

#enddo

#include- ../final/db320D.integrand.prc
#include- ../final/db320D.coeffvecs.prc

#do k=1,79
id ds('k') = dvec'k';
#enddo

#write <db320D.prc> "id INTdb320D(k1,k2,mu11,mu12,mu22) = %e" INTdb320D

B dot,NUM,mu11,mu22,mu12;
format 150;
print+s;
.end
