* vim: set foldmethod=marker:
#-
S x,y,z,w3;
S tau;
S [l1.p3],[l4.p2],[l1.w],[l4.w];
auto S c,X,Y,p,a1,...,a4,b1,...,b4,x,y,P;
S k1f,k2f,S1,S2,S12,gg,sqrtgg;
S t,t1,...,t8,l1,...,l7;
CF INV,NUM,S(s),IS(s);

CF A(a),B(a),AB;
CF IA(a),IB(a),IAB,dot;
CF M,sqrt,F,IF,eta;
S mu,MU1;
S I,DD,CC;

auto S xx;

set mus:mu;
set massless:p0;

*{{{ lprod
#procedure lprod

id AB(p1?,mu?mus,p2?)*AB(p3?,mu?mus,p4?) = -2*A(p1,p3)*B(p2,p4);
id M(px?,mu?mus)*M(py?,mu?mus) = dot(px,py);
id M(px?,mu?mus)*AB(p1?,mu?mus,p2?) = AB(p1,px,p2);

#endprocedure
*}}}
*{{{ cancel
#procedure cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;
id S(p1?)*IS(p1?) = 1;
id AB(?x)*IAB(?x) = 1;
id F(?x)*IF(?x) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

id S(k1f,k2f) = gg;
id IS(k1f,k2f) = 1/gg;
id I^2=-1;

id NUM(px?)*INV(px?) = 1;
splitarg NUM;
id NUM(px?) = px;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
id INV(S(px?)) = IS(px);
id INV(gg) = 1/gg;
id INV(-1) = -1;
id INV(4) = 1/4;
id INV(AB(?x)) = IAB(?x);
id INV(INV(?x)) = NUM(?x);

#endprocedure
*}}}
*{{{ factorise(func)
#procedure factorise(func)
argument 'func';
topolynomial;
endargument;
.sort
factarg 'func';
.sort
argument 'func';
frompolynomial;
endargument;
chainout 'func';
#endprocedure
*}}}
*{{{ expandkf
#procedure expandkf
#call cancel
*id AB(k1f,p3,k2f)*AB(k2f,p3,k1f) = AB(k1f,p3,k1f)*AB(k2f,p3,k2f)-gg*S(p3);
id AB(px?,p1,py?) = AB(px,k1f,py)+S(p1)/gg*AB(px,k2f,py);
id AB(px?,p2,py?) = AB(px,k2f,py)+S(p2)/gg*AB(px,k1f,py);
id AB(px?,k1f,py?) = A(px,k1f)*B(k1f,py);
id AB(px?,k2f,py?) = A(px,k2f)*B(k2f,py);
id A(k1f,k2f)*B(k2f,k1f) = gg;
id AB(k1f,px?!{mu},k1f) = 2*dot(k1f,px);
id AB(k2f,px?!{mu},k2f) = 2*dot(k2f,px);
#endprocedure
*}}}

off stats;

.global

#do s=1,2
G k's' = ( a1's'*k1f+a2's'*k2f+a3's'*AB(k1f,mu,k2f) + a4's'*AB(k2f,mu,k1f) )/2;

id k1f = AB(k1f,mu,k1f);
id k2f = AB(k2f,mu,k2f);

.sort
G l1's' = k's';
G l2's' = k's'-M(p2,mu);
G l3's' = k's'-M(p2,mu)-M(p3,mu);
G l4's' = k's'+M(p1,mu);
#enddo
G w = ( AB(k1f,mu,k2f) - AB(k1f,p3,k2f)*IAB(k2f,p3,k1f)*AB(k2f,mu,k1f) )/2*AB(k2f,p3,k1f)/gg;
G xCC = ( a11*AB(k1f,p3,k1f)+a21*AB(k2f,p3,k2f)-S(p3)-2*dot(p2,p3) )*NUM(gg^2-S(p1)*S(p2));
.sort
G xDD = xCC^2-4*AB(k1f,p3,k2f)*AB(k2f,p3,k1f)*(a11*a21 - MU1^2/gg )*NUM(gg^2-S(p1)*S(p2))^2;
G xDET = -4*AB(k1f,p3,k2f)*AB(k2f,p3,k1f)*(a11*a21 -MU1^2/gg )*NUM(gg^2-S(p1)*S(p2))^2;
 
id a41=a11*a21/a31 - MU1^2/gg/a31;
id a31= -F(CC,sqrt(DD))/2*IAB(k1f,p3,k2f)*INV(gg^2-S(p1)*S(p2));
id 1/a31= -IF(CC,sqrt(DD))*2*AB(k1f,p3,k2f)*NUM(gg^2-S(p1)*S(p2));

id a32=a12*a22/a42 - MU1^2/gg/a42;
id a42= -F(CC,sqrt(DD))/2*IAB(k2f,p3,k1f)*INV(gg^2-S(p1)*S(p2));
id 1/a42= -IF(CC,sqrt(DD))*2*AB(k2f,p3,k1f)*NUM(gg^2-S(p1)*S(p2));

id a12 = a11;
id a22 = a21;
id a11 = S(p2)*(gg+S(p1))*INV(gg^2-S(p1)*S(p2));
id a21 = -S(p1)*(gg+S(p2))*INV(gg^2-S(p1)*S(p2));

#call cancel
id NUM(px?) = px;
id IF(CC,sqrt(DD)) = -F(CC,-sqrt(DD))*INV(xDET);
argument INV;
id a11 = S(p2)*(gg+S(p1))*INV(gg^2-S(p1)*S(p2));
id a21 = -S(p1)*(gg+S(p2))*INV(gg^2-S(p1)*S(p2));
#call cancel
endargument;

*#call cancel
*#call factorise(INV);
*#call expandkf
*B INV,AB;
*.sort
*collect NUM;
*#call factorise(NUM)
*#call cancel

*argument NUM,INV;
*#call expandkf
*endargument;
*#call factorise(INV)
*#call cancel
*id NUM(px?) = px;
*B INV,AB;
*.sort
*collect NUM;
*#call factorise(NUM)
*#call cancel

*id S(p0?massless) = 0;
*argument NUM,INV;
*id S(p0?massless) = 0;
*endargument;

*multiply replace_(p1,P1);
*multiply replace_(p2,P2);
*multiply replace_(p3,P3);
*id AB(px?,mu,py?) = eta(px,py);

*.sort
*format maple;
*#write <Dtest.mpl> "CC:=%e" xCC
*#write <Dtest.mpl> "DD:=%e" xDD
*#write <Dtest.mpl> "xDET:=%e" xDET
*#write <Dtest.mpl> "l11:=%e" l11
*#write <Dtest.mpl> "l12:=%e" l12

format 150;
B INV,AB;
print+s xCC,xDD,xDET,l11,l12;
*.end
.store

*{{{ check on-shell conditions
#do s=1,2
#do k=1,4
L zero'k''s' = l'k''s'*l'k''s'-MU1^2;
#enddo
#enddo
L test = 2*l11*w;

#call lprod
#call cancel

print+s;
.end

.store
*}}}

G Delta = c0 + c1*MU1^2 + c2*MU1^4 + (c3+c4*MU1^2)*dot(l1,w3);

B x;
print+s;
.store

#do s=1,2
G Delta's' = Delta;

repeat;
id,once dot(l1,w3) = l1's'*w;
#call lprod
endrepeat;

print+s;
.store
#enddo

#do s=1,2
#do x=0,2,4 

L d's''x' = Delta's';

if(count(MU1,1)!='x');
discard;
endif;
id MU1=1;

#enddo
#enddo

print+s;
.sort
hide;

#define csset "{c0,c1,c2,c3,c4}"
.sort

#write <matrix_Dbox.mpl> "cs:=Vector(5):"
#define i "1"
#do c='csset'
#write <matrix_Dbox.mpl> "cs['i']:='c':"
#define i "{'i'+1}"
#enddo

#write <matrix_Dbox.mpl> "dvec:=Vector(6):"
#write <matrix_Dbox.mpl> "eqs:=Matrix(6,5):"
#define i "1"
#do s=1,2
#do x=0,2,4

#write <matrix_Dbox.mpl> "dvec['i']:=ds['s']:"

#define j "1"
#do c='csset'

L eqs'i'x'j' = d's''x';

if(count('c',1)<1);
discard;
endif;
id 'c'=1;

.sort
#write <matrix_Dbox.mpl> "eqs['i','j']:=%E:" eqs'i'x'j'
#define j "{'j'+1}"
.sort
hide;
#enddo
#enddo

#define i "{'i'+1}"
#enddo

.sort
unhide;

L cfss = NUM(Delta);

argument NUM;
id dot(l1,w3) = 1;
endargument;

format 150;
print cfss;
.sort

repeat id NUM(x?,?xx) = 1+NUM(?xx);
print cfss;

.end
