#-
#include- ../2to3tools.prc
CF CMOM;

#procedure FromMomTwistors

x1 = s12;
x2 = (s12*(s23 - s15) + s23*s34 + s15*s45 - s34*s45 - TR5)/2/s34;
x3 = (s23(s15*s45-2*s34*s45+s12*(s15+s45)-tr5)+s45*(s12*s15-s15*s45+s34*s45+TR5)+(s34-s12)*s23)*INV(2*(s12 +s23 -s45)*s45);
x4 = (s12*(s23 - s15) + s23*s34 + s15*s45 - s34*s45 + TR5)*INV(2*s12*(s15 - s23 + s45));
x5 = ((s23-s45)*(s12*(s23-s15)+s23*s34+s15*s45-s34*s45+TR5))*INV(2*s12*s23*(-s15 + s23 - s45));
argument INV;
x1 = s12;
x2 = (s12*(s23 - s15) + s23*s34 + s15*s45 - s34*s45 - TR5)/2/s34;
x3 = (s23(s15*s45-2*s34*s45+s12*(s15+s45)-tr5)+s45*(s12*s15-s15*s45+s34*s45+TR5)+(s34-s12)*s23)*INV(2*(s12 +s23 -s45)*s45);
x4 = (s12*(s23 - s15) + s23*s34 + s15*s45 - s34*s45 + TR5)*INV(2*s12*(s15 - s23 + s45));
x5 = ((s23-s45)*(s12*(s23-s15)+s23*s34+s15*s45-s34*s45+TR5))*INV(2*s12*s23*(-s15 + s23 - s45));
endargument;

#endprocedure

#procedure ExpandSpinorStrings
repeat;
id,once AB(p1?,p2?ps,?args) = A(p1,p2)*BB(p2,?args);
al,once AA(p1?,p2?ps,?args) = A(p1,p2)*BA(p2,?args);
al,once BB(p1?,p2?ps,?args) = B(p1,p2)*AB(p2,?args);
al,once BA(p1?,p2?ps,?args) = B(p1,p2)*AA(p2,?args);
al,once IAB(p1?,p2?ps,?args) = IA(p1,p2)*IBB(p2,?args);
al,once IAA(p1?,p2?ps,?args) = IA(p1,p2)*IBA(p2,?args);
al,once IBB(p1?,p2?ps,?args) = IB(p1,p2)*IAB(p2,?args);
al,once IBA(p1?,p2?ps,?args) = IB(p1,p2)*IAA(p2,?args);

id BB(p1?,p2?) = B(p1,p2);
id IBB(p1?,p2?) = IB(p1,p2);
id AA(p1?,p2?) = A(p1,p2);
id IAA(p1?,p2?) = IA(p1,p2);

endrepeat;
id BA(?args) = AB(reverse_(?args));
id IBA(?args) = IAB(reverse_(?args));
#call cancel
#endprocedure

off stats;

.global

#include- db221.input.frm
.sort
L zero = INTdb221
  - IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    ((Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-mu11*mu22))*(
      xx0 + xx1*dot(k1,p4) + xx2*dot(k2,p1) + xx3*dot(k2,p5)
    )
);
.sort
drop INTdb221;

if(count(Ds,1)<1);
discard;
endif;
.sort

#do loop=1,10
id,once dot(k1,p?) = MOM(k1,nu1)*MOM(p,nu1);
al,once dot(k2,p?) = MOM(k2,nu2)*MOM(p,nu2);
al,once AB(p1?,k1,p2?) = MOM(k1,nu3)*CMOM(p1,nu3,p2);
al,once AB(p1?,k2,p2?) = MOM(k2,nu4)*CMOM(p1,nu4,p2);

B MOM,CMOM;
.sort
keep brackets;

#call subsLmom(1,db221D)

id CMOM(?x) = AB(?x);
.sort
id MOM(p1?,nu1?mus)*MOM(p2?,nu1?mus) = dot(p1,p2);
id MOM(p1?,nu1?mus)*AB(p2?,nu1?mus,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?mus,p2?)*AB(p3?,nu1?mus,p4?) = -2*A(p1,p3)*B(p2,p4);
id dot(p1,p3+p4) = dot(p1,p3)+dot(p1,p4);
id dot(p4,p1+p2) = dot(p4,p1)+dot(p4,p2);
id dot(p1?ps,p1?ps) = 0;
#call ExpandSpinorStrings
.sort

#enddo

id mu33 = mu11+mu22+mu12;
#call subsLmom(1,db221D)

if(count(tau1,1)<3);
discard;
endif;
.sort

id AB(px1?,p1+p2,px2?) = AB(px1,p1,px2) + AB(px1,p2,px2);
id AB(px1?,p3+p4,px2?) = AB(px1,p3,px2) + AB(px1,p4,px2);

#call ExpandSpinorStrings
multiply A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1);

id S(p1,p2) = A(p1,p2)*B(p2,p1);
id IS(p1,p2) = IA(p1,p2)*IB(p2,p1);
#call cancel
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id IS(p'i',p'j') = 1/s'i''j';
id S(p'j',p'i') = s'i''j';
id IS(p'j',p'i') = 1/s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

id NUM(x?) = x;
*B tau1,...,tau3,xx0,...,xx4,Ds,I;
AB  s12,s13,s14,s15,
    s23,s24,s25,
    s34,s35,
    s45;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);
argument NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);
id NUM(x?) = x;
.sort

#do i=1,5
#do j=1,5
#if 'j'>'i'
id A(p'i',p'j') = a'i''j';
id IA(p'i',p'j') = 1/a'i''j';
id B(p'i',p'j') = b'i''j';
id IB(p'i',p'j') = 1/b'i''j';
id s'i''j' = -a'i''j'*b'i''j';
id 1/s'i''j' = -1/a'i''j'/b'i''j';
#endif
#enddo
#enddo

*** much better to change variables in Mathematica ***
*** there must be a more efficient way to deal with ***
*** polynomials in FORM ***
*#call changevars
.sort
format mathematica;
#write <db221_tmp.m> "zero = %E;" zero

B Ds,tau1,...,tau3,INV;
print+s zero;
.end
