interface(quiet=true):
interface(displayprecision=10):
Digits:=50:
with(LinearAlgebra):
with(spinors):

delta := proc(a,b,c)
local s12,s23,s34,s45,s15;
global p1,p2,p3,p4,p5;
s12 := S(p1,p2):
s23 := S(p2,p3):
s34 := S(p3,p4):
s45 := S(p4,p5):
s15 := S(p1,p5):

return s12*s15 + a*s12*s23 + b*s23*s34 + c*s34*s45 - s45*s15;

end:

deltabar := proc(a,b,c)
local s12,s23,s34,s45,s15;
global p1,p2,p3,p4,p5;
s12 := S(p1,p2):
s23 := S(p2,p3):
s34 := S(p3,p4):
s45 := S(p4,p5):
s15 := S(p1,p5):

return s12*s15 + a*s12*s23 + b*s23*s34 + c*s34*s45 + s45*s15;

end:

#for i from 1 to 10 do:
genmoms([0,0,0,0,0]);
TR5 := tr5(p1,p2,p3,p4):
printf("{s12 -> %.16f, s23 -> %.16f, s34 -> %.16f, s45 -> %.16f, s15 -> %.16f, tr5 -> %.16f*I};\n",S(p1,p2),S(p2,p3),S(p3,p4),S(p4,p5),S(p5,p1),Re(-I*TR5));
#clearmoms(5);
#od:

s12 := S(p1,p2):
s13 := S(p1,p3):
s14 := S(p1,p4):
s15 := S(p1,p5):
s23 := S(p2,p3):
s24 := S(p2,p4):
s25 := S(p2,p5):
s34 := S(p3,p4):
s35 := S(p3,p5):
s45 := S(p4,p5):


(delta(-1,-1,+1) - TR5) - 2*AB(p1,p2,p3,p4,p1);
(delta(-1,+1,-1) - TR5) - 2*AB(p1,p5,p4,p3,p1);
(deltabar(-1,+1,-1) - TR5) + 2*AB(p1,p5,p4,p2,p1);
(delta(+1,-1,+1) + TR5) + 2*AB(p1,p5,p3,p2,p1);

### results for 5-point coefficients ###
### overall factor if I*prod(<i,i+1>) removed ###

trp1345 := AB(p1,p5,p4,p3,p1);
trp1235 := AB(p1,p5,p3,p2,p1);
trp1245 := AB(p1,p5,p4,p2,p1);
trm1234 := AB(p1,p2,p3,p4,p1);

Cdb320   := s12*s23*s45*s45^2*s15/TR5 + s12*s23*s45*trp1345/TR5*xk1p5sq;
Cdb220M2 := 2*s15*s45^2*trm1234/TR5;
Cdb2205L := -s12*s23*s34*s45*s15/TR5;

Cbt32    := (-2*dpk1w132 + s12*s23/s13)*trp1345/2*(F2(Ds,mu11,mu12,mu22) + F3(Ds,mu11,mu22,k1,k2,s45));
Cbt22M2  := (s45-s23)/s13*trp1345/2*(F2(Ds,mu11,mu12,mu22) + F3(Ds,mu11,mu22,k1,k2,s45));
Cbt225L  :=
 + (-trp1245 + trp1235*trp1345/s13/s35)/2*(
        F2(Ds,mu11,mu12,mu22) + Dsm2^2*mu11*mu22*(4*xk1p3*xk2p3 + xk1k2sq*(s12+s45) + s12*s45)/s12/s45
  )
 - Dsm2^2*xk1k2sq*mu11*mu22*s15
 - Dsm2^2*mu11*mu22/2*(
    - trp1235*(2*xk1p3*(s35 + 2*xk2w45m - (s12-s45)/s45*2*xk2p5) - s12*xk1k2sq)/s35/s12
    - trp1345*(2*xk2p3*(s13 + 2*xk1w12m - (s45-s12)/s12*2*xk1p1) - s45*xk1k2sq)/s13/s45
  ):
collect(Cbt225L,{F2,xk1k2sq,xk1p3,xk2p3,xk1p1,xk2p5,xk1w12m,xk2w45m});

