interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

#with(spinors);
#assign(declaremomn([0,0,0,0])):

# solving planar double box

IS(p1,p2) := 1/S(p1,p2);

read("boxbox.reduceM.log"):

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[33..38]));
Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dVec:

dvecR:=SubVector(dvecT,[1..32]):
csol:=Ieqs.dvecR;

fd:=fopen("dbox.d2c.mpl",WRITE):
for k from 1 to 32 do:
tmp:=simplify(csol[k]);
fprintf(fd,"print(%A,%A):\n",cs[k],tmp):
od:

for k from 33 to 38 do:
fprintf(fd,"print(%A);\n",simplify(dvecT[k])):
od:

fclose(fd):

for s from 1 to 6 do:
  for p from -4 to 4 do:
     ds[s,p]:=cat('d',s,p+4):
  od:
od:

fd:=fopen("dbox.d2c.h",WRITE):
for k from 1 to 32 do:
tmp:=simplify(csol[k]);
fprintf(fd,"L %A = %A;\n",cs[k],tmp):
od:

for k from 33 to 38 do:
fprintf(fd,"L zero%d = %A;\n",k-32,simplify(dvecT[k])):
od:

fclose(fd):

