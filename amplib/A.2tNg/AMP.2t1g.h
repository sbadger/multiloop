id AMP(ptl(p1?,-1,-3/2,q1?,x1?,mt?),ptl(p2?,-1,3/2,q2?,x2?,mt?),ptl(p3?,-1,0,x3?)) =
	  - A(p3,q1)*B(q1,x1)*B(x2,x3)*IB(p3,x3)*IB(q1,x1)*IB(q2,x2)*mt*I - A(p3,q2)*B(q2,x2)*B(x1,x3)*IB(p3,x3)*IB(q1,x1)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,-1,-3/2,q1?,x1?,mt?),ptl(p2?,-1,3/2,q2?,x2?,mt?),ptl(p3?,1,0,x3?)) =
	 A(q1,x3)*B(p3,x2)*B(q1,x1)*IA(p3,x3)*IB(q1,x1)*IB(q2,x2)*mt*I + A(q2,x3)*B(p3,x1)*B(q2,x2)*IA(p3,x3)*IB(q1,x1)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,-1,-3/2,q1?,x1?,mt?),ptl(p2?,1,3/2,q2?,x2?,mt?),ptl(p3?,-1,0,x3?)) =
	  - A(p3,q1)*B(q2,x3)*IB(p3,x3)*I - A(p3,x2)*B(x1,x3)*IA(q2,x2)*IB(p3,x3)*IB(q1,x1)*mt^2*I;

id AMP(ptl(p1?,-1,-3/2,q1?,x1?,mt?),ptl(p2?,1,3/2,q2?,x2?,mt?),ptl(p3?,1,0,x3?)) =
	 A(q1,x3)*B(p3,q2)*IA(p3,x3)*I + A(x2,x3)*B(p3,x1)*IA(p3,x3)*IA(q2,x2)*IB(q1,x1)*mt^2*I;

id AMP(ptl(p1?,1,-3/2,q1?,x1?,mt?),ptl(p2?,-1,3/2,q2?,x2?,mt?),ptl(p3?,-1,0,x3?)) =
	  - A(p3,q2)*B(q1,x3)*IB(p3,x3)*I - A(p3,x1)*B(x2,x3)*IA(q1,x1)*IB(p3,x3)*IB(q2,x2)*mt^2*I;

id AMP(ptl(p1?,1,-3/2,q1?,x1?,mt?),ptl(p2?,-1,3/2,q2?,x2?,mt?),ptl(p3?,1,0,x3?)) =
	 A(q2,x3)*B(p3,q1)*IA(p3,x3)*I + A(x1,x3)*B(p3,x2)*IA(p3,x3)*IA(q1,x1)*IB(q2,x2)*mt^2*I;

id AMP(ptl(p1?,1,-3/2,q1?,x1?,mt?),ptl(p2?,1,3/2,q2?,x2?,mt?),ptl(p3?,-1,0,x3?)) =
	  - A(p3,x1)*A(q2,x2)*B(q2,x3)*IA(q1,x1)*IA(q2,x2)*IB(p3,x3)*mt*I - A(p3,x2)*A(q1,x1)*B(q1,x3)*IA(q1,x1)*IA(q2,x2)*IB(p3,x3)*mt*I;

id AMP(ptl(p1?,1,-3/2,q1?,x1?,mt?),ptl(p2?,1,3/2,q2?,x2?,mt?),ptl(p3?,1,0,x3?)) =
	 A(q1,x1)*A(x2,x3)*B(p3,q1)*IA(p3,x3)*IA(q1,x1)*IA(q2,x2)*mt*I + A(q2,x2)*A(x1,x3)*B(p3,q2)*IA(p3,x3)*IA(q1,x1)*IA(q2,x2)*mt*I;

