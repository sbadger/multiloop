interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.16e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_db320:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := B(p[1],p[3])/B(p[2],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := B(p[3],p[4])/B(p[3],p[5])*eta(p[4],p[5])/2;
bv[4] := B(p[3],p[5])/B(p[3],p[4])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0,-tau1*s23/s13, 1+tau1]:
b := [0, 1, tau2/s34, tau3/s35]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := s12*s23/s13*tau1*(1+tau1);
mu22 := -tau2*tau3*s45/s34/s35;
mu12 :=
         + TR5 * (
          - 1/2*s13^(-1)
          - 1/2*s13^(-1)*tau3*s35^(-1)
          - tau1*s13^(-1)
          - 1/2*(s34 + s45 - s12)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          )

          - 1/2*(s34 + s45 - s12)*(s23*s34 - s45*s15 - s45*s34 + s12*s15
          - s12*s23)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          + 1/2*( - s23*s34 + s45*s15 + s45*s34 - 2*s45*s23 + 2*s45^2 - s12*s15 + s12*s23 - 2*s12*s45)*s13^(-1)*tau3*s35^(-1)
          - 1/2*(s23*s34 - s45*s15 - s45*s34 + s12*s15 - s12*s23)*s13^(-1)
          - 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          + 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
         ;

return k1,k2,mu11,mu12,mu22;
end:

#read("dcoeffs.mpl"):

#fd:=fopen("out_analytic.log",WRITE):
#for k from 1 to 79 do:
# tmp1 := Re(subs(Ds=0,ddd||k)):
# tmp2 := Im(subs(Ds=0,ddd||k)):
# tmp3 := Re(coeff(ddd||k,Ds)):
# tmp4 := Im(coeff(ddd||k,Ds)):
# printf("d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# fprintf(fd,"d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# print("d",k,expand(Dvec[k]));
#od:
#fclose(fd);

xxdiag1:=1:
xxdiag2:=2*I:
xxdiag3:=-2:
xxdiag4:=2*I:

read("db320D_mmppp.input.mpl"):

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
ds(k1,k2,k3):=0:
od:
od:
od:

for th1 from 0 to 6 do:
for th2 from 0 to 4 do:
for th3 from 0 to 4 do:
printf("%d%d%d\n",th1,th2,th3);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/7);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/5);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/5);

k1,k2,mu11,mu12,mu22 := on_shell_db320([p1,p2,p3,p4,p5],t1,t2,t3):
val := INT_db320D_mmppp(k1,k2,mu11,mu12,mu22);

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
  ds(k1,k2,k3) := ds(k1,k2,k3) + evalf(val/t1^k1/t2^k2/t3^k3/(7.*5.*5.)):
od:
od:
od:

od:
od:
od:

read("../final/db320D.coeffvecs.mpl");

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
 tmp1 := Re(subs(Ds=0,ds(k1,k2,k3))):
 tmp2 := Im(subs(Ds=0,ds(k1,k2,k3))):
 tmp3 := Re(coeff(ds(k1,k2,k3),Ds)):
 tmp4 := Im(coeff(ds(k1,k2,k3),Ds)):
 printf("d_%d%d%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k1, k2, k3, tmp1, tmp2, tmp3, tmp4);
# print("d",k1,k2,k3,expand(ds(k1,k2,k3)));
od:
od:
od:

#fd:=fopen("out_numerical.log",WRITE):
for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,Dvec[k])):
 tmp2 := Im(subs(Ds=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Ds)):
 tmp4 := Im(coeff(Dvec[k],Ds)):
 printf("d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# fprintf(fd,"d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# print("d",k,expand(Dvec[k]));
od:
#fclose(fd);

read("../db320D.NumericalReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(79):
Cvec := iMM.Dvec:

for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,Cvec[k])):
 tmp2 := Im(subs(Ds=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Ds)):
 tmp4 := Im(coeff(Cvec[k],Ds)):
 printf("%d %A = %.50e + (%.50e)*I + Ds*(%.50e + (%.50e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
# print(Clbl[k],expand(Cvec[k]));
od:


