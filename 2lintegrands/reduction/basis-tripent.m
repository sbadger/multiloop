
Get["../../autobasisv1/basis-radium.m"];

(* tri-pent *)
L=2;
Dim=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2, p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t) /s};
numeric={s->11,t->3};
Props={l2-l1-p4,l2-l1+p2+p3,l2+p2+p3,l2+p3,l2,l2-p4,l1};
RenormalizationLoopMomenta={
{1,0},
{0,1},
{1,1}
};
RenormalizationPower={3,5,6};

GenerateBasis[]
ISP
Basis

Integrand = 0;
For[j = 1, j <= Length[Basis], j++,
  ThisTerm = cc[];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm*c[ i, Basis[[j, i]]]*ISP[[i]]^Basis[[j, i]];
   ];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm //. cc[x1___]*c[i, x2_] -> cc[x1, x2]
   ];
  Integrand = Integrand + ThisTerm;
  ];

x13=DP[L1,p4];
x12=DP[L1,p2];
x14=DP[L1,Omega];
x24=DP[L5,Omega];

fout = OpenWrite["tripent.basis.h"];
WriteString[fout,"id Basistripent = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];
