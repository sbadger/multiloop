#-
auto S x,s,p,k,l,a,b,Dmu,mu,nu,rho,omega;
S TR5;

CF A(a),B(a),IA(a),IB(a);
CF eta,AB,IAB,BB,AA,IAA,IBB,BA,IBA,MOM,CMOM,dot(s),S(s),IS(s);
CF tr5,ASYM(a);

CF cc,Power,NUM,INV;
auto CF TMP;
CF EPS,GGG(s);

CF cs,ds;

S rt2,I,Ds,[Ds-2],m;

auto S tau,Basis;

CF POLY;

set ps:p1,...,p5,p12f,p23f;
set Dmu2:Dmu11,Dmu12,Dmu22,Dmu33;
set mus:mu1,...,mu20,nu1,...,nu20,rho1,...,rho20;

#procedure cancel
id dot(p1?ps,p1?ps) = 0;
id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
id AB(?x)*IAB(?x) = 1;
id NUM(?x)*INV(?x) = 1;
#endprocedure

#procedure Lcontract

id MOM(-p1?,?x) = -MOM(p1,?x);

id EPS(1,p1?,x1?,nu1?) = AB(x1,nu1,p1)*IA(x1,p1)/rt2;
id EPS(-1,p1?,x1?,nu1?) = AB(p1,nu1,x1)*IB(p1,x1)/rt2;

id AB(p1?,nu1?mus,p3?)*GGG(nu1?mus,mu2?mus) = AB(p1,mu2,p3);
id AB(p1?,nu1?mus,p3?)*MOM(p2?,nu1?mus) = AB(p1,p2,p3);
id AB(p1?,nu1?mus,p2?)*AB(p3?,nu1?mus,p4?) = -2*A(p1,p3)*B(p2,p4);

#do fun={EPS,MOM}
id 'fun'(?x,nu1?)*GGG(nu1?,mu2?) = 'fun'(?x,mu2);
#enddo
id GGG(nu1?,mu2?)*GGG(mu2?,mu3?) = GGG(nu1,mu3);
id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id GGG(nu1?,nu1?) = Ds;

id I^2=1;
id rt2^2 = 2;
id 1/rt2^2 = 1/2;

#endprocedure

#procedure subsLmom(path,topo)

*#do rk={1,5,9,13,17}
*id,once dot(k1,px?) = MOM(k1,mu{'rk'})*MOM(px,mu{'rk'});
*id,once AB(p1?,k1,p2?) = AB(p1,mu{'rk'+1},p2)*MOM(k1,mu{'rk'+1});
*id,once dot(k2,px?) = MOM(k2,mu{'rk'+2})*MOM(px,mu{'rk'+2});
*id,once AB(p1?,k2,p2?) = AB(p1,mu{'rk'+3},p2)*MOM(k2,mu{'rk'+3});
*#enddo

#if 'topo'=db431

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*B(p1,p3)*IB(p2,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1)+b2*MOM(p5,mu1)+b3*B(p3,p4)*IB(p3,p5)*AB(p4,mu1,p5)/2+b4*B(p3,p5)*IB(p3,p4)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = -tau1*s23/s13;
id a4 = 1+tau1;

id b1 = 0;
id b2 = 1;
id b3 = tau2/s34;
id b4 = tau3/s35;

id mu11 = s12*s23/s13*tau1*(1+tau1);
id mu22 = -tau2*tau3*s45/s34/s35;
id mu12 =
         + tr5 * (
          - 1/2*s13^-1
          - 1/2*s13^-1*tau3*s35^-1
          - tau1*s13^-1
          - 1/2*(s34 + s45 - s12)*s13^-1*tau2*s34^-1*s35^-1
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^-1*s34^-1*tau3*s35^-1
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^-1*tau2*s34^-1*s35^-1
          )

          - 1/2*(s34 + s45 - s12)*(s23*s34 - s45*s15 - s45*s34 + s12*s15
          - s12*s23)*s13^-1*tau2*s34^-1*s35^-1
          + 1/2*( - s23*s34 + s45*s15 + s45*s34 - 2*s45*s23 + 2*s45^2 - s12*s15 + s12*s23 - 2*s12*s45)*s13^-1*tau3*s35^-1
          - 1/2*(s23*s34 - s45*s15 - s45*s34 + s12*s15 - s12*s23)*s13^-1
          - 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^-1*s34^-1*tau3*s35^-1
          + 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^-1*tau2*s34^-1*s35^-1
         ;

#elseif 'topo'=db332

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*A(p1,p3)*IA(p2,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p3,mu1)+b2*MOM(p4,mu1)+b3*A(p4,p1)*IA(p3,p1)*AB(p3,mu1,p4)/2+b4*A(p3,p1)*IA(p4,p1)*AB(p4,mu1,p3)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1*s13/s23;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;

id mu11 = -tau1*tau2*s13*s12/s23;
id mu22 = -tau3*s34*b4;
id mu12 =
    s14 + tau3*s14 + b4*s13
  + B(p1,p3)*IB(p2,p3)*tau1*( AB(p1,p4,p2)*(1+tau3) + b4*AB(p1,p3,p2) )
  + IA(p1,p4)*IA(p2,p3)*tau2*( A(p1,p4)*A(p2,p3)*s14*tau3 + (b4*s13+s14)*A(p1,p3)*A(p2,p4) );

id b4 = -s14*IAB(p4,p5,p3,p1,p4)*(
  s15 + s45
  + tau1*AB(p1,p5,p2,p3,p1)/s23
  + tau2*AB(p2,p5,p1,p3,p2)/s23
  + tau3*AB(p3,p5,p4,p1,p3)/s13
);

#elseif 'topo'=db422

id MOM(k1,mu1?) = a1*MOM(p1,mu1) + a2*MOM(p2,mu1) + a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2 + a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1) + b2*MOM(p5,mu1) + b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2 + b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = 1-tau1;

id b2 = 0;
id b3 = tau2;
id b4 = tau3;

id mu11 = -tau1*(1-tau1)*s12*s23/s13;
id mu22 = - tau2*tau3*s15*s45/s14;
id mu12 = (b1*s14 + (tau2+tau3)*s15)
  + tau1/s13*( (b1+tau3*s15/s14)*AB(p1,p4,p2,p3,p1) + tau2*AB(p1,p5,p2,p3,p1) )
  + (1-tau1)/s13*( (b1+tau2*s15/s14)*BA(p1,p4,p2,p3,p1) + tau3*BA(p1,p5,p2,p3,p1) )
;

id b1 = -1/s45/s13*( s13*s15 + tau1*AB(p1,p5,p2,p3,p1) + (1-tau1)*BA(p1,p5,p2,p3,p1));

#elseif 'topo'=db331M2

id MOM(k1,mu1?) = a1*MOM(p1,mu1) + a2*MOM(p23f,mu1) + a3*A(p23f,p4)*IA(p1,p4)*AB(p1,mu1,p23f)/2 + a4*B(p23f,p4)*IB(p1,p4)*AB(p23f,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1) + b2*MOM(p5,mu1) + b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2 + b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = s45*INV(s12+s13);
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s45*s15/s14*tau1*tau2;
id mu22 = -s45*s15/s14*tau3*tau4;
id mu12 = s45*s15*INV(s12+s13)*(1-tau1-tau2)*(1+tau3+tau4) - s45*s15/s14*(tau2*tau3+tau1*tau4);

#elseif 'topo'=db3315L

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1)+b2*MOM(p5,mu1)+b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2+b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s12*s23/s13*tau1*tau2;
id mu22 = -s45*s15/s14*tau3*tau4;
id mu12 =   A(p1,p5)*B(p5,p2)*A(p2,p3)*IA(p1,p3)*tau1*(1+tau3)
          + A(p3,p2)*B(p2,p4)*IA(p3,p1)*IB(p1,p4)*s15*tau1*tau4
          + A(p4,p2)*B(p2,p3)*IA(p4,p1)*IB(p1,p3)*s15*tau2*tau3
          + B(p1,p5)*A(p5,p2)*B(p2,p3)*IB(p1,p3)*tau2*(1+tau4)
          + s15*(1+tau3+tau4);

#elseif 'topo'=db331M1

id MOM(k1,mu1?) = a1*MOM(p12f,mu1) + a2*MOM(p3,mu1) + a3*B(p12f,p4)*IB(p3,p4)*AB(p12f,mu1,p3)/2 + a4*A(p12f,p4)*IA(p3,p4)*AB(p3,mu1,p12f)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1) + b2*MOM(p5,mu1) + b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2 + b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s35*s45/s34*tau1*tau2;
id mu22 = -s45*s15/s14*tau3*tau4;
*id mu12 = s45*s15*INV(s12+s13)*(1-tau1-tau2)*(1+tau3+tau4) - s45*s15/s14*(tau2*tau3+tau1*tau4);

id mu12 =
       - s12*tau1*tau2
       + 1/2*tr5*tau4*s14^-1
       - 1/2*tr5*tau3*s14^-1
       - 1/2*tr5*tau2*tau3*s14^-1
       + 1/2*tr5*tau1*tau4*s14^-1
       + 1/2*NUM(s34 + s45)*tr5*tau2*tau4*s34^-1*s14^-1
       - 1/2*NUM(s34 + s45)*tr5*tau1*tau3*s34^-1*s14^-1
       - 1/2*NUM(s34 + s45)*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau2*tau4*s34^-1*s14^-1
       - 1/2*NUM(s34 + s45)*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau1*tau3*s34^-1*s14^-1
       + NUM(s34 + s45 - s12)*tau2
       + NUM(s34 + s45 - s12)*tau1
       + NUM(s34 - s12)
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau4*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau3*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau2*tau3*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*tau1*tau4*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau4*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau3*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau2*tau4*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau2*tau3*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau1*tau4*s14^-1
       - 1/2*NUM( - s34*s23 + 2*s34*s15 + s45*s15 + s45*s34 + s12*s23 - s12*s15)*INV(s45 - s12)*s12*tau1*tau3*s14^-1
       + NUM(s45 - s12)*INV(s45 - s12)*s12*tau1*tau2
       - INV(s45 - s12)*s12*s35
       - INV(s45 - s12)*s12*tau2*s35
       - INV(s45 - s12)*s12*tau1*s35
       + 1/2*INV(s45 - s12)*tr5*s12*tau4*s14^-1
       - 1/2*INV(s45 - s12)*tr5*s12*tau3*s14^-1
       + 1/2*INV(s45 - s12)*tr5*s12*tau2*tau4*s14^-1
       - 1/2*INV(s45 - s12)*tr5*s12*tau2*tau3*s14^-1
       + 1/2*INV(s45 - s12)*tr5*s12*tau1*tau4*s14^-1
       - 1/2*INV(s45 - s12)*tr5*s12*tau1*tau3*s14^-1
      ;

#elseif 'topo'=bt430

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*B(p1,p3)*IB(p2,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1)+b2*MOM(p5,mu1)+b3*B(p3,p4)*IB(p3,p5)*AB(p4,mu1,p5)/2+b4*B(p3,p5)*IB(p3,p4)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = -tau1*s23/s13;
id a4 = 1+tau1;

id b1 = 0;
id b2 = 1;
id b3 = tau2/s34;
id b4 = tau3/s35;

id mu11 = s12*s23/s13*tau1*(1+tau1);
id mu22 = -tau2*tau3*s45/s34/s35;
id mu12 = tau4;

#elseif 'topo'=bt330M2

id MOM(k1,mu1?) = a1*MOM(p1,mu1) + a2*MOM(p23f,mu1) + a3*A(p23f,p4)*IA(p1,p4)*AB(p1,mu1,p23f)/2 + a4*B(p23f,p4)*IB(p1,p4)*AB(p23f,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1) + b2*MOM(p5,mu1) + b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2 + b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = s45*INV(s12+s13);
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s45*s15/s14*tau1*tau2;
id mu22 = -s45*s15/s14*tau3*tau4;
id mu12 = tau5;


#elseif 'topo'=bt3305L

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p4,mu1)+b2*MOM(p5,mu1)+b3*A(p5,p1)*IA(p4,p1)*AB(p4,mu1,p5)/2+b4*B(p5,p1)*IB(p4,p1)*AB(p5,mu1,p4)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = 0;
id b2 = 1;
id b3 = tau3;
id b4 = tau4;

id mu11 = -s12*s23/s13*tau1*tau2;
id mu22 = -s45*s15/s14*tau3*tau4;
id mu12 = tau5;

#elseif 'topo'=db421

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+b4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = 1-tau1;

id b1 = tau2;
id b2 = tau3;
id b3 = tau4;
id b4 = -(1+tau2+tau3)*s12/s23-(1+tau2)*s13/s23-(1+tau3+tau4);

id mu11 = -s12*s23/s13*tau1*(1-tau1);
id mu22 = s12/s13*(s12*(1 + tau2 + tau3)*tau4 + s23*tau4*(1 + tau3 + tau4) + s13*(tau4 + tau2*(tau3 + tau4)));
id mu12 = s12/s13*(s12*tau1*(1 + tau2 + tau3) + s13*(tau1 + tau1*tau2 + tau3) + s23*(tau1 + tau1*tau3 - tau4 + 2*tau1*tau4));

#elseif 'topo'=bt420

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+b4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = 1-tau1;

id b1 = tau2;
id b2 = tau3;
id b3 = tau4;
id b4 = -(1+tau2+tau3)*s12/s23-(1+tau2)*s13/s23-(1+tau3+tau4);

id mu11 = -s12*s23/s13*tau1*(1-tau1);
id mu22 = s12/s13*(s12*(1 + tau2 + tau3)*tau4 + s23*tau4*(1 + tau3 + tau4) + s13*(tau4 + tau2*(tau3 + tau4)));
id mu12 = tau5;

#elseif 'topo'=db3215L

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+b4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = tau3;
id b2 = tau4;
id b3 = tau5;
id b4 = (tau3-tau4-tau5)-s12/s23*tau4-s45/s23*(1+tau3);

id mu11 = -s12*s23/s13*tau1*tau2;
id mu22 = s12/s13*(s23*tau5+s45)*tau5 + s12*tau3*tau4 - (s23-s45)*s12/s13*tau3*tau5 + (s23+s12)*s12/s13*tau4*tau5;
id mu12 = s12*tau4 + s12*s45/s13*tau1 + s12*s23/s13*tau5*(tau1-tau2) - (s23-s45)*s12/s13*tau1*tau3 + (s23+s12)*s12/s13*tau1*tau4;

#elseif 'topo'=bt3205L

id MOM(k1,mu1?) = a1*MOM(p1,mu1)+a2*MOM(p2,mu1)+a3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+a4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;
id MOM(k2,mu1?) = b1*MOM(p1,mu1)+b2*MOM(p2,mu1)+b3*A(p2,p3)*IA(p1,p3)*AB(p1,mu1,p2)/2+b4*B(p2,p3)*IB(p1,p3)*AB(p2,mu1,p1)/2;

id a1 = 1;
id a2 = 0;
id a3 = tau1;
id a4 = tau2;

id b1 = tau3;
id b2 = tau4;
id b3 = tau5;
id b4 = (tau3-tau4-tau5)-s12/s23*tau4-s45/s23*(1+tau3);

id mu11 = -s12*s23/s13*tau1*tau2;
id mu22 = s12/s13*(s23*tau5+s45)*tau5 + s12*tau3*tau4 - (s23-s45)*s12/s13*tau3*tau5 + (s23+s12)*s12/s13*tau4*tau5;
id mu12 = tau6;

#endif

*#call Lcontract

#endprocedure

#procedure changevars

id dot(p1?ps,p1?ps) = 0;
id AB(p1?ps,p1?ps,?x) = 0;
id AB(?x,p1?ps,p1?ps) = 0;
id AB(p1?ps,p2?ps,p3?ps) = A(p1,p2)*B(p2,p3);
#call cancel

id dot(p1?ps,p2?ps) = S(p1,p2)/2;
#do i=1,5
#do j=1,5
#if 'j'>'i'
id a'i''j' = A(p'i',p'j');
id 1/a'i''j' = IA(p'i',p'j');
id b'i''j' = B(p'i',p'j');
id 1/b'i''j' = IB(p'i',p'j');
id s'i''j' = S(p'i',p'j');
id 1/s'i''j' = IS(p'i',p'j');
#endif
#enddo
#enddo
id tr5 = B(p1,p2)*A(p2,p3)*B(p3,p4)*A(p4,p1) - A(p1,p2)*B(p2,p3)*A(p3,p4)*B(p4,p1);

repeat;
id,once s12 = x1;
id,once s13 = -x5*x2-x1;
id,once s14 = (-x5*x2*x3+x5*x2*x1*x3+x5*x1*x3+x5*x1*x2-x1*x3)/(-x3+x1*x3+x1);
id,once s15 = -x1*x3*(x5-1)/(-x3+x1*x3+x1);
id,once s23 = x4*x2;
id,once s24 = -(x2+x1)*(-x4*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/x2/(-x3+x1*x3+x1);
id,once s25 = (x2*x3+x3+x2)/x2*x1^2*(-1+x4)/(-x3+x1*x3+x1);
id,once s34 = x1*(-x5*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/x2/(-x3+x1*x3+x1);
id,once s35 = -(-x2*x3+x1*x2*x3+x1*x3+x1*x2)*(-x1+x4*x1-x5*x2+x4*x2)/x2/(-x3+x1*x3+x1);
id,once s45 = x2*(-x5+x4);
endrepeat;
#write "step 1"
repeat;
id,once A(p1,p2) = -1;
id,once IA(p1,p2) = -1;
id,once B(p1,p2) = x1;
id,once IB(p1,p2) = 1/x1;
id,once S(p1,p2) = x1;
id,once IS(p1,p2) = 1/x1;
id,once A(p1,p3) = -1;
id,once IA(p1,p3) = -1;
id,once B(p1,p3) = -x5*x2-x1;
id,once IB(p1,p3) = -1/(x5*x2+x1);
id,once S(p1,p3) = -x5*x2-x1;
id,once IS(p1,p3) = -1/(x5*x2+x1);
id,once A(p1,p4) = -1;
id,once IA(p1,p4) = -1;
endrepeat;
#write "step 2"
repeat;
id,once B(p1,p4) = (-x5*x2*x3+x5*x2*x1*x3+x5*x1*x3+x5*x1*x2-x1*x3)/(-x3+x1*x3+x1);
id,once IB(p1,p4) = (-x3+x1*x3+x1)/(-x5*x2*x3+x5*x2*x1*x3+x5*x1*x3+x5*x1*x2-x1*x3);
id,once S(p1,p4) = (-x5*x2*x3+x5*x2*x1*x3+x5*x1*x3+x5*x1*x2-x1*x3)/(-x3+x1*x3+x1);
id,once IS(p1,p4) = (-x3+x1*x3+x1)/(-x5*x2*x3+x5*x2*x1*x3+x5*x1*x3+x5*x1*x2-x1*x3);
id,once A(p1,p5) = -1;
id,once IA(p1,p5) = -1;
id,once B(p1,p5) = -x1*x3*(x5-1)/(-x3+x1*x3+x1);
id,once IB(p1,p5) = -1/x1/x3/(x5-1)*(-x3+x1*x3+x1);
id,once S(p1,p5) = -x1*x3*(x5-1)/(-x3+x1*x3+x1);
id,once IS(p1,p5) = -1/x1/x3/(x5-1)*(-x3+x1*x3+x1);
id,once A(p2,p3) = 1/x1;
id,once IA(p2,p3) = x1;
id,once B(p2,p3) = -x4*x1*x2;
id,once IB(p2,p3) = -1/x4/x1/x2;
id,once S(p2,p3) = x4*x2;
id,once IS(p2,p3) = 1/x4/x2;
id,once A(p2,p4) = (x2+x1)/x1/x2;
id,once IA(p2,p4) = 1/(x2+x1)*x1*x2;
endrepeat;
#write "step 3"
repeat;
id,once B(p2,p4) = x1*(-x4*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/(-x3+x1*x3+x1);
id,once IB(p2,p4) = (-x3+x1*x3+x1)/x1/(-x4*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2);
id,once S(p2,p4) = -(x2+x1)*(-x4*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/x2/(-x3+x1*x3+x1);
id,once IS(p2,p4) = -1/(x2+x1)/(-x4*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)*x2*(-x3+x1*x3+x1);
id,once A(p2,p5) = (x2*x3+x3+x2)/x2/x3;
id,once IA(p2,p5) = 1/(x2*x3+x3+x2)*x2*x3;
id,once B(p2,p5) = -x1^2*x3*(-1+x4)/(-x3+x1*x3+x1);
id,once IB(p2,p5) = -1/x1^2/x3/(-1+x4)*(-x3+x1*x3+x1);
id,once S(p2,p5) = (x2*x3+x3+x2)/x2*x1^2*(-1+x4)/(-x3+x1*x3+x1);
id,once IS(p2,p5) = 1/(x2*x3+x3+x2)*x2/x1^2/(-1+x4)*(-x3+x1*x3+x1);
id,once A(p3,p4) = 1/x2;
id,once IA(p3,p4) = x2;
id,once B(p3,p4) = -x1*(-x5*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/(-x3+x1*x3+x1);
id,once IB(p3,p4) = -(-x3+x1*x3+x1)/x1/(-x5*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2);
id,once S(p3,p4) = x1*(-x5*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)/x2/(-x3+x1*x3+x1);
id,once IS(p3,p4) = 1/x1/(-x5*x2*x3-x1*x3+x4*x1*x2*x3+x4*x1*x3+x4*x1*x2)*x2*(-x3+x1*x3+x1);
id,once A(p3,p5) = (-x2*x3+x1*x2*x3+x1*x3+x1*x2)/x1/x2/x3;
id,once IA(p3,p5) = 1/(-x2*x3+x1*x2*x3+x1*x3+x1*x2)*x1*x2*x3;
id,once B(p3,p5) = x1*x3*(-x1+x4*x1-x5*x2+x4*x2)/(-x3+x1*x3+x1);
id,once IB(p3,p5) = (-x3+x1*x3+x1)/x1/x3/(-x1+x4*x1-x5*x2+x4*x2);
id,once S(p3,p5) = -(-x2*x3+x1*x2*x3+x1*x3+x1*x2)*(-x1+x4*x1-x5*x2+x4*x2)/x2/(-x3+x1*x3+x1);
id,once IS(p3,p5) = -1/(-x2*x3+x1*x2*x3+x1*x3+x1*x2)/(-x1+x4*x1-x5*x2+x4*x2)*x2*(-x3+x1*x3+x1);
id,once A(p4,p5) = (-x3+x1*x3+x1)/x1/x3;
id,once IA(p4,p5) = 1/(-x3+x1*x3+x1)*x1*x3;
id,once B(p4,p5) = -x1*x2*x3*(-x5+x4)/(-x3+x1*x3+x1);
id,once IB(p4,p5) = -1/x1/x2/x3/(-x5+x4)*(-x3+x1*x3+x1);
id,once S(p4,p5) = x2*(-x5+x4);
id,once IS(p4,p5) = 1/x2/(-x5+x4);
endrepeat;

#write "step 4"
denominators INV;
B INV;
.sort
keep brackets;
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

#endprocedure

#procedure simplify5new

id NUM(x?) = x;
AB x1,...,x5;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
id NUM(x?)*INV(x?) = 1;
id NUM(x?) = x;
AB x1,...,x5;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
id NUM(x?)*INV(x?) = 1;
id NUM(x?) = x;
AB x1,...,x5;
print+s;
.sort
collect NUM;
multiply POLY(1,1);
repeat id NUM(x1?)*POLY(x2?,x3?) = POLY(x1*x2,x3);
repeat id INV(x1?)*POLY(x2?,x3?) = POLY(x2,x1*x3);
AB POLY;
.sort
collect NUM;
splitarg NUM;
id NUM(?x) = TMP(?x,0);
repeat;
id TMP(POLY(x1?,x2?$num1),?args) = INV(x2)*TMP(POLY(x1,x2),?args);
argument TMP;
multiply POLY($num1,1);
id POLY(x1?,1)*POLY(x2?,x1?) = POLY(x2,1);
id POLY(x1?,1)*POLY(x2?,x3?) = POLY(x1*x2,x3);
endargument;
id TMP(x1?,?args) = TMP(?args,x1);
id TMP(0,?x) = NUM(?x);
endrepeat;
argument NUM;
id POLY(x1?,1) = x1;
endargument;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

#endprocedure

#procedure INVsimplify5

argument INV;

id dot(p1?ps,p1?ps) = 0;
id AB(p1?ps,p1?ps,?x) = 0;
id AB(?x,p1?ps,p1?ps) = 0;
id AB(p1?ps,p2?ps,p3?ps) = A(p1,p2)*B(p2,p3);
id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;
#call cancel

#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
repeat id AB(?x,p1?)*AB(p1?,?y) = AB(?x,p1,?y);

id AB(p1?,p2?,p3?,p4?,p1?,p5?,p6?,p7?,p1?) = AB(p1,p2,p3,p4,p1)*AB(p1,p5,p6,p7,p1);
id AB(p1?,p2?,p3?,p1?,p4?,p5?,p1?) = -AB(p1,p2,p3,p4,p1,p5,p1)+S(p1,p4)*AB(p1,p2,p3,p5,p1);
id AB(p1?,p2?,p3?,p4?,p1?,p5?,p1?) = S(p1,p5)*AB(p1,p2,p3,p4,p1);
id AB(p1?,p2?,p3?,p4?,p1?) = (S(p1,p2)*S(p3,p4)-S(p1,p3)*S(p2,p4)+S(p1,p4)*S(p2,p3) - tr5(p1,p2,p3,p4))/2;
id tr5(?x,p5,?y) = -tr5(?x,p1,?y)-tr5(?x,p2,?y)-tr5(?x,p3,?y)-tr5(?x,p4,?y);
id tr5(?x,p1?,?y,p1?,?z) = 0;
id tr5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = TR5;

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

id TR5^2 = s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2;

endargument;

#endprocedure

#procedure simplify5

id dot(p1?ps,p1?ps) = 0;
id AB(p1?ps,p1?ps,?x) = 0;
id AB(?x,p1?ps,p1?ps) = 0;
id AB(p1?ps,p2?ps,p3?ps) = A(p1,p2)*B(p2,p3);
id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;
#call cancel

#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
repeat id AB(?x,p1?)*AB(p1?,?y) = AB(?x,p1,?y);

id AB(p1?,p2?,p3?,p4?,p1?,p5?,p6?,p7?,p1?) = AB(p1,p2,p3,p4,p1)*AB(p1,p5,p6,p7,p1);
id AB(p1?,p2?,p3?,p4?,p2?,p5?,p1?) = -AB(p1,p2,p3,p2,p4,p5,p1) + S(p2,p4)*AB(p1,p2,p3,p5,p1);
id AB(p1?,p2?,p3?,p1?,p4?,p5?,p1?) = -AB(p1,p2,p3,p4,p1,p5,p1) + S(p1,p4)*AB(p1,p2,p3,p5,p1);
id AB(p1?,p2?,p3?,p4?,p1?,p5?,p1?) = S(p1,p5)*AB(p1,p2,p3,p4,p1);
id AB(p1?,p2?,p3?,p2?,p4?,p5?,p1?) = S(p2,p3)*AB(p1,p2,p4,p5,p1);
id AB(p1?,p2?,p3?,p4?,p1?) = (S(p1,p2)*S(p3,p4)-S(p1,p3)*S(p2,p4)+S(p1,p4)*S(p2,p3) - tr5(p1,p2,p3,p4))/2;
id tr5(?x,p5,?y) = -tr5(?x,p1,?y)-tr5(?x,p2,?y)-tr5(?x,p3,?y)-tr5(?x,p4,?y);
id tr5(?x,p1?,?y,p1?,?z) = 0;
id tr5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = TR5;

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

id TR5^2 = s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2;

AB s12,s23,s34,s45,s15,s13,s14,s24,s25,s35;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument INV,NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

#endprocedure

*off statistics;

