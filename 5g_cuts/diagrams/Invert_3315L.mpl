interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

Ds:=Dsm2+2:

read("OnShellSolutions.mpl");
read("tmp/checks_5g+++++.mpl");

read("tmp/dbnp331M3.input.mpl");
read("../basis/db3315L.invert.mpl"):

M,Dvec,Clbl := Invert_3315L([p4,p5,p3,p1,p2]):

#for i from 1 to 160 do;
#for j from 1 to 160 do;
#  print(M[i][j]);
#od;
#od;
#Rank(M);
#quit;

r1:=4;
r2:=4;
r3:=4;
r4:=4;

for i1 from 0 to 6 do:
for i2 from 0 to 6 do:
for i3 from 0 to 6 do:
for i4 from 0 to 6 do:
  ds(i1,i2,i3,i4):=0:
od:
od:
od:
od:

for i1 from 0 to r1 do:
for i2 from 0 to r2 do:
for i3 from 0 to r3 do:
for i4 from 0 to r4 do:

t1 := evalf(1.1*exp(2*Pi*I*i1/(r1+1)));
t2 := evalf(1.3*exp(2*Pi*I*i2/(r2+1)));
t3 := evalf(1.2*exp(2*Pi*I*i3/(r3+1)));
t4 := evalf(0.9*exp(2*Pi*I*i4/(r4+1)));

k2p45,k1p12,mu22,mu12,mu11 := on_shell_3315L([p4,p5,p3,p1,p2],1,2,3,4):
k1:=-k1p12+p1+p2:
k2:=-k2p45+p4+p5:
cut_value := expand(INT_np331M3([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
print(i1,i2,i3,i4);

for k1 from 0 to r1 do:
for k2 from 0 to r2 do:
for k3 from 0 to r3 do:
for k4 from 0 to r4 do:
  ds(k1,k2,k3,k4) := expand(ds(k1,k2,k3,k4) + evalf(cut_value/t1^k1/t2^k2/t3^k3/t4^k4/((r1+1)*(r2+1)*(r3+1)*(r4+1)))):
od:
od:
od:
od:

od:
od:
od:
od:

Dvec;
Cvec:=MatrixInverse(M).Dvec;

for i from 1 to 160 do;
  print(Clbl[i],"=",Cvec[i])
od:
