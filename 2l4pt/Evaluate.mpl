# vim: set foldmethod=marker:
interface(quiet=true):
interface(displayprecision=10):

mypow:=proc(a,b); return a^b; end:

Digits:=40:
with(spinors):

#genmoms([0,0,0,0]):
angletheta := 2.4;
anglephi := 1.1;
overalllength := 0.6;

p1 := overalllength*<-1.0,1.0,0.0,0.0>;
p2 := overalllength*<-1.0,-1.0,0.0,0.0>;
p3 := overalllength*<1.0,cos(anglephi)*sin(angletheta),sin(anglephi)*sin(angletheta),cos(angletheta)>;
p4 := overalllength*<1.0,-cos(anglephi)*sin(angletheta),-sin(anglephi)*sin(angletheta),-cos(angletheta)>;

#nf = 1;
#ns = 0;
Nf:=1;Ns:=0;

tree[12]:= A(p1,p2)^3*IA(p2,p3)*IA(p3,p4)*IA(p4,p1):
tree[10]:= A(p1,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1);
tree[6] := A(p2,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1);

S(p1,p2);
S(p1,p3);
S(p1,p4);

read("C7.planar.INT.log"):
read("C6.planar.INT.log"):

Hel:=10;
check:=tree[Hel]*S(p1,p2)^2*S(p1,p4);

#Omega4:= ( AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1) )*IS(p1,p2)/2:
#Omega31:= ( eta(p3,p4)+eta(p4,p3) )/2:
#Omega32:= I*( eta(p3,p4)-eta(p4,p3) )/2:

#Numerator:=(KK1,KK2)->

#+2.1
#+1.1*(2*dot(KK2,p1))
#-0.7*(2*dot(KK2,p1))^3
#-3.3*(2*dot(KK1,Omega4))
#+4.9*(2*dot(KK2,Omega4))
#-2.0*(2*dot(KK1,p4))
#-0.4*(2*dot(KK1,Omega4))*(2*dot(KK1,p4))^3

#+I*S(l1-p1)*(
#+3.14
#+1.5*(2*dot(KK1,p3))^2
#-2.27*(2*dot(KK1,p3))*(2*dot(KK2,Omega31))
#-58*I*(2*dot(KK2,Omega31))
#):

#for fl1 in [0,1/2,1] do:
#for fl2 in [0,1/2,1,-1/2] do:
#  C7_1_INT[Hel,fl1,fl2]:=0:
#  C6_4_INT[Hel,fl1,fl2]:=0:
#od:
#od:

#C7_1_INT[Hel,0,0]:=Numerator(l1,-l6):
#C6_4_INT[Hel,0,0]:=I*Numerator(l1,-l5)/S(l1-p1):


### planar double box ###

#{{{ OnShell_boxbox
OnShell_boxbox:=proc(p1,p2,p3,p4,sol,tau)
local k1,k2;
global l1,l2,l3,l4,l5,l6,l7;
  if sol=1 then:
  k1 := 1/2*B(p2,p3)*IB(p1,p3)*eta(p2,p1):
  k2 := 1/2*B(p1,p4)*IB(p1,p3)*eta(p4,p3)*tau:
elif sol=2 then:
  k1 :=  - 1/2*NUM(S(p1,p2) + S(p1,p3)*tau)/tau*INV(S(p1,p4))*B(p2,p3)*IB(p1,p3)*eta(p2,p1):
  k2 := 1/2*NUM( - 1 + tau)*B(p1,p3)*IB(p1,p4)*eta(p3,p4):
elif sol=3 then:
  k1 := 1/2*B(p2,p3)*IB(p1,p3)*eta(p2,p1)*tau;
  k2 := 1/2*B(p1,p4)*IB(p1,p3)*eta(p4,p3);
elif sol=4 then:
  k1 :=  - 1/2*NUM(tau + S(p1,p2))/tau*B(p1,p3)*IB(p2,p3)*eta(p1,p2);
  k2 :=  - 1/2*NUM( - tau + S(p1,p3))*INV(S(p1,p4))*B(p1,p4)*IB(p1,p3)*eta(p4,p3);
elif sol=5 then:
  k1 := 1/2*NUM(S(p1,p4))*INV(S(p1,p3))*B(p1,p3)*IB(p2,p3)*eta(p1,p2);
  k2 := 1/2*B(p1,p3)*IB(p1,p4)*eta(p3,p4)*tau;
elif sol=6 then:
  k1 := 1/2*B(p1,p3)*IB(p2,p3)*eta(p1,p2)*tau;
  k2 := 1/2*NUM(S(p1,p4))*INV(S(p1,p3))*B(p1,p3)*IB(p1,p4)*eta(p3,p4);
fi:

l1:=k1+p1;
l2:=k1;
l3:=k1-p2;
l4:=k2+p3;
l5:=k2;
l6:=k2-p4;
l7:=k2-k1-p1-p4;

return;

end:
#}}}

R1:=1.0:
R2:=1.1:
delta1:=1e-3*I:
delta2:=2e-3*I:

#{{{ compute solution polynomial from trees ('d' coeffs)

Dvec:=Vector(38):
smin:=[0,-4,0,-4,0,0]:

i:=1:
for sol from 1 to 6 do:
for k1 from 0 to 8 do:
  t1:=evalf(R1*exp(2*Pi*I*k1/9+delta1));
  OnShell_boxbox(p1,p2,p3,p4,sol,t1);
  Integrand:=evalf(
    +C7_1_INT[Hel,0,0]
    +C7_1_INT[Hel,1/2,1/2]
    +C7_1_INT[Hel,1/2,0]
    +C7_1_INT[Hel,0,1/2]
    +C7_1_INT[Hel,1,1]
    +C7_1_INT[Hel,0,1]
    +C7_1_INT[Hel,1,0]
    +C7_1_INT[Hel,1/2,-1/2]
    +C7_1_INT[Hel,1/2,1]
    +C7_1_INT[Hel,1,1/2]
  )/9.;
  j:=i:
  for pow from smin[sol] to 4 do:
    Dvec[j]:=Dvec[j]+Integrand/t1^(pow);
    j:=j+1:
  od:
od:
  i:=j:
od:

printf("polynomial coefficients from trees:\n");
for i from 1 to 38 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%.10Ze\n",Dvec[i]);
od:

#}}}

#{{{ invert to fit full integrand

read("../2lintegrands/reduction/final/boxbox.integrand.mpl"):

printf("integrand coefficients:\n");
for i from 1 to 32 do:
if(abs(Cvec[i])<1e-20)then:Cvec[i]:=0:fi:
printf("%.10Ze\n",Cvec[i]);
od:
printf("verifying null conditions:\n");
for i from 1 to 6 do:
if(abs(Zvec[i])<1e-20)then:Zvec[i]:=0:fi:
printf("%.10Ze\n",Zvec[i]);
od:

#}}}

Cvec_boxbox:=Cvec:

#{{{ boxbox integrand function

C7_1_integrand:=proc(l1,l2,p1,p2,p3,p4,Cvec::Vector) local Omega;

Omega:= ( AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1) )*IS(p1,p2)/2:

return
  Cvec[1] + 
  2*dot(l2,p1)*Cvec[2] +
  mypow(2*dot(l2,p1),2)*Cvec[3] +
  mypow(2*dot(l2,p1),3)*Cvec[4] +
  mypow(2*dot(l2,p1),4)*Cvec[5] + 2*dot(l1,p4)*Cvec[6] +
  2*dot(l1,p4)*2*dot(l2,p1)*Cvec[7] +
  2*dot(l1,p4)*mypow(2*dot(l2,p1),2)*Cvec[8] +
  2*dot(l1,p4)*mypow(2*dot(l2,p1),3)*Cvec[9] +
  2*dot(l1,p4)*mypow(2*dot(l2,p1),4)*Cvec[10] +
  mypow(2*dot(l1,p4),2)*Cvec[11] +
  mypow(2*dot(l1,p4),2)*2*dot(l2,p1)*Cvec[12] +
  mypow(2*dot(l1,p4),3)*Cvec[13] +
  mypow(2*dot(l1,p4),3)*2*dot(l2,p1)*Cvec[14] +
  mypow(2*dot(l1,p4),4)*Cvec[15] +
  mypow(2*dot(l1,p4),4)*2*dot(l2,p1)*Cvec[16] +
  2*dot(l2,Omega)*Cvec[17] +
  2*dot(l2,Omega)*2*dot(l2,p1)*Cvec[18] +
  2*dot(l2,Omega)*mypow(2*dot(l2,p1),2)*Cvec[19] +
  2*dot(l2,Omega)*mypow(2*dot(l2,p1),3)*Cvec[20] +
  2*dot(l1,p4)*2*dot(l2,Omega)*Cvec[21] +
  2*dot(l1,p4)*2*dot(l2,Omega)*2*dot(l2,p1)*Cvec[22] +
  2*dot(l1,p4)*2*dot(l2,Omega)*mypow(2*dot(l2,p1),2)*Cvec[23] +
  2*dot(l1,p4)*2*dot(l2,Omega)*mypow(2*dot(l2,p1),3)*Cvec[24] +
  mypow(2*dot(l1,p4),2)*2*dot(l2,Omega)*Cvec[25] +
  mypow(2*dot(l1,p4),3)*2*dot(l2,Omega)*Cvec[26] +
  mypow(2*dot(l1,p4),4)*2*dot(l2,Omega)*Cvec[27] +
  2*dot(l1,Omega)*Cvec[28] +
  2*dot(l1,Omega)*2*dot(l2,p1)*Cvec[29] +
  2*dot(l1,Omega)*2*dot(l1,p4)*Cvec[30] +
  2*dot(l1,Omega)*mypow(2*dot(l1,p4),2)*Cvec[31] +
  2*dot(l1,Omega)*mypow(2*dot(l1,p4),3)*Cvec[32];

end:

#}}}

### planar triangle|box (1) ###

#{{{ OnShell_tri|box(1)
OnShell_tribox1:=proc(P1,p2,p3,sol,tau1,tau2)
local k1,k2;
global l1,l2,l3,l4,l5,l6;
  if sol=1 then:
  k1:=  - tau1*tau2*p2 + NUM( - 1 + tau1*tau2)*p3 - 1/2*NUM( - 1 + tau1*tau2)*eta(p3,p2)*tau2 + 1/2*eta(p2,p3)*tau1;
  k2:= 1/2*eta(p3,p2)*tau2;
elif sol=2 then:
  k1:=  - tau1*tau2*p2 + NUM( - 1 + tau1*tau2)*p3 - 1/2*NUM( - 1 + tau1*tau2)*eta(p2,p3)*tau2 + 1/2*eta(p3,p2)*tau1;
  k2:= 1/2*eta(p2,p3)*tau2;
elif sol=3 then:
  k1:=  - p3 + 1/2*eta(p2,p3)*tau1;
  k2:= 1/2*eta(p2,p3)*tau2;
elif sol=4 then:
  k1:=  - p3 + 1/2*eta(p3,p2)*tau1;
  k2:= 1/2*eta(p3,p2)*tau2;
fi:

l1:=k1;
l2:=k1-P1;
l3:=k2+p2;
l4:=k2;
l5:=k2-p3;
l6:=k2-k1-p3;

return;

end:

#}}}

#{{{ compute solution polynomial from trees ('d' coeffs)

Dvec:=Vector(94):
smax:=[[5,6,7,8],[5,6,7,8],[4,4,3,2],[4,4,3,2]]:

i:=1:
for sol from 1 to 4 do:
for k1 from 0 to 3 do:
  t1:=evalf(R1*exp(2*Pi*I*k1/4+delta1));
  for k2 from 0 to 8 do:
    t2:=evalf(R2*exp(2*Pi*I*k2/9+delta2));
    OnShell_tribox1(p1+p2,p3,p4,sol,t1,t2);

    Integrand:=evalf(
      +C6_4_INT[Hel,0,0]
      +C6_4_INT[Hel,1/2,1/2]
      +C6_4_INT[Hel,1/2,0]
      +C6_4_INT[Hel,0,1/2]
      +C6_4_INT[Hel,1,1]
      +C6_4_INT[Hel,0,1]
      +C6_4_INT[Hel,1,0]
      +C6_4_INT[Hel,1/2,-1/2]
      +C6_4_INT[Hel,1/2,1]
      +C6_4_INT[Hel,1,1/2]
      -I*C7_1_integrand(l1,-l5,p1,p2,p3,p4,Cvec_boxbox)/S(l1-p1)
    )/4./9.;
    j:=i:
    for pow1 from 0 to 3 do:
      for pow2 from 0 to smax[sol][pow1+1] do:
        Dvec[j]:=Dvec[j]+Integrand/t1^(pow1)/t2^(pow2);
        j:=j+1:
      od:
    od:
  od:
od:
i:=j:
od:

printf("polynomial coefficients from trees:\n");
printf("###\n");
for i from 1 to 94 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%d : %.10Ze\n",i,Dvec[i]);
od:
printf("###\n");

#}}}

#{{{ invert to fit full integrand

P1:=p1+p2:
read("../2lintegrands/reduction/final/tribox1.integrand.mpl"):

printf("integrand coefficients:\n");
for i from 1 to 69 do:
if(abs(Cvec[i])<1e-20)then:Cvec[i]:=0:fi:
printf("%d : %.10Ze\n",i,Cvec[i]);
od:
printf("verifying null conditions:\n");
for i from 1 to 25 do:
if(abs(Zvec[i])<1e-20)then:Zvec[i]:=0:fi:
printf("%.10Ze\n",Zvec[i]);
od:

#}}}

#{{{ tribox1 integrand function

C6_4_integrand:=proc(l1,l2,P1,p2,p3,Cvec::Vector) local Omega1,Omega2;

Omega1:= ( eta(p2,p3)+eta(p3,p2) )*IS(P1)/2:
Omega2:= I*( eta(p2,p3)-eta(p3,p2) )*IS(P1)/2:

return
  Cvec[1] + dot(l1,p2)*Cvec[2] + 
  mypow(dot(l1,p2),2)*Cvec[3] + 
  mypow(dot(l1,p2),3)*Cvec[4] + 
  dot(l2,Omega1)*Cvec[5] + 
  dot(l1,p2)*dot(l2,Omega1)*Cvec[6] + 
  mypow(dot(l1,p2),2)*dot(l2,Omega1)*Cvec[7] + 
  mypow(dot(l1,p2),3)*dot(l2,Omega1)*Cvec[8] + 
  mypow(dot(l2,Omega1),2)*Cvec[9] + 
  dot(l1,p2)*mypow(dot(l2,Omega1),2)*Cvec[10] + 
  mypow(dot(l2,Omega1),3)*Cvec[11] + 
  dot(l1,p2)*mypow(dot(l2,Omega1),3)*Cvec[12] + 
  mypow(dot(l2,Omega1),4)*Cvec[13] + 
  dot(l1,p2)*mypow(dot(l2,Omega1),4)*Cvec[14] + 
  dot(l1,Omega1)*Cvec[15] + 
  dot(l1,Omega1)*dot(l1,p2)*Cvec[16] + 
  dot(l1,Omega1)*mypow(dot(l1,p2),2)*Cvec[17] + 
  dot(l1,Omega1)*dot(l2,Omega1)*Cvec[18] + 
  dot(l1,Omega1)*dot(l1,p2)*dot(l2,Omega1)*Cvec[19] + 
  dot(l1,Omega1)*mypow(dot(l1,p2),2)*dot(l2,Omega1)*Cvec[20] + 
  dot(l1,Omega1)*mypow(dot(l2,Omega1),2)*Cvec[21] + 
  dot(l1,Omega1)*dot(l1,p2)*mypow(dot(l2,Omega1),2)*Cvec[22] + 
  dot(l1,Omega1)*mypow(dot(l2,Omega1),3)*Cvec[23] + 
  dot(l1,Omega1)*dot(l1,p2)*mypow(dot(l2,Omega1),3)*Cvec[24] + 
  dot(l1,Omega1)*mypow(dot(l2,Omega1),4)*Cvec[25] + 
  mypow(dot(l1,Omega1),2)*Cvec[26] + 
  mypow(dot(l1,Omega1),2)*dot(l1,p2)*Cvec[27] + 
  mypow(dot(l1,Omega1),2)*dot(l2,Omega1)*Cvec[28] + 
  mypow(dot(l1,Omega1),2)*dot(l1,p2)*dot(l2,Omega1)*Cvec[29] + 
  mypow(dot(l1,Omega1),2)*mypow(dot(l2,Omega1),2)*Cvec[30] + 
  mypow(dot(l1,Omega1),2)*dot(l1,p2)*mypow(dot(l2,Omega1),2)*
   Cvec[31] + mypow(dot(l1,Omega1),2)*mypow(dot(l2,Omega1),3)*
   Cvec[32] + mypow(dot(l1,Omega1),3)*Cvec[33] + 
  mypow(dot(l1,Omega1),3)*dot(l2,Omega1)*Cvec[34] + 
  mypow(dot(l1,Omega1),3)*mypow(dot(l2,Omega1),2)*Cvec[35] + 
  dot(l2,Omega2)*Cvec[36] + 
  dot(l1,p2)*dot(l2,Omega2)*Cvec[37] + 
  mypow(dot(l1,p2),2)*dot(l2,Omega2)*Cvec[38] + 
  mypow(dot(l1,p2),3)*dot(l2,Omega2)*Cvec[39] + 
  dot(l2,Omega1)*dot(l2,Omega2)*Cvec[40] + 
  dot(l1,p2)*dot(l2,Omega1)*dot(l2,Omega2)*Cvec[41] + 
  mypow(dot(l2,Omega1),2)*dot(l2,Omega2)*Cvec[42] + 
  dot(l1,p2)*mypow(dot(l2,Omega1),2)*dot(l2,Omega2)*Cvec[43] + 
  mypow(dot(l2,Omega1),3)*dot(l2,Omega2)*Cvec[44] + 
  dot(l1,p2)*mypow(dot(l2,Omega1),3)*dot(l2,Omega2)*Cvec[45] + 
  dot(l1,Omega1)*dot(l2,Omega2)*Cvec[46] + 
  dot(l1,Omega1)*dot(l1,p2)*dot(l2,Omega2)*Cvec[47] + 
  dot(l1,Omega1)*mypow(dot(l1,p2),2)*dot(l2,Omega2)*Cvec[48] + 
  dot(l1,Omega1)*dot(l2,Omega1)*dot(l2,Omega2)*Cvec[49] + 
  dot(l1,Omega1)*dot(l1,p2)*dot(l2,Omega1)*dot(l2,Omega2)*Cvec[50] + 
  dot(l1,Omega1)*mypow(dot(l2,Omega1),2)*dot(l2,Omega2)*Cvec[51] + 
  dot(l1,Omega1)*dot(l1,p2)*mypow(dot(l2,Omega1),2)*dot(l2,Omega2)*
   Cvec[52] + dot(l1,Omega1)*mypow(dot(l2,Omega1),3)*dot(l2,Omega2)*
   Cvec[53] + mypow(dot(l1,Omega1),2)*dot(l2,Omega2)*Cvec[54] + 
  mypow(dot(l1,Omega1),2)*dot(l1,p2)*dot(l2,Omega2)*Cvec[55] + 
  mypow(dot(l1,Omega1),2)*dot(l2,Omega1)*dot(l2,Omega2)*Cvec[56] + 
  mypow(dot(l1,Omega1),2)*dot(l1,p2)*dot(l2,Omega1)*dot(l2,Omega2)*
   Cvec[57] + mypow(dot(l1,Omega1),2)*mypow(dot(l2,Omega1),2)*
   dot(l2,Omega2)*Cvec[58] + mypow(dot(l1,Omega1),3)*dot(l2,Omega2)*
   Cvec[59] + mypow(dot(l1,Omega1),3)*dot(l2,Omega1)*dot(l2,Omega2)*
   Cvec[60] + dot(l1,Omega2)*Cvec[61] + 
  dot(l1,Omega2)*dot(l1,p2)*Cvec[62] + 
  dot(l1,Omega2)*mypow(dot(l1,p2),2)*Cvec[63] + 
  dot(l1,Omega2)*dot(l2,Omega1)*Cvec[64] + 
  dot(l1,Omega2)*dot(l1,p2)*dot(l2,Omega1)*Cvec[65] + 
  dot(l1,Omega2)*mypow(dot(l1,p2),2)*dot(l2,Omega1)*Cvec[66] + 
  dot(l1,Omega1)*dot(l1,Omega2)*Cvec[67] + 
  dot(l1,Omega1)*dot(l1,Omega2)*dot(l1,p2)*Cvec[68] + 
  mypow(dot(l1,Omega1),2)*dot(l1,Omega2)*Cvec[69]
;

end:

#}}}
