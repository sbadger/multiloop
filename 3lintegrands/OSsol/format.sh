#! /bin/sh

sed -e "s/\([0-9]*\)ideal(/eqs[\1]:={/g" \
    -e "s/)/};/g" $1 > $1.tmp
mv $1.tmp $1
