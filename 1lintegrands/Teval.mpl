interface(quiet=true);
with(spinors):

Digits:=30:
assign(declaremomn([seq(0,k=1..6)])):

P1:=p1+p2:
P2:=p3+p4:
P3:=p5+p6:

gg:=dot(P1,P2)-sqrt(dot(P1,P2)^2-S(P1)*S(P2));
gg:=dot(P1,P2)+sqrt(dot(P1,P2)^2-S(P1)*S(P2));

gg:=dot(P1,P2)+sign(dot(P1,P2))*sqrt(dot(P1,P2)^2-S(P1)*S(P2)):

k1f := INV(gg^2-S(P1)*S(P2))*( -S(P1)*P2+gg*P1 )*gg:
k2f := INV(gg^2-S(P1)*S(P2))*( -S(P2)*P1+gg*P2 )*gg:

zero=dot(k1f,k1f);
zero=dot(k2f,k2f);
zero=2*dot(k1f,k2f)-gg;

a11:=dot(P1,P1)*(gg+dot(P2,P2))/(gg^2-dot(P1,P1)*dot(P2,P2));
a21:=-dot(P2,P2)*(gg+dot(P1,P1))/(gg^2-dot(P1,P1)*dot(P2,P2));

massless=a11*a21;

