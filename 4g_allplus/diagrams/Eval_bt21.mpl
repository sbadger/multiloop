interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 3 do:
  for j from i+1 to 4 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.30e:\n",i,j,s||i||j):
  od:
od:

on_shell_bt21:=proc(p, tau1, tau2, tau3, tau4, tau5, tau6) local k1,k2,av,a,bv,b,
s12,s13,s14,s23,s24,s34,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := eta(p[1],p[2])/2;
av[4] := eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := eta(p[1],p[2])/2;
bv[4] := eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);

a := [1, 0, tau1, tau2]:
b := [-(1+tau3), tau3, tau4, tau5]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*tau1*tau2;
mu22 := -(tau3*(1+tau3) + tau4*tau5)*s12;
mu12 := tau6;

return k1,k2,mu11,mu12,mu22;
end:

Ds:=Dsm2+2:

xxdiag1  := 1:
xxdiag2  :=-2:
xxdiag3  :=-2:
xxdiag4  :=-2:
xxdiag5  := 1:
xxdiag6  := 2:
xxdiag7  := 2:
xxdiag8  := 4:
xxdiag9  :=-1:
xxdiag10 :=-1/2:
xxdiag11 := 1:
xxdiag12 :=-2:
xxdiag13 := 2:
xxdiag14 := 2:
xxdiag15 :=-1:
xxdiag16 := 2:

xxdiag17 := 1;
xxdiag18 := 2;
xxdiag19 := -2;
xxdiag20 := -4;
xxdiag21 := -1;
xxdiag22 := -2;
xxdiag23 := -1;
xxdiag24 := 1/2;
xxdiag25 := 1;

xxsub1:=1:
xxsub2:=1:
xxsub3:=1:

read("tmp/bt21.input.mpl"):

for k1 from 0 to 3 do:
for k2 from 0 to 3 do:
for k3 from 0 to 2 do:
for k4 from 0 to 2 do:
for k5 from 0 to 2 do:
for k6 from 0 to 2 do:
ds(k1,k2,k3,k4,k5,k6):=0:
od:
od:
od:
od:
od:
od:

for th1 from 0 to 3 do;
for th2 from 0 to 3 do;
for th3 from 0 to 2 do;
for th4 from 0 to 2 do;
for th5 from 0 to 2 do;
for th6 from 0 to 2 do;
printf("%d%d%d%d%d%d\n",th1,th2,th3,th4,th5,th6);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/4);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/4);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/3);
t4 := 2.3*exp(2.*evalf(Pi)*I*th4/3);
t5 := 2.3*exp(2.*evalf(Pi)*I*th5/3);
t6 := 2.3*exp(2.*evalf(Pi)*I*th6/3);

k1,k2,mu11,mu12,mu22 := on_shell_bt21([p1,p2,p3,p4],t1,t2,t3,t4,t5,t6):

#print(expand(dot(k1,k1)-mu11));
#print(expand(dot(k2,k2)-mu22));
#print(expand(dot(k1,p1)));
#print(expand(dot(k1,p1+p2)-s12/2));
#print(expand(dot(k2,p3+p4)-s12/2));

val := INT_bt21(k1,k2,mu11,mu12,mu22);

#print(collect(val,Dsm2));
#quit;

for k1 from 0 to 3 do:
for k2 from 0 to 3 do:
for k3 from 0 to 2 do:
for k4 from 0 to 2 do:
for k5 from 0 to 2 do:
for k6 from 0 to 2 do:
  ds(k1,k2,k3,k4,k5,k6) := expand(ds(k1,k2,k3,k4,k5,k6) + evalf(val/t1^k1/t2^k2/t3^k3/t4^k4/t5^k5/(4.*4.*3.*3.*3.))):
od:
od:
od:
od:
od:
od:

od;
od;
od;
od;
od;
od;

read("../final/bt21.coeffvecs.mpl");

for k1 from 0 to 4 do:
for k2 from 0 to 4 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
for k6 from 0 to 3 do:
# tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
# tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
# tmp3 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
# tmp4 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
# tmp5 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
# tmp6 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
# if abs(tmp1)<1e-20 then; tmp1:=0: fi:
# if abs(tmp2)<1e-20 then; tmp2:=0: fi:
# if abs(tmp3)<1e-20 then; tmp3:=0: fi:
# if abs(tmp4)<1e-20 then; tmp4:=0: fi:
# if abs(tmp5)<1e-20 then; tmp5:=0: fi:
# if abs(tmp6)<1e-20 then; tmp6:=0: fi:
# printf("d_%d%d%d%d%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k1, k2, k3, k4, k5, k6, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint("d",k1,k2,k3,k4,k5,k6,expand(ds(k1,k2,k3,k4,k5,k6)));
od:
od:
od:
od:
od:
od:

for k from 1 to 127 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 tmp5 := Re(coeff(Dvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Dvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint("d",k,expand(Dvec[k]));
od:

read("../final/bt21.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(127):
Cvec := iMM.Dvec:

for k from 1 to 127 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 tmp5 := Re(coeff(Cvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Cvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint(Clbl[k],expand(Cvec[k]));
od:


