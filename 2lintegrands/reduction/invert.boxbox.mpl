interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

#with(spinors);
#assign(declaremomn([0,0,0,0])):

# solving planar double box

IS(p1,p2) := 1/S(p1,p2);

read("boxbox.reduceM.log"):

dvec:=Vector(38):
for k from 1 to 38 do:
dvec[k]:=Dvec[k]:
od:

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[33..38]));
Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dvec:

dvecR:=SubVector(dvecT,[1..32]):
Cvec:=Ieqs.dvecR;

fd:=fopen("final/boxbox.integrand.mpl",WRITE):

fprintf(fd,"Cvec:=Vector(32):\n"):
for k from 1 to 32 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,simplify(Cvec[k])):
od:

fprintf(fd,"Zvec:=Vector(6):\n"):
for k from 33 to 38 do:
fprintf(fd,"Zvec[%d]:=%A;\n",k-32,simplify(dvecT[k])):
od:

fclose(fd):
