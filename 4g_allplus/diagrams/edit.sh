#! /bin/sh

for f in *.input.mpl
do
  sed -i "s/Z\([0-9]*\)_=/Z[\1]:=/g" $f
  sed -i "s/Z\([0-9]*\)_/Z[\1]/g" $f
  sed -i "s/\([^:]\)=/\1:=/g" $f
done
