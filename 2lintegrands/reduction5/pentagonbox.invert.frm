* vim: set foldmethod=marker:
#-
S Omega;
S s,t,u,tau,tau1,...,tau4;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF KK,Idot,PP,IPP;

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,p,x,P,h,k,e,q,c;
S mS;
S k1f,k2f,k3f,k4f,k1,k2;
auto S mu;
S l1,...,l20;

auto S Basis;
CF Power,cc,DP,nc;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h
#include- ../../lib/KKtools.h

*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}

#procedure simplify5

id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id A(px?,p5)*B(p5,py?) = -A(px,p1)*B(p1,py)-A(px,p2)*B(p2,py)-A(px,p3)*B(p3,py)-A(px,p4)*B(p4,py);

AB S,A,B,IS,IA,IB;
.sort
collect NUM;
#call SortCollect
argument NUM;
topolynomial;
endargument;
.sort
factarg NUM;
chainout NUM;
.sort
argument NUM;
frompolynomial;
endargument;
#call SortCollect

id AB(px?,p3+p4,py?) = -AB(px,p1+p2+p5,py);
#call collectKK
#call simplifyKK

id NUM(xx?) = xx;
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
#call cancel

AB A,B,IS,IA,IB;
.sort
collect NUM;
#call SortCollect
argument NUM;
topolynomial;
endargument;
.sort
factarg NUM;
chainout NUM;
.sort
argument NUM;
frompolynomial;
endargument;
#call SortCollect

id NUM(B(p1,p4)*B(p3,p5)*AB(p4,p2,p3) - B(p1,p5)*B(p3,p4)*AB(p4,p1,p3) - B(p1,p5)*B(p3,p4)*AB(p4,p2,p3))
  = B(p1,p4)*B(p3,p5)*A(p4,p2)*B(p2,p3) + B(p1,p5)*B(p3,p4)*A(p4,p5)*B(p5,p3);
id NUM( - A(p1,p5)*A(p3,p4)*AB(p3,p1,p4) - A(p1,p5)*A(p3,p4)*AB(p3,p2,p4) + A(p2,p3)*A(p3,p5)*AB(p1,p4,p2))
  = A(p1,p5)*A(p3,p4)*A(p3,p5)*B(p5,p4) + A(p2,p3)*A(p3,p5)*A(p1,p4)*B(p4,p2);
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);

#call orderS
id S(px?,p4) = -S(px,p1)-S(px,p2)-S(px,p3)-S(px,p5);
id S(p4,px?) = -S(px,p1)-S(px,p2)-S(px,p3)-S(px,p5);
id S(p1?ps,p1?ps) = 0;
#call orderS
id S(p1,p3) = S(p1,p2,p3)-S(p1,p2)-S(p2,p3);
id S(p2,p5) = S(p1,p2,p5)-S(p1,p2)-S(p1,p5);
id S(p3,p5) = S(p1,p2)-S(p1,p2,p3)-S(p1,p2,p5);

#endprocedure

off stats;

#define topo "pentagonbox"
.global

#do i=1,8
G L'i' = l'i';
#enddo

id l1=k1;
id l2=k1-p1;
id l3=k1-p1-p2;
id l4=k1-p1-p2-p3;
id l5=-k2+p4+p5;
id l6=-k2+p5;
id l7=-k2;
id l8=-k2-k1;
.sort

.store
#do sol=1,4
G K1'sol' = k1'sol';
G K2'sol' = k2'sol';
G MU11'sol' = mu11'sol';
G MU12'sol' = mu12'sol';
G MU22'sol' = mu22'sol';
#enddo

id k11 = 
  +p1
  +A(p2,p3)*IA(p1,p3)*eta(p1,p2)/2
;
id k21 = 
  +p5
  -A(p5,p1)*IA(p4,p1)*eta(p4,p5)/2
;
id k12 = 
  +p1
  +B(p2,p3)*IB(p1,p3)*eta(p2,p1)/2
;
id k22 = 
  +p5
  -B(p5,p1)*IB(p4,p1)*eta(p5,p4)/2
;
id k13 = 
  +p1
  +B(p2,p3)*IB(p1,p3)*eta(p2,p1)/2
;
id k23 = 
  +p5
  -B(p4,p3)*IB(p3,p5)*eta(p4,p5)/2
;
id k14 = 
  +p1
  +A(p2,p3)*IA(p1,p3)*eta(p1,p2)/2
;
id k24 = 
  +p5
  -A(p4,p3)*IA(p3,p5)*eta(p5,p4)/2
;

.store

#do sol=1,4

L zero1 = DP(L1,L1);
L zero2 = DP(L2,L2);
L zero3 = DP(L3,L3);
L zero4 = DP(L4,L4);
L zero5 = DP(L5,L5);
L zero6 = DP(L6,L6);
L zero7 = DP(L7,L7);
L zero8 = DP(L8,L8);

multiply replace_(k1,K1'sol');
multiply replace_(k2,K2'sol');
.sort

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,5
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
#call lprod

endrepeat;

id NUM(x?) = x;
#call sortINV
#call cancel
#call simplify5
#call cancel
#call simplify5

format 150;
print+s;
.store
#enddo

G Delta = Basis'topo';
#include- 'topo'.basis.h
id Power(x1?,x2?) = x1^x2;
id nc(x?)*cc(?nn) = nc(x,?nn);
.store

#do sol=1,4

G Delta'sol' = Delta;

multiply replace_(k1,K1'sol');
multiply replace_(k2,K2'sol');
.sort

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,5
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
#call lprod

endrepeat;

id NUM(x?) = x;
#call sortINV
#call cancel
#call simplify5
#call cancel
#call simplify5

B tau1,...,tau4;
print+s;
.store

#enddo

#write <final/'topo'.coeffvecs.mpl> "Cvec:=Vector(4):" 
#do cc=1,4
L cveccount = Delta;
id nc('cc',?x$clbl) = 1;
id nc(?x)=0;
.sort
#write <final/'topo'.coeffvecs.mpl> "Clbl['cc']:=cs['$clbl']:"
.store
#enddo

#write <'topo'.reduceM.log> "ReduceMatrix:=Matrix(4,4):"
#write <final/'topo'.coeffvecs.mpl> "Dvec:=Vector(4):" 
#define dd "0"

#do sol=1,4

#$nonzero=0;
L tmpRow = Delta'sol';

.sort

$nonzero=$nonzero+1;

.sort
hide;

#write "'sol','$nonzero'" 

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "Dvec['dd']:=ds['sol']:" 

#do cc=1,4

L tmpEl'cc' = tmpRow;
id nc('cc',?x$clbl) = 1;
id nc(?x)=0;
.sort

#write <'topo'.reduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
.sort
hide;

#enddo

#endif

unhide;
.store

#enddo

.end
