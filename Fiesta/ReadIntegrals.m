(* ::Package:: *)

<< "5pt_fiesta_v4.mx"
pmvars = Names["Global`*"];

pmvars = Complement[pmvars,{"Datapath", "Dsm2", "ep", "intdb220LList", "intdb220M1List", "intdb220M2List", "intdb320List", "intdb320TList", "M2path", "pmvars"}];

replacepm = Table[ToExpression[pmvars[[k]]]->xxx,{k,1,Length[pmvars]}];
removepm = Table[ToExpression[pmvars[[k]]]->0,{k,1,Length[pmvars]}];

Sq[x_]:=x^2;
Add[x_,y_,z___]:=Add[x+y,z];
Add[x_] := Sqrt[x];

GetError[expr_]:=Module[{err,epm2,epm1,ep0},
ep0  = Coefficient[expr/ep ,1/ep];
epm1 = Coefficient[expr,1/ep];
epm2 = Coefficient[expr,1/ep^2];
ep0  = Apply[List,ep0];
epm1 = Apply[List,epm1];
epm2 = Apply[List,epm2];
ep0  = ep0 /. replacepm;
ep0  = xxx*ep0;
ep0  = ep0 /. {xxx^2->1, xxx->0};
epm1 = epm1 /. replacepm;
epm1 = xxx*epm1;
epm1 = epm1 /. {xxx^2->1, xxx->0};
epm2 = epm2 /. replacepm;
epm2 = xxx*epm2;
epm2 = epm2 /. {xxx^2->1, xxx->0};
ep0  = Map[Sq,ep0];
ep0  = ep0/. {ep0[[0]]->Add};
epm1 = Map[Sq,epm1];
epm1 = epm1/. {epm1[[0]]->Add};
epm2 = Map[Sq,epm2];
epm2 = epm2/. {epm2[[0]]->Add};
{epm2,epm1,ep0}
]

GetValue[expr0_]:=Module[{expr,ep0,epm1,epm2},
expr = expr0/. removepm;
ep0  = Coefficient[expr/ep ,1/ep];
epm1 = Coefficient[expr,1/ep];
epm2 = Coefficient[expr,1/ep^2];
{epm2,epm1,ep0}
]

Dsm2 = 2;

Rdb320 = Table[{0,0},{i,1,5}];
Rdb320T = Table[{0,0},{i,1,5}];
Rdb220M1 = Table[{0,0},{i,1,5}];
Rdb220M2 = Table[{0,0},{i,1,5}];
Rdb2205L = Table[{0,0},{i,1,5}];

Do[
Rdb320[[k,1]] = GetValue[intdb320List[[k]]];
Rdb320[[k,2]] = GetError[intdb320List[[k]]];
Rdb320T[[k,1]] = GetValue[intdb320TList[[k]]];
Rdb320T[[k,2]] = GetError[intdb320TList[[k]]];
Rdb220M1[[k,1]] = GetValue[intdb220M1List[[k]]];
Rdb220M1[[k,2]] = GetError[intdb220M1List[[k]]];
Rdb220M2[[k,1]] = GetValue[intdb220M2List[[k]]];
Rdb220M2[[k,2]] = GetError[intdb220M2List[[k]]];
Rdb2205L[[k,1]] = GetValue[intdb220LList[[k]]];
Rdb2205L[[k,2]] = GetError[intdb220LList[[k]]];,
{k,1,5}]

WriteString[$Output,"\nIntegrals for ordering 123;45"]
WriteString[$Output, "db320   = ", Rdb320[[1]] // InputForm]

db320MB = {-0.2222222222222222, - 0.0628782029417349,-0.522437440229357};

Rdb320[[1,1,1]] - db320MB[[1]]
Rdb320[[1,1,2]] - db320MB[[2]]
Rdb320[[1,1,3]] - db320MB[[3]] - 2/9*N[Pi^2/6]

WriteString[$Output, "db320T  = ",Rdb320T[[1]] // InputForm]
WriteString[$Output, "db220M1 = ",Rdb220M1[[1]] // InputForm]
WriteString[$Output, "db220M2 = ",Rdb220M2[[1]] // InputForm]
WriteString[$Output, "db2205L = ",Rdb2205L[[1]] // InputForm]
WriteString[$Output,"\nIntegrals for ordering 234;51"]
WriteString[$Output, "db320   = ",Rdb320[[2]] // InputForm]
WriteString[$Output, "db320T  = ",Rdb320T[[2]] // InputForm]
WriteString[$Output, "db220M1 = ",Rdb220M1[[2]] // InputForm]
WriteString[$Output, "db220M2 = ",Rdb220M2[[2]] // InputForm]
WriteString[$Output, "db2205L = ",Rdb2205L[[2]] // InputForm]
WriteString[$Output,"\nIntegrals for ordering 345;12"]
WriteString[$Output, "db320   = ",Rdb320[[3]] // InputForm]
WriteString[$Output, "db320T  = ",Rdb320T[[3]] // InputForm]
WriteString[$Output, "db220M1 = ",Rdb220M1[[3]] // InputForm]
WriteString[$Output, "db220M2 = ",Rdb220M2[[3]] // InputForm]
WriteString[$Output, "db2205L = ",Rdb2205L[[3]] // InputForm]
WriteString[$Output,"\nIntegrals for ordering 451;23"]
WriteString[$Output, "db320   = ",Rdb320[[4]] // InputForm]
WriteString[$Output, "db320T  = ",Rdb320T[[4]] // InputForm]
WriteString[$Output, "db220M1 = ",Rdb220M1[[4]] // InputForm]
WriteString[$Output, "db220M2 = ",Rdb220M2[[4]] // InputForm]
WriteString[$Output, "db2205L = ",Rdb2205L[[4]] // InputForm]
WriteString[$Output,"\nIntegrals for ordering 512;34"]
WriteString[$Output, "db320   = ",Rdb320[[5]] // InputForm]
WriteString[$Output, "db320T  = ",Rdb320T[[5]] // InputForm]
WriteString[$Output, "db220M1 = ",Rdb220M1[[5]] // InputForm]
WriteString[$Output, "db220M2 = ",Rdb220M2[[5]] // InputForm]
WriteString[$Output, "db2205L = ",Rdb2205L[[5]] // InputForm]

kinpt = {
{ms12->1., ms23->3., ms34->11., ms45->19., ms15->31., s12->-1., s23->-3., s34->-11., s45->-19., s15->-31., tr5->-Sqrt[154429.]}
};

s[a_,b_] := Module[{},
If[b>a,ToExpression["s"<>ToString[a]<>ToString[b]],ToExpression["s"<>ToString[b]<>ToString[a]]]
]

A52test[ord_,p1_,p2_,p3_,p4_,p5_]  := \
       Idb2205L * ( \
          + s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]*tr5^-1 \
          ) + \
       Idb220M2 * ( \
          + 1/2*(tr5 - s[p3,p4]*s[p4,p5] + s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p5]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb220M1 * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] - s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] + s[p1,p2]*s[p1,p5])*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb320 * ( \
          - s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb320T * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p2]*s[p2,p3]*s[p4,p5]*tr5^-1 \
          );

A52test[1,1,2,3,4,5] /. kinpt[[1]]

A52[ord_,p1_,p2_,p3_,p4_,p5_]  := \
       Rdb2205L[[ord,1]] * ( \
          + s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]*tr5^-1 \
          ) + \
       Rdb220M2[[ord,1]] * ( \
          + 1/2*(tr5 - s[p3,p4]*s[p4,p5] + s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p5]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb220M1[[ord,1]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] - s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] + s[p1,p2]*s[p1,p5])*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb320[[ord,1]] * ( \
          - s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb320T[[ord,1]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p2]*s[p2,p3]*s[p4,p5]*tr5^-1 \
          );

A52err[ord_,p1_,p2_,p3_,p4_,p5_]  := Sqrt[\
       (Rdb2205L[[ord,2]] * ( \
          + s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]*tr5^-1 \
          ))^2 + \
       (Rdb220M2[[ord,2]] * ( \
          + 1/2*(tr5 - s[p3,p4]*s[p4,p5] + s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p5]*s[p4,p5]^2*tr5^-1 \
          ))^2 + \
       (Rdb220M1[[ord,2]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] - s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] + s[p1,p2]*s[p1,p5])*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ))^2 + \
       (Rdb320[[ord,2]] * ( \
          - s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ))^2 + \
       (Rdb320T[[ord,2]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p2]*s[p2,p3]*s[p4,p5]*tr5^-1 \
          ))^2];

A5P12345 = A52[1,1,2,3,4,5] /. kinpt[[1]];
A5P12345err = A52err[1,1,2,3,4,5] /. kinpt[[1]];
A5P23451 = A52[2,2,3,4,5,1] /. kinpt[[1]];
A5P23451err = A52err[2,2,3,4,5,1] /. kinpt[[1]];
A5P34512 = A52[3,3,4,5,1,2] /. kinpt[[1]];
A5P34512err = A52err[3,3,4,5,1,2] /. kinpt[[1]];
A5P45123 = A52[4,4,5,1,2,3] /. kinpt[[1]];
A5P45123err = A52err[4,4,5,1,2,3] /. kinpt[[1]];
A5P51234 = A52[5,5,1,2,3,4] /. kinpt[[1]];
A5P51234err = A52err[5,5,1,2,3,4] /. kinpt[[1]];

A5partial = A52[1,1,2,3,4,5]+A52[2,2,3,4,5,1]+A52[3,3,4,5,1,2]+A52[4,4,5,1,2,3]+A52[5,5,1,2,3,4] /. kinpt[[1]] // Expand
A5partialerr = Sqrt[A52err[1,1,2,3,4,5]^2+A52err[2,2,3,4,5,1]^2+A52err[3,3,4,5,1,2]^2+A52err[4,4,5,1,2,3]^2+A52err[5,5,1,2,3,4]^2] /. kinpt[[1]] // Expand

WriteString[$Output, "\n"]
WriteString[$Output, "A5P(1,2,3;4,5) = ", A5P12345, " +/- ", A5P12345err]
WriteString[$Output, "A5P(2,3,4;5,1) = ", A5P23451, " +/- ", A5P23451err]
WriteString[$Output, "A5P(3,4,5;1,2) = ", A5P34512, " +/- ", A5P34512err]
WriteString[$Output, "A5P(4,5,1;2,3) = ", A5P45123, " +/- ", A5P45123err]
WriteString[$Output, "A5P(5,1,2;3,4) = ", A5P51234, " +/- ", A5P51234err]
WriteString[$Output, "A52(1,2,3,4,5) = ", A5partial, " +/- ", A5partialerr]

A5Pbt[p1_,p2_,p3_,p4_,p5_] := \
(2*s[p4,p5]^4+7*s[p3,p4]*s[p4,p5]^3+2*s[p3,p4]^2*s[p4,p5]^2-3*s[p2,p3]*s[p4,p5]^3-11*s[p2,p3]*s[p3,p4]*s[p4,p5]^2-4*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+s[p2,p3]^2*s[p4,p5]^2+4*s[p2,p3]^2*s[p3,p4]*s[p4,p5]+2*s[p2,p3]^2*s[p3,p4]^2-s[p1,p5]*s[p4,p5]^3-2*s[p1,p5]*s[p3,p4]*s[p4,p5]^2+s[p1,p5]*s[p2,p3]*s[p4,p5]^2+2*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]+s[p1,p2]*s[p4,p5]^3-9*s[p1,p2]*s[p3,p4]*s[p4,p5]^2-2*s[p1,p2]*s[p3,p4]^2*s[p4,p5]+5*s[p1,p2]*s[p2,p3]*s[p4,p5]^2+14*s[p1,p2]*s[p2,p3]*s[p3,p4]*s[p4,p5]+3*s[p1,p2]*s[p2,p3]*s[p3,p4]^2-2*s[p1,p2]*s[p2,p3]^2*s[p4,p5]-2*s[p1,p2]*s[p2,p3]^2*s[p3,p4]+3*s[p1,p2]*s[p1,p5]*s[p4,p5]^2+2*s[p1,p2]*s[p1,p5]*s[p3,p4]*s[p4,p5]-s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p4,p5]+s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]-3*s[p1,p2]^2*s[p4,p5]^2+2*s[p1,p2]^2*s[p3,p4]*s[p4,p5]-4*s[p1,p2]^2*s[p2,p3]*s[p4,p5]-2*s[p1,p2]^2*s[p1,p5]*s[p4,p5]-s[p1,p2]^2*s[p1,p5]*s[p2,p3]-3*s[p1,p2]^3*s[p2,p3])*tr5*(-1/144*s[p1,p2]^-1*s[p1,p3]^-1*s[p3,p5]^-1*s[p4,p5]^-1)+
(2*s[p3,p4]*s[p4,p5]^5+7*s[p3,p4]^2*s[p4,p5]^4+2*s[p3,p4]^3*s[p4,p5]^3-5*s[p2,p3]*s[p3,p4]*s[p4,p5]^4-18*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^3-6*s[p2,p3]*s[p3,p4]^3*s[p4,p5]^2+4*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^3+15*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]^2+6*s[p2,p3]^2*s[p3,p4]^3*s[p4,p5]-s[p2,p3]^3*s[p3,p4]*s[p4,p5]^2-4*s[p2,p3]^3*s[p3,p4]^2*s[p4,p5]-2*s[p2,p3]^3*s[p3,p4]^3-2*s[p1,p5]*s[p4,p5]^5-8*s[p1,p5]*s[p3,p4]*s[p4,p5]^4-4*s[p1,p5]*s[p3,p4]^2*s[p4,p5]^3+3*s[p1,p5]*s[p2,p3]*s[p4,p5]^4+13*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^3+8*s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^2-s[p1,p5]*s[p2,p3]^2*s[p4,p5]^3-5*s[p1,p5]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^2-4*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]+s[p1,p5]^2*s[p4,p5]^4+2*s[p1,p5]^2*s[p3,p4]*s[p4,p5]^3-s[p1,p5]^2*s[p2,p3]*s[p4,p5]^3-2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+s[p1,p2]*s[p3,p4]*s[p4,p5]^4-9*s[p1,p2]*s[p3,p4]^2*s[p4,p5]^3-2*s[p1,p2]*s[p3,p4]^3*s[p4,p5]^2+2*s[p1,p2]*s[p2,p3]*s[p4,p5]^4+11*s[p1,p2]*s[p2,p3]*s[p3,p4]*s[p4,p5]^3+25*s[p1,p2]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^2+5*s[p1,p2]*s[p2,p3]*s[p3,p4]^3*s[p4,p5]-3*s[p1,p2]*s[p2,p3]^2*s[p4,p5]^3-18*s[p1,p2]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^2-20*s[p1,p2]*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]-3*s[p1,p2]*s[p2,p3]^2*s[p3,p4]^3+s[p1,p2]*s[p2,p3]^3*s[p4,p5]^2+6*s[p1,p2]*s[p2,p3]^3*s[p3,p4]*s[p4,p5]+4*s[p1,p2]*s[p2,p3]^3*s[p3,p4]^2+21*s[p1,p2]*s[p1,p5]*s[p4,p5]^4+39*s[p1,p2]*s[p1,p5]*s[p3,p4]*s[p4,p5]^3+6*s[p1,p2]*s[p1,p5]*s[p3,p4]^2*s[p4,p5]^2-15*s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p4,p5]^3-31*s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2-2*s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]-3*s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2-s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]-3*s[p1,p2]^2*s[p3,p4]*s[p4,p5]^3+2*s[p1,p2]^2*s[p3,p4]^2*s[p4,p5]^2+s[p1,p2]^2*s[p2,p3]*s[p4,p5]^3-10*s[p1,p2]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]^2-4*s[p1,p2]^2*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+5*s[p1,p2]^2*s[p2,p3]^2*s[p4,p5]^2+18*s[p1,p2]^2*s[p2,p3]^2*s[p3,p4]*s[p4,p5]+3*s[p1,p2]^2*s[p2,p3]^2*s[p3,p4]^2-2*s[p1,p2]^2*s[p2,p3]^3*s[p4,p5]-2*s[p1,p2]^2*s[p2,p3]^3*s[p3,p4]-36*s[p1,p2]^2*s[p1,p5]*s[p4,p5]^3-33*s[p1,p2]^2*s[p1,p5]*s[p3,p4]*s[p4,p5]^2-2*s[p1,p2]^2*s[p1,p5]*s[p3,p4]^2*s[p4,p5]+10*s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p4,p5]^2-3*s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]-3*s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p3,p4]^2+s[p1,p2]^2*s[p1,p5]*s[p2,p3]^2*s[p4,p5]+4*s[p1,p2]^2*s[p1,p5]*s[p2,p3]^2*s[p3,p4]-3*s[p1,p2]^2*s[p1,p5]^2*s[p4,p5]^2-2*s[p1,p2]^2*s[p1,p5]^2*s[p3,p4]*s[p4,p5]-s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]-3*s[p1,p2]^3*s[p2,p3]*s[p4,p5]^2-s[p1,p2]^3*s[p2,p3]*s[p3,p4]*s[p4,p5]-4*s[p1,p2]^3*s[p2,p3]^2*s[p4,p5]+3*s[p1,p2]^3*s[p2,p3]^2*s[p3,p4]+17*s[p1,p2]^3*s[p1,p5]*s[p4,p5]^2+2*s[p1,p2]^3*s[p1,p5]*s[p3,p4]*s[p4,p5]-s[p1,p2]^3*s[p1,p5]*s[p2,p3]*s[p4,p5]-s[p1,p2]^3*s[p1,p5]*s[p2,p3]^2+2*s[p1,p2]^3*s[p1,p5]^2*s[p4,p5]+s[p1,p2]^3*s[p1,p5]^2*s[p2,p3]-3*s[p1,p2]^4*s[p2,p3]^2+3*s[p1,p2]^4*s[p1,p5]*s[p2,p3])*(-1/144*s[p1,p2]^-1*s[p1,p3]^-1*s[p3,p5]^-1*s[p4,p5]^-1);

A52bt[p1_,p2_,p3_,p4_,p5_] := \
(s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]-s[p1,p5]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]-s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2-s[p1,p5]^2*s[p2,p3]*s[p4,p5]^2-s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]-s[p1,p2]*s[p3,p4]^2*s[p4,p5]^2-s[p1,p2]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+s[p1,p2]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+s[p1,p2]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]+s[p1,p2]*s[p1,p5]*s[p3,p4]*s[p4,p5]^2-s[p1,p2]*s[p1,p5]*s[p3,p4]^2*s[p4,p5]-s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p4,p5]^2-s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]^2-s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p4,p5]+s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]+s[p1,p2]*s[p1,p5]^2*s[p3,p4]*s[p4,p5]+s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p4,p5]-s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p3,p4]-s[p1,p2]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]-s[p1,p2]^2*s[p2,p3]^2*s[p4,p5]-s[p1,p2]^2*s[p1,p5]*s[p3,p4]*s[p4,p5]+s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p4,p5]+s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p3,p4]-s[p1,p2]^2*s[p1,p5]^2*s[p3,p4])*tr5*(+1/72*s[p1,p2]^-1*s[p1,p5]^-1*s[p2,p3]^-1*s[p3,p4]^-1*s[p4,p5]^-1)+
(s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^3+s[p1,p5]*s[p2,p3]*s[p3,p4]^3*s[p4,p5]^2-2*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]^2-2*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^3*s[p4,p5]+s[p1,p5]*s[p2,p3]^3*s[p3,p4]^2*s[p4,p5]+s[p1,p5]*s[p2,p3]^3*s[p3,p4]^3-2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]^3-2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^2+2*s[p1,p5]^2*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^2+2*s[p1,p5]^2*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]+s[p1,p5]^3*s[p2,p3]*s[p4,p5]^3+s[p1,p5]^3*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+s[p1,p2]*s[p3,p4]^3*s[p4,p5]^3+s[p1,p2]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^3-2*s[p1,p2]*s[p2,p3]*s[p3,p4]^3*s[p4,p5]^2-2*s[p1,p2]*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]^2+s[p1,p2]*s[p2,p3]^2*s[p3,p4]^3*s[p4,p5]+s[p1,p2]*s[p2,p3]^3*s[p3,p4]^2*s[p4,p5]-2*s[p1,p2]*s[p1,p5]*s[p3,p4]^2*s[p4,p5]^3+s[p1,p2]*s[p1,p5]*s[p3,p4]^3*s[p4,p5]^2+12*s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^2+2*s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^2+12*s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]+s[p1,p2]*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^3-2*s[p1,p2]*s[p1,p5]*s[p2,p3]^3*s[p3,p4]^2+s[p1,p2]*s[p1,p5]^2*s[p3,p4]*s[p4,p5]^3-2*s[p1,p2]*s[p1,p5]^2*s[p3,p4]^2*s[p4,p5]^2+s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p4,p5]^3+12*s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+2*s[p1,p2]*s[p1,p5]^2*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+2*s[p1,p2]*s[p1,p5]^2*s[p2,p3]^2*s[p4,p5]^2+2*s[p1,p2]*s[p1,p5]^2*s[p2,p3]^2*s[p3,p4]*s[p4,p5]+2*s[p1,p2]*s[p1,p5]^2*s[p2,p3]^2*s[p3,p4]^2+s[p1,p2]*s[p1,p5]^3*s[p3,p4]*s[p4,p5]^2-2*s[p1,p2]*s[p1,p5]^3*s[p2,p3]*s[p4,p5]^2+2*s[p1,p2]^2*s[p2,p3]*s[p3,p4]^2*s[p4,p5]^2+2*s[p1,p2]^2*s[p2,p3]^2*s[p3,p4]*s[p4,p5]^2-2*s[p1,p2]^2*s[p2,p3]^2*s[p3,p4]^2*s[p4,p5]-2*s[p1,p2]^2*s[p2,p3]^3*s[p3,p4]*s[p4,p5]+2*s[p1,p2]^2*s[p1,p5]*s[p3,p4]^2*s[p4,p5]^2+2*s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2+2*s[p1,p2]^2*s[p1,p5]*s[p2,p3]*s[p3,p4]^2*s[p4,p5]+2*s[p1,p2]^2*s[p1,p5]*s[p2,p3]^2*s[p4,p5]^2+12*s[p1,p2]^2*s[p1,p5]*s[p2,p3]^2*s[p3,p4]*s[p4,p5]-2*s[p1,p2]^2*s[p1,p5]*s[p2,p3]^2*s[p3,p4]^2+s[p1,p2]^2*s[p1,p5]*s[p2,p3]^3*s[p4,p5]+s[p1,p2]^2*s[p1,p5]*s[p2,p3]^3*s[p3,p4]-2*s[p1,p2]^2*s[p1,p5]^2*s[p3,p4]*s[p4,p5]^2+2*s[p1,p2]^2*s[p1,p5]^2*s[p3,p4]^2*s[p4,p5]-2*s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]*s[p4,p5]^2+12*s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]*s[p4,p5]+2*s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]*s[p3,p4]^2-2*s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]^2*s[p4,p5]-2*s[p1,p2]^2*s[p1,p5]^2*s[p2,p3]^2*s[p3,p4]-2*s[p1,p2]^2*s[p1,p5]^3*s[p3,p4]*s[p4,p5]+s[p1,p2]^2*s[p1,p5]^3*s[p2,p3]*s[p4,p5]+s[p1,p2]^2*s[p1,p5]^3*s[p2,p3]*s[p3,p4]+s[p1,p2]^3*s[p2,p3]^2*s[p3,p4]*s[p4,p5]+s[p1,p2]^3*s[p2,p3]^3*s[p4,p5]-2*s[p1,p2]^3*s[p1,p5]*s[p2,p3]^2*s[p4,p5]+s[p1,p2]^3*s[p1,p5]*s[p2,p3]^2*s[p3,p4]+s[p1,p2]^3*s[p1,p5]^2*s[p3,p4]*s[p4,p5]+s[p1,p2]^3*s[p1,p5]^2*s[p2,p3]*s[p4,p5]-2*s[p1,p2]^3*s[p1,p5]^2*s[p2,p3]*s[p3,p4]+s[p1,p2]^3*s[p1,p5]^3*s[p3,p4])*(+1/72*s[p1,p2]^-1*s[p1,p5]^-1*s[p2,p3]^-1*s[p3,p4]^-1*s[p4,p5]^-1);

check = 0;
bt1 = 4*A5Pbt[1,2,3,4,5] /. {s13->s45-s12-s23, s35->s12-s34-s45} /. kinpt[[1]];
check =  check + %;
bt2 = 4*A5Pbt[2,3,4,5,1] /. {s14->s23-s15-s45, s24->s15-s23-s34} /. kinpt[[1]];
check =  check + %;
bt3 = 4*A5Pbt[3,4,5,1,2] /. {s25->s34-s15-s12, s35->s12-s34-s45} /. kinpt[[1]];
check =  check + %;
bt4 = 4*A5Pbt[4,5,1,2,3] /. {s13->s45-s12-s23, s14->s23-s15-s45, s24->s15-s23-s34} /. kinpt[[1]];
check =  check + %;
bt5 = 4*A5Pbt[5,1,2,3,4] /. {s25->s34-s15-s12, s14->s23-s15-s45, s24->s15-s23-s34} /. kinpt[[1]];
check =  check + %;

btpartial = 4*A52bt[1,2,3,4,5] /. kinpt[[1]];

WriteString[$Output, "\n"]
WriteString[$Output, "A5Pbt(1,2,3;4,5) = ", bt1]
WriteString[$Output, "A5Pbt(2,3,4;5,1) = ", bt2]
WriteString[$Output, "A5Pbt(3,4,5;1,2) = ", bt3]
WriteString[$Output, "A5Pbt(4,5,1;2,3) = ", bt4]
WriteString[$Output, "A5Pbt(5,1,2;3,4) = ", bt5]
WriteString[$Output, "A52bt(1,2,3,4,5) = ", btpartial]


WriteString[$Output, "\n"]
WriteString[$Output, "A5P(1,2,3;4,5) = ", A5P12345+{0,0,bt1}, " +/- ", A5P12345err];
WriteString[$Output, "A5P(2,3,4;5,1) = ", A5P23451+{0,0,bt2}, " +/- ", A5P23451err];
WriteString[$Output, "A5P(3,4,5;1,2) = ", A5P34512+{0,0,bt3}, " +/- ", A5P34512err];
WriteString[$Output, "A5P(4,5,1;2,3) = ", A5P45123+{0,0,bt4}, " +/- ", A5P45123err];
WriteString[$Output, "A5P(5,1,2;3,4) = ", A5P51234+{0,0,bt5}, " +/- ", A5P51234err];
WriteString[$Output, "A52(1,2,3,4,5) = ", A5partial+{0,0,btpartial}, " +/- ", A5partialerr];

WriteString[$Output, "\n"]
WriteString[$Output, "$A_5^{[P]}(1,2,3;4,5)$ & $",
NumberForm[(A5P12345+{0,0,bt1})[[1]],{6,2}], " \\pm ", NumberForm[A5P12345err[[1]],{1,2}], "$ & $",
NumberForm[(A5P12345+{0,0,bt1})[[2]],{6,2}], " \\pm ", NumberForm[A5P12345err[[2]],{1,2}], "$ & $",
NumberForm[(A5P12345+{0,0,bt1})[[3]],{6,2}], " \\pm ", NumberForm[A5P12345err[[3]],{1,2}], "$ \\\\\n"
];

WriteString[$Output, "$A_5^{[P]}(2,3,4;5,1)$ & $",
NumberForm[(A5P23451+{0,0,bt2})[[1]],{6,2}], " \\pm ", NumberForm[A5P23451err[[1]],{1,2}], "$ & $",
NumberForm[(A5P23451+{0,0,bt2})[[2]],{6,2}], " \\pm ", NumberForm[A5P23451err[[2]],{1,2}], "$ & $",
NumberForm[(A5P23451+{0,0,bt2})[[3]],{6,2}], " \\pm ", NumberForm[A5P23451err[[3]],{1,2}], "$ \\\\\n"
];

WriteString[$Output, "$A_5^{[P]}(3,4,5;1,2)$ & $",
NumberForm[(A5P34512+{0,0,bt3})[[1]],{6,2}], " \\pm ", NumberForm[A5P34512err[[1]],{1,2}], "$ & $",
NumberForm[(A5P34512+{0,0,bt3})[[2]],{6,2}], " \\pm ", NumberForm[A5P34512err[[2]],{1,2}], "$ & $",
NumberForm[(A5P34512+{0,0,bt3})[[3]],{6,2}], " \\pm ", NumberForm[A5P34512err[[3]],{1,2}], "$ \\\\\n"
];

WriteString[$Output, "$A_5^{[P]}(4,5,1;2,3)$ & $",
NumberForm[(A5P45123+{0,0,bt4})[[1]],{6,2}], " \\pm ", NumberForm[A5P45123err[[1]],{1,2}], "$ & $",
NumberForm[(A5P45123+{0,0,bt4})[[2]],{6,2}], " \\pm ", NumberForm[A5P45123err[[2]],{1,2}], "$ & $",
NumberForm[(A5P45123+{0,0,bt4})[[3]],{6,2}], " \\pm ", NumberForm[A5P45123err[[3]],{1,2}], "$ \\\\\n"
];

WriteString[$Output, "$A_5^{[P]}(5,1,2;3,4)$ & $",
NumberForm[(A5P51234+{0,0,bt5})[[1]],{6,2}], " \\pm ", NumberForm[A5P51234err[[1]],{1,2}], "$ & $",
NumberForm[(A5P51234+{0,0,bt5})[[2]],{6,2}], " \\pm ", NumberForm[A5P51234err[[2]],{1,2}], "$ & $",
NumberForm[(A5P51234+{0,0,bt5})[[3]],{6,2}], " \\pm ", NumberForm[A5P51234err[[3]],{1,2}], "$ \\\\\n"
];

WriteString[$Output, "\n"]
WriteString[$Output, "$A_5^{(2),\\text{bare}}(1,2,3,4,5)$ & $",
NumberForm[(A5partial+{0,0,btpartial})[[1]],{6,2}], " \\pm ", NumberForm[A5partialerr[[1]],{1,2}], "$ & $",
NumberForm[(A5partial+{0,0,btpartial})[[2]],{6,2}], " \\pm ", NumberForm[A5partialerr[[2]],{1,2}], "$ & $",
NumberForm[(A5partial+{0,0,btpartial})[[3]],{6,2}], " \\pm ", NumberForm[A5partialerr[[3]],{1,2}], "$ \\\\\n"
];

