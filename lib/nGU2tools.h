* vim: set foldmethod=marker:

CF PentBox,BoxPent;
CF BoxBox,TriPent,PentTri,BoxBoxX;
CF TriBox,BoxTri,TriTriB;

S BFLAV3,BFLAV4,BFLAV6,BFLAV1;

*{{{topologise2
#procedure topologise2

*{{{ general double box
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
id BoxBox(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),Kmom(?x6),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),
    ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4),ptl(-l7,-'i7',-xptl7))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l1,'i1',xptl1),ptl(l7,'i7',xptl7))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort
*{{{ general crossed double box
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
id BoxBoxX(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),Kmom(?x6),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),
    ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3),ptl(-l7,-'i7',-xptl7))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l1,'i1',xptl1),ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l7,'i7',xptl7))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort
*{{{ general penta-box / TriPent
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
id TriPent(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),Kmom(?x6),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),
    ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3),ptl(-l7,-'i7',-xptl7))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l1,'i1',xptl1),ptl(l7,'i7',xptl7))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort
*{{{ general planar box-pentagon
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
#do i8={1,-1}

id BoxPent(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),Kmom(?x6),Kmom(?x7),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),
    ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?),ptl(l8,'i8',xptl8?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4),ptl(-l8,-'i8',-xptl8))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l7,'i7',xptl7))
	  * AMP(ptl(-l7,-'i7',-xptl7),?x7,ptl(l1,'i1',xptl1),ptl(l8,'i8',xptl8))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)*SIGN(xptl8)
	;

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort
*{{{ general planar pentagon-box
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
#do i7={1,-1}
#do i8={1,-1}

id PentBox(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),Kmom(?x6),Kmom(?x7),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),
    ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?),ptl(l7,'i7',xptl7?),ptl(l8,'i8',xptl8?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5),ptl(-l8,-'i8',-xptl8))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))
	  * AMP(ptl(-l6,-'i6',-xptl6),?x6,ptl(l7,'i7',xptl7))
	  * AMP(ptl(-l7,-'i7',-xptl7),?x7,ptl(l1,'i1',xptl1),ptl(l8,'i8',xptl8))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)*SIGN(xptl7)*SIGN(xptl8)
	;

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo

*}}}
.sort
*{{{ box-tri
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
id BoxTri(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),
    ptl(l4,'i4',xptl4?),ptl(l5,'i5',xptl5?),
    ptl(l6,'i6',xptl6?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4),ptl(-l6,-'i6',-xptl6))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l1,'i1',xptl1),ptl(l6,'i6',xptl6))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
*}}}
.sort
*{{{ tri-box
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
id TriBox(
    Kmom(?x1),Kmom(?x2),Kmom(?x3),Kmom(?x4),Kmom(?x5),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),
    ptl(l3,'i3',xptl3?),ptl(l4,'i4',xptl4?),ptl(l5,'i5',xptl5?),
    ptl(l6,'i6',xptl6?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3),ptl(-l6,-'i6',-xptl6))
	  * AMP(ptl(-l3,-'i3',-xptl3),?x3,ptl(l4,'i4',xptl4))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l1,'i1',xptl1),ptl(l6,'i6',xptl6))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)
	*SIGN(xptl5)*SIGN(xptl6)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
*}}}
.sort
*{{{ tri-tri (bow-tie)
#do i1={1,-1}
#do i2={1,-1}
#do i3={1,-1}
#do i4={1,-1}
#do i5={1,-1}
#do i6={1,-1}
id TriTriB(
    Kmom(?x1),Kmom(?x2),Kmom(?x3,0,?x6),Kmom(?x4),Kmom(?x5),
    ptl(l1,'i1',xptl1?),ptl(l2,'i2',xptl2?),ptl(l3,'i3',xptl3?),
    ptl(l4,'i4',xptl4?),ptl(l5,'i5',xptl5?),ptl(l6,'i6',xptl6?))

  	= AMP(ptl(-l1,-'i1',-xptl1),?x1,ptl(l2,'i2',xptl2))
	  * AMP(ptl(-l2,-'i2',-xptl2),?x2,ptl(l3,'i3',xptl3))
	  * AMP(ptl(-l3,-'i3',-BFLAV3),?x3,ptl(l4,'i4',BFLAV4),ptl(-l6,-'i6',-BFLAV6),?x6,ptl(l1,'i1',BFLAV1))
	  * AMP(ptl(-l4,-'i4',-xptl4),?x4,ptl(l5,'i5',xptl5))
	  * AMP(ptl(-l5,-'i5',-xptl5),?x5,ptl(l6,'i6',xptl6))

	*SIGN(xptl1)*SIGN(xptl2)*SIGN(xptl3)*SIGN(xptl4)*SIGN(xptl5)*SIGN(xptl6)
	;
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
*}}}

*** factors of I for fermion propagators ***
*** still need to fix signs in trees and correctly assign propagators ***
id SIGN(0)    = 1;
id SIGN(1/2)  = -I;
id SIGN(-1/2) = I;
id SIGN(1)    = -1;
id SIGN(-1)   = -1;

id AMP(?x1,,?x2) = AMP(?x1,?x2);
id AMP(?x1,0,?x2) = AMP(?x1,?x2);

#endprocedure
*}}}
