(* ::Package:: *)

NumberOfLinks=2;
STRATEGY=STRATEGY_S;
Get["/lscr221/badger/src/FIESTA2.5/FIESTA_2.5.0.m"];
NumberOfSubkernels=1;
CIntegratePath="/lscr221/badger/src/FIESTA2.5/CIntegrateMP";
UsingQLink=False;

CurrentIntegratorSettings={{"epsrel","1.000000E-14"},{"epsabs","1.000000E-16"},{"mineval","0"},{"maxeval","2000000"},{"nstart","1500"},{"nincrease","1500"}};

kList={k1};
mod[x_, n_] := If[x <= n, x, x - n];
sList = {s12, s23, s34, s45, s15};
numbers={-1, -3, -11, -19, -31};
cyclic = Table[
  sList[[j]] -> numbers[[mod[j + i, 5]]], {i, 0,
   4}, {j, 1, 5}];

kinematics={p1^2->0,p2^2->0,p3^2->0,p5^2->0,
p1*p2->s12/2,
p1*p3->(-s12-s23+s45)/2,
p1*p5->s15/2,
p2*p3->s23/2,
p2*p5->(-s12-s15+s34)/2,
p3*p5->(s12-s34-s45)/2
};
nkinematics={s12->-1.,s23->-3.,s34->-11.,s45->-19.,s15->-31.};


penta[ncondition1_]:=Module[{ncondition=ncondition1,result},
proList={-k1^2,-(k1-p1)^2,-(k1-p1-p2)^2,-(k1-p1-p2-p3)^2,-(k1+p5)^2};
powerList={1,1,1,1,1};
d0=10;
result=SDEvaluate[
UF[kList,proList,Join[kinematics,ncondition]],
powerList,1];
Return[result];
];


box[ncondition1_]:=Module[{ncondition=ncondition1,result},
proList={-k1^2,-(k1-p1)^2,-(k1-p1-p2)^2,-(k1-p1-p2-p3)^2};
powerList={1,1,1,1};
d0=8;
result=SDEvaluate[
UF[kList,proList,Join[kinematics,ncondition]],
powerList,1];
Return[result];
];

A51=Dsm2/2(ep*(-1+ep)*(s12*s23*box[cyclic[[1]]] + s23*s34*box[cyclic[[2]]] + s34*s45*box[cyclic[[3]]]+ s45*s15*box[cyclic[[4]]] + s15*s12*box[cyclic[[5]]])-ep*(-1+ep)*(-2+ep)*2*Tr5*penta[cyclic[[1]]]);
A51=A51/.cyclic[[1]];


A51 // Expand


pmvars = Names["Global`*"];
pmvars = Complement[pmvars,{"Datapath", "Dsm2", "ep", "pmvars", "A51"}]

replacepm = Table[ToExpression[pmvars[[k]]]->xxx,{k,1,Length[pmvars]}];
removepm = Table[ToExpression[pmvars[[k]]]->0,{k,1,Length[pmvars]}];

Sq[x_]:=x^2;
Add[x_,y_,z___]:=Add[x+y,z];
Add[x_] := Sqrt[x];

GetError2[expr_]:=Module[{err,ep2,ep1,ep0},
ep0 = Coefficient[expr/ep ,1/ep];
ep1 = Coefficient[expr,ep];
ep2 = Coefficient[expr,ep^2];
ep0 = Apply[List,ep0];
ep1 = Apply[List,ep1];
ep2 = Apply[List,ep2];
ep0 = ep0 /. replacepm;
ep0 = xxx*ep0;
ep0 = ep0 /. {xxx^2->1, xxx->0};
ep1 = ep1 /. replacepm;
ep1 = xxx*ep1;
ep1 = ep1 /. {xxx^2->1, xxx->0};
ep2 = ep2 /. replacepm;
ep2 = xxx*ep2;
ep2 = ep2 /. {xxx^2->1, xxx->0};
ep0 = Map[Sq,ep0];
ep0 = ep0/. {ep0[[0]]->Add};
ep1 = Map[Sq,ep1];
ep1 = ep1/. {ep1[[0]]->Add};
ep2 = Map[Sq,ep2];
ep2 = ep2/. {ep2[[0]]->Add};
{ep0,ep1,ep2}
]

GetValue2[expr0_]:=Module[{expr,ep0,ep1,ep2},
expr = expr0/. removepm;
ep0  = Coefficient[expr/ep ,1/ep];
ep1 = Coefficient[expr,ep];
ep2 = Coefficient[expr,ep^2];
{ep0,ep1,ep2}
]

GetError[expr_]:=Module[{err,ep2,ep1,ep0},
ep0 = Coefficient[expr/ep ,1/ep];
ep1 = Coefficient[expr,1/ep];
ep2 = Coefficient[expr,1/ep^2];
ep0 = Apply[List,ep0];
ep1 = Apply[List,ep1];
ep2 = Apply[List,ep2];
ep0 = ep0 /. replacepm;
ep0 = xxx*ep0;
ep0 = ep0 /. {xxx^2->1, xxx->0};
ep1 = ep1 /. replacepm;
ep1 = xxx*ep1;
ep1 = ep1 /. {xxx^2->1, xxx->0};
ep2 = ep2 /. replacepm;
ep2 = xxx*ep2;
ep2 = ep2 /. {xxx^2->1, xxx->0};
ep0 = Map[Sq,ep0];
ep0 = ep0/. {ep0[[0]]->Add};
ep1 = Map[Sq,ep1];
ep1 = ep1/. {ep1[[0]]->Add};
ep2 = Map[Sq,ep2];
ep2 = ep2/. {ep2[[0]]->Add};
{ep2,ep1,ep0}
]

GetValue[expr0_]:=Module[{expr,ep0,ep1,ep2},
expr = expr0/. removepm;
ep0  = Coefficient[expr/ep ,1/ep];
ep1 = Coefficient[expr,1/ep];
ep2 = Coefficient[expr,1/ep^2];
{ep2,ep1,ep0}
]

Dsm2 = 2;

Truncate[x1_]:=Module[{x=x1,varList,replacement},
  varList=Variables[x];
  varList=Complement[varList,{Dsm2,ep,Tr5,xx1,xx2,xx3,xx4,xx5}];
  replacement=Table[varList[[i]]->0,{i,1,Length[varList]}];
  Return[x/.replacement//Expand];
];


A51 = A51 /. Tr5->-Sqrt[154429.] /. Dsm2->2;
A51//Truncate
Series[%,{ep,0,2}]


GetError2[A51] // InputForm

IR = Series[-1/ep^2*(Exp[-ep*Log[-s12]] + Exp[-ep*Log[-s23]] + Exp[-ep*Log[-s34]] + Exp[-ep*Log[-s45]] + Exp[-ep*Log[-s15]]) /. nkinematics ,{ep,0,0}]


GetValue2[A51] // InputForm
GetError2[A51] // InputForm

ExpandAll[IR*A51];

IRcheck    = GetValue[ExpandAll[IR*A51]];
IRcheckerr = GetError[ExpandAll[IR*A51]];

WriteString[$Output, "\n"]
WriteString[$Output, "$A_5^{(1), \\text{bare}}(1,2,3,4,5)$ & $",
NumberForm[IRcheck[[1]],{6,2}], " \\pm ", NumberForm[IRcheckerr[[1]],{1,2}], "$ & $",
NumberForm[IRcheck[[2]],{6,2}], " \\pm ", NumberForm[IRcheckerr[[2]],{1,2}], "$ & $",
NumberForm[IRcheck[[3]],{6,2}], " \\pm ", NumberForm[IRcheckerr[[3]],{1,2}], "$ \\\\\n"
];

