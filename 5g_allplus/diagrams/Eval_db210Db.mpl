interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.30e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_db210b:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [tau3, tau4, tau5, (tau3-tau4-tau5)-s12/s23*tau4-s45/s23*(1+tau3)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := s12/s13*(s23*tau5+s45)*tau5 + s12*tau3*tau4 - (s23-s45)*s12/s13*tau3*tau5 + (s23+s12)*s12/s13*tau4*tau5;
mu12 := s12*tau4 + s12*s45/s13*tau1 + s12*s23/s13*tau5*(tau1-tau2) - (s23-s45)*s12/s13*tau1*tau3 + (s23+s12)*s12/s13*tau1*tau4;

return k1,k2,mu11,mu12,mu22;
end:

Ds:=Dsm2+2:
xxdiag1:=1:
xxdiag2:=2:
xxdiag3:=-2:
xxdiag4:=2:

xxdiag45:=1:
xxdiag46:=2:
xxdiag47:=2:
xxdiag48:=-2:
xxdiag49:=-1:
xxdiag50:=-2:

xxdiag60 :=  1:
xxdiag61 := -2:
xxdiag62 := -2:
xxdiag63 := -2:
xxdiag64 := -1:
xxdiag65 :=  2:
xxdiag66 := -1:
xxdiag67 :=  1:

read("tmp/db210Db.input.mpl"):
#INT_db210Db := (k1,k2,mu11,mu12,mu22) -> (1+4.*dot(k2,p1))*(mu11*mu22+mu12^2):

for k1 from 0 to 4 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
ds(k1,k2,k3,k4,k5):=0:
od:
od:
od:
od:
od:

for th1 from 0 to 4 do;
for th2 from 0 to 3 do;
for th3 from 0 to 3 do;
for th4 from 0 to 3 do;
for th5 from 0 to 3 do;
printf("%d%d%d%d%d\n",th1,th2,th3,th4,th5);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/5);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/4);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/4);
t4 := 2.3*exp(2.*evalf(Pi)*I*th4/4);
t5 := 2.3*exp(2.*evalf(Pi)*I*th5/4);

k1,k2,mu11,mu12,mu22 := on_shell_db210b([p1,p2,p3,p4,p5],t1,t2,t3,t4,t5):

print(dot(k1,k1) = mu11);
print(dot(k2,k2) = mu22);
print(dot(k1,k2) = mu12/2);

print(dot(k1,p1) = 0);
print(dot(k1,p2) = s12/2);
print(dot(k1,p1+p2) = s12/2);
print(dot(k2,p4+p5) = s45/2);

val := INT_db210Db(k1,k2,mu11,mu12,mu22);

print(collect(val,Dsm2));
quit;

for k1 from 0 to 4 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
  ds(k1,k2,k3,k4,k5) := expand(ds(k1,k2,k3,k4,k5) + evalf(val/t1^k1/t2^k2/t3^k3/t4^k4/t5^k5/(5.*4.*4.*4.*4.))):
od:
od:
od:
od:
od:

od;
od;
od;
od;
od;

read("../final/db210Db.coeffvecs.mpl");

for k1 from 0 to 4 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
 tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
 tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
 tmp3 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
 tmp4 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d%d%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k1, k2, k3, k4, k5, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k1,k2,k3,k4,k5,expand(ds(k1,k2,k3,k4,k5)));
od:
od:
od:
od:
od:

for k from 1 to 180 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k,expand(Dvec[k]));
od:

read("../db210Db.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(180):
Cvec := iMM.Dvec:

for k from 1 to 180 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
# lprint(Clbl[k],expand(Cvec[k]));
od:


