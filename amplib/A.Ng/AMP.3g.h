id AMP(ptl(p1?,-1,0),ptl(p2?,-1,0),ptl(p3?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,0),ptl(p2?,-1,0),ptl(p3?,1,0)) =
	 A(p1,p2)^3*IA(p1,p3)*IA(p2,p3)*I;

id AMP(ptl(p1?,-1,0),ptl(p2?,1,0),ptl(p3?,-1,0)) =
	 A(p1,p3)^3*IA(p1,p2)*IA(p2,p3)*I;

id AMP(ptl(p1?,-1,0),ptl(p2?,1,0),ptl(p3?,1,0)) =
	  - B(p2,p3)^3*IB(p1,p2)*IB(p1,p3)*I;

id AMP(ptl(p1?,1,0),ptl(p2?,-1,0),ptl(p3?,-1,0)) =
	 A(p2,p3)^3*IA(p1,p2)*IA(p1,p3)*I;

id AMP(ptl(p1?,1,0),ptl(p2?,-1,0),ptl(p3?,1,0)) =
	  - B(p1,p3)^3*IB(p1,p2)*IB(p2,p3)*I;

id AMP(ptl(p1?,1,0),ptl(p2?,1,0),ptl(p3?,-1,0)) =
	  - B(p1,p2)^3*IB(p1,p3)*IB(p2,p3)*I;

id AMP(ptl(p1?,1,0),ptl(p2?,1,0),ptl(p3?,1,0)) =
	  0;

