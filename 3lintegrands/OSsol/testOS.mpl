# vim: set foldmethod=marker:
interface(quiet=true):
Digits:=30:
with(spinors):

assign(declaremomn([0,0,0,0])):

freevars:={tau1=0.6739,tau2=0.1123,tau3=0.3927}:

read(cat("boxboxbox.OSsol.log")):
#{{{
for k from 1 to 14 do:
printf("boxboxbox : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k1[k]-p2:
l[4]:=k2[k]:
l[5]:=k3[k]+p3:
l[6]:=k3[k]:
l[7]:=k3[k]-p4:
l[8]:=k2[k]+p1+p2:
l[9]:=k2[k]-k1[k]+p2:
l[10]:=k2[k]-k3[k]-p3:

for j from 1 to 10 do:
printf("l%d^2 = %.4Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4','c1','c2','c3','c4');
od:
#}}}

read(cat("tcourt.OSsol.log")):
#{{{
for k from 1 to 16 do:
assign(sol[k]):
printf("tcourt : solution %d\n",k):

l[1]:=k1[k]+p1:
l[2]:=k1[k]:
l[3]:=k1[k]-p2:
l[4]:=k2[k]-p3:
l[5]:=k2[k]:
l[6]:=k2[k]-k3[k]-p3:
l[7]:=k2[k]-k3[k]-p3-p4:
l[8]:=k2[k]-k3[k]-k1[k]+p2:
l[9]:=k2[k]-k1[k]+p2:
l[10]:=k3[k]:

for j from 1 to 10 do:
printf("l%d^2 = %.1Ze\n",j,subs(freevars,expand(dot(l[j],l[j]))));
od:
printf("\n");

unassign('a1','a2','a3','a4','b1','b2','b3','b4','c1','c2','c3','c4');
od:
#}}}

