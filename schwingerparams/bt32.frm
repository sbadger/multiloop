#-
#include- schwinger.prc
off stats;
.global

G P1 = k1;
G P2 = k1-p1;
G P3 = k1-p12;
G P4 = k1-p123;
G P5 = k2;
G P6 = k2-p5;
G P7 = k2+p45;

.store

G QQ =
#do i=1,7
+x'i'*P'i'^2
#enddo
;

id p1^2=0;
id p12^2=s12;
id p123^2=s45;
id p45^2=s45;
id p4^2=0;

#$a=0;
#$b=0;
#$c=0;
#$d=0;
#$e=0;
#$f=0;

B k1,k2;
.sort
collect NUM;
id k1^2*NUM(xx?$a) = k1^2*a;
id k2^2*NUM(xx?$b) = k2^2*b;
id k1*k2*NUM(xx?$c) = k1*k2*c;
id k1*NUM(xx?$d) = k1*d;
id k2*NUM(xx?$e) = k2*e;
id NUM(xx?$f) = f;
.sort

id k1 = k1 - c*INV(a)*k2/2 + (c*e/4-b*d/2)*INV(a*b-c^2/4);
al k2 = k2 + (c*d/4-a*e/2)*INV(a*b-c^2/4);

B k1,k2,INV;
.sort
collect NUM;
factarg NUM;
factarg INV;
chainout NUM;
chainout INV;
id NUM(xx?)*INV(xx?) = 1;
splitarg INV,NUM;
id NUM(xx?) = xx;
id INV(xx?) = 1/xx;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

id k1^2 = 0;
id k2^2 = 0;
multiply -NUM(c^2-4*a*b)/4;
id NUM(xx?)*INV(xx?) = 1;
id NUM(xx?) = xx;
id a = $a;
id b = $b;
id c = $c;
id d = $d;
id e = $e;
id f = $f;

id p1^2 = 0;
id p12^2 = s12;
id p123^2 = s45;
id p1*p12 = s12/2;
id p45^2 = s45;
id p5^2 = 0;

.sort

G T1 = $a-$c/2;
G T2 = $b-$c/2;
G T3 = $c/2;

print+s;
.store

L [mu11*mu22] = mu11*mu22;

multiply I(0,1,1,1,1,1,1,1);
id mu12 = mu33-mu11-mu22;
#call expandmu
#call expandx(bt32)

*multiply MBresolve;
*id MBresolve*INT(?x) = MBresolve(INT(?x));
*id MBresolve(xxx?)*NUM(?x) = MBresolve(xxx*NUM(?x));
*repeat id MBresolve(xxx?)*eps = MBresolve(eps*xxx);
*id MBresolve(x?) = MBresolve(x,eps);

format 150;
print+s;
.end
