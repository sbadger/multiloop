#! /bin/sh

rm -f tables.tmp tables.fmt.tex tables.tex
cat *.tex > tables.tmp

tr -d "\r\n" < tables.tmp > tables.fmt.tex
vim tables.fmt.tex '+%s/\\\\/\\\\\\hline/g' '+%s/\([%&]\)/\1/g' '+%s/\\begin/\\begin/g' '+%s/FRAC/FRAC/g' '+%s/ *//g' '+wq'

sed -e "s/S(p\([0-9]\),p\([0-9]\))/s_{\1\2}/g" \
    -e "s/INV//g" \
    -e "s/NUM//g" \
    -e "s/FRAC(\(.*\),\(.*\))/\\\\frac{\1}{\2}/g" \
    -e "s/tau\([0-9]\)/tau_{\1}/g" \
    -e "s/\*//g" \
    -e "s/(\(s_{[0-9]*}\))/\1/g" \
    -e "s/(\(\\tau\))/\1 /g" \
    -e "s/tau/\\\\tau/g" tables.fmt.tex > tables.tex

vim tables.tex '+%s/\n&/ \& /g' '+%s/}soln/}\\hlinesoln/g' '+wq'

rm tables.fmt.tex
rm tables.tmp
