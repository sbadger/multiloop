% */ vim: set tw=100: */

\documentclass[11pt]{scrartcl}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{psfrag}
\usepackage{slashed}
\usepackage[sort&compress,numbers]{natbib}
\usepackage{color}

\def\la{\langle}
\def\ra{\rangle}

%% spinor products %%%

\def\A#1#2{\la#1#2\ra}
\def\B#1#2{[#1#2]}
\def\AB#1#2#3{\la#1|#2|#3]}
\def\BA#1#2#3{[#1|#2|#3\ra}
\def\AA#1#2#3{\la#1|#2|#3\ra}
\def\BB#1#2#3{[#1|#2|#3]}

\DeclareMathOperator{\tr}{\rm tr}
\def\trm{\tr_-}
\def\trp{\tr_+}

\def\e{\epsilon}
\def\wh{\widehat}
\def\tC{\tilde{C}}
\def\tT{\tilde{T}}
\def\fl#1{{#1}^\flat}
\def\kf#1{{K_{#1}^\flat}}
\def\kfm#1{{K_{#1}^{\flat,\mu}}}
\setlength{\parskip}{8pt}
\def\coll#1#2{\overset{#1||#2}{\to}}
\def\inf{{\rm Inf}}
\def\gg#1{\gamma_{#1}}
\def\XX{\chi}

\def\cv#1#2{\AB{#1}{\gamma^\mu}{#2}}
\def\cvS#1#2#3{\AB{#1}{#2}{#3}}

\title{Two-Loop $D$-dimensional cuts of 5-point amplitudes}

\begin{document}
\markright{Two-Loop $D$-dimensional cuts of 5-point amplitudes\hfill Simon Badger, \today}
\pagestyle{myheadings}

\section{Momentum Twistor Paramtrization of the Kinematics}

The kinematics can be represented with momentum twistors
$Z_i(\lambda_i, \tilde{\mu}_i)$ for each momentum $i=1,5$ \cite{Hodges:2009hk}. The first
two components are the positive helicity spinors\footnote{language?
$\tfrac{1}{2}(1+\gamma_5) u(p) = u_+(p) = (0, \lambda(p))$} whereas the
negative helicity spinors are obtained via:
\begin{align}
  \tilde{\lambda}_i =
  \frac{\A{i}{\,i+1}\tilde{\mu}_{i-1} + \A{i+1}{\,i-1}\tilde{\mu}_i + \A{i-1}{\,i}\tilde{\mu}_{i+1}}
  {\A{i}{\,i+1}\A{i+1}{\,i-1}\A{i-1}{\,i}}
  \label{eq:momtwistoreq}
\end{align}
where $\A{i}{j}$ are the usual spinor products. The representation is convenient
since momentum conservation is automatically satisfied and we are able to use the
symmetries of $Z$ to reduce the number of independent kinematic quantities.

The explicit form involving $5$ parameters is not unique, we don't know whether
there is an ideal choice or not yet. The first attempt was
\begin{align}
  Z =
  \begin{pmatrix}
    1 & 0 & -1 & \tfrac{x_2-1}{1-x_1x_2} & x_2 \\
    0 & 1 & x_1 & \tfrac{x_1-1}{1-x_1x_2} & -1 \\
    x_2 & 0 & x_3 & 0 & 0 \\
    0 & 1 & x_5 & 0 & 0
  \end{pmatrix}
  \label{eq:momtwistor1}
\end{align}
which has $\A{i\,}{i+1} = 1$. Yang came up with a version that follows from
a good choice for the $4$-point case:
\begin{align}
  Z =
  \begin{pmatrix}
    1 & 0 & \tfrac{1}{x_1} & \tfrac{1}{x_1}+\tfrac{1}{x_2} & \tfrac{1}{x_1}+\tfrac{1}{x_2}+\tfrac{1}{x_3} \\
    0 & 1 & 1 & 1 & 1 \\
    0 & 0 & 0 & x_4 & 1 \\
    0 & 0 & 1 & 1 & \frac{x_5}{x_4}
  \end{pmatrix}
  \label{eq:momtwistor2}
\end{align}

In order to fully re-construct expressions in terms of the general kinematic invariants all phase
factors should be removed before converting to the $x_i$ variables.

\section{Pentagon$|$Box}

The $8$ cut propagators are:
\begin{align}
  l_1 &= k_1 \\
  l_2 &= k_1-p_1 \\
  l_3 &= k_1-p_1-p_2 \\
  l_4 &= k_1-p_1-p_2-p_3 \\
  l_5 &= -k_2+p_4+p_5 \\
  l_6 &= -k_2+p_5 \\
  l_7 &= -k_2 \\
  l_8 &= -k_1-k_2
  \label{eq:db320Dprops}
\end{align}
Using a spinor basis from $(p_1,p_2)$ for $k_1$ and $(p_4,p_5)$ for $k_2$ we find the
following parametrization of the loop momenta,
\begin{align}
  k_1 &= p_1 - \tau_1 \frac{s_{23}\B13\cv12}{2 s_{13}\B23} + (1+\tau_1)\frac{\B23\cv21}{2\B13} \\
  k_2 &= p_5 + \tau_2 \frac{\B34\cv45}{2\B35 s_{34}} + \tau_3\frac{\B35\cv54}{2\B34 s_{35}} \\
  \mu_{11} &= \tau_1(1+\tau_1) \frac{s_{12}s_{23}}{s_{13}} \\
  \mu_{22} &= -\tau_2\tau_3 \frac{s_{45}}{s_{34}s_{35}} \\
  \mu_{12} &= 2(k_1\cdot k_2),
  \label{eq:db320Dparam}
\end{align}
satisfying the constraints,
\begin{align}
  \{
  &k_1^2 = \mu_{11},\, k_2^2 = \mu_{22},\, 2(k_1\cdot k_2) = \mu_{12}, \nonumber\\
  &2(k_1 \cdot p_1) = 0,\,
  2(k_1 \cdot p_2) = s_{12},\,
  2(k_1 \cdot p_3) = s_{45}-s_{12}, \nonumber\\
  &2(k_2 \cdot p_4) = s_{45},\,
  2(k_2 \cdot p_5) = 0
  \}
\end{align}
The explicit form of $\mu_{12}$ is quite long but can be computed easily from
the parametrizations of $k_1$ and $k_2$.

\subsection{Integrand parametrization}

The integrand has $79$ coefficients in terms of 6 ISPs $(k_1\cdot p_5), (k_2\cdot p_2), (k_2\cdot
p_1), \mu_{11}, \mu_{12}, \mu_{22}$, the form can be obtained easily using the {\sc BasisDet}
package \cite{Zhang:2012ce}. We choose to prefer the monomials in $\mu_{ij}$ over the higher
powers of $(k_1\cdot p_5)$ which would be preferred by the polynomial division. This is important
to make the four dimensional limit manifest.

For the all-plus helicity configuration of the five gluon amplitude we find:
\begin{align}
  &\Delta_{8;123*45*}(1^+,2^+,3^+,4^+,5^+) =
  \frac{i \, s_{12}s_{23}s_{45} \, f(D_s,\mu_{11},\mu_{22},\mu_{12})}{\A12\A23\A34\A45\A51}\Big(
  \nonumber\\&
  + \left( 1 + \frac{s_{12} (s_{23}-s_{15})-s_{23} s_{34}+(s_{15}+s_{34}) s_{45}}{{\rm tr}_5(1234)} \right) (k_1\cdot p_5)
  \nonumber\\&
  - \frac{ s_{34}s_{45}s_{51} }{{\rm tr}_5(1234)}
  \Bigg)
  \label{eq:5ptallplus_db320D}
\end{align}
where
\begin{align}
  f(D_s,\mu_{11},\mu_{22},\mu_{12})
  = (D_s-2)\left( \mu_{11}\mu_{22} + \mu_{11}\mu_{33} + \mu_{22}\mu_{33} \right) + 4\left(
  \mu_{12}^2 - 4\mu_{11}\mu_{22} \right)
\end{align}
and $\mu_{33} = \mu_{11} + \mu_{22} + \mu_{12}$. The result is remarkably similar to the known form
of the $\mathcal{N}=4$ SYM amplitude. If we replace the pre-factor with the MHV tree, setting
$f\rightarrow 1$ we obtain the $\mathcal{N}=4$ SYM result for the same topology
\cite{Bern:2006vw,Cachazo:2008vp}. Another observation is that the numerator appearing in the integral is
exactly the same as that appearing in the 4-gluon all-plus amplitude \cite{Bern:2000dn}. It would
be interesting to study the proposed duality between self-dual
Yang-Mills and $\mathcal{N}=4$ SYM studied previously one-loop \cite{Bern:1996ja} in more detail.

\bibliographystyle{JHEP}
\bibliography{twoloop}

\end{document}
