* vim: set foldmethod=marker:
#-
#include ../2to3tools.prc
#include diagrams.prc

auto CF INTdb, INTbt;
S Dsm2;
S w,w123,w543,w132,w534,w12p,w12m,w45p,w45m,w13p,w13m,w53p,w53m;

off stats;
.global

#procedure signs

*** diagrams for pentagon-box: db221
id xxdiag1  =  1;
id xxdiag2  =  2;
id xxdiag3  =  2;
id xxdiag4  =  -2;
id xxdiag5  =  1;
id xxdiag6  =  -2;
id xxdiag7  =  -2;
id xxdiag8  =  -2;
#endprocedure

#define rerun "0"
#define dd "0"

#if 'rerun'=1

*** diagrams for pentagon-box: db221
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq34, rho4, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq34, nu4, 'f2'), P(p3, mu3, 0), P(-qq4, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for pentagon-box: db311
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd' =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq4, rho5, 'f2'), P(kq5, nu7, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq4, nu5, 'f2'), P(p4, mu4, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    V(P(-kq5, rho7, 'f3'), P(p5, mu5, 0), P(kq, nu8, 'f3'))*Prop(rho7, nu7, 'f3')*Prop(rho8, nu8, 'f3')
;
*}}}
#call expandtopo
multiply EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5);

#call subvertices
#call cancel

.sort

argument AB,dot,S,IS;
id kk = k1;
id kk1 = MOM(k1,-p1);
id kk12 = MOM(k1,-p1,-p2);
id kk123 = MOM(k1,-p1,-p2,-p3);
id p123 = MOM(p1,p2,p3);
id p12 = MOM(p1,p2);
id p23 = MOM(p2,p3);
id p45 = MOM(p4,p5);
id qq = k2;
id qq4 = MOM(k2,-p4);
id qq5 = MOM(k2,-p5);
id qq34 = MOM(k2,-p3,-p4);
id qq45 = MOM(k2,-p4,-p5);
id qq345 = MOM(k2,-p3,-p4,-p5);
id kq = MOM(k1,k2);
id kq5 = MOM(k1,k2,p5);
endargument;

*{{{ expand and simplify ids

id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
repeat id AB(p1?,MOM(k1?{k1,k2},?xx),p3?) = AB(p1,k1,p3)+AB(p1,MOM(?xx),p3);

argument AB;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
id AB(p1?,-MOM(?x1),p2?) = -AB(p1,MOM(?x1),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
id AB(p1?,MOM,p3?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

repeat id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
repeat id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
#call cancel

repeat id dot(MOM(p1?),xx?) = dot(p1,xx);

id dot(MOM(k1?{k1,k2},?x1),MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2)) + dot(k2,MOM(?x1)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),MOM(?x2)) = dot(k1,MOM(?x2)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),p1?) = dot(k1,p1) + dot(MOM(?x1),p1);
id dot(k1?{k1,k2},MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2));

argument dot;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
id dot(p1?ps,MOM(p2?,?x)) = dot(p1,p2) + dot(p1,MOM(?x));
id dot(p1?ps,MOM(?x1,p1?ps,?x2)) = dot(p1,MOM(?x1,?x2));
id dot(p1?ps,MOM(p1?ps,?x2)) = dot(p1,MOM(?x2));
id dot(p1?ps,MOM(p1?ps,p2?)) = dot(p1,p2);
id dot(MOM,x?) = 0;
argument dot;
repeat id MOM(p1?,?x) = p1 + MOM(?x);
id MOM = 0;
endargument;
id dot(p1?ps,p1?ps) = 0;
id dot(p?,p?) = S(p);
id IS(MOM(?x)) = IS(?x);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;

id dot(px?,p1+p2+p3) = -dot(px,p4+p5);
id dot(px?,p3+p4+p5) = -dot(px,p1+p2);
id dot(p?,p?) = S(p);
splitarg S,IS;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
.sort

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);
*
id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);
*
id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);
*
id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

id AB(px1?,MOM(px2?,px3?,px4?),px5?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

#call cancel
id IS(p1,p2,p3) = 1/s45;
id IS(p3,p4,p5) = 1/s12;
id dot(p1+p2,p4+p5) = -(s12+s45)/2;
id dot(p1+p2,p3+p4) = -(s12+s34)/2;
id S(k1?{k1,k2}) = dot(k1,k1);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id dot(p1?ps,p2?) = dot(p1,MOM(p2));
argument dot;
splitarg MOM;
endargument;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

AB s12,s23,s34,s45,s15,s13,s14,s24,s25,s35;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument INV,NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

*}}}

.sort

#do i=1,'dd'
  #write <tmp/np_diagrams.prc> "G diag'i' = %e" diag'i'
#enddo

#else
#include- tmp/np_diagrams.prc
#endif

B MOM,V;
print[];
.store

L cutAMPdb221 =
#do i=1,4
+xxdiag'i'*diag'i'
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p4) = 0;
id dot(k2,p3) = s34/2;
id dot(k2,p3+p4) = s34/2;
id dot(k2,p5) = -dot(k1,p5);

topolynomial;
.sort
format maple;
#write <db221.input.mpl> "INT_db221 := proc(k1, k2, mu11, mu12, mu22)"
#write <db221.input.mpl> "local Z;"
#write <db221.input.mpl> "global p1, p2, p3, p4, p5;"
#write <db221.input.mpl> "%X"
#write <db221.input.mpl> "return %e" cutAMPdb221
#write <db221.input.mpl> "end:"
frompolynomial;
.sort
format 150;
#write <db221.input.frm> "L INTdb221 = %e" cutAMPdb221
.store

L cutAMPdb311 =
#do i=5,8
+xxdiag'i'*diag'i'
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p1+p2+p3) = s45/2;
id dot(k1,p3) = (s45-s12)/2;
id dot(k2,p4) = 0;
id dot(k2,p5) = -dot(k1,p5);

topolynomial;
.sort
format maple;
#write <db311.input.mpl> "INT_db311 := proc(k1, k2, mu11, mu12, mu22)"
#write <db311.input.mpl> "local Z;"
#write <db311.input.mpl> "global p1, p2, p3, p4, p5;"
#write <db311.input.mpl> "%X"
#write <db311.input.mpl> "return %e" cutAMPdb311
#write <db311.input.mpl> "end:"
frompolynomial;
.sort
format 150;
#write <db311.input.frm> "L INTdb311 = %e" cutAMPdb311
.end
