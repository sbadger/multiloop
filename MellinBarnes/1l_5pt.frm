#-
#include- ../5g_allplus/2to3tools.prc

CF H,d, dbar;

off stats;
.global

#write "### overall normalization of 1/(prod_{i=1}^5 <i i+1>) removed"
L [A51g(+++++)] =
  + H(p1,p2,p3,p4)
  + H(p1,p2,p3,p5)
  + H(p1,p2,p4,p5)
  + H(p1,p3,p4,p5)
  + H(p2,p3,p4,p5)
;

id H(p1?,p2?,p3?,p4?) = AB(p1,p2,p3,p4,p1);
*id xx1 = 1;
*id xx2 = 1;
*id xx3 = 1;
*id xx4 = 1;
*id xx5 = 1;

#call simplify5

*splitarg NUM;
*#do a1={-1,1}
*#do a2={-1,1}
*#do a3={-1,1}
*id NUM('a3'*s34*s45,'a2'*s23*s34,s15*s45,'a1'*s12*s23, - s12*s15) = -d(-'a1',-'a2',-'a3');
*id NUM('a3'*s34*s45,'a2'*s23*s34,-s15*s45,'a1'*s12*s23,s12*s15) = d('a1','a2','a3');
*id NUM('a3'*s34*s45,'a2'*s23*s34,s15*s45,'a1'*s12*s23,s12*s15) = dbar('a1','a2','a3');
*id NUM('a3'*s34*s45,'a2'*s23*s34,-s15*s45,'a1'*s12*s23,-s12*s15) = -dbar(-'a1',-'a2',-'a3');
*#enddo
*#enddo
*#enddo

B xx1,...,xx5;
print+s;
.end
