#! /bin/sh

rm -f tables.tmp tables.tex
cat *.tex > tables.tmp

sed -e "s/S(p\([0-9]\),p\([0-9]\))/s_{\1\2}/g" \
    -e "s/S(P\([0-9]\))/s_{\1}/g" \
    -e "s/AB(p\([0-9]\),[pP]\([0-9]\),p\([0-9]\))/\AB{\1}{\2}{\3}/g" \
    -e "s/INV//g" \
    -e "s/NUM//g" \
    -e "s/FRAC(\(.*\),\(.*\))/\\\\frac{\1}{\2}/g" \
    -e "s/tau\([0-9]\)/tau_{\1}/g" \
    -e "s/\*//g" \
    -e "s/(\(s_{[0-9]*}\))/\1/g" \
    -e "s/(\(\\tau\))/\1 /g" \
    -e "s/tau/\\\\tau/g" tables.tmp > tables.fmt.tex

mv tables.fmt.tex tables.tex
rm tables.tmp
vim tables.tex '+%s/\([^\\AB}]\)\n/\1/g' '+%s/\n *&/ \&/g' '+wq'


