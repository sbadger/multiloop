interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

# solving planar triangle | box

read("boxtri1.reduceM.log"):

dvec:=Vector(94):
for k from 1 to 94 do:
dvec[k]:=Dvec[k]:
od:

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[70..94]));
Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dvec:

dvecR:=SubVector(dvecT,[1..69]):
Cvec:=Ieqs.dvecR;

fd:=fopen("final/tribox1.integrand.mpl",WRITE):

fprintf(fd,"Cvec:=Vector(69):\n"):
for k from 1 to 69 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,simplify(Cvec[k])):
od:

fprintf(fd,"Zvec:=Vector(25):\n"):
for k from 70 to 94 do:
fprintf(fd,"Zvec[%d]:=%A;\n",k-69,simplify(dvecT[k])):
od:

fclose(fd):
