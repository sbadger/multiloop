interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.16e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_bt31D:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, 1-tau1]:
b := [tau2, tau3, tau4, -(1+tau2+tau3)*s12/s23-(1+tau2)*s13/s23-(1+tau3+tau4)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*(1-tau1);
mu22 := s12/s13*(s12*(1 + tau2 + tau3)*tau4 + s23*tau4*(1 + tau3 + tau4) + s13*(tau4 + tau2*(tau3 + tau4)));
mu12 := tau5;

return k1,k2,mu11,mu12,mu22;
end:

xxdiag1  := 1:
xxdiag2  := 2:
xxdiag3  :=-2:
xxdiag4  := 2:
xxdiag9  := 1:
xxdiag10 :=-2:
xxdiag11 := 2:
xxdiag12 :=-4:
xxdiag13 :=-1:
xxdiag14 := 1/2:
#xxdiag15 := 1:
#xxdiag16 :=-2:
#xxdiag17 :=-2:
#xxdiag18 :=-2:
#xxdiag19 :=-1:
#xxdiag20 := 2:

#xxdiag28 := 1:
#xxdiag29 := 2:
#xxdiag30 := 2:
#xxdiag31 := 4:
#xxdiag32 :=-1:
#xxdiag33 :=-2:
#xxdiag34 :=-1:
#xxdiag35 := -1/2:
#xxdiag36 := 1:

xxdiag45:=1:
xxdiag46:=2:
xxdiag47:=2:
xxdiag48:=-2:
xxdiag49:=-1:
xxdiag50:=-2:

xxdiag51 := 1:
xxdiag52 := -2:
xxdiag53 := -2:
xxdiag54 := 4:
xxdiag55 := -1:
xxdiag56 := 2:
xxdiag57 := -1:
xxdiag58 := -1/2:
xxdiag59 := 1:

xxsub1 := 1:
xxsub2 := 1:
xxsub3 := 1:
Ds := Dsm2 + 2;

w123 := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:
w132 := (AB(p3,p2,p1)*eta(p1,p3)-AB(p1,p2,p3)*eta(p3,p1))/2/s13:
w534 := (AB(p3,p4,p5)*eta(p5,p3)-AB(p5,p4,p3)*eta(p3,p5))/2/s35:

read("tmp/bt31D.input.mpl"):
#read("INT_5g+++++.mpl"):

for k1 from 0 to 4 do:
for k2 from 0 to 2 do:
for k3 from 0 to 2 do:
for k4 from 0 to 2 do:
for k5 from 0 to 2 do:
ds(k1,k2,k3,k4,k5):=0:
od:
od:
od:
od:
od:

for th1 from 0 to 4 do:
for th2 from 0 to 2 do:
for th3 from 0 to 2 do:
for th4 from 0 to 2 do:
for th5 from 0 to 2 do:
printf("%d%d%d%d%d\n",th1,th2,th3,th4,th5);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/5);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/3);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/3);
t4 := 1.3*exp(2.*evalf(Pi)*I*th4/3);
t5 := 1.3*exp(2.*evalf(Pi)*I*th5/3);

k1,k2,mu11,mu12,mu22 := on_shell_bt31D([p1,p2,p3,p4,p5],t1,t2,t3,t4,t5):
val := INT_bt31D(k1,k2,mu11,mu12,mu22);

print(collect(val,Dsm2));
quit;

for k1 from 0 to 4 do:
for k2 from 0 to 2 do:
for k3 from 0 to 2 do:
for k4 from 0 to 2 do:
for k5 from 0 to 2 do:
  ds(k1,k2,k3,k4,k5) := expand(ds(k1,k2,k3,k4,k5) + evalf(val/t1^k1/t2^k2/t3^k3/t4^k4/t5^k5/(5.*3.*3.*3.*3.))):
od:
od:
od:
od:
od:

od:
od:
od:
od:
od:

read("../final/bt31D.coeffvecs.mpl");

for k1 from 0 to 4 do:
for k2 from 0 to 2 do:
for k3 from 0 to 2 do:
for k4 from 0 to 2 do:
for k5 from 0 to 2 do:
# tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
# tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
# tmp3 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
# tmp4 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
# tmp5 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
# tmp6 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
# if abs(tmp1)<1e-20 then; tmp1:=0: fi:
# if abs(tmp2)<1e-20 then; tmp2:=0: fi:
# if abs(tmp3)<1e-20 then; tmp3:=0: fi:
# if abs(tmp4)<1e-20 then; tmp4:=0: fi:
# if abs(tmp5)<1e-20 then; tmp5:=0: fi:
# if abs(tmp6)<1e-20 then; tmp6:=0: fi:
# printf("d_%d%d%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k1, k2, k3, k4, k5, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
  lprint(k1,k2,k3,k4,k5,expand(ds(k1,k2,k3,k4,k5)));
od:
od:
od:
od:
od:

for k from 1 to 69 do:
# tmp1 := Re(subs(Dsm2=0,Dvec[k])):
# tmp2 := Im(subs(Dsm2=0,Dvec[k])):
# tmp3 := Re(coeff(Dvec[k],Dsm2)):
# tmp4 := Im(coeff(Dvec[k],Dsm2)):
# tmp5 := Re(coeff(Dvec[k],Dsm2^2)):
# tmp6 := Im(coeff(Dvec[k],Dsm2^2)):
# if abs(tmp1)<1e-20 then; tmp1:=0: fi:
# if abs(tmp2)<1e-20 then; tmp2:=0: fi:
# if abs(tmp3)<1e-20 then; tmp3:=0: fi:
# if abs(tmp4)<1e-20 then; tmp4:=0: fi:
# if abs(tmp5)<1e-20 then; tmp5:=0: fi:
# if abs(tmp6)<1e-20 then; tmp6:=0: fi:
# printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
 lprint(k,expand(Dvec[k]));
od:

read("../bt31D.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(69):
Cvec := iMM.Dvec:

for k from 1 to 69 do:
# tmp1 := Re(subs(Dsm2=0,Cvec[k])):
# tmp2 := Im(subs(Dsm2=0,Cvec[k])):
# tmp3 := Re(coeff(Cvec[k],Dsm2)):
# tmp4 := Im(coeff(Cvec[k],Dsm2)):
# tmp5 := Re(coeff(Cvec[k],Dsm2^2)):
# tmp6 := Im(coeff(Cvec[k],Dsm2^2)):
# if abs(tmp1)<1e-20 then; tmp1:=0: fi:
# if abs(tmp2)<1e-20 then; tmp2:=0: fi:
# if abs(tmp3)<1e-20 then; tmp3:=0: fi:
# if abs(tmp4)<1e-20 then; tmp4:=0: fi:
# if abs(tmp5)<1e-20 then; tmp5:=0: fi:
# if abs(tmp6)<1e-20 then; tmp6:=0: fi:
# printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
 lprint(Clbl[k],expand(Cvec[k]));
od:


