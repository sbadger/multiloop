(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];
NUM[x_] := x;

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];

(*Series[cGamma1,{eps,0,2}]*)
(*Series[Exp[-eps*EulerGamma],{eps,0,2}]*)
(*Series[cGamma2,{eps,0,2}]*)
(*Series[Exp[-2*eps*EulerGamma],{eps,0,2}]*)

invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> -s12/2, p[1].p[3] -> -(-s12-s23)/2, p[1].p[4] -> -s23/2,
      p[2].p[3] -> -s23/2, p[2].p[4] -> -(-s23-s12)/2,
      p[3].p[4] -> -s12/2};

kinpt = {s12 -> 5., s23 -> 11.};

db220 = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[1],0,a2] PR[l[1]-p[1]-p[2],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[4],0,a5] PR[l[2]-p[3]-p[4],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

db220F = {
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,1,1,3,1},db220]*eps),
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,1,2,2,1},db220]*eps),
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,1,3,1,1},db220]*eps),
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,2,1,2,1},db220]*eps),
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,2,2,1,1},db220]*eps),
  MBresolve[#,eps] & /@ (2*NUM[ - 1 + eps]*INT[8 - 2*eps,{1,1,1,3,1,1,1},db220]*eps),
  MBresolve[#,eps] & /@ (INT[6 - 2*eps,{1,1,1,1,1,1,1},db220]*eps^2)};

SetVerbosity[1];
MBexpand[db220F,1/cGamma2,{eps,0,0}];
Process[%,{5,4,3}];
result = MBintegrate[%, kinpt, FixedContours->True];
MBmerge[result]

xx = s23/s12;
check = -1/s12/6*(1 - 2*eps*Log[s12] + 2*eps^2*Log[s12]^2)*( \
    - 1/eps^2 \
    + 1/eps*( 1/2*xx*(Log[xx]^2+Pi^2)/(1+xx)^2 + xx*Log[xx]/(1+xx) - 8./3. ) \
    + xx/(1+xx)^2*(-PolyLog[3,-xx]+Zeta[3]+Log[xx]*PolyLog[2,-xx] \
                  + (1/2*Log[1+xx]-2/3*Log[xx]-1/2*xx+1/3)*(Log[xx]^2+Pi^2) \
                  + (8/3*(1+xx)-Pi^2/6)*Log[xx])
    -Pi^2/6*(2-5*xx)/(1+xx) -52/9);

Series[check,{eps,0,0} ]/. kinpt

Exit[]

box = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]+p[1],0,a2] PR[l[1]+p[1]+p[2],0,a3] PR[l[1]+p[1]+p[2]+p[3],0,a4]},
	{l[1]}, Invariants->invariants];

pent = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]+p[1],0,a2] PR[l[1]+p[1]+p[2],0,a3] PR[l[1]+p[1]+p[2]+p[3],0,a4] PR[l[1]+p[1]+p[2]+p[3]+p[4],0,a5]},
	{l[1]}, Invariants->invariants];

