* vim: set foldmethod=marker:
#-
S w3,x,y,z;
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];
auto S x,p,e,q,P,m,k,w,d,s;
S [k.3],[q.2],[k.w],[q.w];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,c,p,q,h;
S mu;
S l1,...,l8;

set ps:p1,...,p5,k1f,k2f;
set mps:P1,...,P5;

#include- ../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
#endprocedure
*}}}
*{{{ simplify
#procedure simplify

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
id AB(px?,MOM(p1,p2),py?) = -AB(px,MOM(p3,p4),py);
#call compress(1);
#call breakstrings
#call cancel

id AB(p1?,p2?,p3?) = A(p1,p2)*B(p2,p3);
id IAB(p1?,p2?,p3?) = IA(p1,p2)*IB(p2,p3);

id A(px?,p4)*B(p4,py?) = -A(px,p1)*B(p1,py)-A(px,p2)*B(p2,py)-A(px,p3)*B(p3,py);

id A(p3,p1)*B(p1,p4)*IB(p2,p4) = -A(p3,p2);
id B(p3,p1)*A(p1,p4)*IA(p2,p4) = -B(p3,p2);
id A(p3,p2)*B(p2,p4)*IB(p1,p4) = -A(p3,p1);
id B(p3,p2)*A(p2,p4)*IA(p1,p4) = -B(p3,p1);

id B(p2,p4)*IB(p1,p4) = -A(p3,p1)*IA(p3,p2);

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS
id S(p3,p4) = S(p1,p2);
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);

id S(p1?,p2?)*IS(p1?,p2?) = 1;


#endprocedure
*}}}
*{{{ schouten2
#procedure schouten2

argument NUM;
id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
id AB(p1?,p2?,p3?)*A(p3?,p4?) = AA(p1,p2,p3,p4);
id B(p1?,p2?)*AB(p2?,p3?,p4?) = BB(p1,p2,p3,p4);
endargument;

#call SortCollect

splitarg NUM;

id NUM(A(p1?,p2?)*A(p3?,p4?),-A(p1?,p4?)*A(p3?,p2?)) = A(p1,p3)*A(p2,p4);
id NUM( - A(p1?,p2?)*A(p3?,p4?),A(p1?,p4?)*A(p3?,p2?)) = -A(p1,p3)*A(p2,p4);

repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

#endprocedure
*}}}

off stats;

.global

#do s=1,1
G k1's' = ( a1's'*p1 + a2's'*p2+a3's'*AB(p1,mu,p2) + a4's'*AB(p2,mu,p1) )/2;
G k2's' = ( b1's'*p3 + b2's'*p4+b3's'*AB(p3,mu,p4) + b4's'*AB(p4,mu,p3) )/2;
G k3's' = ( c1's'*p3 + c2's'*p4+c3's'*AB(p3,mu,p4) + c4's'*AB(p4,mu,p3) )/2;

id p1 = AB(p1,mu,p1);
id p2 = AB(p2,mu,p2);
id p3 = AB(p3,mu,p3);
id p4 = AB(p4,mu,p4);

.sort
G l1's'  = k1's';
G l2's'  = k1's'-MOM(p1,mu);
G l3's'  = k1's'-MOM(p1,mu)-MOM(p2,mu);
G l4's'  = k2's';
G l5's'  = k2's'-MOM(p3,mu);
G l6's'  = k2's'-k3's'-MOM(p3,mu);
G l7's'  = k2's'-k3's'-MOM(p3,mu)-MOM(p4,mu);
G l8's'  = k2's'-k3's'-k1's'+MOM(p1,mu)+MOM(p2,mu);
G l9's'  = k2's'-k1's'+MOM(p1,mu)+MOM(p2,mu);
G l10's' = k3's';

#enddo

id MOM(p1?,mu?) = AB(p1,mu,p1)/2;

.store

#do s=1,1
#do x=1,10
L zero's''x' = l'x''s'*l'x''s';
#enddo
#enddo

id NUM(x?)*INV(x?) = 1;
#call lprod
#call cancel

B INV;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(x?)*INV(x?) = 1;

B NUM;
.sort
collect TMP;
id TMP(x?)=1;
id NUM(x?) = x;

id a31 = B(p1,p3)*IB(p2,p3)*a31;
id a41 = IB(p1,p3)*B(p2,p3)*a41;

id b31 = B(p1,p3)*IB(p1,p4)*b31;
id b41 = IB(p1,p3)*B(p1,p4)*b41;

id c31 = B(p1,p3)*IB(p1,p4)*c31;
id c41 = IB(p1,p3)*B(p1,p4)*c41;

#call simplify
id S(p1,p3) = -S(p1,p4)-S(p1,p2);
#call simplify

.sort
format maple;
#write <tcourt.geneqs.mpl> "eqs:=[]:"
#do z=1,10
#write <tcourt.geneqs.mpl> "eqs:=[op(eqs),%E=0]:" zero1'z'
#enddo

format 150;
print+s;
.end
.store

G Delta = c0+c1*dot(l1,p5)+c2*dot(l1,p4)+c3*dot(l7,p1);

B x;
print[];
.store

#do s=1,4
G Delta's' = Delta;

repeat;
id,once dot(l1,p5) = l1's'*AB(p5,mu,p5)/2;
#call lprod
endrepeat;

repeat;
id,once dot(l1,p4) = l1's'*AB(p4,mu,p4)/2;
#call lprod
endrepeat;

repeat;
id,once dot(l7,p1) = l7's'*AB(p1,mu,p1)/2;
#call lprod
endrepeat;

B c0,...,c4;
.sort
collect NUM;
#call SortCollect
#call simplify
id NUM(px?) = px;
id IA(p1?,p2?) = IS(p1,p2)*B(p2,p1);
id IB(p1?,p2?) = IS(p1,p2)*A(p2,p1);
AB A,B;
.sort
collect NUM;
#call SortCollect
#call simplify
#call schouten2
argument NUM;
id AA(p3,p1,p4,p5) = -AA(p3,p4,p1,p5)+AB(p4,p1,p4)*A(p3,p5);
id BB(p5,p4,p1,p3) = -BB(p5,p1,p4,p3)+AB(p4,p1,p4)*B(p5,p3);
id BB(p5,p1,p4,p3) = -BB(p3,p4+p5,p1,p5)+BB(p3,p5,p1,p5);
id AA(p3,p4,p1,p5) = -AA(p5,p1,p4+p5,p3)+AA(p5,p1,p5,p3);
id AA(?x,p4+p5,?y) = -AA(?x,p1+p2+p3,?y);
id BB(?x,p4+p5,?y) = -BB(?x,p1+p2+p3,?y);
endargument;
#call simplify

format 150;
B tau;
print+s;
.store
#enddo

#do s=1,4
L d's' = Delta's';
#enddo

B tau;
print[];
.sort
hide;

#define csset "{c0,c1,c2,c3}"
.sort

#write <matrix_pentbox.planar.mpl> "cs:=Vector(4):"
#define i "1"
#do c='csset'
#write <matrix_pentbox.planar.mpl> "cs['i']:='c':"
#define i "{'i'+1}"
#enddo

#write <matrix_pentbox.planar.mpl> "dvec:=Vector(4):"
#write <matrix_pentbox.planar.mpl> "eqs:=Matrix(4,4):"
#define i "1"
#do s=1,4

#write <matrix_pentbox.planar.mpl> "dvec['i']:=ds['s']:"

#define j "1"
#do c='csset'

L eqs'i'x'j' = d's';

if(count('c',1)<1);
discard;
endif;
id 'c'=1;

.sort
#write <matrix_pentbox.planar.mpl> "eqs['i','j']:=%E:" eqs'i'x'j'
#define j "{'j'+1}"
.sort
hide;
#enddo

#define i "{'i'+1}"
#enddo

.sort
unhide;

.end
