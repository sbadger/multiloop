*** non-planar from BCJ ***
*** version 1: not symmetric ***
*id Delta332(k1?,k2?,p1?,p2?,p3?,p4?,p5?) =
*  + Delta3315L(k1,k2,p1,p2,p3,p4,p5)*(S(k1,p4,p5)-MU(k1,k1))
*  + Delta321(k1,k2,p1,p2,p3,p4,p5)
*  - Delta321(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2);

*id Delta422(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -Delta321(k1,k2,p1,p2,p3,p4,p5);

*id DeltaNP331M1(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -Delta3315L(k1,k2,p1,p2,p3,p4,p5);

*id DeltaNP331M2(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = 0;

*id DeltaNP331M4(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -DeltaNP331M3(k1,k2,p1,p2,p3,p4,p5)-Delta3315L(-k2+p4+p5,-k1+p1+p2,p4,p5,p3,p1,p2);

*id DeltaNP331M3(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = INV(S(k1,k2)-mu11-mu12-mu22)*(
*  - (S(k1,p4,p5)-mu11)*Delta3315L(k1,k2,p1,p2,p3,p4,p5)
*  + (S(p4,p5)-S(p1,p2)-S(k1,p4,p5)+mu11)*Delta3315L(-k2+p4+p5,-k1+p1+p2,p4,p5,p3,p1,p2)
*  - Delta321(k1,k2,p1,p2,p3,p4,p5)
*  + Delta321(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2)
*  - Delta321(-k2+p4+p5,-k1+p1+p2,p4,p5,p3,p1,p2)
*  + Delta321(k1+p3,k2,p3,p1,p2,p4,p5)
*);

*** version 2: symmetric ***
id DeltaNP332(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*
 S(p1,p2)*S(p4,p5)/4*(
    + S(p2,p3)*(2*S(p1,p2)-4*dot(k1,p5)+4*dot(k1,p4)+2*dot(k1,p3)-2*dot(k2,p3))*trp(p1,p3,p4,p5)
    - S(p3,p4)*(2*S(p4,p5)-4*dot(k2,p1)+4*dot(k2,p2)-2*dot(k1,p3)+2*dot(k2,p3))*trp(p5,p3,p2,p1)
    - 2*S(p2,p3)*S(p3,p4)*S(p1,p5)*(2*dot(k1,p3)-2*dot(k2,p3))
);

id DeltaNP232(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = Delta331M1(k1,k2,p1,p2,p3,p4,p5);

id DeltaNP331M2(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -DeltaNP331M1(-k2,-k1,p5,p4,p3,p2,p1);
id DeltaNP331M3(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = DeltaNP331M1(-k2+p4+p5,-k1+p1+p2,p4,p5,p3,p1,p2);
id DeltaNP331M4(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -DeltaNP331M1(-k1+p1+p2,-k2+p4+p5,p2,p1,p3,p5,p4);
id DeltaNP331M1(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*
  - S(p1,p2)*S(p4,p5)/4*(
    + 2*S(p2,p3)*S(p3,p4)*S(p1,p5) + S(p2,p3)*trp(p1,p3,p4,p5) - S(p3,p4)*trp(p3,p5,p1,p2)
);

id DeltaNP422(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*
  S(p1,p2)*S(p2,p3)*S(p4,p5)*(
    + 1/2*(2*dot(k1,p5)-2*dot(k1,p4)-S(p4,p5))*trp(p1,p3,p4,p5) + S(p1,p5)*S(p3,p4)*S(p4,p5)
);
id DeltaNP322M1(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = Delta3315L(k1,k2,p1,p2,p3,p4,p5);

** planar integrands ***

id Delta341(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -Delta431(k2,k1,p5,p4,p3,p2,p1);
id Delta340(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -Delta430(k2,k1,p5,p4,p3,p2,p1);

id Delta431(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*(
  -S(p1,p2)*S(p2,p3)*S(p4,p5)*( trp(p1,p3,p4,p5)*(S(k1,p5)-MU(k1,k1)) + S(p1,p5)*S(p3,p4)*S(p4,p5) )
);

id Delta331M1(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*(
  -S(p3,p4)*S(p4,p5)^2*trp(p1,p2,p3,p5)
);

id Delta331M2(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*(
  -S(p1,p5)*S(p4,p5)^2*trm(p1,p2,p3,p4)
);

id Delta3315L(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*F1(k1,k2)*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1)*tr5(p1,p2,p3,p4))*(
  S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p1,p5)
);

id Delta430(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*(F2(k1,k2)+F3(k1,k2,p4,p5))*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1))*(
  -S(p1,p2)*trp(p1,p3,p4,p5)*IS(p1,p3)*(2*dot(k1,ww(p1,p2,p3))+S(p2,p3))/2
);

id Delta330M2(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = I*(F2(k1,k2)+F3(k1,k2,p4,p5))*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1))*(
  -(S(p4,p5)-S(p2,p3))*IS(p1,p3)*trp(p1,p3,p4,p5)/2
);

id Delta3305L(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = -I*INV(A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1))*(
    1/2*(trp(p1,p2,p4,p5)-trp(p1,p3,p4,p5)*trp(p1,p2,p3,p5)*IS(p1,p3)*IS(p3,p5))*(
        F2(k1,k2)
      + (Dsm2)^2*MU(k1,k1)*MU(k2,k2)*(4*dot(k1,p3)*dot(k2,p3)+(S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))*(S(p1,p2)+S(p4,p5))+S(p1,p2)*S(p4,p5))*IS(p1,p2)*IS(p4,p5)
        )
    + (Dsm2)^2*MU(k1,k1)*MU(k2,k2)*(
        + S(p1,p5)*(S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))
        + trp(p1,p2,p3,p5)*((S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))*IS(p3,p5)/2-dot(k1,p3)*IS(p1,p2)*(1+2*dot(k2,ww(p5,p4,p3))*IS(p3,p5)+(S(k2,-p5)-MU(k2,k2))*(S(p1,p2)-S(p4,p5))*IS(p3,p5)*IS(p4,p5)))
        + trp(p1,p3,p4,p5)*((S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))*IS(p1,p3)/2-dot(k2,p3)*IS(p4,p5)*(1+2*dot(k1,ww(p1,p2,p3))*IS(p1,p3)+(S(k1,-p1)-MU(k1,k1))*(S(p4,p5)-S(p1,p2))*IS(p1,p2)*IS(p1,p3)))
      )
);

id Delta421(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = 0;
id Delta3214L(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = 0;
id Delta420(k1?,k2?,p1?,p2?,p3?,p4?,p5?) = 0;

id F1(k1?,k2?) = (Dsm2)*(MU(k1,k1)*MU(k2,k2)+(MU(k2,k2)+MU(k1,k1))*(MU(k1,k1)+MU(k2,k2)+MU(k1,k2)))+4*(MU(k1,k2)^2-4*MU(k1,k1)*MU(k2,k2));
id F2(k1?,k2?) = 2*(Dsm2)*(MU(k1,k1)+MU(k2,k2))*MU(k1,k2);
id F3(k1?,k2?,p4?,p5?) = (Dsm2)^2*((S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))+S(p4,p5))*IS(p4,p5)*MU(k1,k1)*MU(k2,k2);
id F4(k1?,k2?,p4?,p5?) = (Dsm2)^2*(S(k1,k2)-MU(k1,k1)-MU(k1,k2)-MU(k2,k2))*IS(p4,p5)*MU(k1,k1)*MU(k2,k2)/2;

id trp(p1?,p2?,p3?,p4?) = B(p1,p2)*A(p2,p3)*B(p3,p4)*A(p4,p1);
id trm(p1?,p2?,p3?,p4?) = A(p1,p2)*B(p2,p3)*A(p3,p4)*B(p4,p1);
id dot(k1?,ww(p1?,p2?,p3?)) = (AB(p2,p3,p1)*AB(p1,k1,p2)-AB(p1,p3,p2)*AB(p2,k1,p1))*IS(p1,p2)/2;

argument MU;
id p1=0;
id p2=0;
id p3=0;
id p4=0;
id p5=0;
endargument;

id MU(k1,k1) = mu11;
id MU(-k1,-k1) = mu11;
id MU(k1,k2) = mu12;
id MU(-k1,-k2) = mu12;
id MU(k2,k1) = mu12;
id MU(-k2,-k1) = mu12;
id MU(k2,k2) = mu22;
id MU(-k2,-k2) = mu22;
id Dsm2 = Ds-2;
