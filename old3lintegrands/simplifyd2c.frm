#-
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];
auto S x,p,e,q,P,m,k,w,d;
S [k.3],[q.2],[k.w],[q.w];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM;
auto CF TMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb;

set ps:p1,...,p4;
set mps:P;

#include- ../lib/factorise.h

#procedure toS14
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);
argument NUM,INV;
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);
endargument;
#endprocedure

off stats;
.global

#include- dbox.d2c.h

denominators INV;
#call sortINV
.sort
#call ISortCollect

id INV(S(p1,p2)+S(p1,p3)) = -IS(p2,p3);
id INV(S(p1,p2)+S(p2,p3)) = -IS(p1,p3);
id INV(S(p1,p3)+S(p2,p3)) = -IS(p1,p2);
#call cancel

AB S,IS;
.sort
collect NUM;
#call SortCollect

id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel

id NUM(x?) = x;
id S(p2,p3) = -S(p1,p3)-S(p1,p2);
#call cancel
AB S,IS;
.sort
collect NUM;
#call SortCollect
id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel
#call toS14
.sort

#do sol=1,6
#do pow=-4,4
#write <dbox.d2c.final.mpl> "d'sol'{'pow'+4} := ds['sol','pow']:"
#enddo
#enddo

#define csset "{c000,c100,c010,c200,c110,c020,c300,c210,c120,c030,c400,c310,c410,c040,c130,c140,c001,c002,c101,c012,c011,c102,c201,c022,c111,c112,c301,c032,c211,c122,c311,c132}"
#define i "1"
#do c='csset'
#write <dbox.d2c.final.h>   "L 'c' = %e" 'c'
#write <dbox.d2c.final.mpl> "csolA['i'] := %E:" 'c'
*#write <dbox.d2c.final.mpl> "'c' := %E:" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,6
#write <dbox.d2c.final.h>   "L zero'k' = %e" zero'k'
#write <dbox.d2c.final.mpl> "zeroA['k'] := %E:" zero'k'
*#write <dbox.d2c.final.mpl> "zero'k' := %E:" zero'k'
#enddo

#do k=1,32
#write <dbox.d2c.final.mpl> "printf(\"'k',\%A = \%.16Ze\\\n\",cs['k'],csol['k']);"
#enddo
#do k=1,6
#write <dbox.d2c.final.mpl> "printf(\"zero'k' = \%.16Ze\\\n\",zeroA['k']);"
#enddo

format mathematica;
#define i "1"
#do c='csset'
#write <dbox.d2c.final.m>   "'c' = %e" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,6
#write <dbox.d2c.final.m>   "zero'k' = %e" zero'k'
#enddo

format 150;
B INV;
print[];
.store

#include- xbox.d2c.h

denominators INV;

id S(p3,p2) = S(p2,p3);
id S(p3,p1) = S(p1,p3);
id S(p2,p1) = S(p1,p2);
argument INV;
id S(p3,p2) = S(p2,p3);
id S(p3,p1) = S(p1,p3);
id S(p2,p1) = S(p1,p2);
endargument;

#call sortINV
.sort
#call ISortCollect

id INV(S(p1,p2)+S(p1,p3)) = -IS(p2,p3);
id INV(S(p1,p2)+S(p2,p3)) = -IS(p1,p3);
id INV(S(p1,p3)+S(p2,p3)) = -IS(p1,p2);
#call cancel

AB S,IS;
.sort
collect NUM;
#call SortCollect

id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel

id NUM(x?) = x;
id S(p2,p3) = -S(p1,p3)-S(p1,p2);
#call cancel
AB S,IS;
.sort
collect NUM;
#call SortCollect
id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel
#call toS14
.sort


#write <xbox.d2c.final.mpl> "csolA := Vector(38):"
#write <xbox.d2c.final.mpl> "zeroA := Vector(10):"
#do sol=1,8
#do pow=0,6
#write <xbox.d2c.final.mpl> "d'sol''pow' := ds['sol','pow']:"
#enddo
#enddo

#define csset "{c000,c010,c020,c030,c040,c050,c060,c100,c110,c120,c200,c210,c220,c300,c310,c320,c400,c410,c420,c001,c011,c101,c111,c201,c211,c301,c311,c002,c012,c022,c032,c042,c052,c102,c112,c122,c132,c142}"
#define i "1"
#do c='csset'
#write <xbox.d2c.final.h>   "L 'c' = %e" 'c'
#write <xbox.d2c.final.mpl> "csolA['i'] := %E:" 'c'
*#write <xbox.d2c.final.mpl> "'c' := %E:" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,10
#write <xbox.d2c.final.h>   "L zero'k' = %e" zero'k'
#write <xbox.d2c.final.mpl> "zeroA['k'] := %E:" zero'k'
*#write <xbox.d2c.final.mpl> "zero'k' := %E:" zero'k'
#enddo

#do k=1,38
#write <xbox.d2c.final.mpl> "printf(\"'k',\%A = \%.16Ze\\\n\",cs['k'],csol['k']);"
#enddo
#do k=1,10
#write <xbox.d2c.final.mpl> "printf(\"zero'k' = \%.16Ze\\\n\",zeroA['k']);"
#enddo

format mathematica;
#define i "1"
#do c='csset'
#write <xbox.d2c.final.m>   "'c' = %e" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,10
#write <xbox.d2c.final.m>   "zero'k' = %e" zero'k'
#enddo

format 150;
B INV;
print[];
.store

#include- pbox.d2c.h

denominators INV;

id S(p3,p2) = S(p2,p3);
id S(p3,p1) = S(p1,p3);
id S(p2,p1) = S(p1,p2);
id S(p1,p4) = S(p2,p3);
id S(p4,p1) = S(p2,p3);
argument INV;
id S(p3,p2) = S(p2,p3);
id S(p3,p1) = S(p1,p3);
id S(p2,p1) = S(p1,p2);
id S(p1,p4) = S(p2,p3);
id S(p4,p1) = S(p2,p3);
endargument;

#call sortINV
.sort
#call ISortCollect

id INV(S(p1,p2)+S(p1,p3)) = -IS(p2,p3);
id INV(S(p1,p2)+S(p2,p3)) = -IS(p1,p3);
id INV(S(p1,p3)+S(p2,p3)) = -IS(p1,p2);
#call cancel

AB S,IS;
.sort
collect NUM;
#call SortCollect

id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel

id NUM(x?) = x;
id S(p2,p3) = -S(p1,p3)-S(p1,p2);
#call cancel
AB S,IS;
.sort
collect NUM;
#call SortCollect
id NUM(S(p1,p2)+S(p1,p3)) = -S(p2,p3);
id NUM(S(p1,p2)+S(p2,p3)) = -S(p1,p3);
id NUM(S(p1,p3)+S(p2,p3)) = -S(p1,p2);
#call cancel
#call toS14
.sort

#write <pbox.d2c.final.mpl> "csolA := Vector(20):"
#do sol=1,2
#do t1=0,3
#do t2=0,3
#write <pbox.d2c.final.mpl> "d'sol''t1''t2' := ds['sol','t1','t2']:"
#enddo
#enddo
#enddo

#define csset "{c000,c100,c010,c200,c110,c020,c300,c210,c120,c030,c001,c101,c011,c201,c111,c021,c301,c211,c121,c031}"
#define i "1"
#do c='csset'
#write <pbox.d2c.final.h>   "L 'c' = %e" 'c'
#write <pbox.d2c.final.mpl> "csolA['i'] := %E:" 'c'
*#write <pbox.d2c.final.mpl> "'c' := %E:" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,20
#write <pbox.d2c.final.mpl> "printf(\"'k',\%A = \%.16Ze\\\n\",cs['k'],csol['k']);"
#enddo

format mathematica;
#define i "1"
#do c='csset'
#write <pbox.d2c.final.m>   "'c' = %e" 'c'
#redefine i "{'i'+1}"
#enddo

format 150;
B NUM;
print+s;
.end
