#-
#include- 2to3tools.prc

#define topo "db3315L"

#write <basis/'topo'.invert.mpl> "Invert_3315L := proc(p)"
#write <basis/'topo'.invert.mpl> "  local p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,"
#write <basis/'topo'.invert.mpl> "         Clbl,ReduceMatrix,Dvec;"
#write <basis/'topo'.invert.mpl> "  p1 := p[1];"
#write <basis/'topo'.invert.mpl> "  p2 := p[2];"
#write <basis/'topo'.invert.mpl> "  p3 := p[3];"
#write <basis/'topo'.invert.mpl> "  p4 := p[4];"
#write <basis/'topo'.invert.mpl> "  p5 := p[5];"
#write <basis/'topo'.invert.mpl> "  s12 := S(p1,p2);"
#write <basis/'topo'.invert.mpl> "  s13 := S(p1,p3);"
#write <basis/'topo'.invert.mpl> "  s14 := S(p1,p4);"
#write <basis/'topo'.invert.mpl> "  s15 := S(p1,p5);"
#write <basis/'topo'.invert.mpl> "  s23 := S(p2,p3);"
#write <basis/'topo'.invert.mpl> "  s24 := S(p2,p4);"
#write <basis/'topo'.invert.mpl> "  s25 := S(p2,p5);"
#write <basis/'topo'.invert.mpl> "  s34 := S(p3,p4);"
#write <basis/'topo'.invert.mpl> "  s35 := S(p3,p5);"
#write <basis/'topo'.invert.mpl> "  s45 := S(p4,p5);"
#write <basis/'topo'.invert.mpl> "  TR5:=tr5(p1,p2,p3,p4);"

off stats;

.global

G basis41(nu1) = MOM(p1,nu1);
G basis42(nu1) = MOM(p2,nu1);
G basis43(nu1) = MOM(p3,nu1);
G basis44(nu1) = MOM(p5,nu1);
.store

G x14 = 2*MOM(k1,nu1)*basis44(nu1);
G x13 = 2*MOM(k1,nu1)*basis43(nu1);
G x22 = 2*MOM(k2,nu1)*basis42(nu1);
G x21 = 2*MOM(k2,nu1)*basis41(nu1);
G xmu11 = mu11;
G xmu12 = mu12;
G xmu22 = mu22;

L zero1 = MOM(k1,nu1)*MOM(k1,nu1)-mu11;
L zero2 = MOM(k1,nu1)*MOM(p1,nu1);
L zero3 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)-s12/2;
L zero4 = MOM(k2,nu1)*MOM(p4,nu1)+MOM(k2,nu1)*MOM(p5,nu1)-s45/2;
L zero5 = MOM(k2,nu1)*MOM(p5,nu1);
L zero6 = MOM(k2,nu1)*MOM(k2,nu1)-mu22;
L zero7 = (MOM(k1,nu1)+MOM(k2,nu1))^2-mu11-mu12-mu22;

#call subsLmom(1,'topo')

id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id MOM(p1?,nu1?)*AB(p2?,nu1?,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?,p2?)*AB(p3?,nu1?,p4?) = -2*A(p1,p3)*B(p2,p4);
id NUM(x?) = x;

#call cancel
#call simplify5
id tr5 = TR5;

*** uncomment these lines to convert to mom. twistor variables
*#call changevars
*#call simplify5new

format 150;
print+s;
.store

G Delta = Basis'topo';

#include- basis/'topo'.basis.h

id Power(x1?,x2?) = x1^x2;

id mu11 = xmu11;
id mu12 = xmu12;
id mu22 = xmu22;
.sort

B tau1,tau2,tau3,tau4;
print[];
.sort

#$nc = 1;
B cc;
.sort
keep brackets;
id cc(?x) = cc($nc,?x);
$nc=$nc+1;
.sort
#write "{'$nc'-1} integrand coefficients"
#write <basis/'topo'.invert.mpl> "Clbl:=Vector(160):"
#do cc=1,{'$nc'-1}
id cc('cc',?x$clbl) = cc('cc',?x);
.sort
#write <basis/'topo'.invert.mpl> "  Clbl['cc']:=cs('$clbl'):"
#enddo

.store

#write <basis/'topo'.invert.mpl> "ReduceMatrix:=Matrix({'$nc'-1},{'$nc'-1}):"
#write <basis/'topo'.invert.mpl> "Dvec:=Vector(160):"
#define dd "0"

#do pow1=0,6
#do pow2=0,6
#do pow3=0,6
#do pow4=0,6

#$nonzero=0;
L tmpRow = Delta;

#do pp=1,4
if(count(tau'pp',1)!=`pow`pp'');
discard;
endif;
id tau`pp'=1;
id 1/tau`pp'=1;
#enddo

.sort
$nonzero=$nonzero+1;
.sort
hide;

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <basis/'topo'.invert.mpl> "Dvec['dd']:=ds('pow1','pow2','pow3','pow4'):"

#do cc=1,{'$nc'-1}
  L tmpEl'cc' = tmpRow;
  id cc('cc',?x$clbl) = 1;
  id cc(?x)=0;
  .sort
  format maple;
  #write <basis/'topo'.invert.mpl> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
  .sort
  hide;
#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo
#enddo

#write <basis/'topo'.invert.mpl> "  return ReduceMatrix, Dvec, Clbl;"
#write <basis/'topo'.invert.mpl> "end;"

.end
