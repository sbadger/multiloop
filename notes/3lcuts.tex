% */ vim: set tw=100: */
\documentclass[11pt,a4paper]{article}

\usepackage{jheppub}
\usepackage{psfrag}
\usepackage{slashed}
\usepackage{cancel}
\usepackage{lscape}

\def\la{\langle}
\def\ra{\rangle}

\definecolor{mygreen}{rgb}{0,0.7,0}
\def\simon#1{\textcolor{mygreen}{{\bf\tt [SB: #1]}}}

%%% spinor products %%%

\def\A#1#2{\la#1#2\ra}
\def\B#1#2{[#1#2]}
\def\AB#1#2#3{\la#1|#2|#3]}
\def\BA#1#2#3{[#1|#2|#3\ra}
\def\AA#1#2#3{\la#1|#2|#3\ra}
\def\BB#1#2#3{[#1|#2|#3]}
\def\wh#1{\widehat#1}
\DeclareMathOperator{\tr}{\rm tr}
\def\trm{\tr_-}
\def\trp{\tr_+}
\def\MP#1#2{(#1\cdot#2)}

\def\eps{\epsilon}
\def\fl#1{#1^\flat}
\def\tl#1{\tilde{#1}}
\def\qb{\bar{q}}
\def\sb{\bar{s}}
\def\Sb{\bar{S}}
\def\MHVb{$\overline{\rm MHV}$}

\def\boxX{$\xcancel{\rm\bf box}$}

\def\fl#1{{#1^{\flat}}}
\def\flm#1{{#1^{\flat,\mu}}}
\def\kf#1{{\fl{K_{#1}}}}
\def\kfm#1{{\flm{K_{#1}}}}

\def\ulim#1{\underset{#1}{\lim}}

\preprint{}

\title{Three-Loop Generalised Unitarity Cuts}

\author[a]{Simon Badger}
\author[a]{Hjalte Frellesvig}
\author[a]{Yang Zhang}
\affiliation[a]{
Niels Bohr International Academy and Discovery Center, The Niels Bohr Institute,\\%
University of Copenhagen, Blegdamsvej 17, DK-2100 Copenhagen, Denmark}
\emailAdd{badger@nbi.dk,hjf@nbi.dk,zhang@nbi.dk}

\abstract{
A collection of on-shell solutions for three-loop cuts. These solutions have all
been generated automatically using {\tt Macaulay2}.
\vspace{\baselineskip}\\
Draft from \today
}

\keywords{}
\notoc

\begin{document}

\maketitle
\flushbottom
\section{Notation}

In these notes we will make use of the following basis for the loop momenta
construcuted from massless momenta $p$ and $q$:
\begin{align}
  B_1^\mu({\bf v};p,q) &=
      v_1 \, p^\mu
    + v_2 \, q^\mu
    + v_3 \, \frac{1}{2}\AB{p}{\gamma^\mu}{q}
    + v_4 \, \frac{1}{2}\AB{q}{\gamma^\mu}{p}
  \label{eq:loop-moms}
\end{align}
For box-type topologies it is convenient to define
\begin{align}
  B_2^\mu({\bf v};p,q,r) &=
      v_1 \, p^\mu
    + v_2 \, q^\mu
    + v_3 \, \frac{\B{r}{p}}{2\B{r}{q}}\AB{p}{\gamma^\mu}{q}
    + v_4 \, \frac{\B{r}{q}}{2\B{r}{p}}\AB{q}{\gamma^\mu}{p}
  \label{eq:loop-moms}
\end{align}

Massless projection vectors, or ``flattened'' momenta are defined by
\begin{align}
  \kfm{i}(j) = P_i^\mu - \frac{P_i^2}{\gamma_{ij}} p_j^\mu,
  \label{eq:flatdef}
\end{align}
for some massless momentum $p_j$ with $\gamma_{ij} = 2P_i\cdot p_j$.

\section{Planar Deca-Cuts}

\subsection{Massless box$|$box$|$box}

\begin{figure}[h!]
  \begin{tabular}{cc}
    \begin{minipage}{0.6\textwidth}
      \psfrag{1}{\small$p_1$}
      \psfrag{2}{\small$p_2$}
      \psfrag{3}{\small$p_3$}
      \psfrag{4}{\small$p_4$}
      \psfrag{l1}{\small$l_1$}
      \psfrag{l2}{\small$l_2$}
      \psfrag{l3}{\small$l_3$}
      \psfrag{l4}{\small$l_4$}
      \psfrag{l5}{\small$l_5$}
      \psfrag{l6}{\small$l_6$}
      \psfrag{l7}{\small$l_7$}
      \psfrag{l8}{\small$l_8$}
      \psfrag{l9}{\small$l_9$}
      \psfrag{l10}{\small$l_{10}$}
      \includegraphics[width=\textwidth]{pics/boxboxbox.eps}
    \end{minipage}
%    &
    \begin{minipage}{0.4\textwidth}
      \begin{align}
        l_1 = k_1 + p_1 \nonumber\\
        l_2 = k_1 \nonumber\\
        l_3 = k_1 - p_2 \nonumber\\
        l_4 = k_2 \nonumber\\
        l_5 = k_3 + p_3 \\
        l_6 = k_3 \nonumber\\
        l_7 = k_3 - p_4 \nonumber\\
        l_8 = k_2 + p_1 + p_2 \nonumber\\
        l_9 = k_2 - k_1 + p_2 \nonumber\\
        l_{10} = k_2 - k_3 - p_3 \nonumber
        \label{eq:boxboxboxProps}
      \end{align}
    \end{minipage}
  \end{tabular}
  \caption{The momentum flow and propagator definitions for the three-loop planar triple box
  topology.}
  \label{fig:boxboxbox}
\end{figure}

\begin{equation}
  k_1 = B_2({\bf a};1,2,3) \text{ and } k_2 = B_2({\bf b};3,4,1) \text{ and } k_3 = B_2({\bf b};3,4,1).
\end{equation}
There are fourteen solutions for $\bf a$,$\bf b$ and $\bf c$:
\begin{align}
%boxboxbox-A
\begin{array}{|c|c|c|c|c|}
\hline
soln. & a_1 & a_2 & a_3 & a_4\\
\hline
1 & 0 & 0 & 0 & 
\frac{\tau_2}{(\tau_{2}+s_{12})}-
\frac{\tau_2}{\tau_1(\tau_{2}+s_{12})}\\
\hline
2 & 0 & 0 & \tau_1 & 0\\
\hline
3 & 0 & 0 & -
\frac{(s_{12}+s_{13}\tau_{2})}{\tau_2s_{13}} & 0\\
\hline
4 & 0 & 0 & -
\frac{(-1+\tau_{1})}{\tau_1} & 0\\
\hline
5 & 0 & 0 & 0 & 1\\
\hline
6 & 0 & 0 & 0 & \tau_1\\
\hline
7 & 0 & 0 & 0 & \tau_1\\
\hline
8 & 0 & 0 & \tau_1 & 0\\
\hline
9 & 0 & 0 & \tau_1 & 0\\
\hline
10 & 0 & 0 & 
\frac{(-s_{12}\tau_{1}+s_{13}\tau_{2}+s_{14}\tau_{1}\tau_{2})}{\tau_1\tau_2s_{13}} & 0\\
\hline
11 & 0 & 0 & 
\frac{s_{14}}{s_{13}} & 0\\
\hline
12 & 0 & 0 & 0 & -
\frac{\tau_2s_{13}}{(s_{13}+s_{14}+s_{14}\tau_{2})}\\
\hline
13 & 0 & 0 & 0 & -
\frac{\tau_1s_{13}}{(s_{12}+s_{14}+s_{14}\tau_{1})}\\
\hline
14 & 0 & 0 & 0 & \tau_1\\
\hline
\end{array}
\end{align}
\begin{landscape}
\begin{align}
%boxboxbox-B
\begin{array}{|c|c|c|c|c|}
\hline
soln. & b_1 & b_2 & b_3 & b_4\\
\hline
1 & -1+\tau_1 & -\tau_1 & -
\frac{\tau_1(\tau_{2}+s_{12})}{\tau_2} & 
\frac{\tau_1\tau_2}{(\tau_{2}+s_{12})}-
\frac{\tau_2}{(\tau_{2}+s_{12})}\\
\hline
2 & 0 & -1 & -
\frac{(s_{12}+s_{13}\tau_{2})}{\tau_2s_{13}} & 0\\
\hline
3 & 
\frac{\tau_1(s_{12}+s_{13}\tau_{2})}{\tau_2s_{13}} & -
\frac{(s_{12}\tau_{1}+s_{13}\tau_{2}+s_{13}\tau_{1}\tau_{2})}{\tau_2s_{13}} & -
\frac{(s_{12}+s_{13}\tau_{2})(s_{12}\tau_{1}+s_{13}\tau_{2}+s_{13}\tau_{1}\tau_{2})}{\tau_2^2s_{13}^2} & \tau_1\\
\hline
4 & (-1+\tau_{1}) & -\tau_1 & -(-1+\tau_{1}) & \tau_1\\
\hline
5 & (-1+\tau_{1}) & -\tau_1 & -(-1+\tau_{1}) & \tau_1\\
\hline
6 & 0 & -1 & 0 & 1\\
\hline
7 & 0 & -1 & 0 & \tau_2\\
\hline
8 & 0 & -1 & \tau_2 & 0\\
\hline
9 & 0 & -1 & 
\frac{s_{14}}{s_{13}} & 0\\
\hline
10 & -
\frac{(-s_{12}\tau_{1}+s_{13}\tau_{2}+s_{14}\tau_{1}\tau_{2})}{\tau_2s_{13}} & 
\frac{\tau_1(-s_{12}+s_{14}\tau_{2})}{\tau_2s_{13}} & -
\frac{(-s_{12}+s_{14}\tau_{2})(-s_{12}\tau_{1}+s_{13}\tau_{2}+s_{14}\tau_{1}\tau_{2})}{\tau_2^2s_{13}^2} & \tau_1\\
\hline
11 & 
\frac{(s_{13}\tau_{1}-s_{14})}{s_{14}} & -
\frac{\tau_1s_{13}}{s_{14}} & \tau_1 & -
\frac{s_{13}(s_{13}\tau_{1}-s_{14})}{s_{14}^2}\\
\hline
12 & -
\frac{(-s_{12}\tau_{1}+s_{13}\tau_{2}+s_{14}\tau_{1}\tau_{2})}{\tau_2s_{13}} & 
\frac{\tau_1(-s_{12}+s_{14}\tau_{2})}{\tau_2s_{13}} & -
\frac{(-s_{12}+s_{14}\tau_{2})(-s_{12}\tau_{1}+s_{13}\tau_{2}+s_{14}\tau_{1}\tau_{2})}{\tau_2^2s_{13}^2} & \tau_1\\
\hline
13 & -
\frac{\tau_1s_{14}}{s_{13}} & -
\frac{(s_{13}-s_{14}\tau_{1})}{s_{13}} & 
\frac{s_{14}(s_{13}-s_{14}\tau_{1})}{s_{13}^2} & \tau_1\\
\hline
14 & 0 & -1 & 0 & -
\frac{(s_{12}+s_{13}\tau_{2})}{\tau_2s_{14}}\\
\hline
\end{array}
\end{align}
\end{landscape}
\begin{align}
%boxboxbox-C
\begin{array}{|c|c|c|c|c|}
\hline
soln. & c_1 & c_2 & c_3 & c_4\\
\hline
1 & 0 & 0 & 0 & -
\frac{(-\tau_{2}+s_{13})}{s_{14}}\\
\hline
2 & 0 & 0 & 0 & 
\frac{s_{13}(-1+\tau_{2})}{s_{14}}\\
\hline
3 & 0 & 0 & 0 & 
\frac{s_{13}(-1+\tau_{2})}{s_{14}}\\
\hline
4 & 0 & 0 & 0 & \tau_2\\
\hline
5 & 0 & 0 & 0 & \tau_2\\
\hline
6 & 0 & 0 & 0 & \tau_2\\
\hline
7 & 0 & 0 & 0 & 1\\
\hline
8 & 0 & 0 & 
\frac{s_{14}}{s_{13}} & 0\\
\hline
9 & 0 & 0 & \tau_2 & 0\\
\hline
10 & 0 & 0 & 
\frac{s_{14}(1+\tau_{2})}{s_{13}} & 0\\
\hline
11 & 0 & 0 & \tau_2 & 0\\
\hline
12 & 0 & 0 & 
\frac{s_{14}(1+\tau_{2})}{s_{13}} & 0\\
\hline
13 & 0 & 0 & 
\frac{s_{13}(1+\tau_{1})}{s_{14}} & 0\\
\hline
14 & 0 & 0 & (-1+\tau_{2}) & 0\\
\hline
\end{array}
\end{align}


\subsection{Massless ``tennis-court"}

\begin{figure}[h!]
  \begin{tabular}{cc}
    \begin{minipage}{0.5\textwidth}
      \psfrag{1}{\small$p_1$}
      \psfrag{2}{\small$p_2$}
      \psfrag{3}{\small$p_3$}
      \psfrag{4}{\small$p_4$}
      \psfrag{l1}{\small$l_1$}
      \psfrag{l2}{\small$l_2$}
      \psfrag{l3}{\small$l_3$}
      \psfrag{l4}{\small$l_4$}
      \psfrag{l5}{\small$l_5$}
      \psfrag{l6}{\small$l_6$}
      \psfrag{l7}{\small$l_7$}
      \psfrag{l8}{\small$l_8$}
      \psfrag{l9}{\small$l_9$}
      \psfrag{l10}{\small$l_{10}$}
      \includegraphics[width=\textwidth]{pics/tcourt.eps}
    \end{minipage}
    &
    \begin{minipage}{0.4\textwidth}
      \begin{align}
        l_1 = k_1 + p_1 \nonumber\\
        l_2 = k_1 \nonumber\\
        l_3 = k_1 - p_2 \nonumber\\
        l_4 = k_2 \nonumber\\
        l_5 = k_2 - k_3 - p_3 \\
        l_6 = k_2 - k_3 - p_3 - p_4 \nonumber\\
        l_7 = k_2 - k_3 - k_1 + p_2 \nonumber\\
        l_8 = k_2 + p_1 + p_2 \nonumber\\
        l_9 = k_2 - k_1 + p_2 \nonumber\\
        l_{10} = k_3 \nonumber
        \label{eq:tcourtProps}
      \end{align}
    \end{minipage}
  \end{tabular}
  \caption{The momentum flow and propagator definitions for the three-loop planar tennis court
  topology.}
  \label{fig:tcourt}
\end{figure}

\begin{equation}
  k_1 = B_2({\bf a};1,2,3) \text{ and } k_2 = B_2({\bf b};3,4,1) \text{ and } k_3 = B_2({\bf b};1,2,3).
\end{equation}
There are sixteen solutions for $\bf a$,$\bf b$ and $\bf c$:

\begin{align}
%tcourt-A
\begin{array}{|c|c|c|c|c|}
  \hline
  soln. & a_1 & a_2 & a_3 & a_4\\
  \hline
1 & 0 & 0 & \tau_{1} & 0\\
  \hline
2 & 0 & 0 & \tau_{1} & 0\\
  \hline
3 & 0 & 0 & 
\frac{s_{14}}{s_{13}} & 0\\
  \hline
4 & 0 & 0 & 
\frac{s_{14}}{s_{13}} & 0\\
  \hline
5 & 0 & 0 & 
\frac{(s_{12}\tau_{1}+s_{14})}{\tau_2s_{12}}-
\frac{(s_{13}\tau_{1}-s_{14})}{\tau_2s_{12}(-1+\tau_{2})} & 0\\
  \hline
6 & 0 & 0 & -
\frac{s_{14}(-1+\tau_{1})}{(-s_{12}-s_{14}+s_{14}\tau_{1})} & 0\\
  \hline
7 & 0 & 0 & -
\frac{(-s_{12}\tau_{1}+s_{14})}{\tau_2s_{12}}+
\frac{(s_{13}\tau_{1}+s_{14})}{\tau_2s_{12}(1+\tau_{2})} & 0\\
  \hline
8 & 0 & 0 & 
\frac{(-\tau_{2}+\tau_{1})}{\tau_2} & 0\\
  \hline
9 & 0 & 0 & 0 & -
\frac{(s_{12}+s_{13}\tau_{1})}{\tau_1s_{14}}\\
  \hline
10 & 0 & 0 & 0 & 
\frac{s_{14}}{\tau_1s_{13}}\tau_{2}\\
  \hline
11 & 0 & 0 & 0 & -
\frac{(-s_{12}\tau_{2}+s_{13})}{\tau_1s_{12}}+
\frac{s_{13}(1+\tau_{2})}{\tau_1s_{12}(1+\tau_{1})}\\
  \hline
12 & 0 & 0 & 0 & 1\\
  \hline
13 & 0 & 0 & 0 & \tau_{1}\\
  \hline
14 & 0 & 0 & 0 & 1\\
  \hline
15 & 0 & 0 & 0 & \tau_{1}\\
  \hline
16 & 0 & 0 & 0 & 
\frac{(s_{12}\tau_{2}+s_{13})}{\tau_1s_{12}}-
\frac{s_{13}(-1+\tau_{2})}{\tau_1s_{12}(-1+\tau_{1})}\\
  \hline
\end{array}
\end{align}
\begin{landscape}
\begin{align}
%tcourt-B
\begin{array}{|c|c|c|c|c|}
  \hline
  soln. & b_1 & b_2 & b_3 & b_4\\
  \hline
1 & 1 & 0 & 
\frac{s_{14}}{s_{13}} & 0\\
  \hline
2 & 
\frac{(s_{13}\tau_{2}+s_{14})}{s_{14}} & 0 & 
\frac{(s_{13}\tau_{2}+s_{14})}{s_{13}} & 0\\
  \hline
3 & 
\frac{(s_{13}\tau_{3}+s_{14}-s_{14}\tau_{2})}{s_{14}} & 0 & \tau_{1} & 0\\
  \hline
4 & 1 & 0 & \tau_{1} & 0\\
  \hline
5 & -
\frac{s_{13}(\tau_{2}+\tau_{1})}{\tau_2s_{12}} & 0 & -
\frac{s_{14}(\tau_{2}+\tau_{1})}{\tau_2s_{12}} & 0\\
  \hline
6 & 1 & 0 & 0 & \tau_{1}\\
  \hline
7 & -
\frac{(-s_{12}\tau_{2}-s_{12}\tau_{2}^2-s_{12}\tau_{1}\tau_{2}+s_{14}\tau_{2}+s_{14}\tau_{1})}{\tau_2s_{12}} & 0 & 0 & 
\frac{(-s_{12}\tau_{2}+s_{14})(s_{13}\tau_{1}-s_{14}\tau_{2})}{\tau_2s_{12}s_{14}}\\
  \hline
8 & (1+\tau_{1}) & 0 & 0 & -
\frac{(1+\tau_{1})(s_{12}\tau_{2}+s_{13}\tau_{1})}{\tau_1s_{14}}\\
  \hline
9 & 1 & 0 & (-1+\tau_{1}) & 0\\
  \hline
10 & 
\frac{(s_{13}^2\tau_{1}+s_{13}s_{14}+s_{14}^2\tau_{2})}{s_{13}s_{14}} & 0 & 
\frac{(s_{12}s_{13}s_{14}\tau_{1}\tau_{2}+s_{13}^3\tau_{1}^2+s_{13}^2s_{14}\tau_{1}-s_{13}s_{14}^2\tau_{2}+2s_{13}s_{14}^2\tau_{1}\tau_{2}-s_{14}^3\tau_{2}^2)}{s_{13}(s_{12}^2\tau_{1}+2s_{12}s_{14}\tau_{1}+s_{14}^2\tau_{2}+s_{14}^2\tau_{1})} & 0\\
  \hline
11 & 
\frac{(s_{12}s_{13}\tau_{1}+s_{12}s_{13}\tau_{1}^2+s_{12}s_{14}\tau_{1}\tau_{2}-s_{13}s_{14}\tau_{1}-s_{14}^2\tau_{2})}{\tau_1s_{12}s_{13}} & 0 & -
\frac{s_{14}(-\tau_{2}+\tau_{1})(-s_{12}\tau_{1}+s_{14})}{\tau_1s_{12}s_{13}} & 0\\
  \hline
12 & (1+\tau_{3}+\tau_{2}) & 0 & 0 & \tau_{1}\\
  \hline
13 & (1+\tau_{3}) & 0 & 0 & (1+\tau_{3})\\
  \hline
14 & 1 & 0 & 0 & \tau_{1}\\
  \hline
15 & 1 & 0 & 0 & 1\\
  \hline
16 & -
\frac{(s_{13}\tau_{1}+s_{14}\tau_{2})}{\tau_1s_{12}} & 0 & 0 & -
\frac{(s_{13}\tau_{1}+s_{14}\tau_{2})}{\tau_1s_{12}}\\
  \hline
\end{array}
\end{align}
\end{landscape}
\begin{align}
%tcourt-C
\begin{array}{|c|c|c|c|c|}
  \hline
  soln. & c_1 & c_2 & c_3 & c_4\\
  \hline
1 & 
\frac{s_{13}}{s_{14}}\tau_{2} & 0 & \tau_{2} & 0\\
  \hline
2 & 0 & -
\frac{s_{13}}{s_{14}}\tau_{2} & \tau_{2} & 0\\
  \hline
3 & \tau_{2} & -
\frac{s_{13}}{s_{14}}\tau_{3} & \tau_{3} & -
\frac{s_{13}}{s_{14}}\tau_{2}\\
  \hline
4 & 
\frac{s_{13}}{s_{14}}\tau_{3} & \tau_{2} & \tau_{3} & 
\frac{s_{13}}{s_{14}}\tau_{2}\\
  \hline
5 & \tau_{2} & 
\frac{(-1+\tau_{2})}{\tau_2}\tau_{1} & \tau_{1} & (-1+\tau_{2})\\
  \hline
6 & 
\frac{s_{13}(-1+\tau_{1})}{s_{14}} & -
\frac{s_{13}(-1+\tau_{1})^2}{(-s_{12}-s_{14}+s_{14}\tau_{1})} & -
\frac{s_{13}(-1+\tau_{1})^2}{(-s_{12}-s_{14}+s_{14}\tau_{1})} & 
\frac{s_{13}(-1+\tau_{1})}{s_{14}}\\
  \hline
7 & -
\frac{(1+\tau_{2})}{\tau_2}\tau_{1} & -\tau_{2} & \tau_{1} & (1+\tau_{2})\\
  \hline
8 & -(-\tau_{2}+\tau_{1}) & -\tau_{2} & (-\tau_{2}+\tau_{1}) & \tau_{2}\\
  \hline
9 & 
\frac{s_{13}}{s_{14}}\tau_{2} & -
\frac{(s_{12}+s_{13}\tau_{1})}{\tau_1s_{14}}\tau_{2} & \tau_{2} & -
\frac{s_{13}(s_{12}+s_{13}\tau_{1})}{\tau_1s_{14}^2}\tau_{2}\\
  \hline
10 & -
\frac{s_{14}}{s_{13}}\tau_{2} & -
\frac{s_{13}}{s_{14}}\tau_{1} & \tau_{1} & \tau_{2}\\
  \hline
11 & -
\frac{s_{14}(1+\tau_{1})}{\tau_1s_{13}}\tau_{2} & -\tau_{1} & 
\frac{s_{14}(1+\tau_{1})}{s_{13}} & \tau_{2}\\
  \hline
12 & -\tau_{2} & -\tau_{3} & \tau_{2} & \tau_{3}\\
  \hline
13 & 0 & -\tau_{3} & 0 & \tau_{3}\\
  \hline
14 & \tau_{3} & \tau_{2} & \tau_{2} & \tau_{3}\\
  \hline
15 & \tau_{2} & 0 & 0 & \tau_{2}\\
  \hline
16 & \tau_{1} & 
\frac{s_{14}(-1+\tau_{1})}{\tau_1s_{13}}\tau_{2} & 
\frac{s_{14}(-1+\tau_{1})}{s_{13}} & \tau_{2}\\
  \hline
\end{array}
\end{align}

\end{document}
