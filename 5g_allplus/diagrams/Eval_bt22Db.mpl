interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.16e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_bt22Db:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 := tau5;

return k1,k2,mu11,mu12,mu22;
end:

xxdiag1  :=  1:
xxdiag2  :=  2:
xxdiag3  := -2:
xxdiag4  :=  2:
xxdiag5  :=  1:
xxdiag6  := -2:
xxdiag7  :=  2:
xxdiag8  :=  2:
xxdiag9  :=  1:
xxdiag10 := -2:
xxdiag11 :=  2:
xxdiag12 := -4:
xxdiag13 := -1:
xxdiag14 :=  1/2:
xxdiag15 :=  1:
xxdiag16 := -2:
xxdiag17 := -2:
xxdiag18 := -2:
xxdiag19 := -1:
xxdiag20 :=  2:
xxdiag21 := -1:
xxdiag22 := 1:
xxdiag23 := 2:
xxdiag24 := -2:
xxdiag25 := -4:
xxdiag26 := -1:
xxdiag27 := 1/2:

xxdiag37 := 1:
xxdiag38 := 2:
xxdiag39 := 2:
xxdiag40 := 4:
xxdiag41 := -1:
xxdiag42 := -2:
xxdiag43 := -1:
xxdiag44 := -2:

xxsub1:=1:
xxsub2:=1:
xxsub3:=1:
xxsub4:=1:
xxsub5:=1:
Ds:=Dsm2+2;

read("tmp/bt22Db.input.mpl"):
w := (AB(p5,p1,p4)*eta(p4,p5)-AB(p4,p1,p5)*eta(p5,p4))/2/s45:
w123 := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:
w543 := (AB(p4,p3,p5)*eta(p5,p4)-AB(p5,p3,p4)*eta(p4,p5))/2/s45:
w132 := (AB(p3,p2,p1)*eta(p1,p3)-AB(p1,p2,p3)*eta(p3,p1))/2/s13:
w534 := (AB(p3,p4,p5)*eta(p5,p3)-AB(p5,p4,p3)*eta(p3,p5))/2/s35:

w12p := (AB(p2,p3,p1)*eta(p1,p2)+AB(p1,p3,p2)*eta(p2,p1))/2/s12*I:
w12m := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:

w13p := (AB(p3,p2,p1)*eta(p1,p3)+AB(p1,p2,p3)*eta(p3,p1))/2/s13*I:
w13m := (AB(p3,p2,p1)*eta(p1,p3)-AB(p1,p2,p3)*eta(p3,p1))/2/s13:

w45p := (AB(p4,p3,p5)*eta(p5,p4)+AB(p5,p3,p4)*eta(p4,p5))/2/s45*I:
w45m := (AB(p4,p3,p5)*eta(p5,p4)-AB(p5,p3,p4)*eta(p4,p5))/2/s45:

w53p := (AB(p3,p4,p5)*eta(p5,p3)+AB(p5,p4,p3)*eta(p3,p5))/2/s35*I:
w53m := (AB(p3,p4,p5)*eta(p5,p3)-AB(p5,p4,p3)*eta(p3,p5))/2/s35:

#w132 + s12/s13*w123;
#w13m + s12/s13*w12m;
#w13p + s12/s13*w12p - 2*I*s23/s13*p1;

vv := s34/s45*p5-s35/s45*p4-w45m;
vv := eta(p5,p3)*AB(p3,p4,p5)/s45 - p3;

read("INT_5g+++++.mpl"):
read("FullINT_5g330.5L.mpl"):

#INT_bt22Db := proc(k1,k2,mu11,mu12,mu22);
#return Dsm2*(3*mu11*mu22+mu11*mu12+mu22*mu12) + 4*(mu12^2-4*mu11*mu22);
#end:

### check formula from arXiv:1310.1051
INT_3305L := proc(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
local s12,s23,s34,s45,s15,s35,s13,sk1k2,trp1345,trp1235,trp1245,w123,w453,ans;

s12:=S(p1,p2);
s23:=S(p2,p3);
s34:=S(p3,p4);
s45:=S(p4,p5);
s15:=S(p1,p5);
s13:=S(p1,p3);
s35:=S(p3,p5);
w123 := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:
w453 := (AB(p5,p3,p4)*eta(p4,p5)-AB(p4,p3,p5)*eta(p5,p4))/2/s45:

sk1k2 := S(k1+k2)-mu11-mu22-mu12;

trp1245 := AB(p1,p5,p4,p2,p1);
trp1235 := AB(p1,p5,p3,p2,p1);
trp1345 := AB(p1,p5,p4,p3,p1);

ans:= -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(

1/2*(trp1245-trp1345*trp1235/s13/s35)*(
    2*Dsm2*(mu11+mu22)*mu12
  + Dsm2^2*mu11*mu22*( 4*dot(k1,p3)*dot(k2,p3)+sk1k2*(s12+s45)+s12*s45 )/s12/s45
  )
+ Dsm2^2*mu11*mu22*(
    + sk1k2*s15
  + trp1235*( sk1k2/2/s35 - dot(k1,p3)/s12*(1-2*dot(k2,w453)/s35+(s12-s45)/s35/s45*(S(k2-p5)-mu22)))
  + trp1345*( sk1k2/2/s13 - dot(k2,p3)/s45*(1+2*dot(k1,w123)/s13+(s45-s12)/s13/s12*(S(k1-p1)-mu11)))
)
);

return ans;

end:

prefactor := I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):

for k1 from 0 to 3 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
ds(k1,k2,k3,k4,k5):=0:
od:
od:
od:
od:
od:

for th1 from 0 to 3 do:
for th2 from 0 to 3 do:
for th3 from 0 to 3 do:
for th4 from 0 to 3 do:
for th5 from 0 to 3 do:
printf("%d%d%d%d%d\n",th1,th2,th3,th4,th5);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/4);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/4);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/4);
t4 := 1.3*exp(2.*evalf(Pi)*I*th4/4);
t5 := 1.3*exp(2.*evalf(Pi)*I*th5/4);

k1,k2,mu11,mu12,mu22 := on_shell_bt22Db([p1,p2,p3,p4,p5],t1,t2,t3,t4,t5):
val := INT_bt22Db(k1,k2,mu11,mu12,mu22);
aval := aINT_bt22Db(k1,k2,mu11,mu12,mu22);
print(collect(expand(aval),{Dsm2,xxA,xxB}));
aval := INT_3305L(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
#zeroBCJ :=
#+ FullINT_bt22Db(k1,k2,mu11,mu12,mu22, p1,p2,p3,p4,p5)
#+ FullINT_bt22Db(k2,k1,mu22,mu12,mu11, p5,p4,p3,p2,p1)
#+ FullINT_bt22Db(-k1+p1+p2,k2,mu11,mu12,mu22, p2,p1,p3,p4,p5)
#+ FullINT_bt22Db(k2,-k1+p1+p2,mu22,mu12,mu11, p5,p4,p3,p1,p2)
#;

zeroBCJ :=
+ INT_3305L(k1,k2,mu11,mu12,mu22, p1,p2,p3,p4,p5)
+ INT_3305L(k2,k1,mu22,mu12,mu11, p5,p4,p3,p2,p1)
+ INT_3305L(-k1+p1+p2,k2,mu11,mu12,mu22, p2,p1,p3,p4,p5)
+ INT_3305L(k2,-k1+p1+p2,mu22,mu12,mu11, p5,p4,p3,p1,p2)
;

#print(s35 + 2*dot(k2,w45m) - (s12-s45)/s45*2*dot(k2,p5));
#print(s35 + 2*dot(k2,w45m) + (s12-s45)/s45*(S(k2-p5)-mu22));
#print(S(k2-vv)-mu22);
#print(S(k2-vv-p3)-mu22 + 2*dot(k2,p3));

print(collect(val,{Dsm2,xxA,xxB}));
print(collect(expand(aval),{Dsm2,xxA,xxB}));

print(collect(zeroBCJ,{Dsm2,xxA,xxB}));
quit;

for k1 from 0 to 3 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
  ds(k1,k2,k3,k4,k5) := expand(ds(k1,k2,k3,k4,k5) + evalf(val/t1^k1/t2^k2/t3^k3/t4^k4/t5^k5/(4.*4.*4.*4.*4.))):
od:
od:
od:
od:
od:

od:
od:
od:
od:
od:

read("../final/bt22Db.coeffvecs.mpl");

for k1 from 0 to 3 do:
for k2 from 0 to 3 do:
for k3 from 0 to 3 do:
for k4 from 0 to 3 do:
for k5 from 0 to 3 do:
 tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
 tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3,k4,k5))):
 tmp3 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
 tmp4 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2)):
 tmp5 := Re(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
 tmp6 := Im(coeff(ds(k1,k2,k3,k4,k5),Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("d_%d%d%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k1, k2, k3, k4, k5, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
#  lprint(k1,k2,k3,k4,expand(ds(k1,k2,k3,k4)));
od:
od:
od:
od:
od:

for k from 1 to 146 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 tmp5 := Re(coeff(Dvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Dvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint(k,expand(Dvec[k]));
od:

read("../bt22Db.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(146):
Cvec := iMM.Dvec:

for k from 1 to 146 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 tmp5 := Re(coeff(Cvec[k],Dsm2^2)):
 tmp6 := Im(coeff(Cvec[k],Dsm2^2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 if abs(tmp5)<1e-20 then; tmp5:=0: fi:
 if abs(tmp6)<1e-20 then; tmp6:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I) + Dsm2^2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4, tmp5, tmp6);
# lprint(k,Clbl[k],expand(Cvec[k]),";");
od:


