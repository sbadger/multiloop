#vim: set foldmethod=marker:
interface(quiet=true):
interface(displayprecision=10):

mypow:=proc(a,b); return a^b; end:

Digits:=40:
with(spinors):

genmoms([0,0,0,0]):

s12:=S(p1,p2);
s13:=S(p1,p3);
s14:=S(p1,p4);

       + 1/8*IA(p1,p2)^2*IA(p3,p4)^2*I*s12^3*s14^2
          + 1/8*IA(p1,p2)*IA(p1,p3)*IA(p2,p4)*IA(p3,p4)*I*s12^2*s13*s14^2
          - 1/2*IA(p1,p2)*IA(p1,p3)*IA(p2,p4)*IA(p3,p4)*I*s12^3*s13*s14
          - 1/2*IA(p1,p3)^2*IA(p2,p4)^2*I*s12^2*s13^2*s14
         ;

      - 1/2*IA(p1,p3)^2*IA(p2,p4)^2*I*s12^2*s13^2*s14
          + 1/8*IA(p1,p3)*IA(p1,p4)*IA(p2,p3)*IA(p2,p4)*I*s12*s13*s14^3
          - 1/2*IA(p1,p3)*IA(p1,p4)*IA(p2,p3)*IA(p2,p4)*I*s12^2*s13*s14^2
          + 1/8*IA(p1,p4)^2*IA(p2,p3)^2*I*s12*s14^4
         ;

quit;

tree[12]:= A(p1,p2)^3*IA(p2,p3)*IA(p3,p4)*IA(p4,p1):
tree[10]:= A(p1,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1):
tree[6] := A(p2,p3)^4*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p1):

#read("C7.planarD.INT.log"):
#C7_1_INT_D := proc(l1,l6,mu11,mu12,mu22,p1,p2,p3,p4) return 2*dot(l6,p1)- mu11 + mu12^2*2*dot(l1,p4); end:
read("C7_dboxD_INT.mpl"):
x1:=zerovec(1);

### planar double box ###

#{{{ OnShell_boxboxD
OnShell_boxboxD:=proc(p1,p2,p3,p4,sol,tau1,tau2,tau3,tau4)
local k1,k2,s12,s14,s13,cf1,cf2,bv1,bv2,MU11,MU12,MU22,b;
global l1,l2,l3,l4,l5,l6,l7,mu11,mu12,mu22;

s12:=S(p1,p2):
s13:=S(p1,p3):
s14:=S(p1,p4):

cf1:=[[1,0,(s14-tau1+tau3)/2/s14,(s14-tau1-tau3)/2/s13]]:
cf2:=[[0,1,-(s14-tau2-tau4)/2/s14,-(s14-tau2+tau4)/2/s13]]:
MU11:=[
-s12/4/s14/s13*( (s14-tau1)^2-tau3^2 )
];
MU12:=[
-s12/2/s14/s13*( -s14^2 + s14*(tau1+tau2)+tau1*tau2-tau3*tau4 + 2*s14/s12*tau1*tau2 )
];
MU22:=[
-s12/4/s14/s13*( (s14-tau2)^2-tau4^2 )
];

bv1:=[p1 , p2 , A(p2,p3)*IA(p1,p3)*eta(p1,p2)/2 , A(p1,p3)*IA(p2,p3)*eta(p2,p1)/2 ]:
bv2:=[p3 , p4 , A(p4,p1)*IA(p3,p1)*eta(p3,p4)/2 , A(p3,p1)*IA(p4,p1)*eta(p4,p3)/2 ]:

k1:=Vector(4):
k2:=Vector(4):
for b from 1 to 4 do:
k1:=k1+cf1[sol,b]*bv1[b];
k2:=k2+cf2[sol,b]*bv2[b];
od:

mu11:=MU11[sol];
mu12:=MU12[sol];
mu22:=MU22[sol];

l1:=k1;
l2:=k1-p1;
l3:=k1-p1-p2;
l4:=-k2+p3+p4;
l5:=-k2+p4;
l6:=-k2;
l7:=-k2-k1;

#print(dot(l1,l1)-mu11);
#print(dot(l2,l2)-mu11);
#print(dot(l3,l3)-mu11);
#print(dot(l4,l4)-mu22);
#print(dot(l5,l5)-mu22);
#print(dot(l6,l6)-mu22);
#print(dot(l7,l7)-mu11-mu12-mu22);

return k1,k2;

end:
#}}}

R1:=1.0:
R2:=1.1:
R3:=1.0:
R4:=1.1:
delta1:=1e-3*I:
delta2:=2e-3*I:
delta3:=2e-3*I:
delta4:=2e-3*I:

Nf:=0:Ns:=0:

#{{{ compute solution polynomial from trees ('d' coeffs)

Dvec:=Vector(160):
ds:=Array(0..4,0..4,0..4,0..4):

for k1 from 0 to 4 do:
  t1:=evalf(R1*exp(2*Pi*I*k1/5+delta1));
for k2 from 0 to 4 do:
  t2:=evalf(R2*exp(2*Pi*I*k2/5+delta2));
for k3 from 0 to 4 do:
  t3:=evalf(R3*exp(2*Pi*I*k3/5+delta3));
for k4 from 0 to 4 do:
  t4:=evalf(R4*exp(2*Pi*I*k4/5+delta4));
  
  kk1,kk2:=OnShell_boxboxD(p1,p2,p3,p4,1,t1,t2,t3,t4);
#  Integrand:=C7_1_INT_D(l1,-l6,mu11,mu12,mu22,p1,p2,p3,p4)/5/5/5/5:
  Integrand:=A4g_dblbox(kk1,kk2,mu11,mu12,mu22)/5/5/5/5:
  for pow1 from 0 to 4 do:
  for pow2 from 0 to 4 do:
  for pow3 from 0 to 4 do:
  for pow4 from 0 to 4 do:
    ds[pow1,pow2,pow3,pow4]:=ds[pow1,pow2,pow3,pow4]+Integrand/t1^(pow1)/t2^(pow2)/t3^(pow3)/t4^(pow4);
  od:
  od:
  od:
  od:
  lprint(k1,k2,k3,k4);
od:
od:
od:
od:

read("../2lintegrands/reduction/final/boxboxD.coeffvecs.mpl"):

printf("polynomial coefficients from trees:\n");
for i from 1 to 160 do:
if(abs(Dvec[i])<1e-20)then:Dvec[i]:=0:fi:
printf("%d, %.10Ze\n",i,Dvec[i]);
od:

#}}}

#{{{ invert to fit full integrand

read("../2lintegrands/reduction/final/boxboxD.integrand.mpl"):

fd:=fopen("Dcvecs_2.log",WRITE);
printf("integrand coefficients:\n");
for i from 1 to 160 do:
if(abs(Cvec[i])<1e-20)then:Cvec[i]:=0:fi:
printf("%A = %.10Ze\n",Clbl[i],Cvec[i]);
fprintf(fd,"%A := %.30e+(%.30e)*I:\n",Clbl[i],Re(Cvec[i]),Im(Cvec[i]));
od:

fclose(fd);

#}}}


