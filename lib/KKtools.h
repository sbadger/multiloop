* vim: set foldmethod=marker:

*{{{collectKK
#procedure collectKK
#do fun={AA,BB,AB,IAB,IBB,IAA}
id 'fun'(px1?,px2?,px3?) = 'fun'(px1,KK(px2),px3);
id 'fun'(px1?,px2?,px3?,px4?) = 'fun'(px1,KK(px2),KK(px3),px4);
id 'fun'(px1?,px2?,px3?,px4?,px5?) = 'fun'(px1,KK(px2),KK(px3),KK(px4),px5);
id 'fun'(px1?,px2?,px3?,px4?,px5?,px6?) = 'fun'(px1,KK(px2),KK(px3),KK(px4),KK(px5),px6);
id 'fun'(px1?,px2?,px3?,px4?,px5?,px6?,px7?) = 'fun'(px1,KK(px2),KK(px3),KK(px4),KK(px5),KK(px6),px7);
id 'fun'(px1?,px2?,px3?,px4?,px5?,px6?,px7?,px8?) = 'fun'(px1,KK(px2),KK(px3),KK(px4),KK(px5),KK(px6),KK(px7),px8);
#enddo

#do fun={dot,S,Idot,IS,PP,IPP}
id 'fun'(px1?,px2?) = 'fun'(KK(px1),KK(px2));
id 'fun'(px1?,px2?,px3?) = 'fun'(KK(px1),KK(px2),KK(px3));
#enddo
argument AA,BB,AB,IAB,IBB,IAA,dot,S,Idot,IS,PP,IPP;
splitarg KK;
endargument;

repeat;
#do fun={AA,BB,AB,IAB,IBB,IAA,dot,S,Idot,IS,PP,IPP}
id 'fun'(?x1,KK(px2?),?x3) = 'fun'(?x1,px2,?x3);
#enddo
endrepeat;

#endprocedure
*}}}

*{{{expandKK
#procedure expandKK
argument AA,BB,AB,IAB,IBB,IAA,IS,S,Idot,dot;
repeat;
id KK(px?,?x1) = px+KK(?x1);
id KK(px?)=px;
endrepeat;
endargument;

#endprocedure
*}}}

*{{{simplifyKK
#procedure simplifyKK

repeat;
#do fun={AA,BB,AB,IAB,IBB,IAA,dot,Idot}
id 'fun'(?x1,KK(px2?),?x3) = 'fun'(?x1,px2,?x3);
id 'fun'(?x1,-KK(?x),?x3) = -'fun'(?x1,KK(?x),?x3);
id 'fun'(?x1,KK(-px1?,-px2?),?x3) = -'fun'(?x1,KK(px1,px2),?x3);
id 'fun'(?x1,KK(-px1?,-px2?,-px3?),?x3) = -'fun'(?x1,KK(px1,px2,px3),?x3);
id 'fun'(?x1,KK(-px1?,-px2?,-px3?,-px4?),?x3) = -'fun'(?x1,KK(px1,px2,px3,px4),?x3);
id 'fun'(?x1,KK(-px1?,-px2?,-px3?,-px4?,-px5?),?x3) = -'fun'(?x1,KK(px1,px2,px3,px4,px5),?x3);
id 'fun'(?x1,KK(-px1?,-px2?,-px3?,-px4?,-px5?,-px6?),?x3) = -'fun'(?x1,KK(px1,px2,px3,px4,px5,px6),?x3);
id 'fun'(?x1,KK(?y1,px?ps,?y2),px?ps) = 'fun'(?x1,KK(?y1,?y2),px);
id 'fun'(?x1,KK(?y1,px?ps),px?ps) = 'fun'(?x1,KK(?y1),px);
id 'fun'(?x1,KK(px?ps,?y2),px?ps) = 'fun'(?x1,KK(?y2),px);
id 'fun'(px?ps,KK(?y1,px?ps,?y2),?x1) = 'fun'(px,KK(?y1,?y2),?x1);
id 'fun'(px?ps,KK(?y1,px?ps),?x1) = 'fun'(px,KK(?y1),?x1);
id 'fun'(px?ps,KK(px?ps,?y2),?x1) = 'fun'(px,KK(?y2),?x1);
id 'fun'(?x1,KK,?x2) = 'fun'(?x1,0,?x2);
id 'fun'(?x1,px1?ps,px1?ps,?x2) = 0;
#enddo
id AB(px1?ps,px2?ps,?x1,px3?) = A(px1,px2)*BB(px2,?x1,px3);
id AA(px1?ps,px2?ps,?x1,px3?) = A(px1,px2)*BA(px2,?x1,px3);
id BB(px1?ps,px2?ps,?x1,px3?) = B(px1,px2)*AB(px2,?x1,px3);
id IAB(px1?ps,px2?ps,?x1,px3?) = IA(px1,px2)*IBB(px2,?x1,px3);
id IAA(px1?ps,px2?ps,?x1,px3?) = IA(px1,px2)*IBA(px2,?x1,px3);
id IBB(px1?ps,px2?ps,?x1,px3?) = IB(px1,px2)*IAB(px2,?x1,px3);

id AB(px3?,?x1,px1?ps,px2?ps) = AA(px3,?x1,px1)*B(px1,px2);
id AA(px3?,?x1,px1?ps,px2?ps) = AB(px3,?x1,px1)*A(px1,px2);
id BB(px3?,?x1,px1?ps,px2?ps) = BA(px3,?x1,px1)*B(px1,px2);
id IAB(px3?,?x1,px1?ps,px2?ps) = IAA(px3,?x1,px1)*IB(px1,px2);
id IAA(px3?,?x1,px1?ps,px2?ps) = IAB(px3,?x1,px1)*IA(px1,px2);
id IBB(px3?,?x1,px1?ps,px2?ps) = IBA(px3,?x1,px1)*IB(px1,px2);

id AB(px1?ps,px2?,px3?ps,?x1,px4?) = AB(px1,px2,px3)*AB(px3,?x1,px4);
id IAB(px1?ps,px2?,px3?ps,?x1,px4?) = IAB(px1,px2,px3)*IAB(px3,?x1,px4);

#call cancel
endrepeat;

argument IS,S;
repeat;
id KK(px?,?x1) = px+KK(?x1);
id KK(px?)=px;
endrepeat;
endargument;

#call orderKK
#call orderS

#endprocedure
*}}}

*{{{orderKK
#procedure orderKK
#do i={9,8,7,6,5,4,3,2,1}
id KK(?x2,p'i',?x1) = KK(p'i',?x2,?x1);
#enddo
#endprocedure
*}}}

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}


