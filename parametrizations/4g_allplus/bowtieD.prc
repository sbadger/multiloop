id INTbowtieD(k1,k2,mu11,mu12,mu22) =  + IA(p1,p4)^2*IA(p2,p3)^2*s14^2*
      xxghost7 * ( 1/16*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*s14^2*xxghost5 * (  - 5/32*mu12*mu22 - 1/64*
         mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*s14^2*xxghost4 * (  - 1/64*mu11*mu22 - 5/32*
         mu11*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*s14^2*s12*xxghost5 * ( 5/128*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*s14^2*s12*xxghost4 * ( 5/128*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*[Ds-2]*s14^2*xxghost5 * (  - 1/16*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*[Ds-2]*s14^2*xxghost4 * (  - 1/16*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*s14^2*xxghost6 * ( 1/64*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*s14^2*xxdiag2 * ( 5/16*mu12*mu22 + 1/32*
         mu11*mu22 + 5/16*mu11*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*s14^2*s12*xxdiag2 * (  - 5/64*mu22 - 5/64*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]*s14^2 * ( 3/4*mu12*mu22 + 3/4*mu11*
         mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]*s14^2*xxdiag2 * ( 5/4*mu12*mu22 + 1/
         4*mu11*mu22 + 5/4*mu11*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]*s14^2*s12 * ( mu22 + mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]*s14^2*s12*xxdiag3 * ( 11/16*mu22 + 
         11/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]*s14^2*s12*xxdiag2 * (  - 5/16*mu22
          - 5/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]^2*s14^2*xxdiag3 * (  - 1/2*mu11*mu22
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*I*[Ds-2]^2*s14^2*xxdiag2 * ( 1/2*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*s14*s12*xxghost4 * (  - 3/64*mu11
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*I*s14*s12*xxdiag2 * ( 3/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*I*[Ds-2]*s14*s12 * (  - 1/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*I*[Ds-2]*s14*s12*xxdiag3 * (  - 1/
         8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*I*[Ds-2]*s14*s12*xxdiag2 * ( 3/8*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)^2*NUM(6*s12^2 + 23*
      s14*s12 + 9*s14^2)*I*s12^-1*s13^-1 * ( 1/4 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)^2*NUM(6*s12^2 + 23*
      s14*s12 + 9*s14^2)*I*s12^-1*xxdiag3*s13^-1 * ( 1/4 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*
      s12 - s14)*I*s14^-1 * (  - 3/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*
      s12 - s14)*I*s14^-1*xxdiag3 * (  - 3/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)*NUM(6*s12^2 + 23*s14*
      s12 + 9*s14^2)*I*s13^-1 * ( 1/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(p4,k1)*NUM(6*s12^2 + 23*s14*
      s12 + 9*s14^2)*I*xxdiag3*s13^-1 * ( 1/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(k1,omega4)*NUM(2*s12 + 3*s14
      )*I*s12*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*dot(k1,omega4)*NUM(2*s12 + 3*s14
      )*I*s12*xxdiag3*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*NUM(6*s12^2 + 17*s14*s12 + 3*
      s14^2)*I*s13^-1 * ( 1/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)^2*NUM(6*s12^2 + 17*s14*s12 + 3*
      s14^2)*I*xxdiag3*s13^-1 * ( 1/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*s14*xxghost5 * (  - 3/
         16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*s14*xxghost4 * (  - 3/
         16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*I*s14*xxdiag2 * ( 3/8*
         mu22 + 3/8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*I*[Ds-2]*s14 * (  - 3/2
         *mu22 - 3/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*I*[Ds-2]*s14*xxdiag2
       * ( 3/2*mu22 + 3/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)^2*dot(k2,omega4)*NUM(2*
      s12 - s14)*I*s14^-1 * ( 3/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)^2*dot(k2,omega4)*NUM(2*
      s12 - s14)*I*s14^-1*xxdiag3 * ( 3/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)^2*NUM(6*s12^2 + 23*s14*
      s12 + 9*s14^2)*I*s13^-1 * ( 1/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)^2*NUM(6*s12^2 + 23*s14*
      s12 + 9*s14^2)*I*xxdiag3*s13^-1 * ( 1/8 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k1,k2)*NUM(6*s12^3
       + 17*s14*s12^2 + 28*s14^2*s12 + 9*s14^3)*I*s12^-1*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k1,k2)*NUM(6*s12^3
       + 17*s14*s12^2 + 28*s14^2*s12 + 9*s14^3)*I*s12^-1*xxdiag3*s13^-1 * ( 
          - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k1,omega4)*NUM(2*
      s12 + s14)*NUM(4*s12 + 3*s14)*I*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k1,omega4)*NUM(2*
      s12 + s14)*NUM(4*s12 + 3*s14)*I*xxdiag3*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k2,omega4)*NUM(2*
      s12 + s14)*NUM(4*s12 + 3*s14)*I*s13^-1 * ( 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*dot(k2,omega4)*NUM(2*
      s12 + s14)*NUM(4*s12 + 3*s14)*I*xxdiag3*s13^-1 * ( 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*NUM(2*s12 + s14)*NUM(3*
      s12^2 + 10*s14*s12 + 15*s14^2)*I*s12^-1*s13^-1 * (  - 1/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*NUM(2*s12 + s14)*NUM(3*
      s12^2 + 10*s14*s12 + 15*s14^2)*I*s12^-1*xxdiag3*s13^-1 * (  - 1/16*mu12
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*NUM(11*s12 + 6*s14)*I*
      s14*xxdiag2 * (  - 3/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*NUM(6*s12^2 + 23*s14*
      s12 + 9*s14^2)*I*s14*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(p4,k1)*NUM(27*s12^2 + 28*s14*
      s12 + 9*s14^2)*I*s14*xxdiag3*s13^-1 * ( 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,k2)*NUM(10*s12^2 - 3*s14*
      s12 - 5*s14^2)*I*s14*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,k2)*NUM(10*s12^2 - 3*s14*
      s12 - 5*s14^2)*I*s14*xxdiag3*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*s14*xxghost5 * ( 3/
         16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*I*s12 * ( 3/16*mu12
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*I*s12*xxdiag3 * ( 3/
         16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*I*s14*xxdiag2 * ( 
          - 3/8*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*I*[Ds-2]*s14 * ( 3/
         2*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*I*[Ds-2]*s14*
      xxdiag2 * (  - 3/2*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*NUM(3*s12 - 2*s14)*
      I*s12*xxdiag2 * (  - 3/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k1,omega4)*NUM(9*s12^2 + 5*s14
      *s12 - 3*s14^2)*I*s12*xxdiag3*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k2,omega4)*NUM(2*s12 - 3*s14)*
      I * (  - 3/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k2,omega4)*NUM(2*s12 - 3*s14)*
      I*xxdiag3 * (  - 3/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k2,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*dot(k2,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*xxdiag3*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(2*s12 - s14)*s14*xxghost5 * ( 
          - 1/32*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(2*s12 - s14)*I*s14*xxdiag2
       * ( 1/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(2*s12 - s14)*I*[Ds-2]*s14 * ( 
          - 1/4*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(2*s12 - s14)*I*[Ds-2]*s14*
      xxdiag2 * ( 1/4*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(7*s12 + 4*s14)*I*s14*s12*
      xxdiag3 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(7*s12 + 4*s14)*I*s14*s12*
      xxdiag2 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(6*s12^2 - s14*s12 - 3*s14^2)*I
      *s14*s13^-1 * ( 1/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(6*s12^2 - s14*s12 - 3*s14^2)*I
      *s14*xxdiag3*s13^-1 * ( 1/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(32*s12^2 + 45*s14*s12 - 3*
      s14^2)*I*s14*s13^-1 * (  - 1/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p1,k2)*NUM(32*s12^2 + 45*s14*s12 - 3*
      s14^2)*I*s14*xxdiag3*s13^-1 * (  - 1/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*s14*s12*xxghost5 * (  - 3/64*mu22
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*I*s14*s12*xxdiag2 * ( 3/32*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*I*[Ds-2]*s14*s12 * (  - 1/2*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*I*[Ds-2]*s14*s12*xxdiag3 * (  - 1/
         8*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*I*[Ds-2]*s14*s12*xxdiag2 * ( 3/8*
         mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)^2*dot(k2,omega4)*NUM(2*s12 + 3*s14
      )*I*s12*s13^-1 * ( 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)^2*dot(k2,omega4)*NUM(2*s12 + 3*s14
      )*I*s12*xxdiag3*s13^-1 * ( 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)^2*NUM(6*s12^2 + 17*s14*s12 + 3*
      s14^2)*I*s13^-1 * ( 1/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)^2*NUM(6*s12^2 + 17*s14*s12 + 3*
      s14^2)*I*xxdiag3*s13^-1 * ( 1/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,k2)*NUM(10*s12^2 - 3*s14*
      s12 - 5*s14^2)*I*s14*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,k2)*NUM(10*s12^2 - 3*s14*
      s12 - 5*s14^2)*I*s14*xxdiag3*s13^-1 * ( 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*s12 - 3*s14)*
      I * ( 3/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*s12 - 3*s14)*
      I*xxdiag3 * ( 3/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*s13^-1 * (  - 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k1,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*xxdiag3*s13^-1 * (  - 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*s14*xxghost4 * ( 
          - 3/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*I*s12 * (  - 3/16*
         mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*I*s12*xxdiag3 * ( 
          - 3/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*I*s14*xxdiag2 * ( 3/
         8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*I*[Ds-2]*s14 * ( 
          - 3/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*I*[Ds-2]*s14*
      xxdiag2 * ( 3/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*NUM(2*s12 + 3*s14)*
      I*s14*s12*s13^-1 * (  - 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*NUM(3*s12 - 2*s14)*
      I*s12*xxdiag2 * ( 3/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*dot(k2,omega4)*NUM(9*s12^2 + 5*s14
      *s12 - 3*s14^2)*I*s12*xxdiag3*s13^-1 * (  - 1/32 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(2*s12 - s14)*s14*xxghost4 * ( 
          - 1/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(2*s12 - s14)*I*s14*xxdiag2
       * ( 1/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(2*s12 - s14)*I*[Ds-2]*s14 * ( 
          - 1/4*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(2*s12 - s14)*I*[Ds-2]*s14*
      xxdiag2 * ( 1/4*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(7*s12 + 4*s14)*I*s14*s12*
      xxdiag3 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(7*s12 + 4*s14)*I*s14*s12*
      xxdiag2 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(6*s12^2 - s14*s12 - 3*s14^2)*I
      *s14*s13^-1 * ( 1/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(6*s12^2 - s14*s12 - 3*s14^2)*I
      *s14*xxdiag3*s13^-1 * ( 1/16*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(32*s12^2 + 45*s14*s12 - 3*
      s14^2)*I*s14*s13^-1 * (  - 1/32*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(p4,k1)*NUM(32*s12^2 + 45*s14*s12 - 3*
      s14^2)*I*s14*xxdiag3*s13^-1 * (  - 1/32*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*s14^2*s12^-1*xxghost5 * (  - 1/32*
         mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*s14^2*s12^-1*xxghost4 * (  - 1/32*
         mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*s14^2*xxghost5 * ( 1/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*s14^2*xxghost4 * ( 1/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*[Ds-2]*s14^2*s12^-1*xxghost5 * ( 
          - 1/8*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*[Ds-2]*s14^2*s12^-1*xxghost4 * ( 
          - 1/8*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*s14^2*s12^-1*xxghost6 * ( 1/64*
         mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*s14^2*s12^-1*xxdiag2 * ( 1/16*
         mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*s14^2*xxdiag2 * (  - 1/8*mu22 - 
         1/8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*[Ds-2]*s14^2*s12^-1*xxdiag2 * ( 
         1/2*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*[Ds-2]*s14^2 * ( 1/2*mu22 + 1/2*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*[Ds-2]*s14^2*xxdiag2 * (  - 1/2*
         mu22 - 1/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*I*[Ds-2]^2*s14^2*s12^-1*xxdiag2
       * ( mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*NUM(3*s12^2 + 5*s14*s12 + 3*s14^2)
      *I*s14*s12^-1 * (  - 1/32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*NUM(3*s12^2 + 5*s14*s12 + 3*s14^2)
      *I*s14*s12^-1*xxdiag3 * (  - 1/32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*NUM(9*s12^2 + 43*s14*s12 + 18*
      s14^2)*I*s14*xxdiag2 * ( 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*NUM(2*s12^3 + 26*s14*s12^2 + 21*
      s14^2*s12 - s14^3)*I*s14*s13^-1 * (  - 1/16 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,k2)*NUM(17*s12^3 + 156*s14*s12^2 + 145
      *s14^2*s12 + 14*s14^3)*I*s14*xxdiag3*s13^-1 * (  - 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*s14*s12*xxghost5 * ( 3/64*mu22
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*s14*s12*xxghost4 * (  - 1/16*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*s14*s12 * (  - 3/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*s14*s12*xxdiag3 * (  - 3/16*
         mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*s14*s12*xxdiag2 * (  - 3/32*
         mu22 + 1/8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*s14^2 * (  - 3/16*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*s14^2*xxdiag3 * (  - 3/16*
         mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*[Ds-2]*s14*s12 * ( 1/2*mu22
          - 1/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*[Ds-2]*s14*s12*xxdiag3 * ( 1/
         8*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*I*[Ds-2]*s14*s12*xxdiag2 * ( 
          - 3/8*mu22 + 1/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*NUM(s12 - 3*s14)*I*s14 * ( 1/
         32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*NUM(s12 - 3*s14)*I*s14*xxdiag3
       * ( 1/32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*NUM(13*s12 - 6*s14)*I*s14*s12*
      xxdiag3 * (  - 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k1,omega4)*NUM(13*s12 - 6*s14)*I*s14*s12*
      xxdiag2 * (  - 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*s14*s12*xxghost5 * ( 1/16*mu22
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*s14*s12*xxghost4 * (  - 3/64*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*s14*s12 * ( 3/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*s14*s12*xxdiag3 * ( 3/16*
         mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*s14*s12*xxdiag2 * (  - 1/8*
         mu22 + 3/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*s14^2 * ( 3/16*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*s14^2*xxdiag3 * ( 3/16*mu11
          )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*[Ds-2]*s14*s12 * ( 1/2*mu22
          - 1/2*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*[Ds-2]*s14*s12*xxdiag3 * ( 
          - 1/8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*I*[Ds-2]*s14*s12*xxdiag2 * ( 
          - 1/2*mu22 + 3/8*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*NUM(s12 - 3*s14)*I*s14 * (  - 
         1/32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*NUM(s12 - 3*s14)*I*s14*xxdiag3
       * (  - 1/32*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*NUM(13*s12 - 6*s14)*I*s14*s12*
      xxdiag3 * ( 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*dot(k2,omega4)*NUM(13*s12 - 6*s14)*I*s14*s12*
      xxdiag2 * ( 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(s12^2 - 17*s14*s12 + 2*s14^2)*I*s14*
      xxdiag2 * ( 9/64*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(3*s12^2 + 4*s14*s12 + 3*s14^2)*I*s14*
      s12^-1 * (  - 1/32*mu12^2 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(3*s12^2 + 4*s14*s12 + 3*s14^2)*I*s14*
      s12^-1*xxdiag3 * (  - 1/32*mu12^2 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(6*s12^2 + 7*s14*s12 + 6*s14^2)*I*s14*
      s12^-1 * ( 1/16*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(6*s12^2 + 7*s14*s12 + 6*s14^2)*I*s14*
      s12^-1*xxdiag3 * ( 1/16*mu11*mu22 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(9*s12^2 + 16*s14*s12 + 3*s14^2)*I*s14^2*
      s13^-1 * ( 1/32*mu22 + 1/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(9*s12^2 + 16*s14*s12 + 3*s14^2)*I*s14^2*
      xxdiag3*s13^-1 * ( 1/32*mu22 + 1/32*mu11 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(16*s12^2 + 21*s14*s12 + 10*s14^2)*I*s14*
      s12*xxdiag3 * ( 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(16*s12^2 + 21*s14*s12 + 10*s14^2)*I*s14*
      s12*xxdiag2 * ( 1/64 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(s12^3 + 118*s14*s12^2 + 113*s14^2*s12 + 4
      *s14^3)*I*s14*xxdiag3*s13^-1 * (  - 1/64*mu12 )

       + IA(p1,p4)^2*IA(p2,p3)^2*NUM(4*s12^3 - 131*s14*s12^2 - 124*s14^2*s12
       + 7*s14^3)*I*s14*s13^-1 * ( 1/32*mu12 );

