* vim: set foldmethod=marker:
#-
S Omega;
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,p,x,P,h,k,e,q,c;
S mS;
S k1f,k2f,k3f,k4f,k1,k2,k3;
S mu;
S l1,...,l20;

auto S Basis;
CF Power,cc,DP,nc,ccNS,ccS;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}
*{{{ simplify4
#procedure simplify4

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id IA(p1?,p2?) = IS(p1,p2)*B(p2,p1);
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
#call cancel
id A(p1?,p2?)*B(p2?,p3?)*A(p3?,p4?)*B(p4?,p1?) = AB(p1,p2,p3,p4,p1);
id AB(p3,x1?,x2?,x3?,p3) = AB(x2,x3,p3,x1,x2);
id AB(?x1,p3,?x2) = -AB(?x1,MOM(p1,p2,p4),?x2);
#call breakstrings
#call compress(1);
#call breakstrings
#call compress(1);
#call breakstrings
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);

#call orderMOM
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id S(p3,p4) = S(p1,p2);
id IS(p2,p4) = IS(p1,p3);
id IS(p2,p3) = IS(p1,p4);
id IS(p3,p4) = IS(p1,p2);

#endprocedure
*}}}

off stats;

#define topo "boxboxbox"
.global

#do i=1,10
G L'i' = l'i';
#enddo
#include- ../OSsol/final/'topo'.OSprops.h
.store
#do sol=1,14
G K1'sol' = k1'sol';
G K2'sol' = k2'sol';
G K3'sol' = k3'sol';
#enddo

*{{{
id k11 = 1/2*(1-S(p1,p2)*IS(p1,p4)/tau2-S(p1,p2)*IS(p1,p4)*tau1/tau2)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k21 = (S(p1,p2)*IS(p1,p4)/tau1/tau2+S(p1,p2)*IS(p1,p4)/tau1+S(p1,p2)*IS(p1,p4)/tau2+S(p1,p2)*IS(p1,p4))*p2+(1/tau1*tau2-S(p1,p2)*IS(p1,p4)/tau1-S(p1,p2)*IS(p1,p4))*p3+1/2*(-S(p1,p3)*IS(p1,p4)/tau1-S(p1,p3)*IS(p1,p4))*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*(-S(p1,p2)*IS(p1,p3)/tau1-S(p1,p2)*IS(p1,p3)/tau1*tau2+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau1/tau2+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau1+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau2+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4))*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k31 = 1/2*(1+tau2)*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k12 = 1/2*(S(p1,p4)*IS(p1,p3)-S(p1,p2)*IS(p1,p3)/tau2-S(p1,p2)*IS(p1,p3)*tau1/tau2)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k22 = (S(p1,p2)*IS(p1,p4)/tau1/tau2+S(p1,p2)*IS(p1,p4)/tau1+S(p1,p2)*IS(p1,p4)/tau2+S(p1,p2)*IS(p1,p4))*p2+(1/tau1*tau2-S(p1,p2)*IS(p1,p4)/tau1-S(p1,p2)*IS(p1,p4))*p3+1/2*(-1/tau1-1/tau1*tau2+S(p1,p2)*IS(p1,p4)/tau1/tau2+S(p1,p2)*IS(p1,p4)/tau1+S(p1,p2)*IS(p1,p4)/tau2+S(p1,p2)*IS(p1,p4))*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*(-S(p1,p2)*IS(p1,p4)/tau1-S(p1,p2)*IS(p1,p4))*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k32 = 1/2*(S(p1,p4)*IS(p1,p3)+S(p1,p4)*IS(p1,p3)*tau2)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k13 = 1/2*(-S(p1,p3)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau1)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k23 = (-S(p1,p3)*IS(p1,p4)*tau1-S(p1,p3)*IS(p1,p4)*tau1*tau2+S(p1,p2)*IS(p1,p4)+S(p1,p2)*IS(p1,p4)*tau2)*p2+(S(p1,p2)*IS(p1,p4)/tau1*tau2+S(p1,p2)*IS(p1,p4)*tau2)*p3+1/2*(S(p1,p3)*IS(p1,p4)*tau2-S(p1,p2)*IS(p1,p4)/tau1*tau2)*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*(-S(p1,p2)*IS(p1,p4)-S(p1,p2)*IS(p1,p4)*tau2-S(p1,p2)*IS(p1,p4)*tau1-S(p1,p2)*IS(p1,p4)*tau1*tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k33 = 1/2*(-1+S(p1,p2)*IS(p1,p3)/tau1)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k14 = 1/2*(-1-tau1)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k24 = (-S(p1,p3)*IS(p1,p4)*tau1-S(p1,p3)*IS(p1,p4)*tau1*tau2+S(p1,p2)*IS(p1,p4)+S(p1,p2)*IS(p1,p4)*tau2)*p2+(S(p1,p2)*IS(p1,p4)/tau1*tau2+S(p1,p2)*IS(p1,p4)*tau2)*p3+1/2*(-S(p1,p3)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau2-S(p1,p3)*IS(p1,p4)*tau1-S(p1,p3)*IS(p1,p4)*tau1*tau2)*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*(S(p1,p2)*S(p1,p3)*IS(p1,p3)*IS(p1,p4)*tau2-S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau1*tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k34 = 1/2*(-S(p1,p3)*IS(p1,p4)+S(p1,p2)*IS(p1,p4)/tau1)*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k15 = 1/2*tau2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k25 = (S(p1,p2)*IS(p1,p4)/tau1+S(p1,p2)*IS(p1,p4))*p2+1/2*(-S(p1,p2)*S(p1,p3)*IS(p1,p3)*IS(p1,p4)+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau1)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k35 = 1/2*(-1-tau1)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k16 = 1/2*S(p1,p4)*IS(p1,p3)*tau2*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k26 = (S(p1,p2)*IS(p1,p4)/tau1+S(p1,p2)*IS(p1,p4))*p2+1/2*(-S(p1,p3)*IS(p1,p4)+S(p1,p2)*IS(p1,p4)/tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
id k36 = 1/2*(-S(p1,p3)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau1)*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k17 = 1/2*(-S(p1,p3)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau1)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k27 = (-S(p1,p2)*IS(p1,p4)/tau1-S(p1,p2)*IS(p1,p4))*p3+1/2*(-S(p1,p3)*IS(p1,p4)+S(p1,p2)*IS(p1,p4)/tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
id k37 = 1/2*tau2*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k18 = 1/2*(-1-tau1)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k28 = (-S(p1,p2)*IS(p1,p4)/tau1-S(p1,p2)*IS(p1,p4))*p3+1/2*(-S(p1,p2)*S(p1,p3)*IS(p1,p3)*IS(p1,p4)+S(p1,p2)^2*IS(p1,p3)*IS(p1,p4)/tau1)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k38 = 1/2*S(p1,p3)*IS(p1,p4)*tau2*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k19 = 1/2*tau2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k29 = tau1*p2+1/2*(S(p1,p2)*IS(p1,p3)+S(p1,p2)*IS(p1,p3)*tau1)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k39 = 1/2*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k110 = 1/2*S(p1,p4)*IS(p1,p3)*tau2*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k210 = tau1*p2+1/2*(1+tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
id k310 = 1/2*S(p1,p4)*IS(p1,p3)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k111 = 1/2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k211 = tau1*p3+1/2*(S(p1,p2)*IS(p1,p3)-S(p1,p2)*IS(p1,p3)*tau1)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k311 = 1/2*tau2*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k112 = 1/2*S(p1,p4)*IS(p1,p3)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k212 = tau1*p3+1/2*(1-tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
id k312 = 1/2*S(p1,p4)*IS(p1,p3)*tau2*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
id k113 = 1/2*tau1*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
id k213 = 1/2*S(p1,p2)*IS(p1,p3)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
id k313 = 1/2*tau2*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
id k114 = 1/2*S(p1,p4)*IS(p1,p3)*tau1*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
id k214 = 1/2*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
id k314 = 1/2*S(p1,p4)*IS(p1,p3)*tau2*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*}}}
*{{{ old parameterisation
*id k11 = 1/2*(1-S(p1,p2)*IS(p1,p4)*(1+tau2)/tau1)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k21 = S(p1,p2)*IS(p1,p4)*(1+1/tau1)*(1+1/tau2)*p2+(tau1/tau2-S(p1,p2)*IS(p1,p4)*(1+1/tau2))*p3-1/2*S(p1,p3)*IS(p1,p4)*(1+1/tau2)*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*S(p1,p2)*IS(p1,p3)*(S(p1,p2)*IS(p1,p4)*(1+1/tau1)*(1+1/tau2)-(1+tau1)/tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k31 = 1/2*(1+tau1)*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k12 = 1/2*S(p1,p4)*IS(p1,p3)*(1+(1+tau2)/tau1)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k22 = S(p1,p2)*IS(p1,p4)*(1+1/tau2)*(1-S(p1,p4)*IS(p1,p2)/tau1)*p2-S(p1,p2)*IS(p1,p4)*(1+(1+tau1)/tau2)*p3+1/2*(1+(1+tau1)/tau2)*(S(p1,p2)*IS(p1,p4)-1/tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3)-1/2*S(p1,p2)*IS(p1,p4)*(1+1/tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k32 = 1/2*(-S(p1,p2)*IS(p1,p3)*tau1+S(p1,p4)*IS(p1,p3))*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k13 = -1/2*S(p1,p3)*IS(p1,p4)*(1+tau2)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k23 = (1+tau1)*(S(p1,p2)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau2)*p2+S(p1,p2)*IS(p1,p4)*(1+1/tau2)*tau1*p3+1/2*(S(p1,p3)*IS(p1,p4)-S(p1,p2)*IS(p1,p4)/tau2)*tau1*A(p3,p4)*IA(p2,p4)*eta(p2,p3)-1/2*S(p1,p2)*IS(p1,p4)*(1+tau1)*(1+tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k33 = 1/2*(S(p1,p2)*IS(p1,p3)/tau2-1)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k14 = 1/2*(-1-tau2)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k24 = (1+tau1)*(S(p1,p2)*IS(p1,p4)-S(p1,p3)*IS(p1,p4)*tau2)*p2+S(p1,p2)*IS(p1,p4)*(1+1/tau2)*tau1*p3-1/2*S(p1,p3)*IS(p1,p4)*(1+tau1)*(1+tau2)*A(p3,p4)*IA(p2,p4)*eta(p2,p3)+1/2*S(p1,p2)*IS(p1,p4)*tau1*(1-S(p1,p2)*IS(p1,p3)/tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k34 = 1/2*(S(p1,p2)*IS(p1,p4)/tau2-S(p1,p3)*IS(p1,p4))*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k15 = 1/2*tau1*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k25 = S(p1,p2)*IS(p1,p4)*(1+1/tau2)*p2+1/2*S(p1,p2)*IS(p1,p3)*IS(p1,p4)*(S(p1,p2)/tau2-S(p1,p3))*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k35 = 1/2*(-1-tau2)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k16 = 1/2*tau1*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k26 = S(p1,p2)*IS(p1,p4)*(1+1/tau2)*p2+1/2*(S(p1,p2)*IS(p1,p4)/tau2-S(p1,p3)*IS(p1,p4))*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
*id k36 = -1/2*S(p1,p3)*IS(p1,p4)*(1+tau2)*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k17 = -1/2*S(p1,p3)*IS(p1,p4)*(1+tau2)*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k27 = -S(p1,p2)*IS(p1,p4)*(1+1/tau2)*p3+1/2*(S(p1,p2)*IS(p1,p4)/tau2-S(p1,p3)*IS(p1,p4))*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
*id k37 = 1/2*tau1*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k18 = 1/2*(-1-tau2)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k28 = -S(p1,p2)*IS(p1,p4)*(1+1/tau2)*p3+1/2*S(p1,p2)*IS(p1,p3)*IS(p1,p4)*(S(p1,p2)/tau2-S(p1,p3))*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k38 = 1/2*tau1*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k19 = 1/2*tau2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k29 = tau1*p2+1/2*S(p1,p2)*IS(p1,p3)*(1+tau1)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k39 = 1/2*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k110 = 1/2*tau2*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k210 = tau1*p2+1/2*(1+tau1)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
*id k310 = 1/2*S(p1,p4)*IS(p1,p3)*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k111 = 1/2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k211 = tau2*p3+1/2*S(p1,p2)*IS(p1,p3)*(1-tau2)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k311 = 1/2*tau1*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k112 = 1/2*S(p1,p4)*IS(p1,p3)*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k212 = tau2*p3+1/2*(1-tau2)*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
*id k312 = 1/2*tau1*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*id k113 = 1/2*tau2*A(p2,p3)*IA(p1,p3)*eta(p1,p2);
*id k213 = 1/2*S(p1,p2)*IS(p1,p3)*A(p2,p4)*IA(p3,p4)*eta(p3,p2);
*id k313 = 1/2*tau1*A(p4,p1)*IA(p3,p1)*eta(p3,p4);
*id k114 = 1/2*tau2*A(p1,p3)*IA(p2,p3)*eta(p2,p1);
*id k214 = 1/2*A(p3,p4)*IA(p2,p4)*eta(p2,p3);
*id k314 = 1/2*tau1*A(p3,p1)*IA(p4,p1)*eta(p4,p3);
*}}}

*#include- ../OSsol/final/'topo'.OSsol.final.h
.store

*** compute ISPs ****

#do sol=1,14
G ISP1'sol' = DP(L1,p4);
G ISP2'sol' = DP(-L7,p1);
G ISP3'sol' = DP(-L8,p1);
G ISP4'sol' = DP(-L8,p4);
G ISP5'sol' = DP(L1,Omega);
G ISP6'sol' = DP(-L7,Omega);
G ISP7'sol' = DP(-L8,Omega);

multiply replace_(k1,K1'sol');
multiply replace_(k2,K2'sol');
multiply replace_(k3,K3'sol');

repeat;
id,once DP(x1?,x2?) = 2*x1*x2;

#do i=1,4
id p'i' = MOM(p'i',mu);
#enddo
id eta(p1?,p2?) = AB(p1,mu,p2);
id Omega = ( AB(p2,p3,p1)*AB(p1,mu,p2) - AB(p1,p3,p2)*AB(p2,mu,p1) )/2*IS(p1,p2);

#call lprod
#call cancel
endrepeat;

id INV(S(p1?,p2?)) = IS(p1,p2);

#call cancel
#call breakstrings
#call cancel

#call cancel
#call simplify4
#call cancel
id S(p1,p3)=-S(p1,p2)-S(p1,p4);
#call cancel

B tau1,tau2;
.sort
collect NUM;
#call SortCollect
argument NUM;
id S(p1,p3)=-S(p1,p2)-S(p1,p4);
endargument;
#call SortCollect
id NUM(S(p1,p2)+S(p1,p4)) = -S(p1,p3);
id NUM(x?) = x;
#call cancel

id S(p1,p2) = s;
id S(p1,p4) = t;
id S(p1,p3) = u;
id IS(p1,p2) = 1/s;
id IS(p1,p4) = 1/t;
id IS(p1,p3) = 1/u;

print+s;
.store
#enddo

G Delta = Basis'topo';
#include- 'topo'.basis.h
id Power(x1?,x2?) = x1^x2;
id nc(x?)*cc(?x) = cc(x,?x);

#$totalc=0;
B cc;
.sort
keep brackets;
$totalc=$totalc+1;
.sort
#write "'$totalc'"

id DP(x?,Omega) = xxspurious*DP(x,Omega);
id xxspurious^2=1;
#$xxS=1;
#$xxNS=1;
.sort

if(count(xxspurious,1)<1);
multiply ccNS($xxNS);
$xxNS=$xxNS+1;
else;
multiply ccS($xxS);
$xxS=$xxS+1;
endif;
.sort
id xxspurious=1;

$totalcS=$xxS-1;
$totalcNS=$xxNS-1;
.store
#write "'$totalcS'"
#write "'$totalcNS'"

#do sol=1,14

G Delta'sol' = Delta;

id DP(l1,p4) = ISP1'sol';
id DP(-l7,p1) = ISP2'sol';
id DP(-l8,p1) = ISP3'sol';
id DP(-l8,p4) = ISP4'sol';
id DP(l1,Omega) = ISP5'sol';
id DP(-l7,Omega) = ISP6'sol';
id DP(-l8,Omega) = ISP7'sol';

#call cancel

B tau1,tau2,INV,NUM;
print[];
.store

#enddo

*{{{ full matrix 622 x 398

#write <final/'topo'.coeffvecs.mpl> "Cvec:=Vector('$totalc'):"
#do cc=1,'$totalc'
L cveccount = Delta;

id cc('cc',?x$clbl) = 1;
id cc(?x)=0;
.sort
#write <final/'topo'.coeffvecs.mpl> "Clbl['cc']:=cs['$clbl']:"
.store
#enddo

#write <'topo'.reduceM.log> "ReduceMatrix:=Matrix(622,'$totalc'):"
#write <final/'topo'.coeffvecs.mpl> "Dvec:=Vector(622):"

#define dd "0"

#do sol=1,14

#do pow1=-6,6
#do pow2=-6,6

#$nonzero=0;
L tmpRow = Delta'sol';

if(count(tau1,1)!='pow1');
discard;
endif;

if(count(tau2,1)!='pow2');
discard;
endif;

id tau1=1;
id 1/tau1=1;
id tau2=1;
id 1/tau2=1;
.sort
$nonzero=$nonzero+1;
.sort
hide;

#write "'pow1','pow2','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "Dvec['dd']:=ds['pow1','pow2']:"

#do cc=1,'$totalc'

L tmpEl'cc' = tmpRow;
id cc('cc',?x) = 1;
id cc(?x)=0;
id ccNS(?x) = 1;
id ccS(?x) = 1;
.sort

#write <'topo'.reduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
.sort
hide;

#enddo

#endif

unhide;
.store
#enddo
#enddo
#enddo
*}}}

#do pairsol=1,7

G DeltaNS'pairsol' = 1/2*(Delta{2*'pairsol'-1}+Delta{2*'pairsol'});
G DeltaS'pairsol' = 1/2*(Delta{2*'pairsol'-1}-Delta{2*'pairsol'});

B cc,ccS,ccNS;
print[];
.store

#enddo

*{{{ non-spurious matrix 311 x 199

#write <final/'topo'.coeffvecs.mpl> "CvecNS:=Vector('$totalcNS'):"
#do cc=1,'$totalcNS'
L cveccount = Delta;

id ccS(x2?) = 0;
id cc(x1?,?args)*ccNS(x2?) = cc(x2,?args);
id cc('cc',?x$clbl) = 1;
id cc(?x)=0;
.sort
#write <final/'topo'.coeffvecs.mpl> "ClblNS['cc']:=csNS['$clbl']:"
.store
#enddo

#write <'topo'.reduceM.log> "ReduceMatrixNS:=Matrix(311,'$totalcNS'):"
#write <final/'topo'.coeffvecs.mpl> "DvecNS:=Vector(311):"

#define dd "0"

#do sol=1,7

#do pow1=-6,6
#do pow2=-6,6

#$nonzero=0;
L tmpRow = DeltaNS'sol';

if(count(tau1,1)!='pow1');
discard;
endif;

if(count(tau2,1)!='pow2');
discard;
endif;

id tau1=1;
id 1/tau1=1;
id tau2=1;
id 1/tau2=1;
.sort
$nonzero=$nonzero+1;
.sort
hide;

#write "'pow1','pow2','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "DvecNS['dd']:=ds['pow1','pow2']:"

#do cc=1,'$totalcNS'

L tmpEl'cc' = tmpRow;

id cc(x1?,?args)*ccNS(x2?) = cc(x2,?args);
id cc('cc',?x) = 1;
id cc(?x)=0;
id ccNS(?x) = 1;
id ccS(?x) = 1;
.sort

#write <'topo'.reduceM.log> "ReduceMatrixNS['dd','cc']:=%E:" tmpEl'cc'
.sort
hide;

#enddo

#endif

unhide;
.store
#enddo
#enddo
#enddo
*}}}

*{{{ spurious matrix 311 x 199

#write <final/'topo'.coeffvecs.mpl> "CvecS:=Vector('$totalcS'):"
#do cc=1,'$totalcS'
L cveccount = Delta;

id ccNS(x2?) = 0;
id cc(x1?,?args)*ccS(x2?) = cc(x2,?args);
id cc('cc',?x$clbl) = 1;
id cc(?x)=0;
.sort
#write <final/'topo'.coeffvecs.mpl> "ClblS['cc']:=csNS['$clbl']:"
.store
#enddo

#write <'topo'.reduceM.log> "ReduceMatrixS:=Matrix(311,'$totalcS'):"
#write <final/'topo'.coeffvecs.mpl> "DvecS:=Vector(311):"

#define dd "0"

#do sol=1,7

#do pow1=-6,6
#do pow2=-6,6

#$nonzero=0;
L tmpRow = DeltaS'sol';

if(count(tau1,1)!='pow1');
discard;
endif;

if(count(tau2,1)!='pow2');
discard;
endif;

id tau1=1;
id 1/tau1=1;
id tau2=1;
id 1/tau2=1;
.sort
$nonzero=$nonzero+1;
.sort
hide;

#write "'pow1','pow2','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "DvecS['dd']:=ds['pow1','pow2']:"

#do cc=1,'$totalcS'

L tmpEl'cc' = tmpRow;

id cc(x1?,?args)*ccS(x2?) = cc(x2,?args);
id cc('cc',?x) = 1;
id cc(?x)=0;
id ccS(?x) = 1;
.sort

#write <'topo'.reduceM.log> "ReduceMatrixS['dd','cc']:=%E:" tmpEl'cc'
.sort
hide;

#enddo

#endif

unhide;
.store
#enddo
#enddo
#enddo
*}}}

.end
