* vim: set foldmethod=marker:
#-
S tau1,...,tau10;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
CF TR5,ASYM(a);
auto S symb,a,b,p,x,P,h,s;
S tr5;
S mS;
S k1f,k2f,k3f,k4f,k1,k2;
S mu,mu11,mu22,mu12;
S l1,...,l20;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}
*{{{ OSprops
#procedure OSprops(nprops,p1,p2,p3,p4)

#do i=1,'nprops'
id l'i'*Props(p?,?x) = TMP(p)*Props(?x);
al Props(p?,?x) = Props(?x);
#enddo
id Props=1;
splitarg TMP;
repeat id TMP(p1?,p2?,?x) = TMP(p1)+TMP(p2,?x);
normalize TMP;
id TMP(p?) = MOM(p,mu);
id MOM(k1,mu) =
    a1*MOM('p1',mu)+a2*MOM('p2',mu)+a3*AB('p1',mu,'p2')/2+a4*AB('p2',mu,'p1')/2;
id MOM(k2,mu) =
    b1*MOM('p3',mu)+b2*MOM('p4',mu)+b3*AB('p3',mu,'p4')/2+b4*AB('p4',mu,'p3')/2;
#endprocedure
*}}}

off stats;

.global

#define prop "8"

#define topo "db320D"
#do z=1,'prop'
L Zero'z' = l'z'^2-xxMass'z';
#enddo
id l1?ls = l1*
  Props( k1 , k1-p1 , k1-p1-p2, k1-p1-p2-p3 , k2-p4-p5 , k2-p5 , k2 , k1+k2 );

#call OSprops(8,p1,p2,p4,p5);

id xxMass1 = mu11;
id xxMass2 = mu11;
id xxMass3 = mu11;
id xxMass4 = mu11;
id xxMass5 = mu22;
id xxMass6 = mu22;
id xxMass7 = mu22;
id xxMass8 = mu11+mu12+mu22;

id a3 = B(p1,p3)*IB(p2,p3)*a3;
id a4 = B(p2,p3)*IB(p1,p3)*a4;
id b3 = B(p3,p4)*IB(p3,p5)*b3;
id b4 = B(p3,p5)*IB(p3,p4)*b4;

#write <final/db320D.OSprops.h> "id l1=k1;"
#write <final/db320D.OSprops.h> "id l2=k1-p1;"
#write <final/db320D.OSprops.h> "id l3=k1-p1-p2;"
#write <final/db320D.OSprops.h> "id l4=k2-p1-p2-p3;"
#write <final/db320D.OSprops.h> "id l5=k2-p4;"
#write <final/db320D.OSprops.h> "id l6=k2-p3-p4;"
#write <final/db320D.OSprops.h> "id l7=k2;"
#write <final/db320D.OSprops.h> "id l8=k1+k2;"
#write <final/db320D.OSprops.h> "id k1 = av1*a1+av2*a2+av3*a3+av4*a4;"
#write <final/db320D.OSprops.h> "id k2 = bv1*b1+bv2*b2+bv3*b3+bv4*b4;"
#write <final/db320D.OSprops.h> "id av1=p1;"
#write <final/db320D.OSprops.h> "id av2=p2;"
#write <final/db320D.OSprops.h> "id av3=B(p1,p3)*IB(p2,p3)*eta(p1,p2)/2;"
#write <final/db320D.OSprops.h> "id av4=B(p2,p3)*IB(p1,p3)*eta(p2,p1)/2;"
#write <final/db320D.OSprops.h> "id bv1=p4;"
#write <final/db320D.OSprops.h> "id bv2=p5;"
#write <final/db320D.OSprops.h> "id bv3=B(p3,p4)*IB(p3,p5)*eta(p4,p5)/2;"
#write <final/db320D.OSprops.h> "id bv4=B(p3,p5)*IB(p3,p4)*eta(p5,p4)/2;"

#call lprod
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
#call cancel

#do i=1,5
#do j=1,5
#if 'j'>'i'
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
#endif
#enddo
#enddo

id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
repeat id AB(?x,p1?)*AB(p1?,?y) = AB(?x,p1,?y);

id AB(p1?,p2?,p3?,p1?,p5?,p6?,p1?) = -AB(p1,p2,p3,p5,p1,p6,p1)+S(p1,p5)*AB(p1,p2,p3,p6,p1);
id AB(p1?,p2?,p3?,p5?,p1?,p6?,p1?) = S(p1,p6)*AB(p1,p2,p3,p5,p1);
id AB(p1?,p2?,p3?,p4?,p1?) = S(p1,p2)*S(p3,p4)-S(p1,p3)*S(p2,p4)+S(p1,p4)*S(p2,p3) + TR5(p1,p2,p3,p4);
id TR5(?x,p5,?y) = -TR5(?x,p1,?y)-TR5(?x,p2,?y)-TR5(?x,p3,?y)-TR5(?x,p4,?y);
id TR5(?x,p1?,?y,p1?,?z) = 0;
id TR5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = tr5;

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

B a1,...,a4,b1,...,b4,tr5,mu11,mu12,mu22;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

id NUM(x?) = x;

.sort
hide;
.sort
unhide Zero8;
multiply s13*s35*s34*s23;
.sort
unhide;

id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;

.sort
format 150;
#write <db320D.geneqs.m2> "R=QQ[a1,a2,a3,a4,b1,b2,b3,b4,mu11,mu12,mu22,s12,s23,s34,s45,s15,tr5]"
#write <db320D.geneqs.m2> "I=ideal("
#do z=1,{'prop'-1}
#write <db320D.geneqs.m2> "%E," Zero'z'
#enddo
#write <db320D.geneqs.m2> "%E)" Zero'prop'
#write <db320D.geneqs.m2> "soleq = primaryDecomposition(I)"
#write <db320D.geneqs.m2> "\"db320D.eqs.out\"<<\"nsol:=\"<<#soleq<<\":\"<<endl"
#write <db320D.geneqs.m2> "\"db320D.eqs.out\"<<\"eqs:=Array[0..\"<<#soleq-1<<\"]:\"<<endl"
#write <db320D.geneqs.m2> "for i from 0 to #soleq-1 do \"db320D.eqs.out\"<<i<<toString(soleq#i)<<endl"

format mathematica;
#write "OSeqsdb320 = {"
#write "%E==0," Zero1
#write "%E==0," Zero2
#write "%E==0," Zero3
#write "%E==0," Zero4
#write "%E==0," Zero5
#write "%E==0," Zero6
#write "%E==0," Zero7
#write "%E==0" Zero8
#write "}"

*format 150;
*B a1,...,a4,b1,...,b4;
*print+s;
.store

.end
