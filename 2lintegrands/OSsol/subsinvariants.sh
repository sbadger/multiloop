#! /bin/sh
for f in *.OSsol.h
do
  sed -e "s/11/N(11)/g" \
      -e "s/14/N(14)/g" \
      -e "s/196/N(196)/g" \
      -e "s/121/N(121)/g" \
      -e "s/\([^ab=3]\)3\(3*\)/\1N(3\2)/g" $f > $f.tmp
      mv $f.tmp $f
done
