(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];

(* set Ds = 4 for FDH scheme *)
Dsm2 = 2;

INTdb220mu[MBrepr_] := {
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,3},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,3,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,3,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,2,1,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,2,2,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,3,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,3,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,3,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{3,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (- 8*(1 + 2*eps)*INT[6 - 2*eps,{1,1,1,1,1,1,1},MBrepr]*eps),
       MBresolve[#,eps] & /@ (+ 3*INT[6 - 2*eps,{1,1,1,1,1,1,1},MBrepr]*eps^2*Dsm2)
       };

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];

invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> -s12/2, p[1].p[3] -> -(-s12-s23)/2, p[1].p[4] -> -s23/2,
      p[2].p[3] -> -s23/2, p[2].p[4] -> -(-s23-s12)/2,
      p[3].p[4] -> -s12/2};

kinpt = {
  {s12->2., s23->3.}
};


db220s = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[1],0,a2] PR[l[1]-p[1]-p[2],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[4],0,a5] PR[l[2]-p[3]-p[4],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

db220t = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[2],0,a2] PR[l[1]-p[2]-p[3],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[1],0,a5] PR[l[2]-p[4]-p[1],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

box = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[1],0,a2] PR[l[1]-p[1]-p[2],0,a3] PR[l[1]+p[4],0,a4]},
	{l[1]}, Invariants->invariants];

db220sF = INTdb220mu[db220s];
db220tF = INTdb220mu[db220t];
boxF = {MBresolve[#,eps] & /@ (-Dsm2*eps*(1-eps)*INT[8-2*eps,{1,1,1,1},box])};


MBexpand[db220sF,1/cGamma2,{eps,0,0}];
Rdb220s = MBintegrate[%, kinpt[[1]], FixedContours->True]

MBexpand[db220tF,1/cGamma2,{eps,0,0}];
Rdb220t = MBintegrate[%, kinpt[[1]], FixedContours->True]


MBexpand[boxF,1/cGamma1,{eps,0,2}];
Rbox = MBintegrate[%, kinpt[[1]], FixedContours->True]

A52 = -s12^2*s23*Rdb220s[[1]] - s23^2*s12*Rdb220t[[1]] /. kinpt[[1]] // Chop // Expand
A51 = -s12*s23*Rbox[[1]] /. kinpt[[1]] // Chop // Expand
IR = -2/eps^2*(Exp[-eps*Log[s12]] + Exp[-eps*Log[s23]]) /. kinpt[[1]]

Series[A51*IR,{eps,0,0}]
Series[A52,{eps,0,0}]
Series[A51*IR - A52,{eps,0,0}]



