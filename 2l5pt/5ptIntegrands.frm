* vim: set sw=2:
* vim: set ft=form:
* vim: set foldmethod=marker:
#-
#define treepath "../amplib"
#include- ../lib/GU.hdr
#include- ../lib/nGUtools.h
#include- ../lib/nGU2tools.h
#include- ../lib/GUanalytic.h
#include- ../lib/factorise.h

CF SUSY,HEL,FFSvertex,FLTEST;
S Nf,Ns;

*{{{extraids
#procedure extraids
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
id IS(p1?ps,p2?ps) = IA(p1,p2)*IB(p2,p1);
#endprocedure
*}}}

off statistics;
.global

#do hlbl=0,31

L tmp = HEL('hlbl')*TMP;

#do i=4,1,-1
id HEL(x1?,?x) = HEL(x1-{2^'i'}*integer_(x1/2^{'i'}),integer_(x1/2^{'i'}),?x);
#enddo

repeat;
id HEL(x1?,?x)*TMP(?y) = HEL(?x)*TMP(?y,2*x1-1);
endrepeat;
id HEL = 1;
id TMP(?x) = HEL(?x);
id HEL(a1?$H1,a2?$H2,a3?$H3,a4?$H4,a5?$H5) = HEL(a1,a2,a3,a4,a5);

print+s;
.store

#write "*** 5-point 2-loop cut integrands ***"
#write "*** helicity = '$H1','$H2','$H3','$H4','$H5' ('hlbl') ***"

#do fl1={0,1/2,1}
#do fl2={0,1/2,1,-1/2}

*{{{ planar box|pentagon integrand [12*345*]
L C8boxpent1INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2
#do h8=-1,1,2

+TMPFL('fl1','fl2')*BoxPent(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom(ptl(p5,'$H5',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'),ptl(l8,'h8','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;

*}}}

*{{{ planar pentagon|box integrand [123*45*]
L C8boxpent2INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2
#do h8=-1,1,2

+TMPFL('fl1','fl2')*PentBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom(ptl(p3,'$H3',0)),
Kmom,
Kmom(ptl(p4,'$H4',0)),
Kmom(ptl(p5,'$H5',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl1'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'),ptl(l8,'h8','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;

*}}}

*{{{ planar dbox integrand [12*34*]
L C7dbox1INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0),ptl(p5,'$H5',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

*{{{ planar dbox integrand [12*34[5]]
L C7dbox2INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom(ptl(p5,'$H5',0)),
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

*{{{ planar dbox integrand [12*35*]
L C7dbox3INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom,
Kmom(ptl(p3,'$H3',0),ptl(p4,'$H4',0)),
Kmom(ptl(p5,'$H5',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

*{{{ planar dbox integrand [12[3]45*]
L C7dbox4INT =

#do h1=-1,1,2
#do h2=-1,1,2
#do h3=-1,1,2
#do h4=-1,1,2
#do h5=-1,1,2
#do h6=-1,1,2
#do h7=-1,1,2

+TMPFL('fl1','fl2')*BoxBox(
Kmom(ptl(p1,'$H1',0)),
Kmom(ptl(p2,'$H2',0)),
Kmom(ptl(p3,'$H3',0)),
Kmom(ptl(p4,'$H4',0)),
Kmom(ptl(p5,'$H5',0)),
Kmom,
ptl(l1,'h1','fl1'),ptl(l2,'h2','fl1'),ptl(l3,'h3','fl1'),ptl(l4,'h4','fl2'),
ptl(l5,'h5','fl2'),ptl(l6,'h6','fl2'),ptl(l7,'h7','fl2'-'fl1')
)

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
*}}}

id TMPFL(0,0) = 1;
id TMPFL(1/2,0) = -Nf;
id TMPFL(0,1/2) = -Nf;
id TMPFL(1/2,1/2) = -Nf;
id TMPFL(1,0) = Ns;
id TMPFL(0,1) = Ns;
id TMPFL(1,1) = Ns;
id TMPFL(1/2,1) = Nf*Ns/2;
id TMPFL(1,1/2) = Nf*Ns/2;
id TMPFL(1/2,-1/2) = Nf*Ns/2;
id TMPFL(?x) = 0;
.sort

#call topologise2
#call subtreesGU

#call sortminusl
#call sortminusl
.sort

format maple;
#write <C8.boxpent1.INT.log> "C8_boxpent1_INT['hlbl','fl1','fl2']:=\n%e\n" C8boxpent1INT
#write <C8.boxpent2.INT.log> "C8_boxpent2_INT['hlbl','fl1','fl2']:=\n%e\n" C8boxpent2INT
#write <C7.dbox1.INT.log> "C7_dbox1_INT['hlbl','fl1','fl2']:=\n%e\n" C7dbox1INT
#write <C7.dbox2.INT.log> "C7_dbox2_INT['hlbl','fl1','fl2']:=\n%e\n" C7dbox2INT
#write <C7.dbox3.INT.log> "C7_dbox3_INT['hlbl','fl1','fl2']:=\n%e\n" C7dbox3INT
#write <C7.dbox4.INT.log> "C7_dbox4_INT['hlbl','fl1','fl2']:=\n%e\n" C7dbox4INT

format 180;
B TMPFL,AMP;
print[];
.store
#enddo
#enddo

#enddo
.end

