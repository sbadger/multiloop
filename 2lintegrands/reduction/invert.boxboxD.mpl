interface(quiet=true):
interface(rtablesize=infinity):
with(LinearAlgebra):

#with(spinors);
#assign(declaremomn([0,0,0,0])):

# solving planar double box in 4-2e dimensions

IS(p1,p2) := 1/S(p1,p2):
IS(p1,p3) := 1/S(p1,p3):
IS(p1,p4) := 1/S(p1,p4):
S(p1,p3) := -S(p1,p4)-S(p1,p2):

read("boxboxD.reduceM.log"):
unassign('Dvec'):

dvec:=Vector(160):
for k from 1 to 160 do:
dvec[k]:=Dvec[k]:
od:

eqs:=simplify(ReduceMatrix):
Rank(eqs);

eqsP,eqsL,eqsU:=LUDecomposition(eqs):
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL)):
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(eqsU);
Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dvec:

dvecR:=dvecT:
Cvec:=Ieqs.dvecR;

fd:=fopen("final/boxboxD.integrand.mpl",WRITE):

fprintf(fd,"Cvec:=Vector(160):\n"):
for k from 1 to 160 do:
fprintf(fd,"Cvec[%d]:=%A;\n",k,simplify(Cvec[k])):
od:

fclose(fd):
