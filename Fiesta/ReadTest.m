(* ::Package:: *)

db320 = SeriesData[ep, 0, {-0.222224 - 8.*^-6*pm7143 - 8.*^-6*pm9154,
  -0.06286000000000008 - 4.*^-6*pm10927 - 4.*^-6*pm6328 + 8.*^-6*pm7143 -
   0.000052*pm7144 - 8.*^-6*pm8393 + 8.*^-6*pm9154 - 0.000052*pm9155,
  -0.15679999999999994 - 4.*^-6*pm10286 + 4.*^-6*pm10927 - 0.000056*pm10928 -
   0.000012*pm11570 + 4.*^-6*pm6328 - 0.000016*pm6329 + 0.000052*pm7144 -
   0.000276*pm7145 + 8.*^-6*pm8393 - 0.000128*pm8394 + 0.000052*pm9155 -
   0.000352*pm9156}, -2, 1, 1]

db320T = SeriesData[ep, 0, {4.684200000000001 + 0.000048*pm18714 + 0.000168*pm20725 +
   8.*^-6*pm26845, 1.983500000000001 + 0.000024*pm17899 - 0.000048*pm18714 +
   0.000672*pm18715 + 0.00016*pm19964 - 0.000168*pm20725 + 0.001512*pm20726 +
   0.000136*pm22498 + 0.000016*pm23140 + 0.000016*pm24776 + 4.*^-6*pm26428 -
   8.*^-6*pm26845 + 0.000056*pm26846 + 4.*^-6*pm27449,
  2.132667999999999 + 8.*^-6*pm14761 + 0.000024*pm17204 - 0.000024*pm17899 +
   0.000112*pm17900 - 0.000672*pm18715 + 0.002312*pm18716 +
   0.000032*pm19281 - 0.00016*pm19964 + 0.001888*pm19965 - 0.001512*pm20726 +
   0.011248*pm20727 + 0.000096*pm21857 - 0.000136*pm22498 +
   0.001664*pm22499 - 0.000016*pm23140 + 0.00024*pm23141 + 4.*^-6*pm24013 +
   8.*^-6*pm24368 - 0.000016*pm24776 + 0.000084*pm24777 + 8.*^-6*pm25378 +
   4.*^-6*pm25705 + 0.000016*pm26067 - 4.*^-6*pm26428 + 0.00002*pm26429 -
   0.000056*pm26846 + 0.000216*pm26847 + 4.*^-6*pm27121 - 4.*^-6*pm27449 +
   0.00002*pm27450 + 0.000016*pm27779}, -2, 1, 1]

pmvars = Names["Global`*"];
pmvars = Complement[pmvars,{"Datapath", "Dsm2", "ep", "intdb220LList", "intdb220M1List", "intdb220M2List", "intdb320List", "intdb320TList", "M2path", "pmvars", "db320", "db320T"}];

replacepm = Table[ToExpression[pmvars[[k]]]->xxx,{k,1,Length[pmvars]}];
removepm = Table[ToExpression[pmvars[[k]]]->0,{k,1,Length[pmvars]}];

Sq[x_]:=x^2;
Add[x_,y_,z___]:=Add[x+y,z];
Add[x_] := Sqrt[x];

GetError[expr_]:=Module[{err,epm2,epm1,ep0},
ep0  = Coefficient[expr/ep ,1/ep];
epm1 = Coefficient[expr,1/ep];
epm2 = Coefficient[expr,1/ep^2];
ep0  = Apply[List,ep0];
epm1 = Apply[List,epm1];
epm2 = Apply[List,epm2];
ep0  = ep0 /. replacepm;
ep0  = xxx*ep0;
ep0  = ep0 /. {xxx^2->1, xxx->0};
epm1 = epm1 /. replacepm;
epm1 = xxx*epm1;
epm1 = epm1 /. {xxx^2->1, xxx->0};
epm2 = epm2 /. replacepm;
epm2 = xxx*epm2;
epm2 = epm2 /. {xxx^2->1, xxx->0};
ep0  = Map[Sq,ep0];
ep0  = ep0/. {ep0[[0]]->Add};
epm1 = Map[Sq,epm1];
epm1 = epm1/. {epm1[[0]]->Add};
epm2 = Map[Sq,epm2];
epm2 = epm2/. {epm2[[0]]->Add};
{epm2,epm1,ep0}
]

GetValue[expr0_]:=Module[{expr,ep0,epm1,epm2},
expr = expr0/. removepm;
ep0  = Coefficient[expr/ep ,1/ep];
epm1 = Coefficient[expr,1/ep];
epm2 = Coefficient[expr,1/ep^2];
{epm2,epm1,ep0}
]

Dsm2 = 2;

GetValue[db320]
GetError[db320]

GetValue[db320T]
GetError[db320T]
