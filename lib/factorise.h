* vim: foldmethod=marker:
* vim: ft=form:

#define flist "{naA,naB,AA,BB,AB,BA,S,SS,WRAP}"
CF Ratio,WRAP,IWRAP,ZZ,IZZ,eta;

*{{{ wrap
#procedure wrap(symb)
argument NUM,INV;
id 'symb'=WRAP('symb');
id 1/'symb'=IWRAP('symb');
endargument;
#endprocedure
*}}}

*{{{ unwrap
#procedure unwrap(symb)
argument NUM,INV;
id WRAP('symb')  = 'symb';
id IWRAP('symb') = 1/'symb';
endargument;
id WRAP('symb')  = 'symb';
id IWRAP('symb') = 1/'symb';
#endprocedure
*}}}

*{{{map2nonas
#procedure map2nonas

id A(px?,py?) = naA(px,py);
id IA(px?,py?) = InaA(px,py);
id B(px?,py?) = naB(px,py);
id IB(px?,py?) = InaB(px,py);

#do fun={naA,naB,InaA,InaB}
  #do i=2,10
    id 'fun'(e'i',px?) = -'fun'(px,e'i');
    id 'fun'(q'i',px?) = -'fun'(px,q'i');
    id 'fun'(p'i',px?) = -'fun'(px,p'i');
  #enddo
#enddo

#endprocedure
*}}}

*{{{map2as
#procedure map2as

id naA(px?,py?) = A(px,py);
id InaA(px?,py?) = IA(px,py);
id naB(px?,py?) = B(px,py);
id InaB(px?,py?) = IB(px,py);

#endprocedure
*}}}

*{{{cmom
#procedure cmom(b,ba,bb)
id A('b',px?) = A('ba',px);
id AB('b',?x) = AB('ba',?x);
id AA('b',?x) = AA('ba',?x);
id AA(?x,'b') = AA(?x,'ba');

id IA('b',px?) = IA('ba',px);
id IAB('b',?x) = IAB('ba',?x);
id IAA('b',?x) = IAA('ba',?x);
id IAA(?x,'b') = IAA(?x,'ba');

id B('b',px?) = B('bb',px);
id AB(?x,'b') = AB(?x,'bb');
id BB(?x,'b') = BB(?x,'bb');
id BB('b',?x) = BB('bb',?x);

id IB('b',px?) = IB('bb',px);
id IAB(?x,'b') = IAB(?x,'bb');
id IBB(?x,'b') = IBB(?x,'bb');
id IBB('b',?x) = IBB('bb',?x);

#endprocedure
*}}}

*{{{ orderMOM
#procedure orderMOM
argument A,B,IA,IB,AB,AA,BB,SS,IAB,IAA,IBB,ISS;
#do i={9,8,7,6,5,4,3,2,1}
id MOM(?x1,p'i',?x2) = MOM(p'i',?x1,?x2);
al MOM(?x1,P'i',?x2) = MOM(P'i',?x1,?x2);
#enddo
endargument;

#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
al S(?x1,P'i',?x2) = S(P'i',?x1,?x2);
al IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
al IS(?x1,P'i',?x2) = IS(P'i',?x1,?x2);
#enddo

#endprocedure
*}}}

*{{{ sortNUM
#procedure sortNUM

splitarg NUM;
id NUM(symb?) = symb; 
repeat id NUM(symb1?,symb2?,?x) = NUM(symb1+symb2,?x);

#endprocedure
*}}}

*{{{ sortINV
#procedure sortINV

splitarg INV;
id INV(symb?) = TMPINV(symb); 

argument TMPINV;
#call map2nonas

multiply TMPCOLLECT(1);

#do fun='flist'
repeat id 'fun'(?x)*TMPCOLLECT(symb?) = TMPCOLLECT(symb*I'fun'(?x));
repeat id I'fun'(?x)*TMPCOLLECT(symb?) = TMPCOLLECT(symb*'fun'(?x));
#enddo

id TMPCOLLECT(symb?) = symb;

#call map2as
endargument;

.sort

repeat;
id,once TMPINV(px?) = TMP2INV(px);  

factarg TMP2INV;
chainout TMP2INV;
id TMP2INV(A?{A,B,IA,IB}(p1?,p2?)) = A(p1,p2);
#do fun='flist'
id TMP2INV('fun'(?x)) = 'fun'(?x);
id TMP2INV(I'fun'(?x)) = I'fun'(?x);
#enddo
id TMP2INV(Ratio?(?x)) = TMP3INV(Ratio(?x));
id TMP2INV(symb?) = 1/symb;
chainin TMP3INV;
endrepeat;
id TMP3INV(px?) = INV(px);

repeat id INV(symb1?,symb2?,?x) = INV(symb1+symb2,?x);

#endprocedure
*}}}

*{{{ CollectDen
#procedure CollectDen

#call map2nonas

#do fun={naA,naB,InaA,InaB}
  #do i=2,10
    id 'fun'(e'i',px?) = -'fun'(px,e'i');
    id 'fun'(q'i',px?) = -'fun'(px,q'i');
    id 'fun'(p'i',px?) = -'fun'(px,p'i');
  #enddo
#enddo

id,once AA?(?x) = Ratio(1,1)*AA(?x);

#do fun='flist'
repeat id Ratio(symb1?,symb2?)*'fun'(?x3) = Ratio(symb1*'fun'(?x3),symb2);
repeat id Ratio(symb1?,symb2?)*I'fun'(?x3) = Ratio(symb1,symb2*'fun'(?x3));
#enddo

if(count(Ratio,1)=0);
multiply Ratio(1,1);
endif;

argument Ratio;
#call map2as
endargument;
#endprocedure

*}}}

*{{{ CommonDen
#procedure CommonDen

.sort
id NUM(symb?) = symb;
id Ratio(symb1?,symb2?) = symb1*Ratio(1,symb2);
id Ratio(1,symb?) = INV(symb);
#call sortINV

while(count(NUM,1)>0);
id once, NUM(?x) = TMP(?x)*TMP2()*TMPINV(1);

chainout TMP;
factarg,(-1),TMP;
while(count(TMP,1)>0);
  id,once TMP(Ratio(symb1?$expr1,symb2?$expr2),symbc?)*TMPINV(symb3?) = TMPINV(symb3*symb2)*TMPCOEFF(symbc);
  al TMP(Ratio(symb1?$expr1,symb2?$expr2))*TMPINV(symb3?) = TMPINV(symb3*symb2)*TMPCOEFF(1);
  argument TMP,TMP2;
    id Ratio(symb1?,symb2?) = Ratio(symb1*$expr2,symb2);
  endargument;
  id TMP2(?x)*TMPCOEFF(symb?) = TMP2(?x,symb*Ratio($expr1,1));
endwhile;

argument TMP2;
id Ratio(symb?,1) = symb;
endargument;

repeat id TMP2(symb1?,symb2?,?x) = TMP2(symb1+symb2,?x);
id TMP2(?x)= TMPout(?x);
id TMPINV(?x) = INV(?x);
endwhile;

id TMPout(?x) = NUM(?x);

#call sortINV
.sort

#endprocedure
*}}}

*{{{ ICommonDen
#procedure ICommonDen

.sort
id Ratio(symb1?,symb2?) = symb1*Ratio(1,symb2);
id Ratio(1,symb?) = INV(symb);

while(count(INV,1)>0);
id once, INV(?x) = TMP(?x)*TMP2()*TMPNUM(1);

chainout TMP;
factarg,(-1),TMP;
while(count(TMP,1)>0);
  id,once TMP(Ratio(symb1?$expr1,symb2?$expr2),symbc?)*TMPNUM(symb3?) = TMPNUM(symb3*symb2)*TMPCOEFF(symbc);
  al TMP(Ratio(symb1?$expr1,symb2?$expr2))*TMPNUM(symb3?) = TMPNUM(symb3*symb2)*TMPCOEFF(1);
  argument TMP,TMP2;
    id Ratio(symb1?,symb2?) = Ratio(symb1*$expr2,symb2);
  endargument;
  id TMP2(?x)*TMPCOEFF(symb?) = TMP2(?x,symb*Ratio($expr1,1));
endwhile;

argument TMP2;
id Ratio(symb?,1) = symb;
endargument;

repeat id TMP2(symb1?,symb2?,?x) = TMP2(symb1+symb2,?x);
id TMP2(?x)= TMPout(?x);
id TMPNUM(?x) = NUM(?x);
endwhile;

id TMPout(?x) = INV(?x);

#call sortNUM
.sort

#endprocedure
*}}}

*{{{ factorise
#procedure factorise(TMP)
#call map2nonas
argument 'TMP';
#call map2nonas
endargument;

#do FN='flist'
repeat;
  id,once 'TMP'(?a)=poly'TMP'tmp(?a);
  repeat;
    $f=0;
    $tf=0;
    argument poly'TMP'tmp;
      if ( $f == 0 );
        if ( match(once, `FN'(?arg$a) ) > 0  );
          $f=$f+1;
        endif;
      else;
        if ( match(once, `FN'($a) ) > 0  );
          $f=$f+1;
        endif;
      endif;
      $tf=$tf+1;
    endargument;

    if ($f > 0);
      if ($f == $tf);
        id poly'TMP'tmp(?arg)=poly'TMP'(`FN'($a))*poly'TMP'tmp(?arg);
        argument poly'TMP'tmp;
          once `FN'($a)=1;
        endargument;
      else;
        argument poly'TMP'tmp;
          id `FN'($a)=TMP`FN'($a);
        endargument;
      endif;
    else;
      once poly'TMP'tmp(?arg)=poly'TMP'(?arg);
    endif;
  endrepeat;
endrepeat;
.sort
id poly'TMP'(?arg)='TMP'(?arg);
.sort
argument 'TMP';
  id TMP'FN'(?a)='FN'(?a);
endargument;
#enddo

argument 'TMP';
#call map2as
endargument;
#call map2as

#endprocedure
*}}}

*{{{ schouten
#procedure schouten(TMP)

argument 'TMP';
#call map2nonas
endargument;

id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),naA(p1?,p4?)*naA(p2?,p3?)) = 'TMP'(naA(p1,p3)*naA(p2,p4));
id 'TMP'(-naA(p1?,p2?)*naA(p3?,p4?),-naA(p1?,p4?)*naA(p2?,p3?)) = -'TMP'(naA(p1,p3)*naA(p2,p4));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),-naA(p1?,p4?)*naA(p3?,p2?)) = 'TMP'(naA(p1,p3)*naA(p2,p4));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),-naA(p4?,p1?)*naA(p2?,p3?)) = 'TMP'(naA(p1,p3)*naA(p2,p4));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),naA(p4?,p1?)*naA(p3?,p2?)) = 'TMP'(naA(p1,p3)*naA(p2,p4));

id 'TMP'(-naA(p1?,p2?)*naA(p3?,p4?),-naA(p2?,p4?)*naA(p3?,p1?)) = -'TMP'(naA(p1,p4)*naA(p3,p2));
id 'TMP'(-naA(p1?,p2?)*naA(p3?,p4?),naA(p2?,p4?)*naA(p1?,p3?)) = -'TMP'(naA(p1,p4)*naA(p3,p2));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),naA(p2?,p4?)*naA(p3?,p1?)) = 'TMP'(naA(p1,p4)*naA(p3,p2));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),-naA(p2?,p4?)*naA(p1?,p3?)) = 'TMP'(naA(p1,p4)*naA(p3,p2));
id 'TMP'(-naA(p1?,p2?)*naA(p3?,p4?),-naA(p4?,p2?)*naA(p1?,p3?)) = -'TMP'(naA(p1,p4)*naA(p3,p2));
id 'TMP'(naA(p1?,p2?)*naA(p3?,p4?),naA(p4?,p2?)*naA(p1?,p3?)) = 'TMP'(naA(p1,p4)*naA(p3,p2));

id 'TMP'(naB(p1?,p2?)*naB(p3?,p4?),naB(p1?,p4?)*naB(p2?,p3?)) = 'TMP'(naB(p1,p3)*naB(p2,p4));
id 'TMP'(-naB(p1?,p2?)*naB(p3?,p4?),-naB(p1?,p4?)*naB(p2?,p3?)) = -'TMP'(naB(p1,p3)*naB(p2,p4));
id 'TMP'(naB(p1?,p2?)*naB(p3?,p4?),-naB(p1?,p4?)*naB(p3?,p2?)) = 'TMP'(naB(p1,p3)*naB(p2,p4));
id 'TMP'(naB(p1?,p2?)*naB(p3?,p4?),-naB(p4?,p1?)*naB(p2?,p3?)) = 'TMP'(naB(p1,p3)*naB(p2,p4));
id 'TMP'(naB(p1?,p2?)*naB(p3?,p4?),naB(p4?,p1?)*naB(p3?,p2?)) = 'TMP'(naB(p1,p3)*naB(p2,p4));

id 'TMP'(naA(p1?,p2?)*AB(p3?,?x),naA(p2?,p3?)*AB(p1?,?x)) = -'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(naA(p1?,p2?)*AB(p3?,?x),-naA(p3?,p2?)*AB(p1?,?x)) = -'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(-naA(p2?,p1?)*AB(p3?,?x),naA(p2?,p3?)*AB(p1?,?x)) = -'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(-naA(p2?,p1?)*AB(p3?,?x),-naA(p3?,p2?)*AB(p1?,?x)) = -'TMP'(naA(p3,p1)*AB(p2,?x));

id 'TMP'(AB(?x,p1?)*naB(p2?,p3?),AB(?x,p2?)*naB(p3?,p1?)) = -'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(AB(?x,p1?)*naB(p2?,p3?),-AB(?x,p2?)*naB(p1?,p3?)) = -'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(-AB(?x,p1?)*naB(p3?,p2?),AB(?x,p2?)*naB(p3?,p1?)) = -'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(-AB(?x,p1?)*naB(p3?,p2?),-AB(?x,p2?)*naB(p1?,p3?)) = -'TMP'(AB(?x,p3)*naB(p1,p2));

id 'TMP'(-naA(p1?,p2?)*AB(p3?,?x),-naA(p2?,p3?)*AB(p1?,?x)) = 'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(-naA(p1?,p2?)*AB(p3?,?x),naA(p3?,p2?)*AB(p1?,?x)) = 'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(naA(p2?,p1?)*AB(p3?,?x),-naA(p2?,p3?)*AB(p1?,?x)) = 'TMP'(naA(p3,p1)*AB(p2,?x));
id 'TMP'(naA(p2?,p1?)*AB(p3?,?x),naA(p3?,p2?)*AB(p1?,?x)) = 'TMP'(naA(p3,p1)*AB(p2,?x));

id 'TMP'(-AB(?x,p1?)*naB(p2?,p3?),-AB(?x,p2?)*naB(p3?,p1?)) = 'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(-AB(?x,p1?)*naB(p2?,p3?),AB(?x,p2?)*naB(p1?,p3?)) = 'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(AB(?x,p1?)*naB(p3?,p2?),-AB(?x,p2?)*naB(p3?,p1?)) = 'TMP'(AB(?x,p3)*naB(p1,p2));
id 'TMP'(AB(?x,p1?)*naB(p3?,p2?),AB(?x,p2?)*naB(p1?,p3?)) = 'TMP'(AB(?x,p3)*naB(p1,p2));

id 'TMP'(naA(p1?,p2?)*AA(p3?,?x),naA(p2?,p3?)*AA(p1?,?x)) = -'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(naA(p1?,p2?)*AA(p3?,?x),-naA(p3?,p2?)*AA(p1?,?x)) = -'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(-naA(p2?,p1?)*AA(p3?,?x),naA(p2?,p3?)*AA(p1?,?x)) = -'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(-naA(p2?,p1?)*AA(p3?,?x),-naA(p3?,p2?)*AA(p1?,?x)) = -'TMP'(naA(p3,p1)*AA(p2,?x));

id 'TMP'(-naA(p1?,p2?)*AA(p3?,?x),-naA(p2?,p3?)*AA(p1?,?x)) = 'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(-naA(p1?,p2?)*AA(p3?,?x),naA(p3?,p2?)*AA(p1?,?x)) = 'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(naA(p2?,p1?)*AA(p3?,?x),-naA(p2?,p3?)*AA(p1?,?x)) = 'TMP'(naA(p3,p1)*AA(p2,?x));
id 'TMP'(naA(p2?,p1?)*AA(p3?,?x),naA(p3?,p2?)*AA(p1?,?x)) = 'TMP'(naA(p3,p1)*AA(p2,?x));

argument 'TMP';
#call map2as
id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
endargument;

id 'TMP'(AB(p1?,p2?,p4?),AB(p1?,p3?,p4?)) = 'TMP'(AB(p1,p2+p3,p4));

#endprocedure
*}}}

*{{{ SortCollect
#procedure SortCollect
argument NUM;
#call CollectDen
endargument;
splitarg NUM;
#call CommonDen

argument NUM;
topolynomial;
endargument;
.sort
factarg NUM;
.sort
argument NUM;
frompolynomial;
endargument;
chainout NUM;
#call sortNUM;
.sort

splitarg NUM;
#call schouten(NUM);

id NUM(symb?) = symb;
repeat id NUM(symb1?,symb2?,?x) = NUM(symb1+symb2,?x);
#call cancel

#endprocedure
*}}}

*{{{ ISortCollect
#procedure ISortCollect

B INV;
print[];
.sort

*argument INV;
*#call CollectDen
*endargument;
*splitarg INV;
*#call ICommonDen

*argument INV;
*topolynomial;
*endargument;
*.sort
*factarg INV;
*.sort
*argument INV;
*frompolynomial;
*endargument;
*chainout INV;
*#call sortINV;
*.sort

*splitarg INV;
*#call schouten(INV);
*#call sortINV;

#endprocedure
*}}}

*{{{ compress
#procedure compress(extra)

#do fun={AB,AA,BB,IAB,IAA,IBB,SS,ISS}

repeat;
  id once 'fun'(p1?,?x) = TMPout'fun'(MOM(p1))*TMPin'fun'(?x);
  repeat; 
    id TMPout'fun'(?x1)*TMPin'fun'(p1?,?x2) = TMPout'fun'(?x1,MOM(p1))*TMPin'fun'(?x2);
    id TMPout'fun'(?x1)*TMPin'fun'(MOM(?yy),?x2) = TMPout'fun'(?x1,MOM(?yy))*TMPin'fun'(?x2);
  endrepeat;
  id TMPin'fun'=1;
  argument TMPout'fun';
    splitarg MOM;
    id MOM(p1?) = p1;
  endargument;
  id TMPout'fun'(?x) = TMP'fun'(?x);
endrepeat;

id TMP'fun'(?x) = 'fun'(?x);

id 'fun'(p1?ps,MOM(?y1,p1?ps,?y2),?x) = 'fun'(p1,MOM(?y1,?y2),?x);
id 'fun'(?x,MOM(p1?ps,?y2),p1?ps) = 'fun'(?x,MOM(?y2),p1);
id 'fun'(?x,MOM(?y1,p1?ps,?y2),p1?ps) = 'fun'(?x,MOM(?y1,?y2),p1);
#if('extra'=1)
#do kk1=1,6
  #do kk2=1,4
    #do kk3=1,4
     id 'fun'(px1?,...,px'kk1'?,MOM(py1?,...,py'kk2'?,p1?ps,pz1?,...,pz'kk3'?),p1?ps) = 
    	'fun'(px1,...,px'kk1',MOM(py1,...,py'kk2',pz1,...,pz'kk3'),p1);
    #enddo
  #enddo
#enddo
#endif

argument 'fun';
id MOM(p1?) = p1;
endargument;

#enddo

#endprocedure
*}}}

*{{{ reverseBA
#procedure reverseBA

id BA(p1?,p2?,p3?) = AB(p3,p2,p1);
id BA(p1?,p2?,p3?,p4?,p5?) = AB(p5,p4,p3,p2,p1);
id BA(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = AB(p7,p6,p5,p4,p3,p2,p1);
id IBA(p1?,p2?,p3?) = IAB(p3,p2,p1);
id IBA(p1?,p2?,p3?,p4?,p5?) = IAB(p5,p4,p3,p2,p1);
id IBA(p1?,p2?,p3?,p4?,p5?,p6?,p7?) = IAB(p7,p6,p5,p4,p3,p2,p1);

#endprocedure
*}}}

*{{{ expandargs
#procedure expandargs
argument AB,AA,BB,IAB,IAA,IBB;
repeat id MOM(p1?,?x) = p1+MOM(?x);
id MOM = 0;
endargument;

repeat, id S(p1?,p2?,?x) = S(p1+p2,?x);
repeat, id IS(p1?,p2?,?x) = IS(p1+p2,?x);
repeat, id SS(p1?,p2?,?x,mS?) = SS(p1+p2,?x,mS);
repeat, id ISS(p1?,p2?,?x,mS?) = ISS(p1+p2,?x,mS);

#endprocedure
*}}}

*{{{ breakstrings
#procedure breakstrings

repeat;
  id,once AB(p1?,?x) = oTMP(1,p1)*oTMPAB(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPAB(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPAB(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPAB(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPAB(?y);
  endrepeat;
  id oTMPAB=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPAB(?x)*TMPAB(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPAA(?x)*TMPBB(?y);
  id oTMP(h1?odd_,?x) = TMPAB(?x);
endrepeat;

repeat;
  id,once IAB(p1?,?x) = oTMP(1,p1)*oTMPIAB(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPIAB(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPIAB(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPIAB(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPIAB(?y);
  endrepeat;
  id oTMPIAB=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPIAB(?x)*TMPIAB(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPIAA(?x)*TMPIBB(?y);
  id oTMP(h1?odd_,?x) = TMPIAB(?x);
endrepeat;

repeat;
  id,once AA(p1?,?x) = oTMP(1,p1)*oTMPAA(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPAA(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPAA(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPAA(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPAA(?y);
  endrepeat;
  id oTMPAA=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPAB(?x)*TMPAA(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPAA(?x)*TMPBA(?y);
  id oTMP(h1?even_,?x) = TMPAA(?x);
endrepeat;

repeat;
  id,once BB(p1?,?x) = oTMP(1,p1)*oTMPBB(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPBB(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPBB(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPBB(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPBB(?y);
  endrepeat;
  id oTMPBB=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPBA(?x)*TMPBB(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPBB(?x)*TMPAB(?y);
  id oTMP(h1?even_,?x) = TMPBB(?x);
endrepeat;

repeat;
  id,once IAA(p1?,?x) = oTMP(1,p1)*oTMPIAA(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPIAA(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPIAA(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPIAA(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPIAA(?y);
  endrepeat;
  id oTMPIAA=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPIAB(?x)*TMPIAA(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPIAA(?x)*TMPIBA(?y);
  id oTMP(h1?even_,?x) = TMPIAA(?x);
endrepeat;

repeat;
  id,once IBB(p1?,?x) = oTMP(1,p1)*oTMPIBB(?x);
  repeat;
    id oTMP(h1?,?x)*oTMPIBB(p1?ps) = oTMP(h1+1,?x,p1);
    id oTMP(h1?,?x)*oTMPIBB(p1?ps,?y) = oTMP(h1+1,?x,p1)*oTMP(h1,p1,?y);
    id oTMP(h1?,?x)*oTMPIBB(symb1?,?y) = oTMP(h1+1,?x,symb1)*oTMPIBB(?y);
  endrepeat;
  id oTMPIBB=1;
  id oTMP(h1?odd_,?x)*oTMP(h2?even_,?y) = TMPIBA(?x)*TMPIBB(?y);
  id oTMP(h1?even_,?x)*oTMP(h2?odd_,?y) = TMPIBB(?x)*TMPIAB(?y);
  id oTMP(h1?even_,?x) = TMPIBB(?x);
endrepeat;

#do fun={AB,AA,BB,BA,IAB,IAA,IBB,IBA}
id TMP'fun'(?x) = 'fun'(?x);
#enddo

id AA(p1?,p2?) = A(p1,p2);
id BB(p1?,p2?) = B(p1,p2);

id IAA(p1?,p2?) = IA(p1,p2);
id IBB(p1?,p2?) = IB(p1,p2);

#call reverseBA

#endprocedure
*}}}

*{{{ cancel
#procedure cancel
 
id AA(p1?,p2?) = A(p1,p2);
id BB(p1?,p2?) = B(p1,p2);
id IAA(p1?,p2?) = IA(p1,p2);
id IBB(p1?,p2?) = IB(p1,p2);

id ISS(MOM(?x),0) = IS(?x);
id ISS(MOM(?x),xm?) = ISS(?x,xm);
id SS(MOM(?x),0) = S(?x);
id SS(MOM(?x),xm?) = SS(?x,xm);
id S(p1?ps) = 0;
id SS(P1?mps,p2?ps,mS?) = AB(p2,P1,p2);
id ISS(P1?mps,p2?ps,mS?) = IAB(p2,P1,p2);
id SS(p1?ps,P2?mps,mS?) = AB(p1,P2,p1);
id ISS(p1?ps,P2?mps,mS?) = IAB(p1,P2,p1);

id ISS(?x,0) = IS(?x);
id SS(?x,0) = S(?x);

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id naA(?x)*InaA(?x) = 1;
id naB(?x)*InaB(?x) = 1;
id AB(?x)*IAB(?x) = 1;
id AA(?x)*IAA(?x) = 1;
id BB(?x)*IBB(?x) = 1;
id S(?x)*IS(?x) = 1;
id SS(?x)*ISS(?x) = 1;
id ZZ*IZZ = 1;

id NUM(?x)*INV(?x) = 1;

#do fun={AB,AA,BB}
id 'fun'(p1?,?x1,0,?x2,p2?) = 0;
id 'fun'(?x,p1?ps,p1?ps,?y) = 0;
id 'fun'(?x,MOM,?y) = 0;
#enddo

#do fun={AA,BB,IAA,IBB}
id 'fun'(p1?,P1?mps,MOM(?y1,P1?mps,?y2),p1?) = 'fun'(p1,P1,MOM(?y1,?y2),p1);
id 'fun'(p1?,MOM(?y1,P1?mps,?y2),P1?mps,p1?) = 'fun'(p1,MOM(?y1,?y2),P1,p1);
#enddo

#endprocedure
*}}}

