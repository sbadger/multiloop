interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 5 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d -> %.30f,\n",i,j,s||i||j):
  od:
od:

TR5 := tr5(p1,p2,p3,p4):
printf("TR5 -> %.30Zf};\n",TR5):

prefactor := IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):
printf("prefactor =  %.30Zf;\n", prefactor):

read("../db221D.ReduceM.log"):
MM:=ReduceMatrix:

for i from 1 to 83 do;
for j from 1 to 82 do:
  print(i,j,MM[i,j]);
od;
od;


iMM:=MatrixInverse(MM):

for i from 1 to 83 do;
for j from 1 to 82 do:
  print(i,j,iMM[i,j]);
od;
od;

quit;

on_shell_db221:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,bbb;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := A(p[1],p[3])/A(p[2],p[3])*eta(p[2],p[1])/2;

bv[1] := p[3];
bv[2] := p[4];
bv[3] := A(p[4],p[1])/A(p[3],p[1])*eta(p[3],p[4])/2;
bv[4] := A(p[3],p[1])/A(p[4],p[1])*eta(p[4],p[3])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

bbb := -s14*IAB(p4,p5,p3,p1,p4)*(
  s15 + s45
  + tau1*AB(p1,p5,p2,p3,p1)/s23
  + tau2*AB(p2,p5,p1,p3,p2)/s23
  + tau3*AB(p3,p5,p4,p1,p3)/s13
);

a := [1, 0, tau1*s13/s23, tau2]:
b := [0, 1, tau3, bbb]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := dot(k1,k1);
mu22 := dot(k2,k2);
mu12 := 2*dot(k1,k2);

return k1,k2,mu11,mu12,mu22;
end:

Ds:=Dsm2+2:
xxdiag1 := 1;
xxdiag2 := 2;
xxdiag3 := 2;
xxdiag4 := -2;

read("tmp/db221.input.mpl"):

INT_db221_analytic := proc(k1,k2,mu11,mu12,mu22)
local mu33,b0,b1,b2,b3;
global s12,s23,s34,s45,s15,p1,p2,p3,p4,p5,TR5;

mu33:=mu11+mu12+mu22:

b0 := 0;
b1 := - 2*AB(p2,p3,p4,p5,p2)*s15;
b2 := + 2*AB(p2,p3,p5,p1,p2)*s45;
b3 := 2*(s15*s23*s45 - AB(p2,p3,p4,p5,p2)*s15 - AB(p2,p3,p5,p1,p2)*s45);

return I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*s12*s34/TR5*
    ((Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-4*mu11*mu22))*
    (b0 + b1*dot(k1,p4) + b2*dot(k2,p1) + b3*dot(k1,p5));

end:


for k1 from 0 to 6 do:
for k2 from 0 to 6 do:
for k3 from 0 to 6 do:
ds(k1,k2,k3):=0:
od:
od:
od:

for th1 from 0 to 6 do;
for th2 from 0 to 6 do;
for th3 from 0 to 6 do;
printf("%d%d%d\n",th1,th2,th3);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/7);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/7);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/7);

k1,k2,mu11,mu12,mu22 := on_shell_db221([p1,p2,p3,p4,p5],t1,t2,t3):

val := INT_db221(k1,k2,mu11,mu12,mu22);
val2 := INT_db221_analytic(k1,k2,mu11,mu12,mu22);

for k1 from 0 to 6 do:
for k2 from 0 to 6 do:
for k3 from 0 to 6 do:
  ds(k1,k2,k3) := expand(ds(k1,k2,k3) + evalf(val/t1^k1/t2^k2/t3^k3/(7.*7.*7.))):
od:
od:
od:

od;
od;
od;

read("../final/db221D.coeffvecs.mpl");

for k1 from 0 to 6 do:
for k2 from 0 to 6 do:
for k3 from 0 to 6 do:
 tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3))):
 tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3))):
 tmp3 := Re(coeff(ds(k1,k2,k3),Dsm2)):
 tmp4 := Im(coeff(ds(k1,k2,k3),Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k1, k2, k3, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k1,k2,k3,expand(ds(k1,k2,k3)));
od:
od:
od:

for k from 1 to 83 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k,expand(Dvec[k]));
od:

read("../db221D.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(82):
Cvec := iMM.Dvec:

for k from 1 to 82 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
# lprint(Clbl[k],expand(Cvec[k]));
od:


