(* ::Package:: *)

NumberOfLinks = 28;
STRATEGY = STRATEGY_X;
Get["/lscr221/badger/src/FIESTA2.5/FIESTA_2.5.0.m"];
NumberOfSubkernels = 4;
CIntegratePath = "/lscr221/badger/src/FIESTA2.5/CIntegrateMP";
UsingQLink = False;
(*QLinkPath="/lscr221/badger/src/QLink/QLink64";*)

CurrentIntegratorSettings=
   {{"epsrel","1.000000E-10"},{"epsabs","1.000000E-16"},
   {"mineval","0"},{"maxeval","1000000"},{"nstart","1000"},
   {"nincrease","1000"}};

kList = {k1, k2};
proList = {-k1^2, -(k1 - p1)^2, -(k1 - p1 - p2)^2, -(k1 - p1 - p2 -
       p3)^2, -(k2 + p1 + p2 + p3)^2, -(k2 - p5)^2, -k2^2, -(k1 +
       k2)^2};
kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1*p2 -> s12/2,
   p1*p3 -> (-s12 - s23 + s45)/2,
   p1*p5 -> s15/2,
   p2*p3 -> s23/2,
   p2*p5 -> (-s12 - s15 + s34)/2,
   p3*p5 -> (s12 - s34 - s45)/2
   };
intDB[index1_, condition1_, d1_] :=
  Module[{index = index1, condition = condition1, d = d1, result,
    nkinematics},
   d0 = d1;
   nkinematics =
    Table[{s12, s23, s34, s45, s15}[[i]] -> condition[[i]], {i, 1, 5}];
   result =
    SDEvaluate[UF[kList, proList, Join[kinematics, nkinematics]],
     index, 0];
   Return[result];
   ];
intDBT1[index1_, condition1_, d1_] :=
  Module[{index = index1, condition = condition1, d = d1, result,
    nkinematics},
   d0 = d1;
   nkinematics =
    Table[{s12, s23, s34, s45, s15}[[i]] -> condition[[i]], {i, 1, 5}];
   result =
    SDEvaluate[
     UF[kList, Append[proList, -(k1 + p5)^2],
      Join[kinematics, nkinematics]], index, 0];
   Return[result];
   ];
intDBT2[index1_, condition1_, d1_] :=
  Module[{index = index1, condition = condition1, d = d1, result,
    nkinematics},
   d0 = d1;
   nkinematics =
    Table[{s12, s23, s34, s45, s15}[[i]] -> condition[[i]], {i, 1, 5}];
   result =
    SDEvaluate[
     UF[kList, Append[proList, -k1*p5],
      Join[kinematics, nkinematics]], index, 0];
   Return[result];
   ];
Truncate[x1_] := Module[{x = x1, varList, replacement},
   varList = Variables[x];
   varList = Complement[varList, {Dsm2, ep}];
   replacement = Table[varList[[i]] -> 0, {i, 1, Length[varList]}];

   Return[x /. replacement // Expand];
   ];
db320[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 1, 1, 1, 1, 1, 1, 1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 1, 1, 1, 1, 1, 1, 1}, condition, 6] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 1, 3}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 3, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 2, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 3, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 1, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 2, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 3, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 3, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 2, 2, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 3, 1, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 1, 2, 1, 1, 1, 1}, condition,
        8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 2, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 3, 1, 1, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 1, 2, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{2, 1, 2, 1, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 2, 1, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{3, 1, 1, 1, 1, 1, 1, 1},
        condition, 8] /. intdb -> intDB;
   Return[result];
   ];
db220M1[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 0, 1, 1, 1, 1, 1, 1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 0, 1, 1, 1, 1, 1, 1}, condition, 6] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 1, 1, 1, 3}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 1, 1, 3, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 1, 2, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 1, 3, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 2, 1, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 2, 2, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 1, 3, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 0, 1, 3, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 0, 2, 2, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 0, 3, 1, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 0, 1, 2, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{2, 0, 2, 1, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{3, 0, 1, 1, 1, 1, 1, 1}, condition,
        8] /. intdb -> intDB;
   Return[result];
   ];
db220M2[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 1, 0, 1, 1, 1, 1, 1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 1, 0, 1, 1, 1, 1, 1}, condition, 6] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 1, 1, 1, 3}, condition,
        8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 1, 1, 3, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 1, 2, 2, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 1, 3, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 2, 1, 2, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 2, 2, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 1, 3, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 0, 3, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 2, 0, 2, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 3, 0, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{2, 1, 0, 2, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 2, 0, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{3, 1, 0, 1, 1, 1, 1, 1},
        condition, 8] /. intdb -> intDB;
   Return[result];
   ];
db220L[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 1, 1, 0, 1, 1, 1, 1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 1, 1, 0, 1, 1, 1, 1}, condition, 6] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 1, 1, 1, 3}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 1, 1, 3, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 1, 2, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 1, 3, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 2, 1, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 2, 2, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 0, 3, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 3, 0, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 2, 0, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{1, 3, 1, 0, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 2, 0, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{2, 2, 1, 0, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{3, 1, 1, 0, 1, 1, 1, 1}, condition,
        8] /. intdb -> intDB;
   Return[result];
   ];
db220L1[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{0, 1, 1, 1, 1, 1, 1, 1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{0, 1, 1, 1, 1, 1, 1, 1}, condition, 6] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 1, 1, 1, 3}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 1, 1, 3, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 1, 2, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 1, 3, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 2, 1, 2, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 2, 2, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 1, 3, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 1, 1, 3, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 2, 2, 1, 1, 1, 1}, condition,
        8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 1, 3, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 2, 1, 2, 1, 1, 1, 1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{0, 2, 2, 1, 1, 1, 1, 1}, condition,
        8] + 2 Dsm2 (-1 + ep) ep intdb[{0, 3, 1, 1, 1, 1, 1, 1},
        condition, 8] /. intdb -> intDB;
   Return[result];
   ];
db320T1[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 1, 1, 1, 1, 1, 1, 1, -1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 1, 1, 1, 1, 1, 1, 1, -1}, condition,
        6] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 1, 3, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 3, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 2, 2, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 3, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 1, 2, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 2, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 3, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 3, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 2, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 3, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 1, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 2, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 3, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 1, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 2, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 2, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{3, 1, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] /. intdb -> intDBT1;
   Return[result];
   ];
db320T2[condition1_] := Module[{condition = condition1, result},
   result =
    3 Dsm2 ep^2 intdb[{1, 1, 1, 1, 1, 1, 1, 1, -1}, condition, 6] -
      8 ep (1 + 2 ep) intdb[{1, 1, 1, 1, 1, 1, 1, 1, -1}, condition,
        6] + 2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 1, 3, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 1, 3, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 2, 2, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 1, 3, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 1, 2, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 2, 2, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 1, 3, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 1, 3, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 2, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 1, 3, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 1, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 2, 2, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{1, 3, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 1, 2, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 1, 2, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{2, 2, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] +
      2 Dsm2 (-1 + ep) ep intdb[{3, 1, 1, 1, 1, 1, 1, 1, -1},
        condition, 8] /. intdb -> intDBT2;
   Return[result];
   ];
mod[x_, n_] := If[x <= n, x, x - n];
sList = {s12, s23, s34, s45, s15};
numbers = {-1, -3, -11, -19, -31};
(*numbers = {-31.94458068982129`, -82.65664441470688`, -99.73674948706487`, -1.1705516456698026`, -65.9589871116981`};*)
cyclic = Table[
  sList[[j]] -> numbers[[mod[j + i, 5]]], {i, 0,
   4}, {j, 1, 5}];

intdb220M1List =
  -Table[db220M1[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
DumpSave["5pt_fiesta_v4.mx", {intdb220M1List, intdb220M2List, intdb220LList, intdb320List,intdb320TList}];

Print["******** db220M1 calculated ********"];

intdb220M2List =
  -Table[db220M2[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
DumpSave["5pt_fiesta_v4.mx", {intdb220M1List, intdb220M2List, intdb220LList, intdb320List,intdb320TList}];

Print["******** db220M2 calculated ********"];

intdb220LList =
  -Table[db220L[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
DumpSave["5pt_fiesta_v4.mx", {intdb220M1List, intdb220M2List, intdb220LList, intdb320List,intdb320TList}];

Print["******** db220L calculated ********"];

intdb320List =
  Table[db320[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
DumpSave["5pt_fiesta_v4.mx", {intdb220M1List, intdb220M2List, intdb220LList, intdb320List,intdb320TList}];

Print["******** db320 calculated ********"];

intdb320TList =
  -2*Table[db320T2[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
 intdb320TList +=
  -Table[db220L1[{s12, s23, s34, s45, s15} /. cyclic[[i]]], {i, 1, 5}];
DumpSave["5pt_fiesta_v4.mx", {intdb220M1List, intdb220M2List,intdb220LList, intdb320List,intdb320TList}];

Print["******** db320T calculated ********"];

Exit[];
