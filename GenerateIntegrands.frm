#-
*** Simon Badger 14/12/2012
*** This program generates cut integrands for multi-loop
*** amplitudes in adjoint Yang-Mills with massless fermions and scalars
*** from MHV tree amplitudes. NMHV amplitudes (from 2->4 onwards) will simply
*** be replaced with zero without any warning.

CF A(a),B(a),IA(a),IB(a);
auto CF TMP;
CF NUM,INV,ptl,AMP;

S I;
auto S p,x,h,l;

S Nf,Ns;

CF MHV,MHVb;
CF HJFamp,Power;
S i;
auto S f;

#procedure cancel

id I^2 = -1;
id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;

#endprocedure

#procedure MHV

*** this function is not 100% complete OR tested ***
*** multiple fermion flavours are not included and ***
*** 5-point sign conventions are still to be checked ***

id MHV(?x) = TMPAMP*TMPDEN*TMPHEL(nargs_(?x)-4,0)*TMPMHV(?x);

repeat;
id TMPMHV(ptl(p1?,h1?,f1?),?x2)*TMPAMP(?x3)*TMPDEN(?x4)*TMPHEL(xn?,xh?)
  = TMPAMP(?x3,ptl(p1,h1,f1))*TMPMHV(?x2)*TMPDEN(?x4,p1)*TMPHEL(xn,xh+h1*TMPfl(f1));

*** change particle ids to spin
argument TMPHEL;
id TMPfl(0) = 1;
id TMPfl(1) = 0;
id TMPfl(1/2) = 1/2;
endargument;
endrepeat;

id TMPHEL(xn?,xh?) = TMPHEL(xn,xh)*TMPN(xn+4);

*** deteremine whether MHV or MHV-bar - set non-MHV to zero
id TMPHEL(x?,x?) = 1;
id TMPHEL(x1?,x2?) = TMPHEL(x1+x2);
id TMPHEL(0) = MHVb;
id TMPHEL(?x) = 0;

*** reverse MHV-bar helicities
if(count(MHVb,1)=1);
argument TMPAMP;
id ptl(p1?,h1?,xf?) = ptl(p1,-h1,xf);
endargument;
endif;

*** overall factor of i
id TMPMHV = I;

*** cyclic denominator
*** MHV-bar comes with a factor of (-1)^n ***
id TMPDEN(p1?,?x,pn?)*MHVb = TMPDEN(p1,?x,pn)*TMPIA(pn,p1)*MHVb;
repeat id TMPDEN(p1?,p2?,?x)*MHVb = -TMPDEN(p2,?x)*TMPIA(p1,p2)*MHVb;

id TMPDEN(p1?,?x,pn?) = TMPDEN(p1,?x,pn)*TMPIA(pn,p1);
repeat id TMPDEN(p1?,p2?,?x) = TMPDEN(p2,?x)*TMPIA(p1,p2);
id TMPDEN(p1?) = 1;

*** +ve helicity gluons no longer involved
repeat id TMPAMP(?x1,ptl(p1?,1,0),?x2) = TMPAMP(?x1,?x2);

#do f={0,1,1/2}
id TMPAMP(?x1,ptl(p1?,h1?,'f'),?x2) = TMPAMP(ptl(p1,h1,'f'),?x1,?x2);
id TMPAMP(?x1,ptl(p1?,h1?,'f'),?x2) = TMPAMP(ptl(p1,h1,'f'),?x1,?x2);
#enddo

*** numerators
*** MHV-bar has additional (-1)^n for fermion pairs ***
id TMPAMP(ptl(p1?,-1,1/2),ptl(p2?,-1,1/2),ptl(p3?,0,1)) = -xsign*TMPA(p1,p3)*TMPA(p2,p3)*TMPA(p1,p2)^2;
id TMPAMP(ptl(p1?,1,1/2),ptl(p2?,1,1/2),ptl(p3?,0,1),ptl(p4?,-1,0)) = -TMPA(p1,p4)*TMPA(p2,p4)*TMPA(p3,p4)^2;
id TMPAMP(ptl(p1?,1,1/2),ptl(p2?,-1,1/2),ptl(p3?,0,1),?x) = 0;
id TMPAMP(ptl(p1?,-1,1/2),ptl(p2?,1,1/2),ptl(p3?,0,1),?x) = 0;
id TMPAMP(ptl(p1?,h1?,1/2),ptl(p2?,h1?,1/2),?x) = 0;
id TMPAMP(ptl(p1?,1,1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,0)) = xsign*TMPA(p1,p3)*TMPA(p2,p3)^3;
id TMPAMP(ptl(p1?,-1,1/2),ptl(p2?,1,1/2),ptl(p3?,-1,0)) = xsign*TMPA(p1,p3)^3*TMPA(p2,p3);
id TMPAMP(ptl(p1?,0,1),ptl(p2?,0,1),ptl(p3?,-1,0)) = TMPA(p1,p3)^2*TMPA(p2,p3)^2;
id TMPAMP(ptl(p1?,-1,0),ptl(p2?,-1,0)) = TMPA(p1,p2)^4;

id xsign*MHVb*TMPN(x?odd_) = -MHVb;
id xsign = 1;
id TMPN(x?) = 1;

if(count(MHVb,1)=1);
id TMPA(p1?,p2?) = B(p1,p2);
id TMPIA(p1?,p2?) = IB(p1,p2);
else;
id TMPA(p1?,p2?) = A(p1,p2);
id TMPIA(p1?,p2?) = IA(p1,p2);
endif;
id MHVb=1;
#endprocedure

#procedure dblebox(fl1,fl2,fl3)
id TMPdblebox('fl1','fl2','fl3') =
#do hl1=`H`fl1''
#do hl2=`H`fl1''
#do hl3=`H`fl1''
#do hl4=`H`fl2''
#do hl5=`H`fl2''
#do hl6=`H`fl2''
#do hl7=`H`fl3''

+ AMP(ptl(-l1,-'hl1','fl1'),ptl(p1,'h1',0),ptl(l2,'hl2','fl1'))*
  AMP(ptl(-l2,-'hl2','fl1'),ptl(p2,'h2',0),ptl(l3,'hl3','fl1'))*
  AMP(ptl(-l3,-'hl3','fl1'),ptl(p3,'h3',0),ptl(l4,'hl4','fl2'),ptl(-l7,-'hl7','fl3'))*
  AMP(ptl(-l4,-'hl4','fl2'),ptl(p4,'h4',0),ptl(l5,'hl5','fl2'))*
  AMP(ptl(-l5,-'hl5','fl2'),ptl(p5,'h5',0),ptl(l6,'hl6','fl2'))*
  AMP(ptl(-l6,-'hl6','fl2'),ptl(l1,'hl1','fl1'),ptl(l7,'hl7','fl3'))

#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
#enddo
;
#endprocedure

off stats;

.global

*** 0 = gluon ***
*** 1 = scalar ***
*** 2 = fermion ***

#define f1 "2"
#define f2 "2"
#define f3 "1"
#define f4 "0"

#define H0 "-1,1,2"
#define H1 "0,0"
#define H2 "-1,1,2"

#do h1=`H`f1''
#do h2=`H`f2''
#do h3=`H`f3''
L [zero(1('h1','f1'),2('h2','f2'),3('h3','f3'))] =
   MHV(ptl(p1,'h1','f1'), ptl(p2,'h2','f2'), ptl(p3,'h3','f3'))
   - HJFamp(ptl(p1,'h1','f1'), ptl(p2,'h2','f2'), ptl(p3,'h3','f3'))
;
#do h4=`H`f4''
L [zero(1('h1','f1'),2('h2','f2'),3('h3','f3'),4('h4','f4'))] =
   MHV(ptl(p1,'h1','f1'), ptl(p2,'h2','f2'), ptl(p3,'h3','f3'), ptl(p4,'h4','f4'))
   - HJFamp(ptl(p1,'h1','f1'), ptl(p2,'h2','f2'), ptl(p3,'h3','f3'), ptl(p4,'h4','f4'))

;

argument;
id ptl(?x,2) = ptl(?x,1/2);
endargument;

#call MHV

id HJFamp(?x1,ptl(p1?,-1,1),?x2) = 0;
repeat id HJFamp(?x1,ptl(p1?,1,1),?x2) = HJFamp(?x1,ptl(p1,0,1),?x2);

#include- HJFtrees.prc
id i=I;
denominators INV;
argument INV;
id A(p1?,p2?) = IA(p1,p2);
id B(p1?,p2?) = IB(p1,p2);
endargument;
id INV(x?) = x;
id Power(x1?,x2?) = x1^x2;
#call cancel

format 150;
AB IA,IB;
print+s;
.store
#enddo
#enddo
#enddo
#enddo

.end

#define h1 "-1"
#define h2 "-1"
#define h3 "+1"
#define h4 "+1"
#define h5 "+1"

*** factor of -1 for each fermion loop ***
L [DoubleBox(12[3]45)] =
+TMPdblebox(0,0,0)
*-Nf*TMPdblebox(2,0,2)
*-Nf*TMPdblebox(0,2,2)
*-Nf*TMPdblebox(2,2,0)
*+Ns*TMPdblebox(1,0,1)
*+Ns*TMPdblebox(0,1,1)
*+Ns*TMPdblebox(1,1,0)
*-Nf*Ns*TMPdblebox(2,1,2)
*-Nf*Ns*TMPdblebox(1,2,2)
*-Nf*Ns*TMPdblebox(2,2,1)
;

#call dblebox(0,0,0);
#call dblebox(2,0,2);
#call dblebox(0,2,2);
#call dblebox(2,2,0);
#call dblebox(1,0,1);
#call dblebox(0,1,1);
#call dblebox(1,1,0);
#call dblebox(2,1,2);
#call dblebox(1,2,2);
#call dblebox(2,2,1);

repeat;
id,once AMP(?x) = NUM(MHV(?x));
argument NUM;
argument MHV;
id ptl(?x,2) = ptl(?x,1/2);
endargument;
#call MHV
endargument;
id NUM(x?) = x;
*** this is to ensure the correct spin sums are reconstructed ***
*** across each cut, numerically this can be a bit tricky using ***
*** the standard normalization for real momenta ***
id A(-l1?,p1?) = I*A(l1,p1);
id B(-l1?,p1?) = I*B(l1,p1);
id IA(-l1?,p1?) = I*IA(l1,p1);
id IB(-l1?,p1?) = I*IB(l1,p1);
#call cancel
endrepeat;
.sort

#write <testINT.mpl> "testINT:=(l1,l2,l3,l4,l5,l6,l7,p1,p2,p3,p4,p5)->"
#write <testINT.mpl> "%E:" [DoubleBox(12[3]45)]

format 160;
B IA,IB;
print+s;
.end
