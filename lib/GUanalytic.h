* */ vim: set foldmethod=marker: */

#define quadsum "off"

*{{{mklooppoly
#procedure mklooppoly(pa,pb,K2)
*** this function attempts to expand spinor products w.r.t to a basis of vectors with scalar coefficients ***

id A(px?,py?) = naA(px,py);
id IA(px?,py?) = InaA(px,py);
id B(px?,py?) = naB(px,py);
id IB(px?,py?) = InaB(px,py);

argument inv,num;
id A(px?,py?) = naA(px,py);
id IA(px?,py?) = InaA(px,py);
id B(px?,py?) = naB(px,py);
id IB(px?,py?) = InaB(px,py);
endargument;

*** general loop mom. paramtersation is:
*** l(i) = a1(i)*pa + a2(i)*pb + a3(i)*<pa,pb] + a4(i)*<pb,pa] + a5(i)*K2
*** and if a5=0...
*** |l(i)>  = |pa>+a2(i)/a3(i)*|pb>
*** |l(i)]  = a1(i)|pa]+a3(i)*|pb]

#do i=1,4
#do fun={AB,AA,IAB,IAA,naA,InaA}
id 'fun'(l'i',?x) = 'fun'(cf(1)*spmom('pa')+cf(a'i'2/a'i'3)*spmom('pb'),?x);
#enddo

#do fun={AA,IAA,naA,InaA}
id 'fun'(?x,l'i') = 'fun'(?x,cf(1)*spmom('pa')+cf(a'i'2/a'i'3)*spmom('pb'));
#enddo

#do fun={AB,BB,IAB,IBB,naB,InaB}
id 'fun'(?x,l'i') = 'fun'(?x,cf(a'i'1)*spmom('pa')+cf(a'i'3)*spmom('pb'));
#enddo

#do fun={BB,IBB,naB,InaB}
id 'fun'(l'i',?x) = 'fun'(cf(a'i'1)*spmom('pa')+cf(a'i'3)*spmom('pb'),?x);
#enddo

#enddo

*** general form for loop momenta includes a 5th coeff for an additional momenta K2   ***
*** this is necessary for l3 in box soln. and the triangle contribs. to the bub soln. ***

#do i =1,4
id S(?x1,l'i',?x2) = S(?x1,l'i',?x2,M(l'i'),S(?x1,?x2));
id S(?x1,-l'i',?x2) = S(?x1,-l'i',?x2,M(l'i'),S(?x1,?x2));
id IS(?x1,l'i',?x2) = IS(?x1,l'i',?x2,M(l'i'),S(?x1,?x2));
id IS(?x1,-l'i',?x2) = IS(?x1,-l'i',?x2,M(l'i'),S(?x1,?x2));
#enddo

argument AB,IAB,AA,IAA,BB,IBB,S,IS,PP,IPP;
#do i=1,4
#if 'K2'=0
id l'i' = cf(a'i'1)*spmom('pa','pa')+cf(a'i'2)*spmom('pb','pb')+cf(a'i'3)*spmom('pa','pb')+cf(a'i'4)*spmom('pb','pa');
#else
id l'i' = cf(a'i'1)*spmom('pa','pa')+cf(a'i'2)*spmom('pb','pb')+cf(a'i'3)*spmom('pa','pb')+cf(a'i'4)*spmom('pb','pa')+cf(a'i'5)*spmom('K2','K2');
#endif
#enddo
endargument;

*** after substituting loop mom. param. need to put summed mom. into ***
*** loop() function: l1+p1->spmom(...)+...+p1->loop(...+cf(1)*spmom(p1,p1))    ***

#do fun={naA,naB,InaA,InaB,AB,IAB,AA,IAA,BB,IBB}
id 'fun'(cf(1)*spmom(px?),?x) = 'fun'(px,?x);
id 'fun'(?x,cf(1)*spmom(px?)) = 'fun'(?x,px);
id 'fun'(px1?,px2?) = 'fun'(loop(px1),loop(px2));
id 'fun'(px1?,px2?,px3?) = 'fun'(loop(px1),loop(px2),loop(px3));
id 'fun'(px1?,px2?,px3?,px4?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4));
id 'fun'(px1?,px2?,px3?,px4?,px5?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4),loop(px5));
id 'fun'(px1?,px2?,px3?,px4?,px5?,px6?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4),loop(px5),loop(px6));
id 'fun'(px1?,px2?,px3?,px4?,px5?,px6?,p7?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4),loop(px5),loop(px6),loop(px7));
#enddo

#do fun={S,IS,PP,IPP}
id 'fun'(px1?,xm?,xs?) = 'fun'(loop(px1),xm,xs);
id 'fun'(px1?,px2?,xm?,xs?) = 'fun'(loop(px1),loop(px2),xm,xs);
id 'fun'(px1?,px2?,px3?,xm?,xs?) = 'fun'(loop(px1),loop(px2),loop(px3),xm,xs);
id 'fun'(px1?,px2?,px3?,px4?,xm?,xs?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4),xm,xs);
id 'fun'(px1?,px2?,px3?,px4?,px5?,xm?,xs?) = 'fun'(loop(px1),loop(px2),loop(px3),loop(px4),loop(px5),xm,xs);
#enddo

argument AB,IAB,AA,BB,IAA,IBB,naA,naB,InaA,InaB,IS,IPP,PP,S,dot;
splitarg loop;
argument loop;
id px?=cf(1)*spmom(px,px);
endargument;
id loop(?x,-cf(px?)*spmom(py?,pz?),?z) = loop(?x,cf(-px)*spmom(py,pz),?z);
id loop(?x,-cf(px?)*spmom('pa','pa'),?z) = loop(?x,cf(-px)*spmom('pa','pa'),?z);
id loop(?x,-cf(px?)*spmom('pb','pb'),?z) = loop(?x,cf(-px)*spmom('pb','pb'),?z);
id loop(?x,-cf(px?)*spmom('pa','pb'),?z) = loop(?x,cf(-px)*spmom('pa','pb'),?z);
id loop(?x,-cf(px?)*spmom('pb','pa'),?z) = loop(?x,cf(-px)*spmom('pb','pa'),?z);
id loop(cf(1)*spmom(px?,px?))=px;
id loop(cf(-1)*spmom(px?,px?))=-px;
endargument;

*** sort out the lorentz invariants ***

#do fun={S,IS,IPP}
id 'fun'(?x1,loop(?x2),?x3) = 'fun'(loop(?x2),?x1,?x3);
id 'fun'(?x1,-loop(?x2),?x3) = 'fun'(-loop(?x2),?x1,?x3);
#enddo

repeat;
id IS(loop(?x1),px1?,px2?,?x2,xm?,xs?) = IS(loop(?x1),px1+px2,?x2,xm,xs);
id IS(-loop(?x1),px1?,px2?,?x2,xm?,xs?) = IS(-loop(?x1),px1+px2,?x2,xm,xs);
id IPP(loop(?x1),px1?,px2?,?x2,xm?,xs?) = IPP(loop(?x1),px1+px2,?x2,xm,xs);
id IPP(-loop(?x1),px1?,px2?,?x2,xm?,xs?) = IPP(-loop(?x1),px1+px2,?x2,xm,xs);
id S(loop(?x1),px1?,px2?,?x2,xm?,xs?) = S(loop(?x1),px1+px2,?x2,xm,xs);
id S(-loop(?x1),px1?,px2?,?x2,xm?,xs?) = S(-loop(?x1),px1+px2,?x2,xm,xs);
endrepeat;

id S(loop(?x1),px?,xm?,sx?) = num(sx+2*dot(loop(?x1),px)+xm^2);
id S(-loop(?x1),px?,xm?,sx?) = num(sx-2*dot(loop(?x1),px)+xm^2);
id IS(loop(?x1),px?,xm?,sx?) = inv(sx+2*dot(loop(?x1),px)+xm^2);
id IS(-loop(?x1),px?,xm?,sx?) = inv(sx-2*dot(loop(?x1),px)+xm^2);
id IPP(loop(?x1),px?,xm?,sx?) = inv(sx+2*dot(loop(?x1),px));
id IPP(-loop(?x1),px?,xm?,sx?) = inv(sx-2*dot(loop(?x1),px));

#do fun={AB,AA,BB,naA,naB}
repeat;
id,once 'fun'(?y1,loop(cf(px?)*spmom(?p),?x),?y2) = num(px*'fun'(?y1,spmom(?p),?y2)+'fun'(?y1,loop(?x),?y2));
id,once I'fun'(?y1,loop(cf(px?)*spmom(?p),?x),?y2) = inv(px*'fun'(?y1,spmom(?p),?y2)+'fun'(?y1,loop(?x),?y2));
endrepeat;
id 'fun'(?x,loop,?y)=0;
#enddo

argument inv,num;
#do fun={AB,AA,BB,naA,naB,dot}
repeat;
id,once 'fun'(?y1,loop(cf(px?)*spmom(?p),?x),?y2) = px*'fun'(?y1,spmom(?p),?y2)+'fun'(?y1,loop(?x),?y2);
id,once 'fun'(?y1,loop(-cf(px?)*spmom(?p),?x),?y2) = -px*'fun'(?y1,spmom(?p),?y2)+'fun'(?y1,loop(?x),?y2);
endrepeat;
id 'fun'(?x,loop,?y)=0;
#enddo
endargument;

*** re-construct spinor strings ***

argument inv,num;
repeat;
#do i={1,3,5,7}
id AB(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = AA(px1,...,px'i',py1)*BB(py2,?x);
id AA(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = AA(px1,...,px'i',py1)*BA(py2,?x);
id BB(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = BB(px1,...,px'i',py2)*AB(py1,?x);
#enddo
#do i={2,4,6,8}
id AB(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = AB(px1,...,px'i',py2)*AB(py1,?x);
id AA(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = AB(px1,...,px'i',py2)*AA(py1,?x);
id BB(px1?,...,px'i'?,spmom(py1?ps,py2?ps),?x)  = BA(px1,...,px'i',py1)*BB(py2,?x);
#enddo

#do fun={AB,AA,BB,BA}
id 'fun'(?x1,spmom(py1?,py1?),?x2)  = 'fun'(?x1,py1,?x2);
#enddo

#call cancel
endrepeat;
id dot(spmom(py1?,py2?),px?) = AB(py1,px,py2)/2;
endargument;

argument AB,IAB,AA,BB,IAA,IBB,naA,naB,InaA,InaB,IS,IPP,PP,S,dot,A,B;
id spmom(p1?)=p1;
endargument;

argument inv,num;
argument AB,IAB,AA,BB,IAA,IBB,naA,naB,InaA,InaB,IS,IPP,PP,S,dot,A,B;
id spmom(p1?)=p1;
endargument;
#do i=1,4
id M(l'i')^2 = (`m`i'')^2;
#enddo
endargument;

id naA(px?,py?) = A(px,py);
id InaA(px?,py?) = IA(px,py);
id naB(px?,py?) = B(px,py);
id InaB(px?,py?) = IB(px,py);

argument inv,num;
id naA(px?,py?) = A(px,py);
id InaA(px?,py?) = IA(px,py);
id naB(px?,py?) = B(px,py);
id InaB(px?,py?) = IB(px,py);
endargument;

#endprocedure
*}}}

*{{{inf
#procedure inf(tt)

*** compute large momentum polynomial ***

*repeat;
#do rpt=1,10

id,once num(px?) = polytmp(px);
id,once inv(px?) = polyinvtmp(px);
#do i=-6,6
id polytmp(px?,?x) = polytmp(px,ff('i',px/'tt'^'i'),?x);
id polyinvtmp(px?,?x) = polyinvtmp(px,ff('i',px/'tt'^'i'),?x);
argument polytmp,polyinvtmp;
argument ff;
id 'tt'=0;
id 1/'tt'=0;
endargument;
endargument;
id polytmp(px?,ff('i',py?),?x) = polytmp(px-py*'tt'^'i',ff('i',py),?x);
id polyinvtmp(px?,ff('i',py?),?x) = polyinvtmp(px-py*'tt'^'i',ff('i',py),?x);
.sort
#enddo

id polytmp(0,?x) = poly(?x);
id polyinvtmp(0,?x) = polyinv(?x);

#do i=-6,6
id poly(?x1,ff('i',0),?x2) = poly(?x1,?x2);
id polyinv(?x1,ff('i',0),?x2) = polyinv(?x1,?x2);
#enddo

*#write "here"
*print "%t";
.sort

#enddo
*endrepeat;

id poly(ff(xx?,px?)) = 'tt'^(xx)*num(px);
id polyinv(ff(xx?,px?)) = 1/'tt'^(xx)*inv(px);

repeat;
#do i={4,3,2,1}
id polyinv(ff('i',px?),?x) = 1/'tt'*polyinvtmp(ff('i',px),?x);
argument polyinvtmp;
id ff(xx?,px?) = ff(xx-1,px);
endargument;
id polyinvtmp(?x) = polyinv(?x);
#enddo
endrepeat;

repeat;
#do i={4,3,2,1}
id,once 1/'tt'*poly(ff('i',px?),?x) = polytmp(ff('i',px),?x);
#enddo
argument polytmp;
id ff(xx?,px?) = ff(xx-1,px);
endargument;
id polytmp(?x) = poly(?x);
endrepeat;


.sort
*** expand denominators ***

repeat;
#do i=1,4
id poly(ff('i',px?),?x) = 'tt'^'i'*poly(ff(0,px))+poly(?x);
#enddo

id 'tt'*poly(?x,ff(-1,px?),?y) = polytmp(?x,ff(-1,px),?y);
argument polytmp;
id ff(xx?,px?) = ff(xx+1,px);
endargument;
id polytmp(?x) = poly(?x);
endrepeat;
.sort
#call sortnuminv
.sort

id poly(ff(0,px?),?x) = poly(ff(0,px));

id 1/'tt'=0;

id polyinv(ff(0,px1?),ff(-1,px2?),ff(-2,px3?),?x) = polyinv(ff(0,px1),ff(-1,px2),ff(-2,px3));
id polyinv(ff(0,px1?)) = inv(px1);
.sort

repeat;

#if 'tt'=yy
id,once polyinv(ff(0,px1?),ff(-1,px2?)) = 
inv(px1)*( 1 - num(px2)*inv(px1)/'tt' + num(px2^2)*inv(px1)^2/'tt'^2 - num(px2^3)*inv(px1)^3/'tt'^3 
	   + num(px2)^4*inv(px1)^4/'tt'^4 - num(px2)^5*inv(px1)^5/'tt'^5
);
#else
id,once polyinv(ff(0,px1?),ff(-1,px2?)) = 
inv(px1)*( 1 - num(px2)*inv(px1)/'tt' + num(px2^2)*inv(px1)^2/'tt'^2 - num(px2^3)*inv(px1)^3/'tt'^3 );
#endif
id 1/'tt'=0;
endrepeat;

.sort
repeat;
#if 'tt'=yy
id,once polyinv(ff(0,px1?),ff(-1,px2?),ff(-2,px3?)) = 
inv(px1)*( 1 - num(px2)*inv(px1)/'tt' + num(px2^2-px3*px1)*inv(px1)^2/'tt'^2 + num((2*px1*px3-px2^2)*px2)*inv(px1)^3/'tt'^3 
           +num(px2^4-3*px1*px3*px2^2+px1^2*px3^2)*inv(px1)^4/'tt'^4
           -num(px2^4-4*px1*px3*px2^2+3*px1^2*px3^2)*num(px2)*inv(px1)^5/'tt'^5
);
#else
id,once polyinv(ff(0,px1?),ff(-1,px2?),ff(-2,px3?)) = 
inv(px1)*( 1 - num(px2)*inv(px1)/'tt' + num(px2^2-px3*px1)*inv(px1)^2/'tt'^2 + num((2*px1*px3-px2^2)*px2)*inv(px1)^3/'tt'^3 );
#endif
id 1/'tt'=0;
endrepeat;
.sort
id poly(ff(0,px?)) = num(px);


#call cancel
*#call sortnuminv

.sort

#endprocedure
*}}}

*{{{boxA
#procedure boxA(K1,K2,K3,s1,s2,s3,K1f,K2f,m1,m2,m3,m4)

*** massless box solution: l^2=0 => d=0 ***

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(l1,l2,?x) = -'fun'(l1,'K2',?x);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K2',?x);

id 'fun'(?x,l2,l1) = -'fun'(?x,'K2',l1);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K2',l2);

id 'fun'(l1,l4,?x) = 'fun'(l1,'K1',?x);
id 'fun'(l4,l1,?x) = -'fun'(l4,'K1',?x);

id 'fun'(?x,l4,l1) = 'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l4) = -'fun'(?x,'K1',l4);

id 'fun'(l3,l4,?x) = 'fun'(l3,'K1'+'K2'+'K3',?x);
id 'fun'(l4,l3,?x) = -'fun'(l4,'K1'+'K2'+'K3',?x);

id 'fun'(?x,l4,l3) = 'fun'(?x,'K1'+'K2'+'K3',l3);
id 'fun'(?x,l3,l4) = -'fun'(?x,'K1'+'K2'+'K3',l4);

id 'fun'(l3,l2,?x) = 'fun'(l3,'K3',?x);
id 'fun'(l2,l3,?x) = -'fun'(l2,'K3',?x);

id 'fun'(?x,l2,l3) = 'fun'(?x,'K3',l3);
id 'fun'(?x,l3,l2) = -'fun'(?x,'K3',l2);
#enddo

#do fun={S,IS}
id 'fun'(?x1,-l4,?x2,l3,?x3) = 'fun'(?x1,?x2,-'K1'-'K2'-'K3',?x3);
id 'fun'(?x1,-l3,?x2,l4,?x3) = 'fun'(?x1,?x2,'K1'+'K2'+'K3',?x3);
id 'fun'(?x1,-l4,?x2,l1,?x3) = 'fun'(?x1,?x2,'K1',?x3);
id 'fun'(?x1,-l1,?x2,l2,?x3) = 'fun'(?x1,?x2,'K2',?x3);
id 'fun'(?x1,-l2,?x2,l3,?x3) = 'fun'(?x1,?x2,'K3',?x3);
#enddo
#call dotsort

#call collectl3('K1f','K2f')
#call mklooppoly('K1f','K2f','K3');

argument num,inv;

#do i=2,4
id a'i'3=a13;
id 1/a'i'3=1/a13;
id a'i'4=0;
#enddo

id a21 = a11-'s2'*'igg';
id a22 = a12-1;
id a25 = 0;

id a31 = a11-'s2'*'igg';
id a32 = a12-1;
id a35 = -1;

id a41 = a11+1;
id a42 = a12+'s1'*'igg';
id a45 = 0;

id a13 = Ca;
id 1/a13 = 1/Ca;
id a14 = 0;
id a15 = 0;

#call sortnuminv

endargument;

#call cancel
#call simplifystrings
#call cancel

argument num,inv;
#call cancel
#call simplifystrings
#call cancel
id num(0)=0;
endargument;

#call sortnuminv

id a11=('s2'+('m1')^2-('m2')^2)*'igg';
id 1/a11=inv('s2'+('m1')^2-('m2')^2)*'gg';
id a12=(-'s1'-('m1')^2+('m4')^2)*'igg';
id 1/a12=inv((-'s1'-('m1')^2+('m4')^2)*'igg');

argument num,inv;
id a11=('s2'+('m1')^2-('m2')^2)*'igg';
id 1/a11=inv('s2'+('m1')^2-('m2')^2)*'gg';
id a12=(-'s1'-('m1')^2+('m4')^2)*'igg';
id 1/a12=inv((-'s1'-('m1')^2+('m4')^2)*'igg');

id Ca=-num(scale'ch')*IAB('K1f','K3','K2f')*'igg';
id 1/Ca=-num(Iscale'ch')*AB('K1f','K3','K2f')*'gg';

*id scale'ch'=
*num( ('s2'+('m1')^2-('m2')^2)*AB('K1f','K3','K1f')-('s1'+('m1')^2-('m4')^2)*AB('K2f','K3','K2f') -'gg'*(S('K2','K3')-'s2'+('m2')^2-('m3')^2))*'igg';
*id 1/scale'ch'=
*inv( ('s2'+('m1')^2-('m2')^2)*AB('K1f','K3','K1f')-('s1'+('m1')^2-('m4')^2)*AB('K2f','K3','K2f') -'gg'*(S('K2','K3')-'s2'+('m2')^2-('m3')^2))*'gg';

#call sortnuminv 

endargument;


id Ca=-num(scale'ch')*IAB('K1f','K3','K2f')*'igg';
id 1/Ca=-inv(scale'ch')*AB('K1f','K3','K2f')*'gg';

#endprocedure
*}}}

*{{{boxB
#procedure boxB(K1,K2,K3,s1,s2,s3,K1f,K2f,m1,m2,m3,m4)

*** massless box solution: l^2=0 => c=0 ***

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(l1,l2,?x) = -'fun'(l1,'K2',?x);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K2',?x);

id 'fun'(?x,l2,l1) = -'fun'(?x,'K2',l1);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K2',l2);

id 'fun'(l1,l4,?x) = 'fun'(l1,'K1',?x);
id 'fun'(l4,l1,?x) = -'fun'(l4,'K1',?x);

id 'fun'(?x,l4,l1) = 'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l4) = -'fun'(?x,'K1',l4);

id 'fun'(l3,l4,?x) = 'fun'(l3,'K1'+'K2'+'K3',?x);
id 'fun'(l4,l3,?x) = -'fun'(l4,'K1'+'K2'+'K3',?x);

id 'fun'(?x,l4,l3) = 'fun'(?x,'K1'+'K2'+'K3',l3);
id 'fun'(?x,l3,l4) = -'fun'(?x,'K1'+'K2'+'K3',l4);

id 'fun'(l3,l2,?x) = 'fun'(l3,'K3',?x);
id 'fun'(l2,l3,?x) = -'fun'(l2,'K3',?x);

id 'fun'(?x,l2,l3) = 'fun'(?x,'K3',l3);
id 'fun'(?x,l3,l2) = -'fun'(?x,'K3',l2);
#enddo

#do fun={S,IS}
id 'fun'(?x1,-l4,?x2,l3,?x3) = 'fun'(?x1,?x2,-'K1'-'K2'-'K3',?x3);
id 'fun'(?x1,-l3,?x2,l4,?x3) = 'fun'(?x1,?x2,'K1'+'K2'+'K3',?x3);
id 'fun'(?x1,-l4,?x2,l1,?x3) = 'fun'(?x1,?x2,'K1',?x3);
id 'fun'(?x1,-l1,?x2,l2,?x3) = 'fun'(?x1,?x2,'K2',?x3);
id 'fun'(?x1,-l2,?x2,l3,?x3) = 'fun'(?x1,?x2,'K3',?x3);
#enddo
#call dotsort

#call collectl3('K2f','K1f')
#call mklooppoly('K2f','K1f','K3');

argument num,inv;

#do i=2,4
id a'i'3=a13;
id 1/a'i'3=1/a13;
id a'i'4=0;
#enddo

id a11 = a12;
al a12 = a11;

id a22 = a11-'s2'*'igg';
id a21 = a12-1;
id a25 = 0;

id a32 = a11-'s2'*'igg';
id a31 = a12-1;
id a35 = -1;

id a42 = a11+1;
id a41 = a12+'s1'*'igg';
id a45 = 0;

id a13 = Cb;
id 1/a13 = 1/Cb;
id a14 = 0;
id a15 = 0;

#call sortnuminv

endargument;

#call cancel
#call simplifystrings
#call cancel

argument inv,num;
#call cancel
#call simplifystrings
#call cancel
endargument;

#call sortnuminv
.sort

id a11=('s2'+('m1')^2-('m2')^2)*'igg';
id 1/a11=inv(('s2'+('m1')^2-('m2')^2)*'igg');
id a12=(-'s1'-('m1')^2+('m4')^2)*'igg';
id 1/a12=inv((-'s1'-('m1')^2+('m4')^2)*'igg');

argument num,inv;
id a11=('s2'+('m1')^2-('m2')^2)*'igg';
id 1/a11=inv(('s2'+('m1')^2-('m2')^2)*'igg');
id a12=(-'s1'-('m1')^2+('m4')^2)*'igg';
id 1/a12=inv((-'s1'-('m1')^2+('m4')^2)*'igg');

id Cb=-num(scale'ch')*IAB('K2f','K3','K1f')*'igg';
id 1/Cb=-num(Iscale'ch')*AB('K2f','K3','K1f')*'gg';

*id scale'ch'=
*num( ('s2'+('m1')^2-('m2')^2)*AB('K1f','K3','K1f')-('s1'+('m1')^2-('m4')^2)*AB('K2f','K3','K2f') -'gg'*(S('K2','K3')-'s2'+('m2')^2-('m3')^2))*'igg';
*id 1/scale'ch'=
*inv( ('s2'+('m1')^2-('m2')^2)*AB('K1f','K3','K1f')-('s1'+('m1')^2-('m4')^2)*AB('K2f','K3','K2f') -'gg'*(S('K2','K3')-'s2'+('m2')^2-('m3')^2))*'gg';

#call sortnuminv
endargument;

id Cb=-num(scale'ch')*IAB('K2f','K3','K1f')*'igg';
id 1/Cb=-inv(scale'ch')*AB('K2f','K3','K1f')*'gg';

#endprocedure
*}}}

*{{{box
#procedure box(K1,K2,K3,s1,s2,s3,K1f,K2f,m1,m2,m3,m4)

*** general box solution: c[+/-] = c[0] +/- sqrt[c[1]]] ***

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#call collectl3('K2f','K1f')

*** a few useful kinematic ids ***
#do fun={AB,IAB}
id 'fun'(px?,l2,'K2') = 'fun'(px,l1,'K2');
id 'fun'('K2',l2,px?) = 'fun'('K2',l1,px);
#enddo

#do fun={AA,IAA,BB,IBB}
id 'fun'(px?,l3,l2,px?) ='fun'(px,l3,'K3',px);
id 'fun'(px?,l3,l4,px?) ='fun'(px,l3,'K1'+'K2'+'K3',px);
#enddo

#do fun={S,IS}
id 'fun'(?x1,-l4,?x2,l3,?x3) = 'fun'(?x1,?x2,-'K1'-'K2'-'K3',?x3);
id 'fun'(?x1,-l3,?x2,l4,?x3) = 'fun'(?x1,?x2,'K1'+'K2'+'K3',?x3);
id 'fun'(?x1,-l4,?x2,l1,?x3) = 'fun'(?x1,?x2,'K1',?x3);
id 'fun'(?x1,-l1,?x2,l2,?x3) = 'fun'(?x1,?x2,'K2',?x3);
id 'fun'(?x1,-l2,?x2,l3,?x3) = 'fun'(?x1,?x2,'K3',?x3);
#enddo

#call simplifystrings
#call dotsort
#call cancel
.sort

#call mklooppoly('K1f','K2f','K3');

argument num,inv;

id a21 = a11-'s2'*'igg';
id a22 = a12-1;
id a23 = a13;
id 1/a23 = 1/a13;
id a24 = a14;
id a25 = 0;

id a31 = a11-'s2'*'igg';
id a32 = a12-1;
id a33 = a13;
id 1/a33 = 1/a13;
id a34 = a14;
id a35 = -1;

id a41 = a11+1;
id a42 = a12+'s1'*'igg';
id a43 = a13;
id 1/a43 = 1/a13;
id a44 = a14;
id a45 = 0;

id a11 = [a_'ch'];
id a12 = [b_'ch'];
id a13 = c;
id 1/a13 = 1/c;
*id a14 = a*b/c-('m1')^2*'igg'/c;
id a14 = [d_'ch']/c;
id a15 = 0;

#call sortnuminv

endargument;

#call cancel
#call simplifystrings
#call cancel

argument inv,num;
#call cancel
#call simplifystrings
#call cancel
endargument;

#call sortnuminv

*{{{ sum over c+/- : use with care, usually results in long output
#if 'quadsum'=on
id inv(px?) = quadinv(px/c,px,px*c)*c;
id num(px?) = quadnum(px/c,px,px*c)/c;
argument quadinv,quadnum;
id,once c=0;id 1/c=0;
endargument;

id quadinv(?x) = quadinv(c,?x);
id quadnum(?x) = quadnum(c,?x);

*** simplify quadinv ***
id quadinv(c,0,0,px?) = inv(px);
id quadinv(c,0,px?,0) = 1/c*inv(px);
id quadinv(c,px?,0,0) = 1/c^2*inv(px);

id quadnum(c,0,0,px?) = num(px);
id quadnum(c,0,px?,0) = c*num(px);
id quadnum(c,px?,0,0) = c^2*num(px);

multiply xxf1/2+xxf2/2;

id xxf1=replace_(c,cp);
id xxf2=replace_(c,cm);

id quadinv(cp,px?,py?,pz?) = quadinv([cp*cm],[cp+cm],px,py,pz)*quadnum(cm,px,py,pz);
id quadinv(cm,px?,py?,pz?) = quadinv([cp*cm],[cp+cm],px,py,pz)*quadnum(cp,px,py,pz);

id quadnum(cp?,px?,py?,pz?) = cp^2*px+cp*py+pz;
id cp*cm = [cp*cm];
id 1/cp = cm/[cp*cm];
id 1/cm = cp/[cp*cm];
.sort
id cp^4 = [cp+cm]^4-4*[cp+cm]^2*[cp*cm]+2*[cp*cm]^2-cm^4;
id cp^3 = [cp+cm]^3-3*[cp+cm]*[cp*cm]-cm^3;
id cp^2 = [cp+cm]^2-2*[cp*cm]-cm^2;
id cp = [cp+cm]-cm;

id quadinv([cp*cm],[cp+cm],px?,py?,pz?) = quadinv([cp*cm],px*(px+pz),py*(px*[cp+cm]+py),pz*([cp+cm]*(py-2*px)+pz));

format 150;
B [cp*cm],[cp+cm],quadinv,cp,cm;
print[];
.sort

id [cp+cm] = -xc1/xc2;
id [cp*cm] = xc0/xc2;
id 1/[cp*cm] = inv(xc0/xc2);
id xc2=AB('K1f','K3','K2f');
id 1/xc2=IAB('K1f','K3','K2f');
id xc1=[a_'ch']*AB('K1f','K3','K1f')+[b_'ch']*AB('K2f','K3','K2f')-'s3'-2*dot('K2','K3')-('m2')^2+('m3')^2;
id 1/xc1=inv([a_'ch']*AB('K1f','K3','K1f')+[b_'ch']*AB('K2f','K3','K2f')-'s3'-2*dot('K2','K3')-('m2')^2+('m3')^2);
id xc0=([a_'ch']*[b_'ch']-('m1')^2*'igg')*AB('K2f','K3','K1f');
id 1/xc0=inv(([a_'ch']*[b_'ch']-('m1')^2*'igg')*AB('K2f','K3','K1f'));


argument quadinv,inv,num,sqrt;
id [cp+cm] = -xc1/xc2;
id [cp*cm] = xc0/xc2;
id xc2=AB('K1f','K3','K2f');
id 1/xc2=IAB('K1f','K3','K2f');
id xc1=[a_'ch']*AB('K1f','K3','K1f')+[b_'ch']*AB('K2f','K3','K2f')-'s3'-2*dot('K2','K3')-('m2')^2+('m3')^2;
id xc0=([a_'ch']*[b_'ch']-('m1')^2*'igg')*AB('K2f','K3','K1f');
endargument;
#endif
*}}}

#endprocedure
*}}}

*{{{triA
#procedure triA(K1,K2,s1,s2,K1f,K2f,m1,m2,m3)

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#define s1h "('s1'+('m1')^2-('m3')^2)"
#define s2h "('s2'+('m1')^2-('m2')^2)"
#define s3 "(S('K1','K2'))"
#define m4 "0"
*#define a "(num('s2h'*'gg'+'s1h'*'s2')*inv(('gg')^2-'s1'*'s2'))"
*#define b "(-num('s1h'*'gg'+'s1'*'s2h')*inv(('gg')^2-'s1'*'s2'))"

#do fun={AA,BB,IAA,IBB,AB,IAB}
id 'fun'(?x,l2-l3,?y) = -'fun'(?x,'K1'+'K2',?y);
id 'fun'(?x,-l2+l3,?y) = 'fun'(?x,'K1'+'K2',?y);
#enddo

id IS(?x1,l2,?x2,-l3,?x3) = IS(?x1,?x2,?x3,-'K1'-'K2');
id IS(?x1,-l2,?x2,l3,?x3) = IS(?x1,?x2,?x3,'K1'+'K2');
id IS(?x1,-l3,?x2,l2,?x3) = IS(?x1,?x2,?x3,-'K1'-'K2');
id IS(?x1,l3,?x2,-l2,?x3) = IS(?x1,?x2,?x3,'K1'+'K2');

id IS(?x1,-l3,?x2,l1,?x3) = IS(?x1,?x2,?x3,-'K1');
id IS(?x1,l1,?x2,-l3,?x3) = IS(?x1,?x2,?x3,'K1');
#call dotsort

#do fun={AA,BB}
id 'fun'(px?,l1,l2,py?) = 'fun'(px,'K2',l2,py)+('m2')^2*'fun'(px,py);
id 'fun'(px?,l3,l2,py?) = -'fun'(px,'K1'+'K2',l2,py)+('m2')^2*'fun'(px,py);
id 'fun'(px?,l3,l1,py?) = 'fun'(px,'K1',l1,py)+('m1')^2*'fun'(px,py);
#enddo
#call cancel

id IAB(l1,l3,l1) = -inv('s1'-('m3')^2);
id IAB(l3,l1,l3) = -inv('s1'-('m1')^2);
id IAB(l1,l2,l1) = -inv('s2'-('m2')^2);
id IAB(l2,l1,l2) = -inv('s2'-('m1')^2);
id IAB(l2,l3,l2) = -inv('s3'-('m3')^2);
id IAB(l3,l2,l3) = -inv('s3'-('m2')^2);

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(l1,l2,?x) = -'fun'(l1,'K2',?x);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K2',?x);

id 'fun'(?x,l2,l1) = -'fun'(?x,'K2',l1);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K2',l2);

id 'fun'(l1,l3,?x) = 'fun'(l1,'K1',?x);
id 'fun'(l3,l1,?x) = -'fun'(l3,'K1',?x);

id 'fun'(?x,l3,l1) = 'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l3) = -'fun'(?x,'K1',l3);

id 'fun'(l3,l2,?x) = -'fun'(l3,'K1'+'K2',?x);
id 'fun'(l2,l3,?x) = 'fun'(l2,'K1'+'K2',?x);

id 'fun'(?x,l2,l3) = -'fun'(?x,'K1'+'K2',l3);
id 'fun'(?x,l3,l2) = 'fun'(?x,'K1'+'K2',l2);
#enddo

#call cancel
B IA,IB,IAB,IS;
print+s;
.sort

#call mklooppoly('K1f','K2f',0)

argument num,inv;

#do i=1,3
id m'i' = `m`i'';
id a'i'3 = tt;
id 1/a'i'3 = 1/tt;
id a'i'4 = a'i'4/tt;
#call cancel
#enddo

id AB('K1f','K2','K2f') = 0;
id AB('K2f','K2','K1f') = 0;
id AB('K1f','K1','K2f') = 0;
id AB('K2f','K1','K1f') = 0;

endargument;

*format maple;
*print+s;
.sort

#call inf(tt);
id tt=0;

argument num,inv;
#call cancel
#call simplifystrings
#call cancel
endargument;

#call sortnuminv

id a11 = [a_'ch'];
id 1/a11 = inv([a_'ch']);
id a21 = [a_'ch']-'s2'*'igg';
id 1/a21 = inv([a_'ch']-'s2'*'igg');
id a31 = [a_'ch']+1;
id 1/a31 = inv([a_'ch']+1);

id a12 = [b_'ch'];
id 1/a12 = inv([b_'ch']);
id a22 = [b_'ch']-1;
id 1/a22 = inv([b_'ch']-1);
id a32 = [b_'ch']+'s1'*'igg';
id 1/a32 = inv([b_'ch']+'s1'*'igg');

#do i=1,3
id a'i'4=[a_'ch']*[b_'ch']-('m1')^2*'igg';
id 1/a'i'4=inv([a_'ch']*[b_'ch']-('m1')^2*'igg');
#enddo

argument inv,num;

id a11 = [a_'ch'];
id 1/a11 = inv([a_'ch']);
id a21 = [a_'ch']-'s2'*'igg';
id 1/a21 = inv([a_'ch']-'s2'*'igg');
id a31 = [a_'ch']+1;
id 1/a31 = inv([a_'ch']+1);

id a12 = [b_'ch'];
id 1/a12 = inv([b_'ch']);
id a22 = [b_'ch']-1;
id 1/a22 = inv([b_'ch']-1);
id a32 = [b_'ch']+'s1'*'igg';
id 1/a32 = inv([b_'ch']+'s1'*'igg');

#do i=1,4
id a'i'4=[a_'ch']*[b_'ch']-('m1')^2*'igg';
id 1/a'i'4=inv([a_'ch']*[b_'ch']-('m1')^2*'igg');
#enddo
endargument;

#call sortnuminv
.sort

#endprocedure
*}}}

*{{{triB
#procedure triB(K1,K2,s1,s2,K1f,K2f,m1,m2,m3)

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#define s1h "('s1'+('m1')^2-('m3')^2)"
#define s2h "('s2'+('m1')^2-('m2')^2)"
#define s3 "(S('K1','K2'))"
#define m4 "0"

#do fun={AA,BB,IAA,IBB,AB,IAB}
id 'fun'(?x,l2-l3,?y) = -'fun'(?x,'K1'+'K2',?y);
id 'fun'(?x,-l2+l3,?y) = 'fun'(?x,'K1'+'K2',?y);
#enddo

id IS(?x1,l2,?x2,-l3,?x3) = IS(?x1,?x2,?x3,-'K1'-'K2');
id IS(?x1,-l2,?x2,l3,?x3) = IS(?x1,?x2,?x3,'K1'+'K2');
id IS(?x1,-l3,?x2,l2,?x3) = IS(?x1,?x2,?x3,-'K1'-'K2');
id IS(?x1,l3,?x2,-l2,?x3) = IS(?x1,?x2,?x3,'K1'+'K2');
id IS(?x1,-l3,?x2,l1,?x3) = IS(?x1,?x2,?x3,-'K1');
id IS(?x1,l1,?x2,-l3,?x3) = IS(?x1,?x2,?x3,'K1');

#call dotsort

id AA(px?,l1,l2,py?) = AA(px,'K2',l2,py)+('m2')^2*A(px,py);
id AA(px?,l3,l2,py?) = -AA(px,K3,l2,py)+('m2')^2*A(px,py);
id AA(px?,l3,l1,py?) = AA(px,'K1',l1,py)+('m1')^2*A(px,py);

id IAB(l1,l3,l1) = -inv('s1'-('m3')^2);
id IAB(l3,l1,l3) = -inv('s1'-('m1')^2);
id IAB(l1,l2,l1) = -inv('s2'-('m2')^2);
id IAB(l2,l1,l2) = -inv('s2'-('m1')^2);
id IAB(l2,l3,l2) = -inv('s3'-('m3')^2);
id IAB(l3,l2,l3) = -inv('s3'-('m2')^2);

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(l1,l2,?x) = -'fun'(l1,'K2',?x);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K2',?x);

id 'fun'(?x,l2,l1) = -'fun'(?x,'K2',l1);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K2',l2);

id 'fun'(l1,l3,?x) = 'fun'(l1,'K1',?x);
id 'fun'(l3,l1,?x) = -'fun'(l3,'K1',?x);

id 'fun'(?x,l3,l1) = 'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l3) = -'fun'(?x,'K1',l3);

id 'fun'(l3,l2,?x) = -'fun'(l3,'K1'+'K2',?x);
id 'fun'(l2,l3,?x) = 'fun'(l2,'K1'+'K2',?x);

id 'fun'(?x,l2,l3) = -'fun'(?x,'K1'+'K2',l3);
id 'fun'(?x,l3,l2) = 'fun'(?x,'K1'+'K2',l2);
#enddo

#call mklooppoly('K2f','K1f',0)

argument num,inv;

#do i=1,3
id m'i' = `m`i'';
id a'i'3 = tt;
id 1/a'i'3 = 1/tt;
id a'i'4 = a'i'4/tt;
#call cancel
#enddo

id AB('K1f','K2','K2f') = 0;
id AB('K2f','K2','K1f') = 0;
id AB('K1f','K1','K2f') = 0;
id AB('K2f','K1','K1f') = 0;

endargument;

#call inf(tt);
*id tt=0;

argument num,inv;
#call cancel
#call simplifystrings
#call cancel
endargument;

#call sortnuminv

id a12 = [a_'ch'];
id 1/a12 = inv([a_'ch']);
id a22 = [a_'ch']-'s2'*'igg';
id 1/a22 = inv([a_'ch']-'s2'*'igg');
id a32 = [a_'ch']+1;
id 1/a32 = inv([a_'ch']+1);

id a11 = [b_'ch'];
id 1/a11 = inv([b_'ch']);
id a21 = [b_'ch']-1;
id 1/a21 = inv([b_'ch']-1);
id a31 = [b_'ch']+'s1'*'igg';
id 1/a31 = inv([b_'ch']+'s1'*'igg');

#do i=1,3
id a'i'4=[a_'ch']*[b_'ch']-('m1')^2*'igg';
id 1/a'i'4=inv([a_'ch']*[b_'ch']-('m1')^2*'igg');
#enddo

argument inv,num;
id a12 = [a_'ch'];
id 1/a12 = inv([a_'ch']);
id a22 = [a_'ch']-'s2'*'igg';
id 1/a22 = inv([a_'ch']-'s2'*'igg');
id a32 = [a_'ch']+1;
id 1/a32 = inv([a_'ch']+1);

id a11 = [b_'ch'];
id 1/a11 = inv([b_'ch']);
id a21 = [b_'ch']-1;
id 1/a21 = inv([b_'ch']-1);
id a31 = [b_'ch']+'s1'*'igg';
id 1/a31 = inv([b_'ch']+'s1'*'igg');

#do i=1,4
id a'i'4=[a_'ch']*[b_'ch']-('m1')^2*'igg';
id 1/a'i'4=inv([a_'ch']*[b_'ch']-('m1')^2*'igg');
#enddo
endargument;

#call sortnuminv
.sort

id tt=0;
id 1/tt=0;

#endprocedure
*}}}

*{{{bub
#procedure bub(K1,s1,K1f,XX,m1,m2)

#define s1h "('s1'+('m1')^2-('m2')^2)"
#define gg "AB('XX','K1','XX')"
#define igg "IAB('XX','K1','XX')"
#define m3 "0"
#define m4 "0"

*** l_i = yy^2*YY2+yy*YY1+YY0'i' ***

*** bit of momentum conservation ***
*** l2=l1-K1 ***

#call cancel

id IAB(l1,l2,l1) = -inv('s1'-('m2')^2);
id IAB(l2,l1,l2) = -inv('s1'-('m1')^2);

#do k=1,3
#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(?x,l1-l2,?y) = 'fun'(?x,'K1',?y);
id 'fun'(?x,-l1+l2,?y) = -'fun'(?x,'K1',?y);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K1',l2);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K1',?x);
id 'fun'(l1,l2,?x) = -'fun'(l1,'K1',?x);
id 'fun'(?x,l2,l1) = -'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l2,?y) = -'fun'(?x,l1,'K1',?y)+('m1')^2*'fun'(?x,?y);
id 'fun'(?x,l2,l1,?y) = 'fun'(?x,l2,'K1',?y)+('m2')^2*'fun'(?x,?y);
#enddo
#enddo

#do fun={S,IS}
id 'fun'(?x1,l1,?x2,-l2) = 'fun'(?x1,?x2,'K1');
id 'fun'(?x1,-l2,?x2,l1) = 'fun'(?x1,?x2,'K1');
id 'fun'(?x1,-l1,?x2,l2) = 'fun'(?x1,?x2,-'K1');
id 'fun'(?x1,l2,?x2,-l1) = 'fun'(?x1,?x2,-'K1');
#enddo
#call dotsort
#call orderS

.sort

#call mklooppoly('K1f','XX',0)

#call cancel
argument inv,num;
#call cancel
#preout on
id AB('K1f','K1','XX') = 0;
id AB('XX','K1','K1f') = 0;
id AB('K1f','$K1mXX','XX') = 0;
id AB('XX','$K1mXX','K1f') = 0;
#preout off
endargument;
#call simplifystrings
#call cancel
.sort

argument num,inv;
id a11=yy;
id a12=(S1h-S1*yy)/gg;
id a13=tt;
id 1/a13=1/tt;

id a14=yy*(S1h-S1*yy)/tt/gg-('m1')^2/tt/gg;

id a21=yy-1;
id a22=(S1h-S1-yy*S1)/gg;
id a23=tt;
id 1/a23=1/tt;
id a24=yy*(S1h-S1*yy)/tt/gg-('m1')^2/tt/gg;

endargument;

#call sortnuminv
argument inv,num;
#call cancel
#call simplifystrings
#call cancel
id S1h = S1+('m1')^2-('m2')^2;
endargument;

*B num,inv;
*print[];
.sort

#call inf(yy)
id num(px?) = px;
#call inf(tt)
id tt=0;

*B yy;
*print[];
.sort

id yy^2 = (S1h^2/S1^2-('m1')^2/S1)/3;
id yy = S1h/S1/2;

id gg='gg';
id 1/gg='igg';

id S1='s1';
id S1h='s1h';
id 1/S1=inv('s1');
id 1/S1h=inv('s1h');

argument num,inv;
id gg='gg';
id 1/gg='igg';

id S1='s1';
id S1h='s1h';
id 1/S1=inv('s1');
id 1/S1h=inv('s1h');
endargument;

#endprocedure
*}}}

*{{{bubtri
#procedure bubtri(K1,K2,s1,s2,K1f,XX,m1,m2,m3)

** l2=l1-K1 **
** l3=l1+K2 **

#define gg "(A('K1f','XX')*B('XX','K1f'))"
#define igg "(IA('K1f','XX')*IB('XX','K1f'))"

#define s1h "('s1'+('m1')^2-('m2')^2)"
#define s2h "('s2'+('m1')^2-('m3')^2)"
#define m4 "0"

#call cancel

#do fun={AA,BB}
id 'fun'(px?,l1,l2,py?) = 'fun'(px,'K1',l2,py)+('m2')^2*'fun'(px,py);
id 'fun'(px?,l3,l2,py?) = 'fun'(px,'K1'+'K2',l2,py)+('m2')^2*'fun'(px,py);
id 'fun'(px?,l2,l3,py?) = 'fun'(px,l2,'K1'+'K2',py)+('m2')^2*'fun'(px,py);
id 'fun'(px?,l3,l1,py?) = 'fun'(px,'K2',l1,py)+('m1')^2*'fun'(px,py);
id 'fun'(px?,l1,l3,py?) = -'fun'(px,'K2',l3,py)+('m3')^2*'fun'(px,py);
#enddo

id IAB(l1,l3,l1) = -inv('s2'-('m3')^2);
id IAB(l3,l1,l3) = -inv('s2'-('m1')^2);
id IAB(l1,l2,l1) = -inv('s1'-('m2')^2);
id IAB(l2,l1,l2) = -inv('s1'-('m1')^2);

#do fun={AB,AA,BB,IAA,IBB,IAB}
id 'fun'(l1,l2,?x) = -'fun'(l1,'K1',?x);
id 'fun'(l2,l1,?x) = 'fun'(l2,'K1',?x);

id 'fun'(?x,l2,l1) = -'fun'(?x,'K1',l1);
id 'fun'(?x,l1,l2) = 'fun'(?x,'K1',l2);

id 'fun'(l1,l3,?x) = 'fun'(l1,'K2',?x);
id 'fun'(l3,l1,?x) = -'fun'(l3,'K2',?x);

id 'fun'(?x,l3,l1) = 'fun'(?x,'K2',l1);
id 'fun'(?x,l1,l3) = -'fun'(?x,'K2',l3);

id 'fun'(l3,l2,?x) = -'fun'(l3,'K1'+'K2',?x);
id 'fun'(l2,l3,?x) = 'fun'(l2,'K1'+'K2',?x);

id 'fun'(?x,l2,l3) = -'fun'(?x,'K1'+'K2',l3);
id 'fun'(?x,l3,l2) = 'fun'(?x,'K1'+'K2',l2);

id 'fun'(?x1,l2,l3,?x2) = 'fun'(?x1,l2,'K1'+'K2',?x2)+('m2')^2*'fun'(?x1,?x2);
id 'fun'(?x1,l3,l2,?x2) = 'fun'(?x1,'K1'+'K2',l2,?x2)+('m2')^2*'fun'(?x1,?x2);

#enddo

#do fun={S,IS}
id 'fun'(?x1,l1,?x2,-l2) = 'fun'(?x1,?x2,'K1');
id 'fun'(?x1,-l2,?x2,l1) = 'fun'(?x1,?x2,'K1');
id 'fun'(?x1,-l1,?x2,l2) = 'fun'(?x1,?x2,-'K1');
id 'fun'(?x1,l2,?x2,-l1) = 'fun'(?x1,?x2,-'K1');

id 'fun'(?x1,l2,?x2,-l3) = 'fun'(?x1,?x2,'K2');
id 'fun'(?x1,-l3,?x2,l2) = 'fun'(?x1,?x2,'K2');
id 'fun'(?x1,-l2,?x2,l3) = 'fun'(?x1,?x2,-'K2');
id 'fun'(?x1,l3,?x2,-l2) = 'fun'(?x1,?x2,-'K2');

id 'fun'(?x1,l3,?x2,-l1) = 'fun'(?x1,?x2,-'K1'-'K2');
id 'fun'(?x1,-l1,?x2,l3) = 'fun'(?x1,?x2,-'K1'-'K2');
id 'fun'(?x1,-l3,?x2,l1) = 'fun'(?x1,?x2,'K1'+'K2');
id 'fun'(?x1,l1,?x2,-l3) = 'fun'(?x1,?x2,'K1'+'K2');

#enddo
#call dotsort
#call orderS

#call extraids
#call cancel
#call simplifystrings
#call collectKK
#call simplifyKK
#call expandKK
#call cancel
.sort

print+s;
.sort

#call mklooppoly('K1f','XX','K2')

#call extraids
#call cancel
#call simplifystrings
#call collectKK
#call simplifyKK
#call expandKK
#call cancel
.sort
argument num,inv;
#call extraids
#call cancel
#call simplifystrings
#call collectKK
#call simplifyKK
#call expandKK
#call cancel
endargument;

.sort
argument num,inv;

id a11=yy;
id a21=yy-1;
id a31=yy;

id a12=('s1h'-'s1'*yy)/gg;
id a22=('s1h'-'s1'-'s1'*yy)/gg;
id a32=('s1h'-'s1'*yy)/gg;

id a15=0;
id a25=0;
id a35=1;

#do i=1,3
id a'i'3 = tt;
id 1/a'i'3 = 1/tt;
id a'i'4 = yy*('s1h'-'s1'*yy)/tt/gg-('m1')^2/gg/tt;
#call cancel
#enddo

id yy=YY0*tt+YY1+YY2/tt;
endargument;
id yy=YY0*tt+YY1+YY2/tt;

multiply 1/tt;
*.sort
*print+s;
.sort

#call inf(tt);

#call sortnuminv

if (count(tt,1)=0);
multiply T1;
endif;
id tt^2=T3;
id tt=T2;

id T1=-1/2*AB('XX','K2','K1f')*'igg'*'s1'*IDelta('kk1','kk2');
id T2=3/8*AB('XX','K2','K1f')^2*'igg'^2*'s1'*('s1h'*dot('K1','K2')+'s2h'*'s1')*IDelta('kk1','kk2')^2;
id T3=-1/48*AB('XX','K2','K1f')^3*'igg'^3*'s1'*(
	15*'s1'*('s1'*'s2h'^2+'s2'*'s1h'^2)
	+30*dot('K1','K2')*'s1'*'s1h'*'s2h'
	+(11*'s1h'^2+16*('m1')^2*'s1')*Delta('kk1','kk2')
	)*IDelta('kk1','kk2')^3;

id YY0=yy0('K1f','XX','K2','s1',xsig);
id 1/YY0=inv(yy0('K1f','XX','K2','s1',xsig));
id YY1=yy1('K1f','XX','K2','s1','m1','m2','m3',xsig);
id YY2=yy2('K1f','XX','K2','s1','m1','m2','m3',xsig);
id 1/gg='igg';
id gg='gg';
argument inv,num;
id YY0=yy0('K1f','XX','K2','s1',xsig);
id 1/YY0=inv(yy0('K1f','XX','K2','s1',xsig));
id YY1=yy1('K1f','XX','K2','s1','m1','m2','m3',xsig);
id YY2=yy2('K1f','XX','K2','s1','m1','m2','m3',xsig);
id 1/gg='igg';
id gg='gg';
endargument;

#endprocedure
*}}}

*{{{box4
#procedure box4(K1,K2,K3,s1,s2,s3,K1f,K2f,m1,m2,m3,m4)

*** general box solution: c[+/-] = c[0] +/- sqrt[c[1]]] ***

#define gg "(A('K1f','K2f')*B('K2f','K1f'))"
#define igg "(IA('K1f','K2f')*IB('K2f','K1f'))"

#call collectl3('K2f','K1f')

*** a few useful kinematic ids ***
#do fun={AB,IAB}
id 'fun'(px?,l2,'K2') = 'fun'(px,l1,'K2');
id 'fun'('K2',l2,px?) = 'fun'('K2',l1,px);
#enddo

#do fun={AA,IAA,BB,IBB}
id 'fun'(px?,l3,l2,px?) ='fun'(px,l3,'K3',px);
id 'fun'(px?,l3,l4,px?) ='fun'(px,l3,'K1'+'K2'+'K3',px);
#enddo

#do fun={S,IS}
id 'fun'(?x1,-l4,?x2,l3,?x3) = 'fun'(?x1,?x2,-'K1'-'K2'-'K3',?x3);
id 'fun'(?x1,-l3,?x2,l4,?x3) = 'fun'(?x1,?x2,'K1'+'K2'+'K3',?x3);
id 'fun'(?x1,-l4,?x2,l1,?x3) = 'fun'(?x1,?x2,'K1',?x3);
id 'fun'(?x1,-l1,?x2,l2,?x3) = 'fun'(?x1,?x2,'K2',?x3);
id 'fun'(?x1,-l2,?x2,l3,?x3) = 'fun'(?x1,?x2,'K3',?x3);
#enddo

#call simplifystrings
#call dotsort
#call cancel
.sort

#call mklooppoly('K1f','K2f','K3');

argument num,inv;

id a21 = a11-'s2'*'igg';
id a22 = a12-1;
id a23 = a13;
id 1/a23 = 1/a13;
id a24 = a14;
id a25 = 0;

id a31 = a11-'s2'*'igg';
id a32 = a12-1;
id a33 = a13;
id 1/a33 = 1/a13;
id a34 = a14;
id a35 = -1;

id a41 = a11+1;
id a42 = a12+'s1'*'igg';
id a43 = a13;
id 1/a43 = 1/a13;
id a44 = a14;
id a45 = 0;

id a11 = [a_'ch'];
id a12 = [b_'ch'];
id a13 = c;
id 1/a13 = 1/c;
id a14 = [d_'ch']/c;
id a15 = 0;

#call sortnuminv

endargument;

#call cancel
#call simplifystrings
#call cancel

argument inv,num;
#call cancel
#call simplifystrings
#call cancel
endargument;

#call sortnuminv

id c=mu*c;
id 1/c=1/mu/c;
argument num,inv;
id c=mu*c;
id 1/c=1/mu/c;
endargument;

multiply 1/mu^4;
.sort
#call inf(mu)
.sort

*{{{ sum over c+/- : use with care, usually results in long output
#if 'quadsum'=on
id inv(px?) = quadinv(px/c,px,px*c)*c;
id num(px?) = quadnum(px/c,px,px*c)/c;
argument quadinv,quadnum;
id,once c=0;id 1/c=0;
endargument;

id quadinv(?x) = quadinv(c,?x);
id quadnum(?x) = quadnum(c,?x);

*** simplify quadinv ***
id quadinv(c,0,0,px?) = inv(px);
id quadinv(c,0,px?,0) = 1/c*inv(px);
id quadinv(c,px?,0,0) = 1/c^2*inv(px);

id quadnum(c,0,0,px?) = num(px);
id quadnum(c,0,px?,0) = c*num(px);
id quadnum(c,px?,0,0) = c^2*num(px);

multiply xxf1/2+xxf2/2;

id xxf1=replace_(c,cp);
id xxf2=replace_(c,cm);

*** these next 4 require cp=-cm => [cp+cm]=0 ***
id quadinv(cp,px?,0,pz?) = inv(-[cp*cm]*px+pz);
id quadinv(cm,px?,0,pz?) = inv(-[cp*cm]*px+pz);
id quadnum(cp,px?,0,pz?) = num(-[cp*cm]*px+pz);
id quadnum(cm,px?,0,pz?) = num(-[cp*cm]*px+pz);

id quadinv(cp,px?,py?,pz?) = quadinv([cp*cm],[cp+cm],px,py,pz)*quadnum(cm,px,py,pz);
id quadinv(cm,px?,py?,pz?) = quadinv([cp*cm],[cp+cm],px,py,pz)*quadnum(cp,px,py,pz);

id quadnum(cp?,px?,py?,pz?) = cp^2*px+cp*py+pz;

B cp,cm,inv,quadinv;
print+s;
.sort

id cp*cm = [cp*cm];
id 1/cp = cm/[cp*cm];
id 1/cm = cp/[cp*cm];
.sort
id cp^4 = [cp+cm]^4-4*[cp+cm]^2*[cp*cm]+2*[cp*cm]^2-cm^4;
id cp^3 = [cp+cm]^3-3*[cp+cm]*[cp*cm]-cm^3;
id cp^2 = [cp+cm]^2-2*[cp*cm]-cm^2;
id cp = [cp+cm]-cm;

id quadinv([cp*cm],[cp+cm],px?,py?,pz?) = quadinv([cp*cm],px*(px+pz),py*(px*[cp+cm]+py),pz*([cp+cm]*(py-2*px)+pz));

id [cp+cm] = -xc1/xc2;
id [cp*cm] = xc0/xc2;
id 1/[cp*cm] = inv(xc0/xc2);
id xc2=AB('K1f','K3','K2f');
id 1/xc2=IAB('K1f','K3','K2f');
id xc1=0;
id xc0=(-'igg')*AB('K2f','K3','K1f');
id 1/xc0=inv((-'gg')*AB('K2f','K3','K1f'));


argument quadinv,inv,num,sqrt;
id [cp+cm] = -xc1/xc2;
id [cp*cm] = xc0/xc2;
id xc2=AB('K1f','K3','K2f');
id 1/xc2=IAB('K1f','K3','K2f');
id xc1=0;
id xc0=(-'igg')*AB('K2f','K3','K1f');
#call simplifystrings
#call cancel
#call dotsort
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#call cancel
endargument;
#endif

*}}}

#endprocedure
*}}}

*{{{sortkf
#procedure sortkf

#if 'ss1'=0

id A(K2f,px?) = BA('kk1','kk2',px);
id AA(K2f,?x) = BA('kk1','kk2',?x);
id AA(?x,K2f) = -AB(?x,'kk2','kk1');
id AB(K2f,?x) = BB('kk1','kk2',?x);
id IA(K2f,px?) = IBA('kk1','kk2',px);
id IAA(K2f,?x) = IBA('kk1','kk2',?x);
id IAA(?x,K2f) = -IAB(?x,'kk2','kk1');
id IAB(K2f,?x) = IBB('kk1','kk2',?x);
id B(K2f,px?) = -AB('kk1','kk2',px)*IAB('kk1','kk2','kk1');
id BB(K2f,?x) = -AB('kk1','kk2',?x)*IAB('kk1','kk2','kk1');
id BB(?x,K2f) = BA(?x,'kk2','kk1')*IAB('kk1','kk2','kk1');
id AB(?x,K2f) = AA(?x,'kk2','kk1')*IAB('kk1','kk2','kk1');
id IB(K2f,px?) = -IAB('kk1','kk2',px)*AB('kk1','kk2','kk1');
id IBB(K2f,?x) = -IAB('kk1','kk2',?x)*AB('kk1','kk2','kk1');
id IBB(?x,K2f) = IBA(?x,'kk2','kk1')*AB('kk1','kk2','kk1');
id IAB(?x,K2f) = IAA(?x,'kk2','kk1')*AB('kk1','kk2','kk1');
#call cancel
#call extraids
#call cancel

argument inv,num;
id A(K2f,px?) = BA('kk1','kk2',px);
id AA(K2f,?x) = BA('kk1','kk2',?x);
id AA(?x,K2f) = -AB(?x,'kk2','kk1');
id AB(K2f,?x) = BB('kk1','kk2',?x);
id IA(K2f,px?) = IBA('kk1','kk2',px);
id IAA(K2f,?x) = IBA('kk1','kk2',?x);
id IAA(?x,K2f) = -IAB(?x,'kk2','kk1');
id IAB(K2f,?x) = IBB('kk1','kk2',?x);
id B(K2f,px?) = -AB('kk1','kk2',px)*IAB('kk1','kk2','kk1');
id BB(K2f,?x) = -AB('kk1','kk2',?x)*IAB('kk1','kk2','kk1');
id BB(?x,K2f) = BA(?x,'kk2','kk1')*IAB('kk1','kk2','kk1');
id AB(?x,K2f) = AA(?x,'kk2','kk1')*IAB('kk1','kk2','kk1');
id IB(K2f,px?) = -IAB('kk1','kk2',px)*AB('kk1','kk2','kk1');
id IBB(K2f,?x) = -IAB('kk1','kk2',?x)*AB('kk1','kk2','kk1');
id IBB(?x,K2f) = IBA(?x,'kk2','kk1')*AB('kk1','kk2','kk1');
id IAB(?x,K2f) = IAA(?x,'kk2','kk1')*AB('kk1','kk2','kk1');
#call cancel
#call extraids
#call cancel
endargument;
#endif

#if 'ss2'=0

id A(K1f,px?) = BA('kk2','kk1',px);
id AA(K1f,?x) = BA('kk2','kk1',?x);
id AA(?x,K1f) = -AB(?x,'kk1','kk2');
id AB(K1f,?x) = BB('kk2','kk1',?x);
id IA(K1f,px?) = IBA('kk2','kk1',px);
id IAA(K1f,?x) = IBA('kk2','kk1',?x);
id IAA(?x,K1f) = -IAB(?x,'kk1','kk2');
id IAB(K1f,?x) = IBB('kk2','kk1',?x);
id B(K1f,px?) = -AB('kk2','kk1',px)*IAB('kk2','kk1','kk2');
id BB(K1f,?x) = -AB('kk2','kk1',?x)*IAB('kk2','kk1','kk2');
id BB(?x,K1f) = BA(?x,'kk1','kk2')*IAB('kk2','kk1','kk2');
id AB(?x,K1f) = AA(?x,'kk1','kk2')*IAB('kk2','kk1','kk2');
id IB(K1f,px?) = -IAB('kk2','kk1',px)*AB('kk2','kk1','kk2');
id IBB(K1f,?x) = -IAB('kk2','kk1',?x)*AB('kk2','kk1','kk2');
id IBB(?x,K1f) = IBA(?x,'kk1','kk2')*AB('kk2','kk1','kk2');
id IAB(?x,K1f) = IAA(?x,'kk1','kk2')*AB('kk2','kk1','kk2');
#call cancel
#call extraids
#call cancel

argument inv,num;
id A(K1f,px?) = BA('kk2','kk1',px);
id AA(K1f,?x) = BA('kk2','kk1',?x);
id AA(?x,K1f) = -AB(?x,'kk1','kk2');
id AB(K1f,?x) = BB('kk2','kk1',?x);
id IA(K1f,px?) = IBA('kk2','kk1',px);
id IAA(K1f,?x) = IBA('kk2','kk1',?x);
id IAA(?x,K1f) = -IAB(?x,'kk1','kk2');
id IAB(K1f,?x) = IBB('kk2','kk1',?x);
id B(K1f,px?) = -AB('kk2','kk1',px)*IAB('kk2','kk1','kk2');
id BB(K1f,?x) = -AB('kk2','kk1',?x)*IAB('kk2','kk1','kk2');
id BB(?x,K1f) = BA(?x,'kk1','kk2')*IAB('kk2','kk1','kk2');
id AB(?x,K1f) = AA(?x,'kk1','kk2')*IAB('kk2','kk1','kk2');
id IB(K1f,px?) = -IAB('kk2','kk1',px)*AB('kk2','kk1','kk2');
id IBB(K1f,?x) = -IAB('kk2','kk1',?x)*AB('kk2','kk1','kk2');
id IBB(?x,K1f) = IBA(?x,'kk1','kk2')*AB('kk2','kk1','kk2');
id IAB(?x,K1f) = IAA(?x,'kk1','kk2')*AB('kk2','kk1','kk2');
#call cancel
#call extraids
#call cancel
endargument;

#endif

#endprocedure
*}}}

*{{{sortXXkf
#procedure sortXXkf

id A(K1f,px?) = BA('XX','kk1',px);
id AA(K1f,?x) = BA('XX','kk1',?x);
id AA(?x,K1f) = -AB(?x,'kk1','XX');
id AB(K1f,?x) = BB('XX','kk1',?x);
id IA(K1f,px?) = IBA('XX','kk1',px);
id IAA(K1f,?x) = IBA('XX','kk1',?x);
id IAA(?x,K1f) = -IAB(?x,'kk1','XX');
id IAB(K1f,?x) = IBB('XX','kk1',?x);
id B(K1f,px?) = -AB('XX','kk1',px)*IAB('XX','kk1','XX');
id BB(K1f,?x) = -AB('XX','kk1',?x)*IAB('XX','kk1','XX');
id BB(?x,K1f) = BA(?x,'kk1','XX')*IAB('XX','kk1','XX');
id AB(?x,K1f) = AA(?x,'kk1','XX')*IAB('XX','kk1','XX');
id IB(K1f,px?) = -IAB('XX','kk1',px)*AB('XX','kk1','XX');
id IBB(K1f,?x) = -IAB('XX','kk1',?x)*AB('XX','kk1','XX');
id IBB(?x,K1f) = IBA(?x,'kk1','XX')*AB('XX','kk1','XX');
id IAB(?x,K1f) = IAA(?x,'kk1','XX')*AB('XX','kk1','XX');
#call cancel
#call extraids
#call cancel

argument inv,num;
id A(K1f,px?) = BA('XX','kk1',px);
id AA(K1f,?x) = BA('XX','kk1',?x);
id AA(?x,K1f) = -AB(?x,'kk1','XX');
id AB(K1f,?x) = BB('XX','kk1',?x);
id IA(K1f,px?) = IBA('XX','kk1',px);
id IAA(K1f,?x) = IBA('XX','kk1',?x);
id IAA(?x,K1f) = -IAB(?x,'kk1','XX');
id IAB(K1f,?x) = IBB('XX','kk1',?x);
id B(K1f,px?) = -AB('XX','kk1',px)*IAB('XX','kk1','XX');
id BB(K1f,?x) = -AB('XX','kk1',?x)*IAB('XX','kk1','XX');
id BB(?x,K1f) = BA(?x,'kk1','XX')*IAB('XX','kk1','XX');
id AB(?x,K1f) = AA(?x,'kk1','XX')*IAB('XX','kk1','XX');
id IB(K1f,px?) = -IAB('XX','kk1',px)*AB('XX','kk1','XX');
id IBB(K1f,?x) = -IAB('XX','kk1',?x)*AB('XX','kk1','XX');
id IBB(?x,K1f) = IBA(?x,'kk1','XX')*AB('XX','kk1','XX');
id IAB(?x,K1f) = IAA(?x,'kk1','XX')*AB('XX','kk1','XX');
#call cancel
#call extraids
#call cancel
endargument;

#endprocedure
*}}}
