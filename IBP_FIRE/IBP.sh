#! /bin/bash

topo=$1
strm=s/m:=[-0-9]*/m:=$2/g
strn=s/n:=[-0-9]*/n:=$3/g

sed -i -e "$strm" \
       -e "$strn" initIBP

outfile=IBPout.$topo.$2.$3.log
nohup math -initfile initIBP < $topo.m > $outfile 2>&1 &
