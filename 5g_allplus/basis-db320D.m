ISPbasis={x14, x22, x21, mu11, mu12, mu22}

coeffs={{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 1, 0}, {0, 0, 0, 1, 0,\
  0}, {0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0}, {1, 0, 0, 0, 0, 0}, {0, 0, 0, 0,\
   0, 2}, {0, 0, 0, 0, 1, 1}, {0, 0, 0, 1, 0, 1}, {0, 0, 1, 0, 0, 1}, {0, 1,\
  0, 0, 0, 1}, {1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 2, 0}, {0, 0, 0, 1, 1, 0}, {0,\
   0, 1, 0, 1, 0}, {0, 1, 0, 0, 1, 0}, {1, 0, 0, 0, 1, 0}, {0, 0, 0, 2, 0,\
  0}, {0, 0, 1, 1, 0, 0}, {0, 1, 0, 1, 0, 0}, {1, 0, 0, 1, 0, 0}, {0, 0, 0, 1,\
   0, 2}, {1, 0, 0, 0, 0, 2}, {0, 0, 0, 0, 2, 1}, {0, 0, 0, 1, 1, 1}, {0, 0,\
  1, 0, 1, 1}, {0, 1, 0, 0, 1, 1}, {1, 0, 0, 0, 1, 1}, {0, 0, 0, 2, 0, 1}, {0,\
   0, 1, 1, 0, 1}, {0, 1, 0, 1, 0, 1}, {1, 0, 0, 1, 0, 1}, {0, 0, 2, 0, 0,\
  1}, {0, 1, 1, 0, 0, 1}, {0, 0, 0, 0, 3, 0}, {0, 0, 0, 1, 2, 0}, {0, 0, 1, 0,\
   2, 0}, {0, 1, 0, 0, 2, 0}, {1, 0, 0, 0, 2, 0}, {0, 0, 0, 2, 1, 0}, {0, 0,\
  1, 1, 1, 0}, {0, 1, 0, 1, 1, 0}, {1, 0, 0, 1, 1, 0}, {0, 0, 2, 0, 1, 0}, {0,\
   1, 1, 0, 1, 0}, {0, 0, 0, 3, 0, 0}, {0, 0, 1, 2, 0, 0}, {0, 1, 0, 2, 0,\
  0}, {1, 0, 0, 2, 0, 0}, {0, 0, 2, 1, 0, 0}, {0, 1, 1, 1, 0, 0}, {1, 0, 0, 1,\
   0, 2}, {1, 0, 0, 0, 2, 1}, {0, 0, 1, 1, 1, 1}, {0, 1, 0, 1, 1, 1}, {1, 0,\
  0, 1, 1, 1}, {0, 0, 1, 2, 0, 1}, {0, 1, 0, 2, 0, 1}, {1, 0, 0, 2, 0, 1}, {0,\
   0, 1, 0, 3, 0}, {0, 1, 0, 0, 3, 0}, {1, 0, 0, 0, 3, 0}, {0, 0, 1, 1, 2,\
  0}, {0, 1, 0, 1, 2, 0}, {1, 0, 0, 1, 2, 0}, {0, 0, 2, 0, 2, 0}, {0, 1, 1, 0,\
   2, 0}, {0, 0, 1, 2, 1, 0}, {0, 1, 0, 2, 1, 0}, {1, 0, 0, 2, 1, 0}, {0, 0,\
  3, 0, 1, 0}, {0, 1, 2, 0, 1, 0}, {0, 0, 1, 3, 0, 0}, {0, 1, 0, 3, 0, 0}, {0,\
   0, 3, 1, 0, 0}, {0, 1, 2, 1, 0, 0}, {0, 0, 4, 1, 0, 0}, {0, 1, 3, 1, 0, 0}}

Integrand=0;

Do[
 monomial=1;
 Do[monomial=monomial*ISPbasis[[k]]^(coeffs[[i,k]]),{k,1,6}];
  Integrand = Integrand + (cc @@ coeffs[[i]]) * monomial;
,{i,1,79}]

fout = OpenWrite["basis/db320D.basis.h"];
WriteString[fout,"id Basisdb320D = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

