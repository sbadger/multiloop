* vim: set foldmethod=marker:
#-
#include ../2to3tools.prc
#include diagrams.prc

auto CF INTdb, INTbt, Delta;
CF F1,F2,F3,F4,ww,MU,trp,trm;
S Dsm2;
S w,w123,w543,w132,w534,w12p,w12m,w45p,w45m,w13p,w13m,w53p,w53m;

off stats;
.global

#procedure signs

*** diagrams for pentagon-box: db431
id xxdiag1  =  1;
id xxdiag2  =  2;
id xxdiag3  = -2;
id xxdiag4  =  2;
*** diagrams for box-pentagon: db341
id xxdiag5  =  1;
id xxdiag6  = -2;
id xxdiag7  =  2;
id xxdiag8  =  2;
*** diagrams for box-triangle butterfly : bt430
id xxdiag9  =  1;
id xxdiag10 = -2;
id xxdiag11 =  2;
id xxdiag12 = -4;
id xxdiag13 = -1;
id xxdiag14 =  1/2;
*** diagrams for double-box with massive 2nd leg : db331M2
id xxdiag15 =  1;
id xxdiag16 = -2;
id xxdiag17 = -2;
id xxdiag18 = -2;
id xxdiag19 = -1;
id xxdiag20 =  2;
*** diagrams for double-box with 5-legs : db3315L
id xxdiag21 = -1;
*** diagrams for triangle-box butterfly : bt340
id xxdiag22 =  1;
id xxdiag23 =  2;
id xxdiag24 = -2;
id xxdiag25 = -4;
id xxdiag26 = -1;
id xxdiag27 =  1/2;
*** diagrams for double-triangle butterfly with massive 2nd leg : bt330M2
id xxdiag28 =  1;
id xxdiag29 =  2;
id xxdiag30 =  2;
id xxdiag31 =  4;
id xxdiag32 = -1;
id xxdiag33 = -2;
id xxdiag34 = -1;
id xxdiag35 = -1/2;
id xxdiag36 =  1;
*** diagrams for double-triangle butterfly with five legs : bt3305L
id xxdiag37 =  1;
id xxdiag38 =  2;
id xxdiag39 =  2;
id xxdiag40 =  4;
id xxdiag41 = -1;
id xxdiag42 = -2;
id xxdiag43 = -1;
id xxdiag44 = -2;
*** diagrams for double-box : db421
id xxdiag45 =  1;
id xxdiag46 =  2;
id xxdiag47 =  2;
id xxdiag48 = -2;
id xxdiag49 = -1;
id xxdiag50 = -2;
*** diagrams for butterfly : bt420
id xxdiag51 =  1;
id xxdiag52 = -2;
id xxdiag53 = -2;
id xxdiag54 =  4;
id xxdiag55 = -1;
id xxdiag56 =  2;
id xxdiag57 = -1;
id xxdiag58 = -1/2;
id xxdiag59 =  1;
*** diagrams for double-box : db331M4
id xxdiag60 =  1;
id xxdiag61 = -2;
id xxdiag62 = -2;
id xxdiag63 = -2;
id xxdiag64 = -1;
id xxdiag65 =  2;
*** diagrams for double-box : db3215L
id xxdiag66 = -1;
id xxdiag67 =  1;
*** diagrams for butterfly : bt330M4
id xxdiag68 =  1;
id xxdiag69 =  2;
id xxdiag70 =  2;
id xxdiag71 =  4;
id xxdiag72 = -1;
id xxdiag73 = -2;
id xxdiag74 = -1;
id xxdiag75 = -1/2;
id xxdiag76 =  1;
*** diagrams for butterfly : bt3205L
id xxdiag77 =  1;
id xxdiag78 =  2;
id xxdiag79 = -2;
id xxdiag80 = -4;
id xxdiag81 = -1;
id xxdiag82 = -2;
id xxdiag83 = -1;
id xxdiag84 =  2;
id xxdiag85 = -1;
id xxdiag86 = -2;
id xxdiag87 =  1;
id xxdiag88 =  2;
id xxdiag89 =  1;

id xxsub = 1;
id xxsub1 = 1;
id xxsub2 = 1;
id xxsub3 = 1;
id xxsub4 = 1;
id xxsub5 = 1;
id xxsub6 = 1;
id xxsub7 = 1;
id xxsub8 = 1;
id xxsub9 = 1;
id xxsub10 = 1;
id xxsub11 = 1;

#endprocedure

#define rerun "1"
#define dd "0"

#if 'rerun'=1

*** diagrams for pentagon-box: db431
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-pentagon: db341
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-triangle butterfly : bt430
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)*IS(p123)
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;
*}}}
*** diagrams for double-box with massive 2nd leg : db331M2
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for double-box with 5-legs : db3315L
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for triangle-box butterfly : bt340
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho4, nu4, 'f2')*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;
*}}}
*** diagrams for double-triangle butterfly with massive 2nd leg : bt330M2
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p23, rho3, 0) , P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
        V(P(-p23, nu3 0), P(p2, mu2, 0), P(p3, mu3, 0))*Prop(rho3, nu3, 0)/s23*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

*}}}
*** diagrams for double-triangle butterfly with five legs : bt3305L
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
      V(P(p12, rho4, 0), P(p3, mu3, 0), P(-p123, nu8, 0))*Prop(rho4, nu4, 0)*Prop(rho8, nu8, 0)/s45/s12
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(-p123, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, rho8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

***

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(-qq5, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq5, nu6, 'f2'), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p12, rho8, 0), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

*}}}
*** diagrams for pentagon-triangle: db421
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, rho6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(kq, nu8, 'f3'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;
*}}}
*** diagrams for box-triangle butterfly: bt420
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-p123, rho8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, nu7, 'f2'), P(p123, nu8, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    Prop(rho8, nu8, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu6, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho6, 0))*Prop(rho6, nu6, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(kk123, rho4, 'f1'))*Prop(rho4, nu4, 'f1')*
    V(P(-kk123, nu4, 'f1'), P(-qq45, rho5, 'f2'), P(qq, nu7, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho7, 'f2'))*Prop(rho7, nu7, 'f2')
;

*}}}
*** diagrams for double-box: db331M4 (12*34*)
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "1"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "1"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p45, rho6, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

#define f1 "1"
#define f2 "0"
#define f3 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, nu4, 'f2'), P(kq, rho8, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq345, rho4, 'f2'), P(p3, mu3, 0), P(-qq45, nu5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, rho5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, nu7, 'f2'))*Prop(rho7, nu7, 'f2')*
    V(P(qq, rho7, 'f2'), P(kk, rho1, 'f1'), P(-kq, nu8, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho8, nu8, 'f3')
;

*}}}
*** diagrams for box-triangle: db3215L
*{{{
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk123, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p45, rho6, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
       V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, nu6, 0))*Prop(rho6, nu6, 0)/s45*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;
#define f1 "0"
#define f2 "0"
#define f3 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0), P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk123, nu3, 'f1'), P(p3, mu3, 0), P(-qq45, rho4, 'f2'), P(kq, nu7, 'f3'))*Prop(rho4, nu4, 'f2')*
    V(P(qq45, nu4, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq, nu5, 'f2'), P(kk, rho1, 'f1'), P(-kq, rho7, 'f3'))*Prop(rho1, nu1, 'f1')*
    Prop(rho7, nu7, 'f3')
;
*}}}
*** diagrams for double-triangle butterfly: bt330M4
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu8, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
    V(P(qq, nu6, 'f2'), P(p12, rho8, 0), P(-qq345, rho4, 'f2'))*Prop(rho4, nu4, 'f2')*
    Prop(rho8, nu8, 0)/s12
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p45, nu7, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-qq345, rho4, 'f2'), P(qq, nu6, 'f2'), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho4, nu4, 'f2')*
    V(P(qq345, nu4, 'f2'), P(p3, mu3, 0), P(-qq45, rho5, 'f2'))*Prop(rho5, nu5, 'f2')*
    V(P(qq45, nu5, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

*}}}
*** diagrams for triangle-bubble butterfly: bt3205L
*{{{
#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;
#define f1 "0"
#define f2 "1"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p45, nu7, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')*
      V(P(p4, mu4, 0), P(p5, mu5, 0), P(-p45, rho7, 0))*Prop(rho7, nu7, 0)/s45
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(p12, rho4, 0), P(p3, mu3, 0), P(p45, nu5, 0))*Prop(rho4, nu4, 0)*Prop(rho5, nu5, 0)/s12/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')
;
#define f1 "1"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(-p12, nu4, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(p12, rho4, 0), P(p3, mu3, 0), P(-qq45, rho6, 'f2'))*Prop(rho4, nu4, 0)/s12*Prop(rho6, nu6, 'f2')
;

#define f1 "0"
#define f2 "0"
#redefine dd "{'dd'+1}"
G diag'dd'(k1,k2,p1,p2,p3,p4,p5) =
    V(P(-kk, nu1, 'f1'), P(p1, mu1, 0), P(kk1, rho2, 'f1'))*Prop(rho2, nu2, 'f1')*
    V(P(-kk1, nu2, 'f1'), P(p2, mu2, 0) , P(kk12, rho3, 'f1'))*Prop(rho3, nu3, 'f1')*
    V(P(-kk12, nu3, 'f1'), P(p3, mu3, 0), P(p45, nu5, 0), P(kk, rho1, 'f1'))*Prop(rho1, nu1, 'f1')*Prop(rho5, nu5, 0)/s45*
    V(P(qq45, nu6, 'f2'), P(p4, mu4, 0), P(p5, mu5, 0), P(-qq, rho8, 'f2'))*Prop(rho8, nu8, 'f2')*
    V(P(qq, nu8, 'f2'), P(-p45, rho5, 0), P(-qq45, rho6, 'f2'))*Prop(rho6, nu6, 'f2')
;

*}}}
#call expandtopo
multiply EPS(1,p1,p2,mu1)*EPS(1,p2,p3,mu2)*EPS(1,p3,p4,mu3)*EPS(1,p4,p5,mu4)*EPS(1,p5,p1,mu5);
*multiply EPS(1,p1,p5,mu1)*EPS(1,p2,p1,mu2)*EPS(1,p3,p1,mu3)*EPS(1,p4,p1,mu4)*EPS(1,p5,p1,mu5);

#call subvertices
#call cancel

*if(count(Ds,1)<1);
*discard;
*endif;
.sort

argument AB,dot,S,IS;
id kk = k1;
id kk1 = MOM(k1,-p1);
id kk12 = MOM(k1,-p1,-p2);
id kk123 = MOM(k1,-p1,-p2,-p3);
id p123 = MOM(p1,p2,p3);
id p12 = MOM(p1,p2);
id p23 = MOM(p2,p3);
id p45 = MOM(p4,p5);
id qq = k2;
id qq5 = MOM(k2,-p5);
id qq45 = MOM(k2,-p4,-p5);
id qq345 = MOM(k2,-p3,-p4,-p5);
id kq = MOM(k1,k2);
endargument;

*{{{ expand and simplify ids

id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
repeat id AB(p1?,MOM(k1?{k1,k2},?xx),p3?) = AB(p1,k1,p3)+AB(p1,MOM(?xx),p3);

argument AB;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
id AB(p1?,-MOM(?x1),p2?) = -AB(p1,MOM(?x1),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
id AB(p1?,MOM,p3?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

repeat id AB(p1?,MOM(?x1,p1?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
repeat id AB(p1?,MOM(?x1,p2?,?x2),p2?) = AB(p1,MOM(?x1,?x2),p2);
id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);
#call cancel

repeat id dot(MOM(p1?),xx?) = dot(p1,xx);

id dot(MOM(k1?{k1,k2},?x1),MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2)) + dot(k2,MOM(?x1)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),MOM(?x2)) = dot(k1,MOM(?x2)) + dot(MOM(?x1),MOM(?x2));
id dot(MOM(k1?{k1,k2},?x1),p1?) = dot(k1,p1) + dot(MOM(?x1),p1);
id dot(k1?{k1,k2},MOM(k2?{k1,k2},?x2)) = dot(k1,k2) + dot(k1,MOM(?x2));

argument dot;
repeat id MOM(p1?,p2?,?x) = MOM(p1+p2,?x);
normalize MOM;
splitarg MOM;
endargument;
normalize dot;

id dot(p1?ps,p1?ps) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
id dot(p1?ps,MOM(p2?,?x)) = dot(p1,p2) + dot(p1,MOM(?x));
id dot(p1?ps,MOM(?x1,p1?ps,?x2)) = dot(p1,MOM(?x1,?x2));
id dot(p1?ps,MOM(p1?ps,?x2)) = dot(p1,MOM(?x2));
id dot(p1?ps,MOM(p1?ps,p2?)) = dot(p1,p2);
id dot(MOM,x?) = 0;
argument dot;
repeat id MOM(p1?,?x) = p1 + MOM(?x);
id MOM = 0;
endargument;
id dot(p1?ps,p1?ps) = 0;
id dot(p?,p?) = S(p);
id IS(MOM(?x)) = IS(?x);
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;

id dot(px?,p1+p2+p3) = -dot(px,p4+p5);
id dot(px?,p3+p4+p5) = -dot(px,p1+p2);
id dot(p?,p?) = S(p);
splitarg S,IS;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
.sort

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

id AB(px1?,MOM(px2?,px3?,px4?),px5?) = 0;
id AB(px1?,MOM(px2?,px3?),px4?) = -AB(px1,MOM(p1+p2+p3+p4+p5-px2-px3),px4);
argument AB;
splitarg MOM;
endargument;

id AB(p1?,MOM(p1?,p2?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p1?,p3?),px?) = AB(p1,MOM(p2,p3),px);
id AB(p1?,MOM(p2?,p3?,p1?),px?) = AB(p1,MOM(p2,p3),px);

id AB(px?,MOM(p1?,p2?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p1?,p3?),p1?) = AB(px,MOM(p2,p3),p1);
id AB(px?,MOM(p2?,p3?,p1?),p1?) = AB(px,MOM(p2,p3),p1);

id AB(p1?,MOM(p1?,p2?),px?) = AB(p1,MOM(p2),px);
id AB(p1?,MOM(p2?,p1?),px?) = AB(p1,MOM(p2),px);

id AB(px?,MOM(p1?,p2?),p1?) = AB(px,MOM(p2),p1);
id AB(px?,MOM(p2?,p1?),p1?) = AB(px,MOM(p2),p1);

id AB(p1?,MOM(p2?),p3?) = AB(p1,p2,p3);

#call cancel
id IS(p1,p2,p3) = 1/s45;
id IS(p3,p4,p5) = 1/s12;
id dot(p1+p2,p4+p5) = -(s12+s45)/2;
id S(k1?{k1,k2}) = dot(k1,k1);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
#endif
#enddo
#enddo

id dot(p1?ps,p2?) = dot(p1,MOM(p2));
argument dot;
splitarg MOM;
endargument;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
id dot(MOM(p1?),p2?) = dot(p1,p2);
#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

AB s12,s23,s34,s45,s15,s13,s14,s24,s25,s35;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
argument INV,NUM;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

#do i=1,5
#do j=1,5
#if 'j'>'i'
  id s'i''j' = S(p'i',p'j');
  id 1/s'i''j' = IS(p'i',p'j');
#endif
#enddo
#enddo
argument NUM,INV;
#do i=1,5
#do j=1,5
#if 'j'>'i'
  id s'i''j' = S(p'i',p'j');
  id 1/s'i''j' = IS(p'i',p'j');
#endif
#enddo
#enddo
endargument;

*}}}

.sort

#do i=1,'dd'
  #write <tmp/diagrams.prc> "G diag'i'(k1,k2,p1,p2,p3,p4,p5) = %e;" diag'i'
#enddo

#else
#include- tmp/diagrams.prc
#endif

*B MOM,V;
*print[];
.store

L cutAMPdb431 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

topolynomial;
.sort
format maple;
#write <db431.input.mpl> "INT_431 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db431.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db431.input.mpl> "  p1 := p[1];"
#write <db431.input.mpl> "  p2 := p[2];"
#write <db431.input.mpl> "  p3 := p[3];"
#write <db431.input.mpl> "  p4 := p[4];"
#write <db431.input.mpl> "  p5 := p[5];"
#write <db431.input.mpl> "  s12 := S(p1,p2);"
#write <db431.input.mpl> "  s13 := S(p1,p3);"
#write <db431.input.mpl> "  s14 := S(p1,p4);"
#write <db431.input.mpl> "  s15 := S(p1,p5);"
#write <db431.input.mpl> "  s23 := S(p2,p3);"
#write <db431.input.mpl> "  s24 := S(p2,p4);"
#write <db431.input.mpl> "  s25 := S(p2,p5);"
#write <db431.input.mpl> "  s34 := S(p3,p4);"
#write <db431.input.mpl> "  s35 := S(p3,p5);"
#write <db431.input.mpl> "  s45 := S(p4,p5);"
#write <db431.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db431.input.mpl> "%X"
#write <db431.input.mpl> "return %e;" cutAMPdb431
#write <db431.input.mpl> "end:"
.store

L cutAMPdb341 =
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p3) = -s45/2+s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

topolynomial;
.sort
format maple;
#write <db341.input.mpl> "INT_341 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db341.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db341.input.mpl> "  p1 := p[1];"
#write <db341.input.mpl> "  p2 := p[2];"
#write <db341.input.mpl> "  p3 := p[3];"
#write <db341.input.mpl> "  p4 := p[4];"
#write <db341.input.mpl> "  p5 := p[5];"
#write <db341.input.mpl> "  s12 := S(p1,p2);"
#write <db341.input.mpl> "  s13 := S(p1,p3);"
#write <db341.input.mpl> "  s14 := S(p1,p4);"
#write <db341.input.mpl> "  s15 := S(p1,p5);"
#write <db341.input.mpl> "  s23 := S(p2,p3);"
#write <db341.input.mpl> "  s24 := S(p2,p4);"
#write <db341.input.mpl> "  s25 := S(p2,p5);"
#write <db341.input.mpl> "  s34 := S(p3,p4);"
#write <db341.input.mpl> "  s35 := S(p3,p5);"
#write <db341.input.mpl> "  s45 := S(p4,p5);"
#write <db341.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db341.input.mpl> "%X"
#write <db341.input.mpl> "return %e;" cutAMPdb341
#write <db341.input.mpl> "end:"
.store

L cutAMPbt430 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt430.input.mpl> "INT_430 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt430.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt430.input.mpl> "  p1 := p[1];"
#write <bt430.input.mpl> "  p2 := p[2];"
#write <bt430.input.mpl> "  p3 := p[3];"
#write <bt430.input.mpl> "  p4 := p[4];"
#write <bt430.input.mpl> "  p5 := p[5];"
#write <bt430.input.mpl> "  s12 := S(p1,p2);"
#write <bt430.input.mpl> "  s13 := S(p1,p3);"
#write <bt430.input.mpl> "  s14 := S(p1,p4);"
#write <bt430.input.mpl> "  s15 := S(p1,p5);"
#write <bt430.input.mpl> "  s23 := S(p2,p3);"
#write <bt430.input.mpl> "  s24 := S(p2,p4);"
#write <bt430.input.mpl> "  s25 := S(p2,p5);"
#write <bt430.input.mpl> "  s34 := S(p3,p4);"
#write <bt430.input.mpl> "  s35 := S(p3,p5);"
#write <bt430.input.mpl> "  s45 := S(p4,p5);"
#write <bt430.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt430.input.mpl> "%X"
#write <bt430.input.mpl> "return %e;" cutAMPbt430
#write <bt430.input.mpl> "end:"
.store

L cutAMPbt340 =
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
+xxsub*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(kq)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p3) = -s45/2+s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;

id dot(k1,k2) = dot(k1,k2)-mu12/2;
id IS(kq) = INV(2*dot(k1,k2)-mu12);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt340.input.mpl> "INT_340 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt340.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt340.input.mpl> "  p1 := p[1];"
#write <bt340.input.mpl> "  p2 := p[2];"
#write <bt340.input.mpl> "  p3 := p[3];"
#write <bt340.input.mpl> "  p4 := p[4];"
#write <bt340.input.mpl> "  p5 := p[5];"
#write <bt340.input.mpl> "  s12 := S(p1,p2);"
#write <bt340.input.mpl> "  s13 := S(p1,p3);"
#write <bt340.input.mpl> "  s14 := S(p1,p4);"
#write <bt340.input.mpl> "  s15 := S(p1,p5);"
#write <bt340.input.mpl> "  s23 := S(p2,p3);"
#write <bt340.input.mpl> "  s24 := S(p2,p4);"
#write <bt340.input.mpl> "  s25 := S(p2,p5);"
#write <bt340.input.mpl> "  s34 := S(p3,p4);"
#write <bt340.input.mpl> "  s35 := S(p3,p5);"
#write <bt340.input.mpl> "  s45 := S(p4,p5);"
#write <bt340.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt340.input.mpl> "%X"
#write <bt340.input.mpl> "return %e;" cutAMPbt340
#write <bt340.input.mpl> "end:"
.store

L cutAMPdb331M2 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)
#enddo
#do i=15,20
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p1+p2+p3) = -s45/2;

id IS(kk12) = INV(-2*dot(k1,MOM(p1,p2))+s12);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db331M2.input.mpl> "INT_331M2 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db331M2.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db331M2.input.mpl> "  p1 := p[1];"
#write <db331M2.input.mpl> "  p2 := p[2];"
#write <db331M2.input.mpl> "  p3 := p[3];"
#write <db331M2.input.mpl> "  p4 := p[4];"
#write <db331M2.input.mpl> "  p5 := p[5];"
#write <db331M2.input.mpl> "  s12 := S(p1,p2);"
#write <db331M2.input.mpl> "  s13 := S(p1,p3);"
#write <db331M2.input.mpl> "  s14 := S(p1,p4);"
#write <db331M2.input.mpl> "  s15 := S(p1,p5);"
#write <db331M2.input.mpl> "  s23 := S(p2,p3);"
#write <db331M2.input.mpl> "  s24 := S(p2,p4);"
#write <db331M2.input.mpl> "  s25 := S(p2,p5);"
#write <db331M2.input.mpl> "  s34 := S(p3,p4);"
#write <db331M2.input.mpl> "  s35 := S(p3,p5);"
#write <db331M2.input.mpl> "  s45 := S(p4,p5);"
#write <db331M2.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db331M2.input.mpl> "%X"
#write <db331M2.input.mpl> "return %e;" cutAMPdb331M2
#write <db331M2.input.mpl> "end:"
.store

L cutAMPdb3315L =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
+xxsub2*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq345)
;
#call signs

*if(count(Ds,1)<1);
*discard;
*endif;
*.sort

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db3315L.input.mpl> "INT_3315L := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db3315L.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db3315L.input.mpl> "  p1 := p[1];"
#write <db3315L.input.mpl> "  p2 := p[2];"
#write <db3315L.input.mpl> "  p3 := p[3];"
#write <db3315L.input.mpl> "  p4 := p[4];"
#write <db3315L.input.mpl> "  p5 := p[5];"
#write <db3315L.input.mpl> "  s12 := S(p1,p2);"
#write <db3315L.input.mpl> "  s13 := S(p1,p3);"
#write <db3315L.input.mpl> "  s14 := S(p1,p4);"
#write <db3315L.input.mpl> "  s15 := S(p1,p5);"
#write <db3315L.input.mpl> "  s23 := S(p2,p3);"
#write <db3315L.input.mpl> "  s24 := S(p2,p4);"
#write <db3315L.input.mpl> "  s25 := S(p2,p5);"
#write <db3315L.input.mpl> "  s34 := S(p3,p4);"
#write <db3315L.input.mpl> "  s35 := S(p3,p5);"
#write <db3315L.input.mpl> "  s45 := S(p4,p5);"
#write <db3315L.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db3315L.input.mpl> "%X"
#write <db3315L.input.mpl> "return %e;" cutAMPdb3315L
#write <db3315L.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt330M2 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)
#enddo
#do i=15,20
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=28,36
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
*-xxsub1*INTdb431(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk12)*IS(kq)
*-xxsub2*INTdb331M2(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
*-xxsub3*INTbt430(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk12)
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)*IS(kq)
-xxsub2*Delta331M2(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
-xxsub3*Delta430(k1,k2,p1,p2,p3,p4,p5)*IS(kk12)
;
#call signs

id Dsm2 = Ds-2;
*if(count(Ds,1)<1);
*discard;
*endif;
.sort

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4) = s45/2;
id dot(k2,p5) = 0;
id dot(k2,p4+p5) = s45/2;
id dot(k2,p1+p2+p3) = -s45/2;


id IS(kk12) = INV(-2*dot(k1,MOM(p1,p2))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt330M2.input.mpl> "INT_330M2 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt330M2.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt330M2.input.mpl> "  p1 := p[1];"
#write <bt330M2.input.mpl> "  p2 := p[2];"
#write <bt330M2.input.mpl> "  p3 := p[3];"
#write <bt330M2.input.mpl> "  p4 := p[4];"
#write <bt330M2.input.mpl> "  p5 := p[5];"
#write <bt330M2.input.mpl> "  s12 := S(p1,p2);"
#write <bt330M2.input.mpl> "  s13 := S(p1,p3);"
#write <bt330M2.input.mpl> "  s14 := S(p1,p4);"
#write <bt330M2.input.mpl> "  s15 := S(p1,p5);"
#write <bt330M2.input.mpl> "  s23 := S(p2,p3);"
#write <bt330M2.input.mpl> "  s24 := S(p2,p4);"
#write <bt330M2.input.mpl> "  s25 := S(p2,p5);"
#write <bt330M2.input.mpl> "  s34 := S(p3,p4);"
#write <bt330M2.input.mpl> "  s35 := S(p3,p5);"
#write <bt330M2.input.mpl> "  s45 := S(p4,p5);"
#write <bt330M2.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt330M2.input.mpl> "%X"
#write <bt330M2.input.mpl> "return %e;" cutAMPbt330M2
#write <bt330M2.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt3305L =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=37,44
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
*-xxsub1*INTdb431(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
*-xxsub2*INTbt430(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kk123)
*-xxsub3*INTdb341(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
*-xxsub4*INTbt340(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(qq345)
*-xxsub5*INTdb3315L(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)*IS(kq)
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
-xxsub2*Delta430(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
+xxsub3*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq345)*IS(kq)
+xxsub4*Delta430(k2,k1,p5,p4,p3,p2,p1)*IS(qq345)
-xxsub5*Delta3315L(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p5) = 0;
id dot(k2,p4) = s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt3305L.input.mpl> "INT_3305L := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt3305L.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt3305L.input.mpl> "  p1 := p[1];"
#write <bt3305L.input.mpl> "  p2 := p[2];"
#write <bt3305L.input.mpl> "  p3 := p[3];"
#write <bt3305L.input.mpl> "  p4 := p[4];"
#write <bt3305L.input.mpl> "  p5 := p[5];"
#write <bt3305L.input.mpl> "  s12 := S(p1,p2);"
#write <bt3305L.input.mpl> "  s13 := S(p1,p3);"
#write <bt3305L.input.mpl> "  s14 := S(p1,p4);"
#write <bt3305L.input.mpl> "  s15 := S(p1,p5);"
#write <bt3305L.input.mpl> "  s23 := S(p2,p3);"
#write <bt3305L.input.mpl> "  s24 := S(p2,p4);"
#write <bt3305L.input.mpl> "  s25 := S(p2,p5);"
#write <bt3305L.input.mpl> "  s34 := S(p3,p4);"
#write <bt3305L.input.mpl> "  s35 := S(p3,p5);"
#write <bt3305L.input.mpl> "  s45 := S(p4,p5);"
#write <bt3305L.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt3305L.input.mpl> "%X"
#write <bt3305L.input.mpl> "return %e;" cutAMPbt3305L
#write <bt3305L.input.mpl> "end:"
frompolynomial;
.store

L cutAMPdb421 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <db421.input.mpl> "INT_421 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db421.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db421.input.mpl> "  p1 := p[1];"
#write <db421.input.mpl> "  p2 := p[2];"
#write <db421.input.mpl> "  p3 := p[3];"
#write <db421.input.mpl> "  p4 := p[4];"
#write <db421.input.mpl> "  p5 := p[5];"
#write <db421.input.mpl> "  s12 := S(p1,p2);"
#write <db421.input.mpl> "  s13 := S(p1,p3);"
#write <db421.input.mpl> "  s14 := S(p1,p4);"
#write <db421.input.mpl> "  s15 := S(p1,p5);"
#write <db421.input.mpl> "  s23 := S(p2,p3);"
#write <db421.input.mpl> "  s24 := S(p2,p4);"
#write <db421.input.mpl> "  s25 := S(p2,p5);"
#write <db421.input.mpl> "  s34 := S(p3,p4);"
#write <db421.input.mpl> "  s35 := S(p3,p5);"
#write <db421.input.mpl> "  s45 := S(p4,p5);"
#write <db421.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db421.input.mpl> "%X"
#write <db421.input.mpl> "return %e;" cutAMPdb421
#write <db421.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt420 =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
#enddo
#do i=51,59
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
-xxsub2*Delta421(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
-xxsub3*Delta430(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k1,p3) = s45/2-s12/2;
id dot(k1,p4+p5) = -s45/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt420.input.mpl> "INT_420 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt420.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt420.input.mpl> "  p1 := p[1];"
#write <bt420.input.mpl> "  p2 := p[2];"
#write <bt420.input.mpl> "  p3 := p[3];"
#write <bt420.input.mpl> "  p4 := p[4];"
#write <bt420.input.mpl> "  p5 := p[5];"
#write <bt420.input.mpl> "  s12 := S(p1,p2);"
#write <bt420.input.mpl> "  s13 := S(p1,p3);"
#write <bt420.input.mpl> "  s14 := S(p1,p4);"
#write <bt420.input.mpl> "  s15 := S(p1,p5);"
#write <bt420.input.mpl> "  s23 := S(p2,p3);"
#write <bt420.input.mpl> "  s24 := S(p2,p4);"
#write <bt420.input.mpl> "  s25 := S(p2,p5);"
#write <bt420.input.mpl> "  s34 := S(p3,p4);"
#write <bt420.input.mpl> "  s35 := S(p3,p5);"
#write <bt420.input.mpl> "  s45 := S(p4,p5);"
#write <bt420.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt420.input.mpl> "%X"
#write <bt420.input.mpl> "return %e;" cutAMPbt420
#write <bt420.input.mpl> "end:"
frompolynomial;
.store

L cutAMPdb3215L =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(qq345)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=66,67
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)
+xxsub2*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq5)*IS(qq345)
-xxsub3*Delta421(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub4*Delta3315L(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
-xxsub5*Delta331M2(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2)*IS(qq345)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;
id dot(k1,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);

#include- analytic_5g+++++.prc
topolynomial;
.sort
format maple;
#write <db3215L.input.mpl> "INT_3215L := proc(p,k1,k2,mu11,mu12,mu22)"
#write <db3215L.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <db3215L.input.mpl> "  p1 := p[1];"
#write <db3215L.input.mpl> "  p2 := p[2];"
#write <db3215L.input.mpl> "  p3 := p[3];"
#write <db3215L.input.mpl> "  p4 := p[4];"
#write <db3215L.input.mpl> "  p5 := p[5];"
#write <db3215L.input.mpl> "  s12 := S(p1,p2);"
#write <db3215L.input.mpl> "  s13 := S(p1,p3);"
#write <db3215L.input.mpl> "  s14 := S(p1,p4);"
#write <db3215L.input.mpl> "  s15 := S(p1,p5);"
#write <db3215L.input.mpl> "  s23 := S(p2,p3);"
#write <db3215L.input.mpl> "  s24 := S(p2,p4);"
#write <db3215L.input.mpl> "  s25 := S(p2,p5);"
#write <db3215L.input.mpl> "  s34 := S(p3,p4);"
#write <db3215L.input.mpl> "  s35 := S(p3,p5);"
#write <db3215L.input.mpl> "  s45 := S(p4,p5);"
#write <db3215L.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <db3215L.input.mpl> "%X"
#write <db3215L.input.mpl> "return %e;" cutAMPdb3215L
#write <db3215L.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt3205L =
#do i=1,4
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)*IS(kq)
#enddo
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(qq345)*IS(kq)
#enddo
#do i=9,14
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)
#enddo
#do i=21,21
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(qq345)
#enddo
#do i=37,44
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
#enddo
#do i=45,50
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
#enddo
#do i=51,59
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)*IS(kq)
#enddo
#do i=66,67
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=68,76
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq345)
#enddo
#do i=77,89
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
-xxsub1*Delta431(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kk123)*IS(kq)
+xxsub2*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq5)*IS(qq345)*IS(kq)
-xxsub7*Delta430(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)*IS(qq5)
-xxsub4*Delta3315L(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
+xxsub8*Delta430(k2,k1,p5,p4,p3,p2,p1)*IS(qq345)*IS(qq5)
-xxsub11*Delta3305L(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
-xxsub3*Delta421(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)*IS(kq)
-xxsub9*Delta123x4(k1,k2,p1,p2,p3,p4,p5)*IS(kk123)
-xxsub5*Delta331M2(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2)*IS(qq345)*IS(kq)
-xxsub6*Delta3214L(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
-xxsub10*Delta330M2(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2)*IS(qq345)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id IS(kk123) = INV(-2*dot(k1,MOM(p1,p2,p3))+s45);
id IS(qq345) = INV(-2*dot(k2,MOM(p3,p4,p5))+s12);
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;
.sort

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt3205L.input.mpl> "INT_3205L := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt3205L.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt3205L.input.mpl> "  p1 := p[1];"
#write <bt3205L.input.mpl> "  p2 := p[2];"
#write <bt3205L.input.mpl> "  p3 := p[3];"
#write <bt3205L.input.mpl> "  p4 := p[4];"
#write <bt3205L.input.mpl> "  p5 := p[5];"
#write <bt3205L.input.mpl> "  s12 := S(p1,p2);"
#write <bt3205L.input.mpl> "  s13 := S(p1,p3);"
#write <bt3205L.input.mpl> "  s14 := S(p1,p4);"
#write <bt3205L.input.mpl> "  s15 := S(p1,p5);"
#write <bt3205L.input.mpl> "  s23 := S(p2,p3);"
#write <bt3205L.input.mpl> "  s24 := S(p2,p4);"
#write <bt3205L.input.mpl> "  s25 := S(p2,p5);"
#write <bt3205L.input.mpl> "  s34 := S(p3,p4);"
#write <bt3205L.input.mpl> "  s35 := S(p3,p5);"
#write <bt3205L.input.mpl> "  s45 := S(p4,p5);"
#write <bt3205L.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt3205L.input.mpl> "%X"
#write <bt3205L.input.mpl> "return %e;" cutAMPbt3205L
#write <bt3205L.input.mpl> "end:"
frompolynomial;
.store

L cutAMPbt330M4 =
#do i=5,8
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)*IS(kq)
#enddo
#do i=60,65
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(kq)
#enddo
#do i=22,27
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)*IS(qq5)
#enddo
#do i=68,76
+xxdiag'i'*diag'i'(k1,k2,p1,p2,p3,p4,p5)
#enddo
+xxsub1*Delta431(k2,k1,p5,p4,p3,p2,p1)*IS(qq5)*IS(kq)
-xxsub2*Delta331M2(-k2+p3+p4+p5,-k1+p1+p2,p3,p4,p5,p1,p2)*IS(kq)
+xxsub3*Delta430(k2,k1,p5,p4,p3,p2,p1)*IS(qq5)
;
#call signs

id dot(k1,k1) = 0;
id dot(k2,k2) = 0;

id dot(k1,p1) = 0;
id dot(k1,p2) = s12/2;
id dot(k1,p1+p2) = s12/2;
id dot(k2,p1+p2) = -s12/2;
id dot(k2,p3+p4+p5) = s12/2;
id dot(k2,p3) = (s12-s45)/2;
id dot(k2,p4+p5) = s45/2;

id IS(qq5) = INV(-2*dot(k2,p5));
id dot(k1,k2) = dot(k1,k2)-mu12/2;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
id IS(kq) = INV(2*dot(k1,k2)-mu12);
id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;

B xxdiag1,...,xxdiag89;
print[];
.sort
#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#write <bt330M4.input.mpl> "INT_330M4 := proc(p,k1,k2,mu11,mu12,mu22)"
#write <bt330M4.input.mpl> "  local Z,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <bt330M4.input.mpl> "  p1 := p[1];"
#write <bt330M4.input.mpl> "  p2 := p[2];"
#write <bt330M4.input.mpl> "  p3 := p[3];"
#write <bt330M4.input.mpl> "  p4 := p[4];"
#write <bt330M4.input.mpl> "  p5 := p[5];"
#write <bt330M4.input.mpl> "  s12 := S(p1,p2);"
#write <bt330M4.input.mpl> "  s13 := S(p1,p3);"
#write <bt330M4.input.mpl> "  s14 := S(p1,p4);"
#write <bt330M4.input.mpl> "  s15 := S(p1,p5);"
#write <bt330M4.input.mpl> "  s23 := S(p2,p3);"
#write <bt330M4.input.mpl> "  s24 := S(p2,p4);"
#write <bt330M4.input.mpl> "  s25 := S(p2,p5);"
#write <bt330M4.input.mpl> "  s34 := S(p3,p4);"
#write <bt330M4.input.mpl> "  s35 := S(p3,p5);"
#write <bt330M4.input.mpl> "  s45 := S(p4,p5);"
#write <bt330M4.input.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <bt330M4.input.mpl> "%X"
#write <bt330M4.input.mpl> "return %e;" cutAMPbt330M4
#write <bt330M4.input.mpl> "end:"
frompolynomial;
.end
