(* vim: set ft=mma: *)

Get["/lscr221/badger/src/FIRE/SBases_3.5.0.m"];
Get["/lscr221/badger/src/FIRE/FIRE_3.5.0.m"];
Get["/lscr221/badger/src/FIRE/IBP.m"]

rename = {G[{a1_,a2_,a3_,a4_,a5_,a6_,a7_}] -> I7[a1,a2,a3,a4,a5,a6,a7]};

Internal = {k1, k2};
External = {p123, p12};
Propagators = {k1^2, k2^2, (k1 - p123)^2, (k1 - k2 - p12)^2, (k1 - p12)^2, (k2 - p12)^2, (k2 - p123)^2};
PrepareIBP[];
reps = {p123^2 -> s123, p12^2 -> s12, p12 p123 -> (s123 + s12)/2};
startinglist = {IBP[k1, k1] , IBP[k1, p12], IBP[k1, p123], IBP[k1, k2] , IBP[k2, k2] , IBP[k2, p12], IBP[k2, p123], IBP[k2,k1] } /. reps;

(*IBP[k1,p123] /. reps;*)
(*% /. {a[1]->1,a[2]->1,a[3]->1,a[4]->1,a[5]->0}*)
(*Exit[]*)

(*RESTRICTIONS = {{0, -1, -1, -1, 0, 0, 0}};*)
(*Prepare[];*)

Prepare[AutoDetectRestrictions -> True];
Burn[];

plist={
{0,1,1,2,0,0,0},
{0,1,2,1,0,0,0},
{1,0,1,2,0,0,0},
{1,1,1,1,0,0,0},
{1,1,1,2,0,-1,0},
{1,1,1,2,0,0,0},
{1,1,2,1,0,0,0},
{1,0,2,1,0,0,0},
{1,1,1,2,-1,0,0},
{1,1,2,0,0,0,0},
{1,1,2,1,-1,0,0},
{1,1,2,1,0,-1,0},
{1,1,2,1,0,0,-1},
{2,0,1,1,0,0,0},
{2,1,1,0,0,0,0},
{2,1,1,1,-1,0,0},
{2,1,1,1,0,-1,0},
{2,1,1,1,0,0,0},
{1,1,0,2,0,0,0},
{1,1,1,2,0,0,-1},
{2,1,0,1,0,0,0},
{1,2,1,1,0,-1,0},
{1,2,1,1,0,0,0},
{1,2,1,1,0,0,-1},
{1,1,1,1,-1,0,0},
{1,0,1,1,0,0,0},
{1,1,0,1,0,0,0},
{1,2,1,0,0,0,0},
{1,2,1,1,-1,0,0}
};

ids=Table[0,{Length[plist]}];

Do[
ids[[k]]=F[plist[[k]]],
{k,1,Length[plist]}]

Do[
WriteString["stdout","id I7(",plist[[k]],") = ",ids[[k]] /. rename // InputForm,";\n"],
{k,1,Length[plist]}]


