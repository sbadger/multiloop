id AMP(ptl(p1?,-1,-3,mS?),ptl(p2?,-1,3,mS?),ptl(p3?,-1,0,x3?)) =
	  0;

id AMP(ptl(p1?,-1,-3,mS?),ptl(p2?,-1,3,mS?),ptl(p3?,1,0,x3?)) =
	  0;

id AMP(ptl(p1?,-1,-3,mS?),ptl(p2?,1,3,mS?),ptl(p3?,-1,0,x3?)) =
	  - A(p2,p3)*B(p2,x3)*IB(p3,x3)*I;

id AMP(ptl(p1?,-1,-3,mS?),ptl(p2?,1,3,mS?),ptl(p3?,1,0,x3?)) =
	 A(p2,x3)*B(p2,p3)*IA(p3,x3)*I;

id AMP(ptl(p1?,1,-3,mS?),ptl(p2?,-1,3,mS?),ptl(p3?,-1,0,x3?)) =
	  - A(p2,p3)*B(p2,x3)*IB(p3,x3)*I;

id AMP(ptl(p1?,1,-3,mS?),ptl(p2?,-1,3,mS?),ptl(p3?,1,0,x3?)) =
	 A(p2,x3)*B(p2,p3)*IA(p3,x3)*I;

id AMP(ptl(p1?,1,-3,mS?),ptl(p2?,1,3,mS?),ptl(p3?,-1,0,x3?)) =
	  0;

id AMP(ptl(p1?,1,-3,mS?),ptl(p2?,1,3,mS?),ptl(p3?,1,0,x3?)) =
	  0;

