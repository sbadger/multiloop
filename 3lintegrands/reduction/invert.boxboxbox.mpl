interface(quiet=true):
#interface(rtablesize=infinity):
with(LinearAlgebra):
u:=-s-t;

start:=time():

read("boxboxbox.reduceM.log"):
printf("# finished reading ( %.4f s so far) \n",time()-start);

fd:=fopen("final/boxboxbox.integrand.NS.mpl",WRITE):

DSIZE:=311:
CSIZE:=199:

dvec:=Vector(DSIZE):
for k from 1 to DSIZE do:
dvec[k]:=DvecS[k]:
od:

eqs:=simplify(ReduceMatrixNS):

printf("# finished simplification ( %.4f s so far) \n",time()-start);
printf("# starting rank computation \n");
Rank(eqs);
printf("# finished rank computation ( %.4f s so far) \n",time()-start);

printf("# starting PLU decomp. \n");
eqsP,eqsL,eqsU:=LUDecomposition(eqs);
printf("# finished PLU decomp. ( %.4f s so far) \n",time()-start);
printf("# starting dtransform computation \n");
dtransform:=MatrixInverse(simplify(eqsL)).MatrixInverse(eqsP):
printf("# finished dtransform computation ( %.4f s so far) \n",time()-start);
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[CSIZE+1..DSIZE]));
printf("# starting inversion \n");
Ieqs:=simplify(MatrixInverse(simplify(eqsSq)));
printf("# finished inversion ( %.4f s so far) \n",time()-start);

dvecT:=dtransform.dvec:

dvecR:=SubVector(dvecT,[1..CSIZE]):
Cvec:=Ieqs.dvecR;

printf("# writing vectors ( %.4f s so far) \n",time()-start);

fprintf(fd,"CvecNS:=Vector(%d):\n",CSIZE):
for k from 1 to CSIZE do:
fprintf(fd,"CvecNS[%d]:=%A;\n",k,Cvec[k]):
od:

fprintf(fd,"ZvecS:=Vector(%d):\n",DSIZE-CSIZE):
for k from CSIZE+1 to DSIZE do:
fprintf(fd,"ZvecNS[%d]:=%A;\n",k-CSIZE,dvecT[k]):
od:

printf("# completed in %.4f s \n",time()-start);
