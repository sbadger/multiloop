Get["/lscr221/badger/gitrepos/mathematicam2/BasisDet-1-02.m"];

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* bow-tie bt21 - triangle-bubble*)
L=2;
Dim=4-2\[Epsilon];
n=3;
ExternalMomentaBasis={p1,p2};
Kinematics={
  p1^2 -> 0, p2^2 -> 0, p34^2 -> s,
  p1 p2 -> s/2, p1 p34 -> -s/2,
  p2 p34 -> -s/2,
  \[Omega]1^2 -> -t(s+t)/s
  \[Omega]2^2 -> -t(s+t)/s};
numeric={
  s -> 11,t -> 3
};
Props={l1, l1-p1, l1-p1-p2, l2+p1+p2, l2};
RenormalizationCondition={{{1,0},3},{{0,1},2},{{1,1},5}};

GenerateBasis[0]

fout = OpenWrite["basis/bt21.basis.h"];
WriteString[fout,"id Basisbt21 = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

