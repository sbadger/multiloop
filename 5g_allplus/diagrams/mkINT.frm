#-
S s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,s15,TR5,p1,p2,p3,p4,p5;
S s51;
S I,Ds,Dsm2;
CF dot,IA(a),S,IS,tr5,invtr5,ASYM(a),R,d,dbar,A(a),B(a);
S mu11,mu12,mu22,mu33;
S a,b,c,r4,w,w45p,w45m,w123,w543,w534,w132,w12p,w12m,w13p,w13m,w53p,w53m;
auto S k1,k2;
CF omega;
auto CF INTdb,INTbt;
auto S x;

S [mu11+mu22],[mu11^2*mu22],[mu11*mu12*mu22],[mu11*mu22];

Cf NUM,INV;

off stats;

.global

*** 12*345*
G xINTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) =
  -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
      - R(p1,p2,p3,p4,p5)*S(p1,p2)
      + ( S(p1,p2)*S(p3,p4)*S(p4,p5) + R(p1,p2,p3,p4,p5)*IS(p2,p3)*IS(p1,p5)*d(1,-1,1) )*dot(k2,p1)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));

*** 12*35*
G xINTdb220M3(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) =
  -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)/2*(
      S(p1,p2)^2*S(p1,p5) + R(p1,p2,p3,p4,p5)*S(p1,p2)*IS(p3,p4)*IS(p4,p5)*IS(p2,p3)*d(-1,1,1)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));

*** 12*34*
G xINTdb220M4(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
  I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)/2*(
      S(p1,p2)^2*S(p2,p3) - R(p1,p2,p3,p4,p5)*S(p1,p2)*IS(p3,p4)*IS(p4,p5)*IS(p1,p5)*d(-1,1,-1)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));

*** 12[3]4*
G xINTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
  I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    R(p1,p2,p3,p4,p5)
  )*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22));

*** 123[*,*]45

*G xINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
*  IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
*    + Dsm2*(
*      ((I/2)*s12*(2*dot(k1,omega(p1,p2,p3)) + s23)*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5)*mu12*(mu11 + mu22))/(s13)
*    )
*    + Dsm2^2*(
*      ((I/4)*s12*(2*dot(k1,omega(p1,p2,p3)) + s23)*(S(k1+k2) + s45)*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5)*mu11*mu22)/(s13*s45)
*    )
*);

G xINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
  I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    + Dsm2*(
      ((1/2)*S(k1-omega(p1,p3,p2))*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5)*mu12*(mu11 + mu22))
    )
    + Dsm2^2*(
      ((1/4)*S(k1-omega(p1,p3,p2))*((S(k1+k2) + s45)*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5)*mu11*mu22)/s45)
    )
);

*** 12[*,*]45
G xINTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
  IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
    + Dsm2*(
      ((I/2)*mu12*(mu11 + mu22)*(s23 - s45)*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5))*INV(s12 + s23 - s45)
    )
    + Dsm2^2*(
      ((I/4)*mu11*mu22*(S(k1+k2) + s45)*(-s23 + s45)*(s12*(-s15 + s23) - s23*s34 + s15*s45 + s34*s45 + TR5))*INV(s45*(-s12 - s23 + s45))
    )
);

*** 12[3,*]45
G xINTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  =
 I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(
 + xA*( 2*Dsm2*mu12*(mu11+mu22) + Dsm2^2*mu11*mu22*(4*dot(k1,p3)*dot(k2,p3) + S(k1+k2)*(s12+s45) + s12*s45)/s12/s45)
 - Dsm2^2*S(k1+k2)*mu11*mu22*s15
 - Dsm2^2*mu11*mu22/4*(
      xDp*(2*dot(k1,p3)*(s35 + 2*dot(k2,w45m) - (s12-s45)/s45*2*dot(k2,p5)) - s12*S(k1+k2))/s35/s12
    -  xD*(2*dot(k2,p3)*(s13 + 2*dot(k1,w12m) - (s45-s12)/s12*2*dot(k1,p1)) - s45*S(k1+k2))/s13/s45
  )
);

*** 12[3,*]4
G xINTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)  = 0;

id xA = (xDbar - xD*xDp/s13/s35/2)/4;
id xD    = d(-1, 1,-1)-TR5;
id xDp   = d( 1,-1, 1)+TR5;
id xDbar = dbar(-1, 1,-1)-TR5;

id k1w12p = dot(k1,w12p);
id k1w12m = dot(k1,w12m);
id k2w45p = dot(k2,w45p);
id k2w45m = dot(k2,w45m);
id k1pk2sq = S(k1+k2);
.sort

id x14 = dot(k1,p5);
id x21 = dot(k2,p1);
id x22 = dot(k2,p2);
id r4 = -I*tr5(p1,p2,p3,p4)/4;
id 1/TR5 = invtr5(p1,p2,p3,p4);
.sort
hide;
.sort
unhide xINTbt32,xINTbt22b,xINTbt22c;
denominators INV;
.sort
collect INV;
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);

id INV(s45 - s23 - s12) = 1/s13;
id INV(s15^2*s23^2 - 2*s13*s15*s23*s25 + s13^2*s25^2 + 2*s12*s15*s23*s25 + 2*s12*s15*s23^2 + 2*
         s12*s15^2*s23 + 2*s12*s13*s25^2 + 2*s12*s13*s23*s25 + 2*s12*s13*s15*s25 + 2*s12*s13*s15*s23 + 2*s12*s13^2*s25 + s12^2*s25^2 + 2*s12^2*s23*
         s25 + s12^2*s23^2 + 2*s12^2*s15*s25 + 4*s12^2*s15*s23 + s12^2*s15^2 + 4*s12^2*s13*s25 + 2*s12^2*s13*s23 + 2*s12^2*s13*s15 + s12^2*s13^2 + 2*
         s12^3*s25 + 2*s12^3*s23 + 2*s12^3*s15 + 2*s12^3*s13 + s12^4) = 1/TR5^2;


argument INV;
id s13 = s45 - s23 - s12;
id s25 = s34 - s15 - s12;
endargument;
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);

B I,NUM,INV,mu11,mu12,mu22,Dsm2,IA,dot;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

argument NUM;
id s13 = s45 - s23 - s12;
id s25 = s34 - s15 - s12;
repeat id s12^2*s15^2 =
  2*s12^2*s15*s23 - s12^2*s23^2 - 2*s12*s15*s23*s34 + 2*s12*s23^2*s34 - s23^2*s34^2 + 2*s12*s15^2*s45
- 2*s12*s15*s23*s45 - 2*s12*s15*s34*s45 - 2*s12*s23*s34*s45 - 2*s15*s23*s34*s45 + 2*s23*s34^2*s45
- s15^2*s45^2 + 2*s15*s34*s45^2 - s34^2*s45^2 + TR5^2
;
endargument;

id 1/s13 = INV(s45 - s23 - s12);

id NUM(x?) = x;
AB s12,s23,s34,s45,s15;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(x?)*INV(x?) = 1;
.sort
unhide;

#do i=1,5
#do j={'i'+1},5
id s'i''j' = S(p'i',p'j');
id 1/s'i''j' = IS(p'i',p'j');
argument NUM,INV;
id s'i''j' = S(p'i',p'j');
endargument;
#enddo
#enddo
id I^2 = -1;

id d(a?,b?,c?) = S(p1,p2)*S(p1,p5)+a*S(p1,p2)*S(p2,p3)+b*S(p2,p3)*S(p3,p4)-S(p4,p5)*S(p1,p5)+c*S(p3,p4)*S(p4,p5);
id dbar(a?,b?,c?) = S(p1,p2)*S(p1,p5)+a*S(p1,p2)*S(p2,p3)+b*S(p2,p3)*S(p3,p4)+S(p4,p5)*S(p1,p5)+c*S(p3,p4)*S(p4,p5);
id R(p1?,p2?,p3?,p4?,p5?) = invtr5(p1,p2,p3,p4)*S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p1,p5);
id IS(?x)*S(?x) = 1;
id mu33 = mu11+mu22+mu12;

.store
L newINTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTdb230(k2,k1,mu22,mu12,mu11,p5,p4,p3,p2,p1);
L newINTdb220M2(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTdb220M3(k2,k1,mu22,mu12,mu11,p5,p4,p3,p2,p1);
L newINTdb220M4(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTdb220M4(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = -xINTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = -xINTbt32(k2,k1,mu22,mu12,mu11,p5,p4,p3,p2,p1);
L newINTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);
L newINTbt22e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTbt22c(xxp345mk1,xxp12mk2,mu22,mu12,mu11,p3,p4,p5,p1,p2);
L newINTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = xINTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5);

id invtr5(?x,p5,?y) = -invtr5(?x,p1,?y)-invtr5(?x,p2,?y)-invtr5(?x,p3,?y)-invtr5(?x,p4,?y);
id invtr5(?x,p1?,?y,p1?,?z) = 0;
id invtr5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = 1/TR5;

id tr5(?x,p5,?y) = -tr5(?x,p1,?y)-tr5(?x,p2,?y)-tr5(?x,p3,?y)-tr5(?x,p4,?y);
id tr5(?x,p1?,?y,p1?,?z) = 0;
id tr5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = TR5;

argument S,dot;
id omega(p1,p2,p3) = w123;
id omega(p1,p3,p2) = w132;
id omega(p5,p4,p3) = w543;
id omega(p5,p3,p4) = w534;
endargument;

argument S;
id xxp345mk1 = -k1-p1-p2;
id xxp12mk2 = -k2+p1+p2;
endargument;
id S(-k1-k2) = S(k1+k2);

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id IS(p'i',p'j') = 1/s'i''j';
id S(p'j',p'i') = s'i''j';
id IS(p'j',p'i') = 1/s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

argument NUM,INV;
#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id IS(p'i',p'j') = 1/s'i''j';
id S(p'j',p'i') = s'i''j';
id IS(p'j',p'i') = 1/s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo
endargument;

*** change to new omega basis ***
*id dot(k1,w12m) = -s13/s12*dot(k1,w13m);
*id dot(k1,w12p) = -s13/s12*dot(k1,w13p) + 2*s23/s12*dot(k1,p1)*I;
*id dot(k2,w45m) = -s35/s45*dot(k2,w53m);
*id dot(k2,w45p) = -s35/s45*dot(k2,w53p) + 2*s34/s45*dot(k2,p5)*I;

.sort

format 150;
#write <INT_5g+++++.prc> "id INTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTdb220c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTdb220M2(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTdb220e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTdb220M4(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt22e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt22e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.prc> "id INTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5) = %e" newINTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)

id S(k1-w132) = S(k1-w132)-mu11;
id S(k2-w534) = S(k2-w534)-mu22;
id S(k1+k2) = 2*dot(k1,k2)-mu12;
.sort

format maple;
#write <INT_5g+++++.mpl> "aINT_db320D  := (k1,k2,mu11,mu12,mu22) -> %e;" newINTdb320(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt32D  := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt32(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_db230D  := (k1,k2,mu11,mu12,mu22) -> %e;" newINTdb230(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt23D  := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt23(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_db220Dc := (k1,k2,mu11,mu12,mu22) -> %e;" newINTdb220M2(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_db220Db := (k1,k2,mu11,mu12,mu22) -> %e;" newINTdb220b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_db220De := (k1,k2,mu11,mu12,mu22) -> %e;" newINTdb220M4(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt22Db := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt22b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt22Dc := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt22c(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt22De := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt22e(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)
#write <INT_5g+++++.mpl> "aINT_bt21Db := (k1,k2,mu11,mu12,mu22) -> %e;" newINTbt21b(k1,k2,mu11,mu12,mu22,p1,p2,p3,p4,p5)

format 150;
B NUM,INV,[mu11^2*mu22],[mu11*mu12*mu22],[mu11*mu22],[mu11+mu22],mu12,mu11,mu22,IA;
print+s newINTbt32,newINTbt22c,newINTbt22b;
.end
