interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

Ds:=Dsm2+2:

read("OnShellSolutions.mpl");
read("tmp/checks_5g+++++.mpl");

### non-planar ###
### octa-cuts ###
#read("tmp/db332.input.mpl");
#read("tmp/db422.input.mpl");
#read("tmp/db341ALT.input.mpl");

### hepta-cuts ###
#read("tmp/bt340ALT.input.mpl");
#read("tmp/db232.input.mpl");
#read("tmp/dbNP331M1.input.mpl");
#read("tmp/dbNP331M2.input.mpl");
#read("tmp/dbNP331M3.input.mpl");

### hexa-cuts ###
read("tmp/btNP3305L.input.mpl");

#print("332");
#k1,k2,mu11,mu12,mu22 := on_shell_332([p1,p2,p4,p5,p3],1,2,3):
#expand(INT_332([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP332([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("422");
#k1,k2,mu11,mu12,mu22 := on_shell_422([p1,p2,p3,p4,p5],1,2,3):
#expand(INT_422([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP422([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("232");
#k1,k2,mu11,mu12,mu22 := on_shell_232([p1,p2,p3,p4,p5],1,2,3,4):
#expand(INT_232([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP232([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("NP331M1");
#k1,k2,mu11,mu12,mu22 := on_shell_3315L([p1,p2,p3,p4,p5],1,2,3,4):
#expand(INT_NP331M1([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP331M1([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("NP331M2");
#k1,k2,mu11,mu12,mu22 := on_shell_3315L([p1,p2,p3,p4,p5],1,2,3,4):
#expand(INT_NP331M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP331M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("NP331M3");
#k2p45,k1p12,mu22,mu12,mu11 := on_shell_3315L([p4,p5,p3,p1,p2],1,2,3,4):
#k1:=-k1p12+p1+p2:
#k2:=-k2p45+p4+p5:
#expand(INT_NP331M3([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#expand(Delta_NP331M3([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
#%-%%;

#print("340ALT");
#k2p3,k1,mu22,mu12,mu11 := on_shell_430([p3,p5,p4,p2,p1],1,2,3,4):
#k2:=k2p3-p3:
#collect(expand(INT_340ALT([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22)),{Dsm2});
#collect(expand(Delta_430([p3,p5,p4,p2,p1],k2+p3,k1,mu22,mu12,mu11)),{Dsm2});

ww := (p1,p2,p3) -> (eta(p1,p2)*AB(p2,p3,p1) - eta(p2,p1)*AB(p1,p3,p2))/2/S(p1,p2):
#tmpDelta_430:=(p,k1,k2,mu11,mu12,mu22)->
#  -I*S(p[1],p[2])*trp(p[1],p[3],p[4],p[5])/(2*A(p[1],p[2])*A(p[2],p[3])*A(p[3],p[4])*A(p[4],p[5])*A(p[5],p[1])*S(p[1],p[3]))*( 2*dot(k1,ww(p[1],p[2],p[3]))+S(p[2],p[3]) )*(
#    + 2*Dsm2*(mu11+mu22)*mu12
#    + Dsm2^2*mu11*mu22*(S(k1,k2)-mu11-mu12-mu22+S(p[4],p[5]))/S(p[4],p[5])
#):
#collect(expand(tmpDelta_430([p3,p5,p4,p2,p1],k2+p3,k1,mu22,mu12,mu11)),{Dsm2});

print("NP330;5L");
k1,k2,mu11,mu12,mu22 := on_shell_3305L([p1,p2,p3,p4,p5],1,2,3,4,5):

collect(expand(INT_NP3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22)),{Dsm2});
#expand(Delta_NP3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));

GMy3 := 2*dot(k1-k2,p3);
GMx7 := S(k1+k2+p3)-mu11-mu12-mu22;
GMx8 := S(k1+k2)-mu11-mu12-mu22;

collect(expand(1./(GMx8-GMx7)*(
+ 1/2*(2*S(p1,p2)-2*S(p4,p5)+GMx7-GMx8+GMy3)*Delta_3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22)
+ 1/2*(GMx7-GMx8+GMy3)*Delta_3305L([p4,p5,p3,p1,p2],-k2+p4+p5,-k1+p1+p2,mu22,mu12,mu11)
- Delta_430([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22)
+ Delta_430([p3,p4,p5,p1,p2],-k2+p3+p4+p5,-k1+p1+p2,mu22,mu12,mu11)
- Delta_430([p4,p5,p3,p1,p2],-k2+p4+p5,-k1+p1+p2,mu22,mu12,mu11)
+ Delta_430([p3,p1,p2,p4,p5],k1+p3,k2,mu11,mu12,mu22)
)),{Dsm2});

GMx7-GMx8+GMy3 - 4*dot(k1,p3);

tmpDelta_NP3305L:=(p,k1,k2,mu11,mu12,mu22)->
  I*Dsm2^2*mu11*mu22/(2*A(p[1],p[2])*A(p[2],p[3])*A(p[3],p[4])*A(p[4],p[5])*A(p[5],p[1])*S(p[1],p[2]))*(
    + (S(p[4],p[5])-S(p[1],p[2]))*trp(p1,p2,p4,p5)
    + S(p[1],p[2])*trp(p[1],p[3],p[4],p[5])/S(p[1],p[3])*2*dot(k1,ww(p[1],p[2],p[3]))
    - S(p[4],p[5])*trp(p[1],p[2],p[3],p[5])/S(p[3],p[5])*2*dot(k2,ww(p[5],p[4],p[3]))
    + B(p[1],p[2])*A(p[1],p[5])*B(p[4],p[5])*(A(p[1],p[4])*A(p[2],p[3])*A(p[3],p[5])+A(p[1],p[3])*A(p[2],p[5])*A(p[3],p[4]))/2/A(p[1],p[3])/A(p[3],p[5])*4*dot(k1,p[3])
):
collect(expand(tmpDelta_NP3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22)),{Dsm2});

B(p3,p1)*A(p1,p4)*B(p4,p5)*A(p5,p1)*B(p1,p2)*A(p2,p3);
-(S(p2,p3)-S(p4,p5))*trp(p1,p3,p2,p5)-S(p1,p5)*S(p1,p3)*S(p2,p3);

B(p5,p3)*A(p3,p4)*B(p4,p5)*A(p5,p2)*B(p2,p1)*A(p1,p5);
-(S(p1,p2)-S(p3,p4))*trp(p5,p3,p4,p1)+S(p1,p5)*S(p3,p5)*S(p3,p4);


#%-%%;

quit;

### planar ###
### octa-cuts ###
read("tmp/db431.input.mpl");
read("tmp/db341.input.mpl");

### hepta-cuts ###
read("tmp/db421.input.mpl");
read("tmp/bt430.input.mpl");
read("tmp/bt340.input.mpl");
read("tmp/db331M2.input.mpl");
read("tmp/db3315L.input.mpl");

### hexa-cuts ###
read("tmp/db3215L.input.mpl");
read("tmp/bt420.input.mpl");
read("tmp/bt330M2.input.mpl");
read("tmp/bt330M4.input.mpl");
read("tmp/bt3305L.input.mpl");

### penta-cuts ###
read("tmp/bt3205L.input.mpl");

print("431");
k1,k2,mu11,mu12,mu22 := on_shell_431([p1,p2,p3,p4,p5],1,2,3):
expand(INT_431([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_431([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

print("341");
k2,k1,mu22,mu12,mu11 := on_shell_431([p5,p4,p3,p2,p1],1,2,3):
expand(INT_341([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(-Delta_431([p5,p4,p3,p2,p1],k2,k1,mu22,mu12,mu11));
%-%%;

print("421");
k1,k2,mu11,mu12,mu22 := on_shell_421([p1,p2,p3,p4,p5],1,2,3,4):
expand(INT_421([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));

print("430");
k1,k2,mu11,mu12,mu22 := on_shell_430([p1,p2,p3,p4,p5],1,2,3,4):
expand(INT_430([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_430([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

print("340");
k2,k1,mu22,mu12,mu11 := on_shell_430([p5,p4,p3,p2,p1],1,2,3,4):
expand(INT_340([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(-Delta_430([p5,p4,p3,p2,p1],k2,k1,mu22,mu12,mu11));
%-%%;

print("331;M2");
k1,k2,mu11,mu12,mu22 := on_shell_331M2([p1,p2,p3,p4,p5],1,2,3,4):
expand(INT_331M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_331M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

print("331:5L");
k1,k2,mu11,mu12,mu22 := on_shell_3315L([p1,p2,p3,p4,p5],1,2,3,4):
expand(INT_3315L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_3315L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

print("330;M2");
k1,k2,mu11,mu12,mu22 := on_shell_330M2([p1,p2,p3,p4,p5],1,2,3,4,5):
expand(INT_330M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_330M2([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

print("330;5L");
k1,k2,mu11,mu12,mu22 := on_shell_3305L([p1,p2,p3,p4,p5],1,2,3,4,5):
expand(INT_3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_3305L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
%-%%;

#for i from 1 to 4 do:
#  for j from i+1 to 5 do:
#    s||i||j := S(p||i,p||j);
#  od:
#od:
#TR5 := tr5(p1,p2,p3,p4):
#w := (AB(p5,p1,p4)*eta(p4,p5)-AB(p4,p1,p5)*eta(p5,p4))/2/s45:
#w123 := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:
#w543 := (AB(p4,p3,p5)*eta(p5,p4)-AB(p5,p3,p4)*eta(p4,p5))/2/s45:
#w132 := (AB(p3,p2,p1)*eta(p1,p3)-AB(p1,p2,p3)*eta(p3,p1))/2/s13:
#w534 := (AB(p3,p4,p5)*eta(p5,p3)-AB(p5,p4,p3)*eta(p3,p5))/2/s35:
#w12p := (AB(p2,p3,p1)*eta(p1,p2)+AB(p1,p3,p2)*eta(p2,p1))/2/s12*I:
#w12m := (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12:
#w13p := (AB(p3,p2,p1)*eta(p1,p3)+AB(p1,p2,p3)*eta(p3,p1))/2/s13*I:
#w13m := (AB(p3,p2,p1)*eta(p1,p3)-AB(p1,p2,p3)*eta(p3,p1))/2/s13:
#w45p := (AB(p4,p3,p5)*eta(p5,p4)+AB(p5,p3,p4)*eta(p4,p5))/2/s45*I:
#w45m := (AB(p4,p3,p5)*eta(p5,p4)-AB(p5,p3,p4)*eta(p4,p5))/2/s45:
#w53p := (AB(p3,p4,p5)*eta(p5,p3)+AB(p5,p4,p3)*eta(p3,p5))/2/s35*I:
#w53m := (AB(p3,p4,p5)*eta(p5,p3)-AB(p5,p4,p3)*eta(p3,p5))/2/s35:
#read("INT_5g+++++.mpl"):
#expand(aINT_bt22Db(k1,k2,mu11,mu12,mu22));

print("420");
k1,k2,mu11,mu12,mu22 := on_shell_420([p1,p2,p3,p4,p5],1,2,3,4,5):
expand(INT_420([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));

print("321;5L");
k1,k2,mu11,mu12,mu22 := on_shell_3215L([p1,p2,p3,p4,p5],1,2,3,4,5):
expand(INT_3215L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));

print("330;M4");
xxk2,xxk1,mu22,mu12,mu11 := on_shell_330M2([p3,p4,p5,p1,p2],1,2,3,4,5):
k2 := -xxk2+p3+p4+p5:
k1 := -xxk1+p1+p2:

expand(INT_330M4([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));
expand(Delta_330M2([p3,p4,p5,p1,p2],-k2+p3+p4+p5,-k1+p1+p2,mu22,mu12,mu11));
%-%%;

print("320;5L");
k1,k2,mu11,mu12,mu22 := on_shell_3205L([p1,p2,p3,p4,p5],1,2,3,4,5,6):
expand(INT_3205L([p1,p2,p3,p4,p5],k1,k2,mu11,mu12,mu22));

