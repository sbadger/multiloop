id AMP(ptl(p1?,-1,-3/2,mt?),ptl(p2?,-1,-3/2,mt?),ptl(p3?,-1,3,mS?)) =
	 A(q1,q2)*I + B(x1,x2)*IB(q1,x1)*IB(q2,x2)*mt^2*I;

id AMP(ptl(p1?,-1,-3,mt?),ptl(p2?,-1,3/2,mt?),ptl(p3?,-1,3/2,mS?)) =
	  - A(q2,q3)*I - B(x2,x3)*IB(q2,x2)*IB(q3,x3)*mt^2*I;

id AMP(ptl(p1?,-1,-3/2,mt?),ptl(p2?,-1,-3/2,mt?),ptl(p3?,1,3,mS?)) =
	 A(q1,q2)*I + B(x1,x2)*IB(q1,x1)*IB(q2,x2)*mt^2*I;

id AMP(ptl(p1?,-1,-3,mt?),ptl(p2?,-1,3/2,mt?),ptl(p3?,1,3/2,mS?)) =
	  - A(q2,x3)*B(q2,x2)*IA(q3,x3)*IB(q2,x2)*mt*I + A(q3,x3)*B(q3,x2)*IA(q3,x3)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,-1,-3/2,mt?),ptl(p2?,1,-3/2,mt?),ptl(p3?,-1,3,mS?)) =
	 A(q1,x2)*B(q1,x1)*IA(q2,x2)*IB(q1,x1)*mt*I - A(q2,x2)*B(q2,x1)*IA(q2,x2)*IB(q1,x1)*mt*I;

id AMP(ptl(p1?,-1,-3,mt?),ptl(p2?,1,3/2,mt?),ptl(p3?,-1,3/2,mS?)) =
	  - A(q2,x2)*B(q2,x3)*IA(q2,x2)*IB(q3,x3)*mt*I + A(q3,x2)*B(q3,x3)*IA(q2,x2)*IB(q3,x3)*mt*I;

id AMP(ptl(p1?,-1,-3/2,mt?),ptl(p2?,1,-3/2,mt?),ptl(p3?,1,3,mS?)) =
	 A(q1,x2)*B(q1,x1)*IA(q2,x2)*IB(q1,x1)*mt*I - A(q2,x2)*B(q2,x1)*IA(q2,x2)*IB(q1,x1)*mt*I;

id AMP(ptl(p1?,-1,-3,mt?),ptl(p2?,1,3/2,mt?),ptl(p3?,1,3/2,mS?)) =
	  - A(x2,x3)*IA(q2,x2)*IA(q3,x3)*mt^2*I - B(q2,q3)*I;

id AMP(ptl(p1?,1,-3/2,mt?),ptl(p2?,-1,-3/2,mt?),ptl(p3?,-1,3,mS?)) =
	 A(q1,x1)*B(q1,x2)*IA(q1,x1)*IB(q2,x2)*mt*I - A(q2,x1)*B(q2,x2)*IA(q1,x1)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,1,-3,mt?),ptl(p2?,-1,3/2,mt?),ptl(p3?,-1,3/2,mS?)) =
	  - A(q2,q3)*I - B(x2,x3)*IB(q2,x2)*IB(q3,x3)*mt^2*I;

id AMP(ptl(p1?,1,-3/2,mt?),ptl(p2?,-1,-3/2,mt?),ptl(p3?,1,3,mS?)) =
	 A(q1,x1)*B(q1,x2)*IA(q1,x1)*IB(q2,x2)*mt*I - A(q2,x1)*B(q2,x2)*IA(q1,x1)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,1,-3,mt?),ptl(p2?,-1,3/2,mt?),ptl(p3?,1,3/2,mS?)) =
	  - A(q2,x3)*B(q2,x2)*IA(q3,x3)*IB(q2,x2)*mt*I + A(q3,x3)*B(q3,x2)*IA(q3,x3)*IB(q2,x2)*mt*I;

id AMP(ptl(p1?,1,-3/2,mt?),ptl(p2?,1,-3/2,mt?),ptl(p3?,-1,3,mS?)) =
	 A(x1,x2)*IA(q1,x1)*IA(q2,x2)*mt^2*I + B(q1,q2)*I;

id AMP(ptl(p1?,1,-3,mt?),ptl(p2?,1,3/2,mt?),ptl(p3?,-1,3/2,mS?)) =
	  - A(q2,x2)*B(q2,x3)*IA(q2,x2)*IB(q3,x3)*mt*I + A(q3,x2)*B(q3,x3)*IA(q2,x2)*IB(q3,x3)*mt*I;

id AMP(ptl(p1?,1,-3/2,mt?),ptl(p2?,1,-3/2,mt?),ptl(p3?,1,3,mS?)) =
	 A(x1,x2)*IA(q1,x1)*IA(q2,x2)*mt^2*I + B(q1,q2)*I;

id AMP(ptl(p1?,1,-3,mt?),ptl(p2?,1,3/2,mt?),ptl(p3?,1,3/2,mS?)) =
	  - A(x2,x3)*IA(q2,x2)*IA(q3,x3)*mt^2*I - B(q2,q3)*I;

