id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,0),ptl(p3?,-1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,0),ptl(p3?,-1,1/2),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,0),ptl(p3?,1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,0),ptl(p3?,1,1/2),ptl(p4?,1,0)) =
	  - A(p1,p2)^2*IA(p1,p4)*IA(p3,p4)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,0),ptl(p3?,-1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,0),ptl(p4?,1,0)) =
	 A(p1,p3)^3*IA(p1,p2)*IA(p1,p4)*IA(p3,p4)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,0),ptl(p3?,-1,1/2),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,0),ptl(p4?,-1,0)) =
	 A(p1,p4)^2*A(p2,p4)*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,0),ptl(p3?,1,1/2),ptl(p4?,-1,0)) =
	 A(p1,p4)^2*IA(p1,p2)*IA(p2,p3)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,0),ptl(p3?,1,1/2),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,0),ptl(p3?,-1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,-1,0),ptl(p4?,1,0)) =
	  - A(p1,p3)*A(p2,p3)^2*IA(p1,p2)*IA(p1,p4)*IA(p3,p4)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,0),ptl(p3?,-1,1/2),ptl(p4?,1,0)) =
	 A(p2,p3)^2*IA(p1,p4)*IA(p3,p4)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,0),ptl(p4?,-1,0)) =
	  - A(p2,p4)^3*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,0),ptl(p3?,1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,1/2),ptl(p3?,1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,0),ptl(p3?,1,1/2),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,0),ptl(p3?,-1,1/2),ptl(p4?,-1,0)) =
	  - A(p3,p4)^2*IA(p1,p2)*IA(p2,p3)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,-1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,0),ptl(p3?,-1,1/2),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,0),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,0),ptl(p3?,1,1/2),ptl(p4?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,1/2),ptl(p3?,1,0),ptl(p4?,1,0)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,0),ptl(p3?,1,1/2),ptl(p4?,1,0)) =
	  0;

