interface(quiet=true);
interface(displayprecision=10);
with(spinors):

Digits:=50:
assign(declaremomn([seq(0,k=1..4)])):

cfnums:={c41=4.53,b31=2.7854,c31=1.9.2,b41=1.56292,a41=8.9232,a31=0.4,b21=0.985}:

read("tbox.ossol.log"):

for k from 1 to 14 do:

print("==========",k,"===========");

unassign('a11','a21','a31','a41'):
unassign('b11','b21','b31','b41'):
unassign('c11','c21','c31','c41'):

assign(sol[k]):

print(a11,a21,a31,a41);
print(b11,b21,b31,b41);
print(c11,c21,c31,c41);

k1 := a11*p1 + a21*p2 + B(p1,p3)*IB(p2,p3)*a31*eta(p1,p2)/2 + IB(p1,p3)*B(p2,p3)*a41*eta(p2,p1)/2:
k2 := b11*p1 + b21*p2 + B(p1,p3)*IB(p2,p3)*b31*eta(p1,p2)/2 + IB(p1,p3)*B(p2,p3)*b41*eta(p2,p1)/2:
k3 := c11*p3 + c21*p4 + B(p1,p3)*IB(p1,p4)*c31*eta(p3,p4)/2 + IB(p1,p3)*B(p1,p4)*c41*eta(p4,p3)/2:

l[1]:=k1:
l[2]:=k1-p1:
l[3]:=k1-p1-p2:
l[4]:=k1+k2:
l[5]:=k3:
l[6]:=k3-p3:
l[7]:=k3-p3-p4:
l[8]:=k1+k2+p1+p2:
l[9]:=k2+p1+p2:
l[10]:=-k3+k1+k2:

for i from 1 to 10 do:
tmp:=subs(cfnums,expand(dot(l[i],l[i]))):
if abs(tmp)<1e-15 then: tmp:=0: fi:
print(i,tmp);
od:

od:

unassign('a11','a21','a31','a41'):
unassign('b11','b21','b31','b41'):
unassign('c11','c21','c31','c41'):

read("tcourt.ossol.log"):
#cfnums:={}:
cfnums:={a31=0.2322,a41=3.42,b11=4.34,b31=0.293228,b41=0.342,c21=0.892,c31=0.43679,c41=1.2,c11=4.5355}:

for k from 1 to 16 do:

print("==========",k,"===========");

unassign('a11','a21','a31','a41'):
unassign('b11','b21','b31','b41'):
unassign('c11','c21','c31','c41'):

assign(sol[k]):

print(a11,a21,a31,a41);
print(b11,b21,b31,b41);
print(c11,c21,c31,c41);

k1 := a11*p1 + a21*p2 + B(p1,p3)*IB(p2,p3)*a31*eta(p1,p2)/2 + IB(p1,p3)*B(p2,p3)*a41*eta(p2,p1)/2:
k2 := b11*p3 + b21*p4 + B(p1,p3)*IB(p1,p4)*b31*eta(p3,p4)/2 + IB(p1,p3)*B(p1,p4)*b41*eta(p4,p3)/2:
k3 := c11*p3 + c21*p4 + B(p1,p3)*IB(p1,p4)*c31*eta(p3,p4)/2 + IB(p1,p3)*B(p1,p4)*c41*eta(p4,p3)/2:

l[1]:=k1:
l[2]:=k1-p1:
l[3]:=k1-p1-p2:
l[4]:=k2:
l[5]:=k2-p3:
l[6]:=k2-k3-p3:
l[7]:=k2-k3-p3-p4:
l[8]:=k2-k3-k1+p1+p2:
l[9]:=k2-k1+p1+p2:
l[10]:=k3:

for i from 1 to 10 do:
tmp:=subs(cfnums,expand(dot(l[i],l[i]))):
if abs(tmp)<1e-15 then: tmp:=0: fi:
print(i,tmp);
od:

od:

