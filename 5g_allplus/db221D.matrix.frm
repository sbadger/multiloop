#-
#include- 2to3tools.prc

#define topo "db221D"

off stats;

.global

G basis41(nu1) = MOM(p5,nu1);
G basis42(nu1) = MOM(p1,nu1);
G basis43(nu1) = MOM(p4,nu1);
G basis44(nu1) = MOM(p2,nu1);

.store

G x13 = 2*MOM(k1,nu1)*basis43(nu1);
G x22 = 2*MOM(k2,nu1)*basis42(nu1);
G x21 = 2*MOM(k2,nu1)*basis41(nu1);
G xmu11 = mu11;
G xmu12 = mu12;
G xmu22 = mu22;

L zero1 = MOM(k1,nu1)*MOM(k1,nu1)-mu11;
L zero2 = MOM(k1,nu1)*MOM(p1,nu1);
L zero3 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)-s12/2;
L zero4 = MOM(k2,nu1)*MOM(p3,nu1)+MOM(k2,nu1)*MOM(p4,nu1)-s34/2;
L zero5 = MOM(k2,nu1)*MOM(p4,nu1);
L zero6 = MOM(k2,nu1)*MOM(k2,nu1)-mu22;
L zero7 = 2*MOM(k1,nu1)*MOM(k2,nu1)-mu12;
L zero8 = MOM(k1,nu1)*MOM(p5,nu1)+MOM(k2,nu1)*MOM(p5,nu1);

#call subsLmom(1,'topo')

id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id MOM(p1?,nu1?)*AB(p2?,nu1?,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?,p2?)*AB(p3?,nu1?,p4?) = -2*A(p1,p3)*B(p2,p4);
id NUM(x?) = x;

#call cancel
repeat;
id,once AB(p1?,p2?ps,?args) = A(p1,p2)*BB(p2,?args);
al,once AA(p1?,p2?ps,?args) = A(p1,p2)*BA(p2,?args);
al,once BB(p1?,p2?ps,?args) = B(p1,p2)*AB(p2,?args);
al,once BA(p1?,p2?ps,?args) = B(p1,p2)*AA(p2,?args);
al,once IAB(p1?,p2?ps,?args) = IA(p1,p2)*IBB(p2,?args);
al,once IAA(p1?,p2?ps,?args) = IA(p1,p2)*IBA(p2,?args);
al,once IBB(p1?,p2?ps,?args) = IB(p1,p2)*IAB(p2,?args);
al,once IBA(p1?,p2?ps,?args) = IB(p1,p2)*IAA(p2,?args);

id BB(p1?,p2?) = B(p1,p2);
id IBB(p1?,p2?) = IB(p1,p2);
id AA(p1?,p2?) = A(p1,p2);
id IAA(p1?,p2?) = IA(p1,p2);

endrepeat;
id BA(?args) = AB(reverse_(?args));
id IBA(?args) = IAB(reverse_(?args));
#call cancel

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

*format maple;
*print+s;
*.end

*#do i=1,5
*#do j=1,5
*#if 'j'>'i'
*id S(p'i',p'j') = s'i''j';
*id IS(p'i',p'j') = 1/s'i''j';
*id S(p'j',p'i') = s'i''j';
*id IS(p'j',p'i') = 1/s'i''j';
*id dot(p'i',p'j') = s'i''j'/2;

*id A(p'i',p'j') = a'i''j';
*id IA(p'i',p'j') = 1/a'i''j';
*id B(p'i',p'j') = b'i''j';
*id IB(p'i',p'j') = 1/b'i''j';

*#endif
*#enddo
*#enddo

*B tau1,...,tau3,mu11,mu12,mu22;
*.sort
*collect NUM;
*factarg NUM;
*chainout NUM;
*splitarg NUM;
*id NUM(x?) = x;
*repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);
*argument NUM;
*#do i=1,5
*#do j=1,5
*#if 'j'>'i'
*id a'i''j'*b'i''j' = -s'i''j';
*#endif
*#enddo
*#enddo
*endargument;
*id NUM(0) = 0;

*B NUM,tau1,...,tau3,mu11,mu12,mu22;
*.sort
*collect NUM;
*factarg NUM;
*chainout NUM;
*splitarg NUM;
*id NUM(x?) = x;
*repeat id NUM(x1?, x2?, ?args) = NUM(x1+x2,?args);
*argument NUM;
*#do i=1,5
*#do j=1,5
*#if 'j'>'i'
*id a'i''j'*b'i''j' = -s'i''j';
*#endif
*#enddo
*#enddo
*endargument;
*id NUM(0) = 0;

#call simplify5
id tr5 = TR5;

format 120;
B tau1,...,tau2,b3,a3,a4;
print+s;
.store

G Delta = Basis'topo';

#include- basis/'topo'.basis.h

id Power(x1?,x2?) = x1^x2;

id mu11 = xmu11;
id mu12 = xmu12;
id mu22 = xmu22;
.sort

B tau1,tau2,tau3;
print[];
.sort

#$nc = 1;
B cc;
.sort
keep brackets;
id cc(?x) = cc($nc,?x);
$nc=$nc+1;
.sort
#write "{'$nc'-1} integrand coefficients"
#do cc=1,{'$nc'-1}
id cc('cc',?x$clbl) = cc('cc',?x);
.sort
#write <final/'topo'.coeffvecs.mpl> "Clbl['cc']:=cs('$clbl'):"
#enddo

.store

#write <'topo'.ReduceM.log> "ReduceMatrix:=Matrix(83,{'$nc'-1}):"
#write <final/'topo'.coeffvecs.mpl> "Dvec:=Vector(83):"
#define dd "0"

#do pow1=0,6
#do pow2=0,6
#do pow3=0,6

#$nonzero=0;
L tmpRow = Delta;

#do pp=1,3
if(count(tau'pp',1)!=`pow`pp'');
discard;
endif;
id tau`pp'=1;
id 1/tau`pp'=1;
#enddo

.sort
$nonzero=$nonzero+1;
.sort
hide;

*#write "'pow1','pow2','pow3','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/'topo'.coeffvecs.mpl> "Dvec['dd']:=ds('pow1','pow2','pow3'):"
#write <final/'topo'.coeffvecs.prc> "id ds('pow1','pow2','pow3') = ds('dd');"

#do cc=1,{'$nc'-1}
  L tmpEl'cc' = tmpRow;
  id cc('cc',?x$clbl) = 1;
  id cc(?x)=0;
  .sort
  format maple;
  #write <'topo'.ReduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
  .sort
  hide;
#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo

.end
