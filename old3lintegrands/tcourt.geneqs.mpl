eqs:=[]:
eqs:=[op(eqs), - a31*a41 + a11*a21=0]:
eqs:=[op(eqs), - a31*a41 - a21 + a11*a21=0]:
eqs:=[op(eqs),1 - a31*a41 - a21 - a11 + a11*a21=0]:
eqs:=[op(eqs), - b31*b41 + b11*b21=0]:
eqs:=[op(eqs), - b31*b41 - b21 + b11*b21=0]:
eqs:=[op(eqs), - c31*c41 + c21 + c11*c21 + b41*c31 + b31*c41 - b31*b41
       - b21 - b21*c11 - b11*c21 + b11*b21=0]:
eqs:=[op(eqs),1 - c31*c41 + c21 + c11 + c11*c21 + b41*c31 + b31*c41 - 
      b31*b41 - b21 - b21*c11 - b11 - b11*c21 + b11*b21=0]:
eqs:=[op(eqs), - S(p1,p2) + S(p1,p2)*c31*c41 - S(p1,p2)*c21 - S(p1,p2)*
      c11 - S(p1,p2)*c11*c21 - S(p1,p2)*b41*c31 - S(p1,p2)*b31*c41 + S(
      p1,p2)*b31*b41 + S(p1,p2)*b21 + S(p1,p2)*b21*c11 + S(p1,p2)*b11
       + S(p1,p2)*b11*c21 - S(p1,p2)*b11*b21 + 2*S(p1,p2)*a31*c31 - S(
      p1,p2)*a31*c21 + S(p1,p2)*a31*c11 - 2*S(p1,p2)*a31*b31 + S(p1,p2)
      *a31*b21 - S(p1,p2)*a31*b11 + S(p1,p2)*a31*a41 + S(p1,p2)*a21 - 
      S(p1,p2)*a21*c31 + S(p1,p2)*a21*c21 + S(p1,p2)*a21*b31 - S(p1,p2)
      *a21*b21 + S(p1,p2)*a11 + S(p1,p2)*a11*c31 + S(p1,p2)*a11*c11 - 
      S(p1,p2)*a11*b31 - S(p1,p2)*a11*b11 - S(p1,p2)*a11*a21 + S(p1,p2)
      ^2*IS(p1,p4)*a31*c31 - S(p1,p2)^2*IS(p1,p4)*a31*b31 + S(p1,p4)*
      a41*c41 - S(p1,p4)*a41*c31 + S(p1,p4)*a41*c21 - S(p1,p4)*a41*c11
       - S(p1,p4)*a41*b41 + S(p1,p4)*a41*b31 - S(p1,p4)*a41*b21 + S(p1,
      p4)*a41*b11 - S(p1,p4)*a31*c41 + S(p1,p4)*a31*c31 - S(p1,p4)*a31*
      c21 + S(p1,p4)*a31*c11 + S(p1,p4)*a31*b41 - S(p1,p4)*a31*b31 + S(
      p1,p4)*a31*b21 - S(p1,p4)*a31*b11 + S(p1,p4)*a21*c41 - S(p1,p4)*
      a21*c31 + S(p1,p4)*a21*c21 - S(p1,p4)*a21*c11 - S(p1,p4)*a21*b41
       + S(p1,p4)*a21*b31 - S(p1,p4)*a21*b21 + S(p1,p4)*a21*b11 - S(p1,
      p4)*a11*c41 + S(p1,p4)*a11*c31 - S(p1,p4)*a11*c21 + S(p1,p4)*a11*
      c11 + S(p1,p4)*a11*b41 - S(p1,p4)*a11*b31 + S(p1,p4)*a11*b21 - S(
      p1,p4)*a11*b11=0]:
eqs:=[op(eqs), - S(p1,p2) + S(p1,p2)*b31*b41 + S(p1,p2)*b21 + S(p1,p2)*
      b11 - S(p1,p2)*b11*b21 - 2*S(p1,p2)*a31*b31 + S(p1,p2)*a31*b21 - 
      S(p1,p2)*a31*b11 + S(p1,p2)*a31*a41 + S(p1,p2)*a21 + S(p1,p2)*a21
      *b31 - S(p1,p2)*a21*b21 + S(p1,p2)*a11 - S(p1,p2)*a11*b31 - S(p1,
      p2)*a11*b11 - S(p1,p2)*a11*a21 - S(p1,p2)^2*IS(p1,p4)*a31*b31 - 
      S(p1,p4)*a41*b41 + S(p1,p4)*a41*b31 - S(p1,p4)*a41*b21 + S(p1,p4)
      *a41*b11 + S(p1,p4)*a31*b41 - S(p1,p4)*a31*b31 + S(p1,p4)*a31*b21
       - S(p1,p4)*a31*b11 - S(p1,p4)*a21*b41 + S(p1,p4)*a21*b31 - S(p1,
      p4)*a21*b21 + S(p1,p4)*a21*b11 + S(p1,p4)*a11*b41 - S(p1,p4)*a11*
      b31 + S(p1,p4)*a11*b21 - S(p1,p4)*a11*b11=0]:
eqs:=[op(eqs), - c31*c41 + c11*c21=0]:
