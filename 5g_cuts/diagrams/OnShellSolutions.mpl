on_shell_431:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := B(p[1],p[3])/B(p[2],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := B(p[3],p[4])/B(p[3],p[5])*eta(p[4],p[5])/2;
bv[4] := B(p[3],p[5])/B(p[3],p[4])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0,-tau1*s23/s13, 1+tau1]:
b := [0, 1, tau2/s34, tau3/s35]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := s12*s23/s13*tau1*(1+tau1);
mu22 := -tau2*tau3*s45/s34/s35;
mu12 :=
         + TR5 * (
          - 1/2*s13^(-1)
          - 1/2*s13^(-1)*tau3*s35^(-1)
          - tau1*s13^(-1)
          - 1/2*(s34 + s45 - s12)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          )

          - 1/2*(s34 + s45 - s12)*(s23*s34 - s45*s15 - s45*s34 + s12*s15
          - s12*s23)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          + 1/2*( - s23*s34 + s45*s15 + s45*s34 - 2*s45*s23 + 2*s45^2 - s12*s15 + s12*s23 - 2*s12*s45)*s13^(-1)*tau3*s35^(-1)
          - 1/2*(s23*s34 - s45*s15 - s45*s34 + s12*s15 - s12*s23)*s13^(-1)
          - 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          + 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
         ;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_3315L:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

p1:=p[1];
p2:=p[2];
p3:=p[3];
p4:=p[4];
p5:=p[5];

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 :=   A(p1,p5)*B(p5,p2)*A(p2,p3)*IA(p1,p3)*tau1*(1+tau3)
        + A(p3,p2)*B(p2,p4)*IA(p3,p1)*IB(p1,p4)*s15*tau1*tau4
        + A(p4,p2)*B(p2,p3)*IA(p4,p1)*IB(p1,p3)*s15*tau2*tau3
        + B(p1,p5)*A(p5,p2)*B(p2,p3)*IB(p1,p3)*tau2*(1+tau4)
        + s15*(1+tau3+tau4);

return k1,k2,mu11,mu12,mu22;
end:

on_shell_331M2:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,p23f,p23;

p23 := p[2]+p[3]:
p23f := p23 - S(p23)/2./dot(p23,p[1])*p[1]:

av[1] := p[1];
av[2] := p23f;
av[3] := A(p23f,p[4])/A(p[1],p[4])*eta(p[1],p23f)/2;
av[4] := B(p23f,p[4])/B(p[1],p[4])*eta(p23f,p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [s45/(s45-s23), 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s45*s15/s14*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 := s45*s15/(s45-s23)*(1-tau1-tau2)*(1 + tau3 + tau4) - s45*s15/s14*(tau2*tau3 + tau1*tau4);

return k1,k2,mu11,mu12,mu22;
end:

on_shell_430:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := B(p[1],p[3])/B(p[2],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := B(p[3],p[4])/B(p[3],p[5])*eta(p[4],p[5])/2;
bv[4] := B(p[3],p[5])/B(p[3],p[4])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0,-tau1*s23/s13, 1+tau1]:
b := [0, 1, tau2/s34, tau3/s35]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := s12*s23/s13*tau1*(1+tau1);
mu22 := -tau2*tau3*s45/s34/s35;
mu12 := tau4;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_3305L:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 := tau5;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_330M2:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,p23f,p23;

p23 := p[2]+p[3]:
p23f := p23 - S(p23)/2./dot(p23,p[1])*p[1]:

av[1] := p[1];
av[2] := p23f;
av[3] := A(p23f,p[4])/A(p[1],p[4])*eta(p[1],p23f)/2;
av[4] := B(p23f,p[4])/B(p[1],p[4])*eta(p23f,p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [s45/(s45-s23), 0, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s45*s15/s14*tau1*tau2;
mu22 := -s45*s15/s14*tau3*tau4;
mu12 := tau5;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_421:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, 1-tau1]:
b := [tau2, tau3, tau4, -(1+tau2+tau3)*s12/s23-(1+tau2)*s13/s23-(1+tau3+tau4)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*(1-tau1);
mu22 := s12/s13*(s12*(1 + tau2 + tau3)*tau4 + s23*tau4*(1 + tau3 + tau4) + s13*(tau4 + tau2*(tau3 + tau4)));
mu12 := s12/s13*(s12*tau1*(1 + tau2 + tau3) + s13*(tau1 + tau1*tau2 + tau3) + s23*(tau1 + tau1*tau3 - tau4 + 2*tau1*tau4));

return k1,k2,mu11,mu12,mu22;
end:

on_shell_420:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, 1-tau1]:
b := [tau2, tau3, tau4, -(1+tau2+tau3)*s12/s23-(1+tau2)*s13/s23-(1+tau3+tau4)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*(1-tau1);
mu22 := s12/s13*(s12*(1 + tau2 + tau3)*tau4 + s23*tau4*(1 + tau3 + tau4) + s13*(tau4 + tau2*(tau3 + tau4)));
mu12 := tau5;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_3215L:=proc(p, tau1, tau2, tau3, tau4, tau5) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [tau3, tau4, tau5, (tau3-tau4-tau5)-s12/s23*tau4-s45/s23*(1+tau3)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := s12/s13*(s23*tau5+s45)*tau5 + s12*tau3*tau4 - (s23-s45)*s12/s13*tau3*tau5 + (s23+s12)*s12/s13*tau4*tau5;
mu12 := s12*tau4 + s12*s45/s13*tau1 + s12*s23/s13*tau5*(tau1-tau2) - (s23-s45)*s12/s13*tau1*tau3 + (s23+s12)*s12/s13*tau1*tau4;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_3205L:=proc(p, tau1, tau2, tau3, tau4, tau5, tau6) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[1];
bv[2] := p[2];
bv[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
bv[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0, tau1, tau2]:
b := [tau3, tau4, tau5, (tau3-tau4-tau5)-s12/s23*tau4-s45/s23*(1+tau3)]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := -s12*s23/s13*tau1*tau2;
mu22 := s12/s13*(s23*tau5+s45)*tau5 + s12*tau3*tau4 - (s23-s45)*s12/s13*tau3*tau5 + (s23+s12)*s12/s13*tau4*tau5;
mu12 := tau6;

return k1,k2,mu11,mu12,mu22;
end:

on_shell_332:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,bbb;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := A(p[1],p[3])/A(p[2],p[3])*eta(p[2],p[1])/2;

bv[1] := p[3];
bv[2] := p[4];
bv[3] := A(p[4],p[1])/A(p[3],p[1])*eta(p[3],p[4])/2;
bv[4] := A(p[3],p[1])/A(p[4],p[1])*eta(p[4],p[3])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

bbb := -s14*IAB(p[4],p[5],p[3],p[1],p[4])*(
  s15 + s45
  + tau1*AB(p[1],p[5],p[2],p[3],p[1])/s23
  + tau2*AB(p[2],p[5],p[1],p[3],p[2])/s23
  + tau3*AB(p[3],p[5],p[4],p[1],p[3])/s13
);

a := [1, 0, tau1*s13/s23, tau2]:
b := [0, 1, tau3, bbb]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := dot(k1,k1);
mu22 := dot(k2,k2);
mu12 := 2*dot(k1,k2);

return k1,k2,mu11,mu12,mu22;
end:

on_shell_422:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,bbb;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

bbb := -1/s45/s13*( s13*s15 + tau1*AB(p1,p5,p2,p3,p1) + (1-tau1)*AB(p1,p3,p2,p5,p1));
a := [1, 0, tau1, 1-tau1]:
b := [bbb, 0, tau2, tau3]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := dot(k1,k1);
mu22 := dot(k2,k2);
mu12 := 2*dot(k1,k2);

return k1,k2,mu11,mu12,mu22;
end:

on_shell_232:=proc(p, tau1, tau2, tau3, tau4) local k1,k2,av,a,bv,b,
a1,a2,s12,s35,gg,p12f,
p1,p2,p3,p4,p5,mu11,mu12,mu22;

p1:=p[1];
p2:=p[2];
p3:=p[3];
p4:=p[4];
p5:=p[5];

s12:=S(p1,p2);
s35:=S(p3,p5);
gg:=AB(p3,p1+p2,p3);

p12f:=p1+p2-s12/gg*p3;

av[1] := p12f;
av[2] := p3;
av[3] := eta(p12f,p3)/2;
av[4] := eta(p3,p12f)/2;

bv[1] := p4;
bv[2] := p5;
bv[3] := eta(p4,p5)/2;
bv[4] := eta(p5,p4)/2;

a1 := -(s35+tau3*AB(p4,p3,p5)+tau4*AB(p5,p3,p4))/gg;
a2 := s12/gg*(1-a1);

a := [a1, a2, tau1, tau2]:
b := [0, 1, tau3, tau4]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := dot(k1,k1);
mu22 := dot(k2,k2);
mu12 := 2*dot(k1,k2);

return k1,k2,mu11,mu12,mu22;
end:


