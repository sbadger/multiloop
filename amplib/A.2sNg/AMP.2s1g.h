id AMP(ptl(p1?,-1,-1),ptl(p2?,-1,1),ptl(p3?,-1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1),ptl(p2?,-1,1),ptl(p3?,1,0)) =
	  0;

id AMP(ptl(p1?,-1,-1),ptl(p2?,1,1),ptl(p3?,-1,0)) =
	 A(p1,p3)*A(p2,p3)*IA(p1,p2)*I;

id AMP(ptl(p1?,-1,-1),ptl(p2?,1,1),ptl(p3?,1,0)) =
	  - B(p1,p3)*B(p2,p3)*IB(p1,p2)*I;

id AMP(ptl(p1?,1,-1),ptl(p2?,-1,1),ptl(p3?,-1,0)) =
	 A(p1,p3)*A(p2,p3)*IA(p1,p2)*I;

id AMP(ptl(p1?,1,-1),ptl(p2?,-1,1),ptl(p3?,1,0)) =
	  - B(p1,p3)*B(p2,p3)*IB(p1,p2)*I;

id AMP(ptl(p1?,1,-1),ptl(p2?,1,1),ptl(p3?,-1,0)) =
	  0;

id AMP(ptl(p1?,1,-1),ptl(p2?,1,1),ptl(p3?,1,0)) =
	  0;

