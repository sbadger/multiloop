interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);

for i from 1 to 5 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d -> %.30f,\n",i,j,s||i||j):
  od:
od:

TR5 := tr5(p1,p2,p3,p4):
printf("TR5 -> %.30Zf};\n",TR5):

prefactor := IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1):
printf("prefactor =  %.30Zf;\n", prefactor):

on_shell_db311:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j,bbb;

av[1] := p[1];
av[2] := p[2];
av[3] := A(p[2],p[3])/A(p[1],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := A(p[5],p[1])/A(p[4],p[1])*eta(p[4],p[5])/2;
bv[4] := B(p[5],p[1])/B(p[4],p[1])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

bbb := -1/s45/s13*( s13*s15 + tau1*AB(p1,p5,p2,p3,p1) + (1-tau1)*AB(p1,p3,p2,p5,p1));
a := [1, 0, tau1, 1-tau1]:
b := [bbb, 0, tau2, tau3]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := dot(k1,k1);
mu22 := dot(k2,k2);
mu12 := 2*dot(k1,k2);

return k1,k2,mu11,mu12,mu22;
end:

F1 := proc(Ds,mu11,mu12,mu22) local mu33;
mu33:=mu11+mu22+mu12:
return (Ds-2)*(mu11*mu22+mu22*mu33+mu33*mu11) + 4*(mu12^2-4*mu11*mu22);
end:

INT_db311_analytic := proc(k1,k2,mu11,mu12,mu22);
return
  I*s12*s23*s45/TR5*
  IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*
  F1(Ds,mu11,mu12,mu22)*(s15*s34*s45 - 2*BA(p1,p3,p4,p5,p1)*dot(p5,k2));
end:

Ds:=Dsm2+2:
xxdiag5 := 1;
xxdiag6 := -2;
xxdiag7 := -2;
xxdiag8 := -2;

read("tmp/db311.input.mpl"):

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
ds(k1,k2,k3):=0:
od:
od:
od:

for th1 from 0 to 6 do;
for th2 from 0 to 4 do;
for th3 from 0 to 4 do;
printf("%d%d%d\n",th1,th2,th3);
t1 := 1.2*exp(2.*evalf(Pi)*I*th1/7);
t2 := 1.1*exp(2.*evalf(Pi)*I*th2/5);
t3 := 2.3*exp(2.*evalf(Pi)*I*th3/5);

k1,k2,mu11,mu12,mu22 := on_shell_db311([p1,p2,p3,p4,p5],t1,t2,t3):

val := INT_db311(k1,k2,mu11,mu12,mu22);
val2 := INT_db311_analytic(k1,k2,mu11,mu12,mu22);
print(collect(expand(val-val2),Dsm2));
quit;

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
  ds(k1,k2,k3) := expand(ds(k1,k2,k3) + evalf(val/t1^k1/t2^k2/t3^k3/(7.*5.*5.))):
od:
od:
od:

od;
od;
od;

read("../final/db311D.coeffvecs.mpl");

for k1 from 0 to 6 do:
for k2 from 0 to 4 do:
for k3 from 0 to 4 do:
 tmp1 := Re(subs(Dsm2=0,ds(k1,k2,k3))):
 tmp2 := Im(subs(Dsm2=0,ds(k1,k2,k3))):
 tmp3 := Re(coeff(ds(k1,k2,k3),Dsm2)):
 tmp4 := Im(coeff(ds(k1,k2,k3),Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d%d%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k1, k2, k3, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k1,k2,k3,expand(ds(k1,k2,k3)));
od:
od:
od:

for k from 1 to 76 do:
 tmp1 := Re(subs(Dsm2=0,Dvec[k])):
 tmp2 := Im(subs(Dsm2=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Dsm2)):
 tmp4 := Im(coeff(Dvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("d_%d = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
# lprint("d",k,expand(Dvec[k]));
od:

read("../db311D.ReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(65):
Cvec := iMM.Dvec:

for k from 1 to 65 do:
 tmp1 := Re(subs(Dsm2=0,Cvec[k])):
 tmp2 := Im(subs(Dsm2=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Dsm2)):
 tmp4 := Im(coeff(Cvec[k],Dsm2)):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Dsm2*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
# lprint(Clbl[k],expand(Cvec[k]));
od:


