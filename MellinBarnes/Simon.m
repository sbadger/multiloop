(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];


invariants = {p[i_].p[i_]:>0, p[1].p[2]->-s/2, p[2].p[3]->-t/2, p[1].p[3]->(s+t)/2};
repr = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]+p[1],0,a2] PR[l[1]+p[1]+p[2],0,a3]*
	 PR[l[1]-l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]+p[1]+p[2],0,a5] PR[l[2]+p[1]+p[2]+p[3],0,a6]},
	{l[1],l[2]}, Invariants->invariants];


INT[D_,A1_,A2_,A3_,A4_,A5_,A6_,A7_,MBrepr_] := MBrepr /. {a1->A1, a2->A2, a3->A3, a4->A4, a5->A5, a6->A6, a7->A7, d->D};


(*intA = -eps*repr[[1]] /. {a1->1, a2->1, a3->1, a4->1, a5->1, a6->1, a7->1, d->6-2eps}*)
(*intB = -eps^2*repr[[1]] /. {a1->1, a2->1, a3->1, a4->1, a5->1, a6->1, a7->3, d->8-2eps}*)

NUM[x_] := x;

(*intA = MBresolve[eps^2*INT[6-2*eps, 1, 1, 1, 1, 1, 1, 1, repr[[1]]],eps] + MBresolve[eps*(1-eps)*INT[8-2*eps, 1, 1, 1, 1, 1, 1, 3, repr[[1]]],eps]*)

db220 = repr[[1]];

ints={MBresolve[NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,1,1,3,1,db220]*eps,eps],
      MBresolve[2*NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,1,2,2,1,db220]*eps,eps],
      MBresolve[NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,1,3,1,1,db220]*eps,eps],
      MBresolve[2*NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,2,1,2,1,db220]*eps,eps],
      MBresolve[2*NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,2,2,1,1,db220]*eps,eps],
      MBresolve[NUM[ - 1 + eps]*INT[8 - 2*eps,1,1,1,3,1,1,1,db220]*eps,eps],
      MBresolve[INT[6 - 2*eps,1,1,1,1,1,1,1,db220]*eps^2,eps]};

result = 0;
Do[
numint = MBexpand[ints[[k]],-2*Exp[2*eps*EulerGamma],{eps,0,0}];
result = result+MBintegrate[numint, {s->-3, t->-4}, FixedContours->True],
{k,0,Length[ints]}]

result

xx = t/s;
check = 1/s/6*(1 - 2*eps*Log[-s])*(-1/eps^2 + 1/eps*( 1/2*xx*(Log[xx]^2+Pi^2)/(1+xx)^2 + xx*Log[xx]/(1+xx) - 2./3. ) );
Series[check,{eps,0,-1} ]/. {s->-3., t->-4.}

Exit[]

SetVerbosity[1]
MBDimension /@ int2
int3 = Process[int2,{4}];
int4 = Process[int3,{3}];
int5 = Process[int4,{2}];

?MBintegrate

MBintegrate[int2A, {s->1, t->2}, FixedContours->True]
MBintegrate[int2B, {s->1, t->2}, FixedContours->True]
MBexpand[int2,1,{eps,0,-4}] // MBmerge
