* vim: set foldmethod=marker:
#-
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM,Props;
auto CF TMP;
auto F oTMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb,a,b,c,p,x,P,h;
S mS;
S k1f,k2f,k3f,k4f,k1,k2,k3;
S mu;
S l1,...,l20;

set ls:l1,...,l20;
set ps:p1,...,p20,k1f,k2f,k3f,k4f;
set mps:P1,...,P20;

#include- ../../lib/factorise.h

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}
*{{{ lprod
#procedure lprod
id AB(p1?,mu,p2?)*AB(p3?,mu,p4?) = -2*A(p1,p3)*B(p2,p4);
id AB(p1?,mu,p2?)*MOM(p3?,mu) = AB(p1,p3,p2);
id MOM(p1?,mu)*MOM(p1?,mu) = S(p1);
id MOM(p1?,mu)*MOM(p2?,mu) = dot(p1,p2);
id S(p1?ps) = 0;
id dot(p1?ps,p2?ps) = S(p1,p2)/2;
id dot(p1?ps,p2?mps) = AB(p1,p2,p1)/2;
id dot(p1?mps,p2?ps) = AB(p2,p1,p2)/2;
id S(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1);
#endprocedure
*}}}
*{{{ simplify
#procedure simplify

splitarg NUM;
#call sortNUM

#call compress(1);
#call orderMOM

#call breakstrings
#call cancel
#call sortNUM
argument NUM;
#call breakstrings
#call cancel
endargument;

#call compress(1);
#call breakstrings
#call cancel

id AB(p1?,p2?ps,p3?) = A(p1,p2)*B(p2,p3);
id IAB(p1?,p2?ps,p3?) = IA(p1,p2)*IB(p2,p3);

id A(p3,p1)*B(p1,p4)*IB(p2,p4) = -A(p3,p2);
id B(p3,p1)*A(p1,p4)*IA(p2,p4) = -B(p3,p2);
id A(p3,p2)*B(p2,p4)*IB(p1,p4) = -A(p3,p1);
id B(p3,p2)*A(p2,p4)*IA(p1,p4) = -B(p3,p1);

id B(p2,p4)*IB(p1,p4) = -A(p3,p1)*IA(p3,p2);
#call cancel

*** momentum conservation : needs fixing... ***
id A(p1?,p4)*B(p4,p3?) = AB(p1,p4,p3);
id AB(px?,p4,py?) = -AB(px,MOM(p1,p2,p3),py);

#call compress(1);
#call breakstrings
#call cancel

id A(p1?,p2?)*IA(p1?,p2?) = 1;
id B(p1?,p2?)*IB(p1?,p2?) = 1;
id S(p1?,p2?)*IS(p1?,p2?) = 1;

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id IA(p1?,p2?)*IB(p2?,p1?) = IS(p1,p2);

#call orderS
id AB(p1,MOM(p2,p3),p1) = -S(p1,p4);
id AB(p2,MOM(p1,p3),p2) = -S(p1,p3);
id AB(p3,MOM(p1,p2),p3) = -S(p1,p2);
id S(p3,p4) = S(p1,p2);
id S(p2,p4) = S(p1,p3);
id S(p2,p3) = S(p1,p4);
id IS(p2,p3) = IS(p1,p4);

#endprocedure
*}}}
*{{{ schouten2
#procedure schouten2

argument NUM;
id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
id AB(p1?,p2?,p3?)*A(p3?,p4?) = AA(p1,p2,p3,p4);
id B(p1?,p2?)*AB(p2?,p3?,p4?) = BB(p1,p2,p3,p4);
endargument;

#call SortCollect

splitarg NUM;

id NUM(A(p1?,p2?)*A(p3?,p4?),-A(p1?,p4?)*A(p3?,p2?)) = A(p1,p3)*A(p2,p4);
id NUM( - A(p1?,p2?)*A(p3?,p4?),A(p1?,p4?)*A(p3?,p2?)) = -A(p1,p3)*A(p2,p4);

repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);

#endprocedure
*}}}
*{{{ organize
#procedure organize
id NUM(x?)*INV(x?) = 1;
#call lprod
#call cancel
#call simplify

B INV;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id NUM(x?)*INV(x?) = 1;

B NUM;
.sort
collect TMP;
id TMP(x?)=1;
id NUM(x?) = x;
#call simplify

#endprocedure
*}}}
*{{{ OSprops
#procedure OSprops(nprops,p1,p2,p3,p4,p5,p6)

#do i=1,'nprops'
id l'i'*Props(p?,?x) = TMP(p)*Props(?x);
al Props(p?,?x) = Props(?x);
#enddo
id Props=1;
splitarg TMP;
repeat id TMP(p1?,p2?,?x) = TMP(p1)+TMP(p2,?x);
normalize TMP;
id TMP(p?) = MOM(p,mu);
id MOM(k1,mu) =
    a1*MOM('p1',mu)+a2*MOM('p2',mu)+a3*AB('p1',mu,'p2')/2+a4*AB('p2',mu,'p1')/2;
id MOM(k2,mu) =
    b1*MOM('p3',mu)+b2*MOM('p4',mu)+b3*AB('p3',mu,'p4')/2+b4*AB('p4',mu,'p3')/2;
id MOM(k3,mu) =
    c1*MOM('p5',mu)+c2*MOM('p6',mu)+c3*AB('p5',mu,'p6')/2+c4*AB('p6',mu,'p5')/2;

#endprocedure
*}}}

off stats;

.global

#define prop "10"

#define topo "boxboxbox"
*{{{ boxboxbox
#do z=1,'prop'
L Zero'z' = l'z'^2;
#enddo
id l1?ls = l1*
  Props( k1+p1 , k1 , k1-p2 , k2 ,  k3+p3 , k3 , k3-p4 , k2+p1+p2 , k2-k1+p2 , k2-k3-p3 );

#call OSprops('prop',p1,p2,p1,p2,p3,p4);

id a3= a3*B(p3,p1)*IB(p3,p2);
id a4= a4*B(p3,p2)*IB(p3,p1);

id b3= b3*B(p3,p1)*IB(p3,p2);
id b4= b4*B(p3,p2)*IB(p3,p1);

id c3= c3*B(p1,p3)*IB(p1,p4);
id c4= c4*B(p1,p4)*IB(p1,p3);

#write <final/'topo'.OSprops.h> "id l1=k1+p1;"
#write <final/'topo'.OSprops.h> "id l2=k1;"
#write <final/'topo'.OSprops.h> "id l3=k1-p2;"
#write <final/'topo'.OSprops.h> "id l4=k2;"
#write <final/'topo'.OSprops.h> "id l5=k3+p3;"
#write <final/'topo'.OSprops.h> "id l6=k3;"
#write <final/'topo'.OSprops.h> "id l7=k3-p4;"
#write <final/'topo'.OSprops.h> "id l8=k2+p1+p2;"
#write <final/'topo'.OSprops.h> "id l9=k2-k1+p2;"
#write <final/'topo'.OSprops.h> "id l10=k2-k3-p3;"
#write <final/'topo'.OSprops.h> ".sort"
#write <final/'topo'.OSprops.h> "L av1=p1;"
#write <final/'topo'.OSprops.h> "L av2=p2;"
#write <final/'topo'.OSprops.h> "L av3=B(p3,p1)*IB(p3,p2)*eta(p1,p2)/2;"
#write <final/'topo'.OSprops.h> "L av4=B(p3,p2)*IB(p3,p1)*eta(p2,p1)/2;"
#write <final/'topo'.OSprops.h> "L bv1=p1;"
#write <final/'topo'.OSprops.h> "L bv2=p2;"
#write <final/'topo'.OSprops.h> "L bv3=B(p3,p1)*IB(p3,p2)*eta(p1,p2)/2;"
#write <final/'topo'.OSprops.h> "L bv4=B(p3,p2)*IB(p3,p1)*eta(p2,p1)/2;"
#write <final/'topo'.OSprops.h> "L cv1=p3;"
#write <final/'topo'.OSprops.h> "L cv2=p4;"
#write <final/'topo'.OSprops.h> "L cv3=B(p1,p3)*IB(p1,p4)*eta(p3,p4)/2;"
#write <final/'topo'.OSprops.h> "L cv4=B(p1,p4)*IB(p1,p3)*eta(p4,p3)/2;"

#call lprod
#call cancel
#call organize
id S(p1,p3) = -S(p1,p4)-S(p1,p2);
id A(p2,p3)*B(p1,p2)*B(p3,p4)*IB(p1,p4) = -S(p1,p2);
#call cancel
#call organize

id S(p1,p2) = 3;
id S(p1,p4) = 11;
id IS(p1,p4) = 1/11;

.sort
format 150;
#write <'topo'.geneqs.m2> "R=QQ[a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4]"
#write <'topo'.geneqs.m2> "I=ideal("
#do z=1,{'prop'-1}
#write <'topo'.geneqs.m2> "%E," Zero'z'
#enddo
#write <'topo'.geneqs.m2> "%E)" Zero'prop'
#write <'topo'.geneqs.m2> "soleq = primaryDecomposition(I)"
#write <'topo'.geneqs.m2> "\"'topo'.eqs.out\"<<\"nsol:=\"<<#soleq<<\":\"<<endl"
#write <'topo'.geneqs.m2> "\"'topo'.eqs.out\"<<\"eqs:=Array[0..\"<<#soleq-1<<\"]:\"<<endl"
#write <'topo'.geneqs.m2> "for i from 0 to #soleq-1 do \"'topo'.eqs.out\"<<i<<toString(soleq#i)<<endl"

format 150;
print+s;
.store
*}}}

#define topo "tcourt"
*{{{ tcourt
#do z=1,'prop'
L Zero'z' = l'z'^2;
#enddo
id l1?ls = l1*
  Props( k1+p1 , k1 , k1-p2 , k2-p3 ,  k2 , k2-k3-p3 , k2-k3-p3-p4 , k2-k3-k1+p2 , k2-k1+p2 , k3 );

#call OSprops('prop',p1,p2,p3,p4,p1,p2);

id a3= a3*B(p3,p1)*IB(p3,p2);
id a4= a4*B(p3,p2)*IB(p3,p1);

id b3= b3*B(p1,p3)*IB(p1,p4);
id b4= b4*B(p1,p4)*IB(p1,p3);

id c3= c3*B(p3,p1)*IB(p3,p2);
id c4= c4*B(p3,p2)*IB(p3,p1);

#write <final/'topo'.OSprops.h> "id l1=k1+p1;"
#write <final/'topo'.OSprops.h> "id l2=k1;"
#write <final/'topo'.OSprops.h> "id l3=k1-p2;"
#write <final/'topo'.OSprops.h> "id l4=k2-p3;"
#write <final/'topo'.OSprops.h> "id l5=k2;"
#write <final/'topo'.OSprops.h> "id l6=k2-k3-p3;"
#write <final/'topo'.OSprops.h> "id l7=k2-k3-p3-p4;"
#write <final/'topo'.OSprops.h> "id l8=k2-k3-k1+p2;"
#write <final/'topo'.OSprops.h> "id l9=k2-k1+p2;"
#write <final/'topo'.OSprops.h> "id l10=k3;"
#write <final/'topo'.OSprops.h> ".sort"
#write <final/'topo'.OSprops.h> "L av1=p1;"
#write <final/'topo'.OSprops.h> "L av2=p2;"
#write <final/'topo'.OSprops.h> "L av3=B(p3,p1)*IB(p3,p2)*eta(p1,p2)/2;"
#write <final/'topo'.OSprops.h> "L av4=B(p3,p2)*IB(p3,p1)*eta(p2,p1)/2;"
#write <final/'topo'.OSprops.h> "L bv1=p3;"
#write <final/'topo'.OSprops.h> "L bv2=p4;"
#write <final/'topo'.OSprops.h> "L bv3=B(p1,p3)*IB(p1,p4)*eta(p3,p4)/2;"
#write <final/'topo'.OSprops.h> "L bv4=B(p1,p4)*IB(p1,p3)*eta(p4,p3)/2;"
#write <final/'topo'.OSprops.h> "L cv1=p1;"
#write <final/'topo'.OSprops.h> "L cv2=p2;"
#write <final/'topo'.OSprops.h> "L cv3=B(p3,p1)*IB(p3,p2)*eta(p1,p2)/2;"
#write <final/'topo'.OSprops.h> "L cv4=B(p3,p2)*IB(p3,p1)*eta(p2,p1)/2;"



#call lprod
#call cancel
#call organize
id S(p1,p3) = -S(p1,p4)-S(p1,p2);
id A(p2,p3)*B(p1,p2)*B(p3,p4)*IB(p1,p4) = -S(p1,p2);
#call cancel
#call organize

id S(p1,p2) = 3;
id S(p1,p4) = 11;
id IS(p1,p4) = 1/11;

.sort
format 150;
#write <'topo'.geneqs.m2> "R=QQ[a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4]"
#write <'topo'.geneqs.m2> "I=ideal("
#do z=1,{'prop'-1}
#write <'topo'.geneqs.m2> "%E," Zero'z'
#enddo
#write <'topo'.geneqs.m2> "%E)" Zero'prop'
#write <'topo'.geneqs.m2> "soleq = primaryDecomposition(I)"
#write <'topo'.geneqs.m2> "#soleq"
#write <'topo'.geneqs.m2> "\"'topo'.eqs.out\"<<\"nsol:=\"<<#soleq<<\":\"<<endl"
#write <'topo'.geneqs.m2> "\"'topo'.eqs.out\"<<\"eqs:=Array[0..\"<<#soleq-1<<\"]:\"<<endl;"
#write <'topo'.geneqs.m2> "for i from 0 to #soleq-1 do \"'topo'.eqs.out\"<<i<<toString(soleq#i)<<endl"

format 150;
print+s;
.store
*}}}


.end
