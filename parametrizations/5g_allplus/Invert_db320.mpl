interface(quiet=true):
interface(displayprecision=10):
Digits:=50:

with(LinearAlgebra):
with(spinors):
genmoms([0,0,0,0,0]);
clearmoms(5);
genmoms([0,0,0,0,0]);
clearmoms(5);
genmoms([0,0,0,0,0]);

for i from 1 to 4 do:
printf("p%d = <%.16e, %.16e, %.16e, %.16e>;\n",i,p||i[1],p||i[2],p||i[3],p||i[4]);
od:

for i from 1 to 4 do:
  for j from i+1 to 5 do:
    s||i||j := S(p||i,p||j);
    printf("s%d%d := %.16e:\n",i,j,s||i||j):
  od:
od:

printf("prod <i, i+1> = %.16Ze;\n",A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1));
printf("prod [i, i+1] = %.16Ze;\n",B(p1,p2)*B(p2,p3)*B(p3,p4)*B(p4,p5)*B(p5,p1));
printf("prod S(i, i+1) = %.16Ze;\n\n",S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1));

TR5 := tr5(p1,p2,p3,p4):

on_shell_db320:=proc(p, tau1, tau2, tau3) local k1,k2,av,a,bv,b,
s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5,mu11,mu12,mu22,i,j;

av[1] := p[1];
av[2] := p[2];
av[3] := B(p[1],p[3])/B(p[2],p[3])*eta(p[1],p[2])/2;
av[4] := B(p[2],p[3])/B(p[1],p[3])*eta(p[2],p[1])/2;

bv[1] := p[4];
bv[2] := p[5];
bv[3] := B(p[3],p[4])/B(p[3],p[5])*eta(p[4],p[5])/2;
bv[4] := B(p[3],p[5])/B(p[3],p[4])*eta(p[5],p[4])/2;

s12 := S(p[1],p[2]);
s13 := S(p[1],p[3]);
s14 := S(p[1],p[4]);
s15 := S(p[1],p[5]);
s23 := S(p[2],p[3]);
s24 := S(p[2],p[4]);
s25 := S(p[2],p[5]);
s34 := S(p[3],p[4]);
s35 := S(p[3],p[5]);
s45 := S(p[4],p[5]);

TR5:=tr5(p[1],p[2],p[3],p[4]);

a := [1, 0,-tau1*s23/s13, 1+tau1]:
b := [0, 1, tau2/s34, tau3/s35]:

k1 := a[1]*av[1]+a[2]*av[2]+a[3]*av[3]+a[4]*av[4];
k2 := b[1]*bv[1]+b[2]*bv[2]+b[3]*bv[3]+b[4]*bv[4];

mu11 := s12*s23/s13*tau1*(1+tau1);
mu22 := -tau2*tau3*s45/s34/s35;
mu12 :=
         + TR5 * (
          - 1/2*s13^(-1)
          - 1/2*s13^(-1)*tau3*s35^(-1)
          - tau1*s13^(-1)
          - 1/2*(s34 + s45 - s12)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          - 1/2*(2*s34 + s45 - s12)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          )

          - 1/2*(s34 + s45 - s12)*(s23*s34 - s45*s15 - s45*s34 + s12*s15
          - s12*s23)*s13^(-1)*tau2*s34^(-1)*s35^(-1)
          + 1/2*( - s23*s34 + s45*s15 + s45*s34 - 2*s45*s23 + 2*s45^2 - s12*s15 + s12*s23 - 2*s12*s45)*s13^(-1)*tau3*s35^(-1)
          - 1/2*(s23*s34 - s45*s15 - s45*s34 + s12*s15 - s12*s23)*s13^(-1)
          - 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*s34^(-1)*tau3*s35^(-1)
          + 1/2*(s45*s23*s34 + s45^2*s15 - s45^2*s34 + s12*s23*s34 - 2*s12*s45*s15 + s12*s45*s34 + s12*s45*s23 + s12^2*s15 - s12^2*s23)*tau1*s13^(-1)*tau2*s34^(-1)*s35^(-1)
         ;

return k1,k2,mu11,mu12,mu22;
end:

read("../final/db320D.coeffvecs.mpl"):
read("dcoeffs.mpl"):
Dvec:=Vector(79);

for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,ddd||k)):
 tmp2 := Im(subs(Ds=0,ddd||k)):
 tmp3 := Re(coeff(ddd||k,Ds)):
 tmp4 := Im(coeff(ddd||k,Ds)):
 printf("d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
 Dvec[k] := ddd||k;
od:

for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,Dvec[k])):
 tmp2 := Im(subs(Ds=0,Dvec[k])):
 tmp3 := Re(coeff(Dvec[k],Ds)):
 tmp4 := Im(coeff(Dvec[k],Ds)):
 printf("d_%d = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, tmp1, tmp2, tmp3, tmp4);
od:

read("../db320D.NumericalReduceM.log"):
MM:=ReduceMatrix:
iMM:=MatrixInverse(MM):

Cvec := Vector(79):
Cvec := iMM.Dvec:

for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,Cvec[k])):
 tmp2 := Im(subs(Ds=0,Cvec[k])):
 tmp3 := Re(coeff(Cvec[k],Ds)):
 tmp4 := Im(coeff(Cvec[k],Ds)):
 all := 0:
 if abs(tmp1)<1e-20 then; tmp1:=0: all:=all+1: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: all:=all+1: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: all:=all+1: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: all:=all+1: fi:
 if all=4 then: Cvec[k]:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
od:

PFppppp := -I*IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1)*(1.);

for k from 1 to 79 do:
 tmp1 := Re(subs(Ds=0,Cvec[k])/PFppppp):
 tmp2 := Im(subs(Ds=0,Cvec[k])/PFppppp):
 tmp3 := Re(coeff(Cvec[k],Ds)/PFppppp):
 tmp4 := Im(coeff(Cvec[k],Ds)/PFppppp):
 if abs(tmp1)<1e-20 then; tmp1:=0: fi:
 if abs(tmp2)<1e-20 then; tmp2:=0: fi:
 if abs(tmp3)<1e-20 then; tmp3:=0: fi:
 if abs(tmp4)<1e-20 then; tmp4:=0: fi:
 printf("%d %A = %.16e + (%.16e)*I + Ds*(%.16e + (%.16e)*I);\n", k, Clbl[k], tmp1, tmp2, tmp3, tmp4);
od:


mhv:=1:

delta:=proc(p1,p2,p3,p4,p5,a,b,c) local s12,s23,s34,s45,s51;
s12:=S(p1,p2);
s23:=S(p2,p3);
s34:=S(p3,p4);
s45:=S(p4,p5);
s51:=S(p5,p1);
return s12*s51+a*s12*s23+b*s23*s34-s51*s45+c*s34*s45;
end:

G4:=proc(p1,p2,p3,p4) local s12,s13,s14,s23,s24,s34;
s12:=S(p1,p2):
s13:=S(p1,p3):
s14:=S(p1,p4):
s23:=S(p2,p3):
s24:=S(p2,p4):
s34:=S(p3,p4):
return s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2;
end:

R:=proc(p1,p2,p3,p4,p5) local e1234;
global mhv;
e1234:=(-AB(p1,p2,p3,p4,p1)+AB(p1,p4,p3,p2,p1));
return mhv*e1234*S(p1,p2)*S(p2,p3)*S(p3,p4)*S(p4,p5)*S(p5,p1)/G4(p1,p2,p3,p4);
end:

BDKa:=proc(p1,p2,p3,p4,p5);
return ( S(p1,p2)^2*S(p2,p3) - R(p1,p2,p3,p4,p5)*delta(p1,p2,p3,p4,p5,-1,1,-1)*S(p1,p2)*IS(p3,p4)*IS(p4,p5)*IS(p5,p1) )/2;
end:
BDKc:=proc(p1,p2,p3,p4,p5);
return ( S(p1,p2)*S(p3,p4)*S(p4,p5) + R(p1,p2,p3,p4,p5)*delta(p1,p2,p3,p4,p5,1,-1,1)*IS(p2,p3)*IS(p5,p1) )/2;
end:
BDKd:=proc(p1,p2,p3,p4,p5);
return 2*R(p1,p2,p3,p4,p5)/2;
end:
BDKe:=proc(p1,p2,p3,p4,p5);
return -2*S(p1,p2)*R(p1,p2,p3,p4,p5)/2;
end:

# re-construct full integrand
Pow := (x,y) -> x^y:

cc(0,0,0,0,0,0):= Cvec[1]:
cc(0,0,0,0,0,1):= Cvec[2]:
cc(0,0,0,0,0,2):= Cvec[3]:
cc(0,0,0,0,1,0):= Cvec[4]:
cc(0,0,0,0,1,1):= Cvec[5]:
cc(0,0,0,0,2,0):= Cvec[6]:
cc(0,0,0,0,2,1):= Cvec[7]:
cc(0,0,0,0,3,0):= Cvec[8]:
cc(0,0,0,1,0,0):= Cvec[9]:
cc(0,0,0,1,0,1):=Cvec[10]:
cc(0,0,0,1,0,2):=Cvec[11]:
cc(0,0,0,1,1,0):=Cvec[12]:
cc(0,0,0,1,1,1):=Cvec[13]:
cc(0,0,0,1,2,0):=Cvec[14]:
cc(0,0,0,2,0,0):=Cvec[15]:
cc(0,0,0,2,0,1):=Cvec[16]:
cc(0,0,0,2,1,0):=Cvec[17]:
cc(0,0,0,3,0,0):=Cvec[18]:
cc(0,0,1,0,0,0):=Cvec[19]:
cc(0,0,1,0,0,1):=Cvec[20]:
cc(0,0,1,0,1,0):=Cvec[21]:
cc(0,0,1,0,1,1):=Cvec[22]:
cc(0,0,1,0,2,0):=Cvec[23]:
cc(0,0,1,0,3,0):=Cvec[24]:
cc(0,0,1,1,0,0):=Cvec[25]:
cc(0,0,1,1,0,1):=Cvec[26]:
cc(0,0,1,1,1,0):=Cvec[27]:
cc(0,0,1,1,1,1):=Cvec[28]:
cc(0,0,1,1,2,0):=Cvec[29]:
cc(0,0,1,2,0,0):=Cvec[30]:
cc(0,0,1,2,0,1):=Cvec[31]:
cc(0,0,1,2,1,0):=Cvec[32]:
cc(0,0,1,3,0,0):=Cvec[33]:
cc(0,0,2,0,0,1):=Cvec[34]:
cc(0,0,2,0,1,0):=Cvec[35]:
cc(0,0,2,0,2,0):=Cvec[36]:
cc(0,0,2,1,0,0):=Cvec[37]:
cc(0,0,3,0,1,0):=Cvec[38]:
cc(0,0,3,1,0,0):=Cvec[39]:
cc(0,0,4,1,0,0):=Cvec[40]:
cc(0,1,0,0,0,0):=Cvec[41]:
cc(0,1,0,0,0,1):=Cvec[42]:
cc(0,1,0,0,1,0):=Cvec[43]:
cc(0,1,0,0,1,1):=Cvec[44]:
cc(0,1,0,0,2,0):=Cvec[45]:
cc(0,1,0,0,3,0):=Cvec[46]:
cc(0,1,0,1,0,0):=Cvec[47]:
cc(0,1,0,1,0,1):=Cvec[48]:
cc(0,1,0,1,1,0):=Cvec[49]:
cc(0,1,0,1,1,1):=Cvec[50]:
cc(0,1,0,1,2,0):=Cvec[51]:
cc(0,1,0,2,0,0):=Cvec[52]:
cc(0,1,0,2,0,1):=Cvec[53]:
cc(0,1,0,2,1,0):=Cvec[54]:
cc(0,1,0,3,0,0):=Cvec[55]:
cc(0,1,1,0,0,1):=Cvec[56]:
cc(0,1,1,0,1,0):=Cvec[57]:
cc(0,1,1,0,2,0):=Cvec[58]:
cc(0,1,1,1,0,0):=Cvec[59]:
cc(0,1,2,0,1,0):=Cvec[60]:
cc(0,1,2,1,0,0):=Cvec[61]:
cc(0,1,3,1,0,0):=Cvec[62]:
cc(1,0,0,0,0,0):=Cvec[63]:
cc(1,0,0,0,0,1):=Cvec[64]:
cc(1,0,0,0,0,2):=Cvec[65]:
cc(1,0,0,0,1,0):=Cvec[66]:
cc(1,0,0,0,1,1):=Cvec[67]:
cc(1,0,0,0,2,0):=Cvec[68]:
cc(1,0,0,0,2,1):=Cvec[69]:
cc(1,0,0,0,3,0):=Cvec[70]:
cc(1,0,0,1,0,0):=Cvec[71]:
cc(1,0,0,1,0,1):=Cvec[72]:
cc(1,0,0,1,0,2):=Cvec[73]:
cc(1,0,0,1,1,0):=Cvec[74]:
cc(1,0,0,1,1,1):=Cvec[75]:
cc(1,0,0,1,2,0):=Cvec[76]:
cc(1,0,0,2,0,0):=Cvec[77]:
cc(1,0,0,2,0,1):=Cvec[78]:
cc(1,0,0,2,1,0):=Cvec[79]:

Delta8_numerical:=simplify(
cc(0,0,0,0,0,0) + mu22*cc(0,0,0,0,0,1) + Pow(mu22,2)*cc(0,0,0,0,0,2) +
  mu12*cc(0,0,0,0,1,0) + mu12*mu22*cc(0,0,0,0,1,1) +
  Pow(mu12,2)*cc(0,0,0,0,2,0) + Pow(mu12,2)*mu22*cc(0,0,0,0,2,1) +
  Pow(mu12,3)*cc(0,0,0,0,3,0) + mu11*cc(0,0,0,1,0,0) +
  mu11*mu22*cc(0,0,0,1,0,1) + mu11*Pow(mu22,2)*cc(0,0,0,1,0,2) +
  mu11*mu12*cc(0,0,0,1,1,0) + mu11*mu12*mu22*cc(0,0,0,1,1,1) +
  mu11*Pow(mu12,2)*cc(0,0,0,1,2,0) + Pow(mu11,2)*cc(0,0,0,2,0,0) +
  Pow(mu11,2)*mu22*cc(0,0,0,2,0,1) + Pow(mu11,2)*mu12*cc(0,0,0,2,1,0) +
  Pow(mu11,3)*cc(0,0,0,3,0,0) + x21*cc(0,0,1,0,0,0) +
  mu22*x21*cc(0,0,1,0,0,1) + mu12*x21*cc(0,0,1,0,1,0) +
  mu12*mu22*x21*cc(0,0,1,0,1,1) + Pow(mu12,2)*x21*cc(0,0,1,0,2,0) +
  Pow(mu12,3)*x21*cc(0,0,1,0,3,0) + mu11*x21*cc(0,0,1,1,0,0) +
  mu11*mu22*x21*cc(0,0,1,1,0,1) + mu11*mu12*x21*cc(0,0,1,1,1,0) +
  mu11*mu12*mu22*x21*cc(0,0,1,1,1,1) +
  mu11*Pow(mu12,2)*x21*cc(0,0,1,1,2,0) +
  Pow(mu11,2)*x21*cc(0,0,1,2,0,0) +
  Pow(mu11,2)*mu22*x21*cc(0,0,1,2,0,1) +
  Pow(mu11,2)*mu12*x21*cc(0,0,1,2,1,0) +
  Pow(mu11,3)*x21*cc(0,0,1,3,0,0) + mu22*Pow(x21,2)*cc(0,0,2,0,0,1) +
  mu12*Pow(x21,2)*cc(0,0,2,0,1,0) +
  Pow(mu12,2)*Pow(x21,2)*cc(0,0,2,0,2,0) +
  mu11*Pow(x21,2)*cc(0,0,2,1,0,0) + mu12*Pow(x21,3)*cc(0,0,3,0,1,0) +
  mu11*Pow(x21,3)*cc(0,0,3,1,0,0) + mu11*Pow(x21,4)*cc(0,0,4,1,0,0) +
  x22*cc(0,1,0,0,0,0) + mu22*x22*cc(0,1,0,0,0,1) + mu12*x22*cc(0,1,0,0,1,0) +
  mu12*mu22*x22*cc(0,1,0,0,1,1) + Pow(mu12,2)*x22*cc(0,1,0,0,2,0) +
  Pow(mu12,3)*x22*cc(0,1,0,0,3,0) + mu11*x22*cc(0,1,0,1,0,0) +
  mu11*mu22*x22*cc(0,1,0,1,0,1) + mu11*mu12*x22*cc(0,1,0,1,1,0) +
  mu11*mu12*mu22*x22*cc(0,1,0,1,1,1) +
  mu11*Pow(mu12,2)*x22*cc(0,1,0,1,2,0) +
  Pow(mu11,2)*x22*cc(0,1,0,2,0,0) +
  Pow(mu11,2)*mu22*x22*cc(0,1,0,2,0,1) +
  Pow(mu11,2)*mu12*x22*cc(0,1,0,2,1,0) +
  Pow(mu11,3)*x22*cc(0,1,0,3,0,0) + mu22*x21*x22*cc(0,1,1,0,0,1) +
  mu12*x21*x22*cc(0,1,1,0,1,0) + Pow(mu12,2)*x21*x22*cc(0,1,1,0,2,0) +
  mu11*x21*x22*cc(0,1,1,1,0,0) + mu12*Pow(x21,2)*x22*cc(0,1,2,0,1,0) +
  mu11*Pow(x21,2)*x22*cc(0,1,2,1,0,0) +
  mu11*Pow(x21,3)*x22*cc(0,1,3,1,0,0) + x14*cc(1,0,0,0,0,0) +
  mu22*x14*cc(1,0,0,0,0,1) + Pow(mu22,2)*x14*cc(1,0,0,0,0,2) +
  mu12*x14*cc(1,0,0,0,1,0) + mu12*mu22*x14*cc(1,0,0,0,1,1) +
  Pow(mu12,2)*x14*cc(1,0,0,0,2,0) +
  Pow(mu12,2)*mu22*x14*cc(1,0,0,0,2,1) +
  Pow(mu12,3)*x14*cc(1,0,0,0,3,0) + mu11*x14*cc(1,0,0,1,0,0) +
  mu11*mu22*x14*cc(1,0,0,1,0,1) + mu11*Pow(mu22,2)*x14*cc(1,0,0,1,0,2) +
  mu11*mu12*x14*cc(1,0,0,1,1,0) + mu11*mu12*mu22*x14*cc(1,0,0,1,1,1) +
  mu11*Pow(mu12,2)*x14*cc(1,0,0,1,2,0) +
  Pow(mu11,2)*x14*cc(1,0,0,2,0,0) +
  Pow(mu11,2)*mu22*x14*cc(1,0,0,2,0,1) +
  Pow(mu11,2)*mu12*x14*cc(1,0,0,2,1,0)
):

# analytic result from Yang
mu33:=mu11+mu22+mu12:
a1:=1/TR5*s12*s23*s45*(s15*s34*s45):
b0:=-s12*s23*s45:
b1:=-1/TR5*s12*s23*s45*(s12*(s23-s15)-s23*s34+(s15+s34)*s45):

Delta8_analytic:= expand(PFppppp*((Ds-2)*(mu11*mu22+mu11*mu33+mu22*mu33) + 16*(mu12^2/4 - mu11*mu22))*(
  +a1
  +(b0+b1)*x14/2
)):

zero := subs({Ds=99,x14=3,mu11=11,mu12=17,mu22=29},expand(Delta8_analytic-Delta8_numerical));
zero := subs({Ds=99,x14=5,mu11=31,mu12=7,mu22=13},expand(Delta8_analytic-Delta8_numerical));

printf("I8[1]        = %.16Ze\n",-a1);
printf("I8[2(k1.p5)] = %.16Ze\n",-(b0+b1)/2);
printf("check with hep-th/0604074\n");
printf("I8[1]        = %.16Ze\n",BDKe(p5,p4,p3,p2,p1));
printf("I8[2(k1.p5)] = %.16Ze\n",BDKc(p5,p4,p3,p2,p1));


