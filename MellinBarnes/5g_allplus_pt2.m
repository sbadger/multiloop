(* ::Package:: *)

Get["/lscr221/badger/MBTools/MBprepare.m"];
Get["/lscr221/badger/MBTools/MB.m"];
Get["/lscr221/badger/MBTools/MBresolve.m"];
Get["/lscr221/badger/MBTools/barnesroutines.m"];

INT[D_, plist_, MBrepr_] := MBrepr /. Append[Table[ToExpression["a" <> ToString[i]] -> plist[[i]], {i, 1, Length[plist]}], d->D];

(* Set Ds = 4 for FDH scheme *)
Dsm2 = 2;

INTdb320mu[MBrepr_] := {
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,1,3},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,3,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,2,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,3,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,1,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,2,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,3,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,3,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,2,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,3,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,3,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,2,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{3,1,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (- 8*(1 + 2*eps)*INT[6 - 2*eps,{1,1,1,1,1,1,1,1},MBrepr]*eps),
       MBresolve[#,eps] & /@ (+ 3*INT[6 - 2*eps,{1,1,1,1,1,1,1,1},MBrepr]*eps^2*Dsm2)
       };

INTdb320Tmu[MBrepr_] := {
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,1,3,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,3,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,2,2,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,3,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,1,2,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,2,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,3,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,3,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,2,2,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,3,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,1,2,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,2,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,3,1,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,1,2,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,2,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,2,1,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{3,1,1,1,1,1,1,1,tt-1},MBrepr]*eps*Dsm2),
       MBresolve[#,{eps->0,tt->0}] & /@ (- 8*(1 + 2*eps)*INT[6 - 2*eps,{1,1,1,1,1,1,1,1,tt-1},MBrepr]*eps),
       MBresolve[#,{eps->0,tt->0}] & /@ (+ 3*INT[6 - 2*eps,{1,1,1,1,1,1,1,1,tt-1},MBrepr]*eps^2*Dsm2)
       };

INTdb220mu[MBrepr_] := {
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,1,3},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,1,3,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,2,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,1,3,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,2,1,2,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,2,2,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,1,3,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,1,3,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,2,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{1,3,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,1,2,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{2,2,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (+ 2*( - 1 + eps)*INT[8 - 2*eps,{3,1,1,1,1,1,1},MBrepr]*eps*Dsm2),
       MBresolve[#,eps] & /@ (- 8*(1 + 2*eps)*INT[6 - 2*eps,{1,1,1,1,1,1,1},MBrepr]*eps),
       MBresolve[#,eps] & /@ (+ 3*INT[6 - 2*eps,{1,1,1,1,1,1,1},MBrepr]*eps^2*Dsm2)
       };

cGamma1 = 1/(4*Pi)^(0)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
cGamma2 = 1/(4*Pi)^(0)*Gamma[1+2*eps]*Gamma[1-eps]^3/Gamma[1-3*eps];


invariants = {p[i_].p[i_]:>0,
      p[1].p[2] -> -ms12/2, p[1].p[3] -> -(ms45-ms12-ms23)/2, p[1].p[4] -> -(ms23-ms15-ms45)/2, p[1].p[5] -> -ms15/2,
      p[2].p[3] -> -ms23/2, p[2].p[4] -> -(ms15-ms23-ms34)/2, p[2].p[5] -> -(ms34-ms12-ms15)/2,
      p[3].p[4] -> -ms34/2, p[3].p[5] -> -(ms12-ms34-ms45)/2,
      p[4].p[5] -> -ms45/2};


db320[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[1]+l[2],0,a8]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

db320T1[p1_,p2_,p3_,p4_,p5_] := 2*MBprepare[{l[1]*p[p5]},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[1]+l[2],0,a8]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]},
	{l[1],l[2]}, Invariants->invariants];

db320T2[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1]-p[p1],0,a1] PR[l[1]-p[p1]-p[p2],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

db320T[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]-p[p1]-p[p2]-p[p3],0,a4]*
	 PR[l[1]+l[2],0,a8]*
	 PR[l[2],0,a5] PR[l[2]-p[p5],0,a6] PR[l[2]-p[p4]-p[p5],0,a7]*
     PR[l[1]+p[p5],0,a9]},
	{l[1],l[2]}, Invariants->invariants];

db220M1[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1]-p[p2],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

db220M2[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2]-p[p3],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

db2205L[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3]*
	 PR[l[1]+l[2],0,a7]*
	 PR[l[2],0,a4] PR[l[2]-p[p5],0,a5] PR[l[2]-p[p4]-p[p5],0,a6]},
	{l[1],l[2]}, Invariants->invariants];

ord = {{1,2,3,4,5}, {2,3,4,5,1}, {3,4,5,1,2}, {4,5,1,2,3}, {5,1,2,3,4}};
db320F = Table[0,{k,1,5}]
db320TF = Table[0,{k,1,5}]
db320T1F = Table[0,{k,1,5}]
db320T2F = Table[0,{k,1,5}]
db220M1F = Table[0,{k,1,5}]
db220M2F = Table[0,{k,1,5}]
db2205LF = Table[0,{k,1,5}]


Do[
db320F[[k]] = INTdb320mu[Apply[db320,ord[[k]]]];
db320T1F[[k]] = INTdb320mu[Apply[db320T1,ord[[k]]]];
db320T2F[[k]] = INTdb220mu[Apply[db320T2,ord[[k]]]];
db320TF[[k]] = INTdb320Tmu[Apply[db320T,ord[[k]]]];
db220M1F[[k]] = INTdb220mu[Apply[db220M1,ord[[k]]]];
db220M2F[[k]] = INTdb220mu[Apply[db220M2,ord[[k]]]];
db2205LF[[k]] = INTdb220mu[Apply[db2205L,ord[[k]]]];,
{k,1,5}]


MBmerge[MBexpand[db320F[[1]],1/cGamma2,{eps,0,-2}]] /. {ms12->-s12, ms23->-s23, ms34->-s34, ms45->-s45, ms15->-s15}


MBexpand[db320TF[[1]],1,{tt,0,0}];
MBmerge[MBexpand[%,1/cGamma2,{eps,0,-2}]] /. {ms12->-s12, ms23->-s23, ms34->-s34, ms45->-s45, ms15->-s15} // Simplify


kinpt = {
{ms12->1., ms23->3., ms34->11., ms45->19., ms15->31., s12->-1., s23->-3., s34->-11., s45->-19., s15->-31., tr5->-392.975},
{ms12 -> -5.8114718025114551, ms23 -> -111.1780435632823470, ms34 -> 119.0194598298895731, ms45 -> -254.9541143630703645, ms15 -> 53.8854746804948466,
 s12 -> 5.8114718025114551, s23 -> 111.1780435632823470, s34 -> -119.0194598298895731, s45 -> 254.9541143630703645, s15 -> -53.8854746804948466, tr5 -> 2979.0073842318299273*I}
};


maxeps=0
npt=2


Rdb320 = Table[0,{k,1,5}]
Rdb320T1 = Table[0,{k,1,5}]
Rdb320T2 = Table[0,{k,1,5}]
Rdb320T = Table[0,{k,1,5}]
Rdb220M1 = Table[0,{k,1,5}]
Rdb220M2 = Table[0,{k,1,5}]
Rdb2205L = Table[0,{k,1,5}]
SetVerbosity[1];


Options[MBintegrate]


Do[
tmp = MBexpand[db320F[[k]],1/cGamma2,{eps,0,maxeps}];
tmp = Process[tmp,{5,4,3}];
Rdb320[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16, MaxCuhreDim->3, MaxPoints->10000000, MaxRecursion->3000];,
{k,1,5}]





Do[
tmp = MBexpand[db320T1F[[k]],1/cGamma2,{eps,0,maxeps}];
tmp = Process[tmp,{5,4,3}];
Rdb320T1[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]

Do[
tmp = MBexpand[db320T2F[[k]],1/cGamma2,{eps,0,maxeps}];
Rdb320T2[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]

Do[
tmp = MBexpand[db320TF[[k]],1,{tt,0,0}];
tmp = MBexpand[tmp,1/cGamma2,{eps,0,maxeps}];
Rdb320T[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]

Do[
tmp = MBexpand[db220M1F[[k]],1/cGamma2,{eps,0,maxeps}];
Rdb220M1[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]

Do[
tmp = MBexpand[db220M2F[[k]],1/cGamma2,{eps,0,maxeps}];
Rdb220M2[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]

Do[
tmp = MBexpand[db2205LF[[k]],1/cGamma2,{eps,0,maxeps}];
Rdb2205L[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16];,
{k,1,5}]


Rdb320T[[1]]
Rdb320T[[2]]
Rdb320T[[3]]
Rdb320T[[4]]
Rdb320T[[5]]


Rdb320[[1]]
Rdb320[[2]]
Rdb320[[3]]
Rdb320[[4]]
Rdb320[[5]]


Rdb220M1[[1]]
Rdb220M1[[2]]
Rdb220M1[[3]]
Rdb220M1[[4]]
Rdb220M1[[5]]


Rdb220M2[[1]]
Rdb220M2[[2]]
Rdb220M2[[3]]
Rdb220M2[[4]]
Rdb220M2[[5]]


Rdb2205L[[1]]
Rdb2205L[[2]]
Rdb2205L[[3]]
Rdb2205L[[4]]
Rdb2205L[[5]]


s[a_,b_] := Module[{},
If[b>a,ToExpression["s"<>ToString[a]<>ToString[b]],ToExpression["s"<>ToString[b]<>ToString[a]]]
]


box[p1_,p2_,p3_,p4_,p5_] := MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[p1],0,a2] PR[l[1]-p[p1]-p[p2],0,a3] PR[l[1]+p[p4]+p[p5],0,a4]},
	{l[1]}, Invariants->invariants];

pent = MBprepare[{},
	{PR[l[1],0,a1] PR[l[1]-p[1],0,a2] PR[l[1]-p[1]-p[2],0,a3] PR[l[1]-p[1]-p[2]-p[3],0,a4] PR[l[1]+p[5],0,a5]},
	{l[1]}, Invariants->invariants];

pentF = {MBresolve[#,eps] & /@ (eps*(-1+eps)*(-2+eps)*INT[10 - 2*eps,{1,1,1,1,1},pent])};
boxF = Table[0,{k,1,5}]
Do[
  boxF[[k]] = {MBresolve[#,eps] & /@ (eps*(-1+eps)*INT[8 - 2*eps,{1,1,1,1}, Apply[box,ord[[k]]]])};,
{k,1,5}]


SetVerbosity[1];
MBexpand[pentF,1/cGamma1,{eps,0,maxeps+2}];
Process[%,{5,4,3}];
Rpent = MBintegrate[%, kinpt[[npt]], FixedContours->True, PrecisionGoal->16, MaxPoints->10000000, MaxRecursion->5000];
Rpent


Rpent


Rbox = Table[0,{k,1,5}]
Do[
  tmp = MBexpand[boxF[[k]],1/cGamma1,{eps,0,maxeps+2}];
  Rbox[[k]] = MBintegrate[tmp, kinpt[[npt]], FixedContours->True, PrecisionGoal->16, MaxPoints->10000000, MaxRecursion->3000];,
{k,1,5}]


Rbox[[1]]


A52[ord_,p1_,p2_,p3_,p4_,p5_]  := \
       Rdb2205L[[ord,1]] * ( \
          + s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]*tr5^-1 \
          ) + \
       Rdb220M2[[ord,1]] * ( \
          + 1/2*(tr5 - s[p3,p4]*s[p4,p5] + s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p5]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb220M1[[ord,1]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] - s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] + s[p1,p2]*s[p1,p5])*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb320[[ord,1]] * ( \
          - s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Rdb320T[[ord,1]] * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p2]*s[p2,p3]*s[p4,p5]*tr5^-1 \
          );


A51 = -Dsm2/2*(s12*s23*Rbox[[1,1]] + s23*s34*Rbox[[2,1]] + s34*s45*Rbox[[3,1]] + s45*s15*Rbox[[4,1]] + s15*s12*Rbox[[5,1]] + 2*tr5*Rpent[[1]]) /. kinpt[[npt]];
Series[A51,{eps,0,maxeps+2}]
A514d = Series[A51,{eps,0,0}]


IRcheck = Series[-1/eps^2*(Exp[-eps*Log[-s[1,2]]] + Exp[-eps*Log[-s[2,3]]] + Exp[-eps*Log[-s[3,4]]] + Exp[-eps*Log[-s[4,5]]] + Exp[-eps*Log[-s[1,5]]])*A51 /. kinpt[[npt]] ,{eps,0,maxeps}]


A5partial = A52[1,1,2,3,4,5]+A52[2,2,3,4,5,1]+A52[3,3,4,5,1,2]+A52[4,4,5,1,2,3]+A52[5,5,1,2,3,4] /. kinpt[[npt]] // Expand


A5partial - IRcheck


Print["db320(1,2,3,4,5) = ",Rdb320[[1,1]] // InputForm]
Print["db320[(k1+p5)^2](1,2,3,4,5) = ",Rdb320T[[1,1]] // InputForm]
Print["db220M1(1,2,3,4,5) = ",Rdb220M1[[1,1]] // InputForm]
Print["db220M2(1,2,3,4,5) = ",Rdb220M2[[1,1]] // InputForm]
Print["db2205L(1,2,3,4,5) = ",Rdb2205L[[1,1]] // InputForm]


A52test[p1_,p2_,p3_,p4_,p5_]  := \
       Idb2205L * ( \
          + s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]*tr5^-1 \
          ) + \
       Idb220M2 * ( \
          + 1/2*(tr5 - s[p3,p4]*s[p4,p5] + s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p5]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb220M1 * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] - s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] + s[p1,p2]*s[p1,p5])*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb320 * ( \
          - s[p1,p2]*s[p1,p5]*s[p2,p3]*s[p3,p4]*s[p4,p5]^2*tr5^-1 \
          ) + \
       Idb320T * ( \
          + 1/2*(tr5 + s[p3,p4]*s[p4,p5] - s[p2,p3]*s[p3,p4] + s[p1,p5]*s[p4,p5] + s[p1,p2]*s[p2,p3] - s[p1,p2]*s[p1,p5])*s[p1,p2]*s[p2,p3]*s[p4,p5]*tr5^-1 \
          );


Apply[A52test,ord[[1]]] /. kinpt[[1]]
Apply[A52test,ord[[2]]] /. kinpt[[1]]
Apply[A52test,ord[[3]]] /. kinpt[[1]]
Apply[A52test,ord[[4]]] /. kinpt[[1]]
Apply[A52test,ord[[5]]] /. kinpt[[1]]



