Get["/lscr221/badger/gitrepos/mathematicam2/BasisDet-1-02.m"];

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* db210b *)
L=2;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis = {p1, p2, p3};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1 p2 -> s12/2,
   p1 p3 -> (s45 - s12 - s23)/2,
   p1 p5 -> s15/2,
   p2 p3 -> s23/2,
   p2 p5 -> (s34 - s15 - s12)/2,
   p3 p5 -> (s12 - s45 - s34)/2,
   \[Omega]1^2 -> XXX,
   p1*\[Omega]1 -> 0, p2*\[Omega]1 -> 0, p3*\[Omega]1 -> 0

};
numeric = {s12 -> 11, s23 -> 17, s34->7, s45->3, s15->29};
Props = {l1, l1 - p1, l1 - p2, l2, l2 + p1 + p2 + p3, l1 + l2};
RenormalizationCondition = {{{1, 0}, 4}, {{0, 1}, 3}, {{1, 1}, 5}};

GenerateBasis[0]

fout = OpenWrite["basis/db210Db.basis.h"];
WriteString[fout,"id Basisdb210Db = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

