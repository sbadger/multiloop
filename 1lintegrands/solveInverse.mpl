interface(quiet=true):
#interface(rtablesize=infinity):
with(LinearAlgebra):

read("matrix_box.mpl"):
Ieqs:=MatrixInverse(eqs);
csol:=simplify(Ieqs.dvec);

fd:=fopen("box.d2c.mpl",WRITE):
for k from 1 to 2 do:
tmp:=simplify(csol[k]);
fprintf(fd,"print(%A,%A):\n",cs[k],tmp):
od:

fclose(fd):

for s from 1 to 2 do:
     ds[s]:=cat('d',s):
od:

fd:=fopen("box.d2c.h",WRITE):
for k from 1 to 2 do:
tmp:=simplify(csol[k]);
fprintf(fd,"L %A = %A;\n",cs[k],tmp):
od:

fclose(fd):

unassign('ds'):
# solving triangle

read("matrix_tri.mpl"):

eqsP,eqsL,eqsU:=LUDecomposition(eqs);
dtransform:=MatrixInverse(simplify(eqsP).simplify(eqsL));
#dtransform.eqs-eqsU: ## == 0 ##

eqsSq:=simplify(DeleteRow(eqsU,[8..14])):

Ieqs:=MatrixInverse(eqsSq);

dvecT:=dtransform.dvec:

dvecR:=SubVector(dvecT,[1..7]):
csol:=simplify(Ieqs.dvecR);

fd:=fopen("tri.d2c.mpl",WRITE):
for k from 1 to 7 do:
tmp:=simplify(csol[k]);
fprintf(fd,"print(%A,%A):\n",cs[k],tmp):
od:

for k from 8 to 14 do:
fprintf(fd,"print(%A);\n",simplify(dvecT[k])):
od:

fclose(fd):

for s from 1 to 2 do:
  for p from -3 to 3 do:
     ds[s,p]:=cat('d',s,p+3):
  od:
od:

fd:=fopen("tri.d2c.h",WRITE):
for k from 1 to 7 do:
tmp:=simplify(csol[k]);
fprintf(fd,"L %A = %A;\n",cs[k],tmp):
od:

for k from 8 to 14 do:
fprintf(fd,"L zero%d = %A;\n",k-7,simplify(dvecT[k])):
od:

fclose(fd):

unassign('ds'):
