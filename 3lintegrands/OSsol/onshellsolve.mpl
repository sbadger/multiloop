interface(quiet=true):

for topo in ["boxboxbox","tcourt"] do:

read(cat(topo,".eqs.out")):

sol:=Array(1..nsol):
for k from 1 to nsol do:
sol[k]:=solve(eqs[k-1],[a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4]):
od:

printf("%A : on-shell solutions = %d\n",topo,nsol);

FORMout:=fopen(cat(topo,".OSsol.h"),WRITE):
fprintf(FORMout,"#define nsols \"%d\"\n",nsol):
for k from 1 to nsol do:
 fprintf(FORMout,"#if 'sol'=%d\n",k):
 for l from 1 to nops(sol[k][1]) do:
   fprintf(FORMout,"id %A;\n",factor(sol[k][1,l])):
  od:
 fprintf(FORMout,"#endif\n\n"):
od:
fclose(FORMout):

od:

