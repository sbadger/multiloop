#-
CF Prop,IProp,dot(s),D,MOM,Vec;
CF pow,NUM;
auto CF I,TMP;
CF SYM(s),Gamma,IGamma;

auto S k,p,s,mu,x,a,ff;
S d,[d-4],[d-3],[d-2];

off stats;

#procedure toinvariants

id dot(p12,p12) = s12;
id dot(p12,p123) = (s12+s123)/2;
id dot(p123,p123) = s123;

id NUM(x?) = x;
AB s12,s123;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

#endprocedure

#procedure applyIBPs

*#include- FIRE_tribubIBPS.inc
*denominators INV;

#include- reduze2_tribub/reduze2_tribubIBPS.inc

factarg INV;
chainout INV;
splitarg INV;
id INV(x1?) = 1/x1;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);

*#call toinvariants;
*id INV(x?)*NUM(x?) = 1;

#endprocedure

set ps:p1,p2,p3;
.global

G l1 = k1;
G l2 = k2;
G l3 = k1-p123;
G l4 = k1-k2-p12;
G l5 = k1-p12;
G l6 = k2-p12;
G l7 = k2-p123;
.sort
G tribub = Prop(l1)*Prop(l2)*Prop(l3)*Prop(l4);

.store

G IBP1 = D(mu,k1)*tribub*Vec(k1,mu);
G IBP2 = D(mu,k1)*tribub*Vec(k2,mu);
G IBP3 = D(mu,k1)*tribub*Vec(p12,mu);
G IBP4 = D(mu,k1)*tribub*Vec(p123,mu);
G IBP5 = D(mu,k2)*tribub*Vec(k2,mu);
G IBP6 = D(mu,k2)*tribub*Vec(p12,mu);
G IBP7 = D(mu,k2)*tribub*Vec(p123,mu);
G IBP8 = D(mu,k2)*tribub*Vec(k1,mu);

G [p123.d/d(p123)] = D(p123,p123)*tribub;
G [p12.d/d(p12)] = D(p12,p12)*tribub;

repeat;
id,once D(p1?,p2?)*Prop(p?) = D(p1,p2,p)*TMP(p)+D(p1,p2)*TMP(p);
al,once D(mu?,p1?)*Vec(p?,mu?) = D(p,p1)+D(mu,p1)*TMPVec(p,mu);
endrepeat;
id D(k1,k1) = d;
id D(k2,k2) = d;
id TMP(?x) = Prop(?x);
id TMPVec(?x) = Vec(?x);
id D(mu?,?x)*Vec(p1?,mu?) = D(p1,?x);
id D(p1?,p2?) = 0;

splitarg D;
.sort
id D(p1?,p2?,?x,p2?,?y) = -Prop(MOM(?x,p2,?y))*2*dot(p1,MOM(?x,p2,?y));
id D(p1?,p2?,?x,-p2?,?y) = Prop(MOM(?x,-p2,?y))*2*dot(p1,MOM(?x,-p2,?y));
id D(?x) = 0;
.sort

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
normalize dot;
id dot(p1?ps,p1?ps) = 0;

#call toinvariants
B Prop;
print+s;
.sort

id dot(k1,k1) = IProp(k1);
id dot(k2,k2) = IProp(k2);
id dot(k1,k2) = (-IProp(k1-k2-p12)+IProp(k1)+IProp(k2)+s12-2*dot(k1,p12)+2*dot(k2,p12))/2;
id dot(k1,p123) = (-IProp(k1-p123)+IProp(k1)+s123)/2;
id dot(k1,p12) = (-IProp(k1-p12)+IProp(k1)+s12)/2;
id dot(k2,p123) = (-IProp(k2-p123)+IProp(k2)+s123)/2;
id dot(k2,p12) = (-IProp(k2-p12)+IProp(k2)+s12)/2;

repeat id dot(p1?,MOM(p2?,?x)) = dot(p1,p2)+dot(p1,MOM(?x));
id dot(p1?,MOM) = 0;
normalize dot;
id dot(p1?ps,p1?ps) = 0;

argument Prop,IProp;
repeat;
id MOM(p1?,?x) = p1+MOM(?x);
id MOM = 0;
endrepeat;
endargument;

id Prop(p?)*IProp(p?) = 1;

multiply TMP(1,l1)*TMP(2,l2)*TMP(3,l3)*TMP(4,l4)*TMP(5,l5)*TMP(6,l6)*TMP(7,l7);
repeat id Prop(p?)*TMP(xx?,p?) = pow(xx)*TMP(xx,p);
repeat id IProp(p?)*TMP(xx?,p?) = pow(-xx)*TMP(xx,p);
id TMP(?x) = 1;
multiply I7(0,0,0,0,0,0,0);
repeat id pow(1)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1+1,a2,a3,a4,a5,a6,a7);
repeat id pow(2)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2+1,a3,a4,a5,a6,a7);
repeat id pow(3)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3+1,a4,a5,a6,a7);
repeat id pow(4)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4+1,a5,a6,a7);
repeat id pow(5)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5+1,a6,a7);
repeat id pow(6)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5,a6+1,a7);
repeat id pow(7)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5,a6,a7+1);

repeat id pow(-1)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1-1,a2,a3,a4,a5,a6,a7);
repeat id pow(-2)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2-1,a3,a4,a5,a6,a7);
repeat id pow(-3)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3-1,a4,a5,a6,a7);
repeat id pow(-4)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4-1,a5,a6,a7);
repeat id pow(-5)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5-1,a6,a7);
repeat id pow(-6)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5,a6-1,a7);
repeat id pow(-7)*I7(a1?,a2?,a3?,a4?,a5?,a6?,a7?) = I7(a1,a2,a3,a4,a5,a6,a7-1);

print+s;
.sort

#call toinvariants
*.sort
*topolynomial;
*.sort
*#write "%X"
#call applyIBPs

id NUM(x?) = x;
repeat id d*INV(d-4) = 1 + 4*INV(d-4);
repeat id d*INV(d-3) = 1 + 3*INV(d-3);
repeat id d*INV(d-2) = 1 + 2*INV(d-2);
*repeat id INV(d-4)*INV(d-3) = -INV(d-3)+INV(d-4);
*repeat id INV(d-4)*INV(d-2) = -INV(d-2)/2+INV(d-4)/2;
#call toinvariants;
id INV(x?)*NUM(x?) = 1;
id NUM(x?) = x;

*id INV(s123-s12) = 1/sxxx;

*AB s12,s123,sxxx;
*.sort
*collect NUM;
*factarg NUM;
*chainout NUM;
*splitarg NUM;
*id NUM(x1?) = x1;
*repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
*argument NUM;
*id s123 = s12+sxxx;
*endargument;
*factarg NUM;
*chainout NUM;
*splitarg NUM;
*id NUM(x1?) = x1;
*repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
*id NUM(sxxx+s12) = s123;
*id NUM(x?) = x;
*id sxxx = s123-s12;
*id 1/sxxx = INV(s123-s12);

id INV(d-4) = 1/[d-4];
id INV(s123-s12)*s123 = 1+s12*INV(s123-s12);

AB d,[d-4];
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
argument NUM;
id [d-4] = d-4;
endargument;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

id INV(x?)*NUM(x?) = 1;

*id NUM(-10+3*d) = 2+3*[d-4];
id NUM(-4+d) = [d-4];

B I7;
print+s;
.sort
hide;

G [s123*d/d(s123)] = 1/2*[p123.d/d(p123)];
G [s12*d/d(s12)] = 1/2*[p12.d/d(p12)];
.sort

G zero = ( [s123*d/d(s123)] + [s12*d/d(s12)]  - [d-4]*I7(1,1,1,1,0,0,0) );

B I7;
print+s;
.end



*** via eq. (3.1) from hep-ph/9912329 ***
G [d/d(s123)] = 1/2*INV(s123-s13-s23)*( +[p1.d/d(p1)]+[p2.d/d(p2)]-[p3.d/d(p3)] );
G [d/d(s13)] = 1/2/s13*( +[p1.d/d(p1)]-[p2.d/d(p2)]+[p3.d/d(p3)] );
G [d/d(s23)] = 1/2/s12*( -[p1.d/d(p1)]+[p2.d/d(p2)]+[p3.d/d(p3)] );

B INV,I4,s13,s23,s123,NUM;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);

format 135;
B I4,INV;
print;
.store

G [I4(1,1,1,1)(s12=0)] = ( +[p1.d/d(p1)]+[p2.d/d(p2)]-[p3.d/d(p3)] )*INV(d-4) + I4(1,1,1,1);
id s123=s13+s23;
id 1/s123=INV(s13+s23);
argument INV;
id s123=s13+s23;
endargument;
denominators INV;
factarg INV;
chainout INV;
splitarg INV;
id INV(x1?) = 1/x1;
repeat id INV(x1?,x2?,?x) = INV(x1+x2,?x);

#call toinvariants;
id INV(x?)*NUM(x?) = 1;

B INV,I4,s13,s23,s123,NUM;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x1?) = x1;
repeat id NUM(x1?,x2?,?x) = NUM(x1+x2,?x);
id INV(x?)*NUM(x?) = 1;

id I4(1,0,0,1) = pow(-s123,d/2-2);
id I4(0,1,0,1) = pow(-s13,d/2-2);
id I4(1,0,1,0) = pow(-s23,d/2-2);

print+s;
.store

*** solve differential equation with integrating factor ***

G f(s123) = -[d/d(s123)];
id I4(1,1,1,1) = 1;
id I4(?x) = 0;
print+s;
.store
G g(s123) = [d/d(s123)];
id I4(1,1,1,1) = 0;

id I4(1,0,0,1) = pow(-s123,d/2-2);
id I4(0,1,0,1) = pow(-s13,d/2-2);
id I4(1,0,1,0) = pow(-s23,d/2-2);

print+s;
.store

CF exp,int,log;

G M(s123) = exp( int( f(s123) , s123 ) );

argument exp;
id int(1/2*NUM( - 4 + d)*INV( - s23 - s13 + s123),s123) = 1/2*NUM(-4+d)*log( - s23 - s13 + s123);
endargument;
id exp(1/2*NUM( - 4 + d)*log( - s23 - s13 + s123)) = pow(- s23 - s13 + s123,-2+d/2);

*** go to region where s123<0 s13+s23<0 ***
id pow(x?,a?) = pow(-x,a);

print+s;
.store

CF exp,int,log;
CF F21;

L [I4(1,1,1,1)] = INV(M(s123))*(int(M(s123p)*g(s123p),s123p,0,s123))+xC;

id xC = 0;
id INV(pow(x1?,x2?)) = pow(x1,-x2);

argument int;
id INV(x?) = pow(x,-1);
id 1/x? = pow(x,-1);
id NUM(x?) = pow(x,1);
factarg pow,1;
repeat id pow(x1?,x2?,?xx,a?) = pow(x1,a)*pow(x2,?xx,a);
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);

id pow(s123p, - 3 + 1/2*d)*pow(s23 - s123p,-1) =
   pow(s123p, - 3 + 1/2*d)*pow(s13-s123p+(s23-s123p),1)*pow(s23 - s123p,-1)*pow(s13 - s123p,-1)
  -pow(s123p, - 3 + 1/2*d)*pow(s13 - s123p,-1);


endargument;

splitarg int;
repeat id int(x1?,x2?,?x,s123p,0,s123) = int(x1,s123p,0,s123)+int(x2,?x,s123p,0,s123);

normalize int;
id int(x?,?xx) = pow(d-3,1)*pow(-1, - 4 + 1/2*d)*int(x*pow(d-3,-1)*pow(-1, 4 - 1/2*d),?xx);
argument int;
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);
id pow(x?,0) = 1;
endargument;

id int(pow(s13, - 3 + 1/2*d)*pow(s13 - s123p,-1)*pow(s23 + s13 - s123p, - 3 + 1/2*d),s123p,0,s123)
 = TMP(pow(s13, - 3 + 1/2*d)*pow(s13 - s123a,-1)*pow(s23 + s13 - s123a, - 3 + 1/2*d),s123a,0,s123);
id int(pow(s23, - 3 + 1/2*d)*pow(s23 - s123p,-1)*pow(s23 + s13 - s123p, - 3 + 1/2*d),s123p,0,s123)
 = TMP(pow(s23, - 3 + 1/2*d)*pow(s23 - s123a,-1)*pow(s23 + s13 - s123a, - 3 + 1/2*d),s123a,0,s123);
id int(pow(s123p, - 3 + 1/2*d)*pow(s13 - s123p,-1)*pow(s23 - s123p,-1)*pow(s23 + s13 - s123p, - 3 + 1/2*d)*pow(s23 + s13 - 2*s123p,1),s123p,0,s123)
 = TMP(pow(s123b, - 3 + 1/2*d)*pow(s13 - s123b,-1)*pow(s23 - s123b,-1)*pow(s23 + s13 - s123b, - 3 + 1/2*d)*pow(s23 + s13 - 2*s123b,1),s123b,0,s123);

argument TMP;
repeat id pow(x1?,a1?)*pow(x2?,a1?) = pow(x1*x2,a1);
argument pow;
id s123a=-x*ffa+s23+s13;
id s123b^2=-x*ffb+(s23+s13)*s123b;
endargument;
id s123a=-x*ffa+s23+s13;
id s123=1;
factarg pow,1;
repeat id pow(x1?,x2?,?xx,a?) = pow(x1,a)*pow(x2,?xx,a);
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);
id pow(x*ffa-s23,-1) = -pow(1-x*ffa/s23,-1)*pow(s23,-1);
id pow(x*ffa-s13,-1) = -pow(1-x*ffa/s13,-1)*pow(s13,-1);
id pow(x*ffb-s13*s23,-1) = -pow(1-x*ffb/s13/s23,-1)*pow(s13,-1)*pow(s23,-1);

endargument;

id TMP(x1?,-x*ffa+s23+s13,?a) = -ffa*TMP(x1,x,?a);
id TMP(x1?,s123b,0,1) = -ffb*TMP(x1*pow(2*s123b - s23 - s13,-1),x,0,1);
argument TMP;
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);
id pow(x?,0) = 1;
endargument;

id TMP(-pow(ffa, - 3 + 1/2*d)*pow(s13, - 3 + 1/2*d)*pow(s23,-1)*pow(x, - 3 + 1/2*d)*pow(1 - ffa*s23^-1*x,-1),x,0,1)
 = -pow(ffa, - 3 + 1/2*d)*pow(s13, - 3 + 1/2*d)*pow(s23,-1)*TMP(pow(1-x,0)*pow(x, - 3 + 1/2*d)*pow(1 - ffa*s23^-1*x,-1),x,0,1);
id TMP(-pow(ffa, - 3 + 1/2*d)*pow(s23, - 3 + 1/2*d)*pow(s13,-1)*pow(x, - 3 + 1/2*d)*pow(1 - ffa*s13^-1*x,-1),x,0,1)
 = -pow(ffa, - 3 + 1/2*d)*pow(s23, - 3 + 1/2*d)*pow(s13,-1)*TMP(pow(1-x,0)*pow(x, - 3 + 1/2*d)*pow(1 - ffa*s13^-1*x,-1),x,0,1);
id TMP( - pow(s13,-1)*pow(s23,-1)*pow(x, - 3 + 1/2*d)*pow(ffb, - 3 + 1/2*d)*pow(1 - s13^-1*s23^-1*x*ffb,-1),x,0,1)
 = - pow(s13,-1)*pow(s23,-1)*pow(ffb, - 3 + 1/2*d)*TMP( pow(x, - 3 + 1/2*d)*pow(1-x,0)*pow(1 - s13^-1*s23^-1*x*ffb,-1),x,0,1);

argument TMP;
splitarg pow,1;
endargument;
id TMP(pow(x?,a2?)*pow(1,-x?,a3?)*pow(1,-ffa/s23*x?,a1?),x?,0,1) = Gamma(a2+1)*Gamma(a3+1)*IGamma(a2+a3+2)*F21(-a1,a2+1,a3+a2+2,ffa/s23);
id TMP(pow(x?,a2?)*pow(1,-x?,a3?)*pow(1,-ffa/s13*x?,a1?),x?,0,1) = Gamma(a2+1)*Gamma(a3+1)*IGamma(a2+a3+2)*F21(-a1,a2+1,a3+a2+2,ffa/s13);
id TMP(pow(x?,a2?)*pow(1,-x?,a3?)*pow(1,-ffb/s13/s23*x?,a1?),x?,0,1) = Gamma(a2+1)*Gamma(a3+1)*IGamma(a2+a3+2)*F21(-a1,a2+1,a3+a2+2,ffb/s13/s23);

id Gamma(1) = 1;
id IGamma(-1+d/2) = pow(-2+d/2,-1)*IGamma(-2+d/2);
id IGamma(x?)*Gamma(x?) = 1;

id ffa = pow(s13+s23-s123,1);
id ffb = pow(s123,1)*pow(s13+s23-s123,1);
argument pow,F21;
id ffa = s13+s23-s123;
id ffb = (s13+s23-s123)*s123;
endargument;

id F21(1,d/2-2,d/2-1,x?) = pow(1-x,2-d/2)*F21(d/2-2,d/2-2,d/2-1,-NUM(x)*INV(1-x));
argument F21,4;
factarg INV;
factarg NUM;
chainout NUM;
chainout INV;
id INV(x?)*NUM(x?) = 1;
id NUM(-1)=-1;
id INV(-1)=-1;
endargument;

factarg pow,1;
repeat id pow(x1?,x2?,?xx,a?) = pow(x1,a)*pow(x2,?xx,a);
id pow(1/x?,a?) = pow(x,-a);
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);
id pow(x?,0) = 1;
id pow(1/2,-1) = 2;
id pow( - 4 + d,-1) = INV(d-4);
id pow( - 3 + d,1) = NUM(d-3);
id pow(x?,-3+d/2) = pow(x,-2+d/2)/x;

id pow(-1,-2) = 1;
id pow(-1,4-d/2) = pow(-1,-2+d/2);
id pow(-1,-4+d/2) = pow(-1,-2+d/2);
splitarg pow,1;
repeat id pow(x1?,a1?)*pow(x2?,a1?) = pow(x1*x2,a1);
repeat id pow(x1?,x2?,?xx,a?) = pow(x1+x2,?xx,a);
format 150;
B TMP,F21;
print+s;
.sort
hide;

L [zero(b.c.)] = [I4(1,1,1,1)]-[I4(1,1,1,1)(s12=0)];

*** check boundary conditions ***
id s123=s13+s23;
id 1/s123=INV(s13+s23);
argument INV,pow,F21;
id s123=s13+s23;
argument NUM,INV;
id s123=s13+s23;
endargument;
id NUM(0) = 0;
endargument;

id F21(?x,0) = 1;
factarg pow,1;
repeat id pow(x1?,x2?,?xx,a?) = pow(x1,a)*pow(x2,?xx,a);
repeat id pow(x?,a1?)*pow(x?,a2?) = pow(x,a1+a2);
id pow(x?,0) = 1;
id pow(-1,-4+d) = pow(-1,4-d);
id pow(-1,2-d/2) = pow(-1,-2+d/2);

B NUM,INV;
print+s;
.end
