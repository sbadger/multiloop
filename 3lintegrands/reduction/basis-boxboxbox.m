
Get["../../autobasisv1/basis-radium.m"];

(* box-box *)
L=3;
Dim=4;
ExternalMomentaBasis={p1,p2,p4};
Kinematics={p1^2->0,p2^2->0,p4^2->0,p1 p2->s/2,p1 p4->t/2, p2 p4->-(s+t)/2,\[Omega]1^2->-t(s+t) /s};
numeric={s->11,t->3};
Props={l1-p1,l1,l1-p1-p2,l2-p3-p4,l2,l2-p4,l1+l3,l2-l3,l3+p1+p2,l3};
RenormalizationLoopMomenta={
{1,0,0},
{0,1,0},
{0,0,1},
{1,1,1},
{1,0,1},
{0,1,1}
};
RenormalizationPower={4,4,4,8,6,6};

GenerateBasis[]
ISP
Basis

Integrand = 0;
For[j = 1, j <= Length[Basis], j++,
  ThisTerm = cc[]*nc[j];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm*c[ i, Basis[[j, i]]]*ISP[[i]]^Basis[[j, i]];
   ];
  For[i = 1, i <= Length[ISP], i++,
   ThisTerm = ThisTerm //. cc[x1___]*c[i, x2_] -> cc[x1, x2]
   ];
  Integrand = Integrand + ThisTerm;
  ];

x13=DP[l1,p4];
x21=DP[-l7,p1];
x33=DP[-l8,p4];
x31=DP[-l8,p1];

x14=DP[l1,Omega];
x24=DP[-l7,Omega];
x34=DP[-l8,Omega];

fout = OpenWrite["boxboxbox.basis.h"];
WriteString[fout,"id Basisboxboxbox = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];
