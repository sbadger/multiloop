* vim: set foldmethod=marker:

*{{{sortminusl
#procedure sortminusl

id A(-l1?,px?) = xsgn*I*A(l1,px);
id IA(-l1?,px?) = -xsgn*I*IA(l1,px);
id B(-l1?,px?) = xsgn*I*B(l1,px);
id IB(-l1?,px?) = -xsgn*I*IB(l1,px);

id AB(?x,-l1?) = xsgn*I*AB(?x,l1);
id AB(-l1?,?x) = xsgn*I*AB(l1,?x);

id IAB(?x,-l1?) = -xsgn*I*IAB(?x,l1);
id IAB(-l1?,?x) = -xsgn*I*IAB(l1,?x);

id AA(-l1?,?x) = xsgn*I*AA(l1,?x);
id BB(-l1?,?x) = xsgn*I*BB(l1,?x);
id IAA(-l1?,?x) = -xsgn*I*IAA(l1,?x);
id IBB(-l1?,?x) = -xsgn*I*IBB(l1,?x);
id AA(?x,-l1?) = xsgn*I*AA(?x,l1);
id BB(?x,-l1?) = xsgn*I*BB(?x,l1);
id IAA(?x,-l1?) = -xsgn*I*IAA(?x,l1);
id IBB(?x,-l1?) = -xsgn*I*IBB(?x,l1);

id AB(px?,?x,-l1?,?y,py?) = -AB(px,?x,l1,?y,py);
id IAB(px?,?x,-l1?,?y,py?) = -IAB(px,?x,l1,?y,py);
id AA(px?,?x,-l1?,?y,py?) = -AA(px,?x,l1,?y,py);
id IAA(px?,?x,-l1?,?y,py?) = -IAA(px,?x,l1,?y,py);
id BB(px?,?x,-l1?,?y,py?) = -BB(px,?x,l1,?y,py);
id IBB(px?,?x,-l1?,?y,py?) = -IBB(px,?x,l1,?y,py);

#do i=1,4
id IA(b'i',r'i')*IB(r'i',b'i') = xsgn*IAB(b'i',l'i',b'i');
id A(px?,r'i')*B(r'i',py?) = AB(px,r'i',py);
id B(px?,r'i')*AB(r'i',?y) = BB(px,r'i',?y);
id A(px?,r'i')*AB(?y,r'i') = -AA(?y,r'i',px);
id B(px?,r'i')*AA(r'i',?y) = BA(px,r'i',?y);
id A(px?,r'i')*BB(?y,r'i') = -BA(?y,r'i',px);
#enddo

id xsgn=1;
id I^2=-1;

#endprocedure
*}}}

*{{{orderS
#procedure orderS
#do i={9,8,7,6,5,4,3,2,1}
id S(?x1,p'i',?x2) = S(p'i',?x1,?x2);
id IS(?x1,p'i',?x2) = IS(p'i',?x1,?x2);
#enddo
#endprocedure
*}}}



