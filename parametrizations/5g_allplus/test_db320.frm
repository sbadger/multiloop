#-
#include- ../2to3tools.prc

CF TR5,BA;

.global

#include- tmp.prc

id xxdiag1 = 1;
id xxdiag2 = 2*I;
id xxdiag3 = -2;
id xxdiag4 = 2*I;

*id tau1=0;
*id tau2=0;
*id tau3=0;

#call cancel
id dot(p1?ps,p2?ps) = A(p1,p2)*B(p2,p1)/2;
#call cancel
.sort
#call simplify5
repeat;
id AB(p1?,p2?,?x) = A(p1,p2)*BB(p2,?x);
id BB(p1?,p2?) = B(p1,p2);
id BB(p1?,p2?,?x) = B(p1,p2)*AB(p2,?x);
endrepeat;
multiply A(p1,p2)*A(p2,p3)*A(p3,p4)*A(p4,p5)*A(p5,p1);
#call cancel

id dot(p1?ps,p1?ps) = 0;
id AB(p1?ps,p1?ps,?x) = 0;
id AB(?x,p1?ps,p1?ps) = 0;
id AB(p1?ps,p2?ps,p3?ps) = A(p1,p2)*B(p2,p3);
#call cancel

#do i=1,5
#do j=1,5
#if 'j'>'i'
id A(p'i',p'j')*B(p'j',p'i') = s'i''j';
id IB(p'i',p'j') = A(p'j',p'i')/s'i''j';
id IA(p'i',p'j') = B(p'j',p'i')/s'i''j';
#endif
#enddo
#enddo

id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
repeat;
id AB(?x,p1?)*AB(p1?,?y) = AB(?x,p1,?y);
id AB(?x,p1?)*A(p1?,p2?) = AA(?x,p1,p2);
id AB(?x,p1?)*AA(p1?,?y) = AA(?x,p1,?y);
id B(p1?,p2?)*AB(p2?,?x) = BB(p1,p2,?x);
id B(p1?,p2?)*AA(p2?,?x) = BA(p1,p2,?x);
id AA(?x,p1?)*B(p1?,p2?) = AB(?x,p1,p2);
id AA(?x,p1?)*BB(p1?,?y) = AB(?x,p1,?y);
id AA(?x,p1?)*BB(?y,p1?) = -AB(?x,p1,reverse_(?y));
id BA(?x) = AB(reverse_(?x));
endrepeat;

repeat;
id AB(p1?,p2?,p1?) = S(p1,p2);
id AB(px?,?x,p1?,p2?,p1?) = S(p1,p2)*AB(px,?x,p1);
id AB(p1?,p2?,p1?,?x,px?) = S(p1,p2)*AB(p1,?x,px);
id AB(px?,?x,p1?,p2?,p3?,p1?) = -AB(px,?x,p2,p1)*S(p1,p3)+S(p1,p2)*AB(px,?x,p3,p1);
id AB(p1?,p2?,p3?,p1?,?x,px?) = -S(p1,p2)*AB(p1,p3,?x,px)+S(p1,p3)*AB(p1,p2,?x,px);
id AB(px?,?x,p1?,p2?,p3?,p1?,?y,py?) = -AB(px,?x,p1,p2,p1,p3,?y,py)+S(p1,p3)*AB(px,?x,p1,p2,?y,py);
id AB(px?,?x,p1?,p2?,p1?,?y,py?) = S(p1,p2)*AB(px,?x,p1,?y,py);
id AB(px?,?x,p1?,p1?,?y,py?) = 0;
id AB(?x,p1?,p1?) = 0;
id AB(p1?,p1?,?x) = 0;
#call cancel
id S(p1?ps,p1?ps) = 0;
endrepeat;

repeat;
id AB(p1?,p2?,?x) = A(p1,p2)*BB(p2,?x);
id BB(p1?,p2?) = B(p1,p2);
id BB(p1?,p2?,?x) = B(p1,p2)*AB(p2,?x);
endrepeat;
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);
id A(p1?,p2?)*B(p2?,p3?) = AB(p1,p2,p3);
repeat;
id AB(?x,p1?)*AB(p1?,?y) = AB(?x,p1,?y);
id AB(?x,p1?)*A(p1?,p2?) = AA(?x,p1,p2);
id AB(?x,p1?)*AA(p1?,?y) = AA(?x,p1,?y);
id B(p1?,p2?)*AB(p2?,?x) = BB(p1,p2,?x);
id B(p1?,p2?)*AA(p2?,?x) = BA(p1,p2,?x);
id AA(?x,p1?)*B(p1?,p2?) = AB(?x,p1,p2);
id AA(?x,p1?)*BB(p1?,?y) = AB(?x,p1,?y);
id AA(?x,p1?)*BB(?y,p1?) = -AB(?x,p1,reverse_(?y));
id BA(?x) = AB(reverse_(?x));
endrepeat;

repeat;
id AB(p1?,p2?,p1?) = S(p1,p2);
id AB(px?,?x,p1?,p2?,p1?) = S(p1,p2)*AB(px,?x,p1);
id AB(p1?,p2?,p1?,?x,px?) = S(p1,p2)*AB(p1,?x,px);
id AB(px?,?x,p1?,p2?,p3?,p1?) = -AB(px,?x,p2,p1)*S(p1,p3)+S(p1,p2)*AB(px,?x,p3,p1);
id AB(p1?,p2?,p3?,p1?,?x,px?) = -S(p1,p2)*AB(p1,p3,?x,px)+S(p1,p3)*AB(p1,p2,?x,px);
id AB(px?,?x,p1?,p2?,p3?,p1?,?y,py?) = -AB(px,?x,p1,p2,p1,p3,?y,py)+S(p1,p3)*AB(px,?x,p1,p2,?y,py);
id AB(px?,?x,p1?,p2?,p1?,?y,py?) = S(p1,p2)*AB(px,?x,p1,?y,py);
id AB(px?,?x,p1?,p1?,?y,py?) = 0;
id AB(?x,p1?,p1?) = 0;
id AB(p1?,p1?,?x) = 0;
#call cancel
id S(p1?ps,p1?ps) = 0;
endrepeat;

repeat;
id AB(p1?,p2?,p3?,p4?,p1?,?y,py?) = AB(p1,p2,p3,p4,p1)*AB(p1,?y,py);
id AB(px1?,px2?,p1?,p2?,p3?,p4?,p1?) = AB(px1,px2,p1)*AB(p1,p2,p3,p4,p1);
endrepeat;
id AB(p5?,p1?,p2?,p3?,p4?,p1?,p5?) = S(p1,p5)*AB(p1,p4,p3,p2,p1);

id AB(p1?,p2?,p3?,p4?,p1?) = (S(p1,p2)*S(p3,p4)-S(p1,p3)*S(p2,p4)+S(p1,p4)*S(p2,p3) - TR5(p1,p2,p3,p4))/2;
id TR5(?x,p5,?y) = -TR5(?x,p1,?y)-TR5(?x,p2,?y)-TR5(?x,p3,?y)-TR5(?x,p4,?y);
id TR5(?x,p1?,?y,p1?,?z) = 0;
id TR5(?x) = ASYM(?x);
id ASYM(p1,p2,p3,p4) = tr5;

repeat;
id AB(p1?,p2?,?x) = A(p1,p2)*BB(p2,?x);
id BB(p1?,p2?) = B(p1,p2);
id BB(p1?,p2?,?x) = B(p1,p2)*AB(p2,?x);
endrepeat;
id A(p1?,p2?)*B(p2?,p1?) = S(p1,p2);

#do i=1,5
#do j=1,5
#if 'j'>'i'
id S(p'i',p'j') = s'i''j';
id S(p'j',p'i') = s'i''j';
id dot(p'i',p'j') = s'i''j'/2;
#endif
#enddo
#enddo

format 150;
B AB,AA,BB,BA;
print[];
.sort
*.end

id tr5^2 = s12^2*s34^2-2*s12*s23*s34*s14-2*s12*s24*s13*s34+s13^2*s24^2-2*s13*s24*s23*s14+s23^2*s14^2;
id NUM(x?) = x;

#do i=1,5
#do j=1,5
id 1/s'i''j' = INV(s'i''j');
#enddo
#enddo

argument NUM,INV;
id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;
endargument;
factarg INV;
chainout INV;
splitarg INV;
id INV(x?) = 1/x;
repeat id INV(p1?,p2?,?x) = INV(p1+p2,?x);

id s13 = s45-s12-s23;
id s14 = s23-s45-s15;
id s24 = s15-s23-s34;
id s25 = s34-s15-s12;
id s35 = s12-s34-s45;

AB s12,s23,s34,s45,s15;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
#call cancel
id NUM(x?) = x;

AB s12,s23,s34,s45,s15;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
#call cancel
id NUM(x?) = x;

AB s12,s23,s34,s45,s15;
.sort
collect NUM;
factarg NUM;
chainout NUM;
splitarg NUM;
id NUM(x?) = x;
repeat id NUM(p1?,p2?,?x) = NUM(p1+p2,?x);
#call cancel

format 150;
B AB,tr5,TR5;
print[];
.sort

multiply IA(p1,p2)*IA(p2,p3)*IA(p3,p4)*IA(p4,p5)*IA(p5,p1);
#call cancel

id NUM(x?) = x;
#call changevars
*#call simplify5new
.sort

multiply ds(0,0,0);
repeat;
id,once tau1*ds(x1?,x2?,x3?) = ds(x1+1,x2,x3);
id,once tau2*ds(x1?,x2?,x3?) = ds(x1,x2+1,x3);
id,once tau3*ds(x1?,x2?,x3?) = ds(x1,x2,x3+1);
endrepeat;

#include- ../final/db320D.coeffvecs.prc

B tau1,...,tau3;
print[];
.store

#do k=1,79

G dvec'k' = AMP;
id ds('k') = 1;
id ds(x?) = 0;
id tr5 = TR5;
.sort
format mathematica;
#write <dcoeffs_xvars.m> "ddd'k' := %e" dvec'k'
*format maple;
*#write <dcoeffs.mpl> "ddd'k' := %e" dvec'k'
B TR5;
print[];
.store

#enddo

.end
