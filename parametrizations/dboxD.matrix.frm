#-
#include- 2to2tools.prc
.global

#define sol "1"

#do i=1,7
G L'i'(nu1) = l'i';
#enddo
G MU11 = mu11;
G MU12 = mu12;
G MU22 = mu22;
G Omega(nu1) = (AB(p2,p3,p1)*eta(p1,p2)-AB(p1,p3,p2)*eta(p2,p1))/2/s12;
#include- final/dboxD.OSprops.h
#include- final/dboxD.OSsol.h

*id a3 = tau1;
*id a4 = tau2;
*id b3 = tau3;
*id b4 = tau4;

id a3 = (-tau1+tau2+s14)/s13/2;
id a4 = (-tau1-tau2+s14)/s14/2;
id b3 = (-tau3-tau4+s14)/s13/2;
id b4 = (-tau3+tau4+s14)/s14/2;


id eta(p1?,p2?) = AB(p1,nu1,p2);
id p1?{p1,p2,p3,p4} = MOM(p1,nu1);

#call simplify4

B AB,MOM;
print+s;
.store

G basis41(nu1) = MOM(p1,nu1);
G basis42(nu1) = MOM(p2,nu1);
G basis43(nu1) = MOM(p4,nu1);
G basis44(nu1) = Omega(nu1);

.store

G x13 = 2*MOM(k1,nu1)*basis43(nu1);
G x14 = 2*MOM(k1,nu1)*basis44(nu1);
G x21 = 2*MOM(k2,nu1)*basis41(nu1);
G x24 = 2*MOM(k2,nu1)*basis44(nu1);

L zero1 = MOM(k1,nu1)*MOM(k1,nu1)-mu11;
L zero2 = MOM(k1,nu1)*MOM(p1,nu1);
L zero3 = MOM(k1,nu1)*MOM(p1,nu1)+MOM(k1,nu1)*MOM(p2,nu1)-s12/2;
L zero4 = MOM(k2,nu1)*MOM(p3,nu1)+MOM(k2,nu1)*MOM(p4,nu1)-s12/2;
L zero5 = MOM(k2,nu1)*MOM(p4,nu1);
L zero6 = MOM(k2,nu1)*MOM(k2,nu1)-mu22;
L zero7 = 2*MOM(k1,nu1)*MOM(k2,nu1)-mu12;

id mu11 = MU11;
id mu12 = MU12;
id mu22 = MU22;

id MOM(k1,nu1) = L1(nu1);
id MOM(k2,nu1) = -L6(nu1);

id MOM(p1?,nu1?)*MOM(p2?,nu1?) = dot(p1,p2);
id MOM(p1?,nu1?)*AB(p2?,nu1?,p3?) = AB(p2,p1,p3);
id AB(p1?,nu1?,p2?)*AB(p3?,nu1?,p4?) = -2*A(p1,p3)*B(p2,p4);

#call simplify4

print+s;
.store

G Delta = BasisdboxD;

#include- final/dboxD.basis.h

id Power(x1?,x2?) = x1^x2;

id mu11 = MU11;
id mu12 = MU12;
id mu22 = MU22;

#call simplify4
id NUM(x?) = x;

#$nc = 1;
B cc;
.sort
keep brackets;
id cc(?x) = cc($nc,?x);
$nc=$nc+1;
.sort
#write "{'$nc'-1} integrand coefficients"
#do cc=1,{'$nc'-1}
id cc('cc',?x$clbl) = cc('cc',?x);
.sort
#write <final/dboxD.coeffvecs.mpl> "Clbl['cc']:=cs('$clbl'):"
#enddo

.store

#write <dboxD.reduceM.log> "ReduceMatrix:=Matrix({'$nc'-1},{'$nc'-1}):"
#write <final/dboxD.coeffvecs.mpl> "Dvec:=Vector(160):"
#define dd "0"

#do pow1=0,4
#do pow2=0,4
#do pow3=0,4
#do pow4=0,4

#$nonzero=0;
L tmpRow = Delta;

#do pp=1,4
if(count(tau'pp',1)!=`pow`pp'');
discard;
endif;
id tau`pp'=1;
id 1/tau`pp'=1;
#enddo

.sort
$nonzero=$nonzero+1;
.sort
hide;

*#write "'pow1','pow2','pow3','pow4','$nonzero'"

#if '$nonzero'>0
#define dd "{'dd'+1}"
#write "'dd'"
#write <final/dboxD.coeffvecs.mpl> "Dvec['dd']:=ds('pow1','pow2','pow3','pow4'):"
#write <final/dboxD.coeffvecs.prc> "id ds('pow1','pow2','pow3','pow4') = ds('dd');"

#do cc=1,{'$nc'-1}
  L tmpEl'cc' = tmpRow;
  id cc('cc',?x$clbl) = 1;
  id cc(?x)=0;
  .sort
  format maple;
  #write <dboxD.reduceM.log> "ReduceMatrix['dd','cc']:=%E:" tmpEl'cc'
  .sort
  hide;
#enddo

#endif

unhide;
.store

#enddo
#enddo
#enddo
#enddo

.end
