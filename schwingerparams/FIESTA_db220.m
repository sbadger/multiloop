NumberOfLinks=10;

STRATEGY=STRATEGY_S;

Get["/lscr221/badger/src/FIESTA2.4/FIESTA_2.4.0.m"];

NumberOfSubkernels=2;
CIntegratePath="/lscr221/badger/src/FIESTA2.4/CIntegrateMP";
QLinkPath="/lscr221/badger/src/FIESTA2.4/QLink64";

d0=8
epsorder=2
PrecisionShift=50

SDEvaluate[
UF[{k1, k2},{-k1^2, -(k1+p1)^2, -(k1+p1+p2)^2, -(k2-p1-p2)^2, -(k2+p4)^2, -k2^2, -(k1+k2)^2},
   {p1^2->0, p2^2->0, p4^2->0, p1*p2->s12/2, p1*p4->s23/2, p2*p4->-(s12+s23)/2, s12->-3, s23->-7}],
   {1,1,1,1,1,1,1},0]
ClearResults[]

SDEvaluate[
UF[{k1, k2},{-k1^2, -(k1+p1)^2, -(k1+p1+p2)^2, -(k2-p1-p2)^2, -(k2+p4)^2, -k2^2, -(k1+k2)^2},
   {p1^2->0, p2^2->0, p4^2->0, p1*p2->s12/2, p1*p4->s23/2, p2*p4->-(s12+s23)/2, s12->-3, s23->-7}],
   {2,2,1,1,1,1,1},0]
ClearResults[]

SDEvaluate[
UF[{k1, k2},{-k1^2, -(k1+p1)^2, -(k1+p1+p2)^2, -(k2-p1-p2)^2, -(k2+p4)^2, -k2^2, -(k1+k2)^2},
   {p1^2->0, p2^2->0, p4^2->0, p1*p2->s12/2, p1*p4->s23/2, p2*p4->-(s12+s23)/2, s12->-3, s23->-7}],
   {3,1,1,1,1,1,1},0]
ClearResults[]


Exit[]

