id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,-1/2),ptl(p3?,-1,1)) =
	 A(p1,p2)*I;

id AMP(ptl(p1?,-1,-1),ptl(p2?,-1,1/2),ptl(p3?,-1,1/2)) =
	  - A(p2,p3)*I;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,-1,-1/2),ptl(p3?,1,1)) =
	 A(p1,p2)*I;

id AMP(ptl(p1?,-1,-1),ptl(p2?,-1,1/2),ptl(p3?,1,1/2)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,-1/2),ptl(p3?,-1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1),ptl(p2?,1,1/2),ptl(p3?,-1,1/2)) =
	  0;

id AMP(ptl(p1?,-1,-1/2),ptl(p2?,1,-1/2),ptl(p3?,1,1)) =
	  0;

id AMP(ptl(p1?,-1,-1),ptl(p2?,1,1/2),ptl(p3?,1,1/2)) =
	  - B(p2,p3)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,-1/2),ptl(p3?,-1,1)) =
	  0;

id AMP(ptl(p1?,1,-1),ptl(p2?,-1,1/2),ptl(p3?,-1,1/2)) =
	  - A(p2,p3)*I;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,-1,-1/2),ptl(p3?,1,1)) =
	  0;

id AMP(ptl(p1?,1,-1),ptl(p2?,-1,1/2),ptl(p3?,1,1/2)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,-1/2),ptl(p3?,-1,1)) =
	 B(p1,p2)*I;

id AMP(ptl(p1?,1,-1),ptl(p2?,1,1/2),ptl(p3?,-1,1/2)) =
	  0;

id AMP(ptl(p1?,1,-1/2),ptl(p2?,1,-1/2),ptl(p3?,1,1)) =
	 B(p1,p2)*I;

id AMP(ptl(p1?,1,-1),ptl(p2?,1,1/2),ptl(p3?,1,1/2)) =
	  - B(p2,p3)*I;

