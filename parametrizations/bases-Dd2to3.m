Get["/lscr221/badger/gitrepos/mathematicam2/BasisDet-1-02.m"];

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* db320 *)
L=2;
Dim=4-2\[Epsilon];
n=5;
ExternalMomentaBasis = {p1, p2, p3, p5};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1 p2 -> s12/2,
   p1 p3 -> (s123 - s12 - s23)/2,
   p1 p5 -> s15/2,
   p2 p3 -> s23/2,
   p2 p5 -> (s125 - s15 - s12)/2,
   p3 p5 -> (s12 - s123 - s125)/2,
   \[Omega]1^2 -> -s12*(s12 + s23)/s12};
numeric = {s12 -> 11, s123 -> 3, s15 -> 17, s23 -> 19, s125 -> 29};
Props = {l1, l1 - p1, l1 - p1 - p2, l1 - p1 - p2 - p3, l2, l2 - p5, l2 + p1 + p2 + p3, l1 + l2};
RenormalizationCondition = {{{1, 0}, 5}, {{0, 1}, 4}, {{1, 1}, 7}};

GenerateBasis[0]

fout = OpenWrite["final/db320D.basis.h"];
WriteString[fout,"id Basisdb320D = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

Exit[];

(* db220 *)
L=2;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis = {p1, p2, p3};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0,
   p1 p2 -> s12/2,
   p1 p3 -> (s123 - s12 - s23)/2,
   p1 p5 -> s15/2,
   p2 p3 -> s23/2,
   p2 p5 -> (s125 - s15 - s12)/2,
   p3 p5 -> (s12 - s123 - s125)/2,
   \[Omega]1^2 -> -s12*(s12 + s23)/s12};
numeric = {s12 -> 11, s123 -> 3, s15 -> 17, s23 -> 19, s125 -> 29};
Props = {l1, l1 - p1, l1 - p1 - p2, l2, l2 + p1 + p2 + p3 , l2 + p1 + p2, l1 + l2};
RenormalizationCondition = {{{1, 0}, 4}, {{0, 1}, 4}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["final/db220D.basis.h"];
WriteString[fout,"id Basisdb220D = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

(* db220b *)
L=2;
Dim=4-2\[Epsilon];
n=5;
ExternalMomentaBasis = {p1, p2, p3, p5};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1 p2 -> s12/2,
   p1 p3 -> (s123 - s12 - s23)/2,
   p1 p5 -> s15/2,
   p2 p3 -> s23/2,
   p2 p5 -> (s125 - s15 - s12)/2,
   p3 p5 -> (s12 - s123 - s125)/2,
   \[Omega]1^2 -> -s12*(s12 + s23)/s12};
numeric = {s12 -> 11, s123 -> 3, s15 -> 17, s23 -> 19, s125 -> 29};
Props = {l1, l1 - p1, l1 - p1 - p2, l2, l2 + p5 + p1 + p2 + p3 , l2 + p5 + p1 + p2, l1 + l2};
RenormalizationCondition = {{{1, 0}, 4}, {{0, 1}, 4}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["final/db220bD.basis.h"];
WriteString[fout,"id Basisdb220bD = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];


