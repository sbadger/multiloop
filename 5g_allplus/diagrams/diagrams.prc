CFunction Prop,PropGlu,PropGht,VertGlu3,VertGlu4,VertGht2Glu,VertGht2Glu2;
auto Symbol mu,nu,rho;
auto Symbol x,y,p,l,kk,qq,kq;
S k1,k2,k3;
Symbol mu11,mu12,mu22,mu33;

CFunction Loop,TmpLoop,IDX;
CFunction P,L,V,V3,V4,IS;

ExtraSymbols Vector Z;

#procedure expandtopo

.sort

id V(?x1,P(y1?,y2?,y3?,y4?,y5?,y6?),?x2) = V(?x1,P(y1,y2,y3),P(y4,y5,y6),?x2);

id V(P(p1?,mu1?,0),P(p2?,mu2?,0),P(p3?,mu3?,0)) = VertGlu3(p1,p2,p3,mu1,mu2,mu3);
id V(P(p1?,mu1?,1),P(p2?,mu2?,0),P(p3?,mu3?,1)) = VertGht2Glu(p3,mu2,p1);
id V(P(p1?,mu1?,0),P(p2?,mu2?,1),P(p3?,mu3?,1)) = VertGht2Glu(p2,mu1,p3);
id V(P(p1?,mu1?,1),P(p2?,mu2?,1),P(p3?,mu3?,0)) = VertGht2Glu(p2,mu3,p1);

id V(P(p1?,mu1?,0),P(p2?,mu2?,0),P(p3?,mu3?,0),P(p4?,mu4?,0)) = VertGlu4(mu1,mu2,mu3,mu4);
id V(P(p1?,mu1?,1),P(p2?,mu2?,1),P(p3?,mu3?,1),P(p4?,mu4?,1)) = I/2;
id V(P(p1?,mu1?,0),P(p2?,mu2?,0),P(p3?,mu3?,0),P(p4?,mu4?,1)) = 0;
id V(P(p1?,mu1?,0),P(p2?,mu2?,0),P(p3?,mu3?,1),P(p4?,mu4?,0)) = 0;
id V(P(p1?,mu1?,0),P(p2?,mu2?,1),P(p3?,mu3?,0),P(p4?,mu4?,0)) = 0;
id V(P(p1?,mu1?,1),P(p2?,mu2?,0),P(p3?,mu3?,0),P(p4?,mu4?,0)) = 0;

id V(P(p1?,mu1?,1),P(p2?,mu2?,1),P(p3?,mu3?,0),P(p4?,mu4?,0)) = xxv1*VertGht2Glu2(mu3,mu4);
id V(P(p1?,mu1?,1),P(p2?,mu2?,0),P(p3?,mu3?,1),P(p4?,mu4?,0)) = xxv2*VertGht2Glu2(mu2,mu4);
id V(P(p1?,mu1?,1),P(p2?,mu2?,0),P(p3?,mu3?,0),P(p4?,mu4?,1)) = xxv3*VertGht2Glu2(mu2,mu3);
id V(P(p1?,mu1?,0),P(p2?,mu2?,1),P(p3?,mu3?,1),P(p4?,mu4?,0)) = xxv4*VertGht2Glu2(mu1,mu4);
id V(P(p1?,mu1?,0),P(p2?,mu2?,1),P(p3?,mu3?,0),P(p4?,mu4?,1)) = xxv5*VertGht2Glu2(mu1,mu3);
id V(P(p1?,mu1?,0),P(p2?,mu2?,0),P(p3?,mu3?,1),P(p4?,mu4?,1)) = xxv6*VertGht2Glu2(mu1,mu2);

id Prop(mu1?,mu2?,0) = PropGlu(mu1,mu2);
id Prop(p?,mu1?,mu2?,0) = PropGlu(mu1,mu2)*IS(p);
id Prop(mu1?,mu2?,1) = I;

#endprocedure

#procedure subvertices

repeat;
id,once VertGlu3(p1?, p2?, p3?, mu1?, mu2?, mu3?) = I/rt2*(
    GGG(mu2,mu3)*(MOM(p2,mu1)-MOM(p3,mu1))
  + GGG(mu3,mu1)*(MOM(p3,mu2)-MOM(p1,mu2))
  + GGG(mu1,mu2)*(MOM(p1,mu3)-MOM(p2,mu3))
);

al,once VertGlu4(mu1?,mu2?,mu3?,mu4?) = I/2*(
  - GGG(mu1,mu2)*GGG(mu3,mu4)
  - GGG(mu2,mu3)*GGG(mu1,mu4)
  + 2*GGG(mu1,mu3)*GGG(mu2,mu4)
);

al,once VertGht2Glu(p1?, mu1?, p2?) = I/rt2*(
    MOM(p1,mu1)/2 - MOM(p2,mu1)/2
);

al,once VertGht2Glu2(mu1?, mu2?) = I/2*(
    GGG(mu1,mu2)
);

#call Lcontract
id,once PropGlu(mu1?, mu2?) = I*( -GGG(mu1,mu2) );
#call Lcontract
endrepeat;

#endprocedure
