#-
S s,t,u,tau,tau1,...,tau3;
S Nf,Ns,[4-Nf],[1-Nf+Ns],[3-Ns];
auto S x,p,e,q,P,m,k,w,d;
S [k.3],[q.2],[k.w],[q.w];

CF S,IS,SS,ISS,dot;
CF NUM,INV;
CF MOM;
auto CF TMP;
CF A(a),B(a),IA(a),IB(a);
CF AA,AB,BB,BA,naA,naB;
CF IAA,IAB,IBB,IBA,InaA,InaB;
auto S symb;
S gg,I;

set ps:p0;
set mps:p1;

#include- ../lib/factorise.h

off stats;
.global

#include- bub.d2c.h

denominators INV;
#call sortINV
#call wrap(gg);
.sort
#call ISortCollect
AB gg,S,IS;
.sort
collect NUM;
#call wrap(gg);
#call SortCollect
#call cancel
#call unwrap(gg);

#do sol=1,2
#do pow1=-2,2
#do pow2=0,4
#write <bub.d2c.final.mpl> "d'sol'{'pow1'+2}'pow2' := ds['sol','pow1','pow2']:"
#enddo
#enddo
#enddo

#define csset "{c000,c100,c010,c001,c110,c011,c101,c2x2,cx22}"
#define i "1"
#do c='csset'
#write <bub.d2c.final.h>   "L 'c' = %e" 'c'
#write <bub.d2c.final.mpl> "csolA['i'] := %E:" 'c'
#redefine i "{'i'+1}"
#enddo

#do k=1,14
#write <bub.d2c.final.h>   "L zero'k' = %e" zero'k'
#write <bub.d2c.final.mpl> "zeroA['k'] := %E:" zero'k'
#enddo

#do k=1,9
#write <bub.d2c.final.mpl> "printf(\"'k',\%A = \%.16Ze\\\n\",cs['k'],csol['k']);"
#enddo
#do k=10,24
#write <bub.d2c.final.mpl> "printf(\"zero'k' = \%.16Ze\\\n\",zeroA['k']);"
#enddo

format 150;
B INV;
print+s;
.end
