Get["/lscr221/badger/gitrepos/mathematicam2/BasisDet-1-02.m"];
(*Get["/Users/simon/gitrepos/BasisDet/BasisDet-1-02.m"];*)

\[Mu]11 = mu11;
\[Mu]12 = mu12;
\[Mu]22 = mu22;

(* db3315L *)
L=2;
Dim=4-2\[Epsilon];
n=5;
ExternalMomentaBasis = {p1, p2, p3, p5};
Kinematics = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p5^2 -> 0,
   p1 p2 -> s12/2,
   p1 p3 -> (s45 - s12 - s23)/2,
   p1 p5 -> s15/2,
   p2 p3 -> s23/2,
   p2 p5 -> (s34 - s15 - s12)/2,
   p3 p5 -> (s12 - s45 - s34)/2};
numeric = {s12 -> 11, s23 -> 17, s34->7, s45->3, s15->29};
Props = {l1, l1 - p1, l1 - p2, l2, l2 - p5, l2 + p1 + p2 + p3, l1 + l2};
RenormalizationCondition = {{{1, 0}, 4}, {{0, 1}, 4}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["basis/db3315L.basis.h"];
WriteString[fout,"id Basisdb3315L = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

(* db331M2 *)
L=2;
Dim=4-2\[Epsilon];
n=4;
ExternalMomentaBasis = {p1, p23, p5};
Kinematics = {p1^2 -> 0, p23^2 -> s23, p5^2 -> 0,
   p1 p23 -> (s45 - s12)/2,
   p1 p5 -> (s34 - s12)/2,
   p23 p5 -> (s12 - s34 - s45)/2,
   p1 \[Omega]1 -> 0,
   p23 \[Omega]1 -> 0,
   p5 \[Omega]1 -> 0,
   \[Omega]1^2 -> -s12*(s12 + s23)/s12};
numeric = {s12 -> 11, s23 -> 17, s34->7, s45->3, s15->29};
Props = {l1, l1 - p1, l1 - p1 - p23, l2, l2 - p5, l2 + p1 + p23, l1 + l2};
RenormalizationCondition = {{{1, 0}, 4}, {{0, 1}, 4}, {{1, 1}, 6}};

GenerateBasis[0]

fout = OpenWrite["basis/db331M2.basis.h"];
WriteString[fout,"id Basisdb331M2 = \n"];
Write[fout,Integrand // CForm];
WriteString[fout,";\n"];
Close[fout];

Exit[];

