* vim: set foldmethod=marker:
#-
#include ../2to3tools.prc
#include diagrams.prc

auto CF INTdb, INTbt, Delta;
CF F1,F2,F3,F4,ww,MU,trp,trm;
S Dsm2;
S w,w123,w543,w132,w534,w12p,w12m,w45p,w45m,w13p,w13m,w53p,w53m;

off stats;
.global

G INT431 = Delta431(k1,k2,p1,p2,p3,p4,p5);
G INT430 = Delta430(k1,k2,p1,p2,p3,p4,p5);
G INT331M2 = Delta331M2(k1,k2,p1,p2,p3,p4,p5);
G INT3315L = Delta3315L(k1,k2,p1,p2,p3,p4,p5);
G INT330M2 = Delta330M2(k1,k2,p1,p2,p3,p4,p5);
G INT3305L = Delta3305L(k1,k2,p1,p2,p3,p4,p5);

*** non-planar ***

G INTNP332 = DeltaNP332(k1,k2,p1,p2,p3,p4,p5);
G INTNP232 = DeltaNP232(k1,k2,p1,p2,p3,p4,p5);
G INTNP331M1 = DeltaNP331M1(k1,k2,p1,p2,p3,p4,p5);
G INTNP331M2 = DeltaNP331M2(k1,k2,p1,p2,p3,p4,p5);
G INTNP331M3 = DeltaNP331M3(k1,k2,p1,p2,p3,p4,p5);
G INTNP331M4 = DeltaNP331M4(k1,k2,p1,p2,p3,p4,p5);

G INTNP422 = DeltaNP422(k1,k2,p1,p2,p3,p4,p5);

#include- analytic_5g+++++.prc

topolynomial;
.sort
format maple;
#do topo={431,430,331M2,3315L,330M2,3305L,NP332,NP232,NP331M1,NP331M2,NP331M3,NP331M4,NP422}
#write <checks_5g+++++.mpl> "Delta_'topo' := proc(p, k1, k2, mu11, mu12, mu22)"
#write <checks_5g+++++.mpl> "local Z,mu33,p1,p2,p3,p4,p5,s12,s13,s14,s15,s23,s24,s25,s34,s35,s45,TR5;"
#write <checks_5g+++++.mpl> "  p1 := p[1];"
#write <checks_5g+++++.mpl> "  p2 := p[2];"
#write <checks_5g+++++.mpl> "  p3 := p[3];"
#write <checks_5g+++++.mpl> "  p4 := p[4];"
#write <checks_5g+++++.mpl> "  p5 := p[5];"
#write <checks_5g+++++.mpl> "  TR5:=tr5(p1,p2,p3,p4);"
#write <checks_5g+++++.mpl> "  mu33 := mu11+mu12+mu22;"
#write <checks_5g+++++.mpl> "  %X;"
#write <checks_5g+++++.mpl> "  return %e;" INT'topo'
#write <checks_5g+++++.mpl> "end:"
#enddo
.end
